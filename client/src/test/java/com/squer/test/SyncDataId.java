package com.squer.test;

/**
 * Created by ashutoshpavaskar on 26/06/15.
 */
public class SyncDataId {
    private long numberLong;

    public long getNumberLong() {
        return numberLong;
    }

    public void setNumberLong(long numberLong) {
        this.numberLong = numberLong;
    }
}
