package com.squer.services;

/**
 * Created by mithila on 23/03/18.
 */
public class TestReportDTO {

    private String patientId;

    private byte[] content;

    private String reportId;

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }
}
