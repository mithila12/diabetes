package com.squer.services;

import com.squer.sfe.api.common.entity.reference.DoctorReference;

import java.util.Date;

/**
 * Created by mithila on 20/03/18.
 */
public class MessageDTO {

    private DoctorReference doctor;
    private byte[] content;
    private String message;
    private Date date;
    private String id;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DoctorReference getDoctor() {
        return doctor;
    }

    public void setDoctor(DoctorReference doctor) {
        this.doctor = doctor;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
