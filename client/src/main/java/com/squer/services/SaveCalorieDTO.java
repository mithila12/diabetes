package com.squer.services;

import com.squer.sfe.api.common.entity.PatientCalories;

import java.util.List;

/**
 * Created by mithila on 31/08/17.
 */
public class SaveCalorieDTO {
    List<PatientCalories> list;

    public List<PatientCalories> getList() {
        return list;
    }

    public void setList(List<PatientCalories> list) {
        this.list = list;
    }
}
