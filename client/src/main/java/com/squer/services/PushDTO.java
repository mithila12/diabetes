package com.squer.services;

/**
 * Created by mithila on 19/04/18-Apr-2018 at 1:57 AM
 */
public class PushDTO {

    private String owner;
    private String registerToken;
    private Boolean ios;

    public Boolean getIos() {
        return ios;
    }

    public void setIos(Boolean ios) {
        this.ios = ios;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getRegisterToken() {
        return registerToken;
    }

    public void setRegisterToken(String registerToken) {
        this.registerToken = registerToken;
    }
}
