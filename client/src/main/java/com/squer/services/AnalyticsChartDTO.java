package com.squer.services;

import java.awt.*;
import java.util.List;
import java.util.Map;

/**
 * Created by mithila on 22/05/18-May-2018 at 12:59 AM
 */
public class AnalyticsChartDTO {
   private String name;
   private String type;
   private List<Integer> data;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Integer> getData() {
        return data;
    }

    public void setData(List<Integer> data) {
        this.data = data;
    }
}
