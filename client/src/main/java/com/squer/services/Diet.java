package com.squer.services;

/**
 * Created by mithila on 26/05/17.
 */
public class Diet {
    private String early_morning;
    private String breakfast;
    private String mid_morning;
    private String lunch;
    private String mid_afternoon;
    private String mid_evening;
    private String dinner;
    private String bed_time;

    public String getEarly_morning() {
        return early_morning;
    }

    public void setEarly_morning(String early_morning) {
        this.early_morning = early_morning;
    }

    public String getBreakfast() {
        return breakfast;
    }

    public void setBreakfast(String breakfast) {
        this.breakfast = breakfast;
    }

    public String getMid_morning() {
        return mid_morning;
    }

    public void setMid_morning(String mid_morning) {
        this.mid_morning = mid_morning;
    }

    public String getLunch() {
        return lunch;
    }

    public void setLunch(String lunch) {
        this.lunch = lunch;
    }

    public String getMid_afternoon() {
        return mid_afternoon;
    }

    public void setMid_afternoon(String mid_afternoon) {
        this.mid_afternoon = mid_afternoon;
    }

    public String getMid_evening() {
        return mid_evening;
    }

    public void setMid_evening(String mid_evening) {
        this.mid_evening = mid_evening;
    }

    public String getDinner() {
        return dinner;
    }

    public void setDinner(String dinner) {
        this.dinner = dinner;
    }

    public String getBed_time() {
        return bed_time;
    }

    public void setBed_time(String bed_time) {
        this.bed_time = bed_time;
    }
}
