package com.squer.services;

import com.squer.platform.ServiceLocator;
import com.squer.platform.appframework.auth.UserEntity;
import com.squer.platform.appframework.auth.reference.UserEntityReference;
import com.squer.platform.appframework.entity.SquerReference;
import com.squer.platform.appframework.exception.SquerException;
import com.squer.platform.registry.CoreQuery;
import com.squer.platform.utils.CommonUtils;
import com.squer.platform.utils.StringUtil;
import com.squer.registry.CommonQuery;
import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.utility.CriteriaMap;
import com.squer.platform.registry.PlatformDelegates;
import com.squer.sfe.api.common.entity.Doctor;
import com.squer.sfe.api.common.entity.OtpStore;
import com.squer.sfe.api.common.entity.Patient;
import com.squer.sfe.api.common.entity.reference.PatientReference;
import com.squer.sfe.api.common.impl.typehandler.CommonUtil;

import javax.ws.rs.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.squer.services.PatientService.sendSms;

/**
 * Created by mithila on 12/04/17.
 */
@Path("/ResetPassword")
public class PasswordResetService {

    @Path("/saveOtp")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Map<String,String> saveOtp(PasswordDTO dto) throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try{
            Map<String,String> listMap = new HashMap<String, String>();
            CriteriaMap criteriaMap_doc=new CriteriaMap(CommonQuery.USER_ID_SELECT);
            criteriaMap_doc.addCriteria("userName",dto.getUserName());
            List<OtpStore> otps = repository.find(criteriaMap_doc);
            String contact ="";

            OtpStore otpStore = new OtpStore();
            String otp = CommonUtil.generateOtp();

            if(!CommonUtils.isNullOrEmpty(otps)){
                otpStore = otps.get(0);
                otpStore.setOtp(otp);

                listMap.put("userId",otpStore.getUser().getId());
                listMap.put("otp",otp);
                repository.update(otpStore);

                if(otpStore.getUser().getId().startsWith("DOCTR")){
                    CriteriaMap doctor = new CriteriaMap(CommonQuery.DIABETES_DOCTOR_SELECT);
                    doctor.addCriteria("id",otpStore.getUser().getId());

                    List<Doctor> doctors = repository.find(doctor);
                    contact = doctors.get(0).getContactOffice();
                    listMap.put("contactNo",contact);
                }
                else if(otpStore.getUser().getId().startsWith("PATNT")){
                    CriteriaMap patient = new CriteriaMap(CommonQuery.DIABETES_PATIENT_SELECT);
                    patient.addCriteria("id",otpStore.getUser().getId());

                    List<Patient> patients = repository.find(patient);
                    contact = patients.get(0).getContactNo();
                    listMap.put("contactNo",contact);
                }

            }else {
                CriteriaMap ownerCriteria = new CriteriaMap(CommonQuery.OWNER_ID_SELECT);
                ownerCriteria.addCriteria("userName",dto.getUserName());
                List<Map<String,String>> users = repository.find(ownerCriteria);
                String owner_id = users.get(0).get("owner_id");

                listMap.put("userId",owner_id);
                listMap.put("otp",otp);
                contact = "";
                if(owner_id.startsWith("DOCTR")){
                    CriteriaMap doctor = new CriteriaMap(CommonQuery.DIABETES_DOCTOR_SELECT);
                    doctor.addCriteria("id",owner_id);

                    List<Doctor> doctors = repository.find(doctor);
                     contact = doctors.get(0).getContactOffice();
                    listMap.put("contactNo",contact);
                }
                else if(owner_id.startsWith("PATNT")){
                    CriteriaMap patient = new CriteriaMap(CommonQuery.DIABETES_PATIENT_SELECT);
                    patient.addCriteria("id",owner_id);

                    List<Patient> patients = repository.find(patient);
                     contact = patients.get(0).getContactNo();
                    listMap.put("contactNo",contact);
                }
                otpStore.setOtp(otp);
                otpStore.setUser((SquerReference) ServiceLocator.getInstance().getReference(owner_id));
                repository.create(otpStore);


            }
            String template = "Your One Time Password for SugrQb Username: "+dto.getUserName()+" is "+otp;
            String response = sendSms(template,contact);
            return listMap;
        }catch (Exception e){
            throw new SquerException(e);
        }

    }

    private static boolean sendOtp(String otp,String contact){
        System.out.println(otp);
        System.out.print(contact);
        return true;
    }



    @Path("/verifyOtp")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public boolean verifyOtp(PasswordDTO dto) throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try{
            CriteriaMap criteriaMap_doc=new CriteriaMap(CommonQuery.OTP_STORE_SELECT);
            criteriaMap_doc.addCriteria("user",dto.getUserId());
            List<OtpStore> storeList = repository.find(criteriaMap_doc);
            if(!CommonUtils.isNullOrEmpty(storeList)){
                OtpStore store=storeList.get(0);
                if(dto.getOtp().equalsIgnoreCase(store.getOtp())){
                     CriteriaMap criteriaMap=new CriteriaMap(CoreQuery.USER_SELECT);
                     criteriaMap.addCriteria("owner",dto.getUserId());
                     List<UserEntity>entityList=repository.find(criteriaMap);
                     UserEntity entity=entityList.get(0);
                     entity.setPassword(StringUtil.getHash(dto.getPassword()));
                     repository.update(entity);
                }else{
                    throw new Exception("Invalid otp entered");
                }
            }else {
                throw new Exception("User not found");
            }
        }catch (Exception e){
            throw new SquerException(e);
        }
        return true;
    }

    @Path("/checkAvailability/{username}")
    @GET
    @Consumes("application/json")
    @Produces("application/json")
    public Boolean checkAvailability(@PathParam("username")String username) throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        CriteriaMap criteriaMap_doc=new CriteriaMap(CoreQuery.USER_SELECT);
        criteriaMap_doc.addCriteria("username",username);
        List<UserEntity> users=repository.find(criteriaMap_doc);
        Boolean message = false;
        if(!users.isEmpty()){
            message= false;
        }
        else{
            message = true;
        }
        return message;
    }

}


