package com.squer.services;

import java.util.List;

/**
 * Created by mithila on 26/05/17.
 */
public class Region {
    private String name;
    private String calories;
    private String carbohydrates;
    private String proteins;
    private String fat;
    private String visible_fat;
    private Diet diet;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCalories() {
        return calories;
    }

    public void setCalories(String calories) {
        this.calories = calories;
    }

    public String getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(String carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public String getProteins() {
        return proteins;
    }

    public void setProtiens(String proteins) {
        this.proteins = proteins;
    }

    public String getFat() {
        return fat;
    }

    public void setFat(String fat) {
        this.fat = fat;
    }

    public String getVisible_fat() {
        return visible_fat;
    }

    public void setVisible_fat(String visible_fat) {
        this.visible_fat = visible_fat;
    }

    public Diet getDiet() {
        return diet;
    }

    public void setDiet(Diet diet) {
        this.diet = diet;
    }
}
