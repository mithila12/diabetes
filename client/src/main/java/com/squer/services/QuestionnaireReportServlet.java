package com.squer.services;

import com.squer.platform.ServiceLocator;
import com.squer.platform.api.security.authentication.AuthenticationHandler;
import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.utility.CriteriaMap;
import com.squer.platform.registry.PlatformDelegates;
import com.squer.registry.CommonQuery;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mithila on 23/06/18-Jun-2018 at 10:18 PM
 */
public class QuestionnaireReportServlet extends HttpServlet{

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);

        String authCertificate = request.getParameter("certificate");
        String type = request.getParameter("type");

//        Gson gson = new Gson();
//        JsonElement jsonElement = gson.fromJson(request.getReader(), JsonElement.class);
//        JsonObject jsonObject = jsonElement.getAsJsonObject();
//        ArrayList<String> doctors = gson.fromJson(jsonObject.get("doctors").getAsJsonArray(), new TypeToken<ArrayList<String>>(){}.getType());
        try {
            AuthenticationHandler.authenticate(authCertificate);

            CriteriaMap map = new CriteriaMap(CommonQuery.QUESTIONNAIRE_DETAILS_SELECT);
            map.addCriteria("displayType",type);

            List<Map> questions = repository.find(map);

            Map<String,Map<String,Integer>> doctorwiseMap = new HashMap<>();
            Map<String,Map<String,Integer>> doctorwiseAvgMap = new HashMap<>();

            for(Map q:questions){
                if(doctorwiseMap.containsKey(q.get("name"))){
                    Map<String,Integer> questionCountMap=doctorwiseAvgMap.get(q.get("name"));
                    Map<String,Integer> questionAnswerMap=doctorwiseMap.get(q.get("name"));
                    if(questionAnswerMap.containsKey(q.get("question"))){
                        int average = doctorwiseAvgMap.get(q.get("name")).get(q.get("question"));
                        if(average<Integer.parseInt(q.get("average").toString())){
                            questionCountMap.put(q.get("question").toString(),Integer.parseInt(q.get("average").toString()));
                            questionAnswerMap.put(q.get("question").toString(),Integer.parseInt(q.get("answer").toString()));
                            doctorwiseMap.put(q.get("name").toString(),questionAnswerMap);
                            doctorwiseAvgMap.put(q.get("name").toString(),questionCountMap);
                        }
                    } else {
                        questionCountMap.put(q.get("question").toString(),Integer.parseInt(q.get("average").toString()));
                        questionAnswerMap.put(q.get("question").toString(),Integer.parseInt(q.get("answer").toString()));
                        doctorwiseMap.put(q.get("name").toString(),questionAnswerMap);
                        doctorwiseAvgMap.put(q.get("name").toString(),questionCountMap);

                    }
                }else{
                    Map<String,Integer> questionCountMap = new HashMap<>();
                    Map<String,Integer> questionAnswerMap = new HashMap<>();
                    questionCountMap.put(q.get("question").toString(),Integer.parseInt(q.get("average").toString()));
                    doctorwiseAvgMap.put(q.get("name").toString(),questionCountMap);
                    questionAnswerMap.put(q.get("question").toString(),Integer.parseInt(q.get("answer").toString()));
                    doctorwiseMap.put(q.get("name").toString(),questionAnswerMap);
                }
            }

            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet worksheet = workbook.createSheet("QuestionnaireReport");
            HSSFCellStyle my_style_1 = workbook.createCellStyle();
            my_style_1.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

            HSSFRow detail = worksheet.createRow(0);

            HSSFCell doctorCell = detail.createCell(0);
            doctorCell.setCellValue("Doctor");

            HSSFCell questionCell = detail.createCell(1);
            questionCell.setCellValue("Question");

            HSSFCell answer = detail.createCell(2);
            answer.setCellValue("Majority Answer");
            int index = 1;

            for(Map.Entry<String,Map<String,Integer>> m: doctorwiseMap.entrySet()){
                HSSFRow detailP = worksheet.createRow(index);
                HSSFCell question = detailP.createCell(0);
                question.setCellValue(m.getKey());
                question.setCellStyle(my_style_1);
                worksheet.addMergedRegion(new CellRangeAddress(index,index+m.getValue().size()-1,0,0));
                int innerIndex = 0;
                for(Map.Entry<String,Integer> q: m.getValue().entrySet()){
                    if(innerIndex > 0){
                        detailP = worksheet.createRow(index);
                    }
                    HSSFCell que = detailP.createCell(1);
                    que.setCellValue(q.getKey());

                    HSSFCell ans = detailP.createCell(2);
                    ans.setCellValue(q.getValue());

                    index++;
                    innerIndex++;
                }

            }

            ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
            workbook.write(outByteStream);
            response.setHeader("Content-Type", "xls");
            response.setHeader("Content-Length", String.valueOf(outByteStream.size()));
            response.setHeader("Content-Disposition", "attachment; filename=\"QuestionnaireReport"+new Date().toString()+".xls\"");
            response.getOutputStream().write(outByteStream.toByteArray());
            outByteStream.flush();
            outByteStream.close();

        }catch (Exception e){

        }
    }
    }
