package com.squer.services;

import java.util.List;

/**
 * Created by mithila on 25/05/17.
 */
public class FaqDTO {
    private List<Faqs> faqs;

    public List<Faqs> getFaqs() {
        return faqs;
    }

    public void setFaqs(List<Faqs> faqs) {
        this.faqs = faqs;
    }
}
