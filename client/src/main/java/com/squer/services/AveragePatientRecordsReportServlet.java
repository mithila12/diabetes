package com.squer.services;

import com.google.common.reflect.TypeToken;
import com.squer.platform.ServiceLocator;
import com.squer.platform.api.security.authentication.AuthenticationHandler;
import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.utility.CriteriaMap;
import com.squer.platform.registry.PlatformDelegates;
import com.squer.registry.CommonQuery;
import com.squer.sfe.api.common.entity.Doctor;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by mithila on 20/06/18-Jun-2018 at 10:59 PM
 */

//TODO replace this file in your code
public class AveragePatientRecordsReportServlet extends HttpServlet{

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);

        String authCertificate = request.getParameter("certificate");
        String type = request.getParameter("type");
        String toDateString = request.getParameter("toDate");
        String fromDateString = request.getParameter("fromDate");


//        Gson gson = new Gson();
//        JsonElement jsonElement = gson.fromJson(request.getReader(), JsonElement.class);
//        JsonObject jsonObject = jsonElement.getAsJsonObject();
//        ArrayList<String> doctors = gson.fromJson(jsonObject.get("doctors").getAsJsonArray(), new TypeToken<ArrayList<String>>(){}.getType());
        try {
            AuthenticationHandler.authenticate(authCertificate);
            CriteriaMap recordsMap = new CriteriaMap(CommonQuery.AVERAGE_PATIENT_RECORDS_DETAILS_SELECT);
//        if (dto.getDoctors() != null) {
//            recordsMap.addCriteria("doctors", dto.getDoctors());
//        }
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            Date fromDate = new SimpleDateFormat("yyyy-MM-dd").parse(fromDateString);
            Date toDate = new SimpleDateFormat("yyyy-MM-dd").parse(toDateString);

            long fromDateLong = fromDate.getTime();
            long toDateLong = toDate.getTime();
            getDays(fromDateLong,toDateLong);

            if (type != null) {
                recordsMap.addCriteria("type", type);
            }
            if (fromDate != null) {
                recordsMap.addCriteria("fromDate", fromDate);
            }
            if (toDate != null) {
                recordsMap.addCriteria("toDate", toDate);
            }

            List<String> date = new ArrayList<>();

            while (fromDate.before(toDate) || fromDate.equals(toDate)){
                SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
                String dt1=format.format(fromDate.getTime());
                date.add(dt1);
                Calendar calendar1=Calendar.getInstance();
                calendar1.setTime(fromDate);
                calendar1.add(Calendar.DATE,1);
                fromDate=calendar1.getTime();
            }

            Map<String, Map<String, Integer>> datewiseMap = new LinkedHashMap<>();
            Map<String, List<Map>> doctorMap = new HashMap<>();
            List<Map> records = repository.find(recordsMap);

            if (records.size() > 0) {

                for (Map r : records) {
                    Doctor doctor = (Doctor) repository.restore(ServiceLocator.getInstance().getReference(r.get("doctor").toString()));
                    if (doctorMap.containsKey(doctor.getName())) {
                        List l = doctorMap.get(doctor.getName());
                        l.add(r);
                        doctorMap.put(doctor.getName(), l);
                    } else {
                        List l = new ArrayList();
                        l.add(r);
                        doctorMap.put(doctor.getName(), l);
                    }
                }
            }
//                if(doctorMap.containsKey(r.get("report").toString())){
//                    Map<String,Integer> typeCountMap = datewiseMap.get(r.get("report").toString());
//                    if(typeCountMap.containsKey(r.get(dto.getDuration()).toString())){
//                        int prevCount = typeCountMap.get(r.get(dto.getDuration()).toString());
//                        int newcount = prevCount + Integer.parseInt(r.get("count").toString());
//                        typeCountMap.put(r.get(dto.getDuration()).toString(),newcount);
//                    }else{
//                        typeCountMap.put(r.get(dto.getDuration()).toString(),Integer.parseInt(r.get("count").toString()));
//                    }
//                    datewiseMap.put(r.get("report").toString(),typeCountMap);
//                }else{
//                    Map<String,Integer> typeCountMap = new LinkedHashMap<>();
//                    typeCountMap.put(r.get(dto.getDuration()).toString(),Integer.parseInt(r.get("count").toString()));
//                    datewiseMap.put(r.get("report").toString(),typeCountMap);
//                }


            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet worksheet = workbook.createSheet("AveragePatientRecords");
            HSSFCellStyle my_style_1 = workbook.createCellStyle();
            my_style_1.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

                HSSFRow heading = worksheet.createRow(0);
                HSSFCell doc =heading.createCell(0);
                doc.setCellValue("Doctor Name");

                HSSFCell patientName =heading.createCell(1);
                patientName.setCellValue("Patient Name");

                HSSFCell reportType =heading.createCell(2);
                reportType.setCellValue("Report Type");

                HSSFCell dateValue =heading.createCell(3);
                dateValue.setCellValue("Date");

                HSSFCell reportValue =heading.createCell(4);
                reportValue.setCellValue("Report Value");

            int index = 1;

            for(Map.Entry<String, List<Map>> m : doctorMap.entrySet()) {
                HSSFRow detail = worksheet.createRow(index);
                HSSFCell doctorCell = detail.createCell(0);
                doctorCell.setCellValue(m.getKey());
                doctorCell.setCellStyle(my_style_1);
                worksheet.addMergedRegion(new CellRangeAddress(index,index+m.getValue().size()-1,0,0));
                int innerIndex=0;
                for(Map k:m.getValue()){
                    if(innerIndex > 0){
                        detail = worksheet.createRow(index);
                    }
                    //HSSFRow patientdetail = worksheet.createRow(index);

                    HSSFCell patient = detail.createCell(1);
                    patient.setCellValue(k.get("patientName").toString());

                    HSSFCell rtype = detail.createCell(2);
                    rtype.setCellValue(k.get("report").toString());

                    HSSFCell rdate = detail.createCell(3);
                    rdate.setCellValue(k.get("week").toString());

                    HSSFCell value = detail.createCell(4);
                    value.setCellValue(k.get("value").toString());
                    innerIndex++;
                    index++;
                }
            }

            ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
            workbook.write(outByteStream);
            response.setHeader("Content-Type", "xls");
            response.setHeader("Content-Length", String.valueOf(outByteStream.size()));
            response.setHeader("Content-Disposition", "attachment; filename=\"PatientRecordsReport"+new Date().toString()+".xls\"");
            response.getOutputStream().write(outByteStream.toByteArray());
            outByteStream.flush();
            outByteStream.close();



        }catch (Exception e){
            try {
                throw new Exception(e);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

    }


    public long getDays(long currentTime, long endDateTime) {

        Calendar endDateCalendar;
        Calendar currentDayCalendar;


        //expiration day
        endDateCalendar = Calendar.getInstance(TimeZone.getTimeZone("EST"));
        endDateCalendar.setTimeInMillis(endDateTime);
        endDateCalendar.set(Calendar.MILLISECOND, 0);
        endDateCalendar.set(Calendar.MINUTE, 0);
        endDateCalendar.set(Calendar.HOUR, 0);
        endDateCalendar.set(Calendar.HOUR_OF_DAY, 0);

        //current day
        currentDayCalendar = Calendar.getInstance(TimeZone.getTimeZone("EST"));
        currentDayCalendar.setTimeInMillis(currentTime);
        currentDayCalendar.set(Calendar.MILLISECOND, 0);
        currentDayCalendar.set(Calendar.MINUTE, 0);
        currentDayCalendar.set(Calendar.HOUR,0);
        currentDayCalendar.set(Calendar.HOUR_OF_DAY, 0);


        long remainingDays = (long)Math.ceil((float) (endDateCalendar.getTimeInMillis() - currentDayCalendar.getTimeInMillis()) / (24 * 60 * 60 * 1000));

        return remainingDays;}



    }
