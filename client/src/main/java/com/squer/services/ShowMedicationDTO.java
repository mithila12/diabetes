package com.squer.services;

import com.squer.platform.appframework.config.reference.UserLovReference;

import java.util.List;

/**
 * Created by mithila on 19/03/18.
 */
public class ShowMedicationDTO {
    private String name;

    private UserLovReference route;

    private String dose;

    private List<UserLovReference> doseTimings;

    private String since;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserLovReference getRoute() {
        return route;
    }

    public void setRoute(UserLovReference route) {
        this.route = route;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public List<UserLovReference> getDoseTimings() {
        return doseTimings;
    }

    public void setDoseTimings(List<UserLovReference> doseTimings) {
        this.doseTimings = doseTimings;
    }

    public String getSince() {
        return since;
    }

    public void setSince(String since) {
        this.since = since;
    }
}
