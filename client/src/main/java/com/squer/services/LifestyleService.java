package com.squer.services;

import com.squer.platform.ServiceLocator;
import com.squer.platform.appframework.entity.SquerReference;
import com.squer.platform.appframework.exception.SquerException;
import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.utility.CriteriaMap;
import com.squer.platform.registry.PlatformDelegates;
import com.squer.platform.rest.impl.services.DateDeserializer;
import com.squer.platform.rest.impl.services.SquerReferenceDeserializer;
import com.squer.registry.CommonQuery;
import com.squer.sfe.api.common.entity.Document;
import com.squer.sfe.api.common.entity.PatientLifestyle;
import com.squer.sfe.api.common.entity.PatientLifestyleDTO;
import org.codehaus.jackson.map.deser.CustomDeserializerFactory;
import org.codehaus.jackson.map.deser.StdDeserializerProvider;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.squer.platform.utils.CommonUtils.isNullOrEmpty;

/**
 * Created by user on 1/17/2017.
 */
@Path("/LifestyleService")
public class LifestyleService {

    @Path("/saveAnswers")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void saveAnswers(String obj) throws Exception{
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            org.codehaus.jackson.map.ObjectMapper mapper = new org.codehaus.jackson.map.ObjectMapper();
            CustomDeserializerFactory sf = new CustomDeserializerFactory();
            sf.addSpecificMapping(SquerReference.class, new SquerReferenceDeserializer());
            sf.addSpecificMapping(Date.class, new DateDeserializer());
            mapper.setDeserializerProvider(new StdDeserializerProvider(sf));
            PatientLifestyleDTO dto = mapper.readValue(obj, PatientLifestyleDTO.class);
            List<PatientLifestyle> list = dto.getList();

            if(!isNullOrEmpty(list)){
                List<String> questions = new ArrayList<>();
                String patientId = list.get(0).getPatient().getId();
                for (PatientLifestyle p : list) {
                    if (!questions.contains(p.getLifeStyle().getId()))
                        questions.add(p.getLifeStyle().getId());
                }

                CriteriaMap criteriaMap = new CriteriaMap(CommonQuery.PATIENT_QUESTION_BULK_DELETE);
                criteriaMap.addCriteria("patient", patientId);
                criteriaMap.addCriteria("questions", questions);
                repository.fireAdhoc(criteriaMap);

                for (PatientLifestyle p : list) {
                    repository.create(p);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
}
