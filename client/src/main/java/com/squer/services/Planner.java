package com.squer.services;

import java.util.List;

/**
 * Created by mithila on 26/05/17.
 */
public class Planner {
    private List<Region> region;

    public List<Region> getRegion() {
        return region;
    }

    public void setRegion(List<Region> region) {
        this.region = region;
    }
}
