package com.squer.services;

import com.sun.org.apache.xpath.internal.operations.Bool;

/**
 * Created by mithila on 19/07/17.
 */
public class EmailDTO {
    private String body;
    private String subject;
    private String toEmail;
    private Boolean sendCopy;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getToEmail() {
        return toEmail;
    }

    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }

    public Boolean getSendCopy() {
        return sendCopy;
    }

    public void setSendCopy(Boolean sendCopy) {
        this.sendCopy = sendCopy;
    }
}
