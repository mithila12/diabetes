package com.squer.services;

import java.util.List;

/**
 * Created by mithila on 18/04/18-Apr-2018 at 4:00 PM
 */
public class QuestionsWrapperDTO {

    private List<QuestionDTO> questionDTOS;

    public List<QuestionDTO> getQuestionDTOS() {
        return questionDTOS;
    }

    public void setQuestionDTOS(List<QuestionDTO> questionDTOS) {
        this.questionDTOS = questionDTOS;
    }
}
