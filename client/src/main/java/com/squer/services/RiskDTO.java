package com.squer.services;

/**
 * Created by mithila on 08/07/17.
 */
public class RiskDTO {
    private Double min;
    private Double max;

    public Double getMin() {
        return min;
    }

    public void setMin(Double min) {
        this.min = min;
    }

    public Double getMax() {
        return max;
    }

    public void setMax(Double max) {
        this.max = max;
    }
}
