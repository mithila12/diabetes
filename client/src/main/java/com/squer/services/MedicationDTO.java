package com.squer.services;

import com.squer.sfe.api.common.entity.Medication;

import java.util.List;

/**
 * Created by mithila on 05/09/17.
 */
public class MedicationDTO {
    private List<Medication> list;

    public List<Medication> getList() {
        return list;
    }

    public void setList(List<Medication> list) {
        this.list = list;
    }
}
