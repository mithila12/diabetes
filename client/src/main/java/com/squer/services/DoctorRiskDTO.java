package com.squer.services;

import java.util.List;

/**
 * Created by mithila on 19/06/18-Jun-2018 at 11:35 PM
 */
public class DoctorRiskDTO {

    private List<String> doctors;
    private String type;

    public List<String> getDoctors() {
        return doctors;
    }

    public void setDoctors(List<String> doctors) {
        this.doctors = doctors;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
