package com.squer.services;

import java.util.List;
import java.util.Map;

/**
 * Created by mithila on 02/09/18-Sep-2018 at 11:15 PM
 */
public class RangeDTO {

    private List<String> rangeList;

    private List<Map> rangeMap;

    public List<String> getRangeList() {
        return rangeList;
    }

    public void setRangeList(List<String> rangeList) {
        this.rangeList = rangeList;
    }

    public List<Map> getRangeMap() {
        return rangeMap;
    }

    public void setRangeMap(List<Map> rangeMap) {
        this.rangeMap = rangeMap;
    }
}
