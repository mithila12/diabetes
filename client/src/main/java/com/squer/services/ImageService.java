package com.squer.services;

import com.squer.platform.ServiceLocator;
import com.squer.platform.appframework.config.reference.SysLovReference;
import com.squer.platform.appframework.exception.SquerException;
import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.utility.CriteriaMap;
import com.squer.platform.registry.PlatformDelegates;
import com.squer.platform.utils.CommonUtils;
import com.squer.platform.utils.FileUtil;
import com.squer.platform.utils.SystemPropertyEnum;
import com.squer.platform.utils.SystemUtil;
import com.squer.registry.CommonQuery;
import com.squer.sfe.api.common.entity.Document;
import com.squer.sfe.api.common.entity.DocumentUtil;
import com.squer.sfe.api.common.entity.Patient;
import com.squer.sfe.api.common.entity.reference.DocumentReference;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.map.HashedMap;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 1/3/2017.
 */
@Path("/ImageService")
public class ImageService {
    @POST
    @Path("/upload")
    @SuppressWarnings("unused")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public static boolean base64ToByte(DocumentDTO document) throws Exception {
        Repository repository = (Repository)ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        DocumentReference reference = DocumentUtil.createDocument(document.getOwnerId(),document.getType(),document.getContent());

        return true;
    }

    @Path("/select/{owner}/{type}")
    @GET
    @Consumes("application/json")
    @Produces("application/json")
    public DocumentDTO selectDocument(@PathParam("owner")String owner, @PathParam("type")String type) throws Exception {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            CriteriaMap criteriaMap_doc = new CriteriaMap(CommonQuery.SELECT_DOCUMENT);
            criteriaMap_doc.addCriteria("owner", owner);

            List<Document> documents = repository.find(criteriaMap_doc);
            DocumentDTO dto=  new DocumentDTO();
            for(Document d: documents){
                java.nio.file.Path path = Paths.get(System.getProperty("config.path")+"/documents/"+ServiceLocator.getInstance().getPrincipal().getTenantId() + "/" + d.getOwner().getId() + "." + d.getName());
                byte[] image = Files.readAllBytes(path);
                dto.setByteContent(image);
                dto.setId(owner);
            }

            return dto;
        }
        catch (Exception e){
            throw new SquerException(e);
        }
    }


    public static DocumentDTO fetchDocument(String owner,String type) throws Exception {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            CriteriaMap criteriaMap_doc = new CriteriaMap(CommonQuery.SELECT_DOCUMENT);
            criteriaMap_doc.addCriteria("owner", owner);

            List<Document> documents = repository.find(criteriaMap_doc);
            DocumentDTO dto=  new DocumentDTO();
            for(Document d: documents){
                java.nio.file.Path path = Paths.get(System.getProperty("config.path")+"/documents/"+ServiceLocator.getInstance().getPrincipal().getTenantId() + "/" + d.getOwner().getId() + "." + d.getName());
                byte[] image = Files.readAllBytes(path);
                dto.setByteContent(image);
                dto.setId(owner);
            }

            return dto;
        }
        catch (Exception e){
            throw new SquerException(e);
        }
    }

    @Path("/delete/{owner}")
    @GET
    @Consumes("application/json")
    @Produces("application/json")
    public boolean deleteDocument(@PathParam("owner")String owner) throws Exception {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            CriteriaMap criteriaMap_doc=new CriteriaMap(CommonQuery.SELECT_DOCUMENT);
            criteriaMap_doc.addCriteria("owner",owner);

            List<Document> documents=repository.find(criteriaMap_doc);

            if(!CommonUtils.isNullOrEmpty(documents)){
                CriteriaMap criteriaMap_delDoc=new CriteriaMap(CommonQuery.DELETE_DOCUMENT_BY_OWNER);
                criteriaMap_delDoc.addCriteria("owner",owner);
                repository.fireAdhoc(criteriaMap_delDoc);

                for(Document d:documents){
                    File file=new File(System.getProperty("config.path")+"/documents/"+ServiceLocator.getInstance().getPrincipal().getTenantId()+ "/" + d.getId() + "." +d.getName());
                    file.delete();
                }
            }

            repository.delete(ServiceLocator.getInstance().getReference(owner));
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
}