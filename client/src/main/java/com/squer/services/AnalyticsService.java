package com.squer.services;

import com.squer.platform.ServiceLocator;
import com.squer.platform.api.registry.SecurityQuery;
import com.squer.platform.appframework.auth.UserEntity;
import com.squer.platform.persistence.utility.QueryCondition;
import com.squer.registry.CommonQuery;
import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.utility.CriteriaMap;
import com.squer.platform.registry.PlatformDelegates;
import com.squer.sfe.api.common.entity.AnalyticsData;
import com.squer.sfe.api.common.entity.Doctor;
import com.squer.sfe.api.common.entity.Patient;
import com.squer.sfe.api.common.entity.reference.AnalyticsDataReference;
import org.apache.commons.collections.map.HashedMap;
import org.apache.regexp.RE;
import org.springframework.security.access.method.P;

import javax.ws.rs.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by mithila on 19/04/18-Apr-2018 at 2:47 PM
 */
@Path("/analytics")
public class AnalyticsService {

    @POST
    @Path("/saveAnalyticsData")
    @Produces("application/json")
    @Consumes("application/json")
    public AnalyticsDataReference saveAnalytics(AnalyticsDTO analyticsData) throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try {
            String patientId ="";
            if( ServiceLocator.getInstance().getPrincipal().getUser().getOwner()!=null){
                patientId = ServiceLocator.getInstance().getPrincipal().getUser().getOwner().getId();
            }

            AnalyticsData data = new AnalyticsData();
            if(analyticsData.getOwner()!=null){
                data.setOwner(ServiceLocator.getInstance().getReference(analyticsData.getOwner()));
            }else {
                data.setOwner(ServiceLocator.getInstance().getReference(patientId));
            }
            switch (analyticsData.getPageName()){
                case "desLf":
                    analyticsData.setPageName("SQDTSQ");
                    break;
                case "menu.home":
                    analyticsData.setPageName("Home Page");
                    break;
                case "newmed":
                    analyticsData.setPageName("New Medication");
                    break;
                case "enterRecords":
                    analyticsData.setPageName("Enter New Records");
                    break;
                case "notifKnowledge":
                    analyticsData.setPageName("Notification");
                    break;
                case "medone":
                    analyticsData.setPageName("Medication Details");
                    break;
                case "editOverview":
                    analyticsData.setPageName("Edit Overview");
                    break;
                case "searchPage":
                    analyticsData.setPageName("Search Patients");
                    break;
            }
            data.setPageName(analyticsData.getPageName());
            data.setName(analyticsData.getName());
            Date endDate = new Date(analyticsData.getEndTime());
            data.setEndTime(endDate);
            Date startDate = new Date(analyticsData.getStartTime());
            data.setStartTime(startDate);


            return (AnalyticsDataReference) repository.create(data);
        }catch (Exception e){
            throw new Exception(e);
        }

    }

    @GET
    @Path("/getActiveUsersCount")
    @Consumes("application/json")
    @Produces("application/json")
    public int getActiveUsersCount() throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try{
            CriteriaMap map = new CriteriaMap(SecurityQuery.USER_SELECT);
            List<UserEntity> users = repository.find(map);

            int usercount = users.size();

            return usercount;
        }catch (Exception e){
            throw new Exception(e);
        }

    }

    @GET
    @Path("/getQuestionnaireFilledCount")
    @Consumes("application/json")
    @Produces("application/json")
    public int getQuestionnaireFilledCount() throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try{
            CriteriaMap map = new CriteriaMap(CommonQuery.QUESTIONNAIRE_FILLED_COUNT);
            List<Map> questionFilledMap = repository.find(map);

            int usercount = questionFilledMap.size();

            return usercount;
        }catch (Exception e){
            throw new Exception(e);
        }

    }


    @GET
    @Path("/activeDoctorsCount")
    @Consumes("application/json")
    @Produces("application/json")
    public int activeDoctorsCount() throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try {
            CriteriaMap doctorMap = new CriteriaMap(CommonQuery.DIABETES_DOCTOR_SELECT);
            QueryCondition condition = new QueryCondition("STATUS_ID","SYSLV00000000000000000000000000000001");
            List<QueryCondition> conditions = new ArrayList<>();
            conditions.add(condition);
            doctorMap.addCriteria("searchQuery",conditions);
            List<Doctor> doctors = repository.find(doctorMap);

            int doctorcount = doctors.size();

            return doctorcount;
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    @GET
    @Path("/activePatientCount")
    @Consumes("application/json")
    @Produces("application/json")
    public int activePatientsCount() throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try {
            CriteriaMap patientMap = new CriteriaMap(CommonQuery.DIABETES_PATIENT_SELECT);
            QueryCondition condition = new QueryCondition("STATUS_ID","SYSLV00000000000000000000000000000001");
            List<QueryCondition> conditions = new ArrayList<>();
            conditions.add(condition);
            patientMap.addCriteria("searchQuery",conditions);
            List<Patient> patients = repository.find(patientMap);

            int patientcount = patients.size();

            return patientcount;
        }catch (Exception e){
            throw new Exception(e);
        }


    }


    @GET
    @Path("/totalRegisteredPatientCount")
    @Consumes("application/json")
    @Produces("application/json")
    public int registeredPatientCount() throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try {
            CriteriaMap patientMap = new CriteriaMap(CommonQuery.DIABETES_PATIENT_SELECT);
            List<Patient> patients = repository.find(patientMap);

            int patientcount = patients.size();

            return patientcount;
        }catch (Exception e){
            throw new Exception(e);
        }


    }

    @GET
    @Path("/totalRegisteredDoctorsCount")
    @Consumes("application/json")
    @Produces("application/json")
    public int registeredDoctorsCount() throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try {
            CriteriaMap doctorMap = new CriteriaMap(CommonQuery.DIABETES_DOCTOR_SELECT);
            List<Doctor> doctors = repository.find(doctorMap);

            int doctorcount = doctors.size();

            return doctorcount;
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    @GET
    @Path("/avgPatientsEnrolled")
    @Produces("application/json")
    @Consumes("application/json")
    public double avgPatients() throws Exception{
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try {
            CriteriaMap doctorMap = new CriteriaMap(CommonQuery.AVERAGE_PATIENTS_ENROLLED_BY_DOCTOR);
            List<Map> doctors = repository.find(doctorMap);


            double doctorcount = Double.parseDouble(doctors.get(0).get("pataverage").toString());

            return doctorcount;
        } catch (Exception e) {
            throw new Exception(e);
        }
    }


    //TODO just pass todate and fromdate from url in homecontroller.js
    @GET
    @Path("/getAverageTimeSpentByApp/{fromDate}/{toDate}")
    @Produces("application/json")
    @Consumes("application/json")
    public List<Map> getAvgTimeSpent(@PathParam("fromDate") String fromDate, @PathParam("toDate") String toDate) throws Exception{
        Repository repository =(Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try{
            CriteriaMap map = new CriteriaMap(CommonQuery.AVERAGE_TIME_SPENT_ON_APP);
            map.addCriteria("fromDate",fromDate);
            map.addCriteria("toDate",toDate);
            List<Map> mapList = repository.find(map);

            if(mapList.size()>0){
                for(Map m: mapList){
                    double seconds = Double.parseDouble(m.get("totalTime").toString());
                    double hours = seconds /3600;
                    double perc = (hours * 100)/24 ;
                    m.put("perc",perc);
                    m.put("hours",hours);
                }
            }

            return mapList;
        }catch (Exception e){
            throw new Exception(e);
        }
    }

    @GET
    @Path("/genderwisePatients")
    @Produces("application/json")
    @Consumes("application/json")
    public List<AnalyticsChartDTO> getGenderwisePatients() throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try{
            CriteriaMap map = new CriteriaMap(CommonQuery.GENDERWISE_PATIENT_SELECT);
            List<Map> patients = repository.find(map);
            List<AnalyticsChartDTO> chartDTOS = new ArrayList<>();
            Map<String, Map> patientMap = new HashMap<>();
            List<Map<String,List>>patientsListMap = new ArrayList<>();
            Map<String,List>genderCountMap = new HashMap<>();
            Map<String,List<Integer>> countmap = new HashMap<>();
            if(patients.size()>0){
                for(Map p: patients){
                    if(p.containsKey("age")) {
                        countmap = getAgeRange(countmap, Integer.parseInt(p.get("age").toString()), Integer.parseInt(p.get("count").toString()), p.get("gender").toString());

                    }
                }


                for(Map.Entry<String,List<Integer>> r: countmap.entrySet()) {
                    AnalyticsChartDTO chartDTO = new AnalyticsChartDTO();
                    chartDTO.setType("column");
                    chartDTO.setData(r.getValue());
                    chartDTO.setName(r.getKey());

                    chartDTOS.add(chartDTO);
                }

            }
            return chartDTOS;
        }catch (Exception e){
            throw new Exception(e);
        }

    }

    @POST
    @Path("/averagePatientRecords/")
    @Produces("application/json")
    @Consumes("application/json")
    public StackChartDTO averagePatientRecords(AnalyticsReportsDTO dto) throws Exception{
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try{
            CriteriaMap recordsMap = new CriteriaMap(CommonQuery.AVERAGE_PATIENT_RECORDS_SELECT);
            if(dto.getDoctors()!=null && dto.getDoctors().size()>0){
                recordsMap.addCriteria("doctors",dto.getDoctors());
            }
            if(dto.getType()!=null){
                recordsMap.addCriteria("type",dto.getType());
            }


            List<String> date=new ArrayList<>();

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            //Date toDate = calendar.getTime();
            recordsMap.addCriteria("toDate",dto.getToDate());
            recordsMap.addCriteria("fromDate",dto.getFromDate());

            Date fromDate = new SimpleDateFormat("yyyy-MM-dd").parse(dto.getFromDate());
            Date toDate = new SimpleDateFormat("yyyy-MM-dd").parse(dto.getToDate());

            long fromDateLong = fromDate.getTime();
            long toDateLong = toDate.getTime();

            long diffInMilis = toDateLong - fromDateLong;

            long diffInDays = diffInMilis / (24 * 60 * 60 * 1000);

            int weeks = (int) diffInDays%7;



            String timeSpan ="";
            if(dto.getFilter().equalsIgnoreCase("week")){
                timeSpan = "week";
            }else{
                if(diffInDays<=7){
                    timeSpan ="week";
                }else if (diffInDays>7 && diffInDays<=60){
                    timeSpan = "quarter";
                }else if(diffInDays>60 && diffInDays<=180){
                    timeSpan = "half_year";
                }else if(diffInDays>180){
                    timeSpan = "year";
                }
            }

            List<String> dates = getDates(timeSpan,fromDate,toDate);

            recordsMap.addCriteria("timeSpan",timeSpan);
            Map<String,Map<String,Integer>> datewiseMap=new LinkedHashMap<>();
            List<Map> records = repository.find(recordsMap);

            if(records.size()>0){

                for(Map r:records){
                    if(r.containsKey("report")) {
                        if (datewiseMap.containsKey(r.get("report").toString())) {
                            Map<String, Integer> typeCountMap = datewiseMap.get(r.get("report").toString());
                            if (typeCountMap.containsKey(r.get(timeSpan).toString())) {
                                int prevCount = typeCountMap.get(r.get(timeSpan).toString());
                                int newcount = prevCount + Integer.parseInt(r.get("count").toString());
                                typeCountMap.put(r.get(timeSpan).toString(), newcount);
                            } else {
                                typeCountMap.put(r.get(timeSpan).toString(), Integer.parseInt(r.get("count").toString()));
                            }
                            datewiseMap.put(r.get("report").toString(), typeCountMap);
                        } else {
                            Map<String, Integer> typeCountMap = new LinkedHashMap<>();
                            typeCountMap.put(r.get(timeSpan).toString(), Integer.parseInt(r.get("count").toString()));
                            datewiseMap.put(r.get("report").toString(), typeCountMap);
                        }
                    }
                }

//                for(String d:date){
//                    AnalyticsChartDTO chartDTO = new AnalyticsChartDTO();
//                    if(datewiseMap.containsKey(d)){
//                        chartDTO.setName(d);
//                        chartDTO.setType("bar");
//                        for(Map.Entry<Map<String,Map<String,Integer>>)
//                    }
//
//                }

            }
            List<StackChartInnerDTO> innerDTOS =new ArrayList<>();
            List<String> months = new ArrayList<>();

            if(dto.getFilter().equalsIgnoreCase("week")){
                List<Map<String,List<String>>> weeklyDatesList = getWeeklyDates(dto.getFromDate(),dto.getToDate());
                Map<String, List<Integer>> dtoMap = new HashMap();
                for(Map<String,List<String>> w:weeklyDatesList){
                   for(Map.Entry<String,List<String>> li: w.entrySet()){
                       for(String da: li.getValue()){
                           for(Map.Entry<String,Map<String,Integer>> m: datewiseMap.entrySet()){
                               for(Map.Entry<String,Integer> p:m.getValue().entrySet()){
                                   if(p.getKey().equalsIgnoreCase(da)){
                                       if(dtoMap.containsKey(m.getKey().concat("~").concat(li.getKey()))){
                                           List<Integer> countList = dtoMap.get(m.getKey().concat("~").concat(li.getKey()));
                                           countList.add(p.getValue());
                                           dtoMap.put(m.getKey().concat("~").concat(li.getKey()),countList);
                                       } else {
                                           List<Integer> countList = new ArrayList<>();
                                           countList.add(p.getValue());
                                           dtoMap.put(m.getKey().concat("~").concat(li.getKey()),countList);
                                       }

                                   }

                                   List<Integer> countList1 = dtoMap.get(m.getKey().concat("~").concat(li.getKey()));
                                   List<Integer> tempList = new ArrayList<>();
                                   int countVal = 0;
                                   if (countList1!=null && countList1.size()>0 ) {
                                       for (int i = 0; i < countList1.size(); i++) {
                                           countVal = countVal + countList1.get(i);
                                       }
                                       //countList.clear();
                                       tempList.add(countVal);
                                       dtoMap.put(m.getKey().concat("~").concat(li.getKey()), tempList);
                                   }
                               }

                           }
                           if(!months.contains(li.getKey())){
                               months.add(li.getKey());
                           }
                       }
                   }
                }
                months = new ArrayList<>();
                Map<String,List<Integer>> resultMap=new HashMap<>();

                for(Map.Entry<String,List<Integer>> m : dtoMap.entrySet()){

                    String[] keys = m.getKey().split("~");
                    String key = keys[0].toString();
                    if(!months.contains(keys[1])){
                        months.add(keys[1]);
                    }

                    if(resultMap.containsKey(key)){
                        List<Integer> list=resultMap.get(key);
                        list.add(m.getValue().get(0));
                    }else {
                        List<Integer> list=new ArrayList<>();
                        list.add(m.getValue().get(0));
                        resultMap.put(key,list);
                    }
                }
                StackChartDTO stackChartDTO = new StackChartDTO();
                stackChartDTO.setXaxis(months);
                for(Map.Entry<String,List<Integer>>m : resultMap.entrySet()){
                    StackChartInnerDTO innerDTO = new StackChartInnerDTO();
                    innerDTO.setName(m.getKey());
                    innerDTO.setData(m.getValue());
                    innerDTOS.add(innerDTO);

                }
                stackChartDTO.setSeries(innerDTOS);
            }else{
                for(Map.Entry<String,Map<String,Integer>> m: datewiseMap.entrySet()){
                    StackChartInnerDTO innerDTO = new StackChartInnerDTO();
                    innerDTO.setName(m.getKey());
                    for(Map.Entry<String,Integer> p:m.getValue().entrySet()){
                        if(innerDTO.getData()!=null){
                            List l = innerDTO.getData();
                            l.add(p.getValue());
                            innerDTO.setData(l);
                        } else {
                            List l = new ArrayList();
                            l.add(p.getValue());
                            innerDTO.setData(l);
                        }
                        months.add(p.getKey());
                    }
                    innerDTOS.add(innerDTO);
                }
            }


            StackChartDTO stackChartDTO = new StackChartDTO();
            stackChartDTO.setSeries(innerDTOS);
            stackChartDTO.setXaxis(months);

            return stackChartDTO;
        }catch (Exception e){
            throw new Exception(e);
        }

    }

    @POST
    @Path("/averagePatientRecordsDetails")
    @Produces("application/json")
    @Consumes("application/json")
    public Map averagePatientRecordsDetails(AnalyticsReportsDTO dto) throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);

        CriteriaMap recordsMap = new CriteriaMap(CommonQuery.AVERAGE_PATIENT_RECORDS_DETAILS_SELECT);
        if (dto.getDoctors() != null && dto.getDoctors().size()>0) {
            recordsMap.addCriteria("doctors", dto.getDoctors());
        }
        if (dto.getType() != null) {
            recordsMap.addCriteria("type", dto.getType());
        }
        if (dto.getTimeSpan() != null) {
            recordsMap.addCriteria("timeSpan", dto.getTimeSpan());
        }

        List<String> date = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        recordsMap.addCriteria("toDate", dto.getToDate());
        recordsMap.addCriteria("fromDate",dto.getFromDate());

        //Date fromDate = new SimpleDateFormat("yyyy-MM-dd").parse(dto.getFromDate());
        //Date toDate = new SimpleDateFormat("yyyy-MM-dd").parse(dto.getToDate());

        //List<String> dates = getDates(timeSpan,fromDate,toDate);
        Date fromDate = new SimpleDateFormat("yyyy-MM-dd").parse(dto.getFromDate());
        Date toDate = new SimpleDateFormat("yyyy-MM-dd").parse(dto.getToDate());

        long fromDateLong = fromDate.getTime();
        long toDateLong = toDate.getTime();

        long diffInMilis = toDateLong - fromDateLong;

        long diffInDays = diffInMilis / (24 * 60 * 60 * 1000);

        int weeks = (int) diffInDays%7;



        String timeSpan ="";
        //if(dto.getFilter().equalsIgnoreCase("week")){
          //  timeSpan = "week";
        //}else{
            if(diffInDays<=7){
                timeSpan ="week";
            }else if (diffInDays>7 && diffInDays<=60){
                timeSpan = "quarter";
            }else if(diffInDays>60 && diffInDays<=180){
                timeSpan = "half_year";
            }else if(diffInDays>180){
                timeSpan = "year";
            }
       // }

        List<String> dates = getDates(timeSpan,fromDate,toDate);

        Map<String, Map<String, Integer>> datewiseMap = new LinkedHashMap<>();
        Map<String, List<Map>> doctorMap = new HashMap<>();
        List<Map> records = repository.find(recordsMap);

        if (records.size() > 0) {

            for (Map r : records) {
                for (String d : dto.getDoctors()) {
                    Doctor doctor = (Doctor) repository.restore(ServiceLocator.getInstance().getReference(d));
                    if (d.equalsIgnoreCase(r.get("doctor").toString())) {
                        if (doctorMap.containsKey(doctor.getName())) {
                            List l = doctorMap.get(doctor.getName());
                            l.add(r);
                            doctorMap.put(doctor.getName(), l);
                        } else {
                            List l = new ArrayList();
                            l.add(r);
                            doctorMap.put(doctor.getName(), l);
                        }
                    }
                }
//                if(doctorMap.containsKey(r.get("report").toString())){
//                    Map<String,Integer> typeCountMap = datewiseMap.get(r.get("report").toString());
//                    if(typeCountMap.containsKey(r.get(dto.getDuration()).toString())){
//                        int prevCount = typeCountMap.get(r.get(dto.getDuration()).toString());
//                        int newcount = prevCount + Integer.parseInt(r.get("count").toString());
//                        typeCountMap.put(r.get(dto.getDuration()).toString(),newcount);
//                    }else{
//                        typeCountMap.put(r.get(dto.getDuration()).toString(),Integer.parseInt(r.get("count").toString()));
//                    }
//                    datewiseMap.put(r.get("report").toString(),typeCountMap);
//                }else{
//                    Map<String,Integer> typeCountMap = new LinkedHashMap<>();
//                    typeCountMap.put(r.get(dto.getDuration()).toString(),Integer.parseInt(r.get("count").toString()));
//                    datewiseMap.put(r.get("report").toString(),typeCountMap);
//                }
            }

        }
        return doctorMap;
    }

    @POST
    @Path("/questionnaireGraph/")
    @Produces("application/json")
    @Consumes("application/json")
    public Map questionnaireGraph(DoctorRiskDTO dto) throws Exception{
        Repository repository= (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        CriteriaMap map = new CriteriaMap(CommonQuery.QUESTIONNAIRE_FILLED_COUNT);
        map.addCriteria("displayType",dto.getType());
        if(dto.getDoctors()!=null  && dto.getDoctors().size()>0){
            map.addCriteria("doctors",dto.getDoctors());
        }

        List<Map> questionFilledMap = repository.find(map);

        int usercount = questionFilledMap.size();

        CriteriaMap patientMap = new CriteriaMap(CommonQuery.DIABETES_PATIENT_SELECT);
        List<Patient> patients = repository.find(patientMap);

        int patientcount = patients.size();

        Map countMap = new HashMap();
        countMap.put("filledcount",usercount);
        countMap.put("remaining",patientcount-usercount);

        return countMap;

    }

    @POST
    @Path("/questinnaireDetails")
    @Produces("application/json")
    @Consumes("application/json")
    public Map questionnaireDetails(DoctorRiskDTO doctorRiskDTO) throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        CriteriaMap map = new CriteriaMap(CommonQuery.QUESTIONNAIRE_DETAILS_SELECT);
        if(doctorRiskDTO.getDoctors()!=null && doctorRiskDTO.getDoctors().size()>0){
            map.addCriteria("doctors",doctorRiskDTO.getDoctors());
        }
        map.addCriteria("displayType",doctorRiskDTO.getType());

        List<Map> questions = repository.find(map);
        Map<String,Map<String,Integer>> doctorwiseMap = new HashMap<>();
        Map<String,Map<String,Integer>> doctorwiseAvgMap = new HashMap<>();
        if(questions.size()>0){
            for(Map q:questions){
                if(doctorwiseMap.containsKey(q.get("name"))){
                    Map<String,Integer> questionCountMap=doctorwiseAvgMap.get(q.get("name"));
                    Map<String,Integer> questionAnswerMap=doctorwiseMap.get(q.get("name"));
                    if(questionAnswerMap.containsKey(q.get("question"))){
                        int average = doctorwiseAvgMap.get(q.get("name")).get(q.get("question"));
                        if(average<Integer.parseInt(q.get("average").toString())){
                            questionCountMap.put(q.get("question").toString(),Integer.parseInt(q.get("average").toString()));
                            questionAnswerMap.put(q.get("question").toString(),Integer.parseInt(q.get("answer").toString()));
                            doctorwiseMap.put(q.get("name").toString(),questionAnswerMap);
                            doctorwiseAvgMap.put(q.get("name").toString(),questionCountMap);
                        }
                    } else {
                        questionCountMap.put(q.get("question").toString(),Integer.parseInt(q.get("average").toString()));
                        questionAnswerMap.put(q.get("question").toString(),Integer.parseInt(q.get("answer").toString()));
                        doctorwiseMap.put(q.get("name").toString(),questionAnswerMap);
                        doctorwiseAvgMap.put(q.get("name").toString(),questionCountMap);

                    }
                }else{
                    Map<String,Integer> questionCountMap = new HashMap<>();
                    Map<String,Integer> questionAnswerMap = new HashMap<>();
                    questionCountMap.put(q.get("question").toString(),Integer.parseInt(q.get("average").toString()));
                    doctorwiseAvgMap.put(q.get("name").toString(),questionCountMap);
                    questionAnswerMap.put(q.get("question").toString(),Integer.parseInt(q.get("answer").toString()));
                    doctorwiseMap.put(q.get("name").toString(),questionAnswerMap);
                }
            }
        }

        return doctorwiseMap;
    }


    private Map getAgeRange(Map<String,List<Integer>> countmap, int age,int count,String gender){
        if(age>=18 && age <=24){
            if(countmap.containsKey("18-24")){
                if(gender.equalsIgnoreCase("male")){
                    List prevCountList = countmap.get("18-24");
                    int prevCount = (int) prevCountList.get(0);
                    int newcount = prevCount + count;
                    prevCountList.set(0,newcount);
                    countmap.put("18-24",prevCountList);
                }else{
                    List prevCountList = countmap.get("18-24");
                    int prevCount = (int) prevCountList.get(1);
                    int newcount = prevCount + count;
                    prevCountList.set(1,newcount);
                    countmap.put("18-24",prevCountList);
                }
            }else{
                List prevCountList = new ArrayList();
                if(gender.equalsIgnoreCase("male")){
                    prevCountList.add(0,count);
                    prevCountList.add(1,0);
                    countmap.put("18-24",prevCountList);
                }else{
                    prevCountList.add(0,0);
                    prevCountList.add(1,count);
                    countmap.put("18-24",prevCountList);
                }
            }
        }else if(age>=25 && age<=34){
            if(countmap.containsKey("25-34")){
                if(gender.equalsIgnoreCase("male")){
                    List prevCountList = countmap.get("25-34");
                    int prevCount = (int) prevCountList.get(0);
                    int newcount = prevCount + count;
                    prevCountList.set(0,newcount);
                    countmap.put("25-34",prevCountList);
                }else{
                    List prevCountList = countmap.get("25-34");
                    int prevCount = (int) prevCountList.get(1);
                    int newcount = prevCount + count;
                    prevCountList.set(1,newcount);
                    countmap.put("25-34",prevCountList);
                }


            }else{
                List prevCountList = new ArrayList();
                if(gender.equalsIgnoreCase("male")){
                    prevCountList.add(0,count);
                    prevCountList.add(1,0);
                    countmap.put("25-34",prevCountList);
                }else{
                    prevCountList.add(0,0);
                    prevCountList.add(1,count);
                    countmap.put("25-34",prevCountList);
                }
            }
        }else if(age>=35 && age<=44){
            if(countmap.containsKey("35-44")){
                if(gender.equalsIgnoreCase("male")){
                    List prevCountList = countmap.get("35-44");
                    int prevCount = (int) prevCountList.get(0);
                    int newcount = prevCount + count;
                    prevCountList.set(0,newcount);
                    countmap.put("35-44",prevCountList);
                }else{
                    List prevCountList = countmap.get("35-44");
                    int prevCount = (int) prevCountList.get(1);
                    int newcount = prevCount + count;
                    prevCountList.set(1,newcount);
                    countmap.put("35-44",prevCountList);
                }
            }else{
                List prevCountList = new ArrayList();
                if(gender.equalsIgnoreCase("male")){
                    prevCountList.add(0,count);
                    prevCountList.add(1,0);
                    countmap.put("35-44",prevCountList);
                }else{
                    prevCountList.add(0,0);
                    prevCountList.add(1,count);
                    countmap.put("35-44",prevCountList);
                }
            }
        }else if(age>=45 && age<=54){
            if(countmap.containsKey("45-54")){
                if(gender.equalsIgnoreCase("male")){
                    List prevCountList = countmap.get("45-54");
                    int prevCount = (int) prevCountList.get(0);
                    int newcount = prevCount + count;
                    prevCountList.set(0,newcount);
                    countmap.put("45-54",prevCountList);
                }else{
                    List prevCountList = countmap.get("45-54");
                    int prevCount = (int) prevCountList.get(1);
                    int newcount = prevCount + count;
                    prevCountList.set(1,newcount);
                    countmap.put("45-54",prevCountList);
                }
            }else{
                List prevCountList = new ArrayList();
                if(gender.equalsIgnoreCase("male")){
                    prevCountList.add(0,count);
                    prevCountList.add(1,0);
                    countmap.put("45-54",prevCountList);
                }else{
                    prevCountList.add(0,0);
                    prevCountList.add(1,count);
                    countmap.put("45-54",prevCountList);
                }
            }
        }else if(age>=55 && age<=64){
            if(countmap.containsKey("55-64")){
                if(gender.equalsIgnoreCase("male")){
                    List prevCountList = countmap.get("55-64");
                    int prevCount = (int) prevCountList.get(0);
                    int newcount = prevCount + count;
                    prevCountList.set(0,newcount);
                    countmap.put("55-64",prevCountList);
                }else{
                    List prevCountList = countmap.get("55-64");
                    int prevCount = (int) prevCountList.get(1);
                    int newcount = prevCount + count;
                    prevCountList.set(1,newcount);
                    countmap.put("55-64",prevCountList);
                }
            }else{
                List prevCountList = new ArrayList();
                if(gender.equalsIgnoreCase("male")){
                    prevCountList.add(0,count);
                    prevCountList.add(1,0);
                    countmap.put("55-64",prevCountList);
                }else{
                    prevCountList.add(0,0);
                    prevCountList.add(1,count);
                    countmap.put("55-64",prevCountList);
                }
            }
        }else if(age>=65){
            if(countmap.containsKey("65+")){
                if(gender.equalsIgnoreCase("male")){
                    List prevCountList = countmap.get("65+");
                    int prevCount = (int) prevCountList.get(0);
                    int newcount = prevCount + count;
                    prevCountList.set(0,newcount);
                    countmap.put("65+",prevCountList);
                }else{
                    List prevCountList = countmap.get("65+");
                    int prevCount = (int) prevCountList.get(1);
                    int newcount = prevCount + count;
                    prevCountList.set(1,newcount);
                    countmap.put("65+",prevCountList);
                }
            }else{
                List prevCountList = new ArrayList();
                if(gender.equalsIgnoreCase("male")){
                    prevCountList.add(0,count);
                    prevCountList.add(1,0);
                    countmap.put("65+",prevCountList);
                }else{
                    prevCountList.add(0,0);
                    prevCountList.add(1,count);
                    countmap.put("65+",prevCountList);
                }
            }
        }
        return countmap;
    }

    @GET
    @Path("/averagePageViewSelect/{appName}/{fromDate}/{toDate}/{filter}")
    @Produces("application/json")
    @Consumes("application/json")
    public StackChartDTO averagePageViews(@PathParam("appName") String appName,@PathParam("fromDate") String fromDate,@PathParam("toDate") String toDate,@PathParam("filter") String filter) throws Exception{
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try{
            CriteriaMap map = new CriteriaMap(CommonQuery.AVERAGE_PAGE_VIEW_SELECT);
            map.addCriteria("appName",appName);
            map.addCriteria("fromDate",fromDate);
            map.addCriteria("toDate",toDate);
            map.addCriteria("filter",filter);

            List<Map> pageViewMap = repository.find(map);
            Map<String,Map<String,Integer>> datewiseMap=new LinkedHashMap<>();

            if(pageViewMap.size()>0){
                for(Map m:pageViewMap){
                    double seconds = Double.parseDouble(m.get("totalTime").toString());
                    double hours = seconds /3600;
                    double perc = (hours * 100)/24 ;
                    m.put("perc",perc);
                    m.put("hours",hours);

                    if(datewiseMap.containsKey(m.get("page_name"))){
                        Map<String,Integer> valuesMap = datewiseMap.get(m.get("page_name"));
                        valuesMap.put(m.get("created_on").toString(),Integer.parseInt(m.get("count").toString()));
                        datewiseMap.put(m.get("page_name").toString(),valuesMap);
                    }else{
                        Map<String,Integer> valuesMap = new HashMap<>();
                        valuesMap.put(m.get("created_on").toString(),Integer.parseInt(m.get("count").toString()));
                        datewiseMap.put(m.get("page_name").toString(),valuesMap);
                    }
                }
            }

            List<StackChartInnerDTO> innerDTOS =new ArrayList<>();
            List<String> months = new ArrayList<>();
            if(filter.equalsIgnoreCase("week")){
                List<Map<String,List<String>>> weeklyDatesList = getWeeklyDates(fromDate,toDate);
                Map<String, List<Integer>> dtoMap = new HashMap();
                for(Map<String,List<String>> w:weeklyDatesList){
                    for(Map.Entry<String,List<String>> li: w.entrySet()){
                        for(String da: li.getValue()){
                            for(Map.Entry<String,Map<String,Integer>> m: datewiseMap.entrySet()){
                                for(Map.Entry<String,Integer> p:m.getValue().entrySet()){
                                    if(p.getKey().equalsIgnoreCase(da)){
                                        if(dtoMap.containsKey(m.getKey().concat("~").concat(li.getKey()))){
                                            List<Integer> countList = dtoMap.get(m.getKey().concat("~").concat(li.getKey()));
                                            countList.add(p.getValue());
                                            dtoMap.put(m.getKey().concat("~").concat(li.getKey()),countList);
                                        } else {
                                            List<Integer> countList = new ArrayList<>();
                                            countList.add(p.getValue());
                                            dtoMap.put(m.getKey().concat("~").concat(li.getKey()),countList);
                                        }

                                    }

                                    List<Integer> countList1 = dtoMap.get(m.getKey().concat("~").concat(li.getKey()));
                                    List<Integer> tempList = new ArrayList<>();
                                    int countVal = 0;
                                    if (countList1!=null && countList1.size()>0 ) {
                                        for (int i = 0; i < countList1.size(); i++) {
                                            countVal = countVal + countList1.get(i);
                                        }
                                        //countList.clear();
                                        tempList.add(countVal);
                                        dtoMap.put(m.getKey().concat("~").concat(li.getKey()), tempList);
                                    }
                                }

                            }
                            if(!months.contains(li.getKey())){
                                months.add(li.getKey());
                            }
                        }
                    }
                }
                months = new ArrayList<>();
                Map<String,List<Integer>> resultMap=new HashMap<>();

                for(Map.Entry<String,List<Integer>> m : dtoMap.entrySet()){

                    String[] keys = m.getKey().split("~");
                    String key = keys[0].toString();
                    if(!months.contains(keys[1])){
                        months.add(keys[1]);
                    }

                    if(resultMap.containsKey(key)){
                        List<Integer> list=resultMap.get(key);
                        list.add(m.getValue().get(0));
                    }else {
                        List<Integer> list=new ArrayList<>();
                        list.add(m.getValue().get(0));
                        resultMap.put(key,list);
                    }
                }
                StackChartDTO stackChartDTO = new StackChartDTO();
                stackChartDTO.setXaxis(months);
                for(Map.Entry<String,List<Integer>>m : resultMap.entrySet()){
                    StackChartInnerDTO innerDTO = new StackChartInnerDTO();
                    innerDTO.setName(m.getKey());
                    innerDTO.setData(m.getValue());
                    innerDTOS.add(innerDTO);

                }
                stackChartDTO.setSeries(innerDTOS);
                return stackChartDTO;

            }else{
                for(Map.Entry<String,Map<String,Integer>> m: datewiseMap.entrySet()){
                    StackChartInnerDTO innerDTO = new StackChartInnerDTO();
                    innerDTO.setName(m.getKey());
                    for(Map.Entry<String,Integer> p:m.getValue().entrySet()){
                        if(innerDTO.getData()!=null){
                            List l = innerDTO.getData();
                            l.add(p.getValue());
                            innerDTO.setData(l);
                        } else {
                            List l = new ArrayList();
                            l.add(p.getValue());
                            innerDTO.setData(l);
                        }
                        if(!months.contains(p.getKey())){
                            months.add(p.getKey());
                        }

                    }
                    innerDTOS.add(innerDTO);
                }

                StackChartDTO stackChartDTO = new StackChartDTO();
                stackChartDTO.setSeries(innerDTOS);
                stackChartDTO.setXaxis(months);


                return stackChartDTO;
            }




        }catch (Exception e){
            throw new Exception(e);
        }

    }

    @GET
    @Path("/averagePageViewDetails/{fromDate}/{toDate}")
    @Produces("application/json")
    @Consumes("application/json")
    public Map<String,List<Map>> averagePageViewsSelect(@PathParam("fromDate") String fromDate,@PathParam("toDate") String toDate) throws Exception{
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try{
            CriteriaMap map = new CriteriaMap(CommonQuery.AVERAGE_PAGE_VIEW_DETAILS_SELECT);
            map.addCriteria("fromDate",fromDate);
            map.addCriteria("toDate",toDate);

            List<Map> pageViewMap = repository.find(map);

            Map<String,List<Map>> pageMap = new HashMap<>();
            for(Map p: pageViewMap){
                double seconds = Double.parseDouble(p.get("totalTime").toString());
                double hours = seconds /3600;
                double perc = (hours * 100)/24 ;
                p.put("perc",perc);
                p.put("hours",String.format("%.2f", hours));
                if(pageMap.containsKey(p.get("name"))){
                    List<Map> pageList = pageMap.get(p.get("name"));
                    pageList.add(p);
                    pageMap.put(p.get("name").toString(),pageList);
                }else{
                    List<Map> pageList = new ArrayList<>();
                    pageList.add(p);
                    pageMap.put(p.get("name").toString(),pageList);
                }
            }


            return pageMap;
        }catch (Exception e){
            throw new Exception(e);
        }

    }


    // TODO New Api added.. This is details page for second last graph fetch this api for that graph
    @GET
    @Path("/maxUsageViewDetails/{appName}/{fromDate}/{toDate}")
    @Produces("application/json")
    @Consumes("application/json")
    public Map<String,Map<String,Integer>> maxUsageViewsSelect(@PathParam("appName") String appName,@PathParam("fromDate") String fromDate,@PathParam("toDate") String toDate) throws Exception{
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try{
            CriteriaMap map = new CriteriaMap(CommonQuery.AVERAGE_PAGE_VIEW_SELECT);
            map.addCriteria("appName",appName);
            map.addCriteria("fromDate",fromDate);
            map.addCriteria("toDate",toDate);

            List<Map> pageViewMap = repository.find(map);

            Map<String,Map<String,Integer>> datewiseMap=new LinkedHashMap<>();
            if(pageViewMap.size()>0){
                for(Map m:pageViewMap){
                    double seconds = Double.parseDouble(m.get("totalTime").toString());
                    double hours = seconds /3600;
                    double perc = (hours * 100)/24 ;
                    m.put("perc",perc);
                    m.put("hours",hours);

                    if(datewiseMap.containsKey(m.get("page_name"))){
                        Map<String,Integer> valuesMap = datewiseMap.get(m.get("page_name"));
                        valuesMap.put(m.get("created_on").toString(),Integer.parseInt(m.get("count").toString()));
                        datewiseMap.put(m.get("page_name").toString(),valuesMap);
                    }else{
                        Map<String,Integer> valuesMap = new HashMap<>();
                        valuesMap.put(m.get("created_on").toString(),Integer.parseInt(m.get("count").toString()));
                        datewiseMap.put(m.get("page_name").toString(),valuesMap);
                    }
                }
            }


            return datewiseMap;
        }catch (Exception e){
            throw new Exception(e);
        }

    }

    @GET
    @Path("/totalActiveUsersSelect/{fromDate}/{toDate}/{filter}")
    @Produces("application/json")
    @Consumes("application/json")
    public StackChartDTO totalActiveUsers(@PathParam("fromDate") String fromDateString,@PathParam("toDate") String toDateString,@PathParam("filter") String filter) throws Exception{
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try {
            CriteriaMap map = new CriteriaMap(CommonQuery.TOTAL_ACTIVE_USERS_SELECT);
            List<String> date=new ArrayList<>();

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            //Date toDate = calendar.getTime();

            Date fromDate = new SimpleDateFormat("yyyy-MM-dd").parse(fromDateString);
            Date toDate = new SimpleDateFormat("yyyy-MM-dd").parse(toDateString);

            long fromDateLong = fromDate.getTime();
            long toDateLong = toDate.getTime();

            long diffInMilis = toDateLong - fromDateLong;

            long diffInDays = diffInMilis / (24 * 60 * 60 * 1000);

            String timeSpan ="";
            if(!filter.equalsIgnoreCase("week")){
                if(diffInDays<=7){
                    timeSpan ="week";
                }else if (diffInDays>7 && diffInDays<=60){
                    timeSpan = "quarter";
                }else if(diffInDays>60 && diffInDays<=180){
                    timeSpan = "half_year";
                }else if(diffInDays>180){
                    timeSpan = "year";
                }
            }else {
                timeSpan = "week";
            }


            List<String> dates = getDates(timeSpan,fromDate,toDate);


            map.addCriteria("toDate",toDateString);
            map.addCriteria("fromDate",fromDateString);
            map.addCriteria("timeSpan",timeSpan);

            List<Map> pageViewMap = repository.find(map);

            Map<String,Map<String,Integer>> datewiseMap=new LinkedHashMap<>();
            for(Map r:pageViewMap){
                if(datewiseMap.containsKey(r.get(timeSpan).toString())){
                    Map<String,Integer> typeCountMap = datewiseMap.get(r.get(timeSpan).toString());
                    if(typeCountMap.containsKey(r.get(timeSpan).toString())){
                        int prevCount = typeCountMap.get(r.get(timeSpan).toString());
                        int newcount = prevCount + Integer.parseInt(r.get("usercount").toString());
                        typeCountMap.put(r.get(timeSpan).toString(),newcount);
                    }else{
                        typeCountMap.put(r.get(timeSpan).toString(),Integer.parseInt(r.get("usercount").toString()));
                    }
                    datewiseMap.put(r.get(timeSpan).toString(),typeCountMap);
                }else{
                    Map<String,Integer> typeCountMap = new LinkedHashMap<>();
                    typeCountMap.put(r.get(timeSpan).toString(),Integer.parseInt(r.get("usercount").toString()));
                    datewiseMap.put(r.get(timeSpan).toString(),typeCountMap);
                }
            }

            List<StackChartInnerDTO> innerDTOS =new ArrayList<>();
            List<String> months = new ArrayList<>();

            if(filter.equalsIgnoreCase("week")){
                List<Map<String,List<String>>> weeklyDatesList = getWeeklyDates(fromDateString,toDateString);
                Map<String, List<Integer>> dtoMap = new HashMap();
                for(Map.Entry<String,Map<String,Integer>> m: datewiseMap.entrySet()){
                    for(Map<String,List<String>> w:weeklyDatesList){
                    for(Map.Entry<String,List<String>> li: w.entrySet()){
                        for(String da: li.getValue()){
                                for(Map.Entry<String,Integer> p:m.getValue().entrySet()){
                                    if(p.getKey().equalsIgnoreCase(da)){
                                        if(dtoMap.containsKey(m.getKey().concat("~").concat(li.getKey()))){
                                            List<Integer> countList = dtoMap.get(m.getKey().concat("~").concat(li.getKey()));
                                            countList.add(p.getValue());
                                            dtoMap.put(m.getKey().concat("~").concat(li.getKey()),countList);
                                        } else {
                                            List<Integer> countList = new ArrayList<>();
                                            countList.add(p.getValue());
                                            dtoMap.put(m.getKey().concat("~").concat(li.getKey()),countList);
                                        }

                                        List<Integer> countList1 = dtoMap.get(m.getKey().concat("~").concat(li.getKey()));
                                        List<Integer> tempList = new ArrayList<>();
                                        int countVal = 0;
                                        if (countList1!=null && countList1.size()>0 ) {
                                            for (int i = 0; i < countList1.size(); i++) {
                                                countVal = countVal + countList1.get(i);
                                            }
                                            //countList.clear();
                                            tempList.add(countVal);
                                            dtoMap.put(m.getKey().concat("~").concat(li.getKey()), tempList);
                                        }
                                    }

                                }

                            }
                            if(!months.contains(li.getKey())){
                                months.add(li.getKey());
                            }
                        }
                    }
                }
                months = new ArrayList<>();
                Map<String,List<Integer>> resultMap=new HashMap<>();

                for(Map.Entry<String,List<Integer>> m : dtoMap.entrySet()){

                    String[] keys = m.getKey().split("~");
                    String key = keys[0].toString();
                    if(!months.contains(keys[1])){
                        months.add(keys[1]);
                    }

                    if(resultMap.containsKey(keys[1])){
                        List<Integer> list=resultMap.get(keys[1]);
                        list.add(m.getValue().get(0));
                    }else {
                        List<Integer> list=new ArrayList<>();
                        list.add(m.getValue().get(0));
                        resultMap.put(keys[1],list);
                    }
                }
                StackChartDTO stackChartDTO = new StackChartDTO();
                stackChartDTO.setXaxis(months);
                for(Map.Entry<String,List<Integer>>m : resultMap.entrySet()){
                    StackChartInnerDTO innerDTO = new StackChartInnerDTO();
                    innerDTO.setName(m.getKey());
                    innerDTO.setData(m.getValue());
                    innerDTOS.add(innerDTO);

                }
                stackChartDTO.setSeries(innerDTOS);
                return stackChartDTO;
            }else{
                for(Map.Entry<String,Map<String,Integer>> m: datewiseMap.entrySet()){
                    StackChartInnerDTO innerDTO = new StackChartInnerDTO();
                    innerDTO.setName(m.getKey());
                    for(Map.Entry<String,Integer> p:m.getValue().entrySet()){
                        if(innerDTO.getData()!=null){
                            List l = innerDTO.getData();
                            l.add(p.getValue());
                            innerDTO.setData(l);
                        } else {
                            List l = new ArrayList();
                            l.add(p.getValue());
                            innerDTO.setData(l);
                        }
                        months.add(p.getKey());
                    }
                    innerDTOS.add(innerDTO);
                }

                StackChartDTO stackChartDTO = new StackChartDTO();
                stackChartDTO.setSeries(innerDTOS);
                stackChartDTO.setXaxis(months);

                return stackChartDTO;
            }

//            for(Map.Entry<String,Map<String,Integer>> m: datewiseMap.entrySet()){
//                StackChartInnerDTO innerDTO = new StackChartInnerDTO();
//                innerDTO.setName(m.getKey());
//                months = new ArrayList<>();
//                for(Map.Entry<String,Integer> p:m.getValue().entrySet()){
//                    if(innerDTO.getData()!=null){
//                        List l = innerDTO.getData();
//                        l.add(p.getValue());
//                        innerDTO.setData(l);
//                    } else {
//                        List l = new ArrayList();
//                        l.add(p.getValue());
//                        innerDTO.setData(l);
//                    }
//                    months.add(p.getKey());
//                }
//                innerDTOS.add(innerDTO);
//            }


        }catch (Exception e){
            throw new Exception(e);
        }
    }

    @GET
    @Path("/getPendingRegistrations")
    @Consumes("application/json")
    @Produces("application/json")
    public int pendingRegistrationCount() throws Exception{
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try{
            CriteriaMap map = new CriteriaMap(CommonQuery.PENDING_REGISTRATION_COUNT_SELECT);
            List<Map> pendingData = repository.find(map);

            int count = Integer.parseInt(pendingData.get(0).get("pendingReg").toString());

            return count;
        }catch (Exception e){
            throw new Exception(e);
        }

    }

    /*new api added*/
    @GET
    @Path("/activeUsersDetails/{fromDate}/{toDate}")
    @Produces("application/json")
    @Consumes("application/json")
    public List<Map> activeUsersDetails(@PathParam("fromDate") String fromDateString,@PathParam("toDate") String toDateString) throws Exception{
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);

        try {
            CriteriaMap map = new CriteriaMap(CommonQuery.TOTAL_ACTIVE_USERS_DETAILS_SELECT);

            map.addCriteria("toDate",toDateString);
            map.addCriteria("fromDate",fromDateString);

            List<Map> activeUsers = repository.find(map);

            return activeUsers;
        }catch (Exception e){
            throw new Exception(e);
        }

    }

    @GET
    @Path("/totalInvitesSent")
    @Consumes("application/json")
    @Produces("application/json")
    public int totalInvitesSent() throws Exception{
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try{
            CriteriaMap map = new CriteriaMap(CommonQuery.ENROLL_DATA_SELECT);
            List<Map> pendingData = repository.find(map);

            int count = pendingData.size();

            return count;
        }catch (Exception e){
            throw new Exception(e);
        }
    }

    @POST
    @Path("/doctorwiseRiskPatients")
    @Consumes("application/json")
    @Produces("application/json")
    public Map riskPatients(DoctorRiskDTO doctorRiskDTO) throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);

        CriteriaMap criteriaMap_doc = new CriteriaMap(CommonQuery.ANALYTICS_REPORTS_SELECT);
        if(doctorRiskDTO.getDoctors()!=null && doctorRiskDTO.getDoctors().size()>0){
            criteriaMap_doc.addCriteria("doctors", doctorRiskDTO.getDoctors());
        }
        String typeId = null;
        if (!doctorRiskDTO.getType().equalsIgnoreCase("obesity")) {
            if (doctorRiskDTO.getType().equalsIgnoreCase("sugar_profile_report_type")) {
                doctorRiskDTO.setType(null);
                typeId = "USRLVRT000000000000000000000000000001";
            }
            criteriaMap_doc.addCriteria("reportType", doctorRiskDTO.getType());
            criteriaMap_doc.addCriteria("typeId", typeId);
            List<Map> patients = repository.find(criteriaMap_doc);
            int h = 0;
            int m = 0;
            int l = 0;
            int totalCount = patients.size();
            System.out.println(totalCount);
            if (totalCount > 0) {
                for (Map object : patients) {
                    float a = Float.parseFloat((object.get("ReportValue").toString()));
                    float b = (float) 0.0;
                    if (Float.parseFloat(object.get("ThresholdMax").toString()) > 0) {
                        b = Float.parseFloat((object.get("ThresholdMax").toString()));
                    } else if (Float.parseFloat(object.get("ThresholdMin").toString()) > 0) {
                        b = Float.parseFloat((object.get("ThresholdMin").toString()));
                    }
                    if (a <= b) {
                        m++;
                    } else if (a > b) {
                        h++;
                    }
                }
                double highPerc =  ((h*100)/ totalCount);
                double lowPerc = ((l*100)/ totalCount);
                double medPerc = ((m*100)/ totalCount);

                if(highPerc!=0 && lowPerc !=0 && medPerc !=0){
                    lowPerc = 100-(highPerc+medPerc);

                }else if(highPerc!=0 && lowPerc != 0 && medPerc == 0){
                    highPerc = 100 - lowPerc;

                }else if(highPerc!=0 && lowPerc == 0 && medPerc != 0){
                    highPerc = 100 - medPerc;

                }else if(highPerc==0 && lowPerc != 0 && medPerc != 0){
                    lowPerc = 100 - medPerc;

                }else if(highPerc!=0 && lowPerc == 0 && medPerc == 0){
                    highPerc = 100;

                }else if(highPerc == 0 && lowPerc != 0 && medPerc == 0){
                    lowPerc = 100;

                }else if(highPerc==0 && lowPerc == 0 && medPerc != 0){
                    medPerc = 100;

                }else if(highPerc == 0 && lowPerc == 0 && medPerc == 0){
                    highPerc = 0;
                    lowPerc = 0;
                    medPerc = 0;
                }

                Map<String,Double>resultMap=new HashedMap();
                resultMap.put("h",highPerc);
                resultMap.put("m",lowPerc);
                resultMap.put("l",medPerc);

                return resultMap;
            }else{
                Map<String,Double>resultMap=new HashedMap();
                resultMap.put("h",0.0);
                resultMap.put("m",0.0);
                resultMap.put("l",0.0);

                return resultMap;
            }

        } else {
            criteriaMap_doc = new CriteriaMap(CommonQuery.ANALYTICS_OBESITY_SELECT);
            if(doctorRiskDTO.getDoctors()!=null && doctorRiskDTO.getDoctors().size()>0){
                criteriaMap_doc.addCriteria("doctors", doctorRiskDTO.getDoctors());
            }
            List<Map> patients = repository.find(criteriaMap_doc);
            int h = 0;
            int m = 0;
            int l = 0;
            int totalCount = patients.size();
            if (totalCount > 0) {
                for (Map object : patients) {
                    float a = Float.parseFloat((object.get("BMI").toString()));
                    if (a <= 18.5) {
                        l++;
                    } else if (a >= 18.5 && a <= 25) {
                        m++;
                    } else {
                        h++;
                    }
                }
                double highPerc =  ((h*100)/ totalCount);
                double lowPerc = ((l*100)/ totalCount);
                double medPerc = ((m*100)/ totalCount);

                if(highPerc!=0 && lowPerc !=0 && medPerc !=0){
                    lowPerc = 100-(highPerc+medPerc);

                }else if(highPerc!=0 && lowPerc != 0 && medPerc == 0){
                    highPerc = 100 - lowPerc;

                }else if(highPerc!=0 && lowPerc == 0 && medPerc != 0){
                    highPerc = 100 - medPerc;

                }else if(highPerc==0 && lowPerc != 0 && medPerc != 0){
                    lowPerc = 100 - medPerc;

                }else if(highPerc!=0 && lowPerc == 0 && medPerc == 0){
                    highPerc = 100;

                }else if(highPerc == 0 && lowPerc != 0 && medPerc == 0){
                    lowPerc = 100;

                }else if(highPerc==0 && lowPerc == 0 && medPerc != 0){
                    medPerc = 100;

                }else if(highPerc == 0 && lowPerc == 0 && medPerc == 0){
                    highPerc = 0;
                    lowPerc = 0;
                    medPerc = 0;
                }

                Map<String,Double>resultMap=new HashedMap();
                resultMap.put("h",highPerc);
                resultMap.put("m",lowPerc);
                resultMap.put("l",medPerc);

                return resultMap;
            }else{
                Map<String,Double>resultMap=new HashedMap();
                resultMap.put("h",0.0);
                resultMap.put("m",0.0);
                resultMap.put("l",0.0);

                return resultMap;
            }

        }

    }

    @POST
    @Path("/doctorwisePatientDetails")
    @Produces("application/json")
    @Consumes("application/json")
    public Map riskPatientDetails(DoctorRiskDTO doctorRiskDTO) throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        CriteriaMap criteriaMap_doc = new CriteriaMap(CommonQuery.ANALYTICS_REPORTS_SELECT);
        if(doctorRiskDTO.getDoctors()!=null && doctorRiskDTO.getDoctors().size()>0){
            criteriaMap_doc.addCriteria("doctors", doctorRiskDTO.getDoctors());
        }
        String typeId = null;
        if (!doctorRiskDTO.getType().equalsIgnoreCase("obesity")) {
            if (doctorRiskDTO.getType().equalsIgnoreCase("sugar_profile_report_type")) {
                doctorRiskDTO.setType(null);
                typeId = "USRLVRT000000000000000000000000000001";
            }
            criteriaMap_doc.addCriteria("reportType", doctorRiskDTO.getType());
            criteriaMap_doc.addCriteria("typeId", typeId);
            List<Patient> highRisk = new ArrayList<>();
            List<Patient> mildRisk = new ArrayList<>();
            List<Patient> lowRisk = new ArrayList<>();
            int h=0;
            int m=0;
            int l = 0;
            List<Map> patients=repository.find(criteriaMap_doc);
            Map<String,Map<String,List<Patient>>> detailsMap = new HashMap<>();
            for(Map object: patients) {
                float a=   Float.parseFloat((object.get("ReportValue").toString()));
                float b = (float) 0.0;
                if(Float.parseFloat(object.get("ThresholdMax").toString())>0){
                    b =   Float.parseFloat((object.get("ThresholdMax").toString()));
                }else if(Float.parseFloat(object.get("ThresholdMin").toString())>0){
                    b = Float.parseFloat((object.get("ThresholdMin").toString()));
                }
                if(a<=b) {
                    object.put("risklevel","Within Target");
                }else if(a>b){
                    object.put("risklevel","Beyond Target");
                }

            }

            for(Map p : patients){
                Patient patient= (Patient) repository.restore(ServiceLocator.getInstance().getReference(p.get("ID").toString()));
                for(String d: doctorRiskDTO.getDoctors()){
                    Doctor doctor = (Doctor) repository.restore(ServiceLocator.getInstance().getReference(d));
                    if(d.equalsIgnoreCase(p.get("doctor").toString())){
                        if(detailsMap.containsKey(doctor.getName())){
                            Map<String,List<Patient>> listMap = detailsMap.get(doctor.getName());
                            if(listMap.containsKey(p.get("risklevel"))){
                                List patientList = listMap.get(p.get("risklevel"));
                                patientList.add(patient);
                                listMap.put(p.get("risklevel").toString(),patientList);
                                detailsMap.put(doctor.getName(),listMap);
                            }else {
                                List patientList = new ArrayList();
                                patientList.add(patient);
                                listMap.put(p.get("risklevel").toString(),patientList);
                                detailsMap.put(doctor.getName(),listMap);
                            }
                            //patientMap.put(mildR)
                        }else {
                            Map<String,List<Patient>> listMap = new HashMap<>();
                            List patientList = new ArrayList();
                            patientList.add(patient);
                            listMap.put(p.get("risklevel").toString(),patientList);
                            detailsMap.put(doctor.getName(),listMap);
                        }
                    }
                }

            }

            return detailsMap;

        }else {
            criteriaMap_doc = new CriteriaMap(CommonQuery.ANALYTICS_OBESITY_SELECT);
            if(doctorRiskDTO.getDoctors()!=null && doctorRiskDTO.getDoctors().size()>0){
                criteriaMap_doc.addCriteria("doctors", doctorRiskDTO.getDoctors());
            }

            List<Map> patients=repository.find(criteriaMap_doc);
            List<Patient> normal = new ArrayList<>();
            List<Patient> overWeight = new ArrayList<>();
            List<Patient> underWeight = new ArrayList<>();
            int h=0;
            int m=0;
            int l = 0;


            Map<String,Map<String,List<Patient>>> detailsMap = new HashMap<>();
            for(Map object: patients) {
                float a=   Float.parseFloat((object.get("BMI").toString()));
                float b = (float) 0.0;
                if(a<=18.5) {
                    object.put("risklevel","Underweight");
                }else if( a>= 18.5 && a <=25){
                    object.put("risklevel","Normal");
                } else if(a>=25){
                    object.put("risklevel","Overweight");
                }

            }

            for(Map p : patients){
                Patient patient= (Patient) repository.restore(ServiceLocator.getInstance().getReference(p.get("ID").toString()));
                DocumentDTO documentDTO = ImageService.fetchDocument(patient.getId(),"SYSLVIMGTP000000000000000000000000001");
                if(documentDTO.getByteContent()!=null){
                    patient.setByteContent(documentDTO.getByteContent());
                }
                for(String d: doctorRiskDTO.getDoctors()){
                    Doctor doctor = (Doctor) repository.restore(ServiceLocator.getInstance().getReference(d));
                    if(d.equalsIgnoreCase(p.get("doctor").toString())){
                        if(detailsMap.containsKey(doctor.getName())){
                            Map<String,List<Patient>> listMap = detailsMap.get(doctor.getName());
                            if(listMap.containsKey(p.get("risklevel"))){
                                List patientList = listMap.get(p.get("risklevel"));
                                patientList.add(patient);
                                listMap.put(p.get("risklevel").toString(),patientList);
                                detailsMap.put(doctor.getName(),listMap);
                            }else {
                                List patientList = new ArrayList();
                                patientList.add(patient);
                                listMap.put(p.get("risklevel").toString(),patientList);
                                detailsMap.put(doctor.getName(),listMap);
                            }
                            //patientMap.put(mildR)
                        }else {
                            Map<String,List<Patient>> listMap = new HashMap<>();
                            List patientList = new ArrayList();
                            patientList.add(patient);
                            listMap.put(p.get("risklevel").toString(),patientList);
                            detailsMap.put(doctor.getName(),listMap);
                        }
                    }
                }

            }

            return detailsMap;

        }
    }

    //TODO - New Api Added-Remaining data formatting
    @POST
    @Path("/getPercentPatients/")
    @Consumes("application/json")
    @Produces("application/json")
    public StackChartDTO getPercentPatients(AnalyticsReportsDTO dto) throws Exception {

        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);

        try {
            CriteriaMap recordsMap = new CriteriaMap(CommonQuery.PERCENT_PATIENT_RECORDS_SELECT);
            if (dto.getDoctors() != null && dto.getDoctors().size() > 0) {
                recordsMap.addCriteria("doctors", dto.getDoctors());
            }
            if (dto.getType() != null) {
                recordsMap.addCriteria("type", dto.getType());
            }
            if (dto.getReportId() != null) {
                recordsMap.addCriteria("reportId", dto.getReportId());
            }

            List<String> date = new ArrayList<>();

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            //Date toDate = calendar.getTime();
            recordsMap.addCriteria("toDate", dto.getToDate());
            recordsMap.addCriteria("fromDate", dto.getFromDate());

            Date fromDate = new SimpleDateFormat("yyyy-MM-dd").parse(dto.getFromDate());
            Date toDate = new SimpleDateFormat("yyyy-MM-dd").parse(dto.getToDate());

            long fromDateLong = fromDate.getTime();
            long toDateLong = toDate.getTime();

            long diffInMilis = toDateLong - fromDateLong;

            long diffInDays = diffInMilis / (24 * 60 * 60 * 1000);

            String timeSpan = "";
           // if(!dto.getFilter().equalsIgnoreCase("week")){
            if (diffInDays <= 7) {
                timeSpan = "week";
            } else if (diffInDays > 7 && diffInDays <= 60) {
                timeSpan = "quarter";
            } else if (diffInDays > 60 && diffInDays <= 180) {
                timeSpan = "half_year";
            } else if (diffInDays > 180) {
                timeSpan = "year";
            }
//            }else{
//                timeSpan = "week";
//            }


            List<String> dates = getDates(timeSpan, fromDate, toDate);

            recordsMap.addCriteria("timeSpan", timeSpan);
            Map<String, Integer> datewiseMap = new LinkedHashMap<>();
            List<String> months = new ArrayList<>();
            List<StackChartInnerDTO> innerDTOS = new ArrayList<>();
            List<Map> records = repository.find(recordsMap);

            RangeDTO rangeDTO = new RangeDTO();
            if (records.size() > 0) {
                for (Map r : records) {
                    rangeDTO = getRange(r.get("REPORT_TYPE_ID").toString());
                    for (Map i : rangeDTO.getRangeMap()) {
                        if (datewiseMap.containsKey(r.get(timeSpan).toString().concat("~").concat(i.get("lower") + "").concat("-").concat(i.get("upper") + ""))) {
                            int prevCount = datewiseMap.get(r.get(timeSpan).toString().concat("~").concat(i.get("lower") + "").concat("-").concat(i.get("upper") + ""));
                            if (Double.parseDouble(r.get("value").toString()) >= Double.parseDouble(i.get("lower").toString()) && Double.parseDouble(r.get("value").toString()) <= Double.parseDouble(i.get("upper").toString())) {
                                datewiseMap.put(r.get(timeSpan).toString().concat("~").concat(i.get("lower") + "").concat("-").concat(i.get("upper") + ""), prevCount + 1);
                            } else {
                                datewiseMap.put(r.get(timeSpan).toString().concat("~").concat(i.get("lower") + "").concat("-").concat(i.get("upper") + ""), prevCount);
                            }
                        } else {
                            if (Double.parseDouble(r.get("value").toString()) >= Double.parseDouble(i.get("lower").toString()) && Double.parseDouble(r.get("value").toString()) <= Double.parseDouble(i.get("upper").toString())) {
                                datewiseMap.put(r.get(timeSpan).toString().concat("~").concat(i.get("lower") + "").concat("-").concat(i.get("upper") + ""), 1);
                            } else {
                                datewiseMap.put(r.get(timeSpan).toString().concat("~").concat(i.get("lower") + "").concat("-").concat(i.get("upper") + ""), 0);
                            }
                        }
                    }


                }

                /*if (dto.getFilter().equalsIgnoreCase("week")) {
                    List<Map<String, List<String>>> weeklyDatesList = getWeeklyDates(dto.getFromDate(), dto.getToDate());
                    Map<String, List<Integer>> dtoMap = new HashMap();
                    for (Map.Entry<String, Integer> m : datewiseMap.entrySet()) {
                        for (Map<String, List<String>> w : weeklyDatesList) {
                            for (Map.Entry<String, List<String>> li : w.entrySet()) {
                                for (String da : li.getValue()) {
                                    List l = new ArrayList();
                                    for (String r : rangeDTO.getRangeList()) {
                                        String[] ranges = m.getKey().split("~");
                                        String key = ranges[1].toString();
                                        String xKey = ranges[0].toString();
                                        if (key.contains("-9999999")) {
                                            key = key.replace("-9999999", " and above");
                                        }
                                        if (key.equalsIgnoreCase(r)) {
                                            if (dtoMap.containsKey(key)) {
                                                List<Integer> countList = dtoMap.get(key);
                                                countList.add(m.getValue());
                                                dtoMap.put(key, countList);
                                            } else {
                                                List<Integer> countList = new ArrayList<>();
                                                countList.add(m.getValue());
                                                dtoMap.put(key, countList);
                                            }
                                        }
                                        if (!months.contains(li.getKey())) {
                                            months.add(li.getKey());
                                        }
                                    }
                                }

                                for (Map.Entry<String, List<Integer>> ma : dtoMap.entrySet()) {
                                    StackChartInnerDTO innerDTO = new StackChartInnerDTO();
                                    innerDTO.setName(ma.getKey());
                                    innerDTO.setData(ma.getValue());
                                    innerDTOS.add(innerDTO);
                                }
                                StackChartDTO stackChartDTO = new StackChartDTO();
                                stackChartDTO.setSeries(innerDTOS);
                                stackChartDTO.setXaxis(months);

                                return stackChartDTO;
                            }
                        }
                    }
                } else {*/
                    Map<String, List<Integer>> dtoMap = new HashMap();
                    for (Map.Entry<String, Integer> m : datewiseMap.entrySet()) {
                        List l = new ArrayList();
                        for (String r : rangeDTO.getRangeList()) {
                            String[] ranges = m.getKey().split("~");
                            String key = ranges[1].toString();
                            if (key.contains("-9999999")) {
                                key = key.replace("-9999999", " and above");
                            }
                            if (key.equalsIgnoreCase(r)) {
                            /*if (innerDTO.getData() != null) {
                                l = innerDTO.getData();
                                l.add(m.getValue());
                                innerDTO.setData(l);
                            } else {
                                l.add(m.getValue());
                                innerDTO.setData(l);
                            }*/
                                if (dtoMap.containsKey(key)) {
                                    List<Integer> countList = dtoMap.get(key);
                                    countList.add(m.getValue());
                                    dtoMap.put(key, countList);
                                } else {
                                    List<Integer> countList = new ArrayList<>();
                                    countList.add(m.getValue());
                                    dtoMap.put(key, countList);
                                }
                            }
                            if (!months.contains(ranges[0])) {
                                months.add(ranges[0]);
                            }
                        /*
                        innerDTOS.add(dtoMap);
                        if(!innerDTOS.contains(innerDTO)) {
                            innerDTOS.add(innerDTO);
                        }
                        */
                        }
                    }

                    for (Map.Entry<String, List<Integer>> m : dtoMap.entrySet()) {
                        StackChartInnerDTO innerDTO = new StackChartInnerDTO();
                        innerDTO.setName(m.getKey());
                        innerDTO.setData(m.getValue());
                        innerDTOS.add(innerDTO);
                    }

                }
                StackChartDTO stackChartDTO = new StackChartDTO();
                stackChartDTO.setSeries(innerDTOS);
                stackChartDTO.setXaxis(months);

                return stackChartDTO;
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    private RangeDTO getRange(String reportType){
        //yahape ek map bana tko jo bhi key chaiye uska and har case me woh use kar
        List<String> rangeList = new ArrayList<>();
        List<Map> rangeMapList = new ArrayList<>();
        switch (reportType){
            case "USRLVRT000000000000000000000000000001":
                rangeList.add("2-4");
                rangeList.add("4-6");
                rangeList.add("6-8");
                rangeList.add("8-10");
                rangeList.add("10 and above");

                Map rangeMap = new HashMap();
                rangeMap.put("lower",2);
                rangeMap.put("upper",4);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",4);
                rangeMap.put("upper",6);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",6);
                rangeMap.put("upper",8);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",8);
                rangeMap.put("upper",10);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",10);
                rangeMap.put("upper",9999999);
                rangeMapList.add(rangeMap);
                break;
            case "USRLVRT000000000000000000000000000012":
                rangeList.add("0.1-0.4");
                rangeList.add("0.4-0.7");
                rangeList.add("0.7-1.0");
                rangeList.add("1.0 and above");

                rangeMap = new HashMap();
                rangeMap.put("lower",0.1);
                rangeMap.put("upper",0.4);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",0.4);
                rangeMap.put("upper",0.7);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",0.7);
                rangeMap.put("upper",1.0);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",1.0);
                rangeMap.put("upper",9999999);
                rangeMapList.add(rangeMap);
                break;
            case "USRLVRT000000000000000000000000000010":
                rangeList.add("20-40");
                rangeList.add("40-60");
                rangeList.add("60-80");
                rangeList.add("80-100");
                rangeList.add("100 and above");

                rangeMap = new HashMap();
                rangeMap.put("lower",20);
                rangeMap.put("upper",40);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",40);
                rangeMap.put("upper",60);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",60);
                rangeMap.put("upper",80);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",80);
                rangeMap.put("upper",100);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",100);
                rangeMap.put("upper",9999999);
                rangeMapList.add(rangeMap);
                break;
            case "USRLVRT000000000000000000000000000018":
                rangeList.add("20-40");
                rangeList.add("40-60");
                rangeList.add("60-80");
                rangeList.add("80-100");
                rangeList.add("100 and above");

                rangeMap = new HashMap();
                rangeMap.put("lower",20);
                rangeMap.put("upper",40);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",40);
                rangeMap.put("upper",60);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",60);
                rangeMap.put("upper",80);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",80);
                rangeMap.put("upper",100);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",100);
                rangeMap.put("upper",9999999);
                rangeMapList.add(rangeMap);
                break;

            case "USRLVRT000000000000000000000000000005":
                rangeList.add("20-40");
                rangeList.add("40-60");
                rangeList.add("60-80");
                rangeList.add("80-100");
                rangeList.add("100-120");
                rangeList.add("120-140");
                rangeList.add("140 and above");

                rangeMap = new HashMap();
                rangeMap.put("lower",20);
                rangeMap.put("upper",40);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",40);
                rangeMap.put("upper",60);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",60);
                rangeMap.put("upper",80);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",80);
                rangeMap.put("upper",100);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",100);
                rangeMap.put("upper",120);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",120);
                rangeMap.put("upper",140);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",140);
                rangeMap.put("upper",9999999);
                rangeMapList.add(rangeMap);
                break;

            case "USRLVRT000000000000000000000000000002":
                rangeList.add("20-40");
                rangeList.add("40-60");
                rangeList.add("60-80");
                rangeList.add("80-100");
                rangeList.add("100-120");
                rangeList.add("120-140");
                rangeList.add("140 and above");

                rangeMap = new HashMap();
                rangeMap.put("lower",20);
                rangeMap.put("upper",40);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",40);
                rangeMap.put("upper",60);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",60);
                rangeMap.put("upper",80);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",80);
                rangeMap.put("upper",100);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",100);
                rangeMap.put("upper",120);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",120);
                rangeMap.put("upper",140);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",140);
                rangeMap.put("upper",9999999);
                rangeMapList.add(rangeMap);
                break;
            default:
                rangeList.add("50-100");
                rangeList.add("100-150");
                rangeList.add("150-200");
                rangeList.add("200-220");
                rangeList.add("220 and above");

                rangeMap = new HashMap();
                rangeMap.put("lower",50);
                rangeMap.put("upper",100);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",100);
                rangeMap.put("upper",150);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",150);
                rangeMap.put("upper",200);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",200);
                rangeMap.put("upper",220);
                rangeMapList.add(rangeMap);

                rangeMap = new HashMap();
                rangeMap.put("lower",220);
                rangeMap.put("upper",9999999);
                rangeMapList.add(rangeMap);
                break;
        }

        RangeDTO dto = new RangeDTO();
        dto.setRangeList(rangeList);
        dto.setRangeMap(rangeMapList);

        return dto;


    }





    public long getDays(long currentTime, long endDateTime) {

        Calendar endDateCalendar;
        Calendar currentDayCalendar;


        //expiration day
        endDateCalendar = Calendar.getInstance(TimeZone.getTimeZone("EST"));
        endDateCalendar.setTimeInMillis(endDateTime);
        endDateCalendar.set(Calendar.MILLISECOND, 0);
        endDateCalendar.set(Calendar.MINUTE, 0);
        endDateCalendar.set(Calendar.HOUR, 0);
        endDateCalendar.set(Calendar.HOUR_OF_DAY, 0);

        //current day
        currentDayCalendar = Calendar.getInstance(TimeZone.getTimeZone("EST"));
        currentDayCalendar.setTimeInMillis(currentTime);
        currentDayCalendar.set(Calendar.MILLISECOND, 0);
        currentDayCalendar.set(Calendar.MINUTE, 0);
        currentDayCalendar.set(Calendar.HOUR,0);
        currentDayCalendar.set(Calendar.HOUR_OF_DAY, 0);


        long remainingDays = (long)Math.ceil((float) (endDateCalendar.getTimeInMillis() - currentDayCalendar.getTimeInMillis()) / (24 * 60 * 60 * 1000));

        return remainingDays;
    }

    private List<String> getDates(String timeSpan,Date fromDate,Date toDate){

        List<String> dates = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        switch (timeSpan){
            case "week":
                calendar.add(Calendar.DAY_OF_MONTH,-6);
                fromDate = calendar.getTime();
                while (fromDate.before(toDate) || fromDate.equals(toDate)){
                    SimpleDateFormat format=new SimpleDateFormat("YYYY-MM-dd");
                    String dt1=format.format(fromDate.getTime());
                    dates.add(dt1);
                    Calendar calendar1=Calendar.getInstance();
                    calendar1.setTime(fromDate);
                    calendar1.add(Calendar.DATE,1);
                    fromDate=calendar1.getTime();
                }

                break;
            case "quarter":
                fromDate = new Date();
                calendar.add(Calendar.MONTH, -2);
                calendar.set(Calendar.DAY_OF_MONTH,1);
                while (fromDate.before(toDate) || fromDate.equals(toDate)){
//                        Calendar c = Calendar.getInstance();
//                        c.add(c.MONTH,-3);
                    dates.add(String.valueOf(fromDate.getMonth()+1));
                    Calendar calendar1=Calendar.getInstance();
                    calendar1.setTime(fromDate);
                    calendar1.add(Calendar.MONTH,1);
                    fromDate=calendar1.getTime();
                }

                break;
            case "half_year":
                fromDate = new Date();
                calendar.add(Calendar.MONTH, -5);
                calendar.set(Calendar.DAY_OF_MONTH,1);
                while (fromDate.before(toDate) || fromDate.equals(toDate)){
                    dates.add(String.valueOf(fromDate.getMonth()+1));
                    Calendar calendar1=Calendar.getInstance();
                    calendar1.setTime(fromDate);
                    calendar1.add(Calendar.MONTH,1);
                    fromDate=calendar1.getTime();
                }

                break;
            case "year":
                fromDate = new Date();
                calendar.add(Calendar.YEAR, -1);
                while (fromDate.before(toDate) ){
                    dates.add(String.valueOf(fromDate.getYear()+1900)+"-"+String.valueOf(fromDate.getYear()+1900+1));
                    Calendar calendar1=Calendar.getInstance();
                    calendar1.setTime(fromDate);
                    calendar1.add(Calendar.YEAR,1);
                    fromDate=calendar1.getTime();
                }
                break;
        }

        return dates;

    }


    @GET
    @Path("/weeklyDates/{fromDate}/{toDate}")
    @Produces("application/json")
    @Consumes("application/json")
    public List<Map> getWeeklyDatesT(@PathParam("fromDate") String fromDateString,@PathParam("toDate") String toDateString) throws Exception{
        try {

            Date fromDate = new SimpleDateFormat("yyyy-MM-dd").parse(fromDateString);
            Date toDate = new SimpleDateFormat("yyyy-MM-dd").parse(toDateString);

            Calendar date1 = Calendar.getInstance();
            Calendar date2 = Calendar.getInstance();

            date1.clear();
            date1.setTime(fromDate);
            date2.clear();
            date2.setTime(toDate);

            long diff = date2.getTimeInMillis() - date1.getTimeInMillis();

            float dayCount = (float) diff / (24 * 60 * 60 * 1000);

            int week = (int) (dayCount / 7);
            int remainingDays = (int) dayCount % 7;
            if(remainingDays >0){
                week = week + 1;
            }
            System.out.println(week);
            List<Map> weekMapList = new ArrayList<>();

            for(int i=0; i<=week-1;i++){
                Map weekMap = new HashMap();
                List<String> dates = new ArrayList<>();
                Calendar tempDate = Calendar.getInstance();
                tempDate.setTime(fromDate);
                tempDate.add(Calendar.DATE,6);

                while (fromDate.before(tempDate.getTime()) || fromDate.equals(tempDate.getTime())){
                    SimpleDateFormat format=new SimpleDateFormat("YYYY-MM-dd");
                    String dt1=format.format(fromDate.getTime());
                    dates.add(dt1);
                    Calendar calendar1=Calendar.getInstance();
                    calendar1.setTime(fromDate);
                    calendar1.add(Calendar.DATE,1);
                    fromDate=calendar1.getTime();
                }
                int no = i+1;
                weekMap.put("Week "+no,dates);
                if(!tempDate.getTime().equals(toDate.getTime())){
                    tempDate.add(Calendar.DATE,1);
                    fromDate = tempDate.getTime();
                    weekMapList.add(weekMap);
                }else{
                    break;
                }
            }

            return weekMapList;
        }catch (Exception e){
            throw new Exception(e);
        }
    }

    public List<Map<String,List<String>>> getWeeklyDates(String fromDateString,String toDateString) throws Exception{
        try {

            Date fromDate = new SimpleDateFormat("yyyy-MM-dd").parse(fromDateString);
            Date toDate = new SimpleDateFormat("yyyy-MM-dd").parse(toDateString);

            Calendar date1 = Calendar.getInstance();
            Calendar date2 = Calendar.getInstance();

            date1.clear();
            date1.setTime(fromDate);
            date2.clear();
            date2.setTime(toDate);

            long diff = date2.getTimeInMillis() - date1.getTimeInMillis();

            float dayCount = (float) diff / (24 * 60 * 60 * 1000);

            int week = (int) (dayCount / 7);
            int remainingDays = (int) dayCount % 7;
            if(remainingDays >0){
                week = week + 1;
            }
            System.out.println(week);
            List<Map<String,List<String>>> weekMapList = new ArrayList<>();

            for(int i=0; i<=week-1;i++){
                Map<String,List<String>> weekMap = new HashMap();
                List<String> dates = new ArrayList<>();
                Calendar tempDate = Calendar.getInstance();
                tempDate.setTime(fromDate);
                tempDate.add(Calendar.DATE,6);

                while (fromDate.before(tempDate.getTime()) || fromDate.equals(tempDate.getTime())){
                    SimpleDateFormat format=new SimpleDateFormat("YYYY-MM-dd");
                    String dt1=format.format(fromDate.getTime());
                    dates.add(dt1);
                    Calendar calendar1=Calendar.getInstance();
                    calendar1.setTime(fromDate);
                    calendar1.add(Calendar.DATE,1);
                    fromDate=calendar1.getTime();
                }
                int no = i+1;
                weekMap.put("Week "+no,dates);
                if(!tempDate.getTime().equals(toDate.getTime())){
                    tempDate.add(Calendar.DATE,1);
                    fromDate = tempDate.getTime();
                    weekMapList.add(weekMap);
                }else{
                    break;
                }
            }

            return weekMapList;
        }catch (Exception e){
            throw new Exception(e);
        }
    }


    public void getNoOfWeek()
    {

        Calendar date1 = Calendar.getInstance();
        Calendar date2 = Calendar.getInstance();

        date1.clear();
        date1.set(2015, 12, 29); // set date 1 (yyyy,mm,dd)
        date2.clear();
        date2.set(2016, 02, 7); //set date 2 (yyyy,mm,dd)

        long diff = date2.getTimeInMillis() - date1.getTimeInMillis();

        float dayCount = (float) diff / (24 * 60 * 60 * 1000);

        int week = (int) (dayCount / 7);
         System.out.println(week);
    }


}
