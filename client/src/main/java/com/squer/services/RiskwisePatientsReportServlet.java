package com.squer.services;

import com.squer.platform.ServiceLocator;
import com.squer.platform.api.security.authentication.AuthenticationHandler;
import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.utility.CriteriaMap;
import com.squer.platform.registry.PlatformDelegates;
import com.squer.registry.CommonQuery;
import com.squer.sfe.api.common.entity.Doctor;
import com.squer.sfe.api.common.entity.Patient;
import org.apache.commons.collections.map.HashedMap;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * Created by mithila on 22/06/18-Jun-2018 at 10:59 AM
 */
public class RiskwisePatientsReportServlet extends HttpServlet{

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String authCertificate = request.getParameter("certificate");

        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try {
            AuthenticationHandler.authenticate(authCertificate);

            String type = request.getParameter("type");
            CriteriaMap criteriaMap_doc = new CriteriaMap(CommonQuery.ANALYTICS_REPORTS_SELECT);
            String typeId = null;
            Map<String,Map<String,List<Patient>>> detailsMap = new HashMap<>();

            if (!type.equalsIgnoreCase("obesity")) {
                if (type.equalsIgnoreCase("sugar_profile_report_type")) {
                    type = null;
                    typeId = "USRLVRT000000000000000000000000000001";
                }
                criteriaMap_doc.addCriteria("reportType", type);
                criteriaMap_doc.addCriteria("typeId", typeId);
                List<Patient> highRisk = new ArrayList<>();
                List<Patient> mildRisk = new ArrayList<>();
                List<Patient> lowRisk = new ArrayList<>();
                int h=0;
                int m=0;
                int l = 0;
                List<Map> patients=repository.find(criteriaMap_doc);
                 detailsMap = new HashMap<>();
                for(Map object: patients) {
                    float a=   Float.parseFloat((object.get("ReportValue").toString()));
                    float b = (float) 0.0;
                    if(Float.parseFloat(object.get("ThresholdMax").toString())>0){
                        b =   Float.parseFloat((object.get("ThresholdMax").toString()));
                    }else if(Float.parseFloat(object.get("ThresholdMin").toString())>0){
                        b = Float.parseFloat((object.get("ThresholdMin").toString()));
                    }
                    if(a<=b) {
                        object.put("risklevel","Within Target");
                    }else if(a>b){
                        object.put("risklevel","Beyond Target");
                    }

                }

                for(Map p : patients){
                    Patient patient= (Patient) repository.restore(ServiceLocator.getInstance().getReference(p.get("ID").toString()));
                        Doctor doctor = (Doctor) repository.restore(ServiceLocator.getInstance().getReference(p.get("doctor").toString()));
                        if(detailsMap.containsKey(doctor.getName())){
                            Map<String,List<Patient>> listMap = detailsMap.get(doctor.getName());
                            if(listMap.containsKey(p.get("risklevel"))){
                                List patientList = listMap.get(p.get("risklevel"));
                                patientList.add(patient);
                                listMap.put(p.get("risklevel").toString(),patientList);
                                detailsMap.put(doctor.getName(),listMap);
                            }else {
                                List patientList = new ArrayList();
                                patientList.add(patient);
                                listMap.put(p.get("risklevel").toString(),patientList);
                                detailsMap.put(doctor.getName(),listMap);
                            }
                            //patientMap.put(mildR)
                        }else {
                            Map<String,List<Patient>> listMap = new HashMap<>();
                            List patientList = new ArrayList();
                            patientList.add(patient);
                            listMap.put(p.get("risklevel").toString(),patientList);
                            detailsMap.put(doctor.getName(),listMap);
                        }
                    }

            }else {
                criteriaMap_doc = new CriteriaMap(CommonQuery.ANALYTICS_OBESITY_SELECT);
                List<Map> patients=repository.find(criteriaMap_doc);
                List<Patient> normal = new ArrayList<>();
                List<Patient> overWeight = new ArrayList<>();
                List<Patient> underWeight = new ArrayList<>();
                int h=0;
                int m=0;
                int l = 0;


                for(Map object: patients) {
                    float a=   Float.parseFloat((object.get("BMI").toString()));
                    float b = (float) 0.0;
                    if(a<=18.5) {
                        object.put("risklevel","Underweight");
                    }else if( a>= 18.5 && a <=25){
                        object.put("risklevel","Normal");
                    } else if(a>=25){
                        object.put("risklevel","Overweight");
                    }

                }
                detailsMap = new HashMap<>();
                for(Map p : patients){
                    Patient patient= (Patient) repository.restore(ServiceLocator.getInstance().getReference(p.get("ID").toString()));
                        Doctor doctor = (Doctor) repository.restore(ServiceLocator.getInstance().getReference(p.get("doctor").toString()));
                        if(detailsMap.containsKey(doctor.getName())){
                            Map<String,List<Patient>> listMap = detailsMap.get(doctor.getName());
                            if(listMap.containsKey(p.get("risklevel"))){
                                List patientList = listMap.get(p.get("risklevel"));
                                patientList.add(patient);
                                listMap.put(p.get("risklevel").toString(),patientList);
                                detailsMap.put(doctor.getName(),listMap);
                            }else {
                                List patientList = new ArrayList();
                                patientList.add(patient);
                                listMap.put(p.get("risklevel").toString(),patientList);
                                detailsMap.put(doctor.getName(),listMap);
                            }
                            //patientMap.put(mildR)
                        }else {
                            Map<String,List<Patient>> listMap = new HashMap<>();
                            List patientList = new ArrayList();
                            patientList.add(patient);
                            listMap.put(p.get("risklevel").toString(),patientList);
                            detailsMap.put(doctor.getName(),listMap);
                        }

                    }

                }


            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet worksheet = workbook.createSheet("RiskwisePatientsReport");

            HSSFCellStyle my_style_1 = workbook.createCellStyle();
            my_style_1.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

            HSSFRow heading = worksheet.createRow(0);
            HSSFCell doc =heading.createCell(0);
            doc.setCellValue("Doctor Name");

            HSSFCell risk =heading.createCell(1);
            risk.setCellValue("Risk Level");

            HSSFCell patientName =heading.createCell(2);
            patientName.setCellValue("Patient Name");

            HSSFCell rType =heading.createCell(3);
            rType.setCellValue("Gender");

            HSSFCell reportValue =heading.createCell(4);
            reportValue.setCellValue("Age");

            HSSFCell weight =heading.createCell(5);
            reportValue.setCellValue("Weight");

            HSSFCell BMI =heading.createCell(6);
            reportValue.setCellValue("BMI");

            int index = 1;
            for(Map.Entry<String,Map<String,List<Patient>>> d:detailsMap.entrySet()){
                HSSFRow detail = worksheet.createRow(index);

                HSSFCell doctorCell = detail.createCell(0);
                worksheet.addMergedRegion(new CellRangeAddress(index,index+d.getValue().size()-1,0,0));
                doctorCell.setCellValue(d.getKey());
                doctorCell.setCellStyle(my_style_1);
                int innerIndex=0;
                for(Map.Entry<String,List<Patient>> r:d.getValue().entrySet()){
                    if(innerIndex > 0){
                        detail = worksheet.createRow(index);
                    }
                    //HSSFRow riskValue = worksheet.createRow(index);

                    HSSFCell riskcell = detail.createCell(1);
                    worksheet.addMergedRegion(new CellRangeAddress(index,index+r.getValue().size()-1,0,0));
                    riskcell.setCellValue(r.getKey());
                    riskcell.setCellStyle(my_style_1);
                    int innerindex1 = 0;
                    for(Patient p: r.getValue()){
//                        HSSFRow patientDetails = worksheet.createRow(index);
//                        HSSFRow patientdetail = worksheet.createRow(index);
                        if (innerindex1 > 0) {
                            detail = worksheet.createRow(index);

                        }

                        HSSFCell patient = detail.createCell(2);
                        patient.setCellValue(p.getName());

                        HSSFCell reportType = detail.createCell(3);
                        reportType.setCellValue(p.getGender().getDisplayName());

                        HSSFCell value = detail.createCell(4);
                        value.setCellValue(p.getAge());

                        HSSFCell weightValue = detail.createCell(5);
                        weightValue.setCellValue(p.getWeight());

                        HSSFCell bmiValue = detail.createCell(6);
                        bmiValue.setCellValue(p.getbMI());

                        index++;
                        innerIndex++;
                        innerindex1++;
                    }
                }

            }

            ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
            workbook.write(outByteStream);
            response.setHeader("Content-Type", "xls");
            response.setHeader("Content-Length", String.valueOf(outByteStream.size()));
            response.setHeader("Content-Disposition", "attachment; filename=\"RiskwisePatientsReport"+new Date().toString()+".xls\"");
            response.getOutputStream().write(outByteStream.toByteArray());
            outByteStream.flush();
            outByteStream.close();


        }catch (Exception e){
            try {
                throw new Exception(e);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
        }
    }
