package com.squer.services;

import com.squer.platform.appframework.exception.SquerException;
import com.squer.sfe.util.EmailUtil;

import javax.ws.rs.*;

/**
 * Created by mithila on 19/07/17.
 */
@Path("/EmailService")
public class EmailService {

    @Path("/sendEmail")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Boolean sendEmail(EmailDTO dto) throws SquerException {
        try {
            boolean result = EmailUtil.sendMail("support@sugrqb.com",dto.getBody(),dto.getSubject());

            if(result==true && dto.getSendCopy()==true){
                String body= "We have received your request, " +dto.getBody()+". We will get back to you soon. Thank you.";
               boolean ack =  EmailUtil.sendAcknowledgement(dto.getToEmail(),body,"Acknowledgement");
               System.out.print(ack);
            }

            return result;
        }catch (Exception e){
            throw new SquerException(e);
        }


    }
}
