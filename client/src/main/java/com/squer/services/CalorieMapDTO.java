package com.squer.services;

import com.squer.sfe.api.common.entity.reference.CalorieMasterDataReference;

/**
 * Created by mithila on 30/06/17.
 */
public class CalorieMapDTO {
    private CalorieMasterDataReference selectedFood;
    private String foodItem;
    private Integer qty;
    private Integer total;
    private Integer calorieContent;
    private String portionSize;
    private String id;

    public CalorieMasterDataReference getSelectedFood() {
        return selectedFood;
    }

    public void setSelectedFood(CalorieMasterDataReference selectedFood) {
        this.selectedFood = selectedFood;
    }

    public String getFoodItem() {
        return foodItem;
    }

    public void setFoodItem(String foodItem) {
        this.foodItem = foodItem;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getCalorieContent() {
        return calorieContent;
    }

    public void setCalorieContent(Integer calorieContent) {
        this.calorieContent = calorieContent;
    }

    public String getPortionSize() {
        return portionSize;
    }

    public void setPortionSize(String portionSize) {
        this.portionSize = portionSize;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
