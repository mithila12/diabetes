package com.squer.services;

/**
 * Created by mithila on 23/04/18-Apr-2018 at 12:38 PM
 */
public class PublisherDTO {

    private String to;
    private String title;
    private String url;
    private String picture;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
