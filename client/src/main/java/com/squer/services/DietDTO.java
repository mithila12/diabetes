package com.squer.services;

/**
 * Created by mithila on 26/05/17.
 */
public class DietDTO {
    private Planner planner;

    public Planner getPlanner() {
        return planner;
    }

    public void setPlanner(Planner planner) {
        this.planner = planner;
    }
}
