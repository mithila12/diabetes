package com.squer.services;

import com.squer.platform.appframework.config.reference.UserLovReference;

/**
 * Created by mithila on 29/06/17.
 */
public class TargetDataDTO {
    private String riskFactor;
    private String reportName;
    private Double reportValue;
    private UserLovReference reportId;
    private Double thresholdMax;
    private Double thresholdMin;
    private String reportType;

    public String getRiskFactor() {
        return riskFactor;
    }

    public void setRiskFactor(String riskFactor) {
        this.riskFactor = riskFactor;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public Double getReportValue() {
        return reportValue;
    }

    public void setReportValue(Double reportValue) {
        this.reportValue = reportValue;
    }

    public Double getThresholdMax() {
        return thresholdMax;
    }

    public void setThresholdMax(Double thresholdMax) {
        this.thresholdMax = thresholdMax;
    }

    public Double getThresholdMin() {
        return thresholdMin;
    }

    public void setThresholdMin(Double thresholdMin) {
        this.thresholdMin = thresholdMin;
    }

    public UserLovReference getReportId() {
        return reportId;
    }

    public void setReportId(UserLovReference reportId) {
        this.reportId = reportId;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }
}
