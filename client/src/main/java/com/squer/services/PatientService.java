package com.squer.services;

import com.mysql.jdbc.StringUtils;
import com.squer.platform.ServiceLocator;
import com.squer.platform.appframework.auth.UserEntity;
import com.squer.platform.appframework.auth.reference.UserEntityReference;
import com.squer.platform.appframework.config.UserLov;
import com.squer.platform.appframework.config.reference.SysLovReference;
import com.squer.platform.appframework.config.reference.UserLovReference;
import com.squer.platform.appframework.entity.SquerEntity;
import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.utility.CriteriaMap;
import com.squer.platform.persistence.utility.QueryCondition;
import com.squer.platform.registry.CoreQuery;
import com.squer.platform.registry.PlatformDelegates;
import com.squer.registry.CommonQuery;
import com.squer.sfe.api.common.entity.*;
import com.squer.sfe.api.common.entity.reference.DoctorReference;
import com.squer.sfe.api.common.entity.reference.PatientReference;
import org.apache.commons.collections.map.HashedMap;
import org.jasypt.commons.CommonUtils;
import org.springframework.security.access.method.P;

import javax.ws.rs.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.*;

/**
 * Created by mithila on 26/03/17.
 */

@Path("/PatientService")
public class PatientService {


    @Path("/getPatient/{doctorid}/{reportType}")
    @GET
    @Consumes("application/json")
    @Produces("application/json")
    public Map getPatient(@PathParam("doctorid")String doctorid, @PathParam("reportType")String reportType) throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        CriteriaMap criteriaMap_doc=new CriteriaMap(CommonQuery.DIABETES_PATIENT_REPORT_SELECT);
        criteriaMap_doc.addCriteria("doctorid",doctorid);
        String typeId = null;
        if(reportType.equalsIgnoreCase("sugar_profile_report_type")){
            reportType = null;
            typeId ="USRLVRT000000000000000000000000000001";
        }
        criteriaMap_doc.addCriteria("reportType",reportType);
        criteriaMap_doc.addCriteria("typeId",typeId);
        List<Map> patients=repository.find(criteriaMap_doc);
        int h=0;
        int m=0;
        int l = 0;
        int totalCount = patients.size();
        System.out.println(totalCount);
        if(totalCount >0){
            for(Map object: patients) {
                float a=   Float.parseFloat((object.get("ReportValue").toString()));
                float b = (float) 0.0;
                if(Float.parseFloat(object.get("ThresholdMax").toString())>0){
                    b=   Float.parseFloat((object.get("ThresholdMax").toString()));
                }else if(Float.parseFloat(object.get("ThresholdMin").toString())>0){
                    b = Float.parseFloat((object.get("ThresholdMin").toString()));
                }
                if(a<=b) {
                    m++;
                }else if(a>b){
                    h++;
                }
            }


//        double highPerc =  Math.round (((double)h*100)/(double) totalCount);
//        double lowPerc = Math.round (((double)l*100)/(double) totalCount);
//        double medPerc = Math.round (((double)m*100)/(double) totalCount);

                double highPerc = ((h * 100) / totalCount);
                double lowPerc = ((l * 100) / totalCount);
                double medPerc = ((m * 100) / totalCount);

                if (highPerc != 0 && lowPerc != 0 && medPerc != 0) {
                    lowPerc = 100 - (highPerc + medPerc);

                } else if (highPerc != 0 && lowPerc != 0 && medPerc == 0) {
                    highPerc = 100 - lowPerc;

                } else if (highPerc != 0 && lowPerc == 0 && medPerc != 0) {
                    highPerc = 100 - medPerc;

                } else if (highPerc == 0 && lowPerc != 0 && medPerc != 0) {
                    lowPerc = 100 - medPerc;

                } else if (highPerc != 0 && lowPerc == 0 && medPerc == 0) {
                    highPerc = 100;

                } else if (highPerc == 0 && lowPerc != 0 && medPerc == 0) {
                    lowPerc = 100;

                } else if (highPerc == 0 && lowPerc == 0 && medPerc != 0) {
                    medPerc = 100;

                } else if (highPerc == 0 && lowPerc == 0 && medPerc == 0) {
                    highPerc = 0;
                    lowPerc = 0;
                    medPerc = 0;
                }
                Map<String,Double>resultMap=new HashedMap();
                resultMap.put("h",highPerc);
                resultMap.put("m",medPerc);
                resultMap.put("l",lowPerc);
                return resultMap;
            }else{
                Map<String,Double>resultMap=new HashedMap();
                resultMap.put("h",0.0);
                resultMap.put("m",0.0);
                resultMap.put("l",0.0);
                return resultMap;
            }
    }

    @Path("/getPatientList/{doctorid}/{reportType}")
    @GET
    @Consumes("application/json")
    @Produces("application/json")
    public Map<String, List<Patient>> getPatientList(@PathParam("doctorid")String doctorid, @PathParam("reportType")String reportType) throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        CriteriaMap criteriaMap_doc=new CriteriaMap(CommonQuery.DIABETES_PATIENT_REPORT_SELECT);
        criteriaMap_doc.addCriteria("doctorid",doctorid);
        String typeId = null;
        if(reportType.equalsIgnoreCase("sugar_profile_report_type")){
            reportType = null;
            typeId ="USRLVRT000000000000000000000000000001";
        }
        criteriaMap_doc.addCriteria("reportType",reportType);
        criteriaMap_doc.addCriteria("typeId",typeId);

        Map<String, List<Patient>> listMap = new HashMap<String, List<Patient>>();
        List<Patient> highRisk = new ArrayList<>();
        List<Patient> mildRisk = new ArrayList<>();
        List<Patient> lowRisk = new ArrayList<>();
        int h=0;
        int m=0;
        int l = 0;
        List<Map> patients=repository.find(criteriaMap_doc);

        for(Map object: patients) {
            float a=   Float.parseFloat((object.get("ReportValue").toString()));
            float b = (float) 0.0;
            if(Float.parseFloat(object.get("ThresholdMax").toString())>0){
                b =   Float.parseFloat((object.get("ThresholdMax").toString()));
            }else if(Float.parseFloat(object.get("ThresholdMin").toString())>0){
                b = Float.parseFloat((object.get("ThresholdMin").toString()));
            }
            if(a<=b) {
                m++;
            }else if(a>b){
                h++;
            }
            Patient patient= (Patient) repository.restore(ServiceLocator.getInstance().getReference(object.get("ID").toString()));
            DocumentDTO documentDTO = ImageService.fetchDocument(patient.getId(),"SYSLVIMGTP000000000000000000000000001");
            if(documentDTO.getByteContent()!=null){
                patient.setByteContent(documentDTO.getByteContent());
            }
            if(a<=b) {
                mildRisk.add(patient);
            }else if(a>b){
                highRisk.add(patient);
            }
        }

        listMap.put("highRisk", highRisk);
        listMap.put("lowRisk", lowRisk);
        listMap.put("mildRisk", mildRisk);

        return listMap;
    }

    @Path("/getWeight/{doctorid}")
    @GET
    @Consumes("application/json")
    @Produces("application/json")
    public Map getPatientWeight(@PathParam("doctorid")String doctorid) throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        CriteriaMap criteriaMap_doc=new CriteriaMap(CommonQuery.PATIENT_WEIGHT_SELECT);
        criteriaMap_doc.addCriteria("doctorid",doctorid);
        List<Map> patients=repository.find(criteriaMap_doc);
        int h=0;
        int m=0;
        int l = 0;
        int totalCount = patients.size();
        if(totalCount >0){
            for(Map object: patients) {
                float a=   Float.parseFloat((object.get("BMI").toString()));
                if(a<=18.5) {
                    l++;
                }else if( a>= 18.5 && a <=25){
                    m++;
                } else{
                    h++;
                }
            }
            double highPerc =  ((h*100)/ totalCount);
            double lowPerc = ((l*100)/ totalCount);
            double medPerc = ((m*100)/ totalCount);

            if(highPerc!=0 && lowPerc !=0 && medPerc !=0){
                lowPerc = 100-(highPerc+medPerc);

            }else if(highPerc!=0 && lowPerc != 0 && medPerc == 0){
                highPerc = 100 - lowPerc;

            }else if(highPerc!=0 && lowPerc == 0 && medPerc != 0){
                highPerc = 100 - medPerc;

            }else if(highPerc==0 && lowPerc != 0 && medPerc != 0){
                lowPerc = 100 - medPerc;

            }else if(highPerc!=0 && lowPerc == 0 && medPerc == 0){
                highPerc = 100;

            }else if(highPerc == 0 && lowPerc != 0 && medPerc == 0){
                lowPerc = 100;

            }else if(highPerc==0 && lowPerc == 0 && medPerc != 0){
                medPerc = 100;

            }else if(highPerc == 0 && lowPerc == 0 && medPerc == 0){
                highPerc = 0;
                lowPerc = 0;
                medPerc = 0;
            }

            Map<String,Double>resultMap=new HashedMap();
            resultMap.put("h",highPerc);
            resultMap.put("m",lowPerc);
            resultMap.put("l",medPerc);

            return resultMap;
        }else{
            Map<String,Double>resultMap=new HashedMap();
            resultMap.put("h",0.0);
            resultMap.put("m",0.0);
            resultMap.put("l",0.0);

            return resultMap;
        }



    }

    @Path("/getWeightList/{doctorid}")
    @GET
    @Consumes("application/json")
    @Produces("application/json")
    public Map getPatientWeightList(@PathParam("doctorid")String doctorid) throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        CriteriaMap criteriaMap_doc=new CriteriaMap(CommonQuery.PATIENT_WEIGHT_SELECT);
        criteriaMap_doc.addCriteria("doctorid",doctorid);
        List<Map> patients=repository.find(criteriaMap_doc);
        Map<String, List<Patient>> listMap = new HashMap<String, List<Patient>>();
        List<Patient> normal = new ArrayList<>();
        List<Patient> overWeight = new ArrayList<>();
        List<Patient> underWeight = new ArrayList<>();
        int h=0;
        int m=0;
        int l = 0;


        for(Map object: patients) {
            float a=   Float.parseFloat((object.get("BMI").toString()));
            Patient patient= (Patient) repository.restore(ServiceLocator.getInstance().getReference(object.get("ID").toString()));
            DocumentDTO documentDTO = ImageService.fetchDocument(patient.getId(),"SYSLVIMGTP000000000000000000000000001");
            if(documentDTO.getByteContent()!=null){
                patient.setByteContent(documentDTO.getByteContent());
            }
            if(a<=18.5) {
                underWeight.add(patient);
            }else if( a>= 18.5 && a <=25){
                normal.add(patient);
            } else if(a>=25){
                overWeight.add(patient);
            }
        }

        listMap.put("highRisk", overWeight);
        listMap.put("lowRisk", normal);
        listMap.put("mildRisk", underWeight);

        return listMap;
    }


    @Path("/getPatientTarget/{patientId}")
    @GET
    @Consumes("application/json")
    @Produces("application/json")
    public List<TargetDataDTO> getPatientTargetData(@PathParam("patientId")String patientId) throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        List<TargetDataDTO> dataDTOList = new ArrayList<>();

        CriteriaMap allReports=new CriteriaMap(CoreQuery.USER_LOV_SELECT);
        List<QueryCondition> queryConditions=new ArrayList<>();
        queryConditions.add(new QueryCondition("TYPE","like","%_report_type"));
        allReports.addCriteria("searchQuery",queryConditions);
        List<UserLov> allReportsMap=repository.find(allReports);

        CriteriaMap lastEntered=new CriteriaMap(CommonQuery.TARGET_REPORT_SELECT);
        lastEntered.addCriteria("patientId",patientId);
        List<Map> lastEnterdRecords=repository.find(lastEntered);

        Map<String,Double> lastEnterdMap=new HashedMap();

        CriteriaMap threshold=new CriteriaMap(CommonQuery.TARGET_THRESHOLD_SELECT);
        threshold.addCriteria("patientId",patientId);
        List<Map> thresholdValues=repository.find(threshold);

        Map<String,RiskDTO> thresholdMap=new HashedMap();

        for(Map m : lastEnterdRecords){
            if(!m.get("type_id").equals("USRLVRT000000000000000000000000000016")){
                lastEnterdMap.put(m.get("type_id").toString(),Double.parseDouble(m.get("report_value").toString()));
            }
        }
        for(Map t : thresholdValues){
            RiskDTO dto = new RiskDTO();
            dto.setMax(Double.parseDouble(t.get("max_value").toString()));
            dto.setMin(Double.parseDouble(t.get("min_value").toString()));
            thresholdMap.put(t.get("metric").toString(),dto);
        }



        for(UserLov all: allReportsMap){
            TargetDataDTO dataDTO = new TargetDataDTO();
            dataDTO.setReportName(all.getName());
            dataDTO.setReportType(all.getType());
            dataDTO.setReportId((UserLovReference) ServiceLocator.getInstance().getReference(all.getId()));
            if(lastEnterdMap.containsKey(all.getId())){
                dataDTO.setReportValue(lastEnterdMap.get(all.getId()));
            }else{
                dataDTO.setReportValue(0.0);
            }

            if(thresholdMap.containsKey(all.getId())){
               dataDTO.setThresholdMax(thresholdMap.get(all.getId()).getMax());
                dataDTO.setThresholdMin(thresholdMap.get(all.getId()).getMin());
            }else{
                dataDTO.setThresholdMax(0.0);
                dataDTO.setThresholdMin(0.0);
            }


            if("USRLVRT000000000000000000000000000014".equalsIgnoreCase(dataDTO.getReportId().getId())){
                if(dataDTO.getReportValue()!=0){
                    dataDTO.setRiskFactor("Present");
                }else {
                    dataDTO.setRiskFactor("Absent");
                }
            }else {
                if (dataDTO.getReportValue()==0){
                    dataDTO.setRiskFactor("No Data");
                } else if (dataDTO.getReportValue()<dataDTO.getThresholdMax() && dataDTO.getReportValue()>dataDTO.getThresholdMin()){
                    dataDTO.setRiskFactor("Normal");
                }else if (dataDTO.getReportValue()>dataDTO.getThresholdMax()){
                    dataDTO.setRiskFactor("High");
                }else {
                    dataDTO.setRiskFactor("Low");
                }
            }

            dataDTOList.add(dataDTO);

        }


        return dataDTOList;
    }

    @GET
    @Path("/getPatientReports/{patientId}/{duration}/{type}")
    @Produces("application/json")
    @Consumes("application/json")
    public Map<String,List<Map>> getPatientReports(@PathParam("patientId") String patientId, @PathParam("duration") String duration, @PathParam("type") String type) throws Exception {

        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try{
            CriteriaMap map = new CriteriaMap(CommonQuery.PATIENTS_REPORT_DURATION);
            map.addCriteria("PATIENT_ID",patientId);
            map.addCriteria("REPORT_TYPE_ID",type);
            map.addCriteria("timeSpan",duration);
            List<Map> reports = repository.find(map);
            List<Map<String,List<Map>>> patientReportsList = new ArrayList<>();
            Map<String,List<Map>> reportsMap = new LinkedHashMap<>();
            if(reports.size()>0){
                for(Map r:reports){


                    if(reportsMap.containsKey(r.get("NAME").toString())){
                        List reportList = reportsMap.get(r.get("NAME").toString());
                        reportList.add(r);
                    }else {
                        List l = new ArrayList();
                        l.add(r);
                        reportsMap.put(r.get("NAME").toString(),l);
                    }
                }
            }
            return reportsMap;
        }catch (Exception e){
            throw new Exception(e);
        }

    }

    @GET
    @Path("/getMedicineList/{patientId}")
    @Consumes("application/json")
    @Produces("application/json")
    public List<ShowMedicationDTO> getMedicineList(@PathParam("patientId") String patientId) throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try {
            CriteriaMap map = new CriteriaMap(CommonQuery.MEDICINE_SELECT);
            QueryCondition condition = new QueryCondition("PATIENT_ID",patientId);
            List<QueryCondition> conditions = new ArrayList<>();
            conditions.add(condition);
            map.addCriteria("searchQuery",conditions);

            List<Medication> medications = repository.find(map);


            List<ShowMedicationDTO> medicationDTOS = new ArrayList<>();
            Map<String, ShowMedicationDTO> medicalDetail = new HashedMap();

            for(Medication m: medications){
                if(medicalDetail.containsKey(m.getName())){
                    List doseTiming = medicalDetail.get(m.getName()).getDoseTimings();
                    doseTiming.add(m.getDoseTime());
                    medicalDetail.get(m.getName()).setDoseTimings(doseTiming);
                } else {
                    ShowMedicationDTO dto = new ShowMedicationDTO();
                    dto.setName(m.getName());
                    dto.setDose(m.getDose());
                    dto.setSince(m.getSince());
                    dto.setRoute(m.getRoute());
                    List doseTime = new ArrayList();
                    doseTime.add(m.getDoseTime());
                    dto.setDoseTimings(doseTime);
                    medicalDetail.put(m.getName(), dto);
                }
            }

            for(Map.Entry<String,ShowMedicationDTO> r: medicalDetail.entrySet()) {
                medicationDTOS.add(r.getValue());
            }
            return medicationDTOS;
            /*List<Map<String,List<UserLovReference>>> doseTimingsList = new ArrayList<>();
            Map<String,List<UserLovReference>> doseTimings = new HashedMap();
            //String name = null;
            ShowMedicationDTO dto = new ShowMedicationDTO();
            List<ShowMedicationDTO> uniqueList = new ArrayList<>();
            if(medications.size()>0){
                for(Medication m :medications){
                    String name = m.getName();

                    if(doseTimings.containsKey(name)){
                        List l = doseTimings.get(name);
                        l.add(m.getDoseTime());
                    }else {
                        List l = new ArrayList();
                        l.add(m.getDoseTime());
                        doseTimings.put(name,l);
                    }
                }

                for(Medication me:medications){
                    ShowMedicationDTO medicationDTO= new ShowMedicationDTO();
                    for(Map.Entry<String,List<UserLovReference>> r: doseTimings.entrySet()){
                        if(r.getKey().equals(me.getName())){

                            medicationDTO.setName(me.getName());
                            medicationDTO.setDose(me.getDose());
                            medicationDTO.setRoute(me.getRoute());
                            medicationDTO.setSince(me.getSince());
                            medicationDTO.setDoseTimings(r.getValue());

                            medicationDTOS.add(medicationDTO);
                        }
                    }
                }
            }*/
            }catch (Exception e){
            throw new Exception(e);
        }

    }

    @GET
    @Path("/getMessages/{doctorId}")
    @Produces("application/json")
    @Consumes("application/json")
    public List<MessageDTO> getMessages(@PathParam("doctorId") String doctorId) throws Exception {

        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);

        try {
            CriteriaMap map = new CriteriaMap(CommonQuery.MESSAGE_SELECT);
            QueryCondition condition = new QueryCondition("DOCTOR_ID",doctorId);

            List<QueryCondition> conditions = new ArrayList<>();
            conditions.add(condition);
            map.addCriteria("sortOrder","ORDER BY DATE_RECORDED ASC");
            map.addCriteria("searchQuery",conditions);

            List<Messages> messages = repository.find(map);
            List<MessageDTO> messageDTOS = new ArrayList<>();

            for(Messages m: messages){
                DocumentDTO documentDTO = ImageService.fetchDocument(m.getId(),"SYSLVIMGTP000000000000000000000000002");
                MessageDTO dto = new MessageDTO();
                if(documentDTO.getByteContent()!=null){
                    dto.setContent(documentDTO.getByteContent());
                }
                dto.setDate(m.getCreatedOn());
                dto.setDoctor(m.getDoctor());
                dto.setMessage(m.getMessage());
                dto.setId(m.getId());
                messageDTOS.add(dto);
            }
            return messageDTOS;

        }catch (Exception e){
            throw new Exception(e);
        }

    }

    @GET
    @Path("/getLifeStyleQuestionAnswers/{patientId}")
    @Produces("application/json")
    @Consumes("application/json")
    public Map<String,List<String>> getLifeStyleQuestionAnswers(@PathParam("patientId") String patientId) throws Exception {
        try {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);

            CriteriaMap map = new CriteriaMap(CommonQuery.LIFESTYLE_QUESTION_SELECT);
            map.addCriteria("PATIENT_ID",patientId);

            List<Map> answers = repository.find(map);

            Map<String,List<String>> answersMap = new LinkedHashMap<>();
            for(Map a: answers) {
                if (answersMap.containsKey(a.get("NAME").toString())) {
                    List l = answersMap.get(a.get("NAME").toString());
                    l.add(a.get("answerText").toString());
                } else {
                    List l = new ArrayList();
                    l.add(a.get("answerText").toString());
                    answersMap.put(a.get("NAME").toString(), l);
                }
            }
            return answersMap;
        }catch (Exception e){
            throw new Exception();
        }
    }


    @GET
    @Path("/getAllPatientsList/{doctorId}")
    @Produces("application/json")
    @Consumes("application/json")
    public List<Patient> getAllPatients(@PathParam("doctorId") String doctorId) throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try{
            CriteriaMap map = new CriteriaMap(CommonQuery.DIABETES_PATIENT_SELECT);
            List<QueryCondition> conditions = new ArrayList<>();
            QueryCondition condition = new QueryCondition("DOCTOR_ID",doctorId);
            QueryCondition condition1 = new QueryCondition("STATUS_ID","SYSLV00000000000000000000000000000001");
            conditions.add(condition);
            conditions.add(condition1);
            map.addCriteria("searchQuery",conditions);
            List<Patient> patients = repository.find(map);

            for(Patient p:patients){
                DocumentDTO documentDTO = ImageService.fetchDocument(p.getId(),"SYSLVIMGTP000000000000000000000000001");

                if(documentDTO.getByteContent()!=null){
                    p.setByteContent(documentDTO.getByteContent());
                }

            }

            return patients;
        }catch (Exception e){
            throw new Exception(e);
        }

    }

    @POST
    @Path("/enrollPatients/")
    @Produces("application/json")
    @Consumes("application/json")
    public EnrollDTO enrollPatients(EnrollDTO dto) throws Exception {

        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try {
            String owner = ServiceLocator.getInstance().getPrincipal().getUser().getOwner().getId();
            Doctor doctor = (Doctor) repository.restore(ServiceLocator.getInstance().getReference(owner));

            String doctorCode = doctor.getCode();
            String response = "";
            if(dto.getContactNo()!=null && dto.getTemplate()!=null){

                String contactNo = dto.getContactNo();
                String template = "Welcome to SugrQb! Your companion in Diabetes management. Your doctor has invited you to download SugrQb App from Playstore : goo.gl/MwqHAA. or App Store: tiny.cc/12f7ty Use doctor's unique access code: "+ doctorCode+" to register yourself.";
                response = sendSms(template,contactNo);
                System.out.print(response);

                EnrollPatientsData patientsData = new EnrollPatientsData();
                patientsData.setContactNo(contactNo);
                patientsData.setDoctor((DoctorReference) ServiceLocator.getInstance().getReference(doctor.getId()));
                repository.create(patientsData);
                dto.setStatus("Sent");
            }else {
                dto.setStatus("Failed");
            }

            return dto;
        }catch (Exception e){
            throw new Exception(e);
        }

    }

    public static String sendSms(String template, String contact) {
        try {
            // Construct data
            String apiKey = "apikey=" + URLEncoder.encode("JLSXa1lemaY-SOhrTkOjnSZYbmR3zBJchU78KjzfCD\t", "UTF-8");
            String message = "&message=" + URLEncoder.encode(template, "UTF-8");
            String sender = "&sender=" + URLEncoder.encode("SUGRQB", "UTF-8");
            String numbers = "&numbers=" + URLEncoder.encode(contact, "UTF-8");

            // Send data
            String data = "https://api.textlocal.in/send/?" + apiKey + numbers + message + sender;
            URL url = new URL(data);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);

            // Get the response
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            String sResult="";
            while ((line = rd.readLine()) != null) {
                // Process line...
                sResult=sResult+line+" ";
            }
            rd.close();

            return sResult;
        } catch (Exception e) {
            System.out.println("Error SMS "+e);
            return "Error "+e;
        }
    }

    @GET
    @Path("/getTestReports/{patientId}")
    @Produces("application/json")
    @Consumes("application/json")
    public List<TestReportDTO> getTestReports(@PathParam("patientId") String patientId) throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try{
            CriteriaMap map = new CriteriaMap(CommonQuery.PATIENT_REPORTS_SELECT);
            QueryCondition condition = new QueryCondition("PATIENT_ID",patientId);
            List<QueryCondition> conditions = new ArrayList<>();
            conditions.add(condition);
            map.addCriteria("searchQuery",conditions);

            List<PatientReports> reports = repository.find(map);
            List<TestReportDTO> documentDTOS = new ArrayList<>();
            if(reports.size()>0){
                for(PatientReports p: reports){
                    DocumentDTO documentDTO = ImageService.fetchDocument(p.getId(),"SYSLVIMGTP000000000000000000000000003");

                    if(documentDTO.getByteContent()!=null){
                        TestReportDTO dto = new TestReportDTO();
                        dto.setContent(documentDTO.getByteContent());
                        dto.setPatientId(p.getPatient().getId());
                        dto.setReportId(p.getId());

                        documentDTOS.add(dto);
                    }
                }
            }
            return documentDTOS;
        }catch (Exception e){
            throw new Exception(e);
        }

    }

    @GET
    @Path("/getAllDoctors")
    @Produces("application/json")
    @Consumes("application/json")
    public List<Doctor> getAllDoctors() throws Exception{
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try{
            CriteriaMap map = new CriteriaMap(CommonQuery.DIABETES_DOCTOR_SELECT);
            List<Doctor> doctors = repository.find(map);
            return doctors;
        }catch (Exception e){
            throw new Exception(e);
        }

    }

    @GET
    @Path("/activePatients/{doctorId}")
    @Produces("application/json")
    @Consumes("application/json")
    public int activePatients(@PathParam("doctorId") String doctorId) throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try {
            CriteriaMap patientMap = new CriteriaMap(CommonQuery.DIABETES_PATIENT_SELECT);
            QueryCondition condition = new QueryCondition("STATUS_ID","SYSLV00000000000000000000000000000001");
            QueryCondition condition1 = new QueryCondition("DOCTOR_ID",doctorId);
            List<QueryCondition> conditions = new ArrayList<>();
            conditions.add(condition);
            conditions.add(condition1);
            patientMap.addCriteria("searchQuery",conditions);
            List<Patient> patients = repository.find(patientMap);

            int patientcount = patients.size();

            return patientcount;
        }catch (Exception e){
            throw new Exception(e);
        }

    }

    @GET
    @Path("/getMessageById/{id}")
    @Produces("application/json")
    @Consumes("application/json")
    public MessageDTO getMessageById(@PathParam("id") String id) throws Exception {

        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try {
            Messages messages = (Messages) repository.restore(ServiceLocator.getInstance().getReference(id));

            DocumentDTO documentDTO = ImageService.fetchDocument(messages.getId(),"SYSLVIMGTP000000000000000000000000002");
            MessageDTO dto = new MessageDTO();
            if(documentDTO.getByteContent()!=null){
                dto.setContent(documentDTO.getByteContent());
            }
            dto.setDate(messages.getCreatedOn());
            dto.setDoctor(messages.getDoctor());
            dto.setMessage(messages.getMessage());
            dto.setId(messages.getId());

            return dto;
        }catch (Exception e){
            throw new Exception(e);
        }

    }

    @GET
    @Path("/getMessagesForPatient/{patientId}")
    @Produces("application/json")
    @Consumes("application/json")
    public List<Messages> getMessagesForPatient(@PathParam("patientId") String patientId) throws Exception{
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try {
            CriteriaMap map = new CriteriaMap(CommonQuery.GET_MESSAGES_FOR_PATIENT);
            map.addCriteria("patientId",patientId);

            List<Messages> messages = repository.find(map);

            return messages;

        }catch (Exception e){
            throw new Exception(e);
        }
    }

    @GET
    @Path("/deletePatient/{patientId}")
    @Produces("application/json")
    @Consumes("application/json")
    public PatientReference deletePatient(@PathParam("patientId") String patientId) throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);

        try {
            Patient patient = (Patient) repository.restore(ServiceLocator.getInstance().getReference(patientId));

            patient.setStatus((SysLovReference) ServiceLocator.getInstance().getReference("SYSLV00000000000000000000000000000002"));

            updateUserEntity(patient);
            return (PatientReference) repository.update(patient);


        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
        }
    }

    private UserEntityReference updateUserEntity(SquerEntity entity) throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try {
            CriteriaMap map = new CriteriaMap(CoreQuery.USER_SELECT);
            QueryCondition condition = new QueryCondition("owner_id",entity.getId());
            List<QueryCondition> conditions = new ArrayList<>();
            conditions.add(condition);
            map.addCriteria("searchQuery",conditions);
            List<UserEntity> userEntities = repository.find(map);

            UserEntity userEntity = userEntities.get(0);
            userEntity.setStatus((SysLovReference) ServiceLocator.getInstance().getReference("SYSLV00000000000000000000000000000002"));

            return (UserEntityReference) repository.update(userEntity);
        }catch (Exception e){
            throw new Exception(e);
        }

    }

}