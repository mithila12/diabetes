package com.squer.services;

import java.util.List;
import java.util.Map;

/**
 * Created by mithila on 23/05/18-May-2018 at 7:50 PM
 */
public class StackChartDTO {

    private List<String> xaxis;

    private List<StackChartInnerDTO> series;

    public List<String> getXaxis() {
        return xaxis;
    }

    public void setXaxis(List<String> xaxis) {
        this.xaxis = xaxis;
    }

    public List<StackChartInnerDTO> getSeries() {
        return series;
    }

    public void setSeries(List<StackChartInnerDTO> series) {
        this.series = series;
    }
}
