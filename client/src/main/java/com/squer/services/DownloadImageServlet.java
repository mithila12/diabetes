package com.squer.services;

import com.squer.platform.ServiceLocator;
import com.squer.platform.api.security.authentication.AuthenticationHandler;
import com.squer.platform.appframework.tenant.entity.reference.TenantReference;
import com.squer.platform.persistence.Repository;
import com.squer.platform.registry.PlatformDelegates;
import com.squer.platform.utils.StringUtil;
import com.squer.sfe.api.common.entity.Document;
import org.apache.commons.io.FilenameUtils;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

/**
 * Created by mithila on 27/01/17.
 */
public class DownloadImageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            downloadImages(req, resp);
        } catch (Exception e) {
            new ServletException(e);
        }
    }

    public void downloadImages(HttpServletRequest req,HttpServletResponse res) throws Exception{
      //  String certificate=req.getParameter("cert");
      //  if(StringUtil.isNullOrEmpty(certificate)){
      //      throw new Exception("Invalid Access");
      //  }
        AuthenticationHandler.authenticateAsGuest((TenantReference) ServiceLocator.getInstance().getReference("TENAT00000000000000000000000000000001"));

        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        String docId=req.getParameter("documentId");
        Document document= (Document) repository.restore(ServiceLocator.getInstance().getReference(docId));
        BufferedOutputStream output = new BufferedOutputStream(res.getOutputStream());
        File file = new File(System.getProperty("config.path")+"/documents/"+ServiceLocator.getInstance().getPrincipal().getTenantId()+ "/" + document.getOwner().getId() + "." +document.getName());
        String ext = FilenameUtils.getExtension(file.getName());
        FileInputStream fis = new FileInputStream(file);
        BufferedInputStream bis = new BufferedInputStream(fis);
        res.setHeader("Content-Type", ext);
        res.setHeader("Content-Length", String.valueOf(file.length()));
        res.setHeader("Content-Disposition", "attachment; filename=\"" + document.getOwner().getId() + "\"");
        byte[] buffer = new byte[(int) file.length()];
        for (int length = 0; (length = bis.read(buffer)) > 0; ) {
            output.write(buffer, 0, length);
        }

        output.flush();
        output.close();
    }

}
