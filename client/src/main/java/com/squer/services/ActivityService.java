package com.squer.services;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squer.platform.ServiceLocator;
import com.squer.platform.appframework.config.reference.UserLovReference;
import com.squer.platform.appframework.entity.SquerReference;
import com.squer.platform.appframework.exception.SquerException;
import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.utility.CriteriaMap;
import com.squer.platform.persistence.utility.QueryCondition;
import com.squer.platform.registry.PlatformDelegates;
import com.squer.platform.rest.impl.services.DateDeserializer;
import com.squer.platform.rest.impl.services.SquerReferenceDeserializer;
import com.squer.registry.CommonQuery;
import com.squer.sfe.api.common.entity.*;
import com.squer.sfe.api.common.entity.reference.CalorieMasterDataReference;
import com.squer.sfe.api.common.entity.reference.PatientReference;
import com.squer.sfe.api.common.entity.reference.QuestionMasterReference;
import com.squer.sfe.api.common.entity.reference.QuestionnaireReference;
import org.apache.commons.collections.map.HashedMap;
import org.codehaus.jackson.map.deser.CustomDeserializerFactory;
import org.codehaus.jackson.map.deser.StdDeserializerProvider;

import javax.swing.text.StyledEditorKit;
import javax.ws.rs.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.squer.platform.utils.CommonUtils.isNullOrEmpty;

/**
     * Created by mithila on 25/05/17.
     */
    @Path("/ActivityService")
    public class ActivityService {
        @Path("/faqs")
        @GET
        @Consumes("application/json")
        @Produces("application/json")
        public FaqDTO selectFaqs() throws Exception {
            try {
                Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
                byte[] jsonData = Files.readAllBytes(Paths.get(System.getProperty("config.path")+"/documents/faqs.txt"));

                //create ObjectMapper instance
                ObjectMapper objectMapper = new ObjectMapper();

                //convert json string to object
                FaqDTO dto = objectMapper.readValue(jsonData, FaqDTO.class);

                return dto;
            }
            catch (Exception e){
                throw new SquerException(e);
            }
        }

    @Path("/techfaqs")
    @GET
    @Consumes("application/json")
    @Produces("application/json")
    public FaqDTO selectTechFaqs() throws Exception {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            byte[] jsonData = Files.readAllBytes(Paths.get(System.getProperty("config.path")+"/documents/techfaqs.txt"));

            //create ObjectMapper instance
            ObjectMapper objectMapper = new ObjectMapper();

            //convert json string to object
            FaqDTO dto = objectMapper.readValue(jsonData, FaqDTO.class);

            return dto;
        }
        catch (Exception e){
            throw new SquerException(e);
        }
    }

    @Path("/techfaqsDoc")
    @GET
    @Consumes("application/json")
    @Produces("application/json")
    public FaqDTO selectTechFaqsDoc() throws Exception {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            byte[] jsonData = Files.readAllBytes(Paths.get(System.getProperty("config.path")+"/documents/techfaqs+.txt"));

            //create ObjectMapper instance
            ObjectMapper objectMapper = new ObjectMapper();

            //convert json string to object
            FaqDTO dto = objectMapper.readValue(jsonData, FaqDTO.class);

            return dto;
        }
        catch (Exception e){
            throw new SquerException(e);
        }
    }


    @Path("/fetchCalorieData")
        @GET
        @Consumes("application/json")
        @Produces("application/json")
        public List<CalorieMasterData> calorieMasterData() throws Exception {
            try {
                Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
                CriteriaMap map  = new CriteriaMap(CommonQuery.CALORIE_MASTER_DATA);
                List<CalorieMasterData> masterData = repository.find(map);

                return masterData;
            }
            catch (Exception e){
                throw new SquerException(e);
            }
        }

        @Path("/diet")
        @GET
        @Consumes("application/json")
        @Produces("application/json")
        public DietDTO selectDietPlan() throws Exception {
            try {
                Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
                byte[] jsonData = Files.readAllBytes(Paths.get(System.getProperty("config.path")+"/documents/dietPlanner.txt"));

                //create ObjectMapper instance
                ObjectMapper objectMapper = new ObjectMapper();

                //convert json string to object
                DietDTO dto = objectMapper.readValue(jsonData, DietDTO.class);

                return dto;
            }
            catch (Exception e){
                throw new SquerException(e);
            }
        }

        @Path("/saveActivity")
        @POST
        @Consumes("application/json")
        @Produces("application/json")
        public Boolean saveActivity(ActivityDTO dto) throws Exception {
            try {
                Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
                Activity activity = new Activity();
                activity.setActivity(dto.getActivity());
                activity.setActivityOf((PatientReference) ServiceLocator.getInstance().getReference(dto.getActivityOf()));
                activity.setCaloriesBurnt(dto.getCaloriesBurnt());
                activity.setActiveTime(dto.getActiveTime());
                activity.setDistanceTravelled(dto.getDistanceTravelled());
                activity.setDate(new Date());
                repository.create(activity);
                return true;
            } catch (Exception e) {
                throw new SquerException(e);
            }
        }

    @Path("/saveCalories")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Boolean saveCalories(String obj) throws Exception {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            org.codehaus.jackson.map.ObjectMapper mapper = new org.codehaus.jackson.map.ObjectMapper();
            CustomDeserializerFactory sf = new CustomDeserializerFactory();
            sf.addSpecificMapping(SquerReference.class, new SquerReferenceDeserializer());
            sf.addSpecificMapping(Date.class, new DateDeserializer());
            mapper.setDeserializerProvider(new StdDeserializerProvider(sf));
            SaveCalorieDTO dto = mapper.readValue(obj, SaveCalorieDTO.class);
            List<PatientCalories> list = dto.getList();
            if(!isNullOrEmpty(list)){
                List<String> items = new ArrayList<>();
                String patientId = list.get(0).getPatient().getId();
                String date = list.get(0).getDate();
                for (PatientCalories p : list) {
                    if (!items.contains(p.getFoodType().getId()))
                        items.add(p.getFoodType().getId());
                }

                CriteriaMap criteriaMap = new CriteriaMap(CommonQuery.CALORIES_BULK_DELETE);
                criteriaMap.addCriteria("patient", patientId);
                criteriaMap.addCriteria("curDate",date);
                criteriaMap.addCriteria("items", items);
                repository.fireAdhoc(criteriaMap);

                for (PatientCalories p : list) {
                    repository.create(p);
                }
            }
            return true;
        } catch (Exception e) {
            throw new SquerException(e);
        }
    }

    @Path("/questions/{typeId}")
    @GET
    @Consumes("application/json")
    @Produces("application/json")
    public Map selectQuestions(@PathParam("typeId") String typeId) throws Exception {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            String type ="";
            if(typeId.equalsIgnoreCase("firstTime")){
                type = "USRLVDIS00000000000000000000000000001";
            }else {
                 type ="USRLVDIS00000000000000000000000000002";
            }

            CriteriaMap map = new CriteriaMap(CommonQuery.QUESTIONS_SELECT);
            map.addCriteria("type",type);
            Map<String,Map<String, List<String>>>resultMap=new LinkedHashMap<>();
            List<String> answers = new ArrayList<>();

            List<Map> opdata = repository.find(map);
            for(Map object: opdata) {
                String question = object.get("question").toString();
                String id = object.get("id").toString();
                String has_ans = object.get("has_ans").toString();
                System.out.println(has_ans);
                if(resultMap.containsKey(question)){
                    Map answerMap = resultMap.get(question);
                    if(answerMap.containsKey(id)){
                        List l = (List) answerMap.get(id);
                        l.add( object.get("answer").toString());
                    }
                } else {
                    Map<String, List<String>> answerMap = new HashMap<>();
                    List l = new ArrayList();
                    if(has_ans.equalsIgnoreCase("true")){
                        l.add(object.get("answer").toString());
                        answerMap.put(object.get("id").toString(),l);
                        resultMap.put(object.get("question").toString(),answerMap);
                    }else {
                        resultMap.put(object.get("question").toString(),answerMap);
                    }

                }

            }
            return resultMap;
        }
        catch (Exception e){
            throw new SquerException(e);
        }
    }

    @Path("/riskQuestions/{patientId}")
    @GET
    @Consumes("application/json")
    @Produces("application/json")
    public List<QuestionDTO> selectAllQuestions(@PathParam("patientId") String patientId) throws Exception {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            Patient patient = (Patient) repository.restore(ServiceLocator.getInstance().getReference(patientId));

            CriteriaMap firstMap = new CriteriaMap(CommonQuery.EXISTING_QUESTIONS_SELECT);
            firstMap.addCriteria("type","USRLVDIS00000000000000000000000000001");
            firstMap.addCriteria("patientId",patientId);
            List<Map> questions = repository.find(firstMap);

            Map<String, QuestionDTO> questionDTOMap = new HashMap<>();
            List<QuestionDTO> questionDTOS = new ArrayList<>();
            if(questions.size()>0){
                Date registeredDate = patient.getCreatedOn();
                Calendar regirsterdDateInstance = Calendar.getInstance();
                regirsterdDateInstance.setTime(registeredDate);
                // current date before 3 months
                regirsterdDateInstance.add(Calendar.DATE,90);


                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                Date currentDate = calendar.getTime();

                if(calendar.getTime().equals(regirsterdDateInstance.getTime()) || calendar.getTime().after(regirsterdDateInstance.getTime())){
                    CriteriaMap secondMap = new CriteriaMap(CommonQuery.EXISTING_QUESTIONS_SELECT);
                    secondMap.addCriteria("type","USRLVDIS00000000000000000000000000002");
                    secondMap.addCriteria("patientId",patientId);
                    List<Map> secondQuestions = repository.find(secondMap);

                    if(secondQuestions.size()>0){
                        questionDTOMap = generateMap(secondQuestions,patientId,"secondTime");
                    }else {
                        CriteriaMap map = new CriteriaMap(CommonQuery.QUESTIONS_SELECT);
                        map.addCriteria("type","USRLVDIS00000000000000000000000000002");

                        List<Map> opdata = repository.find(map);

                        questionDTOMap = generateMap(opdata,patientId,"secondTime");
                    }
                }else {
                    CriteriaMap secondMap = new CriteriaMap(CommonQuery.EXISTING_QUESTIONS_SELECT);
                    secondMap.addCriteria("type","USRLVDIS00000000000000000000000000001");
                    secondMap.addCriteria("patientId",patientId);
                    List<Map> secondQuestions = repository.find(firstMap);

                    if(secondQuestions.size()>0){
                        questionDTOMap = generateMap(secondQuestions,patientId,"firstTime");
                    }else {
                        CriteriaMap map = new CriteriaMap(CommonQuery.QUESTIONS_SELECT);
                        map.addCriteria("type","USRLVDIS00000000000000000000000000001");

                        List<Map> opdata = repository.find(map);

                        questionDTOMap = generateMap(opdata,patientId,"firstTime");
                    }
                }
            }else {
                CriteriaMap map = new CriteriaMap(CommonQuery.QUESTIONS_SELECT);
                map.addCriteria("type","USRLVDIS00000000000000000000000000001");
                map.addCriteria("patientId",patientId);

                List<Map> opdata = repository.find(map);

                questionDTOMap = generateMap(opdata,patientId,"firstTime");
            }

            for(Map.Entry<String,QuestionDTO> r: questionDTOMap.entrySet()) {
                questionDTOS.add(r.getValue());
            }
            return questionDTOS;
        }
        catch (Exception e){
            throw new SquerException(e);
        }
    }

    private Map<String, QuestionDTO> generateMap(List<Map> data,String patientId,String showQuestion){

        Map<String, QuestionDTO> questionDTOMap = new HashedMap();
        for(Map object: data) {
            String question = object.get("question").toString();
            String id = object.get("id").toString();
            String has_ans = object.get("has_ans").toString();
            String ans ="";
            if(object.containsKey("totalScore")){
                ans = object.get("totalScore").toString();
            }else {
                ans = "0";
            }

            System.out.println(has_ans);

            if(questionDTOMap.containsKey(question) && has_ans.equalsIgnoreCase("true")){
                List answers = questionDTOMap.get(question).getAnswers();
                answers.add(object.get("answer"));
                questionDTOMap.get(question).setAnswers(answers);
            } else {
                QuestionDTO questionDTO = new QuestionDTO();
                questionDTO.setQuestion(question);
                questionDTO.setId(id);
                questionDTO.setShowQuestions(showQuestion);
                questionDTO.setSelectedAns(ans);
                questionDTO.setOwner(patientId);
                List answers = new ArrayList();
                answers.add(object.get("answer"));
                questionDTO.setAnswers(answers);
                questionDTOMap.put(question, questionDTO);
            }

        }
        return questionDTOMap;
    }


    @Path("/saveQuestions")
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public boolean saveQuestions(QuestionsWrapperDTO dto) throws Exception {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            try{
                for(QuestionDTO q: dto.getQuestionDTOS()){
                    QuestionScore score = new QuestionScore();
                    score.setTotalScore(q.getSelectedAns());
                    score.setQuestion((QuestionMasterReference) ServiceLocator.getInstance().getReference(q.getId()));
                    score.setOwner(ServiceLocator.getInstance().getReference(q.getOwner()));
                    score.setTypeOfQuiz((QuestionnaireReference) ServiceLocator.getInstance().getReference("QUEST00000000000000000000000000000001"));
                    Calendar calendar = Calendar.getInstance();
                    score.setMonth(String.valueOf(Calendar.MONTH));
                    repository.create(score);
                }
            }catch (Exception e){
                throw new Exception(e);
            }

            return true;

    }


    @Path("/activity/{patientId}/{timeSpan}/{activity}")
    @GET
    @Consumes("application/json")
    @Produces("application/json")
    public List<Activity> fetchActivity(@PathParam("patientId") String patientId,@PathParam("timeSpan") String timeSpan,@PathParam("activity") String activity) throws Exception {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            CriteriaMap map = new CriteriaMap(CommonQuery.ACTIVITY_SELECT);
            map.addCriteria("patientId",patientId);
            map.addCriteria("timeSpan",timeSpan);
            map.addCriteria("activity",activity);
            List<Activity> activities = repository.find(map);

            return activities;
        }
        catch (Exception e){
            throw new SquerException(e);
        }
    }

    @Path("/calories/{patientId}/{date}")
    @GET
    @Consumes("application/json")
    @Produces("application/json")
    public Map fetchCalories(@PathParam("patientId") String patientId,@PathParam("date") String date) throws Exception {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            CriteriaMap map = new CriteriaMap(CommonQuery.CALORIES_MAP_SELECT);
            map.addCriteria("patientId",patientId);
            map.addCriteria("date",date);
            List<Map<String,String>> calories = repository.find(map);

            Map<String,List<CalorieMapDTO>> calorieMap = new HashMap<>();

            for(Map m : calories){
                CalorieMapDTO temp = new CalorieMapDTO();
                temp.setCalorieContent(Integer.parseInt(m.get("content").toString()));
                temp.setFoodItem(m.get("food_item").toString());
                temp.setSelectedFood((CalorieMasterDataReference) ServiceLocator.getInstance().getReference(m.get("master_food_id").toString(),m.get("food_item").toString()));
                temp.setQty(Integer.parseInt(m.get("quantity").toString()));
                temp.setPortionSize(m.get("size").toString());
                temp.setTotal(Integer.parseInt(m.get("total").toString()));
                temp.setId(m.get("cid").toString());
                String s =new String();
                switch (m.get("food_type").toString()){
                    case "USRLVFT000000000000000000000000000001":
                        s="1";
                        break;
                    case "USRLVFT000000000000000000000000000002":
                        s="2";
                        break;
                    case "USRLVFT000000000000000000000000000003":
                        s="3";
                        break;
                    case "USRLVFT000000000000000000000000000004":
                        s="4";
                        break;
                }

                if(calorieMap.containsKey(s)){
                    calorieMap.get(s).add(temp);
                }else{
                    List<CalorieMapDTO> t1 = new ArrayList<CalorieMapDTO>();
                    t1.add(temp);
                    calorieMap.put(s,t1);
                }
            }


            return calorieMap;
        }
        catch (Exception e){
            throw new SquerException(e);
        }
    }

    @Path("/activityGraph/{patientId}/{timeSpan}")
    @GET
    @Consumes("application/json")
    @Produces("application/json")
    public List<GraphDTO> acticityGraphData(@PathParam("patientId") String patientId,@PathParam("timeSpan") String timeSpan) throws Exception {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            List<GraphDTO> graphDTOList = new ArrayList<>();

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            Date toDate = calendar.getTime();
            List<String> date=new ArrayList<>();
            CriteriaMap burntMap = new CriteriaMap(CommonQuery.CALORIE_BURNT_SELECT);
            burntMap.addCriteria("patientId",patientId);
            burntMap.addCriteria("toDate",toDate);
            burntMap.addCriteria("timeSpan",timeSpan);

            CriteriaMap consumedMap = new CriteriaMap(CommonQuery.CALORIE_CONSUMED_SELECT);
            consumedMap.addCriteria("patientId",patientId);
            consumedMap.addCriteria("toDate",toDate);
            consumedMap.addCriteria("timeSpan",timeSpan);

            switch (timeSpan){
                case "week":
                    Date fromDate = new Date();
                    calendar.add(Calendar.DAY_OF_MONTH,-6);
                    fromDate = calendar.getTime();
                    burntMap.addCriteria("fromDate",fromDate);
                    consumedMap.addCriteria("fromDate",fromDate);
                    while (fromDate.before(toDate) || fromDate.equals(toDate)){
                        SimpleDateFormat format=new SimpleDateFormat("YYYY-MM-dd");
                        String dt1=format.format(fromDate.getTime());
                        date.add(dt1);
                        Calendar calendar1=Calendar.getInstance();
                        calendar1.setTime(fromDate);
                        calendar1.add(Calendar.DATE,1);
                        fromDate=calendar1.getTime();
                    }

                    break;
                case "quarter":
                    fromDate = new Date();
                    calendar.add(Calendar.MONTH, -2);
                    calendar.set(Calendar.DAY_OF_MONTH,1);
                    fromDate = calendar.getTime();
                    burntMap.addCriteria("fromDate",fromDate);
                    consumedMap.addCriteria("fromDate",fromDate);
                    while (fromDate.before(toDate) || fromDate.equals(toDate)){
//                        Calendar c = Calendar.getInstance();
//                        c.add(c.MONTH,-3);
                        date.add(String.valueOf(fromDate.getMonth()+1));
                        Calendar calendar1=Calendar.getInstance();
                        calendar1.setTime(fromDate);
                        calendar1.add(Calendar.MONTH,1);
                        fromDate=calendar1.getTime();
                    }

                    break;
                case "half_year":
                    fromDate = new Date();
                    calendar.add(Calendar.MONTH, -5);
                    calendar.set(Calendar.DAY_OF_MONTH,1);
                    fromDate = calendar.getTime();
                    burntMap.addCriteria("fromDate",fromDate);
                    consumedMap.addCriteria("fromDate",fromDate);
                    while (fromDate.before(toDate) || fromDate.equals(toDate)){
                        date.add(String.valueOf(fromDate.getMonth()+1));
                        Calendar calendar1=Calendar.getInstance();
                        calendar1.setTime(fromDate);
                        calendar1.add(Calendar.MONTH,1);
                        fromDate=calendar1.getTime();
                    }

                    break;
                case "year":
                    fromDate = new Date();
                    calendar.add(Calendar.YEAR, -1);
                    fromDate = calendar.getTime();
                    burntMap.addCriteria("fromDate",fromDate);
                    consumedMap.addCriteria("fromDate",fromDate);
                    while (fromDate.before(toDate) ){
                        date.add(String.valueOf(fromDate.getYear()+1900)+"-"+String.valueOf(fromDate.getYear()+1900+1));
                        Calendar calendar1=Calendar.getInstance();
                        calendar1.setTime(fromDate);
                        calendar1.add(Calendar.YEAR,1);
                        fromDate=calendar1.getTime();
                    }

                    break;

                }

                 List<Map> caloriesBurnt = repository.find(burntMap);
                 List<Map> caloriesConsumed = repository.find(consumedMap);

                 Map<String,Double> caloriesBurntMap=new HashedMap();
                Map<String,Double> caloriesConsumedMap=new HashedMap();

                 for(Map cb : caloriesBurnt){
                     caloriesBurntMap.put(cb.get(timeSpan).toString(),Double.parseDouble(cb.get("calories_burnt").toString()));
                 }
                 for(Map cc: caloriesConsumed){
                     caloriesConsumedMap.put(cc.get(timeSpan).toString(),Double.parseDouble(cc.get("calories_consumed").toString()));
                 }


                Map<String,Double> graphData= new LinkedHashMap<>();

                 for(String d :date){
                     GraphDTO dto = new GraphDTO();
//                     SimpleDateFormat format=new SimpleDateFormat("YYYY-MM-dd");
//                     String dt=format.format(d);
                     dto.setDate(d);
                    if(caloriesBurntMap.containsKey(d)){
                         caloriesBurntMap.get(d);
                         dto.setCaloriesBurnt(caloriesBurntMap.get(d));
                     }
                     else {
                         dto.setCaloriesBurnt(0.0);
                     }
                     if(caloriesConsumedMap.containsKey(d)){
                         caloriesConsumedMap.get(d);
                         dto.setCaloriesConsumed(caloriesConsumedMap.get(d));
                     }
                     else {
                         dto.setCaloriesConsumed(0.0);
                     }
                     graphDTOList.add(dto);

                 }


            return graphDTOList;
        }
        catch (Exception e){
            throw new SquerException(e);
        }
    }

    @Path("/saveQuizScore")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Boolean saveQuizScore(QuestionDTO dto) throws Exception {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            return true;
        } catch (Exception e) {
            throw new SquerException(e);
        }
    }

    @Path("/saveMedDoseTime")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public void saveMedDoseTime(String obj) throws Exception {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            org.codehaus.jackson.map.ObjectMapper mapper = new org.codehaus.jackson.map.ObjectMapper();
            CustomDeserializerFactory sf = new CustomDeserializerFactory();
            sf.addSpecificMapping(SquerReference.class, new SquerReferenceDeserializer());
            sf.addSpecificMapping(Date.class, new DateDeserializer());
            mapper.setDeserializerProvider(new StdDeserializerProvider(sf));
            MedicationDTO dto = mapper.readValue(obj, MedicationDTO.class);
            List<Medication> list = dto.getList();
            if (!isNullOrEmpty(list)) {
                List<String>doseTimeId= new ArrayList<>();
                String patientId = list.get(0).getPatient().getId();
                String name=list.get(0).getName().toString();
//                for (Medication medication : list) {
//                    if (!doseTimeId.contains(medication.getDoseTime().getId()))
//                        doseTimeId.add(medication.getDoseTime().getId());
//                }
                CriteriaMap criteriaMap = new CriteriaMap(CommonQuery.MEDICINE_BULK_DELETE);
                criteriaMap.addCriteria("patient", patientId);
                criteriaMap.addCriteria("name",name);
//                criteriaMap.addCriteria("doseTime",doseTimeId );
                repository.fireAdhoc(criteriaMap);
                for (Medication medication : list) {
                    repository.create(medication);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    @Path("/deleteMedDoseTime/{patientId}/{name}")
    @GET
    @Consumes("application/json")
    @Produces("application/json")
    public boolean deleteMedDoseTime(@PathParam("patientId") String patientId, @PathParam("name") String name) throws Exception {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            CriteriaMap criteriaMap_del = new CriteriaMap(CommonQuery.MEDICINE_BULK_DELETE);
            criteriaMap_del.addCriteria("patient", patientId);
            criteriaMap_del.addCriteria("name", name);
            repository.fireAdhoc(criteriaMap_del);
            return true;
        } catch (Exception e) {
        }
        return false;


    }


    @GET
    @Path("/getPreviousScore/{patientId}")
    @Produces("application/json")
    @Consumes("application/json")
    public Double getPreviousScore(@PathParam("patientId") String patientId) throws Exception {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            try {
                CriteriaMap map = new CriteriaMap(CommonQuery.SCORE_SELECT);
                List<QueryCondition> conditions = new ArrayList<>();
                QueryCondition condition = new QueryCondition("OWNER_ID",patientId);
                QueryCondition condition1 = new QueryCondition("QUIZ_TYPE","QUEST00000000000000000000000000000001");
                conditions.add(condition);
                conditions.add(condition1);
                map.addCriteria("searchQuery",conditions);
                map.addCriteria("sortOrder","ORDER BY CREATED_ON DESC");
                List<QuestionScore> scores = repository.find(map);
                Double score = 0.0;
                if(scores.size()>0){
                    score = Double.parseDouble(scores.get(0).getTotalScore());
                }

                return score;
            }catch (Exception e){
                throw new Exception(e);
            }

    }

    @GET
    @Path("/checkIfQuestionsFilled/{patientId}")
    @Produces("application/json")
    @Consumes("application/json")
    public boolean checkQuestionStatus(@PathParam("patientId") String patientId) throws Exception{
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try{
            if(patientId!=null){
                Patient patient = (Patient) repository.restore(ServiceLocator.getInstance().getReference(patientId));

                Date registeredDate = patient.getCreatedOn();
                Calendar regirsterdDateInstance = Calendar.getInstance();
                regirsterdDateInstance.setTime(registeredDate);
                // current date before 3 months
                regirsterdDateInstance.add(Calendar.DATE,90);


                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                Date currentDate = calendar.getTime();

                if(calendar.getTime().equals(regirsterdDateInstance.getTime()) || calendar.getTime().after(regirsterdDateInstance.getTime())){
                    CriteriaMap secondMap = new CriteriaMap(CommonQuery.EXISTING_QUESTIONS_SELECT);
                    secondMap.addCriteria("type","USRLVDIS00000000000000000000000000002");
                    secondMap.addCriteria("patientId",patientId);
                    List<Map> secondQuestions = repository.find(secondMap);

                    if(secondQuestions.size()>0){
                        return true;
                    }else {
                        return false;
                    }
                }else {
                    CriteriaMap secondMap = new CriteriaMap(CommonQuery.EXISTING_QUESTIONS_SELECT);
                    secondMap.addCriteria("type","USRLVDIS00000000000000000000000000001");
                    secondMap.addCriteria("patientId",patientId);
                    List<Map> secondQuestions = repository.find(secondMap);

                    if(secondQuestions.size()>0){
                        return true;
                    }else {
                        return false;
                    }
                }
            }else{
                throw new Exception("Please login again");
            }
        }catch (Exception e){
            throw new Exception(e);
        }
    }

}
