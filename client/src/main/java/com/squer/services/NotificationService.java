package com.squer.services;

import com.squer.platform.ServiceLocator;
import com.squer.platform.appframework.exception.SquerException;
import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.utility.CriteriaMap;
import com.squer.platform.persistence.utility.QueryCondition;
import com.squer.platform.registry.PlatformDelegates;
import com.squer.registry.CommonQuery;
import com.squer.sfe.api.common.entity.NotificationUtil;
import com.squer.sfe.api.common.entity.PushData;
import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.notification.PushNotificationBigPayload;
import javapns.notification.PushNotificationPayload;
import javapns.notification.PushedNotification;
import net.minidev.json.JSONObject;
import org.json.JSONException;

import javax.ws.rs.*;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mithila on 19/04/18-Apr-2018 at 1:11 AM
 */

@Path("/notification")
public class NotificationService {


    @POST
    @Path("/saveDeviceToken")
    @Produces("application/json")
    @Consumes("application/json")
    public boolean saveDeviceToken(PushDTO pushData) throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try {
        CriteriaMap map = new CriteriaMap(CommonQuery.PUSH_SELECT);
        List<QueryCondition> conditionList =new ArrayList<>();
        QueryCondition condition = new QueryCondition("OWNER",pushData.getOwner());
        QueryCondition condition1 = new QueryCondition("REGISTER_TOKEN",pushData.getRegisterToken());
        conditionList.add(condition);
        conditionList.add(condition1);
        map.addCriteria("searchQuery",conditionList);
            List<PushData> pushDataList = repository.find(map);
            if(pushDataList.size()==0){
                PushData p = new PushData();
                p.setOwner(ServiceLocator.getInstance().getReference(pushData.getOwner()));
                if(pushData.getRegisterToken()!=null){
                    p.setRegisterToken(pushData.getRegisterToken());
                }
                if(pushData.getIos()!=null){
                    p.setIos(pushData.getIos());
                }
                repository.create(p);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @POST
    @Path("/sendPushNotification/")
    @Produces("application/json")
    @Consumes("application/json")
    public boolean showNotificationsOnPublisher(PublisherDTO dto) throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try{
            CriteriaMap map = new CriteriaMap(CommonQuery.PUSH_SELECT);
            List<QueryCondition> conditions = new ArrayList<>();
            if(dto.getTo().equalsIgnoreCase("doctor")){
                QueryCondition condition = new QueryCondition("OWNER","like","DOCTR%");
                conditions.add(condition);
                map.addCriteria("searchQuery",conditions);
            }else if(dto.getTo().equalsIgnoreCase("patient")) {
                QueryCondition condition = new QueryCondition("OWNER","like","PATNT%");
                conditions.add(condition);
                map.addCriteria("searchQuery",conditions);
            }
            List<PushData> pushDataList = repository.find(map);
            if(pushDataList.size()>0){
                for(PushData p: pushDataList){
                    String regToken=p.getRegisterToken();
                    String authKey ="";
                    String picture = "";
                    String picurl ="";
                    if(dto.getPicture()!=null){
                        picture = dto.getPicture();
                    }
                    if(dto.getUrl()!=null){
                        picurl = dto.getUrl();
                    }
                    if(dto.getTo().equalsIgnoreCase("patient")){
                            if(p.getIos()!=null && p.getIos()) {
                                NotificationUtil.sendIosNotification(regToken, "PATNT00000000000000000000000000000001", "Knowledge", dto.getTitle(), null, dto.getPicture(), dto.getUrl());
                            }
                            else{
                                authKey = "AAAACFxGEP4:APA91bGXD_oXGRuX0IL8oABhmv7McZuu_NB28jTyZh_Sdi3lVUwu1F7IBkvfUs80AcGMcnOKC1KqSgULriubLCcweaRJ9eNwp3FqWV7x3fbiiVftkna8NcVX7KjYn1Py5No4TYOAU7E_";
                                String FMCurl = "https://fcm.googleapis.com/fcm/send";
                                URL url = new URL(FMCurl);
                                HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                                conn.setUseCaches(false);
                                conn.setDoInput(true);
                                conn.setDoOutput(true);

                                conn.setRequestMethod("POST");
                                conn.setRequestProperty("Authorization","key="+authKey);
                                conn.setRequestProperty("Content-Type","application/json, UTF-8");


                                //                    {
                                //                        "to":"eACXfZId6_w:APA91bFl5gEtWyouNcxSCxlVk_s8Sc9K9t6gG2F_WTY5lBngABe5kYi6q1uBw6yc614QL0XJUlM26KdzLqwavZJ16hdb98boADVFEjDBT3VJseTB2qkP9_-TLrHaRz26F4B-rB_8yom2",
                                //                            "data": {
                                //                        "title": "Diabetes Diagnosis",
                                //                                "style": "picture",
                                //                                "picture":"http://36.media.tumblr.com/c066cc2238103856c9ac506faa6f3bc2/tumblr_nmstmqtuo81tssmyno1_1280.jpg"
                                //                        "message": "Scrum: Daily touchbase @ 10am Please be on time so we can cover everything on the agenda.",
                                //                                "navigateTo":"Knowledge",
                                //                                "url":"http://publisher.sugrqb.in/demovideo/",
                                //                                "image":"www/img/icon.png"
                                //
                                //                    }
                                //                    }

                                JSONObject json = new JSONObject();
                                json.put("to",regToken.trim());

                                JSONObject notificationDetail = new JSONObject();
                                notificationDetail.put("title", dto.getTitle()); // Notification title
                                //notificationDetail.put(); // Notification body
                                notificationDetail.put("navigateTo","Knowledge");
                                notificationDetail.put("message",dto.getTitle());
                                notificationDetail.put("style", "picture");
                                notificationDetail.put("picture",dto.getPicture());
                                notificationDetail.put("url",dto.getUrl());
                                notificationDetail.put("image","www/img/icon.png");
                                json.put("data", notificationDetail);


                                //                JSONObject json = new JSONObject();
                                //                json.put("to",regToken.trim());
                                //                JSONObject info = new JSONObject();
                                //                info.put("title", "SugrQb"); // Notification title
                                //                info.put("body", "One of your patients has added new record!"); // Notification body
                                //                json.put("notification", info);

                                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                                wr.write(json.toString());
                                wr.flush();
                                conn.getInputStream();
                            }

                    }else if(dto.getTo().equalsIgnoreCase("doctor")){
                        if(p.getIos()!=null && p.getIos()){
                            if(regToken!=null){
                                NotificationUtil.sendIosNotification(regToken,"DOCTR00000000000000000000000000000001","Knowledge",dto.getTitle(),null,dto.getPicture(),dto.getUrl());
                            }
                        }else{
                            authKey = "AAAAq0bVEoM:APA91bGaLxLEwh7x4_vl_k_QWQwJYzTwVSoF-swwXMlgJd_F08c0cRw6aSas43Hx9W5Tt476nmbgf2MEcikWCDltO8khOc6dCMCuiXr6pPMM6_Ox3f1uU0cHgk6N4qCbAb0VEIA4sNA6";
                            String FMCurl = "https://fcm.googleapis.com/fcm/send";
                            URL url = new URL(FMCurl);
                            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                            conn.setUseCaches(false);
                            conn.setDoInput(true);
                            conn.setDoOutput(true);

                            conn.setRequestMethod("POST");
                            conn.setRequestProperty("Authorization","key="+authKey);
                            conn.setRequestProperty("Content-Type","application/json, UTF-8");


                            //                    {
                            //                        "to":"eACXfZId6_w:APA91bFl5gEtWyouNcxSCxlVk_s8Sc9K9t6gG2F_WTY5lBngABe5kYi6q1uBw6yc614QL0XJUlM26KdzLqwavZJ16hdb98boADVFEjDBT3VJseTB2qkP9_-TLrHaRz26F4B-rB_8yom2",
                            //                            "data": {
                            //                        "title": "Diabetes Diagnosis",
                            //                                "style": "picture",
                            //                                "picture":"http://36.media.tumblr.com/c066cc2238103856c9ac506faa6f3bc2/tumblr_nmstmqtuo81tssmyno1_1280.jpg"
                            //                        "message": "Scrum: Daily touchbase @ 10am Please be on time so we can cover everything on the agenda.",
                            //                                "navigateTo":"Knowledge",
                            //                                "url":"http://publisher.sugrqb.in/demovideo/",
                            //                                "image":"www/img/icon.png"
                            //
                            //                    }
                            //                    }

                            JSONObject json = new JSONObject();
                            json.put("to",regToken.trim());

                            JSONObject notificationDetail = new JSONObject();
                            notificationDetail.put("title", dto.getTitle()); // Notification title
                            //notificationDetail.put(); // Notification body
                            notificationDetail.put("navigateTo","Knowledge");
                            notificationDetail.put("message",dto.getTitle());
                            notificationDetail.put("style", "picture");
                            notificationDetail.put("picture",dto.getPicture());
                            notificationDetail.put("url",dto.getUrl());
                            notificationDetail.put("image","www/img/icon.png");
                            json.put("data", notificationDetail);


                            //                JSONObject json = new JSONObject();
                            //                json.put("to",regToken.trim());
                            //                JSONObject info = new JSONObject();
                            //                info.put("title", "SugrQb"); // Notification title
                            //                info.put("body", "One of your patients has added new record!"); // Notification body
                            //                json.put("notification", info);

                            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                            wr.write(json.toString());
                            wr.flush();
                            conn.getInputStream();
                        }

                    }



                }
            }

            return true;


        }catch (Exception e){
            throw new Exception(e);
        }

    }

    @POST
    @Path("/sendIosNotif/{senderId}/{owner}")
    @Produces("application/json")
    @Consumes("application/json")
    public boolean sendIosNotif(@PathParam("senderId") String senderId,@PathParam("owner") String owner) throws SquerException {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);


            JSONObject json = new JSONObject();

            json.put("to",senderId);

            JSONObject notificationDetail = new JSONObject();// Notification title
            notificationDetail.put("alert", "hello"); // Notification body
            notificationDetail.put("navigateTo","record");
            notificationDetail.put("content-available",1);
            notificationDetail.put("category","rich-apns");
            notificationDetail.put("mutable-content",1);
            json.put("aps", notificationDetail);
            json.put("media-url","https://i.imgur.com/t4WGJQx.jpg");

            String rawJSON = json.toJSONString();

            PushNotificationBigPayload payload = PushNotificationBigPayload.fromJSON(rawJSON);

            List<PushedNotification> NOTIFICATIONS=null;

            if(owner.startsWith("DOCTR")){

                    NOTIFICATIONS=Push.payload(payload, System.getProperty("config.path")+"/developmentPushCertificate.p12",
                            "welcome", false, senderId);
            }else if(owner.startsWith("PATNT")){
                if("prod".equalsIgnoreCase(System.getProperty("systemMode"))){
                    NOTIFICATIONS= Push.payload(payload, System.getProperty("config.path")+"/productionPushCertificate.p12",
                            "welcome", true, senderId);
                }else{
                    NOTIFICATIONS=Push.payload(payload, System.getProperty("config.path")+"/sugrqb-push-notification.p12",
                            "welcome", false, senderId);
                }
            }



            for (PushedNotification NOTIFICATION: NOTIFICATIONS)
                return  (NOTIFICATION.isSuccessful());
        }catch (Exception e){
            e.printStackTrace();
            throw new SquerException(e);
        }
        return false;
    }




}
