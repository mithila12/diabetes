package com.squer.registry;

import com.squer.platform.ServiceLocator;
import com.squer.platform.appframework.config.reference.UserLovReference;

/**
 * Created by mangesh on 11/09/15.
 */
public enum LeaveType {

    PRIVILEGE_LEAVE("USRLVLTYPE000000000000000000000000001"),
    CASUAL_LEAVE("USRLVLTYPE000000000000000000000000002"),
    SICK_LEAVE("USRLVLTYPE000000000000000000000000003");

    private UserLovReference leaveType;

    private LeaveType(String id){
        try{
            leaveType = (UserLovReference) ServiceLocator.getInstance().getReference(id);
        }
        catch(Exception e){
            throw new RuntimeException(e);
        }
    }

    public UserLovReference getReference(){
        return leaveType;
    }

}
