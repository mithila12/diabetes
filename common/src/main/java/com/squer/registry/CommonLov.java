package com.squer.registry;

import com.squer.platform.ServiceLocator;
import com.squer.platform.appframework.config.reference.SysLovReference;

/**
 * Created by ashutoshpavaskar on 07/11/14.
 */
public enum CommonLov {

    RESIDENCE_ADDRESS_TYPE ("SYSLVADD00000000000000000000000000001"),
    WORK_ADDRESS_TYPE ("SYSLVADD00000000000000000000000000002"),
    DOCTOR_STATUS_ACTIVE_APPROVED("SYSLVDOCSTA00000000000000000000000001"),
    DOCTOR_STATUS_INACTIVE_APPROVED("SYSLVDOCSTA00000000000000000000000002"),
    DOCTOR_STATUS_ACTIVE_UNAPPROVED("SYSLVDOCSTA00000000000000000000000003"),
    DOCTOR_STATUS_INACTIVE_UNAPPROVED("SYSLVDOCSTA00000000000000000000000004")
    ;



    private SysLovReference reference;

    private CommonLov(String id) {
        try {
            reference = (SysLovReference) ServiceLocator.getInstance().getReference(id);
        }catch(Exception e){
            throw new RuntimeException(e);
        }
    }

    public SysLovReference getReference(){
        return reference;
    }
}
