package com.squer.registry;

import com.squer.platform.impl.persistence.Delegates;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ashutoshpavaskar on 12/10/14.
 */
public class CommonDelegates implements Delegates{

        private static Map<String, Delegates> delegatesMap = new HashMap<>();

        public static final Delegates DoctorModelImpl = new CommonDelegates("DoctorModelImpl");
        public static final Delegates PatientModelImpl = new CommonDelegates("PatientModelImpl");
        public static final Delegates PatientReportsModelImpl = new CommonDelegates("PatientReportsModelImpl");
        public static final Delegates MessageModelImpl = new CommonDelegates("MessageModelImpl");
        public static final Delegates AnalyticsDataModelImpl = new CommonDelegates("AnalyticsDataModelImpl");


    private String id;
        private CommonDelegates(){

        }
        private CommonDelegates(String id){
            this.id = id;
            delegatesMap.put(id, this);
        }

        public String id(){
            return id;
        }

        public Delegates getDelegate(String id){
            return  delegatesMap.get(id);
        }

}
