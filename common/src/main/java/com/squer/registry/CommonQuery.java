package com.squer.registry;

import com.squer.platform.registry.FinderQuery;

/**
 * Created by ashutoshpavaskar on 08/10/14.
 */
public enum CommonQuery implements FinderQuery{
    ENTITY_CODE_INSERT("entitycode_insert"),
    ENTITY_CODE_SELECT("entitycode_select"),
    PATIENT_LIST_DOCTOR_SELECT("patientList_doctor_select"),
    USER_LOV_TYPE_SELECT("lov_select"),
    LOV_SELECT("lovtype_select"),
    INSERT_DOCUMENT("docmt_insert"),
    SELECT_DOCUMENT("docmt_select"),
    DELETE_DOCUMENT_BY_OWNER("docmt_byOwner_delete"),
    DELETE_DOCUMENT("docmt_delete"),
    DIABETES_PATIENT_INSERT("patnt_insert"),
    DIABETES_PATIENT_UPDATE("patnt_update"),
    DIABETES_PATIENT_DELETE("patnt_delete"),
    DIABETES_PATIENT_SELECT("patnt_select"),
    DIABETES_PATIENT_REPORT_SELECT("patnt_report_select"),
    DIABETES_DOCTOR_INSERT("doctr_insert"),
    DIABETES_DOCTOR_UPDATE("doctr_update"),
    DIABETES_DOCTOR_DELETE("doctr_delete"),
    DIABETES_DOCTOR_SELECT("doctr_select"),
    PATIENT_LIFESTYLE_SELECT("patls_select"),
    PATIENT_LIFESTYLE_INSERT("patls_insert"),
    PATIENT_LIFESTYLE_UPDATE("patls_update"),
    PATIENT_LIFESTYLE_DELETE("patls_delete"),
    PATIENT_REPORTS_INSERT("prepo_insert"),
    PATIENT_REPORTS_UPDATE("prepo_update"),
    PATIENT_REPORTS_DELETE("prepo_delete"),
    PATIENT_REPORTS_SELECT("prepo_select"),
    THRESHOLD_INSERT("thrsh_insert"),
    THRESHOLD_UPDATE("thrsh_update"),
    THRESHOLD_DELETE("thrsh_delete"),
    THRESHOLD_SELECT("thrsh_select"),
    MESSAGE_INSERT("messg_insert"),
    MESSAGE_UPDATE("messg_update"),
    MESSAGE_DELETE("messg_delete"),
    MESSAGE_SELECT("messg_select"),
    MEDICINE_SELECT("medic_select"),
    MEDICINE_INSERT("medic_insert"),
    MEDICINE_UPDATE("medic_update"),
    MEDICINE_DELETE("medic_delete"),
    MEDICINE_TIME_SELECT("mtmap_select"),
    QAMAP_SELECT("qamap_select"),
    PATIENT_THRESHOLD_TYPE_SELECT("patients_threshold_select"),
    GET_MESSAGES_FOR_PATIENT("get_messages_for_patient"),
    PATIENT_REG_COUNT_SELECT("patients_reg_count_select"),
    PATIENT_REG_TODAY_SELECT("patients_reg_today_select"),
    PATIENT_PROFILE_PERCENT_SELECT("profile_percent_count_select"),
    LIFESTYLE_QUESTION_SELECT("question_select"),
    ANSWER_SELECT("answer_select"),
    GRAPH_REPORT_COUNT_SELECT("graph_report_count_select"),
    OBESITY_REPORT_COUNT_SELECT("obesity_graph_count_select"),
    TARGET_DATA_PATIENT_SELECT("target_data_patient_select"),
    PATIENT_PROFILE_SELECT("patients_reports_select"),
    ALL_PATIENTS_REPORTS_SELECT("all_patients_report_select"),
    PATIENT_WEIGHT_SELECT("obesity_select"),
    ANALYTICS_OBESITY_SELECT("analytics_obesity_select"),
    ANALYTICS_REPORTS_SELECT("analyitics_patnt_report_select"),
    OTP_STORE_SELECT("otpst_select"),
    PUSH_INSERT("pushd_insert"),
    PUSH_SELECT("pushd_select"),
    PUSH_UPDATE("pushd_update"),
    OTP_STORE_UPDATE("otpst_update"),
    USER_ID_SELECT("user_id_select"),
    OWNER_ID_SELECT("owner_id_select"),
    PATIENTS_REPORT_DURATION("patients_reports_duration_select"),
    REGISTER_TOKEN_SELECT("register_token_select"),
    REGISTER_TOKEN_DOCTOR_SELECT("register_token_doctor_select"),
    MASTER_RECORD_SELECT("rcrdm_select"),
    ACTIVITY_DELETE("actvt_delete"),
    ACTIVITY_UPDATE("actvt_update"),
    ACTIVITY_SELECT("actvt_select"),
    CALORIE_MASTER_DATA("calmd_select"),
    QUESTIONS_SELECT("questions_select"),
    EXISTING_QUESTIONS_SELECT("existing_questions_select"),
    SCORE_DELETE("quesc_delete"),
    SCORE_UPDATE("quesc_update"),
    SCORE_SELECT("quesc_select"),
    SCORE_INSERT("quesc_insert"),
    SUB_QUESTION_SELECT("sub_question_select"),
    CALORIE_CONSUMED_SELECT("calories_consumed_select"),
    TARGET_REPORT_SELECT("target_report_select"),
    CALORIES_MAP_SELECT("calories_map_select"),
    TARGET_THRESHOLD_SELECT("target_threshold_select"),
    ALL_REPORT_ID_SELECT("all_reports_id_select"),
    CALORIE_BURNT_SELECT("calories_burnt_select"),
    PATIENT_QUESTION_BULK_DELETE("patls_bulk_delete"),
    CALORIES_BULK_DELETE("calories_bulk_delete"),
    ANALYTICS_DELETE("anada_delete"),
    ANALYTICS_UPDATE("anada_update"),
    ANALYTICS_SELECT("anada_select"),
    ANALYTICS_INSERT("anada_insert"),
    AVERAGE_PATIENT_RECORDS_SELECT("average_patient_records_select"),
    AVERAGE_PATIENT_RECORDS_DETAILS_SELECT("average_patient_records_details_select"),
    PERCENT_PATIENT_RECORDS_SELECT("percent_patient_records_select"),
    QUESTIONNAIRE_DETAILS_SELECT("questionnaire_details_select"),
    AVERAGE_PAGE_VIEW_SELECT("average_page_views_select"),
    QUESTIONNAIRE_FILLED_COUNT("questionnaire_count_select"),
    AVERAGE_PATIENTS_ENROLLED_BY_DOCTOR("avg_patients_enrolled_by_doctor"),
    AVERAGE_TIME_SPENT_ON_APP("average_time_spent_on_app"),
    TOTAL_ACTIVE_USERS_SELECT("total_active_users_select"),
    TOTAL_ACTIVE_USERS_DETAILS_SELECT("total_active_users_details_select"),
    AVERAGE_PAGE_VIEW_DETAILS_SELECT("average_page_views_details"),
    GENDERWISE_PATIENT_SELECT("genderwise_patient_select"),
    PENDING_REGISTRATION_COUNT_SELECT("pending_registrations_select"),
    ENROLL_COUNT_SELECT("enrol_select_count"),
    ENROLL_DATA_SELECT("enrol_select"),
    MEDICINE_BULK_DELETE("medic_bulk_delete");


    private String id;
    private String sortField;
    private String sortOrder;

    private CommonQuery(String id){
        this.id = id;
    }

    private  CommonQuery(String id,String sortField,String sortOrder){
        this.id=id;
        this.sortField=sortField;
        this.sortOrder=sortOrder;
    }

    public String id(){
        return id;
    }

    public String sortField() {
        return sortField;
    }

    public String sortOrder() {
        return sortOrder;
    }
}
