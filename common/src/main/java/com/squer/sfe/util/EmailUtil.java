package com.squer.sfe.util;

import com.squer.platform.ServiceLocator;
import com.squer.platform.api.config.SystemPropertyEntity;
import com.squer.platform.appframework.exception.SquerException;
import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.utility.CriteriaMap;
import com.squer.platform.registry.PlatformDelegates;
import com.squer.registry.BframeworkQuery;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.apache.commons.mail.HtmlEmail;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by smart user on 1/27/2017.
 */
    public class EmailUtil {


        public static boolean sendMail(String mailTo, String body,String subject) throws SquerException {
            try{

                /*
                Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
                HtmlEmail email = new HtmlEmail();
               // email.setTLS(true);
                //email.setSSL(true);

                CriteriaMap criteria = new CriteriaMap(BframeworkQuery.SYSPROPERTY_SELECT);
                criteria.addCriteria("name", "mail.hostname");
                SystemPropertyEntity property = (SystemPropertyEntity) repository.find(criteria).get(0);
                email.setHostName(property.getValue());

                criteria = new CriteriaMap(BframeworkQuery.SYSPROPERTY_SELECT);
                criteria.addCriteria("name", "mail.port");
                property = (SystemPropertyEntity) repository.find(criteria).get(0);
                email.setSmtpPort( Integer.parseInt(property.getValue()));
                email.addTo(mailTo);
                email.setSubject(subject);

                criteria = new CriteriaMap(BframeworkQuery.SYSPROPERTY_SELECT);
                criteria.addCriteria("name", "mail.frommail");
                SystemPropertyEntity frommail = (SystemPropertyEntity) repository.find(criteria).get(0);

                criteria = new CriteriaMap(BframeworkQuery.SYSPROPERTY_SELECT);
                criteria.addCriteria("name", "mail.friendlyname");
                SystemPropertyEntity friendlyname = (SystemPropertyEntity) repository.find(criteria).get(0);

                email.setFrom(frommail.getValue(),friendlyname.getValue());

                criteria = new CriteriaMap(BframeworkQuery.SYSPROPERTY_SELECT);
                criteria.addCriteria("name", "mail.password");
                property = (SystemPropertyEntity) repository.find(criteria).get(0);

                //email.setAuthentication(frommail.getValue(),property.getValue());

                Document doc = Jsoup.parse(body, "UTF-8");
                Elements images = doc.getElementsByTag("img");

                List<InternetAddress> mailList=new ArrayList();
                mailList.add(new javax.mail.internet.InternetAddress(frommail.getValue(),friendlyname.getValue()));
                email.setReplyTo(mailList);

                email.setHtmlMsg(doc.html());

                email.send();
                return true;
                */
                Client client = Client.create();
                client.addFilter(new HTTPBasicAuthFilter("api", "key-f47213677904f3faedf8d7d343d9e830"));
                WebResource webResource = client.resource("https://api.mailgun.net/v3/sugrqb.com/messages");
                MultivaluedMapImpl formData = new MultivaluedMapImpl();
                formData.add("from", "SugrQb Support <support@sugrqb.com>");
                formData.add("to", mailTo);
                formData.add("subject", subject);
                formData.add("text", body);
                webResource.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class,
                        formData);
                return true;
            }catch (Exception e){
                e.printStackTrace();
                return false;

            }
        }


    public static boolean sendAcknowledgement(String mailTo, String body,String subject) throws SquerException {
        try{
            /*
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            HtmlEmail email = new HtmlEmail();
            email.setTLS(true);
            email.setSSL(true);

            CriteriaMap criteria = new CriteriaMap(BframeworkQuery.SYSPROPERTY_SELECT);
            criteria.addCriteria("name", "mail.hostname");
            SystemPropertyEntity property = (SystemPropertyEntity) repository.find(criteria).get(0);
            email.setHostName(property.getValue());

            criteria = new CriteriaMap(BframeworkQuery.SYSPROPERTY_SELECT);
            criteria.addCriteria("name", "mail.port");
            property = (SystemPropertyEntity) repository.find(criteria).get(0);
            email.setSmtpPort( Integer.parseInt(property.getValue()));
            email.addTo(mailTo);
            email.setSubject(subject);

            criteria = new CriteriaMap(BframeworkQuery.SYSPROPERTY_SELECT);
            criteria.addCriteria("name", "mail.frommail");
            SystemPropertyEntity frommail = (SystemPropertyEntity) repository.find(criteria).get(0);

            criteria = new CriteriaMap(BframeworkQuery.SYSPROPERTY_SELECT);
            criteria.addCriteria("name", "mail.friendlyname");
            SystemPropertyEntity friendlyname = (SystemPropertyEntity) repository.find(criteria).get(0);

            email.setFrom(frommail.getValue(),friendlyname.getValue());

            criteria = new CriteriaMap(BframeworkQuery.SYSPROPERTY_SELECT);
            criteria.addCriteria("name", "mail.password");
            property = (SystemPropertyEntity) repository.find(criteria).get(0);


           // email.setAuthentication(frommail.getValue(),property.getValue());

            Document doc = Jsoup.parse(body, "UTF-8");
            Elements images = doc.getElementsByTag("img");

            List<InternetAddress> mailList=new ArrayList();
            mailList.add(new javax.mail.internet.InternetAddress(frommail.getValue(),friendlyname.getValue()));
            email.setReplyTo(mailList);

            email.setHtmlMsg(doc.html());

            email.send();
            */
            Client client = Client.create();
            client.addFilter(new HTTPBasicAuthFilter("api", "key-f47213677904f3faedf8d7d343d9e830"));
            WebResource webResource = client.resource("https://api.mailgun.net/v3/sugrqb.com/messages");
            MultivaluedMapImpl formData = new MultivaluedMapImpl();
            formData.add("from", "SugrQb Support <support@sugrqb.com>");
            formData.add("to", mailTo);
            formData.add("subject", subject);
            formData.add("text", body);
            webResource.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class,
                    formData);

            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;

        }
    }


}


