package com.squer.sfe.util;

import com.squer.platform.utils.SystemPropertyList;

/**
 * Created by smart user on 1/27/2017.
 */

    public enum  EmailPropertyEnum implements SystemPropertyList {

        MAIL_HOSTNAME("mail.hostname","sg2plcpnl0257.prod.sin2.secureserver.net"),
        MAIL_PORT("mail.port","465"),
        MAIL_FROMMAIL("mail.frommail","support@sugrqb.com"),
        MAIL_FRIENDLY_NAME("mail.friendlyname","Squer Products"),
        MAIL_PASSWORD("mail.password","Welcome@123"),
        CME_MAIL_TEXT_PATH("mail.baodytext","");


        private String propertyName;
        private String defaultValue;

        private EmailPropertyEnum(String property, String defaultValue){
            this.propertyName = property;
            this.defaultValue = defaultValue;
        }

        public String getPropertyName(){
            return  propertyName;
        }

        public String getDefaultValue(){
            return defaultValue;
        }
    }


