package com.squer.sfe.api.common.entity.reference;

import com.squer.platform.appframework.entity.SquerReferenceImpl;

/**
 * Created by shriram on 22/4/15.
 */
public class DocumentReference extends SquerReferenceImpl {

    public DocumentReference() {
        super(null);
    }

    public DocumentReference(String id) {
        super(id);
    }
}
