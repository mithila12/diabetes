package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.config.reference.UserLovReference;
import com.squer.platform.appframework.entity.AuditableEntity;
import com.squer.platform.appframework.entity.SquerReference;
import com.squer.platform.appframework.metadata.Persistence;
import com.squer.sfe.api.common.entity.reference.QuestionMasterReference;
import com.squer.sfe.api.common.entity.reference.QuestionnaireReference;

/**
 * Created by mithila on 14/06/17.
 */
@Persistence(prefix = "QUESC", module = "common")
public class QuestionScore extends AuditableEntity {
    private String totalScore;
    private QuestionnaireReference typeOfQuiz;
    private String month;
    private QuestionMasterReference question;
    private SquerReference owner;


    public QuestionMasterReference getQuestion() {
        return question;
    }

    public void setQuestion(QuestionMasterReference question) {
        this.question = question;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(String totalScore) {
        this.totalScore = totalScore;
    }

    public QuestionnaireReference getTypeOfQuiz() {
        return typeOfQuiz;
    }

    public void setTypeOfQuiz(QuestionnaireReference typeOfQuiz) {
        this.typeOfQuiz = typeOfQuiz;
    }

    public SquerReference getOwner() {
        return owner;
    }

    public void setOwner(SquerReference owner) {
        this.owner = owner;
    }
}
