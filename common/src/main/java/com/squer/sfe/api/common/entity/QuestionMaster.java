package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.config.reference.UserLovReference;
import com.squer.platform.appframework.entity.AuditableEntity;
import com.squer.platform.appframework.metadata.Persistence;
import com.squer.sfe.api.common.entity.reference.QuestionnaireReference;

/**
 * Created by mithila on 01/06/17.
 */
@Persistence(prefix="QUESM", module="common")
public class QuestionMaster extends AuditableEntity {

    private String question;
    private int order;
    private QuestionnaireReference questionType;
    private String answer;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public QuestionnaireReference getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QuestionnaireReference questionType) {
        this.questionType = questionType;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
