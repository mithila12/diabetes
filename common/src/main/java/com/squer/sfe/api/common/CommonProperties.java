package com.squer.sfe.api.common;

import com.squer.platform.utils.SystemPropertyList;

/**
 * Created by nileshborole on 27/02/15.
 */
public enum CommonProperties implements SystemPropertyList {

    SYSTEM_CSV_REPOSITORY_PATH("system.csv.repository.path",null),
    SYSTEM_MYSQL_DATABASE_PATH("system.mysql.database.path", "/var/lib/mysql/");


    private String propertyName;
    private String defaultValue;

    private CommonProperties(String property, String defaultValue){
        this.propertyName = property;
        this.defaultValue = defaultValue;
    }


    @Override
    public String getPropertyName() {
        return this.propertyName;
    }

    @Override
    public String getDefaultValue() {
        return this.defaultValue;
    }
}
