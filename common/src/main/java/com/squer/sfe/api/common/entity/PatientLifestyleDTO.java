package com.squer.sfe.api.common.entity;

import java.util.List;

/**
 * Created by user on 1/17/2017.
 */
public class PatientLifestyleDTO {
    private  List<PatientLifestyle> list;

    public List<PatientLifestyle> getList() {
        return list;
    }

    public void setList(List<PatientLifestyle> list) {
        this.list = list;
    }
}
