package com.squer.sfe.api.common.entity.reference;

/**
 * Created by mithila on 23/01/17.
 */
public class MedTimeMapReference extends PartyReference {
    public MedTimeMapReference () {
        super();
    }

    public MedTimeMapReference (String id,String name) {
        super(id, name);
    }
}
