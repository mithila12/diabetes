package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.config.reference.UserLovReference;
import com.squer.platform.appframework.entity.AuditableEntity;
import com.squer.platform.appframework.metadata.Persistence;
import com.squer.sfe.api.common.entity.reference.PatientReference;

/**
 * Created by Squer on 11/17/2016.
 */
@Persistence(prefix="PATLS", module="common")
public class PatientLifestyle extends AuditableEntity {
    private PatientReference patient;
    private UserLovReference lifeStyle;
    private UserLovReference answerId;
    private String answer;

    public PatientReference getPatient() {
        return patient;
    }

    public void setPatient(PatientReference patient) {
        this.patient = patient;
    }

    public UserLovReference getLifeStyle() {
        return lifeStyle;
    }

    public void setLifeStyle(UserLovReference lifeStyle) {
        this.lifeStyle = lifeStyle;
    }

    public UserLovReference getAnswerId() {
        return answerId;
    }

    public void setAnswerId(UserLovReference answerId) {
        this.answerId = answerId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
