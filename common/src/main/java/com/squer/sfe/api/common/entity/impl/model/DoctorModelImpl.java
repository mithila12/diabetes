package com.squer.sfe.api.common.entity.impl.model;

import com.squer.platform.ServiceLocator;
import com.squer.platform.appframework.auth.UserEntity;
import com.squer.platform.appframework.auth.reference.SecurityRoleReference;
import com.squer.platform.appframework.config.reference.SysLovReference;
import com.squer.platform.appframework.entity.SquerEntity;
import com.squer.platform.appframework.entity.SquerReference;
import com.squer.platform.appframework.exception.SquerException;
import com.squer.platform.appframework.metadata.Persistence;
import com.squer.platform.impl.persistence.BaseModelImpl;
import com.squer.platform.log.PlatformLoggerEnum;
import com.squer.platform.log.SquerLogger;
import com.squer.platform.persistence.Repository;
import com.squer.platform.registry.PlatformDelegates;
import com.squer.platform.utils.StringUtil;
import com.squer.platform.utils.SystemPropertyEnum;
import com.squer.platform.utils.SystemUtil;
import com.squer.sfe.api.common.entity.Doctor;
import com.squer.sfe.api.common.entity.DocumentUtil;
import com.squer.sfe.api.common.entity.LoginUtil;
import com.squer.sfe.api.common.entity.Patient;
import com.squer.sfe.api.common.entity.reference.DoctorReference;
import com.squer.sfe.api.common.impl.typehandler.CommonUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Squer on 11/22/2016.
 */
public class DoctorModelImpl extends BaseModelImpl {
    private static SquerLogger logger = SquerLogger.getLogger(PlatformLoggerEnum.Persistence);

    public SquerEntity preCreate(Persistence persistence, SquerEntity entity) throws SquerException {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            Doctor doctor = (Doctor) entity;
            setName(doctor);
            String docCode = CommonUtil.generateEntityCode(entity, "D");
            doctor.setStatus((SysLovReference) ServiceLocator.getInstance().getReference("SYSLV00000000000000000000000000000001"));
            doctor.setCode(docCode);
            return entity;
        } catch (Exception e) {
            throw new SquerException(e);
        }
    }

    public SquerReference postCreate(Persistence persistence, SquerEntity entity) throws SquerException {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            Doctor doctor = (Doctor) entity;
            DoctorReference reference = (DoctorReference) ServiceLocator.getInstance().getReference(doctor.getId());

            LoginUtil.createUser(repository, reference, doctor.getCode(), doctor.getPassword(),
                    (SecurityRoleReference) ServiceLocator.getInstance().getReference("SROLE00000023d6ee570149c6b4e03a007111"));

            return super.postCreate(persistence, entity);
        } catch (Exception e) {
            logger.fatal(e);
            throw new SquerException(e);
        }
    }

    public SquerEntity preUpdate(Persistence persistence, SquerEntity entity) throws SquerException {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            Doctor doctor = (Doctor) entity;
            setName(doctor);
            //doctor.setStatus((SysLovReference) ServiceLocator.getInstance().getReference("SYSLV00000000000000000000000000000001"));
            doctor.setCode(doctor.getCode());
            return entity;
        } catch (Exception e) {
            throw new SquerException(e);
        }
    }

    public SquerReference postUpdate(Persistence persistence, SquerEntity entity) throws SquerException{
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            Doctor doctor = (Doctor) entity;
            if(doctor.getContent()!=null){
                DocumentUtil.createDocument(doctor.getId(),"SYSLVIMGTP000000000000000000000000001",doctor.getContent());
                //CriteriaMap criteria = new CriteriaMap(CommonQuery.INSERT_DOCUMENT);
            }
            return super.postUpdate(persistence, entity);
        }catch (Exception e){
            logger.fatal(e);
            throw new SquerException(e);
        }
    }

    private void setName(Doctor doctor) throws SquerException {
        String nameFormat = SystemUtil.getPropertyValue(SystemPropertyEnum.NAME_FORMAT);
        if (nameFormat.equalsIgnoreCase("fname-lname"))
            doctor.setName((StringUtil.isNullOrEmpty(doctor.getFirstName()) ? "" : doctor.getFirstName() + " " +
                    (StringUtil.isNullOrEmpty(doctor.getMiddleName()) ? "" : doctor.getMiddleName()) + " " +
                    (StringUtil.isNullOrEmpty(doctor.getLastName()) ? "" : doctor.getLastName())).trim());
        else
            doctor.setName((StringUtil.isNullOrEmpty(doctor.getLastName()) ? "" : doctor.getLastName() + " " +
                    (StringUtil.isNullOrEmpty(doctor.getFirstName()) ? "" : doctor.getFirstName()) + " " +
                    (StringUtil.isNullOrEmpty(doctor.getMiddleName()) ? "" : doctor.getMiddleName())).trim());
    }
}
