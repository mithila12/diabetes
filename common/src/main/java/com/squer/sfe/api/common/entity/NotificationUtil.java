package com.squer.sfe.api.common.entity;

import com.squer.platform.ServiceLocator;
import com.squer.platform.appframework.exception.SquerException;
import com.squer.platform.persistence.Repository;
import com.squer.platform.registry.PlatformDelegates;
import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.notification.PushNotificationBigPayload;
import javapns.notification.PushedNotification;
import net.minidev.json.JSONObject;
import org.apache.regexp.RE;
import org.json.JSONException;

import javax.ws.rs.PathParam;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by mithila on 25/05/18-May-2018 at 12:44 PM
 */
public class NotificationUtil {

    public static boolean sendIosNotification(String senderId,String owner,String navigateTo,String message,String patientId,String picture,String url) throws JSONException, CommunicationException, KeystoreException, SquerException {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        try {
            JSONObject json = new JSONObject();

            json.put("to",senderId);
            if(navigateTo.equalsIgnoreCase("Messages")){
                JSONObject notificationDetail = new JSONObject();// Notification title
                notificationDetail.put("alert", message); // Notification body
                notificationDetail.put("navigateTo",navigateTo);
                notificationDetail.put("content-available",1);
                notificationDetail.put("category","rich-apns");
                notificationDetail.put("mutable-content",1);
                json.put("aps", notificationDetail);
                json.put("media-url","https://i.imgur.com/t4WGJQx.jpg");
            }else if(navigateTo.equalsIgnoreCase("record")){
                if(patientId!=null){
                    Patient patient = (Patient) repository.restore(ServiceLocator.getInstance().getReference(patientId));
                    JSONObject notificationDetail = new JSONObject();// Notification title
                    notificationDetail.put("alert", message); // Notification body
                    notificationDetail.put("navigateTo",navigateTo);
                    notificationDetail.put("content-available",1);
                    notificationDetail.put("patientId", patient.getId());
                    notificationDetail.put("patientName", patient.getName());
                    notificationDetail.put("age", patient.getAge());
                    notificationDetail.put("gender", patient.getGender().getId());
                    notificationDetail.put("category","rich-apns");
                    notificationDetail.put("mutable-content",1);
                    json.put("aps", notificationDetail);
                    json.put("media-url","https://i.imgur.com/t4WGJQx.jpg");
                }

            }else if(navigateTo.equalsIgnoreCase("Knowledge")){
                JSONObject notificationDetail = new JSONObject();// Notification title
                notificationDetail.put("alert", message); // Notification body
                notificationDetail.put("navigateTo",navigateTo);
                notificationDetail.put("content-available",1);
                notificationDetail.put("category","rich-apns");
                notificationDetail.put("mutable-content",1);
                notificationDetail.put("style", "picture");
                notificationDetail.put("picture",picture);
                notificationDetail.put("url",url);
                json.put("aps", notificationDetail);
                json.put("media-url","https://i.imgur.com/t4WGJQx.jpg");
            }

            String rawJSON = json.toJSONString();

            PushNotificationBigPayload payload = PushNotificationBigPayload.fromJSON(rawJSON);

            List<PushedNotification> NOTIFICATIONS=null;

            if(owner.startsWith("DOCTR")){
//                if("prod".equalsIgnoreCase(System.getProperty("systemMode"))){
                   // NOTIFICATIONS= Push.payload(payload, System.getProperty("config.path")+"/sugrqbPlusProdPushNotif.p12",
                   //         "welcome", true, senderId);
//                }else{
                    NOTIFICATIONS=Push.payload(payload, System.getProperty("config.path")+"/developmentPushCertificate.p12",
                            "welcome", false, senderId);
//                }
            }else if(owner.startsWith("PATNT")){
                //if("prod".equalsIgnoreCase(System.getProperty("systemMode"))){
                    NOTIFICATIONS= Push.payload(payload, System.getProperty("config.path")+"/sugrqb-productionPushNotif.p12",
                            "welcome", true, senderId);
//                }else{
//                    NOTIFICATIONS=Push.payload(payload, System.getProperty("config.path")+"/sugrqb-push-notification.p12",
//                            "welcome", false, senderId);
//                }
            }



            for (PushedNotification NOTIFICATION: NOTIFICATIONS)
                return  (NOTIFICATION.isSuccessful());

            return true;
        }catch (Exception e){
            e.printStackTrace();
            throw new SquerException(e);
        }

    }


    public static boolean sendAndroidNotification(String senderId, String owner, String navigateTo, Boolean isImage,String message,String patientId) throws Exception {
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);

        String authKey = "";
        if(navigateTo.equalsIgnoreCase("Messages")){
            authKey = "AAAACFxGEP4:APA91bGXD_oXGRuX0IL8oABhmv7McZuu_NB28jTyZh_Sdi3lVUwu1F7IBkvfUs80AcGMcnOKC1KqSgULriubLCcweaRJ9eNwp3FqWV7x3fbiiVftkna8NcVX7KjYn1Py5No4TYOAU7E_";
        }else if(navigateTo.equalsIgnoreCase("record")) {
            authKey = "AAAAq0bVEoM:APA91bGaLxLEwh7x4_vl_k_QWQwJYzTwVSoF-swwXMlgJd_F08c0cRw6aSas43Hx9W5Tt476nmbgf2MEcikWCDltO8khOc6dCMCuiXr6pPMM6_Ox3f1uU0cHgk6N4qCbAb0VEIA4sNA6";
        }


        String FMCurl = "https://fcm.googleapis.com/fcm/send";
        URL url = new URL(FMCurl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setUseCaches(false);
        conn.setDoInput(true);
        conn.setDoOutput(true);

        conn.setRequestMethod("POST");
        conn.setRequestProperty("Authorization","key="+authKey);
        conn.setRequestProperty("Content-Type","application/json, UTF-8");

        JSONObject json = new JSONObject();
        json.put("to",senderId.trim());
            if(navigateTo.equalsIgnoreCase("Messages")){
                JSONObject notificationDetail = new JSONObject();
                notificationDetail.put("title", "SugrQb"); // Notification title
                notificationDetail.put("message", message); // Notification body
                notificationDetail.put("navigateTo",navigateTo);
                if(isImage){
                    notificationDetail.put("style","picture");
                }
                json.put("data", notificationDetail);
            }else if(navigateTo.equalsIgnoreCase("record")) {
                if (patientId != null) {
                    Patient patient = (Patient) repository.restore(ServiceLocator.getInstance().getReference(patientId));
                    JSONObject info = new JSONObject();
                    info.put("title", "SugrQb+"); // Notification title
                    info.put("message", message);// Notification body
                    info.put("navigateTo", "record");
                    info.put("patientId", patient.getId());
                    info.put("patientName", patient.getName());
                    info.put("age", patient.getAge());
                    info.put("gender", patient.getGender().getId());
                    //info.put("picture",Paths.get(System.getProperty("config.path")+"/documents/"+ServiceLocator.getInstance().getPrincipal().getTenantId() + "/" + document.getOwner().getId() + "." + document.getName()));
                    json.put("data", info);
                }
            }

        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
        wr.write(json.toString());
        wr.flush();
        conn.getInputStream();
        return true;
    }

}
