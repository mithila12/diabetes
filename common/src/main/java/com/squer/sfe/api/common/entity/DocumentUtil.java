package com.squer.sfe.api.common.entity;

import com.squer.platform.ServiceLocator;
import com.squer.platform.appframework.config.reference.SysLovReference;
import com.squer.platform.appframework.entity.SquerReference;
import com.squer.platform.appframework.exception.SquerException;
import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.utility.CriteriaMap;
import com.squer.platform.registry.PlatformDelegates;
import com.squer.platform.utils.CommonUtils;
import com.squer.registry.CommonQuery;
import com.squer.sfe.api.common.entity.reference.DocumentReference;
import org.apache.commons.codec.binary.Base64;

import javax.ws.rs.PathParam;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * Created by user on 1/10/2017.
 */
public class DocumentUtil {
    public static DocumentReference createDocument(String owner, String type, String msgContent) throws Exception{

        Repository repository = (Repository)ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        final String DOCUMENT_PATH = System.getProperty("config.path")+"/documents/"+ServiceLocator.getInstance().getPrincipal().getTenantId();
        String name = msgContent.substring(msgContent.indexOf('/')+1,msgContent.indexOf(';'));
        String imageContent = msgContent.substring(msgContent.indexOf(';') + "base64,".length());
        byte[] content = Base64.decodeBase64(imageContent);

        Document documentEntity  = new Document();
        documentEntity.setName(name);
        documentEntity.setOwner(ServiceLocator.getInstance().getReference(owner));
        documentEntity.setType(ServiceLocator.getInstance().getReference(type));

       // documentEntity.setName(name);
        DocumentReference reference = (DocumentReference) repository.create(documentEntity);
        OutputStream os = null;
        try {
            os = new FileOutputStream(DOCUMENT_PATH + "/" + owner + "." + documentEntity.getName());
            os.write(content);
        }finally {
            try{
                os.close();
                os.flush();
            }catch (Exception e){
                throw new SquerException(e);
            }
        }
        return reference;

    }

    public static boolean deleteDocument(String owner) throws Exception {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            CriteriaMap criteriaMap_doc=new CriteriaMap(CommonQuery.SELECT_DOCUMENT);
            criteriaMap_doc.addCriteria("owner",owner);

            List<Document> documents=repository.find(criteriaMap_doc);

            if(!CommonUtils.isNullOrEmpty(documents)){
                CriteriaMap criteriaMap_delDoc=new CriteriaMap(CommonQuery.DELETE_DOCUMENT_BY_OWNER);
                criteriaMap_delDoc.addCriteria("owner",owner);
                repository.fireAdhoc(criteriaMap_delDoc);

                for(Document d:documents){
                    File file=new File(System.getProperty("config.path")+"/documents/"+ServiceLocator.getInstance().getPrincipal().getTenantId()+ "/" + d.getOwner().getId() + "." +d.getName());
                    file.delete();
                }
            }
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
}
