package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.auth.reference.UserEntityReference;
import com.squer.platform.appframework.entity.AuditableEntity;
import com.squer.platform.appframework.entity.SquerReference;
import com.squer.platform.appframework.metadata.Persistence;

/**
 * Created by mithila on 12/04/17.
 */
@Persistence(prefix="OTPST", module="common")
public class OtpStore extends AuditableEntity {

    private String otp;
    private SquerReference user;

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public SquerReference getUser() {
        return user;
    }

    public void setUser(SquerReference user) {
        this.user = user;
    }



}
