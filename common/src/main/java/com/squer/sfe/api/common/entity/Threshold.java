package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.config.reference.UserLovReference;
import com.squer.platform.appframework.entity.AuditableEntity;
import com.squer.platform.appframework.metadata.Persistence;
import com.squer.sfe.api.common.entity.reference.PatientReference;

/**
 * Created by user on 12/19/2016.
 */
@Persistence(prefix="THRSH", module="common")
public class Threshold extends AuditableEntity{
    private PatientReference patient;
    private UserLovReference metric;
    private Float value;
    private Float valueMin;

    public PatientReference getPatient() {
        return patient;
    }

    public void setPatient(PatientReference patient) {
        this.patient = patient;
    }

    public UserLovReference getMetric() {
        return metric;
    }

    public void setMetric(UserLovReference metric) {
        this.metric = metric;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public Float getValueMin() {
        return valueMin;
    }

    public void setValueMin(Float valueMin) {
        this.valueMin = valueMin;
    }
}
