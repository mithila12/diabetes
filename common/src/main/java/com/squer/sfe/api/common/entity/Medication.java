package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.config.reference.UserLovReference;
import com.squer.platform.appframework.entity.AuditableEntity;
import com.squer.platform.appframework.metadata.Persistence;
import com.squer.sfe.api.common.entity.reference.PatientReference;

/**
 * Created by user on 12/28/2016.
 */
@Persistence(prefix="MEDIC", module="common")
public class Medication extends AuditableEntity{
    private String name;
    private PatientReference patient;
    private UserLovReference route;
    private String dose;
    private UserLovReference doseTime;
    private String since;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PatientReference getPatient() {
        return patient;
    }

    public void setPatient(PatientReference patient) {
        this.patient = patient;
    }

    public UserLovReference getRoute() {
        return route;
    }

    public void setRoute(UserLovReference route) {
        this.route = route;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public UserLovReference getDoseTime() {
        return doseTime;
    }

    public void setDoseTime(UserLovReference doseTime) {
        this.doseTime = doseTime;
    }

    public String getSince() {
        return since;
    }

    public void setSince(String since) {
        this.since = since;
    }
}
