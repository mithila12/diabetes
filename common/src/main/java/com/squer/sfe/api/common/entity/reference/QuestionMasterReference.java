package com.squer.sfe.api.common.entity.reference;

import com.squer.platform.appframework.entity.SquerReferenceImpl;

/**
 * Created by mithila on 01/06/17.
 */
public class QuestionMasterReference extends SquerReferenceImpl {

    public QuestionMasterReference(){super(null);}

    public QuestionMasterReference(String id){ super(id);}
}
