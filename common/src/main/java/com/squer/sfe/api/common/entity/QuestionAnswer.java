package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.config.reference.UserLovReference;
import com.squer.platform.appframework.entity.AuditableEntity;
import com.squer.platform.appframework.metadata.Persistence;
import com.squer.sfe.api.common.entity.reference.PatientReference;

/**
 * Created by user on 1/3/2017.
 */
@Persistence(prefix="QAMAP", module="common")
public class QuestionAnswer extends AuditableEntity{

    private UserLovReference question;
    private String answer;

    public UserLovReference getQuestion() {
        return question;
    }

    public void setQuestion(UserLovReference question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
