package com.squer.sfe.api.common.entity.reference;

import com.squer.platform.appframework.entity.SquerReferenceImpl;

/**
 * Created by Squer on 11/18/2016.
 */
public class PatientReportsReference extends SquerReferenceImpl {
    public PatientReportsReference () {
        super(null);
    }

    public PatientReportsReference (String id) {
        super(id);
    }
}
