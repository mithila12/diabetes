package com.squer.sfe.api.common.impl.typehandler;

import com.squer.platform.ServiceLocator;
import com.squer.platform.appframework.entity.SquerEntity;
import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.utility.CriteriaMap;
import com.squer.platform.registry.PlatformDelegates;
import com.squer.platform.utils.CommonUtils;
import com.squer.platform.utils.StringUtil;
import com.squer.platform.utils.SystemPropertyEnum;
import com.squer.platform.utils.SystemUtil;
import com.squer.registry.CommonQuery;
import org.apache.commons.lang3.text.WordUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ashutoshpavaskar on 07/11/14.
 */
public class CommonUtil {
    public static String getName(String firstName, String middleName, String lastName){

        String nameFormat = null;
        try {
            nameFormat = SystemUtil.getPropertyValue(SystemPropertyEnum.NAME_FORMAT);
        }catch (Exception e){
            nameFormat= "fname-lname";
        }
        String name = "";
        if(nameFormat.equals("fname-lname")) {
            if(!StringUtil.isNullOrEmpty(firstName))
                name = firstName;
            if(!StringUtil.isNullOrEmpty(middleName))
                name = name + " " + middleName;
            if(!StringUtil.isNullOrEmpty(lastName))
                name = name +  " " + lastName;
        }else{
            if(!StringUtil.isNullOrEmpty(lastName))
                name = lastName;
            if(!StringUtil.isNullOrEmpty(firstName))
                name = name +  " " + firstName;
            if(!StringUtil.isNullOrEmpty(middleName))
                name = name + " " + middleName;

        }
        return WordUtils.capitalizeFully(name," ".toCharArray());
    }


    public static String generateEntityCode(SquerEntity entity, String prefix) throws Exception{
        DecimalFormat decimalFormat = new DecimalFormat("0000001");
        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        CriteriaMap criteriaMap = new CriteriaMap(CommonQuery.ENTITY_CODE_INSERT);
        criteriaMap.addCriteria("id", entity.getId());
        repository.fireAdhoc(criteriaMap);
        criteriaMap = new CriteriaMap(CommonQuery.ENTITY_CODE_SELECT);
        criteriaMap.addCriteria("id", entity.getId());
        List<Integer> numbers = repository.find(criteriaMap);
        if(CommonUtils.isNullOrEmpty(numbers))
            throw new Exception("Error creating patient code. Please try again");
        String code = decimalFormat.format(numbers.get(0));
        return prefix+code;
    }

    public static String generateOtp() throws Exception{
        List<Integer> numbers = new ArrayList<>();
        for(int i = 0; i < 10; i++){
            numbers.add(i);
        }

        Collections.shuffle(numbers);

        String result = "";
        for(int i = 0; i < 4; i++){
            result += numbers.get(i).toString();
        }
        System.out.println(result);

        return result;
    }

}
