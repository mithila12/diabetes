package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.config.reference.UserLovReference;
import com.squer.platform.appframework.entity.AuditableEntity;
import com.squer.platform.appframework.metadata.Persistence;
import com.squer.sfe.api.common.entity.reference.PatientReference;

import java.util.Date;

/**
 * Created by Squer on 11/18/2016.
 */
@Persistence(prefix="PREPO", module="common", model = "CommonDelegates.PatientReportsModelImpl")
public class PatientReports extends AuditableEntity {
    private PatientReference patient;
    private UserLovReference reportType;
    private String content;
    private String reportName;
    private String reportOtherText;
    private String reportValue;
    private Date dateRecorded;

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public PatientReference getPatient() {
        return patient;
    }

    public void setPatient(PatientReference patient) {
        this.patient = patient;
    }

    public UserLovReference getReportType() {
        return reportType;
    }

    public void setReportType(UserLovReference reportType) {
        this.reportType = reportType;
    }

    public String getReportValue() {
        return reportValue;
    }

    public void setReportValue(String reportValue) {
        this.reportValue = reportValue;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getReportOtherText() {
        return reportOtherText;
    }

    public void setReportOtherText(String reportOtherText) {
        this.reportOtherText = reportOtherText;
    }

    public Date getDateRecorded() {
        return dateRecorded;
    }

    public void setDateRecorded(Date dateRecorded) {
        this.dateRecorded = dateRecorded;
    }
}
