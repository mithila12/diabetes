package com.squer.sfe.api.common.entity.reference;

public class DoctorReference extends PartyReference {
    public DoctorReference () {
        super();
    }

    public DoctorReference (String id, String name) {
        super(id, name);
    }
}
