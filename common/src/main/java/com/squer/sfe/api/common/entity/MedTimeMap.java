package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.config.reference.UserLovReference;
import com.squer.platform.appframework.entity.AuditableEntity;
import com.squer.platform.appframework.metadata.Persistence;

/**
 * Created by mithila on 22/01/17.
 */
@Persistence(prefix = "MTMAP", module = "common")
public class MedTimeMap extends AuditableEntity{
    private UserLovReference doseTime;
    private String time;

    public UserLovReference getDoseTime() {
        return doseTime;
    }

    public void setDoseTime(UserLovReference doseTime) {
        this.doseTime = doseTime;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
