package com.squer.sfe.api.common.entity.reference;

import com.squer.platform.appframework.entity.SquerReferenceImpl;

/**
 * Created by mithila on 14/06/17.
 */
public class QuestionScoreReference extends SquerReferenceImpl{
    public QuestionScoreReference(){super(null);}

    public QuestionScoreReference(String id){super(id);}

}
