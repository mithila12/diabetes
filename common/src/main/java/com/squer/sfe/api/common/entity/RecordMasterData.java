package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.config.reference.UserLovReference;
import com.squer.platform.appframework.entity.AuditableEntity;
import com.squer.platform.appframework.metadata.Persistence;


/**
 * Created by mithila on 13/05/17.
 */
@Persistence(prefix="RCRDM", module="common")
public class RecordMasterData extends AuditableEntity{
    private UserLovReference type;
    private Float min;
    private Float max;
    private Float normalMin;
    private Float normalMax;
    private String errorMsg;

    public UserLovReference getType() {
        return type;
    }

    public void setType(UserLovReference type) {
        this.type = type;
    }

    public Float getMin() {
        return min;
    }

    public void setMin(Float min) {
        this.min = min;
    }

    public Float getMax() {
        return max;
    }

    public void setMax(Float max) {
        this.max = max;
    }

    public Float getNormalMin() {
        return normalMin;
    }

    public void setNormalMin(Float normalMin) {
        this.normalMin = normalMin;
    }

    public Float getNormalMax() {
        return normalMax;
    }

    public void setNormalMax(Float normalMax) {
        this.normalMax = normalMax;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
