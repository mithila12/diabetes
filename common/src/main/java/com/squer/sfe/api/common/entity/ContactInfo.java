package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.config.reference.UserLovReference;
import com.squer.platform.appframework.entity.SquerEntity;
import com.squer.platform.appframework.metadata.Persistence;
import com.squer.sfe.api.common.entity.reference.PartyReference;

@Persistence(prefix="CONTT", module="common" )
public class ContactInfo implements SquerEntity{

    private String id;
    private UserLovReference type;
    private String value;
    private String IS_PRIMARY;
    private PartyReference owner;

    public void setId (String id){
        this.id = id;
            }

    public String getId (){
        return this.id;
    }
    public void setType (UserLovReference type){
        this.type = type;
            }

    public UserLovReference getType (){
        return this.type;
    }
    public void setValue (String value){
        this.value = value;
            }

    public String getValue (){
        return this.value;
    }
    public void setIS_PRIMARY (String IS_PRIMARY){
        this.IS_PRIMARY = IS_PRIMARY;
            }

    public String getIS_PRIMARY (){
        return this.IS_PRIMARY;
    }

    public PartyReference getOwner() {
        return owner;
    }

    public void setOwner(PartyReference owner) {
        this.owner = owner;
    }
}