package com.squer.sfe.api.common.entity.reference;

import com.squer.platform.appframework.entity.SquerReferenceImpl;

/**
 * Created by mithila on 18/06/18-Jun-2018 at 2:21 PM
 */
public class EnrollPatientsDataReference extends SquerReferenceImpl{

    public EnrollPatientsDataReference(){super(null);}

    public EnrollPatientsDataReference(String id){super(id);}
}
