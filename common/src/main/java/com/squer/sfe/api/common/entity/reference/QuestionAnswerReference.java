package com.squer.sfe.api.common.entity.reference;

/**
 * Created by user on 1/3/2017.
 */
public class QuestionAnswerReference extends PartyReference{
    public QuestionAnswerReference () {
        super();
    }

    public QuestionAnswerReference (String id,String name) {
        super(id, name);
    }
}
