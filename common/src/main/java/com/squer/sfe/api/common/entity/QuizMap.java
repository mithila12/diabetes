package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.auth.reference.UserEntityReference;
import com.squer.platform.appframework.config.reference.UserLovReference;
import com.squer.platform.appframework.entity.AuditableEntity;
import com.squer.platform.appframework.metadata.Persistence;
import com.squer.sfe.api.common.entity.reference.QuestionMasterReference;
import com.squer.sfe.api.common.entity.reference.QuestionnaireReference;

/**
 * Created by mithila on 01/06/17.
 */
@Persistence(prefix="QUIZM", module="common")
public class QuizMap extends AuditableEntity {

    private UserEntityReference owner;
    private QuestionMasterReference question;
    private UserLovReference answer;
    private QuestionnaireReference questionType;

    public UserEntityReference getOwner() {
        return owner;
    }

    public void setOwner(UserEntityReference owner) {
        this.owner = owner;
    }

    public QuestionMasterReference getQuestion() {
        return question;
    }

    public void setQuestion(QuestionMasterReference question) {
        this.question = question;
    }

    public UserLovReference getAnswer() {
        return answer;
    }

    public void setAnswer(UserLovReference answer) {
        this.answer = answer;
    }

    public QuestionnaireReference getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QuestionnaireReference questionType) {
        this.questionType = questionType;
    }
}
