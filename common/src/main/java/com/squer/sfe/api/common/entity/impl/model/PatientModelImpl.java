package com.squer.sfe.api.common.entity.impl.model;

import com.squer.platform.ServiceLocator;
import com.squer.platform.appframework.auth.reference.SecurityRoleReference;
import com.squer.platform.appframework.config.UserLov;
import com.squer.platform.appframework.config.reference.SysLovReference;
import com.squer.platform.appframework.config.reference.UserLovReference;
import com.squer.platform.appframework.entity.SquerEntity;
import com.squer.platform.appframework.entity.SquerReference;
import com.squer.platform.appframework.exception.SquerException;
import com.squer.platform.appframework.metadata.Persistence;
import com.squer.platform.impl.persistence.BaseModelImpl;
import com.squer.platform.log.PlatformLoggerEnum;
import com.squer.platform.log.SquerLogger;
import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.utility.CriteriaMap;
import com.squer.platform.registry.PlatformDelegates;
import com.squer.platform.utils.CommonUtils;
import com.squer.platform.utils.StringUtil;
import com.squer.platform.utils.SystemPropertyEnum;
import com.squer.platform.utils.SystemUtil;
import com.squer.registry.CommonQuery;
import com.squer.sfe.api.common.entity.*;
import com.squer.sfe.api.common.entity.reference.PatientReference;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Squer on 11/18/2016.
 */
public class PatientModelImpl extends BaseModelImpl {
    private static SquerLogger logger = SquerLogger.getLogger(PlatformLoggerEnum.Persistence);

    public SquerEntity preCreate(Persistence persistence, SquerEntity entity) throws SquerException {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            Patient patient = (Patient) entity;
            setName(patient);
            calcBmi(patient);
            patient.setStatus((SysLovReference) ServiceLocator.getInstance().getReference("SYSLV00000000000000000000000000000001"));
            if(!patient.getDateOfBirth().isEmpty()){
                calcAge(patient);
            }
            return entity;
        } catch (Exception e) {
            throw new SquerException(e);
        }
    }

    public SquerReference postCreate(Persistence persistence, SquerEntity entity) throws SquerException {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            Patient patient = (Patient) entity;
            PatientReference reference = (PatientReference) ServiceLocator.getInstance().getReference(patient.getId());
            if(patient.getContent()!=null){
                CriteriaMap criteriaMap = new CriteriaMap(CommonQuery.SELECT_DOCUMENT);
                criteriaMap.addCriteria("owner",patient.getId());
                List<Document> documentList = repository.find(criteriaMap);

                if(!CommonUtils.isNullOrEmpty(documentList)){
                    DocumentUtil.deleteDocument(documentList.get(0).getOwner().getId());
                }
                DocumentUtil.createDocument(patient.getId(),"SYSLVIMGTP000000000000000000000000001",patient.getContent());
                //CriteriaMap criteria = new CriteriaMap(CommonQuery.INSERT_DOCUMENT);
            }

            List<String> lovType = new ArrayList<String>();
            lovType.add("sugar_profile_report_type");
            lovType.add("cvd_report_type");
            lovType.add("nephro_report_type");
            lovType.add("other_report_type");
            lovType.add("lifestyle_question");

            CriteriaMap criteriaMap = new CriteriaMap(CommonQuery.USER_LOV_TYPE_SELECT);
            criteriaMap.addCriteria("userLovType", lovType);

            List<UserLov> referenceList = repository.find(criteriaMap);

            for (UserLov lov : referenceList) {
                if ("lifestyle_question".equalsIgnoreCase(lov.getType())) {
                    PatientLifestyle lifestyle = new PatientLifestyle();
                    lifestyle.setPatient(reference);
                    lifestyle.setLifeStyle((UserLovReference) ServiceLocator.getInstance().getReference(lov.getId()));
                    lifestyle.setAnswer(null);
                    lifestyle.setAnswerId(null);
                    repository.create(lifestyle);
                } else {
                    Threshold threshold = new Threshold();
                    threshold.setPatient(reference);
                    threshold.setMetric((UserLovReference) ServiceLocator.getInstance().getReference(lov.getId()));
                    switch (threshold.getMetric().getId()){
                        case "USRLVRT000000000000000000000000000001":
                            threshold.setValue(Float.parseFloat("7"));
                            threshold.setValueMin(Float.parseFloat("0"));
                            break;
                        case "USRLVRT000000000000000000000000000002":
                            threshold.setValue(Float.parseFloat("130"));
                            threshold.setValueMin(Float.parseFloat("0"));
                            break;
                        case "USRLVRT000000000000000000000000000003":
                            threshold.setValue(Float.parseFloat("180"));
                            threshold.setValueMin(Float.parseFloat("0"));
                            break;
                        case "USRLVRT000000000000000000000000000004":
                            threshold.setValue(Float.parseFloat("200"));
                            threshold.setValueMin(Float.parseFloat("0"));
                            break;
                        case "USRLVRT000000000000000000000000000005":
                            threshold.setValue(Float.parseFloat("130"));
                            threshold.setValueMin(Float.parseFloat("0"));
                            break;
                        case "USRLVRT000000000000000000000000000018":
                            threshold.setValue(Float.parseFloat("80"));
                            threshold.setValueMin(Float.parseFloat("0"));
                            break;
                        case "USRLVRT000000000000000000000000000006":
                            threshold.setValue(Float.parseFloat("200"));
                            threshold.setValueMin(Float.parseFloat("0"));
                            break;
                        case "USRLVRT000000000000000000000000000007":
                            threshold.setValue(Float.parseFloat("100"));
                            threshold.setValueMin(Float.parseFloat("0"));
                            break;
                        case "USRLVRT000000000000000000000000000009":
                            threshold.setValue(Float.parseFloat("150"));
                            threshold.setValueMin(Float.parseFloat("0"));
                            break;
                        case "USRLVRT000000000000000000000000000010":
                            threshold.setValue(Float.parseFloat("0"));
                            threshold.setValueMin(Float.parseFloat("60"));
                            break;
                        case "USRLVRT000000000000000000000000000012":
                            if(patient.getGender().getId().equalsIgnoreCase("USRLVGEN00000000000000000000000000001")){
                                threshold.setValue(Float.parseFloat("0.9"));
                                threshold.setValueMin(Float.parseFloat("0"));
                            }else if(patient.getGender().getId().equalsIgnoreCase("USRLVGEN00000000000000000000000000002")){
                                threshold.setValue(Float.parseFloat("0.7"));
                                threshold.setValueMin(Float.parseFloat("0"));
                            }
                            break;
                        case "USRLVRT000000000000000000000000000013":
                            threshold.setValue(Float.parseFloat("20"));
                            threshold.setValueMin(Float.parseFloat("0"));
                            break;
                    }
                    repository.create(threshold);
                }
            }

            LoginUtil.createUser(repository, reference, patient.getUsername(), patient.getPassword(),
                    (SecurityRoleReference) ServiceLocator.getInstance().getReference("SROLE00000023d6ee570149c6b4e03a007111"));

            return super.postCreate(persistence, entity);
        } catch (Exception e) {
            logger.fatal(e);
            throw new SquerException(e);
        }
    }

    @Override
    public SquerEntity preUpdate(Persistence persistence, SquerEntity entity) throws SquerException {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            Patient patient = (Patient) entity;
            setName(patient);
            calcBmi(patient);
            if(!patient.getDateOfBirth().isEmpty()){
                calcAge(patient);
            }
//            if(patient.getStatus()==null || patient.getStatus().getId().equals("SYSLV00000000000000000000000000000001")){
//                patient.setStatus((SysLovReference) ServiceLocator.getInstance().getReference("SYSLV00000000000000000000000000000001"));
//            }else if(patient.getStatus()!=null && patient.getStatus().getId().equals("SYSLV00000000000000000000000000000002")){
//                patient.setStatus((SysLovReference) ServiceLocator.getInstance().getReference("SYSLV00000000000000000000000000000002"));
//            }

            return super.preUpdate(persistence, entity);
        } catch (Exception e) {
            throw new SquerException(e);
        }
    }

    @Override
    public SquerReference postUpdate(Persistence persistence, SquerEntity entity) throws SquerException {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            Patient patient = (Patient) entity;
            if(patient.getContent()!=null){
                CriteriaMap criteriaMap = new CriteriaMap(CommonQuery.SELECT_DOCUMENT);
                criteriaMap.addCriteria("owner",patient.getId());
                List<Document> documentList = repository.find(criteriaMap);

                if(!CommonUtils.isNullOrEmpty(documentList)){
                    DocumentUtil.deleteDocument(documentList.get(0).getOwner().getId());
                }
                DocumentUtil.createDocument(patient.getId(),"SYSLVIMGTP000000000000000000000000001",patient.getContent());
                //CriteriaMap criteria = new CriteriaMap(CommonQuery.INSERT_DOCUMENT);
            }
            return super.postUpdate(persistence, entity);
        }catch (Exception e){
            logger.fatal(e);
            throw new SquerException(e);
        }
    }

    private void setName(Patient patient) throws SquerException {
        String nameFormat = SystemUtil.getPropertyValue(SystemPropertyEnum.NAME_FORMAT);
        if (nameFormat.equalsIgnoreCase("fname-lname"))
            patient.setName((StringUtil.isNullOrEmpty(patient.getFirstName()) ? "" : patient.getFirstName() + " " +
                    (StringUtil.isNullOrEmpty(patient.getMiddleName()) ? "" : patient.getMiddleName()) + " " +
                    (StringUtil.isNullOrEmpty(patient.getLastName()) ? "" : patient.getLastName())).trim());
        else
            patient.setName((StringUtil.isNullOrEmpty(patient.getLastName()) ? "" : patient.getLastName() + " " +
                    (StringUtil.isNullOrEmpty(patient.getFirstName()) ? "" : patient.getFirstName()) + " " +
                    (StringUtil.isNullOrEmpty(patient.getMiddleName()) ? "" : patient.getMiddleName())).trim());
    }

    private void calcBmi(Patient patient) throws SquerException {
        if (patient.getWeight() != 0 && patient.getHeight() != 0) {
            Float bmi = (float) patient.getWeight() / ((((float) patient.getHeight()) / 100) * ((float) patient.getHeight() / 100));
            patient.setbMI(bmi);
        }
    }

    private void calcAge(Patient patient) throws SquerException, ParseException {
        //LocalDate dob = LocalDate.parse(patient.getDateOfBirth());
        //LocalDate curDate = LocalDate.now();
        //return Period.between(dob, curDate).getYears();
        SimpleDateFormat format=new SimpleDateFormat("DD/MM/YYYY");
        Calendar calendar1=Calendar.getInstance();
        calendar1.setTime(format.parse(patient.getDateOfBirth()));

        Calendar calendar2=Calendar.getInstance();
        calendar2.setTime(new Date());

        int yd=calendar2.get(Calendar.YEAR)-calendar1.get(Calendar.YEAR);
        System.out.println(yd);
        patient.setAge(yd);

    }

}
