package com.squer.sfe.api.common.entity.reference;

import com.squer.platform.appframework.entity.SquerReferenceImpl;

/**
 * Created by mithila on 06/05/17.
 */
public class PushDataReference extends SquerReferenceImpl {

    public PushDataReference(){
        super(null);
    }

    public PushDataReference(String id){
        super(id);
    }
}
