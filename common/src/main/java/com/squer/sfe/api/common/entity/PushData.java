package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.auth.reference.UserEntityReference;
import com.squer.platform.appframework.entity.AuditableEntity;
import com.squer.platform.appframework.entity.SquerReference;
import com.squer.platform.appframework.metadata.Persistence;

/**
 * Created by mithila on 06/05/17.
 */
@Persistence(prefix="PUSHD", module="common")
public class PushData extends AuditableEntity{
    private SquerReference owner;
    private String registerToken;
    private Boolean ios;

    public Boolean getIos() {
        return ios;
    }

    public void setIos(Boolean ios) {
        this.ios = ios;
    }

    public SquerReference getOwner() {
        return owner;
    }

    public void setOwner(SquerReference owner) {
        this.owner = owner;
    }

    public String getRegisterToken() {
        return registerToken;
    }

    public void setRegisterToken(String registerToken) {
        this.registerToken = registerToken;
    }
}
