package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.config.reference.UserLovReference;
import com.squer.platform.appframework.entity.AuditableEntity;
import com.squer.platform.appframework.entity.SquerReference;
import com.squer.platform.appframework.entity.StandardEntity;
import com.squer.platform.appframework.metadata.Persistence;

/**
 * Created by shriram on 22/4/15.
 */
@Persistence(prefix="DOCMT", module="common")
public class Document extends AuditableEntity{

    private SquerReference owner;
    private SquerReference type;
    private String name;
    private UserLovReference status;

    public SquerReference getOwner() {
        return owner;
    }

    public void setOwner(SquerReference owner) {
        this.owner = owner;
    }

    public SquerReference getType() {
        return type;
    }

    public void setType(SquerReference type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserLovReference getStatus() {
        return status;
    }

    public void setStatus(UserLovReference status) {
        this.status = status;
    }
}
