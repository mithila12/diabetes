package com.squer.sfe.api.common.entity.reference;

import com.squer.platform.appframework.entity.NamedReference;

/**
 * Created by ashutoshpavaskar on 10/10/14.
 */
public class PartyReference extends NamedReference {

    public PartyReference(){
        super(null);
    }

    public PartyReference(String id, String name){
        super(id, name);

    }


}
