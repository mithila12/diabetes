package com.squer.sfe.api.common.entity.reference;

import com.squer.platform.appframework.entity.SquerReferenceImpl;

/**
 * Created by mithila on 13/05/17.
 */
public class RecordMasterDataReference extends SquerReferenceImpl {

    public RecordMasterDataReference(){ super(null);}

    public RecordMasterDataReference(String id){super(id);}
}
