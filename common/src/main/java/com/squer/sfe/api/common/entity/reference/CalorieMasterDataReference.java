package com.squer.sfe.api.common.entity.reference;

import com.squer.platform.appframework.entity.NamedReference;
import com.squer.platform.appframework.entity.SquerReferenceImpl;

/**
 * Created by mithila on 01/06/17.
 */
public class CalorieMasterDataReference extends NamedReference {

    public CalorieMasterDataReference(){ super(null,null); }

    public CalorieMasterDataReference(String id,String name){ super(id,name);}
}
