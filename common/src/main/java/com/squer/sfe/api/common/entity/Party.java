package com.squer.sfe.api.common.entity;

/**
 * Created by ashutoshpavaskar on 06/11/14.
 */
public class Party {
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
