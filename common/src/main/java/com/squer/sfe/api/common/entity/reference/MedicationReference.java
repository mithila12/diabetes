package com.squer.sfe.api.common.entity.reference;

/**
 * Created by user on 12/28/2016.
 */
public class MedicationReference extends PartyReference {

    public MedicationReference () {
        super();
    }

    public MedicationReference (String id,String name) {
        super(id, name);
    }
}
