package com.squer.sfe.api.common.entity.reference;

/**
 * Created by user on 12/19/2016.
 */
public class ThresholdReference extends PartyReference {

    public ThresholdReference () {
        super();
    }

    public ThresholdReference (String id, String name) {
        super(id, name);
    }
}
