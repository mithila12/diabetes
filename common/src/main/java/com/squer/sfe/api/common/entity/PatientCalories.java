package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.config.reference.UserLovReference;
import com.squer.platform.appframework.entity.AuditableEntity;
import com.squer.platform.appframework.metadata.Persistence;
import com.squer.sfe.api.common.entity.reference.CalorieMasterDataReference;
import com.squer.sfe.api.common.entity.reference.PatientReference;

import java.util.Date;

/**
 * Created by mithila on 13/06/17.
 */
@Persistence(prefix = "CALPT", module="common")
public class PatientCalories extends AuditableEntity {

    private String caloriesConsumed;
    private CalorieMasterDataReference foodItem;
    private String quantity;
    private UserLovReference foodType;
    private PatientReference patient;
    private String date;

    public UserLovReference getFoodType() {
        return foodType;
    }

    public void setFoodType(UserLovReference foodType) {
        this.foodType = foodType;
    }

    public String getCaloriesConsumed() {
        return caloriesConsumed;
    }

    public void setCaloriesConsumed(String caloriesConsumed) {
        this.caloriesConsumed = caloriesConsumed;
    }

    public PatientReference getPatient() {
        return patient;
    }

    public void setPatient(PatientReference patient) {
        this.patient = patient;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public CalorieMasterDataReference getFoodItem() {
        return foodItem;
    }

    public void setFoodItem(CalorieMasterDataReference foodItem) {
        this.foodItem = foodItem;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
