package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.entity.StandardEntity;
import com.squer.platform.appframework.metadata.Persistence;

/**
 * Created by mithila on 01/06/17.
 */
@Persistence(prefix="QUEST", module="common")
public class Questionnaire extends StandardEntity {
    private Boolean showOnDoctor;

    public Boolean getShowOnDoctor() {
        return showOnDoctor;
    }

    public void setShowOnDoctor(Boolean showOnDoctor) {
        this.showOnDoctor = showOnDoctor;
    }
}
