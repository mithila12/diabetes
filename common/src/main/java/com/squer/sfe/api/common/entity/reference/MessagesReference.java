package com.squer.sfe.api.common.entity.reference;

import com.squer.platform.appframework.entity.SquerReferenceImpl;

/**
 * Created by mithila on 9/12/16.
 */
public class MessagesReference extends SquerReferenceImpl {

    public MessagesReference () {
        super(null);
    }

    public MessagesReference (String id) {
        super(id);
    }
}