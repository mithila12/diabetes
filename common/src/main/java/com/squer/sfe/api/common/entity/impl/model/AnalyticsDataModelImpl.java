package com.squer.sfe.api.common.entity.impl.model;

import com.squer.platform.ServiceLocator;
import com.squer.platform.appframework.entity.SquerEntity;
import com.squer.platform.appframework.exception.SquerException;
import com.squer.platform.appframework.metadata.Persistence;
import com.squer.platform.impl.persistence.BaseModelImpl;
import com.squer.platform.log.PlatformLoggerEnum;
import com.squer.platform.log.SquerLogger;
import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.utility.CriteriaMap;
import com.squer.platform.persistence.utility.QueryCondition;
import com.squer.platform.registry.PlatformDelegates;
import com.squer.registry.CommonQuery;
import com.squer.sfe.api.common.entity.AnalyticsData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mithila on 19/04/18-Apr-2018 at 3:59 PM
 */
public class AnalyticsDataModelImpl extends BaseModelImpl {


    private static SquerLogger logger = SquerLogger.getLogger(PlatformLoggerEnum.Persistence);

    public SquerEntity preCreate(Persistence persistence, SquerEntity entity) throws SquerException {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            AnalyticsData data = (AnalyticsData) entity;

//            CriteriaMap map = new CriteriaMap(CommonQuery.ANALYTICS_SELECT);
//            QueryCondition condition = new QueryCondition("OWNER_ID",data.getOwner().getId());
//            List<QueryCondition> list = new ArrayList<>();
//            list.add(condition);
//            map.addCriteria("searchQuery",list);
//            map.addCriteria("sortOrder","ORDER BY CREATED_ON DESC");
//            map.addCriteria("limit",true);
//            map.addCriteria("lower",1);
//            List<AnalyticsData> analyticsData = repository.find(map);
//            if(analyticsData.size()>0){
//                AnalyticsData analytics = analyticsData.get(0);
//                analytics.setEndTime(data.getStartTime());
//
//                repository.update(analytics);
//
//            }

            return entity;
        } catch (Exception e) {
            throw new SquerException(e);
        }
    }

}
