package com.squer.sfe.api.common.entity.reference;

import com.squer.platform.appframework.entity.SquerReferenceImpl;

/**
 * Created by mithila on 13/06/17.
 */
public class PatientCaloriesReference extends SquerReferenceImpl {

    public PatientCaloriesReference(){super(null);}

    public PatientCaloriesReference(String id){super(id);}
}
