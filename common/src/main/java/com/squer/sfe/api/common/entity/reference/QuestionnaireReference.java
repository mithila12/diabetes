package com.squer.sfe.api.common.entity.reference;

/**
 * Created by mithila on 01/06/17.
 */
public class QuestionnaireReference extends PartyReference{

    public QuestionnaireReference(){super();}

    public QuestionnaireReference(String id, String name){ super(id,name);}
}
