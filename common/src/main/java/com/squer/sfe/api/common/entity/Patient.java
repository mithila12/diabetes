package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.config.reference.SysLovReference;
import com.squer.platform.appframework.config.reference.UserLovReference;
import com.squer.platform.appframework.entity.StandardEntity;
import com.squer.platform.appframework.metadata.Persistence;
import com.squer.sfe.api.common.entity.reference.DoctorReference;
import com.squer.sfe.api.common.impl.typehandler.CommonUtil;
import java.util.Date;
/**
 * Created by Squer on 11/16/2016.
 */
@Persistence(prefix="PATNT", module="common", model = "CommonDelegates.PatientModelImpl" )
public class Patient extends PartyEntity {

    private String firstName;
    private String lastName;
    private String middleName;
    private UserLovReference gender;
    private String dateOfBirth;
    private int age;
    private String username;
    private String password;
    private String contactNo;
    private int weight;
    private UserLovReference weightUnit;
    private int height;
    private UserLovReference heightUnit;
    private float bMI;
    private UserLovReference diabetesType;
    private String diabetesDiagnose;
    private String hospitalHistory;
    private String otherIllness;
    private String allergy;
    private String famDiabetes;
    private String famBp;
    private String famChole;
    private String email;
    private boolean tobaccoUse;
    private String tobaccoDuration;
    private String tobaccoFreq;
    private boolean alcoholUse;
    private String alcoholDuration;
    private String alcoholFreq;
    private String alcoholQty;
    private String content;
    private byte[] byteContent;
    private String occupation;
    private String profileImg;
    private SysLovReference status;
    private DoctorReference doctor;


    @Override
    public String getName() {
        return CommonUtil.getName(firstName, middleName,  lastName);
    }

    @Override
    public void setName(String name) {
        super.setName(CommonUtil.getName(firstName, middleName, lastName));
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public UserLovReference getGender() {
        return gender;
    }

    public void setGender(UserLovReference gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public UserLovReference getWeightUnit() {
        return weightUnit;
    }

    public void setWeightUnit(UserLovReference weightUnit) {
        this.weightUnit = weightUnit;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public UserLovReference getHeightUnit() {
        return heightUnit;
    }

    public void setHeightUnit(UserLovReference heightUnit) {
        this.heightUnit = heightUnit;
    }

    public float getbMI() {
        return bMI;
    }

    public void setbMI(float bMI) {
        this.bMI = bMI;
    }

    public UserLovReference getDiabetesType() {
        return diabetesType;
    }

    public void setDiabetesType(UserLovReference diabetesType) {
        this.diabetesType = diabetesType;
    }

    public String getDiabetesDiagnose() {
        return diabetesDiagnose;
    }

    public void setDiabetesDiagnose(String diabetesDiagnose) {
        this.diabetesDiagnose = diabetesDiagnose;
    }

    public String getHospitalHistory() {
        return hospitalHistory;
    }

    public void setHospitalHistory(String hospitalHistory) {
        this.hospitalHistory = hospitalHistory;
    }

    public String getOtherIllness() {
        return otherIllness;
    }

    public void setOtherIllness(String otherIllness) {
        this.otherIllness = otherIllness;
    }

    public String getAllergy() {
        return allergy;
    }

    public void setAllergy(String allergy) {
        this.allergy = allergy;
    }

    public String getFamDiabetes() {
        return famDiabetes;
    }

    public void setFamDiabetes(String famDiabetes) {
        this.famDiabetes = famDiabetes;
    }

    public String getFamBp() {
        return famBp;
    }

    public void setFamBp(String famBp) {
        this.famBp = famBp;
    }

    public String getFamChole() {
        return famChole;
    }

    public void setFamChole(String famChole) {
        this.famChole = famChole;
    }

    public boolean isTobaccoUse() {
        return tobaccoUse;
    }

    public boolean isAlcoholUse() {
        return alcoholUse;
    }

    public void setAlcoholUse(boolean alcoholUse) {
        this.alcoholUse = alcoholUse;
    }

    public void setTobaccoUse(boolean tobaccoUse) {
        this.tobaccoUse = tobaccoUse;
    }

    public String getTobaccoDuration() {
        return tobaccoDuration;
    }

    public void setTobaccoDuration(String tobaccoDuration) {
        this.tobaccoDuration = tobaccoDuration;
    }

    public String getTobaccoFreq() {
        return tobaccoFreq;
    }

    public void setTobaccoFreq(String tobaccoFreq) {
        this.tobaccoFreq = tobaccoFreq;
    }

    public String getAlcoholDuration() {
        return alcoholDuration;
    }

    public void setAlcoholDuration(String alcoholDuration) {
        this.alcoholDuration = alcoholDuration;
    }

    public String getAlcoholFreq() {
        return alcoholFreq;
    }

    public void setAlcoholFreq(String alcoholFreq) {
        this.alcoholFreq = alcoholFreq;
    }

    public String getAlcoholQty() {
        return alcoholQty;
    }

    public String getContent() {
        return content;
    }

    public byte[] getByteContent() {
        return byteContent;
    }

    public void setByteContent(byte[] byteContent) {
        this.byteContent = byteContent;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setAlcoholQty(String alcoholQty) {
        this.alcoholQty = alcoholQty;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    public DoctorReference getDoctor() {
        return doctor;
    }

    public void setDoctor(DoctorReference doctor) {
        this.doctor = doctor;
    }

    public SysLovReference getStatus() {
        return status;
    }

    public void setStatus(SysLovReference status) {
        this.status = status;
    }
}
