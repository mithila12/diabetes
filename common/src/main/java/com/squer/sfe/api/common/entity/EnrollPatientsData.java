package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.entity.AuditableEntity;
import com.squer.platform.appframework.metadata.Persistence;
import com.squer.sfe.api.common.entity.reference.DoctorReference;

/**
 * Created by mithila on 18/06/18-Jun-2018 at 2:18 PM
 */

@Persistence(prefix="ENROL", module="common")
public class EnrollPatientsData extends AuditableEntity{

    private DoctorReference doctor;

    private String contactNo;

    public DoctorReference getDoctor() {
        return doctor;
    }

    public void setDoctor(DoctorReference doctor) {
        this.doctor = doctor;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }
}
