package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.entity.AuditableEntity;
import com.squer.platform.appframework.entity.StandardEntity;
import com.squer.platform.appframework.metadata.Persistence;

/**
 * Created by mithila on 30/05/17.
 */
@Persistence(prefix="CALMD", module="common")
public class CalorieMasterData extends StandardEntity {

    private String foodItem;
    private String portionSize;
    private String calorieContent;
    private String energyContent;

    public String getFoodItem() {
        return foodItem;
    }

    public void setFoodItem(String foodItem) {
        this.foodItem = foodItem;
    }

    public String getPortionSize() {
        return portionSize;
    }

    public void setPortionSize(String portionSize) {
        this.portionSize = portionSize;
    }

    public String getCalorieContent() {
        return calorieContent;
    }

    public void setCalorieContent(String calorieContent) {
        this.calorieContent = calorieContent;
    }

    public String getEnergyContent() {
        return energyContent;
    }

    public void setEnergyContent(String energyContent) {
        this.energyContent = energyContent;
    }
}
