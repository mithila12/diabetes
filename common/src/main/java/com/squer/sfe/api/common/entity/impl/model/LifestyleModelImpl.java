package com.squer.sfe.api.common.entity.impl.model;

import com.squer.platform.ServiceLocator;
import com.squer.platform.appframework.entity.SquerEntity;
import com.squer.platform.appframework.entity.SquerReference;
import com.squer.platform.appframework.exception.SquerException;
import com.squer.platform.appframework.metadata.Persistence;
import com.squer.platform.impl.persistence.BaseModelImpl;
import com.squer.platform.log.PlatformLoggerEnum;
import com.squer.platform.log.SquerLogger;
import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.utility.CriteriaMap;
import com.squer.platform.registry.PlatformDelegates;
import com.squer.registry.CommonQuery;
import com.squer.sfe.api.common.entity.DocumentUtil;
import com.squer.sfe.api.common.entity.Patient;
import com.squer.sfe.api.common.entity.PatientLifestyle;
import com.squer.sfe.api.common.entity.PatientReports;

import java.util.List;

/**
 * Created by user on 1/17/2017.
 */
public class LifestyleModelImpl extends BaseModelImpl {
    SquerLogger logger = SquerLogger.getLogger(PlatformLoggerEnum.Persistence);

    @Override
    public SquerEntity preUpdate(Persistence persistence, SquerEntity entity) throws SquerException {
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            PatientLifestyle lifestyle = (PatientLifestyle) entity;
            CriteriaMap criteriaMap = new CriteriaMap(CommonQuery.PATIENT_LIFESTYLE_SELECT);
            criteriaMap.addCriteria("patient",lifestyle.getPatient());
            criteriaMap.addCriteria("lifeStyle",lifestyle.getLifeStyle());

            List<PatientLifestyle> questions = repository.find(criteriaMap);

            for(PatientLifestyle q: questions){
                repository.delete(ServiceLocator.getInstance().getReference(q.getId()));
             }
            return super.preUpdate(persistence, entity);
        } catch (Exception e) {
            throw new SquerException(e);
        }
    }
    }


