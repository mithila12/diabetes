package com.squer.sfe.api.common.entity.reference;

import com.squer.platform.appframework.entity.SquerReferenceImpl;

/**
 * Created by mithila on 19/04/18-Apr-2018 at 12:19 PM
 */
public class AnalyticsDataReference extends SquerReferenceImpl {

    public AnalyticsDataReference(){super(null);}

    public AnalyticsDataReference(String id){super(id);}
}
