package com.squer.sfe.api.common.entity.impl.model;

import com.squer.platform.ServiceLocator;
import com.squer.platform.appframework.entity.SquerEntity;
import com.squer.platform.appframework.entity.SquerReference;
import com.squer.platform.appframework.exception.SquerException;
import com.squer.platform.appframework.metadata.Persistence;
import com.squer.platform.impl.persistence.BaseModelImpl;
import com.squer.platform.log.PlatformLoggerEnum;
import com.squer.platform.log.SquerLogger;
import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.utility.CriteriaMap;
import com.squer.platform.persistence.utility.QueryCondition;
import com.squer.platform.registry.PlatformDelegates;
import com.squer.registry.CommonQuery;
import com.squer.sfe.api.common.entity.*;
import net.minidev.json.JSONObject;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 1/11/2017.
 */
public class PatientReportsModelImpl extends BaseModelImpl {
    SquerLogger logger = SquerLogger.getLogger(PlatformLoggerEnum.Persistence);
    public SquerReference postCreate(Persistence persistence, SquerEntity entity) throws Exception{
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            PatientReports reports = (PatientReports) entity;
            if(reports.getContent()!=null){
                DocumentUtil.createDocument(reports.getId(),"SYSLVIMGTP000000000000000000000000003",reports.getContent());

            }

            // register_token_select
            Patient patient = (Patient) repository.restore(ServiceLocator.getInstance().getReference(reports.getPatient().getId()));
                CriteriaMap thresholdMap = new CriteriaMap(CommonQuery.THRESHOLD_SELECT);
                QueryCondition condition = new QueryCondition("METRIC_ID",reports.getReportType().getId());
                QueryCondition condition1 = new QueryCondition("PATIENT_ID",reports.getPatient().getId());
                List<QueryCondition> conditions = new ArrayList<>();
                conditions.add(condition);
                conditions.add(condition1);
                thresholdMap.addCriteria("searchQuery",conditions);
                List<Threshold> thresholds = repository.find(thresholdMap);
                if(thresholds.size()>0){
                    Threshold threshold = thresholds.get(0);

                    float thresholdVal = 0;
                    if(threshold.getValueMin()!=null && threshold.getValue()!=null){
                        if(threshold.getValueMin()>0){
                            thresholdVal = threshold.getValueMin();
                        }else if(threshold.getValue()>0){
                            thresholdVal = threshold.getValue();
                        }
                    }


                    if(Float.parseFloat(reports.getReportValue())>thresholdVal){
                        String doctorId = patient.getDoctor().getId();
                        CriteriaMap criteriaMap = new CriteriaMap(CommonQuery.REGISTER_TOKEN_DOCTOR_SELECT);
                        criteriaMap.addCriteria("doctorId",doctorId);
                        List<Map> resultMap =repository.find(criteriaMap);

                        for(Map m : resultMap){
                            if(m.containsKey("REG_TN")){
                                if(m.get("REG_TN").toString()!=null){
                                    String regToken=m.get("REG_TN").toString();
                                    String message ="One of your patients has added new record which is beyond target!";

                                    String ownerId = m.get("OWNER_ID").toString();
                                    if(m.containsKey("isIos")){
                                        if(Boolean.valueOf(m.get("isIos").toString())){
                                            NotificationUtil.sendIosNotification(regToken,ownerId,"record",message,patient.getId(),null,null);
                                        }else {
                                            NotificationUtil.sendAndroidNotification(regToken,ownerId,"record",false,message,patient.getId());
                                        }
                                    }else {
                                        NotificationUtil.sendAndroidNotification(regToken,ownerId,"record",false,message,patient.getId());
                                    }
                                }

                            }

                        }
                    }

                }



            return super.postCreate(persistence, entity);
        } catch (Exception e){
            logger.fatal(e);
            throw new SquerException(e);
        }
    }
}
