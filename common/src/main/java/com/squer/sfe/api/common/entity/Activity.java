package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.entity.AuditableEntity;
import com.squer.platform.appframework.metadata.Persistence;
import com.squer.sfe.api.common.entity.reference.PatientReference;

import java.util.Date;

/**
 * Created by mithila on 26/05/17.
 */
@Persistence(prefix="ACTVT", module="common")
public class Activity extends AuditableEntity{

    private String activity;
    private String activeTime;
    private String caloriesBurnt;
    private Date date;
    private String distanceTravelled;
    private PatientReference activityOf;

    public PatientReference getActivityOf() {
        return activityOf;
    }

    public void setActivityOf(PatientReference activityOf) {
        this.activityOf = activityOf;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getActiveTime() {
        return activeTime;
    }

    public void setActiveTime(String activeTime) {
        this.activeTime = activeTime;
    }

    public String getCaloriesBurnt() {
        return caloriesBurnt;
    }

    public void setCaloriesBurnt(String caloriesBurnt) {
        this.caloriesBurnt = caloriesBurnt;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDistanceTravelled() {
        return distanceTravelled;
    }

    public void setDistanceTravelled(String distanceTravelled) {
        this.distanceTravelled = distanceTravelled;
    }
}
