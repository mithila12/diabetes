package com.squer.sfe.api.common.entity;

import com.squer.platform.ServiceLocator;
import com.squer.platform.appframework.auth.SecurityRole;
import com.squer.platform.appframework.auth.UserEntity;
import com.squer.platform.appframework.auth.reference.SecurityRoleReference;
import com.squer.platform.appframework.config.reference.SysLovReference;
import com.squer.platform.appframework.entity.SquerEntity;
import com.squer.platform.appframework.entity.SquerReference;
import com.squer.platform.persistence.Repository;
import com.squer.platform.utils.StringUtil;
import com.squer.platform.utils.SystemPropertyEnum;
import com.squer.platform.utils.SystemUtil;
import com.squer.sfe.api.common.entity.reference.DoctorReference;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mithila on 30/11/16.
 */
public class LoginUtil {
    public static void createUser(Repository repository,
                                  SquerReference owner,
                                  String username,
                                  String password,
                                  SecurityRoleReference role) throws Exception{


        List<SecurityRoleReference> roles = new ArrayList<>();
        roles.add(role);
        String defaultPassword = SystemUtil.getPropertyValue(SystemPropertyEnum.DEFAULT_USER_PASSWORD);
        UserEntity user= new UserEntity();
        user.setOwner(owner);
        user.setUserName(username);
        if(password==null){
            user.setPassword(StringUtil.getHash(defaultPassword));
        }
        else{
            user.setPassword(StringUtil.getHash(password));
        }
        user.setStatus((SysLovReference) ServiceLocator.getInstance().getReference("SYSLV00000000000000000000000000000001"));
        user.setRoles(roles);
        repository.create(user);

    }
}
