package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.entity.AuditableEntity;
import com.squer.platform.appframework.entity.SquerReference;
import com.squer.platform.appframework.metadata.Persistence;

import java.util.Date;

/**
 * Created by mithila on 19/04/18-Apr-2018 at 12:02 PM
 */
@Persistence(prefix="ANADA", module="common",model = "CommonDelegates.AnalyticsDataModelImpl" )
public class AnalyticsData extends AuditableEntity {

    private String type;

    private String contentid;

    private String name;

    private SquerReference owner;

    private Date startTime;

    private Date endTime;

    private String pageName;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContentid() {
        return contentid;
    }

    public void setContentid(String contentid) {
        this.contentid = contentid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SquerReference getOwner() {
        return owner;
    }

    public void setOwner(SquerReference owner) {
        this.owner = owner;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }
}
