package com.squer.sfe.api.common.entity.reference;

import com.squer.platform.appframework.entity.SquerReferenceImpl;

/**
 * Created by mithila on 26/05/17.
 */
public class ActivityReference extends SquerReferenceImpl{

    public ActivityReference (){super(null);}

    public ActivityReference(String id){ super(id);}
}
