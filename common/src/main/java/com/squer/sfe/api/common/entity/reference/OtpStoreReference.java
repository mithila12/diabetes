package com.squer.sfe.api.common.entity.reference;

import com.squer.platform.appframework.entity.SquerReferenceImpl;

/**
 * Created by mithila on 12/04/17.
 */
public class OtpStoreReference extends SquerReferenceImpl {

    public OtpStoreReference() { super(null); }

    public OtpStoreReference(String id){ super(id); }
}
