package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.entity.AuditableEntity;
import com.squer.platform.appframework.metadata.Persistence;
import com.squer.sfe.api.common.entity.reference.DoctorReference;

import java.util.Date;

/**
 * Created by mithila on 9/12/16.
 */
@Persistence(prefix="MESSG", module="common", model = "CommonDelegates.MessageModelImpl")
public class Messages extends AuditableEntity {

    private DoctorReference doctor;
    private String content;
    private byte[] byteContent;
    private String message;
    private Date date;

    public byte[] getByteContent() {
        return byteContent;
    }

    public void setByteContent(byte[] byteContent) {
        this.byteContent = byteContent;
    }

    public DoctorReference getDoctor() {
        return doctor;
    }

    public void setDoctor(DoctorReference doctor) {
        this.doctor = doctor;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
