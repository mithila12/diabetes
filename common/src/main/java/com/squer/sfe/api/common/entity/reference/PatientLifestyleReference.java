package com.squer.sfe.api.common.entity.reference;

/**
 * Created by Squer on 11/17/2016.
 */
public class PatientLifestyleReference extends PartyReference{

    public PatientLifestyleReference () {
        super();
    }

    public PatientLifestyleReference (String id, String name) {
        super(id, name);
    }
}
