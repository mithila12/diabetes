package com.squer.sfe.api.common.impl.typehandler;

import com.squer.platform.ServiceLocator;
import com.squer.platform.appframework.entity.NamedReferenceImpl;
import com.squer.platform.appframework.entity.SquerReference;
import com.squer.platform.utils.StringUtil;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by ashutoshpavaskar on 24/03/15.
 */
public class NamedReferenceTypeHandler implements TypeHandler<NamedReferenceImpl> {

    public NamedReferenceTypeHandler() {
    }

    public void setParameter(PreparedStatement preparedStatement, int i, NamedReferenceImpl o, JdbcType jdbcType) throws SQLException {
    }

    public NamedReferenceImpl getResult(ResultSet resultSet, String s) throws SQLException {
        try {
            String id = resultSet.getString(s);
            if(StringUtil.isNullOrEmpty(id))
                return null;
            SquerReference reference = ServiceLocator.getInstance().getReference(id);
            NamedReferenceImpl namedReference = new NamedReferenceImpl(reference.getId());
            return namedReference;
        } catch (Exception var6) {
            throw new SQLException(var6);
        }
    }

    public NamedReferenceImpl getResult(ResultSet resultSet, int i) throws SQLException {
        return null;
    }

    public NamedReferenceImpl getResult(CallableStatement callableStatement, int i) throws SQLException {
        return null;
    }
}
