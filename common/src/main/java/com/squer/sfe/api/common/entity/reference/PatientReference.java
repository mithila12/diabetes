package com.squer.sfe.api.common.entity.reference;

/**
 * Created by Squer on 11/17/2016.
 */
public class PatientReference extends PartyReference {
    public PatientReference () {
        super();
    }

    public PatientReference (String id, String name) {
        super(id, name);
    }
}
