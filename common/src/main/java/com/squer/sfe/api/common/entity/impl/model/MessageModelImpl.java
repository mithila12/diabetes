package com.squer.sfe.api.common.entity.impl.model;

import com.squer.platform.ServiceLocator;
import com.squer.platform.appframework.auth.reference.SecurityRoleReference;
import com.squer.platform.appframework.entity.SquerEntity;
import com.squer.platform.appframework.entity.SquerReference;
import com.squer.platform.appframework.exception.SquerException;
import com.squer.platform.appframework.metadata.Persistence;
import com.squer.platform.impl.persistence.BaseModelImpl;
import com.squer.platform.log.PlatformLoggerEnum;
import com.squer.platform.log.SquerLogger;
import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.utility.CriteriaMap;
import com.squer.platform.registry.PlatformDelegates;
import com.squer.registry.CommonQuery;
import com.squer.sfe.api.common.entity.Document;
import com.squer.sfe.api.common.entity.DocumentUtil;
import com.squer.sfe.api.common.entity.Messages;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.squer.sfe.api.common.entity.NotificationUtil;
import javafx.beans.binding.BooleanBinding;
import net.minidev.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 1/11/2017.
 */
public class MessageModelImpl extends BaseModelImpl {
    private static SquerLogger logger = SquerLogger.getLogger(PlatformLoggerEnum.Persistence);

    public SquerReference postCreate(Persistence persistence, SquerEntity entity) throws Exception{
        try {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            Messages messages = (Messages) entity;

            if(messages.getContent()!=null) {
                DocumentUtil.createDocument(messages.getId(), "SYSLVIMGTP000000000000000000000000002", messages.getContent());
                //CriteriaMap criteria = new CriteriaMap(CommonQuery.INSERT_DOCUMENT);
            }


//            CriteriaMap criteriaMap_doc = new CriteriaMap(CommonQuery.SELECT_DOCUMENT);
//            criteriaMap_doc.addCriteria("owner", messages.getId());
//
//            List<Document> documents = repository.find(criteriaMap_doc);
//            Document document = documents.get(0);

            // register_token_select
            CriteriaMap criteriaMap = new CriteriaMap(CommonQuery.REGISTER_TOKEN_SELECT);
            criteriaMap.addCriteria("doctorId",messages.getDoctor().getId());
            List<Map> resultMap =repository.find(criteriaMap);
            if(resultMap.size()>0){
                for(Map m : resultMap){
                    String regToken=m.get("REG_TN").toString();
                    String message ="";
                    if(messages.getMessage()!=null){
                        message = messages.getMessage();
                    }else {
                        message = "New Image";
                    }
                    String ownerId = m.get("OWNER_ID").toString();
                    if(m.containsKey("isIos")){
                        if(Boolean.valueOf(m.get("isIos").toString())){
                            NotificationUtil.sendIosNotification(regToken,ownerId,"Messages",message,null,null,null);
                        }else {
                            NotificationUtil.sendAndroidNotification(regToken,ownerId,"Messages",false,message,null);
                        }
                    }else {
                        NotificationUtil.sendAndroidNotification(regToken,ownerId,"Messages",false,message,null);
                    }
                }
            }

            return super.postCreate(persistence, entity);
        } catch (Exception e){
            logger.fatal(e);
            throw new SquerException(e);
        }
    }
}
