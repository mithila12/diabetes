package com.squer.sfe.api.common.entity;

import com.squer.platform.appframework.config.UserLov;
import com.squer.platform.appframework.config.reference.SysLovReference;
import com.squer.platform.appframework.config.reference.UserLovReference;
import com.squer.platform.appframework.entity.StandardEntity;
import com.squer.platform.appframework.metadata.Persistence;
import com.squer.sfe.api.common.impl.typehandler.CommonUtil;

import java.util.Date;

@Persistence(prefix="DOCTR", module="common", model = "CommonDelegates.DoctorModelImpl" )
public class  Doctor extends PartyEntity {

    private String code;
    private String firstName;
    private String lastName;
    private String middleName;
    private String degrees;
    private String age;
    private UserLovReference gender;
    private String specialities;
    private String experience;
    private String email;
    private String password;
    private String content;
    private Date dateOfBirth;
    private String addressOffice;
    private String addressClinic;
    private String contactOffice;
    private String contactClinic;
    private String profileImg;
    private SysLovReference status;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public UserLovReference getGender() {
        return gender;
    }

    public void setGender(UserLovReference gender) {
        this.gender = gender;
    }

    public void setDateOfBirth (Date dateOfBirth){
        this.dateOfBirth = dateOfBirth;
    }

    public Date getDateOfBirth (){
        return this.dateOfBirth;
    }

    public String getAddressOffice() {
        return addressOffice;
    }

    public void setAddressOffice(String addressOffice) {
        this.addressOffice = addressOffice;
    }

    public String getAddressClinic() {
        return addressClinic;
    }

    public void setAddressClinic(String addressClinic) {
        this.addressClinic = addressClinic;
    }

    public String getDegrees() {
        return degrees;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSpecialities() {
        return specialities;
    }

    public void setSpecialities(String specialities) {
        this.specialities = specialities;
    }

    public void setDegrees(String degrees) {
        this.degrees = degrees;
    }

    public String getContactOffice() {
        return contactOffice;
    }

    public String getContactClinic() {
        return contactClinic;
    }

    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    public void setContactClinic(String contactClinic) {
        this.contactClinic = contactClinic;
    }

    public void setContactOffice(String contactOffice) {
        this.contactOffice = contactOffice;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public SysLovReference getStatus() {
        return status;
    }

    public void setStatus(SysLovReference status) {
        this.status = status;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}