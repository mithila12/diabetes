package com.squer.sfe.api.common.impl.typehandler;

import com.squer.sfe.api.common.entity.reference.DoctorReference;
import com.squer.sfe.api.common.entity.reference.PartyReference;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: ashutoshpavaskar
 * Date: 17/09/13
 * Time: 5:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class PartyTypeHandler implements TypeHandler {

    @Override
    public void setParameter(PreparedStatement preparedStatement, int i, Object o, JdbcType jdbcType) throws SQLException {

    }

    @Override
    public PartyReference getResult(ResultSet resultSet, String s) throws SQLException {
        try{
            String ownerId = resultSet.getString(s);
            if(ownerId==null || "".equals(ownerId))
                return null;
            String name = "";

            PartyReference reference = null;
            String firstName;
            String middleName;
            String lastName;
            switch (ownerId.substring(0,5)){
                case "DOCTR":
                    firstName = resultSet.getString("first_name");
                    lastName = resultSet.getString("last_name");
                    middleName = resultSet.getString("middle_name");
                    name = CommonUtil.getName(firstName,middleName, lastName);
                    reference = new DoctorReference(ownerId, name);
                    break;

                default:
                    name = resultSet.getString("name");
                    reference = new PartyReference(ownerId, name);
            }

            return reference;
        }catch (Exception e){
            throw new SQLException(e);
        }
    }

    @Override
    public Object getResult(ResultSet resultSet, int i) throws SQLException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Object getResult(CallableStatement callableStatement, int i) throws SQLException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
