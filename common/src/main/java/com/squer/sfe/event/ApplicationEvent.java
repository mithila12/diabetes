package com.squer.sfe.event;

import com.squer.platform.event.SquerEventType;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mangesh on 25/07/15.
 */
public class ApplicationEvent implements SquerEventType {

    private static Map<String, SquerEventType> eventsMap = new HashMap<>();

    private String id;

    public static ApplicationEvent ATTENDEE_STATUS_UPDATED = new ApplicationEvent("ATTENDEE_STATUS_UPDATED");
    public static ApplicationEvent CREATE_APPROVAL_CHAIN = new ApplicationEvent("CREATE_APPROVAL_CHAIN");
    public static ApplicationEvent RESET_HOLIDAY_ACTIVITY = new ApplicationEvent("RESET_HOLIDAY_ACTIVITY");
    public static ApplicationEvent SET_HOLIDAY_ACTIVITY = new ApplicationEvent("SET_HOLIDAY_ACTIVITY");
    public static ApplicationEvent DOCTOR_APPROVAL_CHAIN_CREATED = new ApplicationEvent("DOCTOR_APPROVAL_CHAIN_CREATED");

    private ApplicationEvent(String id){
        this.id = id;
    }

    @Override
    public String id() {
        return id;
    }
}
