sfe.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/login')
    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: 'templates/login-view.html',
            controller: "LoginController",
            resolve: {
                'pageLabels': function (LabelService) {
                    return LabelService.promise;
                }
            }
        })
        .state('home', {
            url: '/home',
            templateUrl: 'templates/home.html',
            resolve: {
                'pageLabels': function (LabelService) {
                    return LabelService.promise;
                }
            }
        })
        .state('report', {
            url: '/report',
            templateUrl: 'templates/home.html',
            resolve: {
                'pageLabels': function (LabelService) {
                    return LabelService.promise;
                }
            }
        })
        .state('pc', {
            url: '/pc',
            templateUrl: 'templates/home.html',
            resolve: {
                'pageLabels': function (LabelService) {
                    return LabelService.promise;
                }
            }
        })
        .state('pcadmin', {
            url: '/pcadmin',
            templateUrl: 'templates/home.html',
            resolve: {
                'pageLabels': function (LabelService) {
                    return LabelService.promise;
                }
            }
        })
        .state('mis', {
            url: '/mis',
            templateUrl: 'templates/home.html',
            resolve: {
                'pageLabels': function (LabelService) {
                    return LabelService.promise;
                }
            }
        })
        .state('sysadmin', {
            url: '/sysadmin',
            templateUrl: 'templates/home.html',
            resolve: {
                'pageLabels': function (LabelService) {
                    return LabelService.promise;
                }
            }
        })
        .state('company', {
            url: '/company',
            templateUrl: 'templates/home.html',
            resolve: {
                'pageLabels': function (LabelService) {
                    return LabelService.promise;
                }
            }
        })
        .state('ho', {
            url: '/ho',
            templateUrl: 'templates/home.html',
            resolve: {
                'pageLabels': function (LabelService) {
                    return LabelService.promise;
                }
            }
        })

        .state('administrator', {
            url: '/administrator',
            templateUrl: 'templates/home.html',
            resolve: {
                'pageLabels': function (LabelService) {
                    return LabelService.promise;
                }
            }
        })
        .state('sysadmin.new_standardExpense', {
            url: '/standardExpense/new',
            templateUrl: 'templates/sysadmin/standardExpense.html'
        })
        .state('sysadmin.search_division', {
            url: '/division/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'DIVSN',
                'query': 'divsn_select',
                'labels': 'division'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }

        })
        .state('sysadmin.new_division', {
            url: '/division/new',
            templateUrl: 'templates/sysadmin/division.html',
            data: {
                'entity': 'DIVSN',
                'query': 'divsn_select',
                'labels': 'division'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }

        })
        .state('sysadmin.division', {
            url: '/division/edit/:id',
            templateUrl: 'templates/sysadmin/division.html',
            data: {
                'entity': 'DIVSN',
                'query': 'divsn_select',
                'labels': 'division'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.search_employee', {
            url: '/employee/search/:context',
            templateUrl: 'templates/sysadmin/employeeSearch.html',
            data: {
                'entity': 'EMPLY',
                'query': 'emply_select',
                'labels': 'employee'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.search_preCallPlan', {
            url: '/precallplanning/search/:context',
            templateUrl: 'templates/home/precallsearch.html',
            data: {
                'entity': 'PRECALL',
                'query': 'precall_select',
                'labels': 'precall'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.search_patient', {
            url: '/patient/search/:context',
            templateUrl: 'templates/component/commonsearch_edit.html',
            data: {
                'entity': 'PATNT',
                'query': 'patnt_select',
                'labels': 'patient',
                "fixedQuery": 'location_id~location_id'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.new_patient', {
            url: '/patient/new',
            templateUrl: 'templates/patient/patient.html'
        })
        .state('home.edit_patient', {
            url: '/patient/edit/:id',
            templateUrl: 'templates/patient/patient.html'
        })
        .state('home.search_counter', {
            url: '/counter/search/:context',
            templateUrl: 'templates/component/commonsearch_edit.html',
            data: {
                'entity': 'CONTR',
                'query': 'contr_select',
                'labels': 'counter',
                "fixedQuery": 'location_id~location_id'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.new_counter', {
            url: '/counter/new',
            templateUrl: 'templates/patient/counter.html'
        })
        .state('home.edit_counter', {
            url: '/counter/edit/:id',
            templateUrl: 'templates/patient/counter.html'
        })
        .state('pcadmin.adminsearch_diabetespatient', {
            url: '/admindiabetespatient/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'DPATN',
                'query': 'dpatn_select',
                'labels': 'diabetespatient'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('pc.search_diabetespatient', {
            url: '/diabetespatient/search/:context',
            templateUrl: 'templates/component/commonsearch_edit.html',
            data: {
                'entity': 'DPATN',
                'query': 'dpatn_select',
                'labels': 'diabetespatient',
                'fixedQuery': 'counselor~id'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('pc.new_diabetespatient', {
            url: '/diabetespatient/new',
            templateUrl: 'templates/patient/diabetesPatient.html'
        })
        .state('pc.edit_diabetespatient', {
            url: '/diabetespatient/edit/:id',
            templateUrl: 'templates/patient/diabetesPatient.html'
        })
        .state('pc.search_patientcall',{
            url: '/diabetespatient/searchpatientcall/:context',
            templateUrl: 'templates/patient/patientCallSearch.html'
        })
        .state('pcadmin.search_patientcall',{
            url: '/diabetespatient/searchpatientcall/:context',
            templateUrl: 'templates/patient/patientCallSearch.html'
        })
        .state('pcadmin.edit_diabetespatient', {
            url: '/diabetespatient/edit/:id',
            templateUrl: 'templates/patient/diabetesPatient.html'
        })
        .state('sysadmin.new_employee', {
            url: '/employee/new',
            templateUrl: 'templates/sysadmin/employee.html'
        })
        .state('sysadmin.employee', {
            url: '/employee/edit/:id',
            templateUrl: 'templates/sysadmin/employee.html'
        })
        .state('home.employee', {
            url: '/employee/edit/:id',
            templateUrl: 'templates/sysadmin/employee.html'
        })
        .state('home.password', {
            url: '/employee/password/:id',
            templateUrl: 'templates/home/password.html'
        })
        .state('sysadmin.password_new', {
            url: '/password',
            templateUrl: 'templates/sysadmin/adminpassword.html'
        })
        .state('sysadmin.search_speciality', {
            url: '/speciality/search/:context/:type',
            templateUrl: 'templates/component/commonsearch.html',
                        data: {
                            'entity': 'USRLV',
                            'query': 'usrlv_select',
                            'labels': 'DOCTOR_SPECIALTY',
                            "fixedQuery": 'type:DOCTOR_SPECIALTY'
                        },
                        resolve: {
                            'columnLabels': function (SearchConfigService) {
                                return SearchConfigService.promise;
                            }
                        }
        })
        .state('sysadmin.new_speciality', {
            url: '/speciality/new/:type',
            templateUrl: 'templates/sysadmin/userlov.html',
            controller: "UserLovController"
        })
        .state('sysadmin.search_town', {
            url: '/town/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'TOWNS',
                'query': 'towns_select',
                'labels': 'towns'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })

        .state('sysadmin.new_town', {
            url: '/town/new',
            templateUrl: 'templates/sysadmin/town.html',
            data: {
                'entity': 'TWNMS',
                'query': 'twnms_select',
                'labels': 'towns'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })

        .state('sysadmin.town', {
            url: '/town/:id',
            templateUrl: 'templates/sysadmin/town.html',
            data: {
                'entity': 'TWNMS',
                'query': 'twnms_select',
                'labels': 'towns'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.doctor_speciality', {
            url: '/edit/:type/:id',
            templateUrl: 'templates/sysadmin/userlov.html'
        })
        .state('sysadmin.search_securityrole', {
            url: '/securityrole/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'SROLE',
                'query': 'srole_select',
                'labels': 'srole'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.new_securityrole', {
            url: '/securityrole/new',
            templateUrl: 'templates/sysadmin/securityrole.html'
        })
        .state('sysadmin.securityrole', {
            url: '/securityrole/:id',
            templateUrl: 'templates/sysadmin/securityrole.html'
        })
        .state('sysadmin.search_eswap',{
            url:'/employee/swap',
            templateUrl:'templates/sysadmin/employeeSwapping.html'
        })

        .state('sysadmin.search_designation', {
            url: '/designation/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'JOBTT',
                'query': 'jobtt_select',
                'labels': 'employee_designation'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.new_designation', {
            url: '/designation/new',
            templateUrl: 'templates/sysadmin/designation.html'

        })
        .state('sysadmin.jobtitle', {
            url: '/jobtitle/:id',
            templateUrl: 'templates/sysadmin/designation.html'
        })
        .state('sysadmin.search_locationType', {
            url: '/locationType/search/:context/:type',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'USRLV',
                'query': 'usrlv_select',
                'labels': 'location_type',
                "fixedQuery": 'type:LOCATION_TYPE'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }

        })
        .state('sysadmin.new_locationType', {
            url: '/location_type/new',
            templateUrl: 'templates/sysadmin/userlov.html'
        })
        .state('sysadmin.search_doctorClassification', {
            url: '/doctorClassification/search/:context/:type',
            templateUrl: 'templates/component/commonsearch.html',
                data: {
                    'entity': 'USRLV',
                    'query': 'usrlv_select',
                    'labels': 'doctor_classification',
                    "fixedQuery": 'type:doctor_classification'
                },
                resolve: {
                    'columnLabels': function (SearchConfigService) {
                        return SearchConfigService.promise;
                    }
                }
        })
        .state('sysadmin.new_doctorClassification', {
            url: '/doctorClassification/new/:type',
            templateUrl: 'templates/sysadmin/userlov.html'
        })
        .state('sysadmin.doctor_classification', {
            url: '/edit/:type/:id',
            templateUrl: 'templates/sysadmin/userlov.html'
        })
        .state('sysadmin.search_doctorInputs', {
            url: '/doctorInputs/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'DOCTI',
                'query': 'docti_select',
                'labels': 'doctor_inputs'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.new_doctorInputs', {
            url: '/doctorInputs/new',
            templateUrl: 'templates/sysadmin/doctorinput.html',
            controller: "DoctorInputController"
        })

        .state('sysadmin.docti', {
            url: '/doctorinputs/:id',
            templateUrl: 'templates/sysadmin/doctorinput.html'
        })
        .state('sysadmin.search_maritalStatus', {
            url: '/maritalStatus/search/:context/:type',
            templateUrl: 'templates/sysadmin/userlovsearch.html',
            controller: "UserLovSearchController"
        })
        .state('sysadmin.new_maritalStatus', {
            url: '/maritalStatus/new/:type',
            templateUrl: 'templates/sysadmin/userlov.html',
            controller: "UserLovController"
        })
        .state('sysadmin.marital_status', {
            url: '/edit/:type/:id',
            templateUrl: 'templates/sysadmin/userlov.html'
        })
        .state('sysadmin.search_states', {
            url: '/states/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'STATE',
                'query': 'state_select',
                'labels': 'state'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.new_state', {
            url: '/states/new',
            templateUrl: 'templates/sysadmin/state.html'
        })
        .state('sysadmin.state', {
            url: '/states/edit/:id',
            templateUrl: 'templates/sysadmin/state.html'
        })
        .state('sysadmin.search_employeeStatus', {
            url: '/employeeStatus/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'EMPLS',
                'query': 'empls_select',
                'labels': 'employeeStatus'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.new_employeeStatus', {
            url: '/employeeStatus/new/:type',
            templateUrl: 'templates/sysadmin/employeeStatus.html'
        })
        .state('sysadmin.employee_status', {
            url: '/employeeStatus/edit/:id',
            templateUrl: 'templates/sysadmin/employeeStatus.html'
        })

        .state('sysadmin.search_orgHeirarchy', {
            url: '/orgHierarchy/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'ORGHY',
                'query': 'orghy_select',
                'labels': 'orgHierarchy'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.new_orgHierarchy', {
            url: '/orgHierarchy/new',
            templateUrl: 'templates/sysadmin/orgHeirarchy.html'
        })
        .state('sysadmin.organizationHierarchy', {
            url: '/orgHeirarchy/edit/:id',
            templateUrl: 'templates/sysadmin/orgHeirarchy.html'
        })

        .state('sysadmin.search_location', {
            url: '/location/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'locat_saplocation',
                'query': 'locat_saplocation_select',
                'labels': 'location'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.new_location', {
            url: '/location/new',
            templateUrl: 'templates/sysadmin/location.html'
        })
        .state('sysadmin.location', {
            url: '/location/edit/:id',
            templateUrl: 'templates/sysadmin/location.html'
        })
        .state('sysadmin.search_doctor', {
            url: '/doctor/search/:context',
            templateUrl: 'templates/component/commonsearch_edit.html',
            data: {
                'entity': 'DOCTR',
                'query': 'doctr_select',
                'labels': 'doctor'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('company.search_doctorROI', {
            url: '/doctor/roi/:context',
            templateUrl: 'templates/home/doctorROIReport.html'
        })
        .state('home.search_doctor', {
            url: '/doctor/search/:context',
            templateUrl: 'templates/common/doctorSearch.html',
            data: {
                'entity': 'DOCTR',
                'query': 'doctr_search_select',
                'labels': 'doctor',
                "fixedQuery": 'location_id~location_id'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('report.patient_Reports', {
            url: '/ho/patientreport/:context',
            templateUrl: 'templates/patient/hopatientreport.html'
        })
        .state('report.counter_Reports', {
            url: '/ho/counterreport/:context',
            templateUrl: 'templates/patient/hocounterreport.html'
        })
        .state('report.joint_visit_attendee', {
            url: '/ho/jointvisitattendeereport/:context',
            templateUrl: 'templates/ho/jointvisitreport.html'
        })
        .state('home.new_doctor', {
            url: '/doctor/new',
            templateUrl: 'templates/common/doctornew.html'
        })

        .state('home.survey_Page1',{
            url:'/doctor/survey/page1/:id/:pageNo',
            templateUrl:'templates/common/page1.html'
        })
        .state('home.survey_Page2',{
            url:'/doctor/survey/page2/:id/:pageNo',
            templateUrl:'templates/common/page2.html'
        })
        .state('home.survey_Page3',{
            url:'/doctor/survey/page3/:id/:pageNo',
            templateUrl:'templates/common/page3.html'
        })

        .state('home.employee_surveyPg1',{
             url:'/employeeSurvey/page1',
             templateUrl:'templates/common/employeeSurveyPage1.html'
        })

         .state('home.employee_surveyPg2',{
             url:'/employeeSurvey/page2',
             templateUrl:'templates/common/employeeSurveyPage2.html'
        })

        .state('home.advanceSearch_doctor',{
            url: '/doctor/advancesearch',
            templateUrl: 'templates/common/doctorAdvancedSearch.html'
        })
        .state('home.doctorInfo_Employee',{
            url:'/doctor/doctorinformation/:id/:uid',
            templateUrl:'templates/common/doctornewforEmployee.html'
        })
        .state('home.doctor', {
            url: '/doctor/edit/:id',
            templateUrl: 'templates/common/doctornew.html'
        })
        .state('home.search_targetPlanning', {
            url: '/targetPlanning/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'DTRGT',
                'query': 'dtrgt_select',
                'labels': 'targetPlanning',
                "fixedQuery": 'location_id~location_id'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.new_targetPlanning', {
            url: '/targetPlanning/new',
            templateUrl: 'templates/home/targetPlanning.html'
        })
        .state('home.edit_stdTourPlan', {
            url: '/stdTourPlan/edit',
            templateUrl: 'templates/home/stdTourPlan.html'
        })
        .state('sysadmin.new_doctor', {
            url: '/doctor/new',
            templateUrl: 'templates/common/doctornew.html'
        })
        .state('sysadmin.doctor', {
            url: '/doctor/edit/:id',
            templateUrl: 'templates/common/doctornew.html'
        })
        .state('sysadmin.search_chemist', {
            url: '/chemist/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'CHEMT',
                'query': 'chemt_select',
                'labels': 'chemist'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }

        })
        .state('home.search_leave', {
            url: '/leave/search/:context',
            templateUrl: 'templates/home/leaveSearchNew.html',
            data: {
                'entity': 'LEAVE',
                'query': 'leave_select',
                'labels': 'leave'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }

        })
        .state('home.leave', {
            url: '/leave/edit/:id',
            templateUrl: 'templates/common/leave.html'
        })
        .state('home.new_leave', {
            url: '/leave/new',
            templateUrl: 'templates/common/leave.html'
        })
        .state('sysadmin.leave', {
                    url: '/adminLeave',
                    templateUrl: 'templates/sysadmin/adminLeaveApplication.html'
                })
        .state('sysadmin.new_chemist', {
            url: '/chemist/new',
            templateUrl: 'templates/common/chemist.html'
        })
        .state('sysadmin.chemist', {
            url: '/chemist/edit/:id',
            templateUrl: 'templates/common/chemist.html'
        })
        .state('home.search_chemist', {
            url: '/chemist/search/:context',
            templateUrl: 'templates/component/commonsearch_edit.html',
            data: {
                'entity': 'CHEMT',
                'query': 'chemt_select',
                'labels': 'chemist',
                "fixedQuery": 'location_id~location_id'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.new_chemist', {
            url: '/chemist/new',
            templateUrl: 'templates/common/chemist.html'
        })
        .state('home.chemist', {
            url: '/chemist/edit/:id',
            templateUrl: 'templates/common/chemist.html'
        })
        .state('sysadmin.search_stockist', {
            url: '/stockist/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'STKST',
                'query': 'stkst_select',
                'labels': 'stockist'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }

        })
        .state('sysadmin.new_stockist', {
            url: '/stockist/new',
            templateUrl: 'templates/common/stockist.html'
        })
        .state('sysadmin.stockist', {
            url: '/stockist/edit/:id',
            templateUrl: 'templates/common/stockist.html'
        })
        .state('sysadmin.search_product', {
            url: '/product/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'product_brand_div',
                'query': 'product_brand_div_select',
                'labels': 'product'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.new_product', {
            url: '/product/new',
            templateUrl: 'templates/common/product.html'
        })
        .state('sysadmin.product', {
            url: '/product/edit/:id',
            templateUrl: 'templates/common/product.html'
        })
        /*.state('sysadmin.expenseType', {
         url: '/expenseType/edit/:id',
         templateUrl: 'templates/sysadmin/userlov.html',
         data: {
         'entity': 'USRLV',
         'query': 'usrlv_select',
         'labels': 'expenseType'
         },
         resolve: {
         'columnLabels': function (SearchConfigService) {
         return SearchConfigService.promise;
         }
         }
         })*/
        .state('sysadmin.search_expenseType', {
            url: '/expenseType/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'EXPTY',
                'query': 'expty_select',
                'labels': 'expenseType'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }

        })
        .state('sysadmin.new_expenseType', {
            url: '/expenseType/new',
            templateUrl: 'templates/sysadmin/userlov.html'
        })
        .state('sysadmin.expenseType', {
            url: '/expenseType/edit/:id',
            templateUrl: 'templates/sysadmin/userlov.html'
        })

        .state('sysadmin.search_brand', {
            url: '/brand/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'BRAND',
                'query': 'brand_select',
                'labels': 'brand',
                "fixedQuery": 'isAdmin:true'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.new_brand', {
            url: '/brand/new',
            templateUrl: 'templates/common/brand.html'
        })
        .state('sysadmin.brand', {
            url: '/brand/edit/:id',
            templateUrl: 'templates/common/brand.html'
        })

        .state('home.dashboard', {
            url: '/home/dashboard',
            templateUrl: 'templates/home/dashboard.html'
        })

        .state('home.test_dashboard', {
            url: '/home/test_dashboard',
            templateUrl: 'templates/home/test_dashboard.html'
        })

        .state('home.search_myPlan', {
            url: '/myTourPlan/search/:context',
            templateUrl: 'templates/home/myTourPlan.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })

        .state('home.search_myTeamPlan', {
                    url: '/myTeamTourPlan/search/:context',
                    templateUrl: 'templates/home/myTeamTourPlan.html',
                    resolve: {
                        'pageLabels': function (SearchConfigService) {
                            return SearchConfigService.promise;
                        }
                    }
                })
        .state('home.search_viewPlan', {
            url: '/viewTourPlan/search/:context',
            templateUrl: 'templates/home/viewtourplan.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.new_tourplan', {
            url: '/myTourPlan/new/:month/:year',
            templateUrl: 'templates/home/newplan.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.manager_tourPlan', {
            url: '/managerTourPlan/new',
            templateUrl: 'templates/home/managerTourPlan.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.dateplan', {
            url: '/myTourPlan/datePlan',
            templateUrl: 'templates/home/dateplan.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })

        .state('home.search_beats', {
            url: '/beats/search/:context',
            templateUrl: 'templates/home/beatsSearchNew.html',
            data: {
                'entity': 'BEATS',
                'query': 'beats_docCount_select',
                'labels': 'beats'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.new_beats', {
            url: '/beats/new',
            templateUrl: 'templates/home/beats.html'
        })
        .state('home.beats', {
            url: '/beats/:id',
            templateUrl: 'templates/home/beats.html'
        })
        .state('home.brand', {
            url: '/beats/edit/:id',
            templateUrl: 'templates/home/beats.html'

        })

        .state('home.search_myTownRequest', {
            url: '/townRequest/saerch/:context',
            templateUrl: 'templates/common/townRequestSearch.html',
            data: {
                'entity': 'town_req',
                'query': 'town_req_select',
                'labels': 'town_req'
            }
        })

        .state('home.newTownRequest', {
            url: '/townRequest/new',
            templateUrl: 'templates/common/newTownRequest.html'
        })
        .state('home.search_customerVisits', {
            url: '/customervisits/search/:context',
            templateUrl: 'templates/home/myTourView.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.search_coachingForm', {
            url: '/coachingform/search/:context',
            templateUrl: 'templates/home/coachingFeedbackSearch.html',
            data: {
                'entity': 'COAFD',
                'query': 'coafd_select',
                'labels': 'coachingfeedback'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.new_coachingFeedback', {
            url: '/coachingform/new',
            templateUrl: 'templates/home/coachingfeedback.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.coachingFeedback', {
            url: '/coachingform/edit/:id',
            templateUrl: 'templates/home/coachingfeedback.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.search_myCoachingFeedback', {
            url: '/coachingfeedback/search/:context',
            templateUrl: 'templates/home/myCoachingFeedbackSearch.html',
            data: {
                'entity': 'coarp',
                'query': 'my_all_coarp_select',
                'labels': 'mycoachingfeedback'
            },
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.mycoachingFeedback', {
            url: '/coachingfeedback/view/:id',
            templateUrl: 'templates/home/mycoachingFeedback.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.search_doctorEngagement', {
            url: '/doctorEngagement/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'CMEAC',
                'query': 'cmeac_select',
                'labels': 'doctorEngagement'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })

        .state('home.doctorEngagement', {
            url: '/doctorEngagement/new',
            templateUrl: 'templates/home/doctorEngagement.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })

        .state('home.expense', {
            url: '/expense/new',
            templateUrl: 'templates/home/expenseReportUpdated.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.search_expenseApproval', {
            url: '/expense/approval',
            templateUrl: 'templates/home/expenseApproval.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })

        .state('home.new_report', {
            url: '/customervisits/new_customervisit/:id',
            templateUrl: 'templates/home/myTourReportNew.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.search_tourplanApproval', {
            url: '/tourplan/approvals/search/:context',
            templateUrl: 'templates/home/approvalinbox.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.search_leaveapproval', {
            url: '/leave/approvals/search/:context',
            templateUrl: 'templates/home/leaveapprovalinbox.html',
            data: {
                'entity': 'LEAVE',
                'query': 'leave_select',
                'fixedQuery': 'approver_id~id'
            },
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })

        .state('home.report_effort', {
            url: '/reports/effort',
            templateUrl: 'templates/report/effortReport.html',
            resolve: {}
        })
        .state('home.lastReporting', {
            url: '/reports/lastReporting',
            templateUrl: 'templates/reports/lastReporting.html',
            resolve: {}
        })
        .state('home.report_sales', {
            url: '/reports/sales',
            templateUrl: 'templates/report/salesreport.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.stockist_report_sales', {
            url: '/reports/stockistsales',
            templateUrl: 'templates/report/stockistSalesReport.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })


        .state('company.report_doctor', {
            url: '/doctor_report/:id',
            templateUrl: 'templates/report/doctorreport.html',

            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })


        .state('home.company_report_doctor', {
            url: '/company_report_doctor/:id',
            templateUrl: 'templates/report/doctorreport.html',

            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })

        .state('sysadmin.search_competitor', {
            url: '/competitors/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'COMPT',
                'query': 'compt_select',
                'labels': 'competitor'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.new_competitor', {
            url: '/competitors/new',
            templateUrl: 'templates/sysadmin/competitor.html',
            data: {
                'entity': 'COMPT',
                'query': 'compt_select',
                'labels': 'competitor'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.competitor', {
            url: '/competitors/:id',
            templateUrl: 'templates/sysadmin/competitor.html',
            data: {
                'entity': 'COMPT',
                'query': 'compt_select',
                'labels': 'competitor'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.new_brandgroup', {
            url: '/brandgroup/new',
            templateUrl: 'templates/sysadmin/brandgroup.html',
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.search_brandgroup', {
            url: '/brandgroup/search/:context',
            templateUrl: 'templates/sysadmin/brandgroup.html',
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })

        .state('sysadmin.new_coachingForm', {
            url: '/coachingForm/new',
            templateUrl: 'templates/sysadmin/coachingForm.html'
        })

        .state('sysadmin.search_approvalconfig', {
            url: '/approvalconfig/search/:context',
            templateUrl: 'templates/component/commonsearch_edit.html',
            data: {
                'entity': 'ACDFN',
                'query': 'acdfn_select',
                'labels': 'chainDefinition'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.new_approvalconfig', {
            url: '/approvalconfig/new',
            templateUrl: 'templates/sysadmin/approvalconfig.html',
            data: {
                'entity': 'ACDFN',
                'query': 'acdfn_select',
                'labels': 'chainDefinition'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.approvalconfig', {
            url: '/approvalconfig/:id',
            templateUrl: 'templates/sysadmin/approvalconfig.html',
            data: {
                'entity': 'ACDFN',
                'query': 'acdfn_select',
                'labels': 'chainDefinition'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.playground', {
            url: '/playground',
            templateUrl: 'playground/addresstest.html'
        })


        .state('home.reports', {
            url: '/reports/:id',
            templateUrl: 'templates/reports/report.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })


        .state('ho.reports', {
            url: '/reports/:id',
            templateUrl: 'templates/reports/report.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })

        .state('company.reports', {
            url: '/reports/:id',
            templateUrl: 'templates/reports/report.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })


        .state('sysadmin.reports', {
            url: '/reports/:id',
            templateUrl: 'templates/reports/report.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })

        .state('pcadmin.reports', {
            url: '/reports/:id',
            templateUrl: 'templates/reports/report.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })


        .state('company.overview_report', {
            url: '/overview_report/:id',
            templateUrl: 'templates/report/overviewReport.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })


        .state('company.sales_report', {
            url: '/sales_report',
            templateUrl: 'templates/widgets/salesReport.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })

        .state('home.company_overview_report', {
            url: '/company_overview_report/:id',
            templateUrl: 'templates/report/overviewReport.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })

        .state('sysadmin.uploads', {
            url: '/uploads/:type',
            templateUrl: 'templates/component/upload.html'
        })

        .state('sysadmin.commonuploads', {
            url: '/commonuploads/search/:context',
            templateUrl: 'templates/component/commonupload.html',
            data : {
                'labels' : 'commonupload',
                'type' : '',
                'entity' : 'USTAT',
                'fixedQuery' : ''
            }
        })

        .state('sysadmin.employeeUpload', {
            url: '/employeeUpload',
            templateUrl: 'templates/uploads/employeeUpload.html'
        })

        .state('sysadmin.consolidate', {
            url: '/consolidations',
            templateUrl: 'templates/component/consolidation.html'
        })

        .state('sysadmin.search_distanceType', {
            url: '/distanceMaster/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'DISTN_BY_NAME',
                'query': 'distn_by_name_select',
                'labels': 'distanceMaster'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }

        })

        .state('sysadmin.new_distance', {
            url: '/distanceMaster/new',
            templateUrl: 'templates/sysadmin/distance.html'
        })
        .state('sysadmin.distance', {
            url: '/distanceMaster/edit/:id',
            templateUrl: 'templates/sysadmin/distance.html'
        })

        .state('sysadmin.search_standardExpenseType', {
            url: '/standardExpenseType/search/:context',
            templateUrl: 'templates/component/commonsearch_edit.html',
            data: {
                'entity': 'STDEP_STEXV',
                'query': 'stdep_stexv_select',
                'labels': 'standardExpense'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })

        .state('sysadmin.search_tourplan', {
            url: '/tourplan/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'TOURP',
                'query': 'tourp_select',
                'labels': 'tourPlan'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.tourplanApproval', {
            url: '/tourplanApproval/update/:id',
            templateUrl: 'templates/sysadmin/tourplanapproval.html'
        })
        .state('sysadmin.search_expenseApproval', {
            url: '/expenseApproval/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'expnsebyempcode',
                'query': 'expnsebyempcode_select',
                'labels': 'expenseApproval'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })

        .state('mis.search_expenseApproval', {
            url: '/expenseApproval/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'expnsebyempcode',
                'query': 'expnsebyempcode_select',
                'labels': 'expenseApproval'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })

        .state('sysadmin.search_requests', {
            url: '/request/downloads/:context',
            templateUrl: 'templates/sysadmin/requestDownload.html'
        })

        .state('sysadmin.expenseApprovalEdit', {
            url: '/expenseApproval/update/:id',
            templateUrl: 'templates/sysadmin/expenseApproval.html'
        })

        .state('mis.expenseApprovalEdit', {
            url: '/expenseApproval/update/:id',
            templateUrl: 'templates/sysadmin/expenseApproval.html'
        })

        .state('sysadmin.standardExpense', {
            url: '/standardExpense/edit/:id',
            templateUrl: 'templates/sysadmin/standardExpense.html'
        })

        .state('home.search_expense', {
            url: '/expense/search/:context',
            templateUrl: 'templates/home/monthlyExpenses.html',
            //templateUrl: 'templates/common/comingsoon.html',
            data: {
                'entity': 'EXPRP',
                'query': 'exprp_select',
                'labels': 'expenseReport'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('mis.dashboard', {
            url: '/expenseApproval/summery',
            templateUrl: 'templates/mis/overview.html'
        })
        .state('mis.search_approval', {
            url: '/expenseApproval/search',
            templateUrl: 'templates/mis/searchApproval.html'
        })
        .state('mis.overview', {
            url: '/expense/summery',
            templateUrl: 'templates/mis/overview.html'
        })

        .state('home.search_doctorapproval', {
            url: '/doctorapproval/search/:context',
            templateUrl: 'templates/common/doctorapprovalsearch.html',
            data: {
                'entity': 'DOCTR',
                'query': 'doctr_approval_select',
                'labels': 'doctor'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.search_patientapproval', {
            url: '/patientapproval/search/:context',
            templateUrl: 'templates/patient/patientapprovalsearch.html',
            data: {
                'entity': 'PATNT',
                'query': 'patnt_select',
                'labels': 'patient'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.search_myDistanceRequest', {
            url: '/distanceRequest/search/:context',
            templateUrl: 'templates/common/distanceRequestSearch.html'
        })
        .state('home.new_myDistanceRequest', {
            url: '/distanceRequest/new',
            templateUrl: 'templates/common/newDistanceRequest.html'
        })
        .state('home.search_issueReporting', {
            url: '/issues/search/:context',
            templateUrl: 'templates/component/commonsearch_edit.html',
            data: {
                'entity': 'ISSUE',
                'query': 'issue_select',
                'labels': 'issue',
                "fixedQuery": 'REPORTED_BY~id'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.new_issue', {
            url: 'issue/Request/new',
            templateUrl: 'templates/common/newIssueRequest.html'
        })

        .state('sysadmin.search_issueReporting', {
            url: '/search/issues/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'ISSUE',
                'query': 'issue_select',
                'labels': 'issue'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.issues', {
            url: '/manage/issues/:id',
            templateUrl: 'templates/sysadmin/issue.html'
        })
        .state('home.search_discount', {
            url: '/discount/search',
            templateUrl: 'templates/home/discountRequest.html'
        })

        .state('home.search_discountApproval', {
            url: '/search/discount/approval/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'discount_approval',
                'query': 'discount_approval_select',
                'labels': 'discount',
                "fixedQuery": 'approvar~id'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.focusedBrandApproval', {
            url: '/search/focusedbrand/approval/:context',
            templateUrl: 'templates/home/focusedBrandApproval.html'
        })

        .state('home.approveDiscount', {
            url: '/discount/approve/:id',
            templateUrl: 'templates/home/discountApprove.html'
        })
        .state('home.search_inputs', {
            url: '/search/inputs/:context',
            templateUrl: 'templates/home/inventory.html'
        })
        .state('ho.dashboard', {
            url: '/patientApproval/search/:context',
            templateUrl: 'templates/patient/hopatientapprovalsearch.html',
            data: {
                'entity': 'ho_patnt_approval',
                'query': 'ho_patnt_approval_select',
                'labels': 'patient'
            }
        })
        .state('ho.search_approval', {
            url: '/patientapproval/search/:context',
            templateUrl: 'templates/patient/hopatientapprovalsearch.html',
            data: {
                'entity': 'ho_patnt_approval',
                'query': 'ho_patnt_approval_select',
                'labels': 'patient'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.unmappedDoc', {
            url: '/unmappeddoctors/search',
            templateUrl: 'templates/home/unmappedDoctor.html'
        })
        .state('sysadmin.unlock_request', {
            url: '/unlock/request',
            templateUrl: 'templates/sysadmin/unlockRequest.html'
        })
        .state('home.unlockReporting', {
            url: '/unlock/request',
            templateUrl: 'templates/home/unlockRequest.html'
        })
        .state('sysadmin.search_holiday', {
            url: '/holiday/search/:context',
            templateUrl: 'templates/sysadmin/holidaySearch.html',
            data: {
                'entity': 'HOLYD',
                'query': 'holyd_select',
                'labels': 'holiday'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.weekendconfig', {
            url: '/holiday/weekendconfig',
            templateUrl: 'templates/sysadmin/weekendconfiguration.html'
        })
        .state('sysadmin.holidayPlaner', {
            url: 'holiday/plan',
            templateUrl: 'templates/sysadmin/holidayPlan.html'
        })

        .state('sysadmin.new_holidayForm', {
            url: '/holiday/new',
            templateUrl: 'templates/sysadmin/holiday.html'
        })
        .state('sysadmin.holiday', {
            url: '/holiday/edit/:id',
            templateUrl: 'templates/sysadmin/holiday.html'
        })

        .state('sysadmin.search_survey', {
            url: '/survey/search/:context',
            templateUrl: 'templates/component/commonsearch_edit.html',
            data: {
                'entity': 'SURVY',
                'query': 'survy_select',
                'labels': 'surveyMaster'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.editSurvey',{
            url: '/survey/edit/:id',
            templateUrl:'templates/sysadmin/createsurvey.html',
            controller:'surveyCtrl',
            controllerAs:'ctrl'
        })
        .state('sysadmin.newSurvey',{
            url: '/survey/create',
            templateUrl:'templates/sysadmin/createsurvey.html',
            controller:'surveyCtrl',
            controllerAs:'ctrl'
        })
        .state('mis.reports', {
            url: '/mis/report',
            templateUrl: 'templates/mis/consolidatedReport.html'
        })
        .state('sysadmin.search_rconf', {
            url: '/rconf/search/:context',
            templateUrl: 'templates/component/commonsearch_edit.html',
            data: {
                'entity': 'RCONF',
                'query': 'rconf_select',
                'labels': 'roleConfiguration'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.new_rconf', {
            url: '/rconf/new',
            templateUrl: 'templates/sysadmin/roleConfiguration.html'
        })
        .state('sysadmin.search_slMaster', {
            url: '/situationalLeadership/search',
            templateUrl: 'templates/sysadmin/SLForm.html'
        })

        .state('sysadmin.search_activityMaster', {
            url: '/activityMaster/search/:context',
            templateUrl: 'templates/component/commonsearch_edit.html',
            data: {
                   'entity': 'ACTVY',
                   'query': 'actvy_select',
                   'labels': 'activity'
            },
            resolve: {
                    'columnLabels': function (SearchConfigService) {
                     return SearchConfigService.promise;
                      }
            }
        })

        .state('sysadmin.new_activity',{
                url:'/activity/new',
                templateUrl:'templates/sysadmin/doctoractivity.html'
        })

        .state('sysadmin.editActivity',{
                url:'/activity/edit/:id',
                templateUrl:'templates/sysadmin/doctoractivity.html'
        })

        .state('home.search_slReport', {
            url: '/situationalLeadership/report',
            templateUrl: 'templates/home/SLReport.html'
        })
        .state('sysadmin.reportDownload', {
            url: '/report/downloads',
            templateUrl: 'templates/sysadmin/reportDownload.html'
        })

        .state('home.search_stockist', {
            url: '/stockist/search/:context',
            templateUrl: 'templates/home/stockistsearch.html',
            data: {
                'entity': 'STKST',
                'query': 'stkst_select',
                'labels': 'stockist',
                "fixedQuery": 'location_id~location_id'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('home.pms_system', {
            url: '/pms/performancemanagement',
            templateUrl: 'templates/common/performancemanagement.html'
        })

        .state('home.mySlScore',{
            url: '/sl/myslscore/:context',
            templateUrl: 'templates/home/mySlReport.html'
        })

        .state('home.doctorInput',{
            url: '/doctor/input/:context',
            templateUrl: 'templates/home/doctorinput.html'
        })

        .state('sysadmin.search_announcement', {
            url: '/announcement/search/:context',
            templateUrl: 'templates/component/commonsearch_edit.html',
            data: {
                'entity': 'ansmt',
                'query': 'ansmt_select',
                'labels': 'announcement'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('sysadmin.announcements',{
            url: '/announcements/add',
            templateUrl: 'templates/sysadmin/announcements.html'
        })

        .state('sysadmin.announcementsEdit',{
            url: '/announcements/edit/:id',
            templateUrl: 'templates/sysadmin/announcements.html'
        })


        .state('home.dynamicReports', {
            url: '/dynamicReports/:id',
            templateUrl: 'templates/reports/dynamicReport.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })

        .state('sysadmin.dynamicReports', {
            url: '/adminDynamicReports/:id',
            templateUrl: 'templates/reports/adminDynamicReport.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })

        .state('home.reportHome', {
            url: '/reportHome/:context',
            templateUrl: 'templates/reports/reportHome.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })

        .state('sysadmin.reportHome', {
            url: '/reportHome/:context',
            templateUrl: 'templates/reports/reportHome.html',
            resolve: {
                'pageLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })

        .state('mis.search_locationType', {
            url: '/locationType/search/:context/:type',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'USRLV',
                'query': 'usrlv_select',
                'labels': 'location_type',
                "fixedQuery": 'type:LOCATION_TYPE'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }

        })
        .state('mis.new_locationType', {
            url: '/location_type/new',
            templateUrl: 'templates/sysadmin/userlov.html'
        })
        .state('mis.search_expenseType', {
            url: '/expenseType/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'EXPTY',
                'query': 'expty_select',
                'labels': 'expenseType'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }

        })
        .state('mis.new_expenseType', {
            url: '/expenseType/new',
            templateUrl: 'templates/sysadmin/userlov.html'
        })
        .state('mis.expenseType', {
            url: '/expenseType/edit/:id',
            templateUrl: 'templates/sysadmin/userlov.html'
        })

        .state('mis.search_distanceType', {
            url: '/distanceMaster/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'DISTN_BY_NAME',
                'query': 'distn_by_name_select',
                'labels': 'distanceMaster'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }

        })

        .state('mis.new_distance', {
            url: '/distanceMaster/new',
            templateUrl: 'templates/sysadmin/distance.html'
        })
        .state('mis.distance', {
            url: '/distanceMaster/edit/:id',
            templateUrl: 'templates/sysadmin/distance.html'
        })

        .state('mis.search_standardExpenseType', {
            url: '/standardExpenseType/search/:context',
            templateUrl: 'templates/component/commonsearch_edit.html',
            data: {
                'entity': 'STDEP_STEXV',
                'query': 'stdep_stexv_select',
                'labels': 'standardExpense'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }
        })
        .state('mis.standardExpense', {
            url: '/standardExpense/edit/:id',
            templateUrl: 'templates/sysadmin/standardExpense.html'
        })

        .state('sysadmin.search_leaveDetails',{
            url: '/leave/details/:context',
            templateUrl: 'templates/sysadmin/leavedetails.html'
        })
         .state('sysadmin.doctorSurveyUnlock',{
            url: '/doctor/survey/unlock',
            templateUrl: 'templates/sysadmin/doctorSurveyUnlock.html'
        })

        .state('home.secondarySales',{
            url: '/secondarysales/:context',
            templateUrl: 'templates/home/secondarysales.html'
        })
        .state('home.secondarySalesDetails',{
            url: '/secondarysalesdetails',
            templateUrl: 'templates/home/secondarysalesDetails.html'
        })
        .state('home.cmeReporting',{
            url: '/cmeReporting',
            templateUrl: 'templates/home/cmeReporting.html'
        })
        .state('home.search_target',{
            url: '/target/search',
            templateUrl: 'templates/home/targets.html'
        })
         .state('sysadmin.search_admindoctorapproval', {
            url: '/admindoctorapproval/search/:context',
            templateUrl: 'templates/common/adminDoctorApprovalsSearch.html',
            data: {
                'entity': 'DOCTR',
                'query': 'admin_doctr_approval_select',
                'labels': 'doctor'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }

         })

         .state('sysadmin.leaveApplication', {
                     url: '/adminLeaveApplication/',
            templateUrl: 'templates/common/adminLeaveApplication.html'
         })
        .state('sysadmin.leaveReport',{
            url: '/report/leave',
            templateUrl: 'templates/sysadmin/leaveReport.html'
        })

        .state('mis.leaveReport',{
            url: '/report/leave',
            templateUrl: 'templates/sysadmin/leaveReport.html'
        })

        .state('home.leaveReport',{
            url:'/report/leave',
            templateUrl:'templates/home/leaveReportManagerView.html'
        })

        .state('home.doctorUniverse',{
            url:'/report/doctoruniverse/:context',
            templateUrl:'templates/home/doctorUniverse.html'
        })

        .state('sysadmin.doctorUniverse',{
            url:'/report/doctoruniverse/:context',
            templateUrl:'templates/home/doctorUniverse.html'
        })
        .state('mis.search_leaveDetails',{
            url: '/leave/details/:context',
            templateUrl: 'templates/sysadmin/leavedetails.html'
        })


        .state('administrator.adminEmployee', {
            url: '/adminEmployee/search/:context',
            templateUrl: 'templates/component/commonsearch_edit.html',
            data: {
                'entity': 'admin_employee',
                'query':  'admin_employee_select',
                'labels': 'admin_employee'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }

        })

        .state('administrator.new_adminEmployee', {
            url: '/adminemployee/new',
            templateUrl: 'templates/sysadmin/adminEmployee.html'
        })
        .state('administrator.edit_adminEmployee', {
            url: '/adminemployee/edit/:id',
            templateUrl: 'templates/sysadmin/adminEmployee.html'
        })


        .state('sysadmin.adminEmployee', {
            url: '/adminEmployee/search/:context',
            templateUrl: 'templates/component/commonsearch_edit.html',
            data: {
                'entity': 'admin_employee',
                'query':  'admin_employee_select',
                'labels': 'admin_employee'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }

        })

        .state('sysadmin.new_adminEmployee', {
            url: '/adminemployee/new',
            templateUrl: 'templates/sysadmin/adminEmployee.html'
        })
        .state('sysadmin.edit_adminEmployee', {
            url: '/adminemployee/edit/:id',
            templateUrl: 'templates/sysadmin/adminEmployee.html'
        })

        .state('sysadmin.EbStatus',{
            url: '/ebstatus',
            templateUrl: 'templates/sysadmin/ebstatus.html'
        })

        .state('home.doctor_survey', {
            url:'/doctorSurvey/search/:context',
            templateUrl: 'templates/component/commonsearch_edit.html',
            data: {
                'entity': 'doctorlist_survey',
                'query':  'doctorlist_survey_select',
                'labels': 'survey',
                "fixedQuery": 'emplId~id'
            },
            resolve: {
                'columnLabels': function (SearchConfigService) {
                    return SearchConfigService.promise;
                }
            }

        })

        .state('home.doctor_survey_new',{
            url:'/doctorSurvey/new',
            templateUrl:'templates/common/doctorSurvey.html'
        })

         .state('home.doctor_survey_edit',{
            url:'/doctorSurvey/edit/:id',
            templateUrl:'templates/common/doctorSurvey.html'
        })

        .state('sysadmin.unlockSecondary',{
            url:'/unlocksecondary/search/:context',
            templateUrl: 'templates/component/commonsearch.html',
            data: {
                'entity': 'SESLM',
                'query': 'seslm_select',
                'labels': 'unlockSecondarySales'
            }
        })

        .state('home.doctorInfo',{
            url:'/doctorInfo/',
            templateUrl:'templates/home/doctorInfo.html',
            controller:'DoctorInfoController',
            controllerAs: 'vm'
        })

        .state('sysadmin.dashboard',{
            url:'/doctr/',
            templateUrl:'templates/doctor/home.html',
            controller:'HomeController',
            controllerAs:'vm'

        })

        .state('sysadmin.patDetails',{
            url:'/patDetails/',
            templateUrl:'templates/doctor/patientDetails.html',
            controller:'PatientsController',
            controllerAs:'vm'
        })

        .state('sysadmin.records',{
            url:'/records/:id',
            templateUrl:'templates/doctor/records.html',
            controller:'PatientsController',
            controllerAs:'vm'
        })
        .state('sysadmin.modal',{
            url:'/doctormodal/',
            templateUrl:'templates/doctor/doctorModal.html',
            controller:'modalController',
            controllerAs:'vm'
        })
        .state('sysadmin.detailtotalactiveuser',{
            url:'/detailtotalactiveuser/',
            templateUrl:'templates/doctor/TotalActiveUser.html',
            controller:'TotalActiveUserController',
            controllerAs:'vm'
        })
        .state('sysadmin.listmodal',{
            url:'/doctorlistmodal/',
            templateUrl:'templates/doctor/doctorListModal.html',
            controller:'doctorListModalController',
            controllerAs:'vm'
        })
        .state('sysadmin.questionnaire',{
            url:'/questionnairemodal/',
            templateUrl:'templates/doctor/questionnaireModal.html',
            controller:'questionnaireModalController',
            controllerAs:'vm'
        })
        .state('sysadmin.activedoctors',{
        url:'/activeDoctors/',
        templateUrl:'templates/doctor/activeDoctors.html',
        controller:'activeDoctorController'
        })
        .state('sysadmin.activepatients',{
            url:'/activePatients/',
            templateUrl:'templates/doctor/activePatients.html',
            controller:'activePatientsController'
        })
        .state('sysadmin.PendingRegist',{
            url:'/PendingRegist/',
            templateUrl:'templates/doctor/PendingRegist.html',
            controller:'PendingRegistController'
        })
        .state('sysadmin.DoctorwiseInfo',{
            url:'/DoctorwiseInfo/',
            templateUrl:'templates/doctor/DoctorwiseInfo.html',
            controller:'DoctorwiseInfoController'
        })
        .state('sysadmin.DoctorwiseRisk',{
            url:'/DoctorwiseRisk/',
            templateUrl:'templates/doctor/DoctorwiseRisk.html',
            controller:'DoctorwiseRiskController'
        })
        .state('sysadmin.sqdtsqInfo',{
            url:'/sqdtsqInfo/',
            templateUrl:'templates/doctor/sqdtsqInfo.html',
            controller:'sqdtsqInfoController'
        })
        .state('sysadmin.appDetail',{
            url:'/appDetail/',
            templateUrl:'templates/doctor/appDetail.html',
            controller:'appDetailModalController',
            controllerAs:'vm'
        })
        .state('sysadmin.avgenrollpatient',{
            url:'/avgenrollpatient/',
            templateUrl:'templates/doctor/avgenrollpatient.html',
            controller:'avgenrollpatientController',
            controllerAs:'vm'
        })
        .state('sysadmin.maxUsageFunc',{
            url:'/maxUsageFunc/',
            templateUrl:'templates/doctor/maxUsageFunctionality.html',
            controller:'maxUsageController',
            controllerAs:'vm'
        })
        .state('sysadmin.patnt',{
            url:'/patnt/',
            templateUrl:'templates/patient/home.html',

        })

})

