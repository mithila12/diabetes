sfe.controller('ExpenseSearchController',
    function ($scope,$state, $http,LabelService, RepositoryService, LocalStore, $modal,daysOfWeek,months) {

        $scope.labels = LabelService.getLabels("expenseReport")
        $scope.links = LabelService.getLabels("links");
        $scope.user;
        $scope.year;
        $scope.month;
        $scope.empCode;
        $scope.division;
        $scope.zone;
        $scope.status;
        $scope.jobTitle=0;
        $scope.monthList=[];
        $scope.years=[];
        $scope.divisions=[];
        $scope.reportStatus=[];
        $scope.zones={};
        $scope.status=0;

        $scope.init=function(){
            var isDefault=true;
            if(LocalStore.fetch("mis_year")!=null) {
                $scope.year = LocalStore.fetch("mis_year");
                $scope.currentYr = LocalStore.fetch("mis_year");
                $scope.years.push(parseInt($scope.year)-1);
                $scope.years.push(parseInt($scope.year));
                $scope.years.push(parseInt($scope.year)+1);

            }

            if(LocalStore.fetch("mis_month")!=null) {
                $scope.monthList=months;
                $scope.month=parseInt(LocalStore.fetch("mis_month"));
                $scope.currentMonth = months[LocalStore.fetch("mis_month")];
                isDefault=false;
            }

            if(LocalStore.fetch("mis_div")!=null) {
                $scope.division = LocalStore.fetch("mis_div");
                isDefault=false;
            }

            if(LocalStore.fetch("mis_zone")!=null) {
                $scope.zone = LocalStore.fetch("mis_zone");
                $scope.zones=LocalStore.fetch("mis_zones");
                isDefault=false;
            }

            if(LocalStore.fetch("mis_status")!=null) {
                $scope.status = LocalStore.fetch("mis_status");
                isDefault=false;
            }

            if(LocalStore.fetch("mis_jobTitle")!=null) {
                $scope.jobTitle = LocalStore.fetch("mis_jobTitle");
                isDefault=false;
            }

            if(LocalStore.fetch("mis_empCode")!=null) {
                $scope.empCode = LocalStore.fetch("mis_empCode");
                isDefault=false;
            }

            if(isDefault){
                d = new Date();
                var yr = d.getFullYear();
                $scope.year = d.getFullYear();
                $scope.month = d.getMonth();
                $scope.user = LocalStore.fetch('USER_INFO').id;
                $scope.monthList=months;
                $scope.currentYr=yr;
                $scope.currentMonth= months[d.getMonth()];
                $scope.years.push(yr-1);
                $scope.years.push(yr);
                $scope.years.push(yr+1);
                $scope.status=0;
                $scope.jobTitle=0;

            }else {
                $scope.search();
            }


            RepositoryService.find("divsn",{},'simple').then(function (data) {
                $scope.divisions = data.data;
            });

            RepositoryService.find("usrlv",{type: 'ExpenseReport_status', paged:'N'},'advanced').then(function(data){
                $scope.reportStatus=data.data;
            });
        };

        $scope.search=function(){

            if($scope.empCode==null || !$scope.empCode.trim()) {
                $scope.empCode=0;
                LocalStore.remove("mis_empCode");
            }else{
                LocalStore.store("mis_empCode",$scope.empCode);
            }

            if($scope.division==null || $scope.division=="0"){
                $scope.division=0;
                LocalStore.remove("mis_div");
            }else{
                LocalStore.store("mis_div",$scope.division);
            }

            if($scope.status==null){
                $scope.status=0;
                LocalStore.remove("mis_status");
            }else{
                LocalStore.store("mis_status",$scope.status);
            }

            if($scope.jobTitle==null){
                $scope.jobTitle=0;
                LocalStore.remove("mis_jobTitle");
            }else{
                LocalStore.store("mis_jobTitle",$scope.jobTitle);
            }

            if($scope.zone==null || $scope.zone==0){
                $scope.zone=0;
                LocalStore.remove("mis_zone");
            }else{
                LocalStore.store("mis_zone",$scope.zone);
            }

            if($scope.zones.length>0){
                var zones=[];
                for(z in $scope.zones){
                    var zone={id:$scope.zones[z].id,name:$scope.zones[z].name};
                    zones.push(zone);
                }
                LocalStore.store("mis_zones",zones);
            }

            if($scope.empCode==0 && $scope.division==0 && $scope.status==0){
                $scope.empCode=0;
            }


            LocalStore.store("mis_year",$scope.year);
            LocalStore.store("mis_month",$scope.month);

            $http({
                method: 'GET',
                url: restUrl + "/expense/expenseReport/searchMisExpenseApproval/" + $scope.month + "/" + $scope.year + "/" + $scope.division + "/" +$scope.zone+"/"+ $scope.empCode + "/" + $scope.status+"/"+$scope.jobTitle,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            }).then(function (response) {
                $scope.displayData = response.data;
                if($scope.empCode==0){
                    $scope.empCode='';
                }
            });

        }

        $scope.getZone=function(id){
            RepositoryService.find('divsn_zone',{hid:'ORGHY00000000000000000000000000000001',division:id},'advance').then(function(data){
                $scope.zones=data.data;
            })
        }
 });

sfe.controller('overViewController',function($scope,$state, $http,LabelService, RepositoryService, LocalStore, $modal,daysOfWeek,months){
    $scope.displyData=[];
    $scope.years=[];
    $scope.jobTitle=0;
    $scope.init=function() {

        d = new Date();
        $scope.year=d.getFullYear();
        $scope.month=d.getMonth()+1;
        var yr = d.getFullYear();
        $scope.years.push(yr-1);
        $scope.years.push(yr);
        $scope.years.push(yr+1);
        $scope.monthList=months;
        $scope.currentYr=yr;
        $scope.currentMonth= months[d.getMonth()];
        $scope.jobTitle=0;
        $scope.search();
    }

    $scope.search=function() {

        $scope.displyData=[];
        var queryParams={};
        if($scope.jobTitle!=0) {
            queryParams = {year: $scope.year, month: $scope.month, jobTitle: $scope.jobTitle};
        }else{
            queryParams = {year: $scope.year, month: $scope.month}
        }
        RepositoryService.find('expense_summery', queryParams, 'advance').then(function (data) {
            $scope.displyData = data.data;
        })
    }
})

sfe.controller('downloadReport',function($scope,$state, $http,LabelService, RepositoryService, LocalStore, $modal,daysOfWeek,months){
    $scope.years=[];

    $scope.init=function() {

        RepositoryService.find("divsn",{},'simple').then(function (data) {
            $scope.divisions = data.data;
        });

        d = new Date();
        $scope.year=d.getFullYear();
        $scope.month=d.getMonth()+1;
        var yr = d.getFullYear();
        $scope.years.push(yr-1);
        $scope.years.push(yr);
        $scope.years.push(yr+1);
        $scope.monthList=months;
        $scope.currentYr=yr;
        $scope.currentMonth= months[d.getMonth()];
    }

    $scope.download=function(){
        if($scope.div==null || $scope.div==''){
            alert("Please select Division");
        }else if($scope.year<='2015' && $scope.month <= 7){
            alert("Because of change in data structures in  August 2015, reports for previous month of August 2015 are not available.")
        }else{
            window.open("/webapp/download/misreport?cert="+encodeURIComponent(LocalStore.fetch("USER_INFO").certificate)+"&logedDiv="+LocalStore.fetch("headerdata").jobTitle.id+"&month="+$scope.month+"&year="+$scope.year+"&division="+$scope.div);
        }
    }
})