    sfe.controller('MonthlyExpenseController',
    function ($scope,$state, $http,LabelService, RepositoryService, LocalStore, $modal,daysOfWeek,months) {

        $scope.monthList=new Array();
        $scope.years=new Array();
        $scope.year;
        $scope.month;
        $scope.user;
        $scope.displayData={};

        $scope.init=function(){
            var d = new Date();
            var yr = d.getFullYear();
            var i=0;
            $scope.years.push(yr-1);
            $scope.years.push(yr);
            $scope.years.push(yr+1);
            $scope.monthList=months;
            $scope.currentYr=yr;
            $scope.currentMonth= months[d.getMonth()];
            $scope.year=d.getFullYear();
            $scope.month=d.getMonth();
            $scope.user=LocalStore.fetch('USER_INFO').id;
        };
        $scope.addNew=function(){
            $state.go('home.expense');
        };

        $scope.getExpenses=function()
        {
            $http({
                method: 'GET',
                url: restUrl + "/expense/expenseReport/searchExpense/" +$scope.month+"/"+$scope.year+"/"+$scope.user,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            }).success(function(response){
                $scope.displayData=response;
            }).error(function(response){
                $scope.displayData={};
            });
        };
    });

sfe.controller('ExpenseApprovalController',function($scope,$state, $http,LabelService, RepositoryService, LocalStore, $modal,daysOfWeek,months){
    $scope.years = [];
    $scope.monthList = [];
    $scope.status = {};

    $scope.init=function(){
        $scope.status = "all";
        var isDefault=true;
        $scope.monthList = months;
        if(LocalStore.fetch("apr_year")!=null) {
            $scope.year = LocalStore.fetch("apr_year");
            $scope.currentYr = LocalStore.fetch("apr_year");
            isDefault=false;

            $scope.years.push(parseInt($scope.year)-1);
            $scope.years.push(parseInt($scope.year));
            $scope.years.push(parseInt($scope.year)+1);

        }

        if(LocalStore.fetch("apr_month")!=null) {
            $scope.month = LocalStore.fetch("apr_month");
            $scope.currentMonth = months[LocalStore.fetch("apr_month")];
            isDefault=false;
        }

        if(isDefault) {
            var d = new Date();
            var yr = d.getFullYear();
            var i = 0;
            $scope.years.push(yr - 1);
            $scope.years.push(yr);
            $scope.years.push(yr + 1);
            $scope.currentYr = yr;
            $scope.currentMonth = months[d.getMonth()];
            $scope.year = d.getFullYear();
            $scope.month = d.getMonth();
        }
    };

    $scope.getExpense=function()
    {

        if($scope.status!=null){
            var approvalStatus = $scope.status;
        }

        if($scope.year==0 && $scope.month!=0){
            alert("Year is required.");
        }
        else
        {
            LocalStore.store("apr_month",$scope.month);
            LocalStore.store("apr_year",$scope.year);

            $http({
                method: 'GET',
                url: restUrl + "/expense/expenseReport/searchExpenseApproval/" +$scope.month+"/"+$scope.year+"/"+LocalStore.fetch('USER_INFO').id+"/"+approvalStatus,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            }).then(function(response){
                $scope.displayData=response.data;
            });

        }
    };

});

sfe.controller('ExpenseReportController',
    function ($scope,$state, $http,LabelService, RepositoryService, LocalStore, $modal,daysOfWeek,months) {

        $scope.labels = LabelService.getLabels("expenseReport")
        $scope.links = LabelService.getLabels("links");

        $scope.definition =[];
        $scope.controls = {};
        $scope.controls.type = [];
        $scope.controls.fromLocation = [];
        $scope.controls.amount = [];
        $scope.showTowns = [];
        $scope.showLocationType = [];
        $scope.beats = [];
        $scope.locationTypes = [];
        $scope.approvals=[];
        $scope.days = [];
        $scope.total=0.00;
        $scope.expense={};
        $scope.expnDefn={}
        $scope.expnDefn.owner={};
        $scope.expnDefn.fromDate={};
        $scope.expnDefn.toDate={};
        $scope.expnDefn.status={};
        $scope.expID;
        $scope.sequenceNo=0;
        $scope.displayData={};
        $scope.duplicateLocation={};
        $scope.routes=[];
        $scope.misc=[];
        $scope.title;
        $scope.monthList=new Array();
        $scope.years=new Array();
        $scope.year;
        $scope.month;
        $scope.user;
        $scope.logedInUser=LocalStore.fetch('USER_INFO').id;
        $scope.routeLists={};
        $scope.locationValue={};
        $scope.summery={};
        $scope.totalTA=0;
        $scope.totalAllowance=0;
        $scope.totalMisc=0;
        $scope.isMIS=false;
        $scope.showSubmit=true;
        $scope.showExpenseError=false;
        $scope.activeUser=LocalStore.fetch('USER_INFO').id;
        $scope.expOwner={};
        $scope.ebChckList=[];
        $scope.sundryDocCount=0;
        var cnt=0;
        $scope.types =['Allowance','Travel','Others'];
        $scope.sundriesEditable=false;

        $scope.initSearch =function(type){

        }

        $scope.init = function(){
            var d;
            if(LocalStore.fetch("searchYear")!=null)
            {
                d = new Date(LocalStore.fetch("searchYear"), LocalStore.fetch("searchMonth"), 01);
                $scope.year=LocalStore.fetch("searchYear");
                $scope.month=LocalStore.fetch("searchMonth")+1;
                $scope.user=LocalStore.fetch("searchUser");
                //$scope.isMIS=LocalStore.fetch("isMIS");
                if(LocalStore.fetch("isMIS")==='true'){
                    $scope.isMIS=true;
                }
                LocalStore.remove("searchYear");
                LocalStore.remove("searchMonth");
                LocalStore.remove("searchUser");
                LocalStore.remove("isMIS");
                $scope.getMonthDetails();
            }
            else {
                d = new Date();
                $scope.year=d.getFullYear();
                $scope.month=d.getMonth()+1;
                $scope.user=LocalStore.fetch('USER_INFO').id;
            }

            var yr = d.getFullYear();
            var i=0;
            $scope.years.push(yr-1);
            $scope.years.push(yr);
            $scope.years.push(yr+1);
            $scope.monthList=months;
            $scope.currentYr=yr;
            $scope.currentMonth= months[d.getMonth()];

        }

        $scope.selectType = function(index){
            $scope.showTowns[index] = false;
            $scope.showLocationType[index] = false;
            if($scope.controls.type[index]=='Travel')
                $scope.showTowns[index] = true;
            else if ($scope.controls.type[index]=='Allowance')
                $scope.showLocationType[index] = true;

        }

        $scope.getMonthDetails=function(){
            $scope.totalTA=0;
            $scope.totalAllowance=0;
            $scope.totalMisc=0;
            $scope.finalOctroi=0;
            $scope.finalPromotional=0;
            $scope.finalMedicalCheckUp=0;
            $scope.totalOther=0;
            $scope.totalSundries=0;
            $scope.jobtitle="";

            $http({
                method: 'GET',
                url: restUrl + "/expense/expenseService/getExpense/" +$scope.month+"/"+$scope.year+"/"+$scope.user,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            }).success(function(response){
                if(response.length>0) {
                    $scope.displayData = response;
                    $scope.duplicateDisplayData = JSON.parse(JSON.stringify(response));
                    $http({
                        method: 'GET',
                        url: restUrl + "/expense/expenseService/getExpenseApproval/" + $scope.displayData[0].expenseId,
                        headers: {
                            "content-type": "application/json",
                            "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                        }
                    }).success(function (expenseApprovalData) {
                        $scope.approvals =JSON.parse(JSON.stringify(expenseApprovalData)) ;
                        for (d in $scope.displayData) {
                            $scope.totalTA = $scope.totalTA + $scope.displayData[d].fare + $scope.displayData[d].returnFare+$scope.displayData[d].totalTransit;
                            $scope.totalAllowance = $scope.totalAllowance + $scope.displayData[d].allowance+$scope.displayData[d].totalMeetingAllowance+$scope.displayData[d].totalOutStationAllowance;
                            $scope.finalOctroi=$scope.finalOctroi+$scope.displayData[d].totalOctroi;
                            $scope.finalPromotional=$scope.finalPromotional+$scope.displayData[d].totalPromotional;
                            $scope.finalMedicalCheckUp=$scope.finalMedicalCheckUp+$scope.displayData[d].totalMedicalCheckUp;
                            $scope.totalOther=$scope.totalOther + $scope.displayData[d].totalOther;
                            $scope.totalSundries=$scope.totalSundries + $scope.displayData[d].totalSundries;
                            $scope.totalMisc = $scope.totalMisc + $scope.displayData[d].miscTotal;
                        }

                        $scope.summery = {
                            totalTA: $scope.totalTA,
                            totalMisc: $scope.totalMisc,
                            totalAllowance: $scope.totalAllowance,
                            finalPromotional:$scope.finalPromotional,
                            finalMedicalCheckUp:$scope.finalMedicalCheckUp,
                            finalOctroi:$scope.finalOctroi,
                            totalOther:$scope.totalOther,
                            totalSundries:$scope.totalSundries,
                            sundries: response[response.length - 1].sundries
                        };
                        $scope.sundriesEditable=response[response.length - 1].sundriesEditable;
                        $scope.sundryDocCount=response[response.length - 1].sundriesDocCount;
                        if($scope.approvals[0]!=null) {
                            for (apr in $scope.approvals) {
                                var approval = $scope.approvals[apr];
                                if (apr == 0) {
                                    $scope.approvals[apr].prvDa = $scope.summery.totalAllowance;
                                    $scope.approvals[apr].prvTa = $scope.summery.totalTA;
                                    $scope.approvals[apr].prvOctroi = $scope.summery.finalOctroi;
                                    $scope.approvals[apr].prvPromotional = $scope.summery.finalPromotional;
                                    $scope.approvals[apr].prvMedicalCheckUp = $scope.summery.finalMedicalCheckUp;
                                    $scope.approvals[apr].prvOther = $scope.summery.totalOther;
                                    $scope.approvals[apr].prvSundries = $scope.summery.sundries;
                                }
                                else {
                                    $scope.approvals[apr].prvTa = $scope.approvals[apr - 1].ta;
                                    $scope.approvals[apr].prvDa = $scope.approvals[apr - 1].da;
                                    $scope.approvals[apr].prvOctroi = $scope.approvals[apr - 1].octroi;
                                    $scope.approvals[apr].prvPromotional = $scope.approvals[apr - 1].promotional;
                                    $scope.approvals[apr].prvMedicalCheckUp = $scope.approvals[apr - 1].medicalCheckUp;
                                    $scope.approvals[apr].prvOther = $scope.approvals[apr - 1].other;
                                    $scope.approvals[apr].prvSundries = $scope.approvals[apr - 1].sundries;

                                }
                                if (apr == 0 && approval.ta == 0 && approval.taAdded == 0 && approval.taDeducted == 0) {
                                    approval.ta = $scope.summery.totalTA;
                                }
                                if (apr > 0 && approval.ta == 0 && approval.taAdded == 0 && approval.taDeducted == 0) {
                                    approval.ta = $scope.approvals[apr - 1].ta;
                                }

                                if (apr == 0 && approval.da == 0 && approval.daAdded == 0 && approval.daDeducted == 0) {
                                    approval.da = $scope.summery.totalAllowance;
                                }
                                if (apr > 0 && approval.da == 0 && approval.daAdded == 0 && approval.daDeducted == 0) {
                                    approval.da = $scope.approvals[apr - 1].da;
                                }
                                if (apr == 0 && approval.octroi == 0 && approval.octroiAdded == 0 && approval.octroiDeducted == 0) {
                                    approval.octroi = $scope.summery.finalOctroi;
                                }

                                if (apr > 0 && approval.octroi == 0 && approval.octroiAdded == 0 && approval.octroiDeducted == 0) {
                                    approval.octroi = $scope.approvals[apr - 1].octroi;
                                }

                                if (apr == 0 && approval.promotional == 0 && approval.promotionalAdded == 0 && approval.promotionalDeducted == 0) {
                                    approval.promotional = $scope.summery.finalPromotional;
                                }

                                if (apr > 0 && approval.promotional == 0 && approval.promotionalAdded == 0 && approval.promotionalDeducted == 0) {
                                    approval.promotional = $scope.approvals[apr - 1].promotional;
                                }

                                if (apr == 0 && approval.medicalCheckUp == 0 && approval.medicalCheckUpAdded == 0 && approval.medicalCheckUpDeducted == 0) {
                                    approval.medicalCheckUp = $scope.summery.finalMedicalCheckUp;
                                }

                                if (apr > 0 && approval.medicalCheckUp == 0 && approval.medicalCheckUpAdded == 0 && approval.medicalCheckUpDeducted == 0) {
                                    approval.medicalCheckUp = $scope.approvals[apr - 1].medicalCheckUp;
                                }

                                if (apr == 0 && approval.other == 0 && approval.otherAdded == 0 && approval.otherDeducted == 0) {
                                    approval.other = $scope.summery.totalOther;
                                }

                                if (apr > 0 && approval.other == 0 && approval.otherAdded == 0 && approval.otherDeducted == 0) {
                                    approval.other = $scope.approvals[apr - 1].other;
                                }

                                if (apr == 0 && approval.sundries == 0 && approval.sundriesAdded == 0 && approval.sundriesDeducted == 0) {
                                    approval.sundries = $scope.summery.sundries;
                                }

                                if (apr > 0 && approval.sundries == 0 && approval.sundriesAdded == 0 && approval.sundriesDeducted == 0) {
                                    approval.sundries = $scope.approvals[apr - 1].sundries;
                                }
                            }

                            var aprLen=$scope.approvals.length-1;
                            var cnt=0;
                            var prvApproved=false;
                            for(cnt=aprLen;cnt>=0;cnt--){
                                var approval=$scope.approvals[cnt];

                                if(prvApproved){
                                    approval.prvApproved=true;
                                }else{
                                    approval.prvApproved=false;
                                }

                                if(approval.status==='APPROVED'){
                                    prvApproved=true;
                                }
                            }


                        }else{
                            $scope.approvals={};
                        }
                        RepositoryService.restore(response[0].expenseId).then(function (expData) {
                            $scope.expense = expData.data;

                            var fdate=moment($scope.expense.fromDate.text,"DD/MM/YYYY");
                            var tdate=moment($scope.expense.toDate.text,"DD/MM/YYYY");

                            var fromDate=fdate.get('year')+"-"+(fdate.get('month')+1)+"-"+fdate.get('date');
                            var toDate=tdate.get('year')+"-"+(tdate.get('month')+1)+"-"+tdate.get('date');

                            RepositoryService.find("emply_locationByPeriod", {id: $scope.expense.owner.id,activity_from_date:fromDate,activity_to_date:toDate}, 'simple').success(function (empData) {
                                var date=null;
                                for (emp in empData){
                                    if(date==null){
                                        $scope.expOwner=empData[emp];
                                        date=empData[emp].activeFrom.date;
                                    }else{
                                        if(empData[emp].activeFrom.date > date){
                                            date=empData[emp].activeFrom.date;
                                            $scope.expOwner=empData[emp];
                                        }
                                    }
                                }
                                $scope.jobtitle=$scope.expOwner.jobTitle.id;
                                $scope.empLocation=$scope.expOwner.location.id;

                                if($scope.logedInUser!=$scope.expense.owner.id){
                                    var currentApproval={};
                                    for (apr in $scope.approvals) {
                                        var apr = $scope.approvals[apr];
                                        if(apr.jobTitle.id==$scope.jobtitle){
                                            currentApproval=apr;
                                        }
                                    }
                                    var isEditable=response[response.length - 1].sundriesEditable;
                                    if(isEditable && !currentApproval.prvApproved && !currentApproval.status=='APPROVED'){
                                        $scope.sundriesEditable=true;
                                    }else if(!isEditable || currentApproval.prvApproved || currentApproval.status=='APPROVED'){
                                        $scope.sundriesEditable=false;
                                    }
                                    if($scope.jobtitle=='JOBTT000000008aea432014dd69a749d0494a' && isEditable){
                                        $scope.sundriesEditable=true;
                                    }
                                }
                                if($scope.logedInUser==$scope.expense.owner.id && $scope.expense.status.id=='USRLVEXPSTS00000000000000000000000001'){
                                    $scope.sundriesEditable=response[response.length - 1].sundriesEditable;
                                }else if($scope.logedInUser==$scope.expense.owner.id && $scope.expense.status.id!='USRLVEXPSTS00000000000000000000000001'){
                                    $scope.sundriesEditable=false;
                                }

                                $http({
                                    method: 'GET',
                                    url: restUrl + "/expense/expenseService/getEBstatus/" +$scope.month+"/"+$scope.year+"/"+$scope.expense.owner.id+"/"+$scope.jobtitle+"/"+$scope.expense.status.id,
                                    headers: {
                                        "content-type": "application/json",
                                        "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                                    }
                                }).success(function(ebData){
                                    $scope.ebChckList=ebData;
                                });

                                if("JOBTT00000000000000000000000000000005"!=$scope.jobtitle){
                                    $scope.isRm=true;
                                }else{
                                    $scope.isRm=false;
                                }
                            });
                        });
                    });
                }
            });

            RepositoryService.find("usrlv",{type: 'location_type', paged:'N'},'advanced').then(function(data){
                $scope.locationValue=data.data;
                var tmploc={name:"Select",id:"0"};
                $scope.locationValue.splice(0,0,tmploc);
            });

        };

        $scope.submitReport=function(){
            var sCnf=true;
            if($scope.sundriesEditable && $scope.summery.sundries==0){
                sCnf=confirm("You have not updated your sundries amount. Do you want to save?");
            }

            if(sCnf) {
                var cnf = false;
                cnf = confirm("Are you sure you want to submit this report? You will be not able to make any changes after submission.");
                if (cnf) {
                    $http({
                        method: 'POST',
                        data: $scope.expense,
                        url: restUrl + "/expense/expenseService/submitExpense",
                        headers: {
                            "content-type": "application/json",
                            "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                        }
                    }).success(function (data) {
                        $scope.expense = data;
                        $scope.expense.status = {id: 'USRLVEXPSTS00000000000000000000000002', displayName: "SUBMITTED"};
                        $scope.sundriesEditable=false;
                        $http({
                            method: 'GET',
                            url: restUrl + "/expense/expenseService/getExpenseApproval/" + data.id,
                            headers: {
                                "content-type": "application/json",
                                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                            }
                        }).then(function (response) {
                            $scope.approvals = response.data;
                        });
                    });
                }
            }
        };

        $scope.showRouteSelection = function(data,displayData,index){
            $scope.routes=data.routes;
            $scope.title=data.displayDate;
            var status=$scope.expense.status.id;

            $http({
                method: 'GET',
                url: restUrl + "/expense/expenseService/getRoutes/"+$scope.expense.owner.id+"/"+data.qDate+"/"+LocalStore.fetch("headerdata").jobTitle.id,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            }).success(function(response){
                $scope.routeList=response;

                if(index>0) {
                    if (displayData[index - 1].location == 'USRLV000000055936e80149cd895d48007ffd' || displayData[index - 1].location == 'USRLV000000055936e80149cd895d48007ffe') {
                        var tmproute;

                        if (displayData[index - 1].routes.length > 0) {
                            if(displayData[index - 1].returnJourney){
                                tmproute = displayData[index - 1].routes[0];
                            }else {
                                tmproute = displayData[index - 1].routes[displayData[index - 1].routes.length - 1];
                            }
                            var tmp=0;
                            for(r in  $scope.routeList)
                            {
                                if($scope.routeList[r].id==tmproute.fromTown.id)
                                {
                                    tmp=tmp+1;
                                }
                            }
                            if(tmp==0) {
                                $scope.routeList.push({id: tmproute.fromTown.id, name: tmproute.fromTown.displayName});
                            }
                        }
                    }
                }

                var modalInstance = $modal.open({
                    templateUrl: 'route.html',
                    controller: 'RouteController',
                    size: 'xs',
                    backdrop: false,
                    backdropClick: false,
                    dialogFade: true,
                    keyboard: false,
                    resolve: {
                        input: function(){
                            return data;
                        },
                        displayDate:function(){
                            return  $scope.title;
                        },
                        routeList:function(){
                            return $scope.routeList;
                        },
                        displayData:function(){
                            return displayData;
                        },
                        summery:function(){
                            return $scope.summery;
                        },
                        expStstus:function(){
                            return status;
                        },
                        ebStatus:function(){
                            return $scope.ebChckList[index]
                        },
                        isRm:function(){
                            return $scope.isRm;
                        }
                    }

                });
            });
        };


        $scope.showCycleRouteSelection = function(data,displayData){

            $scope.title=data.displayDate;
            var status=$scope.expense.status.id;
            var modalInstance = $modal.open({
                templateUrl: 'cycleMeetRout.html',
                controller: 'CycleRouteController',
                size: 'xs',
                backdrop: false,
                backdropClick: false,
                dialogFade: true,
                keyboard: false,
                resolve: {
                    input: function(){
                        return data;
                    },
                    displayDate:function(){
                        return $scope.title;
                    },
                    displayData:function(){
                        return displayData;
                    },
                    expStatus:function(){
                        return status;
                    },
                    summery:function(){
                        return $scope.summery;
                    }
                }

            });
        }

        $scope.showMicsSelection = function(data,displayData,index){
            $scope.misc=data.miscList;
            $scope.title=data.displayDate;
            var status=$scope.expense.status.id;
            RepositoryService.find("expense_expty",{'jobTitle':$scope.jobtitle,'location':$scope.empLocation},'advanced').then(function(expdata){
                $scope.expList = expdata.data;

                LocalStore.store("miscList", $scope.misc);
                var modalInstance = $modal.open({
                    templateUrl: 'mics.html',
                    controller: 'MiscController',
                    size: 'xs',
                    backdrop: false,
                    backdropClick: false,
                    dialogFade: true,
                    keyboard: false,
                    resolve: {
                        input: function () {
                            return data;
                        },
                        displayDate: function () {
                            return  $scope.title;
                        },
                        expList:function(){
                            return $scope.expList;
                        },
                        summery:function(){
                            return $scope.summery;
                        },
                        displayData:function(){
                            return displayData;
                        },
                        expStstus:function(){
                            return status;
                        },
                        index:function(){
                            return index;
                        }
                    }
                });
            });
        };

        $scope.showExpenseDocuments=function(data){
            $scope.title=data.displayDate;
            $scope.docs=data.documents;
            $scope.misc=data.miscList;
            var status=$scope.expense.status.id;

            if(data.routes.length>0) {
                $scope.routeExpenseId=data.routes[0].expenseDetails.id;
            }
            else if(data.nonCallExpDtlId!=null){
                $scope.routeExpenseId=data.nonCallExpDtlId;
            }else{
                $scope.routeExpenseId=null;
            }

            var returnJourney=false;
            if(data.routes.length>0) {
                returnJourney=data.returnJourney;
            }

            var documentInstance = $modal.open({
                templateUrl: 'docs.html',
                controller: 'doscController',
                size: 'xs',
                backdrop: false,
                backdropClick: false,
                dialogFade: true,
                keyboard: false,
                resolve: {
                    input: function () {
                        return data;
                    },
                    displayDate: function () {
                        return  $scope.title;
                    },
                    documents:function(){
                        return $scope.docs;
                    },
                    returnJourney:function(){
                        return returnJourney;
                    },
                    miscList :function(){
                        return $scope.misc;
                    },
                    routeExpenseId:function(){
                        return $scope.routeExpenseId;
                    },
                    expStstus:function(){
                        return status;
                    }
                }
            })
        };

        $scope.showSundryDocuments=function(displayData){
            var status=$scope.expense.status.id;
            var owner=displayData[displayData.length-1].sundryDetailId;
            var documentInstance = $modal.open({
                templateUrl: 'docsSundries.html',
                controller: 'doscSundriesController',
                size: 'xs',
                backdrop: false,
                backdropClick: false,
                dialogFade: true,
                keyboard: false,
                resolve: {
                    expStatus:function(){
                        return status;
                    },
                    displayData:function(){
                        return displayData;
                    },
                    owner:function(){
                        return owner;
                    }
                }
            })
        };

        $scope.showVisitedTowns=function(event,date,displayDate){
            $http({
                method: 'GET',
                url: restUrl + "/expense/expenseService/getvisiteddata/"+$scope.expense.owner.id+"/"+date,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            }).success(function(response){

                var visitedTowns = $modal.open({
                    templateUrl: 'visitedTowns.html',
                    controller: 'visitedTownsController',
                    size: 'sm',
                    backdrop: false,
                    backdropClick: false,
                    dialogFade: true,
                    keyboard: false,
                    resolve: {
                        towns: function () {
                            return response.towns;
                        },
                        date:function (){
                            return displayDate;
                        },
                        employeeList:function(){
                            return response.ebEmp;
                        },
                        isRm:function(){
                            return $scope.isRm;
                        }
                    }
                })
            });
        }

        $scope.showUpdateSundries=function(action){
            if(action=='show')
            {
                $scope.updatedSundries="0";
                $("#sundriesUpdateBox").show(1000);
            }
            else
            {$("#sundriesUpdateBox").hide(1000);}
        }

        $scope.updateSundries=function(expenseId,amount){

            if(isNaN(amount) || amount<0 || amount%1!=0) {
                alert("Sundries amount should be integer (round number)");
            }else {
                $http({
                    method: 'GET',
                    url: restUrl + "/expense/expenseService/updatesundries/" + expenseId + "/" + amount,
                    headers: {
                        "content-type": "application/json",
                        "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                    }
                }).success(function (response) {
                    $("#sundriesUpdateBox").hide(1000);
                    $scope.summery.sundries = amount;
                }).error(function (response) {
                    alert("There is some problem occurred. Please try again after refreshing the page");
                })
            }
        }

        $scope.getSeq=function(){
            cnt=cnt+1;
            $scope.sequenceNo=cnt;
        };

        $scope.getExpenseDetails=function(year,month,user,isMIS)
        {
            LocalStore.store("searchYear",year);
            LocalStore.store("searchMonth",month);
            LocalStore.store("searchUser",user);
            LocalStore.store("isMIS",isMIS);
            $state.go('home.expense');
        };

        $scope.addExpType=function(details,prvDetails,nextDetails,index,data)
        {
            var duplicateDetails=JSON.parse(JSON.stringify($scope.displayData[index]));
            var check=true;
            var prvExpType={};
            var date=new Date(details.expDate.date);

            if(date.getDay()==1){
                prvDetails=data[index-2];
            }

            console.log(prvDetails);

            for (l in $scope.locationValue) {
                if (details.location == $scope.locationValue[l].id) {
                    details.allowance = $scope.locationValue[l].value;
                }
            }

            if (details.location == 'USRLV000000055936e80149cd895d48007ffd' && prvDetails != null &&  prvDetails.location!='USRLV000000055936e80149cd895d48007ffd') {
                if (prvDetails.location != 'USRLV000000055936e80149cd895d48007ffe') {
                    check = false;
                    alert("You can select EX-outstation only if previous location type is outstation");
                    details.location="0";
                    details.allowance=0;
                }
            }
            if (details.location != 'USRLV000000055936e80149cd895d48007ffe' && nextDetails != null) {
                if (nextDetails.location == 'USRLV000000055936e80149cd895d48007ffd') {
                    check = false;
                    alert("You can not change outstation when next location type is EX-outstation. delete Ex-outstation first.");
                    details.location="USRLV000000055936e80149cd895d48007ffe";
                    details.allowance=duplicateDetails.allowance;
                }
            }

            if (check){
                if (details.expDtId == null) {
                    if(details.location!="0") {
                        var cnf=true;
                        if(details.routes.length>0){
                            cnf=confirm("This will delete your routes and documents");
                        }
                        if(cnf) {
                            $http({
                                method: 'GET',
                                url: restUrl + "/expense/expenseService/getLocationValue/" + $scope.user + "/" + details.location + "/" + details.expDate.date,
                                headers: {
                                    "content-type": "application/json",
                                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                                }
                            }).success(function (response) {
                                details.allowance = response.value;
                                $http({
                                    method: 'POST',
                                    data: details,
                                    url: restUrl + "/expense/expenseService/createLocationType",
                                    headers: {
                                        "content-type": "application/json",
                                        "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                                    }
                                }).success(function (response) {
                                    details.allowance = parseFloat(response.allowance);
                                    details.routes = response.routes;
                                    details.documents = response.documents;
                                    details.expDtId = response.expDtId;
                                    details.location = response.location;
                                    details.docCount = response.docCount;
                                    details.allowance = response.allowance;
                                    details.returnAllowance = response.returnAllowance;
                                    details.fare = parseFloat(response.fare);
                                    details.returnFare = response.returnFare;
                                    details.kms = response.kms;
                                    details.returnKms = response.returnKms;
                                    details.total = parseFloat(response.miscTotal) + parseFloat(response.allowance) + parseFloat(response.fare)+parseFloat(details.returnFare);
                                    var tot = 0.00;
                                    var ta = 0.00;
                                    for (d in $scope.displayData) {
                                        ta = ta + parseFloat($scope.displayData[d].fare)+parseFloat($scope.displayData[d].returnFare)+parseFloat($scope.displayData[d].totalTransit);
                                        tot = tot + parseFloat($scope.displayData[d].allowance)+parseFloat($scope.displayData[d].totalMeetingAllowance+parseFloat($scope.displayData[d].totalOutStationAllowance));
                                    }
                                    $scope.summery.totalAllowance = tot;
                                    $scope.summery.totalTA = ta;
                                });
                            }).error(function(data){
                                alert("There is some problem occurred. Please try again after refreshing the page");
                            });
                        }else{
                            details.location = "0";
                            details.allowance="0"
                        }
                    }
                }
                else {
                    RepositoryService.restore(details.expDtId).then(function (exData) {
                        prvExpType=exData.data;
                        details.allowance=0;
                        var cnf=true;
                        if(details.routes.length>0) {
                            cnf = confirm("This will delete your routes and documents");
                        }
                        if (cnf == true) {
                            $http({
                                method: 'POST',
                                data:details,
                                url: restUrl + "/expense/expenseService/updateLocationType",
                                headers: {
                                    "content-type": "application/json",
                                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                                }
                            }).success(function (response) {
                                details.allowance = parseFloat(response.allowance);
                                details.routes = response.routes;
                                details.documents = response.documents;
                                details.expDtId = response.expDtId;
                                details.location = response.location;
                                details.docCount = response.docCount;
                                details.allowance = response.allowance;
                                details.returnAllowance = response.returnAllowance;
                                details.fare = parseFloat(response.fare);
                                details.returnFare = response.returnFare;
                                details.kms = response.kms;
                                details.returnKms = response.returnKms;
                                details.total = parseFloat(response.miscTotal) + parseFloat(response.allowance) + parseFloat(response.fare)+parseFloat(details.returnFare);
                                var tot = 0.00;
                                var ta = 0.00;
                                for (d in $scope.displayData) {
                                    ta = ta + parseFloat($scope.displayData[d].fare)+parseFloat($scope.displayData[d].returnFare)+parseFloat($scope.displayData[d].totalTransit);
                                    tot = tot + parseFloat($scope.displayData[d].allowance)+parseFloat($scope.displayData[d].totalMeetingAllowance);
                                }
                                $scope.summery.totalAllowance = tot;
                                $scope.summery.totalTA = ta;
                            }).error(function(data){
                                alert("There is some problem occurred. Please try again after refreshing the page");
                                details.location = prvExpType.locationType.id;
                                details.allowance = prvExpType.amount;
                            })
                        }
                        else
                        {
                            details.location = prvExpType.locationType.id;
                            details.allowance = prvExpType.amount;
                        }
                    });
                }
            }
        };

        $scope.addExpenseApproval=function(approval,type){
            if(type==='da'){
                if(isNaN(approval.daAdded) || parseFloat(approval.daAdded)<0 || parseFloat(approval.daAdded)%1!=0 ||
                    isNaN(approval.daDeducted) || parseFloat(approval.daDeducted)<0 || parseFloat(approval.daDeducted)%1!=0){
                    alert("Enter valid amount")
                }
                else {
                    var prvApr=parseFloat(approval.prvDa);
                    var curAdd=parseFloat(approval.daAdded);
                    var curDed=parseFloat(approval.daDeducted);

                    if(((prvApr+curAdd)-curDed)<0){
                        alert("Can not deduct amount bigger than approved amount")
                    }
                    else
                    {
                        if((approval.daRemark==null || !approval.daRemark.trim())) {
                            alert("Enter remarks for DA approval");
                        }else {
                            var finalAmt = (prvApr + curAdd) - curDed;
                            var approvalDetail = {
                                id: approval.daId,
                                instanceDetail: {id: approval.instanceDetail.id},
                                approvalId:{id:approval.approval.id},
                                type: {id: 'USRLVEXPCAT00000000000000000000000002'},
                                addedAmount: curAdd,
                                deductedAmount: curDed,
                                finalAmount: finalAmt,
                                remarks: approval.daRemark
                            };
                            RepositoryService.update(approvalDetail, "EXADT").success(function (data) {
                                approval.da=finalAmt;
                            }).error(function(data){
                                aalert("There is some problem occurred. Please try again after refreshing the page");
                            });
                        }
                    }
                }
            }
            else if(type=='ta'){
                if(isNaN(approval.taAdded) || parseFloat(approval.taAdded)<0 || parseFloat(approval.taAdded)%1!=0 ||
                    isNaN(approval.taDeducted) || parseFloat(approval.taDeducted)<0 || parseFloat(approval.taDeducted)%1!=0){
                    alert("Enter valid amount")
                }
                else {
                    var prvApr=parseFloat(approval.prvTa);
                    var curAdd=parseFloat(approval.taAdded);
                    var curDed=parseFloat(approval.taDeducted);

                    if(((prvApr+curAdd)-curDed)<0){
                        alert("Can not deduct amount bigger than approved amount")
                    }
                    else
                    {
                        if((approval.taRemark==null || !approval.taRemark.trim())) {
                            alert("Enter remarks for TA approval");
                        }else {
                            var finalAmt = (prvApr + curAdd) - curDed;
                            var approvalDetail = {
                                id: approval.taId,
                                instanceDetail: {id: approval.instanceDetail.id},
                                approvalId:{id:approval.approval.id},
                                type: {id: 'USRLVEXPCAT00000000000000000000000001'},
                                addedAmount: curAdd,
                                deductedAmount: curDed,
                                finalAmount: finalAmt,
                                remarks: approval.taRemark
                            };
                            RepositoryService.update(approvalDetail, "EXADT").success(function (data) {
                                approval.ta = finalAmt;
                            }).error(function (data) {
                                alert("There is some problem occurred. Please try again after refreshing the page");
                            });
                        }
                    }
                }
            }
            else if(type=='octroi'){

                if(isNaN(approval.octroiAdded) || parseFloat(approval.octroiAdded)<0 || parseFloat(approval.octroiAdded)%1!=0 ||
                    isNaN(approval.octroiDeducted) || parseFloat(approval.octroiDeducted)<0 || parseFloat(approval.octroiDeducted)%1!=0){
                    alert("Enter valid amount")
                }
                else {
                    var prvApr=parseFloat(approval.prvOctroi);
                    var curAdd=parseFloat(approval.octroiAdded);
                    var curDed=parseFloat(approval.octroiDeducted);

                    if(((prvApr+curAdd)-curDed)<0){
                        alert("Can not deduct amount bigger than approved amount")
                    }
                    else
                    {
                        if((approval.octroiRemark==null || !approval.octroiRemark.trim())) {
                            alert("Enter remarks for Octroi approval");
                        }else {
                            var finalAmt = (prvApr + curAdd) - curDed;
                            var approvalDetail = {
                                id: approval.octroiId,
                                instanceDetail: {id: approval.instanceDetail.id},
                                approvalId:{id:approval.approval.id},
                                type: {id: 'USRLVEXPCAT00000000000000000000000003'},
                                addedAmount: curAdd,
                                deductedAmount: curDed,
                                finalAmount: finalAmt,
                                remarks: approval.octroiRemark
                            };
                            RepositoryService.update(approvalDetail, "EXADT").success(function (data) {
                                approval.octroi = finalAmt;
                            }).error(function (data) {
                                alert("There is some problem occurred. Please try again after refreshing the page");
                            });
                        }
                    }
                }
            }
            else if(type=='promotional'){

                if(isNaN(approval.promotionalAdded) || parseFloat(approval.promotionalAdded)<0 || parseFloat(approval.promotionalAdded)%1!=0 ||
                    isNaN(approval.promotionalDeducted) || parseFloat(approval.promotionalDeducted)<0 || parseFloat(approval.promotionalDeducted)%1!=0){
                    alert("Enter valid amount")
                }
                else {
                    var prvApr=parseFloat(approval.prvPromotional);
                    var curAdd=parseFloat(approval.promotionalAdded);
                    var curDed=parseFloat(approval.promotionalDeducted);

                    if(((prvApr+curAdd)-curDed)<0){
                        alert("Can not deduct amount bigger than approved amount")
                    }
                    else
                    {
                        if((approval.promotionalRemark==null || !approval.promotionalRemark.trim())) {
                            alert("Enter remarks for Promotional approval");
                        }else {
                            var finalAmt = (prvApr + curAdd) - curDed;
                            var approvalDetail = {
                                id: approval.promotionalId,
                                instanceDetail: {id: approval.instanceDetail.id},
                                approvalId:{id:approval.approval.id},
                                type: {id: 'USRLVEXPCAT00000000000000000000000004'},
                                addedAmount: curAdd,
                                deductedAmount: curDed,
                                finalAmount: finalAmt,
                                remarks: approval.promotionalRemark
                            };
                            RepositoryService.update(approvalDetail, "EXADT").success(function (data) {
                                approval.promotional = finalAmt;
                            }).error(function (data) {
                                alert("There is some problem occurred. Please try again after refreshing the page");
                            });
                        }
                    }
                }
            }
            else if(type=='medicalCheckUp'){

                if(isNaN(approval.medicalCheckUpAdded) || parseFloat(approval.medicalCheckUpAdded)<0 || parseFloat(approval.medicalCheckUpAdded)%1!=0 ||
                    isNaN(approval.medicalCheckUpDeducted) || parseFloat(approval.medicalCheckUpDeducted)<0 || parseFloat(approval.medicalCheckUpDeducted)%1!=0){
                    alert("Enter valid amount")
                }
                else {
                    var prvApr=parseFloat(approval.prvMedicalCheckUp);
                    var curAdd=parseFloat(approval.medicalCheckUpAdded);
                    var curDed=parseFloat(approval.medicalCheckUpDeducted);

                    if(((prvApr+curAdd)-curDed)<0){
                        alert("Can not deduct amount bigger than approved amount")
                    }
                    else
                    {
                        if((approval.medicalCheckUpRemark==null || !approval.medicalCheckUpRemark.trim())) {
                            alert("Enter remarks for medicalCheckUp approval");
                        }else {
                            var finalAmt = (prvApr + curAdd) - curDed;
                            var approvalDetail = {
                                id: approval.medicalCheckUpId,
                                instanceDetail: {id: approval.instanceDetail.id},
                                approvalId:{id:approval.approval.id},
                                type: {id: 'USRLVEXPCAT00000000000000000000000005'},
                                addedAmount: curAdd,
                                deductedAmount: curDed,
                                finalAmount: finalAmt,
                                remarks: approval.medicalCheckUpRemark
                            };
                            RepositoryService.update(approvalDetail, "EXADT").success(function (data) {
                                approval.medicalCheckUp = finalAmt;
                            }).error(function (data) {
                                alert("There is some problem occurred. Please try again after refreshing the page");
                            });
                        }
                    }
                }
            }

        };
        $scope.saveApproval=function(approval){

            var dosave=true;
            if(isNaN(approval.daAdded) || parseFloat(approval.daAdded)<0 || parseFloat(approval.daAdded)%1!=0 ||
                isNaN(approval.daDeducted) || parseFloat(approval.daDeducted)<0 || parseFloat(approval.daDeducted)%1!=0 ||
                isNaN(approval.taAdded) || parseFloat(approval.taAdded)<0 || parseFloat(approval.taAdded)%1!=0 ||
                isNaN(approval.taDeducted) || parseFloat(approval.taDeducted)<0 || parseFloat(approval.taDeducted)%1!=0 ||
                isNaN(approval.octroiAdded) || parseFloat(approval.octroiAdded)<0 || parseFloat(approval.octroiAdded)%1!=0 ||
                isNaN(approval.octroiDeducted) || parseFloat(approval.octroiDeducted)<0 || parseFloat(approval.octroiDeducted)%1!=0||
                isNaN(approval.promotionalAdded) || parseFloat(approval.promotionalAdded)<0 || parseFloat(approval.promotionalAdded)%1!=0||
                isNaN(approval.promotionalDeducted) || parseFloat(approval.promotionalDeducted)<0 || parseFloat(approval.promotionalDeducted)%1!=0 ||
                isNaN(approval.medicalCheckUpAdded) || parseFloat(approval.medicalCheckUpAdded)<0 || parseFloat(approval.medicalCheckUpAdded)%1!=0||
                isNaN(approval.medicalCheckUpDeducted) || parseFloat(approval.medicalCheckUpDeducted)<0 || parseFloat(approval.medicalCheckUpDeducted)%1!=0 ||
                isNaN(approval.otherAdded) || parseFloat(approval.otherAdded)<0 || parseFloat(approval.otherAdded)%1!=0 ||
                isNaN(approval.otherDeducted) || parseFloat(approval.otherDeducted)<0 || parseFloat(approval.otherDeducted)%1!=0 ||
                isNaN(approval.sundriesAdded) || parseFloat(approval.sundriesAdded)<0 || parseFloat(approval.sundriesAdded)%1!=0 ||
                isNaN(approval.sundriesDeducted) || parseFloat(approval.sundriesDeducted)<0 || parseFloat(approval.sundriesDeducted)%1!=0){
                alert("Enter valid amount");
                dosave=false;
            }else{
                var finalTa=0;
                var finalDa=0;
                var finalOctroi=0
                var finalPromotional=0;
                var finalMedicalCheckUp=0;
                var finalOther=0;
                var finalSundries=0;

                finalTa=(parseFloat(approval.prvTa)+parseFloat(approval.taAdded))-parseFloat(approval.taDeducted);
                finalDa=(parseFloat(approval.prvDa)+parseFloat(approval.daAdded))-parseFloat(approval.daDeducted);
                finalOctroi=(parseFloat(approval.prvOctroi)+parseFloat(approval.octroiAdded))-parseFloat(approval.octroiDeducted);
                finalPromotional=(parseFloat(approval.prvPromotional)+parseFloat(approval.promotionalAdded))-parseFloat(approval.promotionalDeducted);
                finalMedicalCheckUp=(parseFloat(approval.prvMedicalCheckUp)+parseFloat(approval.medicalCheckUpAdded))-parseFloat(approval.medicalCheckUpDeducted);
                finalOther=(parseFloat(approval.prvOther)+parseFloat(approval.otherAdded))-parseFloat(approval.otherDeducted);
                finalSundries=(parseFloat(approval.prvSundries)+parseFloat(approval.sundriesAdded))-parseFloat(approval.sundriesDeducted);

                approval.ta=finalTa;
                approval.da=finalDa;
                approval.octroi=finalOctroi;
                approval.promotional=finalPromotional;
                approval.medicalCheckUp=finalMedicalCheckUp;
                approval.other=finalOther;
                approval.sundries=finalSundries;

                dosave=true;
            }

            if((approval.daAdded!=0 || approval.daDeducted!=0) && (approval.daRemark==null || !approval.daRemark.trim())) {
                alert("Enter remarks DA approval");
                dosave=false;
            }else if((approval.taAdded!=0 || approval.taDeducted!=0) && (approval.taRemark==null || !approval.taRemark.trim())){
                alert("Enter remarks TA approval");
                dosave=false;
            }else if((approval.octroiAdded!=0 || approval.octroiDeducted!=0) && (approval.octroiRemark==null || !approval.octroiRemark.trim())){
                alert("Enter remarks for octroi approval");
                dosave=false;
            }else if((approval.promotionalAdded!=0 || approval.promotionalDeducted!=0) && (approval.promotionalRemark==null || !approval.promotionalRemark.trim())){
                alert("Enter remarks for promotional approval");
                dosave=false;
            }else if((approval.medicalCheckUpAdded!=0 || approval.medicalCheckUpDeducted!=0) && (approval.medicalCheckUpRemark==null || !approval.medicalCheckUpRemark.trim())){
                alert("Enter remarks for medical checkUp approval");
                dosave=false;
            }else if((approval.otherAdded!=0 || approval.otherDeducted!=0) && (approval.otherRemark==null || !approval.otherRemark.trim())){
                alert("Enter remarks for other approval");
                dosave=false;
            }else if((approval.sundriesAdded!=0 || approval.sundriesDeducted!=0) && (approval.sundriesRemark==null || !approval.sundriesRemark.trim())){
                alert("Enter remarks for Sundries approval");
                dosave=false;
            }


            if(dosave) {
                var modalInstance = $modal.open({
                    templateUrl: 'confirmApproval.html',
                    controller: 'approvalConfirmController',
                    size: 'lg',
                    backdrop: false,
                    backdropClick: false,
                    dialogFade: true,
                    keyboard: false,
                    resolve: {
                        approval: function(){
                            return approval;
                        }
                    }
                });
            }
        };

        $scope.goBack=function(){

            if($scope.isMIS==true) {
                $state.go("mis.search_approval");
            }
            else{
                $state.go('home.search_expenseApproval');
            }
        }

        $scope.isCamps=function(d,index){
            var isCamp=!d.cycleMeet;
            var hasDoc=false;
            var nonCallList= d.nonCallAct;
            if(nonCallList==null || nonCallList.length==0){
                isCamp=true;
            }
            if(LocalStore.fetch("headerdata").jobTitle.id=="JOBTT00000000000000000000000000000003"){
                if (!$scope.ebChckList[index] && isCamp)
                    return true;
                else if (!isCamp) {
                    return false;
                }
                else {
                    if (d.nonCallExpType == null && d.noOfDoc == 0) {
                        hasDoc = true;
                        return isCamp && hasDoc;
                    }
                }
            }else{
                if(d.nonCallExpType==null && d.noOfDoc==0){
                    hasDoc=true;
                    return isCamp && hasDoc;
                }
            }

        }
    });

sfe.controller('visitedTownsController',function ($scope, $http,LabelService, RepositoryService, LocalStore, $modalInstance,towns,date,employeeList,isRm){
    $scope.visitedTowns=towns;
    $scope.visitDate=date;
    $scope.init = function(){};
    $scope.employeeList=employeeList;
    $scope.isRm=isRm;
    $scope.closeModal = function(){
        $modalInstance.dismiss('cancel');
    };
})

sfe.controller('approvalConfirmController', function ($scope, $http,LabelService, RepositoryService, LocalStore, $modalInstance,approval) {
    $scope.approval=approval;
    $scope.init = function(){};

    $scope.save =function(approval){
        approval.status = "APPROVED";
        var owner;
        if(approval.owner == null){
            owner=null;
        }
        else{
            owner=approval.owner.id;
        }
        var finalApproval={instanceDetail:{id:approval.instanceDetail.id},approval:null,jobTitle:{id:approval.jobTitle.id},owner:{id:owner},status:approval.status,
            ta:approval.ta,da:approval.da,octroi:approval.octroi,promotional:approval.promotional,medicalCheckUp:approval.medicalCheckUp,other:approval.other,sundries:approval.sundries,
            daRemark:approval.daRemark,taRemark:approval.taRemark,octroiRemark:approval.octroiRemark,promotionalRemark:approval.promotionalRemark,
            medicalCheckUpRemark:approval.medicalCheckUpRemark,otherRemark:approval.otherRemark,sundriesRemark:approval.sundriesRemark,
            taAdded:approval.taAdded,taDeducted:approval.taDeducted,
            daAdded:approval.daAdded,daDeducted:approval.daDeducted,
            octroiAdded:approval.octroiAdded, octroiDeducted:approval.octroiDeducted,
            promotionalDeducted:approval.promotionalDeducted,promotionalAdded:approval.promotionalAdded,
            medicalCheckUpAdded:approval.medicalCheckUpAdded,medicalCheckUpDeducted:approval.medicalCheckUpDeducted,
            otherAdded:approval.otherAdded,otherDeducted:approval.otherDeducted,
            sundriesAdded:approval.sundriesAdded,sundriesDeducted:approval.sundriesDeducted,
            taId:approval.taId,daId:approval.daId,octroiId:approval.octroiId,promotionalId:approval.promotionalId,medicalCheckUpId:approval.medicalCheckUpId,
            otherId:approval.otherId,sundriesId:approval.sundriesId};

        $http({
            method: 'POST',
            data:finalApproval,
            url: restUrl + "/expense/expenseService/approveExpense",
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        }).success(function (response) {
            if(response){
                if (approval.owner == null) {
                    approval.owner = {"id": LocalStore.fetch('USER_INFO').id};
                }
                if($scope.approval.jobTitle.id!='JOBTT000000008aea432014dd69a749d0494a') {
                    $('#sundriesEditableBtn').hide();
                }
                alert("Your Approval is saved successfully.");
                $modalInstance.dismiss('cancel');
            }
        }).error(function(response){
            alert("There is some problem occurred. Please try again after refreshing the page");
        })
    }

    $scope.closeModal = function(){
        $modalInstance.dismiss('cancel');
    };
});


sfe.controller('doscSundriesController',
    function ($scope, $http,LabelService, RepositoryService, LocalStore, $modalInstance,expStatus,displayData,owner){
        $scope.displayData=displayData;
        $scope.docList=displayData[displayData.length-1].sundriesDocuments;
        $scope.doccount=displayData[displayData.length-1].sundriesDocCount;
        $scope.status=expStatus;

        $scope.init = function(){
            console.log($scope.docList);
        };

        $scope.uploadFile = function(files){
            $scope.errMsg=[];

            if(files==null)
            {
                $scope.errMsg.push("Select file for upload.")
                $scope.isShowAlert=true;
            }
            else {
                $scope.isShowAlert=false;
                $scope.errMsg=[];

                var file = files[0];
                var re = /(?:\.([^.]+))?$/;
                var ext = re.exec(file.name)[1];
                var fd = new FormData();

                fd.append("file", file);
                ext=ext.toLowerCase();
                if(ext=="jpg" || ext=="jpeg" || ext=="png" || ext=="tiff" || ext=="tif" || ext=="gif" || ext=="pdf"){

                    if(file.size <= 5000000){

                        return $http({
                            method: 'POST',
                            data: fd,
                            url: restUrl + "/expense/UploadDocumentService/upload" + "/" + owner + "/SYSLVEXP00000000000000000000000000003/" + ext,
                            transformRequest: angular.identity,

                            headers: {
                                "content-type": "multipart/form-data",
                                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                            }

                        }).then(function (data) {
                            $scope.docList.push(data.data);
                            displayData[displayData.length-1].SundriesDocuments=$scope.docList;
                            displayData[displayData.length-1].sundriesDocCount = parseInt(displayData[displayData.length-1].sundriesDocCount) + 1;
                        });
                        document.getElementById('uploadFileSundries').value='';
                    }else{
                        $scope.errMsg.push('File is bigger than 1MB');
                        $scope.isShowAlert=true;
                        return false;
                    }
                }else{
                    $scope.errMsg.push("Select Image File Only");
                    $scope.isShowAlert=true;
                    return false;
                }
            }
        };
        $scope.delete=function(id,name,index){
            $http({
                method: 'GET',
                url: restUrl + "/expense/UploadDocumentService/deleteByName/"+id+"/"+name,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            }).then(function(response) {
                if(response){
                    $scope.docList.splice(index,1);
                }
                displayData[displayData.length-1].sundriesDocCount = parseInt(displayData[displayData.length-1].sundriesDocCount) - 1;
            });
        };

        $scope.downloadFile=function(id){
            window.open("/webapp/download/expense?documentId="+id);
        }
        $scope.closeModal = function(){
            $modalInstance.dismiss('cancel');
        };
    });

sfe.controller('doscController',
    function ($scope, $http,LabelService, RepositoryService, LocalStore, $modalInstance,input,displayDate,documents,returnJourney,miscList,routeExpenseId,expStstus) {
        $scope.input=input;
        $scope.title=displayDate;
        $scope.docType=[];
        $scope.docs=documents;
        $scope.miscList=miscList;
        $scope.returnJourney=returnJourney;
        $scope.docTp=false;
        $scope.docSel=false;
        $scope.uploadBtn=false;
        $scope.exp="";
        $scope.exp.owner="";
        $scope.expenseStatus=expStstus;
        $scope.routeExpenseId=routeExpenseId;
        $scope.errMsg=[];
        $scope.isShowAlert=false;

        $scope.init = function(){
            RepositoryService.find("usrlv",{type: 'DOCUMENT_TYPE', paged:'N'},'advanced').then(function(data){
                $scope.docType = data.data;
            });
        };

        $scope.uploadFile = function(files,type){
            $scope.errMsg=[];

            if(files==null)
            {
                $scope.errMsg.push("Select file for upload.")
                $scope.isShowAlert=true;
            }
            else if(type!="001" && type=="0"){
                $scope.errMsg.push("Select document type");
                $scope.isShowAlert=true;
            }
            else {
                $scope.isShowAlert=false;
                $scope.errMsg=[];

                var owner = "";
                if (type != "001") {
                    for (m in $scope.miscList) {
                        if ($scope.miscList[m].type.id === type) {
                            owner = $scope.miscList[m].expenseDetails.id;
                        }
                    }
                } else {
                    owner = $scope.routeExpenseId;
                    type=0;
                }
                var file = files[0];
                var re = /(?:\.([^.]+))?$/;
                var ext = re.exec(file.name)[1];
                var fd = new FormData();
                ext=ext.toLowerCase();
                fd.append("file", file);
                ext=ext.toLowerCase();
                if(ext=="jpg" || ext=="jpeg" || ext=="png" || ext=="tiff" || ext=="tif" || ext=="gif" || ext=="pdf"){

                    if(file.size <= 5000000){

                        return $http({
                            method: 'POST',
                            data: fd,
                            url: restUrl + "/expense/UploadDocumentService/upload" + "/" + owner + "/" + type + "/" + ext,
                            transformRequest: angular.identity,

                            headers: {
                                "content-type": "multipart/form-data",
                                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                            }

                        }).then(function (data) {
                            $scope.docs.push(data.data);
                            $scope.input.docCount = parseInt($scope.input.docCount) + 1;
                        });
                        document.getElementById('uploadFile').value='';
                    }else{
                        $scope.errMsg.push('File is bigger than 1MB');
                        $scope.isShowAlert=true;
                        return false;
                    }
                }else{
                    $scope.errMsg.push("Select Image and PDF File Only");
                    $scope.isShowAlert=true;
                    return false;
                }
            }
        };
        $scope.closeModal = function(){
            $modalInstance.dismiss('cancel');
        };

        $scope.enableUpload=function(type){
            if(type==='misc'){
                $scope.docTp=true;
                $scope.docSel=true;
                $scope.uploadBtn=true;
            }
            else
            {
                $scope.docTp=false;
                $scope.docSel=true;
                $scope.uploadBtn=true;
                $scope.exp.type="001";
            }
        }

        $scope.delete=function(id,name,index){
            $http({
                method: 'GET',
                url: restUrl + "/expense/UploadDocumentService/deleteByName/"+id+"/"+name,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            }).then(function(response) {
                if(response){
                    $scope.docs.splice(index,1);
                }
                $scope.input.docCount=parseInt($scope.input.docCount)-1;
            });
        };

        $scope.downloadFile=function(id){
            window.open("/webapp/download/expense?documentId="+id);
        }
    });

sfe.controller('CycleRouteController',function ($scope, $http,LabelService, RepositoryService, LocalStore, $modalInstance,input,displayDate,displayData,expStatus,summery) {
    $scope.input=input;
    $scope.title=displayDate;
    $scope.expenseStatus=expStatus;
    $scope.routeList=[];
    $scope.nonCalAct={};
    $scope.displayData=displayData;
    $scope.summery=summery;
    $scope.errMsg=[];
    $scope.isShowAlert=false;

    $scope.init = function(){
        if($scope.input.fromTown!=null)
        {
            $scope.nonCalAct.fromTown=$scope.input.fromTown;
            $scope.nonCalAct.toTown=$scope.input.toTown;
            $scope.nonCalAct.fare=$scope.input.fare;
            $scope.nonCalAct.id=$scope.input.nonCallExpDtlId;
        }
    };

    $scope.save=function(data){
        $scope.errMsg=[];
        if($scope.nonCalAct.fromTown==null || $scope.nonCalAct.toTown==null || $scope.nonCalAct.fare==null)
        {
            $scope.errMsg.push("Enter all route information");
            $scope.isShowAlert=true;
        }
        else if(isNaN($scope.nonCalAct.fare) || ($scope.nonCalAct.fare < 0) || $scope.nonCalAct.fare%1!=0){
            $scope.errMsg.push("Enter amount in integer");
            $scope.isShowAlert=true;
        }
        else {
            $scope.errMsg=[];
            $scope.isShowAlert=false;
            $scope.input.fromTown = $scope.nonCalAct.fromTown;
            $scope.input.toTown = $scope.nonCalAct.toTown;
            $scope.input.nonCallExpDtlId=$scope.nonCalAct.id;

            if ($scope.input.fromTown.length > 0 && $scope.input.toTown.length > 0) {
                data.fare=$scope.nonCalAct.fare;
                $http({
                    method: 'POST',
                    data:data,
                    url: restUrl + "/expense/expenseService/nonCallActivityRoute",
                    headers: {
                        "content-type": "application/json",
                        "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                    }
                }).success(function (response) {
                    $scope.input.fare = parseFloat($scope.nonCalAct.fare);
                    $scope.input.total = parseFloat($scope.input.allowance) + parseFloat($scope.input.miscTotal) + parseFloat($scope.input.fare) + ($scope.input.returnFare)+parseFloat($scope.input.totalMeetingAllowance);
                    $scope.input.nonCallExpDtlId=response.id;
                    var tot = 0.00;

                    for (d in $scope.displayData) {
                        tot = tot + parseFloat($scope.displayData[d].fare) + (parseFloat($scope.displayData[d].returnFare))+parseFloat($scope.displayData[d].totalTransit);
                    }
                    $scope.summery.totalTA = tot;
                    $modalInstance.dismiss('cancel');
                });
            }
        }
    };

    $scope.closeModal = function(){
        $modalInstance.dismiss('cancel');
    };
});


sfe.controller('RouteController',
    function ($scope, $http,LabelService, RepositoryService, LocalStore, $modalInstance,daysOfWeek,months,input,displayDate,routeList,summery,displayData,expStstus,ebStatus,isRm) {
        $scope.input=input;
        $scope.empHqTown={};
        $scope.hqTownList= input.empHqTown;
        $scope.title=displayDate;
        $scope.towns=routeList;
        var duplicateRoutes=JSON.parse(JSON.stringify(input.routes));
        $scope.deleteTownList=[];
        $scope.townDistance={};
        $scope.dist=0.00;
        $scope.distValue=0;
        $scope.returnKms=0.00;
        $scope.returnAllowance=0.00;
        $scope.totValue=0;
        $scope.oneWayAmount=0;
        $scope.returnAmount=0;
        $scope.expDtId;
        $scope.routes={};
        $scope.kms=0.00;
        $scope.fare=0.00;
        $scope.locDist=0;
        var i = 0;
        $scope.expenseDt={};
        $scope.dist=0;
        $scope.displayData=displayData;
        $scope.summery=summery;
        $scope.locTp;
        $scope.expenseStatus=expStstus;
        $scope.showSave=true;
        $scope.showCalculate=false;
        $scope.disabledRouteList=false;
        $scope.isreturn=false;
        $scope.errMsg=[];
        $scope.isShowAlert=false;
        $scope.isValidDistance=[true];
        $scope.ebStatus=ebStatus;
        $scope.isRm=isRm;

        $scope.init = function(){

            var date1=new Date($scope.input.expDate.date);

            for(hq in $scope.input.empHqTown){
                var vf=new Date($scope.input.empHqTown[hq].validFrom.date);
                var vt=null;
                if($scope.input.empHqTown[hq].validTo!=null)
                    vt=new Date($scope.input.empHqTown[hq].validTo.date);

                if(date1 >= vf && (date1<=vt || vt==null)){
                    $scope.empHqTown={id:$scope.input.empHqTown[hq].townId,name:$scope.input.empHqTown[hq].townName};
                }
            }
            $scope.locTp=input.location;
            var hqTown=$scope.empHqTown;
            var addHqTown=true;
            //add hqtown to townList
            if($scope.towns.length>0){
                for(r in $scope.towns){
                    if($scope.towns[r].id==$scope.empHqTown.id){
                        addHqTown=false;
                    }
                }
            }
            if(addHqTown){
                $scope.towns.push(hqTown);
            }

            var newRoute={};
            if(input.routes.length==0)
            {
                if($scope.input.location=='USRLV000000055936e80149cd895d48007fff'){
                    newRoute={seqNo:1,fromTown:{id:hqTown.id,displayName:hqTown.name},toTown:null};
                    var newRoute2={seqNo:2,fromTown:null,toTown:null};
                    $scope.input.routes.push(newRoute);
                    $scope.input.routes.push(newRoute2);
                    $scope.disabledRouteList=true;
                }else{
                    newRoute={seqNo:1,fromTown:null,toTown:null};
                    $scope.input.routes.push(newRoute);
                }
                $scope.dist=0.00;
                $scope.distValue=0;
                $scope.returnKms=0.00;
                $scope.returnAllowance=0.00;
                $scope.oneWayAmount=0;
                $scope.returnAmount=0;
                $scope.showSave=false;
                $scope.isreturn=false;
                $scope.showCalculate=false;
            }
            else {
                if($scope.input.location=='USRLV000000055936e80149cd895d48007fff' || $scope.input.location=='USRLV000000055936e80149cd895d48007ffd'){
                    $scope.disabledRouteList=true;
                    $scope.showCalculate=true;
                }
                $scope.dist = $scope.input.kms;
                $scope.distValue = parseFloat($scope.input.allowancePerKm);
                $scope.oneWayAmount = parseFloat($scope.input.fare);
                $scope.returnKms=$scope.input.returnKms;
                $scope.returnAllowance=$scope.input.returnAllowance;
                $scope.returnAmount=$scope.input.returnFare;
                $scope.isreturn=$scope.input.returnJourney;

                if($scope.distValue==0){
                    $scope.isOneWayeditable=true
                }
                else{
                    $scope.isOneWayeditable=false;
                }

                if($scope.returnAllowance==0 && $scope.isreturn){
                    $scope.isReturneditable=true
                }
                else{
                    $scope.isReturneditable=false;
                }
                if($scope.isreturn)
                {
                    $scope.returnMsg="Return Distance from "+$scope.input.routes[0].fromTown.displayName +" to "+$scope.input.routes[$scope.input.routes.length-1].fromTown.displayName +" is "+$scope.returnKms;
                }

                var i=1;
                for(i=1;i<=input.routes.length;i++){
                    $scope.isValidDistance.push(true);
                }
            }

            if(!$scope.ebStatus){
                $scope.disabledRouteList=true;
            }
        };

        $scope.addLocation=function(route,index)
        {
            if(route.fromTown==null || route.fromTown.id==null)
            {
                alert("Please select a town");
            }
            else {
                if($scope.isreturn){
                    var cnf=confirm("Adding any more town will delete calculated return distance");
                    if(cnf){
                        $scope.returnAllowance=0;
                        $scope.returnAmount=0;
                        $scope.returnKms=0;
                        $scope.isreturn=false;

                        var newRoute = {seqNo: index + 1, fromTown: null, toTown: null};
                        $scope.input.routes.push(newRoute);
                    }
                }
                else {
                    var newRoute = {seqNo: index + 1, fromTown: null, toTown: null};
                    $scope.input.routes.push(newRoute);
                }
            }
        };
        $scope.save=function(data,displayData){
            $scope.saveRoutes=true;
            $scope.routes=data.routes;
            if($scope.routes[$scope.routes.length-1].fromTown==null || $scope.routes[$scope.routes.length-1].fromTown.id==null)
            {
                $scope.errMsg.push("Can not save empty town. Please add a town in empty row or delete it.");
                $scope.isShowAlert=true;
                $scope.saveRoutes=false;
            }
            else if ($scope.routes.length < 2 || $scope.routes[$scope.routes.length - 1].fromTown == null) {
                $scope.errMsg.push("Can not save empty town. Please add a town in empty row or delete it.");
                $scope.isShowAlert=true;
                $scope.saveRoutes=false;
            }
            else if( $scope.oneWayAmount==null || isNaN($scope.oneWayAmount) || parseFloat($scope.oneWayAmount)<0){
                $scope.errMsg.push("Enter proper amount for one way travel.");
                $scope.isShowAlert=true;
                $scope.saveRoutes=false;
            }
            else if(($scope.isOneWayeditable==true && $scope.oneWayAmount %1 !=0) || ($scope.isReturneditable==true && $scope.returnAmount%1!=0)){
                $scope.errMsg.push("Enter amount in integer (round number)");
                $scope.isShowAlert=true;
                $scope.saveRoutes=false;
            }
            else if($scope.isreturn){
                if( $scope.returnAmount==null || isNaN($scope.returnAmount)){
                    $scope.errMsg.push("Enter proper amount for return travel");
                    $scope.isShowAlert=true;
                    $scope.saveRoutes=false
                }
            }
            else if(data.location=='USRLV000000055936e80149cd895d48007fff' && $scope.isreturn==false){
                $scope.errMsg.push("In case of EX-HQ location type, it is compulsory to calculate return distance");
                $scope.isShowAlert=true;
                $scope.saveRoutes=false
            }

            if($scope.saveRoutes) {
                $scope.isShowAlert=false;
                $scope.errMsg=[];

                $scope.expenseDt = {owner: {id: LocalStore.fetch('USER_INFO').id}, expense: {id: data.expenseId}, expenseType: {id: 'SYSLVEXP00000000000000000000000000002'}, expenseDate: data.expDate, locationType: null, returnJourney: $scope.isreturn, kms: $scope.dist, returnKms: $scope.returnKms, allowancePerKm: $scope.distValue, returnAllowance: $scope.returnAllowance, amount: $scope.oneWayAmount, returnAmount: $scope.returnAmount, remarks: null};
                $scope.expenseRequest={"details":$scope.expenseDt,"deleteList":$scope.deleteTownList,"routes":$scope.routes};

                $http({
                    method: 'POST',
                    data:$scope.expenseRequest,
                    url: restUrl + "/expense/expenseService/saveRoutes",
                    headers: {
                        "content-type": "application/json",
                        "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                    }
                }).success(function (response) {
                    $scope.expDtId = response.details.id;
                    $scope.expenseDt.id = response.details.id;
                    data.routes=response.routes;
                    $scope.deleteTownList=response.deleteList;
                    data.kms = response.details.kms;
                    data.allowancePerKm = response.details.allowancePerKm;
                    data.returnKms = $scope.returnKms;
                    data.returnAllowance = $scope.returnAllowance;
                    data.returnJourney = $scope.isreturn;
                    data.fare=parseFloat($scope.oneWayAmount);
                    data.returnFare=$scope.returnAmount;
                    data.total=parseFloat(data.allowance)+parseFloat(data.miscTotal)+parseFloat(data.fare)+parseFloat(data.returnFare);

                    var tot=0.00;
                    for(d in $scope.displayData) {
                        tot=tot+parseFloat($scope.displayData[d].fare)+parseFloat($scope.displayData[d].returnFare)+($scope.displayData[d].totalTransit);
                    }
                    $scope.summery.totalTA=tot;
                    $scope.deleteTownList = [];
                    $modalInstance.dismiss('cancel');
                });
            }
        };

        $scope.getDistance=function(routes,index) {
            getDistance(routes,index);

            if(routes[index+1]!=null && routes[index+1].fromTown.id){
                getDistance(routes,index+1);
            }
        };

        function getDistance(routes,index){
            var cnt=0;
            for(r in routes)
            {
                if(routes[index].fromTown.id==routes[r].fromTown.id){
                    cnt=cnt+1;
                }
            }

            if(cnt==1 || $scope.isRm) {
                for (t in $scope.towns) {
                    if ($scope.towns[t].id == routes[index].fromTown.id) {
                        routes[index].fromTown.displayName = $scope.towns[t].name;
                    }
                }

                if (index > 0) {
                    findDistance(routes[index - 1].fromTown, routes[index].fromTown, routes, index,false);
                }
            }
            else
            {
                alert("You can not add a town multiple times.");
                routes.splice(index, 1);
                if((index-1)==0 && routes.length==1){
                    $scope.showCalculate=false;
                }

                var cnt=1;
                for(r in routes){
                    routes[r].seqNo=cnt;
                    cnt++;
                }
                routes[index-1].toTown=null;
                routes[index-1].distance=0;
                getDistance(routes, index - 1);
                $scope.returnAllowance = 0.00;
                $scope.oneWayAmount = 0;
                $scope.returnAmount = 0;
                $scope.isreturn=false;
            }
        };

        function findDistance(frmTn,toTn,routes,index){

            if(index==routes.length-1){
                $scope.returnKms=0;
                $scope.returnAllowance=0;
                $scope.returnAmount=0;
            }
            var tmpKms=0.00;
            if(index>0)
            {
                $scope.showSave=true;
                if($scope.input.location=='USRLV000000055936e80149cd895d48007fff'){
                    $scope.showCalculate=true;
                    $scope.showSave=false;
                }
                if($scope.input.location=='USRLV000000055936e80149cd895d48007ffd' && toTn.id!=$scope.empHqTown.id){
                    $scope.showCalculate=true;
                    $scope.showSave=false;
                }
                else if($scope.input.location=='USRLV000000055936e80149cd895d48007ffd' && toTn.id==$scope.empHqTown.id)
                {
                    $scope.showCalculate=false;
                    $scope.showSave=true;
                }
            }

            $http({
                method: 'GET',
                url: restUrl + "/expense/expenseService/getDistance/"+frmTn.id+"/"+toTn.id,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            }).success(function(data){
                tmpKms=data.kms;
                routes[index-1].distance=tmpKms;
                routes[index-1].toTown=routes[index].fromTown;
                $scope.dist=0.00;
                if(data.id==null || data.id==""){
                    $scope.isValidDistance.splice(index);
                    $scope.isValidDistance.splice(index,0,false);
                }else{
                    $scope.isValidDistance.splice(index);
                    $scope.isValidDistance.splice(index,0,true);
                }
                for(r in routes)
                {
                    if(routes[r].distance){
                        $scope.dist=parseFloat($scope.dist)+parseFloat(routes[r].distance);
                    }
                }
                $http({
                    method: 'GET',
                    url: restUrl + "/expense/expenseService/getDistanceValue/"+LocalStore.fetch('USER_INFO').id+"/"+$scope.dist+"/"+$scope.input.expDate.date,
                    headers: {
                        "content-type": "application/json",
                        "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                    }
                }).success(function(response){
                    $scope.distValue=response.value;
                    $scope.isOneWayeditable=response.editable;
                    $scope.oneWayAmount=Math.round(parseFloat(response.value) * parseFloat($scope.dist));
                }).error(function(data){
                    alert("There is problem while getting values. Please try after refresh or inform to administrator.");
                    $scope.input.routes = duplicateRoutes;
                    $modalInstance.dismiss('cancel');
                })
            }).error(function(data){
                alert("There is problem while getting values. Please try after refresh or inform to administrator.");
                $scope.input.routes = duplicateRoutes;
                $modalInstance.dismiss('cancel');
            })
        }

        $scope.calculateReturnJourney=function(routes)
        {
            if($scope.isreturn){
                var cnf=confirm("Calculate return journey from "+ routes[routes.length-1].fromTown.displayName.toLowerCase() +" to "+routes[0].fromTown.displayName.toLowerCase()+"?")
                if(cnf==true) {
                    getReturnDistance(routes[routes.length - 1].fromTown, routes[0].fromTown);
                    $scope.showSave = true;
                    $scope.isreturn = true;
                }
                else
                {
                    $scope.showSave = false;
                    $scope.isreturn = false;
                }
            }
            else
            {
                $scope.returnAllowance=0;
                $scope.returnAmount=0;
                $scope.returnKms=0;
                $scope.isreturn=false;
                $scope.isValidDistance.splice(routes.length);


            }
        }

        function getReturnDistance(fromTown,toTown){
            $http({
                method: 'GET',
                url: restUrl + "/expense/expenseService/getDistance/"+fromTown.id+"/"+toTown.id,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            }).success(function(data) {
                $scope.returnKms=data.kms;
                $scope.returnMsg="Return Distance from "+fromTown.displayName+" to "+toTown.displayName +" is "+$scope.returnKms;
                $http({
                    method: 'GET',
                    url: restUrl + "/expense/expenseService/getDistanceValue/" + LocalStore.fetch('USER_INFO').id + "/" + $scope.returnKms+"/"+$scope.input.expDate.date,
                    headers: {
                        "content-type": "application/json",
                        "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                    }
                }).success(function (response) {
                    $scope.returnAllowance=response.value;
                    $scope.isReturneditable=response.editable;
                    $scope.returnAmount=Math.round(parseFloat(response.value) * parseFloat($scope.returnKms));
                    if(data.id==null || data.id==""){
                        $scope.isValidDistance.push(false);
                    }else{
                        $scope.isValidDistance.push(true);
                    }
                }).error(function(resp){
                    alert("There is problem while getting values. Please try after refresh or inform to administrator.");
                    $scope.input.routes = duplicateRoutes;
                    $modalInstance.dismiss('cancel');
                })
            }).error(function (resp){
                alert("There is problem while getting values. Please try after refresh or inform to administrator.");
                $scope.input.routes = duplicateRoutes;
                $modalInstance.dismiss('cancel');
            });
        }
        $scope.remove=function(route,index){
            if(route[index].expenseDetails!=null) {
                $scope.isValidDistance.splice(index);
                if(route[index].id==null)
                    $scope.deleteTownList.push(route[index-1].id);
                else
                    $scope.deleteTownList.push(route[index].id);
            }
            if (route.length == 2) {
                var cnf=true;
                if($scope.isreturn){
                    cnf=confirm("Delete return distance?")
                }
                if(cnf) {
                    route[index - 1].toTown = null;
                    route[index - 1].distance = 0;
                    route.splice(index, 1);
                    if(((index-1)==0)){
                        $scope.showCalculate=false;
                    }
                    $scope.dist = 0.00;
                    $scope.distValue = 0;
                    $scope.returnKms = 0.00;
                    $scope.returnAllowance = 0.00;
                    $scope.oneWayAmount = 0;
                    $scope.returnAmount = 0;
                    $scope.isreturn=false;
                    $scope.dist = parseFloat($scope.dist) - parseFloat(route[index - 1].distance);
                    $scope.isValidDistance.splice(index);
                }
            }
            else {
                if(index==route.length-1){
                    var cnf=true;
                    if($scope.isreturn){
                        cnf=confirm("Delete return distance?")
                    }
                    if(cnf) {
                        route[index - 1].toTown = null;
                        route[index - 1].id = null;
                        route[index - 1].distance = 0;
                        route.splice(index, 1);
                        if(((index-1)==0)){
                            $scope.showCalculate=false;
                        }
                        getDistance(route, index - 1);
                        $scope.returnAllowance = 0.00;
                        $scope.oneWayAmount = 0;
                        $scope.returnAmount = 0;
                        $scope.isreturn=false;
                        $scope.isValidDistance.splice(index);
                    }
                }
                else
                {
                    var cnf=true;
                    if($scope.isreturn){
                        cnf=confirm("Delete return distance?")
                    }
                    if(cnf) {
                        route.splice(index, 1);
                        if(((index-1)==0)){
                            $scope.showCalculate=false;
                        }
                        //route[index].seqNo = index + 1;
                        var cnt=1;
                        for(r in route){
                            route[r].seqNo=cnt;
                            cnt++
                        }
                        route[index - 1].toTown = route[index].fromTown;
                        route[index - 1].distance = 0;
                        getDistance(route, index);
                        if(index < (route.length-1)){
                            getDistance(route, index+1);
                        }
                        $scope.returnAllowance = 0.00;
                        $scope.oneWayAmount = 0;
                        $scope.returnAmount = 0;
                        $scope.isreturn=false;
                        $scope.isValidDistance.splice(index);
                    }
                }
            }
        }

        $scope.closeModal = function(input){
            if(input.routes[0].id==null){
                $scope.input.routes.length=0;
            }
            var cnf=true;
            if($scope.deleteTownList.length>0){
                cnf=confirm("Do you want to discard changes?");
            }
            if(cnf) {
                $scope.input.routes = duplicateRoutes;
                $modalInstance.dismiss('cancel');
            }
        }

    });

sfe.controller('MiscController',
    function ($scope, $http,LabelService, RepositoryService, LocalStore, $modalInstance,daysOfWeek,months,input,displayDate,expList,summery,displayData,expStstus,index) {
        $scope.input=input;
        $scope.miscs=expList;
        $scope.summery=summery;
        $scope.displayData=displayData;
        $scope.docs=input.documents;
        $scope.expenseStatus=expStstus;
        $scope.miscIndex=index;
        $scope.duplicateMiscs=JSON.parse(JSON.stringify(input.miscList));
        $scope.deleteMiscsList=[];
        var total=parseFloat($scope.input.total)-parseFloat($scope.input.miscTotal);
        var miscTotal=0;
        var i=0;
        $scope.title=displayDate;
        $scope.errMsg=[];
        $scope.saveMiscs=true;
        $scope.isShowAlert=false;

        $scope.init = function(){
            if(input.miscList.length==0)
            {
                var newMisc={amount:"",type:"",expense:{id:input.expenseId},expenseDetails:{},employee:{id:LocalStore.fetch('USER_INFO').id},expenseDate:input.expDate,remarks:""};
                $scope.input.miscList.push(newMisc);
            }
        };

        $scope.checkDuplicateMisc=function(misc,index){
            var newMisc=0;
            for(m in $scope.input.miscList)
            {
                if($scope.input.miscList[m].type.id==misc.type.id){
                    newMisc=newMisc+1;
                }
            }

            if(newMisc>1){
                alert("Can not add same item multiple times.");
                $scope.input.miscList.splice(index-1,1);
            }
        }

        $scope.addMisc=function(misc,index)
        {
            $scope.errMsg=[];
            if(misc.type==null || misc.type.id==null || misc.amount==null || misc.amount==0 || misc.remarks=="")
            {
                $scope.errMsg.push("Enter all the requested details.");
                $scope.isShowAlert=true;
            }
            else if(isNaN(misc.amount) || misc.amount<0 || misc.amount%1!=0){
                $scope.errMsg.push("Expense amount should be integer (round number)");
                $scope.isShowAlert=true;
            }
            else {
                $scope.errMsg=[];
                $scope.isShowAlert=false;
                var newMisc = {amount: "", type: "", expense: misc.expense, expenseDetails: {}, employee: misc.employee, expenseDate: misc.expenseDate, remarks: ""};
                $scope.input.miscList.push(newMisc);
            }
        };

        $scope.save=function(data,displayData){
            var miscs=data.miscList;
            var doSave=true;

            if(miscs[miscs.length-1].type==null || miscs[miscs.length-1].type.id==null || miscs[miscs.length-1].amount==null || miscs[miscs.length-1].amount==0 || miscs[miscs.length-1].remarks=="")
            {
                if($scope.deleteMiscsList.length>0 && miscs.length==1){
                    data.miscList=[];
                    doSave=true;
                }
                else {
                    $scope.errMsg.push("Enter all the requested details.");
                    $scope.isShowAlert = true;
                    doSave=false;
                }
            }

            if(isNaN(miscs[miscs.length-1].amount) || miscs[miscs.length-1].amount<0 || miscs[miscs.length-1].amount%1!=0){
                $scope.errMsg.push("Expense Amount should be integer (round number)");
                $scope.isShowAlert=true;
                doSave=false;
            }

            if(doSave) {
                var expenseMisc={"dto":data,"deleteList":$scope.deleteMiscsList}
                $http({
                    method: 'POST',
                    data:expenseMisc,
                    url: restUrl + "/expense/expenseService/saveMisc",
                    headers: {
                        "content-type": "application/json",
                        "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                    }
                }).success(function (response) {
                    if(response.miscList!=null) {
                        data.miscList = response.miscList;
                        data.miscTotal = response.miscTotal;
                        data.totalTransit=response.totalTransit;
                        data.totalOctroi=response.totalOctroi;
                        data.totalPromotional=response.totalPromotional;
                        data.totalMedicalCheckUp=response.totalMedicalCheckUp;
                        data.totalMeetingAllowance=response.totalMeetingAllowance;
                        data.totalOther=response.totalOther;
                        data.totalOutStationAllowance=response.totalOutStationAllowance;
                        data.documents = response.documents;
                        data.docCount = response.docCount;
                        data.total = parseFloat(data.fare) + parseFloat(data.returnFare) + parseFloat(data.allowance) + parseFloat(data.miscTotal);
                    }else{
                        data.totalTransit=0;
                        data.totalOctroi=0;
                        data.totalPromotional=0;
                        data.totalMedicalCheckUp=0;
                        data.totalMeetingAllowance=0;
                        data.totalOther=0;
                        data.miscTotal = 0;
                        data.total = parseFloat(data.fare) + parseFloat(data.returnFare) + parseFloat(data.allowance) + parseFloat(data.miscTotal);
                    }

                    displayData[$scope.miscIndex]=data;

                    for(m in data.miscList){
                        for (nm in $scope.miscs) {
                            if(data.miscList[m].type.id==$scope.miscs[nm].id){
                                data.miscList[m].type.displayName=$scope.miscs[nm].name;
                            }
                        }
                    }
                    for(d in data.documents) {
                        for (nm in $scope.miscs) {
                            if (data.documents[d].type!=null && data.documents[d].type.id == $scope.miscs[nm].id) {
                                data.documents[d].type.displayName = $scope.miscs[nm].name;
                            }
                        }
                    }
                    var tot=0;
                    var totFare=0;
                    var totDa=0;
                    var finalPromotional=0;
                    var finalOctroi=0;
                    var finalMedical=0;
                    var totalOther=0;

                    for(d in displayData)
                    {
                        tot=tot+parseFloat(displayData[d].miscTotal)
                        totFare=totFare+parseFloat(displayData[d].fare)+parseFloat(displayData[d].returnFare)+parseFloat(displayData[d].totalTransit);
                        totDa=totDa+parseFloat(displayData[d].allowance)+parseFloat(displayData[d].totalMeetingAllowance)+parseFloat(displayData[d].totalOutStationAllowance);
                        finalPromotional=finalPromotional+parseFloat(displayData[d].totalPromotional);
                        finalOctroi=finalOctroi+parseFloat(displayData[d].totalOctroi);
                        finalMedical=finalMedical+parseFloat(displayData[d].totalMedicalCheckUp);
                        totalOther=totalOther+parseFloat(displayData[d].totalOther);
                    }
                    $scope.summery.totalMisc=tot;
                    $scope.summery.totalTA=totFare;
                    $scope.summery.totalAllowance=totDa;
                    $scope.summery.finalPromotional=finalPromotional;
                    $scope.summery.finalOctroi=finalOctroi;
                    $scope.summery.finalMedicalCheckUp=finalMedical;
                    $scope.summery.totalOther=totalOther;
                    $modalInstance.dismiss('cancel');

                });
            }
        };

        $scope.remove=function(index,misc,miscList){
            if(misc.expenseDetails.id!=null) {
                $scope.deleteMiscsList.push(misc);
                $scope.input.miscList.splice(index, 1);
            }
            else{
                $scope.input.miscList.splice(index, 1);
            }
            if(index==0 && miscList.length==0){
                var newMisc={amount:"",type:"",expense:misc.expense,expenseDetails:{},employee:misc.employee,expenseDate:misc.expenseDate};
                $scope.input.miscList.push(newMisc);
            }
        };
        $scope.closeModal = function(){

            var cnf=true;
            if($scope.deleteMiscsList.length>0)
            {
                cnf=confirm("Do you want to discard changes?");
            }
            if(cnf) {
                $scope.input.miscList = $scope.duplicateMiscs;
                LocalStore.remove("miscList");
                $modalInstance.dismiss('cancel');
            }

        }
    });