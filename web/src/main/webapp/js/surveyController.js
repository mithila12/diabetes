sfe.controller('Page1Controller', function ($scope, $http, $rootScope, $compile, $state, $stateParams,LocalStore, RepositoryService, UIService) {

        $scope.nextPage={};
         $scope.answers =[];
        $scope.init=function() {
            $scope.q1Alert="";
            $scope.q2Alert="Enter value between 0 to 100";
            RepositoryService.find('doctr', {'id': $stateParams.id}, 'simple').success(function (data) {
            $scope.doctor = data[0];
                return $http({
                    method: 'GET',
                    url: restUrl + "/miscellaneous/surveyservice/survey/" + $stateParams.id + '/' + $stateParams.pageNo,
                    headers: {
                        "content-type": "application/json",
                        "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                    }
                }).success(function (data) {
                    $scope.answers = data;
                })
            })

        }


        $scope.nextPage = function(){
                    var answer1 = $scope.answers[1].value;
                    $scope.numflag=true;
                    $scope.numfieldmsg="";

                    if(!(!answer1.trim())){
                        if(isNaN(parseInt(answer1)) || answer1<0){
                            $scope.answers[1].value="";
                            $scope.numflag=false;
                            $scope.numfieldmsg="Field accepts only positive numbers";
                            return false;
                        }else $scope.numflag=true;
                    }

            var data = {answerList: $scope.answers}
            $http({
                method: 'POST',
                data: data,
                url: restUrl + "/miscellaneous/surveyservice/saveanswers",
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            }).success(function (response) {
                $state.go('home.survey_Page2', {"id":$stateParams.id , "pageNo": 2})
            });

        }

        $scope.numValidation = function(){


        }
 });

 sfe.controller('page2controller', function ($scope, $http, $rootScope, $compile, $state, $stateParams,LocalStore, RepositoryService, UIService) {

 $scope.nextPage={};
          $scope.answers =[];
        $scope.init=function() {
            $scope.q1Alert="";
            $scope.q2Alert="Enter value between 0 to 100";
            RepositoryService.find('doctr', {'id': $stateParams.id}, 'simple').success(function (data) {
            $scope.doctor = data[0];
                return $http({
                    method: 'GET',
                    url: restUrl + "/miscellaneous/surveyservice/survey/" + $stateParams.id + '/' + $stateParams.pageNo,
                    headers: {
                        "content-type": "application/json",
                        "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                    }
                }).success(function (data) {
                    $scope.answers = data;
                })
            })

        }

        $scope.nextPage = function(){
                    var answer0 = $scope.answers[0].value;
                    var answer1 = $scope.answers[1].value;
                    var answer2 = $scope.answers[2].value;
                    var answer3 = $scope.answers[3].value;
                    var answer4 = $scope.answers[4].value;
                    var answer5 = $scope.answers[5].value;
                    var answer6 = $scope.answers[6].value;

                    $scope.numflag0=true;
                    $scope.numflag1=true;
                    $scope.numflag2=true;
                    $scope.numflag3=true;
                    $scope.numflag4=true;
                    $scope.numflag5=true;
                    $scope.numflag6=true;

                    $scope.numfieldmsg="";

                    if(!(!answer0.trim())){
                        if(isNaN(parseInt(answer0)) || answer0<0){
                           console.log(2);
                            $scope.answers[0].value="";
                            $scope.numflag0=false;
                            $scope.numfieldmsg="Field accepts only positive numbers";
                            return false;
                        }else{
                        console.log(3);
                            $scope.numflag0=true;
                        }
                    }

                    if(!(!answer1.trim())){
                    if(isNaN(parseInt(answer1)) || answer1<0){
                        $scope.answers[1].value="";
                        $scope.numflag1=false;
                        $scope.numfieldmsg="Field accepts only positive numbers";
                        return false;
                    }else  $scope.numflag1=true;
                    }

                    if(!(!answer2.trim())){
                    if(isNaN(parseInt(answer2)) || answer2<0){
                        $scope.answers[2].value="";
                        $scope.numflag2=false;
                        $scope.numfieldmsg="Field accepts only positive numbers";
                        return false;
                    }else  $scope.numflag2=true;
                    }

                    if(!(!answer3.trim())){
                    if(isNaN(parseInt(answer3)) || answer3<0){
                        $scope.answers[3].value="";
                        $scope.numflag3=false;
                        $scope.numfieldmsg="Field accepts only positive numbers";
                        return false;
                    }else  $scope.numflag3=true;
                    }

                    if(!(!answer4.trim())){
                    if(isNaN(parseInt(answer4)) || answer4<0){
                        $scope.answers[4].value="";
                        $scope.numflag4=false;
                        $scope.numfieldmsg="Field accepts only positive numbers";
                        return false;
                    }else  $scope.numflag4=true;
                    }

                    if(!(!answer5.trim())){
                    if(isNaN(parseInt(answer5)) || answer5<0){
                        $scope.answers[5].value="";
                        $scope.numflag5=false;
                        $scope.numfieldmsg="Field accepts only positive numbers";
                        return false;
                    }else  $scope.numflag5=true;
                    }

                    if(!(!answer6.trim())){
                    if(isNaN(parseInt(answer6)) || answer6<0){
                        $scope.answers[6].value="";
                        $scope.numflag6=false;
                        $scope.numfieldmsg="Field accepts only positive numbers";
                        return false;
                    }else  $scope.numflag6=true;
                  }

            var data = {answerList: $scope.answers}
            $http({
                method: 'POST',
                data: data,
                url: restUrl + "/miscellaneous/surveyservice/saveanswers",
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            }).success(function (response) {
                $state.go('home.survey_Page3', {"id":$stateParams.id , "pageNo": 3})
            });

        }

         $scope.prvPage = function(){
                 var data = {answerList: $scope.answers}
                            $http({
                                method: 'POST',
                                data: data,
                                url: restUrl + "/miscellaneous/surveyservice/saveanswers",
                                headers: {
                                    "content-type": "application/json",
                                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                                }
                            }).success(function (response) {
                                $state.go('home.survey_Page1', {"id":$stateParams.id , "pageNo": 1})
                            });

         }



        $scope.numValidation = function(){

        }


});

sfe.controller('page3controller', function ($scope, $http, $rootScope, $compile, $state, $stateParams,LocalStore, RepositoryService, UIService) {

          $scope.ans=[];
          $scope.answers =[];
          $scope.ans1=false;
          $scope.ans2=false;
          $scope.ans3=false;
          $scope.ans4=false;

          $scope.init=function() {
            $scope.q1Alert="";
            $scope.q2Alert="Enter value between 0 to 100";
            RepositoryService.find('doctr', {'id': $stateParams.id}, 'simple').success(function (data) {
            $scope.doctor = data[0];
                return $http({
                    method: 'GET',
                    url: restUrl + "/miscellaneous/surveyservice/survey/" + $stateParams.id + '/' + $stateParams.pageNo,
                    headers: {
                        "content-type": "application/json",
                        "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                    }
                }).success(function (data) {
                    $scope.answers = data;
                    if($scope.answers[16].value=="1"){
                        $scope.ans1=true;
                    }
                    if($scope.answers[17].value=="1"){
                        $scope.ans2=true;
                    }
                   if($scope.answers[18].value=="1"){
                       $scope.ans3=true;
                   }
                  if($scope.answers[19].value=="1"){
                        $scope.ans4=true;
                  }
                })
            })

        }
 $scope.submit = function(){
                     var answer0 = $scope.answers[0].value;
                     var answer1 = $scope.answers[1].value;
                     var answer2 = $scope.answers[2].value;
                     var answer3 = $scope.answers[3].value;
                     var answer4 = $scope.answers[4].value;
                     var answer5 = $scope.answers[5].value;
                     var answer6 = $scope.answers[6].value;
                     var answer7 = $scope.answers[7].value;
                     var answer8 = $scope.answers[8].value;
                     var answer9 = $scope.answers[9].value;
                     var answer10 = $scope.answers[10].value;
                     var answer11 = $scope.answers[11].value;
                     var answer12 = $scope.answers[12].value;
                     var answer13 = $scope.answers[13].value;
                     var answer14 = $scope.answers[14].value;
                     var answer15 = $scope.answers[15].value;
                     $scope.numflag0=true;
                     $scope.numflag1=true;
                     $scope.numflag2=true;
                     $scope.numflag3=true;
                     $scope.numflag4=true;
                     $scope.numflag5=true;
                     $scope.numflag6=true;
                     $scope.numflag7=true;
                     $scope.numflag8=true;
                     $scope.numflag9=true;
                     $scope.numflag10=true;
                     $scope.numflag11=true;
                     $scope.numflag12=true;
                     $scope.numflag13=true;
                     $scope.numflag14=true;
                     $scope.numflag15=true;
                     $scope.numfieldmsg="";

                        if(!(!answer0.trim())){
                             if(isNaN(parseInt(answer0)) || answer0<0){
                                 $scope.answers[0].value="";
                                 $scope.numflag0=false;
                                 $scope.numfieldmsg="Field accepts only positive numbers";
                                 return false;
                             }else  $scope.numflag0=true;
                        }
                        if(!(!answer1.trim())){
                             if(isNaN(parseInt(answer1)) || answer1<0){
                                 $scope.answers[1].value="";
                                 $scope.numflag1=false;
                                 $scope.numfieldmsg="Field accepts only positive numbers";
                                 return false;
                             }else  $scope.numflag1=true;
                        }
                         if(!(!answer2.trim())){
                             if(isNaN(parseInt(answer2)) || answer2<0){
                                 $scope.answers[2].value="";
                                 $scope.numflag2=false;
                                 $scope.numfieldmsg="Field accepts only positive numbers";
                                return false;
                             }else  $scope.numflag2=true;
                        }

                         if(!(!answer3.trim())){
                             if(isNaN(parseInt(answer3)) || answer3<0){
                                 $scope.answers[3].value="";
                                 $scope.numflag3=false;
                                 $scope.numfieldmsg="Field accepts only positive numbers";
                                 return false;
                             }else  $scope.numflag3=true;
                        }

                         if(!(!answer4.trim())){
                             if(isNaN(parseInt(answer4)) || answer4<0){
                                 $scope.answers[4].value="";
                                 $scope.numflag4=false;
                                 $scope.numfieldmsg="Field accepts only positive numbers";
                                 return false;
                             }else  $scope.numflag4=true;
                        }

                         if(!(!answer5.trim())){
                         if(isNaN(parseInt(answer5)) || answer5<0){
                             $scope.answers[5].value="";
                             $scope.numflag5=false;
                             $scope.numfieldmsg="Field accepts only positive numbers";
                             return false;
                         }else  $scope.numflag5=true;
                         }
                          if(!(!answer6.trim())){
                         if(isNaN(parseInt(answer6)) || answer6<0){
                             $scope.answers[6].value="";
                             $scope.numflag6=false;
                             $scope.numfieldmsg="Field accepts only positive numbers";
                             return false;
                         }else  $scope.numflag6=true;
                         }

                         if(!(!answer7.trim())){
                         if(isNaN(parseInt(answer7)) || answer7<0){
                             $scope.answers[7].value="";
                             $scope.numflag7=false;
                             $scope.numfieldmsg="Field accepts only positive numbers";
                            return false;
                         }else  $scope.numflag7=true;
                         }
                          if(!(!answer8.trim())){
                         if(isNaN(parseInt(answer8)) || answer8<0){
                             $scope.answers[8].value="";
                             $scope.numflag8=false;
                             $scope.numfieldmsg="Field accepts only positive numbers";
                             return false;
                         }else  $scope.numflag8=true;
                         }
                          if(!(!answer9.trim())){
                         if(isNaN(parseInt(answer9)) || answer9<0){
                              $scope.answers[9].value="";
                              $scope.numflag9=false;
                              $scope.numfieldmsg="Field accepts only positive numbers";
                              return false;
                         }else  $scope.numflag9=true;
                            }

                         if(!(!answer10.trim())){
                             if(isNaN(parseInt(answer10)) || answer10<0){
                                   $scope.answers[10].value="";
                                   $scope.numflag10=false;
                                   $scope.numfieldmsg="Field accepts only positive numbers";
                                   return false;
                               }else  $scope.numflag10=true;
                         }

                         if(!(!answer11.trim())){
                         if(isNaN(parseInt(answer11)) || answer11<0){

                            $scope.answers[11].value="";
                            $scope.numflag11=false;
                            $scope.numfieldmsg="Field accepts only positive numbers";
                            return false;
                         }else  $scope.numflag11=true;
                         }

                         if(!(!answer12.trim())){
                         if(isNaN(parseInt(answer12)) || answer12<0){
                             $scope.answers[12].value="";
                             $scope.numflag12=false;
                             $scope.numfieldmsg="Field accepts only positive numbers";
                            return false;
                         }else  $scope.numflag12=true;
                         }

                          if(!(!answer13.trim())){
                         if(isNaN(parseInt(answer13)) || answer13<0){
                          $scope.answers[13].value="";
                          $scope.numflag13=false;
                          $scope.numfieldmsg="Field accepts only positive numbers";
                          return false;
                         }else  $scope.numflag13=true;
                         }

                          if(!(!answer14.trim())){
                         if(isNaN(parseInt(answer14)) || answer14<0){
                             $scope.answers[14].value="";
                             $scope.numflag14=false;
                             $scope.numfieldmsg="Field accepts only positive numbers";
                            return false;
                         }else  $scope.numflag14=true;
                         }
                          if(!(!answer15.trim())){
                         if(isNaN(parseInt(answer15)) || answer15<0){
                             $scope.answers[15].value="";
                             $scope.numflag15=false;
                             $scope.numfieldmsg="Field accepts only positive numbers";
                             return false;
                         }else  $scope.numflag15=true;
                            }


            if($scope.ans1)
                $scope.answers[16].value="1";
            else
               $scope.answers[16].value="";


            if($scope.ans2)
               $scope.answers[17].value="1";
            else
              $scope.answers[17].value="";


            if($scope.ans3)
                $scope.answers[18].value="1";
            else
               $scope.answers[18].value="";


            if($scope.ans4)
                $scope.answers[19].value="1";
            else
               $scope.answers[19].value="";

            var data = {answerList: $scope.answers}

            $http({
                method: 'POST',
                data: data,
                url: restUrl + "/miscellaneous/surveyservice/submit/" + $stateParams.id,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            }).success(function (response) {
                alert('Survey submitted successfully.');
                $state.go('home.search_doctor',({context:'advanced'}));
            });

        }

        $scope.prvPage = function(){
         var data = {answerList: $scope.answers}
                    $http({
                        method: 'POST',
                        data: data,
                        url: restUrl + "/miscellaneous/surveyservice/saveanswers",
                        headers: {
                            "content-type": "application/json",
                            "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                        }
                    }).success(function (response) {
                        $state.go('home.survey_Page2', {"id":$stateParams.id , "pageNo": 2})
                    });

        }

         $scope.numValidation = function(){

                }


});


sfe.controller('EmployPage1Controller', function ($scope, $http, $rootScope, $compile, $state, $stateParams,LocalStore, RepositoryService, UIService) {

        $scope.nextPage={};
        $scope.answers =[];
        $scope.que3emply ={};
        $scope.que5emply ={};
        $scope.q3list = [];
        $scope.q5list = [];
        $scope.q8list = [];
        $scope.q11list = [];
        $scope.que8emply = {};
        $scope.que11emply = {};
        $scope.employees =[];
        $scope.freq1 = "";
        $scope.freq2 = "";
        $scope.freq3 = "";
        $scope.freq4 = "";
        $scope.error = "";
        $scope.mandatory =false;
       $scope.motivation1 = "";
       $scope.motivation2 = "";
       $scope.motivation3 = "";
       $scope.motivation4 = "";
        $scope.err3 = false;
        $scope.err5 = false;
        $scope.err8 = false;
        $scope.err11 = false;
        $scope.fieldsMissing = false;
        $scope.divisionSelected1 = {};
        $scope.divisionSelected2 = {};
        $scope.divisionSelected3 = {};
        $scope.divisionSelected4 = {};
        $scope.employeeList1 = [];
        $scope.employeeList2 = [];
        $scope.employeeList3 = [];
        $scope.employeeList4 = [];


        $scope.init=function() {

            //RepositoryService.find('employee_survey', {}, 'simple').success(function (data) {
              // $scope.employees = data;
              RepositoryService.find("employee_survey",{},"advanced").success(function (edata) {
                $scope.employees = edata;

                RepositoryService.find("divsn",{},"advanced").then(function (data) {
                    $scope.divisions = data.data;
                });

                return $http({
                    method: 'GET',
                    url: restUrl + "/miscellaneous/surveyservice/survey/" + LocalStore.fetch("headerdata").id + '/' + 1 +'/SURVY00000000000000000000000000000002',
                    headers: {
                        "content-type": "application/json",
                        "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                    }
                }).success(function (data) {
                    $scope.answers = data;

                    console.log($scope.answers[3])

                    for(n in $scope.answers[3]){
                        for(d in $scope.employees){
                        var tList ={id:null,displayName:null,freqValue:null,freqName:null};

                            if($scope.employees[d].emplId==$scope.answers[3][n].value1){
                                                console.log($scope.employees[d].emplId==$scope.answers[3][n].value1)

                                tList.id=$scope.answers[3][n].value1;
                                tList.displayName=$scope.employees[d].empName;

                                if($scope.answers[3][n].value2 == null ){
                                    tList.freqValue = $scope.answers[3][n].value2;
                                    tList.freqName = 'Frequency not selected';
                                }
                                if($scope.answers[3][n].value2 =='1' ){
                                    tList.freqValue = $scope.answers[3][n].value2;
                                    tList.freqName = 'Several times a week';
                                }
                                if($scope.answers[3][n].value2 =='2' ){
                                    tList.freqValue = $scope.answers[3][n].value2;
                                    tList.freqName = 'Once a week';
                                }
                                if($scope.answers[3][n].value2 =='3' ){
                                    tList.freqValue = $scope.answers[3][n].value2;
                                    tList.freqName = 'Once in 2 weeks or more';
                                }

                                if($scope.answers[3][n].value3 == '1'){
                                   tList.motivationValue = $scope.answers[3][n].value3;
                                   tList.motivationName = 'High';
                                }

                                if($scope.answers[3][n].value3 == '2'){
                                   tList.motivationValue = $scope.answers[3][n].value3;
                                   tList.motivationName = 'Medium';
                                }

                                if($scope.answers[3][n].value3 == '3'){
                                   tList.motivationValue = $scope.answers[3][n].value3;
                                   tList.motivationName = 'Low';
                                }

                                $scope.q3list.push(tList);
                            break;
                            }
                        }
                     }


                   for(n in $scope.answers[5]){
                          for(d in $scope.employees){
                            var tList ={id:null,displayName:null,freqValue:null,freqName:null};

                            if($scope.employees[d].emplId==$scope.answers[5][n].value1){
                                tList.id=$scope.answers[5][n].value1;
                                tList.displayName=$scope.employees[d].empName;

                                if($scope.answers[5][n].value2 == null ){
                                    tList.freqValue = $scope.answers[5][n].value2;
                                    tList.freqName = 'Frequency not selected';
                                }
                                if($scope.answers[5][n].value2 =='1' ){
                                    tList.freqValue = $scope.answers[5][n].value2;
                                    tList.freqName = 'Several times a week';
                                }
                                if($scope.answers[5][n].value2 =='2' ){
                                    tList.freqValue = $scope.answers[5][n].value2;
                                    tList.freqName = 'Once a week';
                                }
                                if($scope.answers[5][n].value2 =='3' ){
                                    tList.freqValue = $scope.answers[5][n].value2;
                                    tList.freqName = 'Once in 2 weeks or more';
                                }

                                if($scope.answers[5][n].value3 == '1'){
                                   tList.motivationValue = $scope.answers[5][n].value3;
                                   tList.motivationName = 'High';
                                }

                                if($scope.answers[5][n].value3 == '2'){
                                   tList.motivationValue = $scope.answers[5][n].value3;
                                   tList.motivationName = 'Medium';
                                }
                                if($scope.answers[5][n].value3 == '3'){
                                   tList.motivationValue = $scope.answers[5][n].value3;
                                   tList.motivationName = 'Low';
                                }

                                $scope.q5list.push(tList);
                            break;
                            }
                        }
                    }

                })
            })
        }

        $scope.getEmployeeList1 = function(){
            /*RepositoryService.find("emply_per_division",{division:$scope.divisionSelected1},"advanced").then(function (data) {
                $scope.employeeList1 = data.data;
                                console.log(data.data)

            });*/
            $scope.employeeList1=[];
            for(e in $scope.employees){
                if($scope.employees[e].divId==$scope.divisionSelected1){
                    $scope.employeeList1.push($scope.employees[e]);
                }
            }
        }

        $scope.getEmployeeList2 = function(){
            $scope.employeeList2=[];
            for(e in $scope.employees){
               if($scope.employees[e].divId==$scope.divisionSelected2){
                   $scope.employeeList2.push($scope.employees[e]);
               }
           }

        }

        $scope.getEmployeeList3 = function(){
             $scope.employeeList3=[];
             for(e in $scope.employees){
                if($scope.employees[e].divId==$scope.divisionSelected3){
                    $scope.employeeList3.push($scope.employees[e]);
                }
            }
        }

        $scope.getEmployeeList4 = function(){
             $scope.employeeList4=[];
             for(e in $scope.employees){
                if($scope.employees[e].divId==$scope.divisionSelected4){
                    $scope.employeeList4.push($scope.employees[e]);
                }
            }
        }


        $scope.addEmployees1 = function(){

            $scope.fieldsMissing1 = false;
            if($scope.que3emply==null || $scope.que3emply=="" || $scope.motivation1==null || $scope.motivation1=="" || $scope.freq1==null || $scope.freq1==""){
                $scope.fieldsMissing1 = true;
                return;
            }

             var flag=0;
             var empId1={id:null,displayName:null,freqValue:null,freqName:null,motivationValue:null,motivationName:null};

            empId1.id = $scope.que3emply[0].emplId;
            empId1.displayName = $scope.que3emply[0].empName;


/*
             for(n in $scope.employeeList1){
                if($scope.employeeList1[n].emplId == $scope.que3emply){
                    empId1.id = $scope.que3emply;
                    empId1.displayName = $scope.employeeList1[n].empName;

                }
             }
*/
            if($scope.motivation1==1){
                empId1.motivationValue = $scope.motivation1;
                empId1.motivationName = "High";
            }
            if($scope.motivation1==2){
                empId1.motivationValue = $scope.motivation1;
                empId1.motivationName = "Medium";
            }
            if($scope.motivation1==3){
                 empId1.motivationValue = $scope.motivation1;
                 empId1.motivationName = "Low";
            }

            if($scope.freq1==1){
                empId1.freqValue = 1;
                empId1.freqName = "Several times a week";
            }
            if($scope.freq1==2){
                empId1.freqValue = 2;
                empId1.freqName = "Once a week";
            }
            if($scope.freq1==3){
                empId1.freqValue = 3;
                empId1.freqName = "Once in 2 weeks or more";
            }

            if($scope.q3list.length>4){
                alert("Maximum 5 Employees are expected.");
                return;
            }

            for(n in $scope.q3list){
                if($scope.q3list[n].id == $scope.que3emply)
                        flag=1;
            }

            if(flag==1){
                alert("You can not select same employee multiple times");
                 return false;
            }

            $scope.q3list.push(empId1);

            var temp ={value1:empId1.id,
                        value2:empId1.freqValue,
                        value3:empId1.motivationValue,
                        displaySequence:$scope.q3list.length,
                        owner:$scope.answers[3][0].owner,
                        question: $scope.answers[3][0].question};

            var tempArrary1 = [];
            tempArrary1.push(temp);
            $scope.answers.push(tempArrary1);

            $scope.divisionSelected1 = "";
            $scope.que3emply ="";
            $scope.freq1="";
            $scope.motivation1 ="";

        }

         $scope.deleteEmployee1 = function(index,id){
            for(n in $scope.answers[3]){
                if($scope.answers[3][n].value1==null || !$scope.answers[3][n].value1.trim())
                    $scope.answers[3].splice(n,1);
            }

            for(a in $scope.answers){
                if($scope.answers[a][0].value1==id){
                    $scope.answers.splice(a,1);

                }
            }

            $scope.q3list.splice(index,1);
         }


        $scope.addEmployees2 = function(){

            $scope.fieldsMissing2 = false;
            if($scope.que5emply==null || $scope.que5emply=="" || $scope.motivation2==null || $scope.motivation2=="" || $scope.freq2==null || $scope.freq2==""){
                $scope.fieldsMissing2 = true;
                return;
            }

             var flag=0;
             var empId1={id:null,displayName:null,freqValue:null,freqName:null,motivationValue:null,motivationName:null};


             empId1.id = $scope.que5emply[0].emplId;
             empId1.displayName = $scope.que5emply[0].empName;

            if($scope.freq2==1){
                empId1.freqValue = 1;
                empId1.freqName = "Several times a week";
            }
            if($scope.freq2==2){
                empId1.freqValue = 2;
                empId1.freqName = "Once a week";
            }
            if($scope.freq2==3){
                empId1.freqValue = 3;
                empId1.freqName = "Once in 2 weeks or more";
            }

            if($scope.motivation2==1){
                empId1.motivationValue = $scope.motivation2;
                empId1.motivationName = "High";
            }
            if($scope.motivation2==2){
                empId1.motivationValue = $scope.motivation2;
                empId1.motivationName = "Medium";
            }
            if($scope.motivation2==3){
                 empId1.motivationValue = $scope.motivation2;
                 empId1.motivationName = "Low";
            }


            if($scope.q5list.length>4){
                alert("Maximum 5 Employees are expected.");
                return;
            }
            for(n in $scope.q5list){
                if($scope.q5list[n].id == $scope.que5emply[0].emplId)
                        flag=1;
            }

            if(flag==1){
                alert("You can not select same employee multiple times");
                 return false;
            }

            $scope.q5list.push(empId1);

            var temp ={value1:empId1.id,
                        value2:empId1.freqValue,
                        value3:empId1.motivationValue,
                        displaySequence:$scope.q5list.length,
                        owner:$scope.answers[5][0].owner,
                        question: $scope.answers[5][0].question};

            var tempArrary2 = [];
           tempArrary2.push(temp);
           $scope.answers.push(tempArrary2);


            $scope.divisionSelected2 = "";
            $scope.que5emply ="";
            $scope.freq2="";
            $scope.motivation2="";

        }

         $scope.deleteEmployee2 = function(index,id){

         for(n in $scope.answers[5]){
             if($scope.answers[5][n].value1 == id)
                 $scope.answers[5].splice(n,1);
         }


            for(a in $scope.answers){
                if($scope.answers[a][0].value1==id){
                    $scope.answers.splice(a,1);

                }
            }
            $scope.q5list.splice(index,1);
         }


        $scope.nextPage = function(){
        console.log($scope.answers)

                $scope.mandatory = false;
                $scope.errors = [];

                if($scope.answers[0][0].value1 == ""){
                        $scope.mandatory = true;
                        $scope.errors.push("Question 1 is mandatory.");
                }
                if($scope.answers[1][0].value1 == ""){
                        $scope.mandatory = true;
                        $scope.errors.push("Question 2 is mandatory.");

                }
                if($scope.answers[2][0].value1 == ""){
                        $scope.mandatory = true;
                        $scope.errors.push("Question 3 is mandatory.");

                }
                if($scope.answers[4][0].value1 == ""){
                        $scope.mandatory = true;
                        $scope.errors.push("Question 5 is mandatory.");

                }
                if($scope.mandatory == true){
                    return false;
                }


                var data = {answerList: $scope.answers}
                $http({
                    method: 'POST',
                    data: data,
                    url: restUrl + "/miscellaneous/surveyservice/saveanswers/"+ LocalStore.fetch("headerdata").id +'/SURVY00000000000000000000000000000002' + '/'+1,
                    headers: {
                        "content-type": "application/json",
                        "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                    }
                }).success(function (response) {
                  $state.go('home.employee_surveyPg2');
                });

        }

         $scope.init2=function() {

          RepositoryService.find("employee_survey",{},"advanced").success(function (edata) {
                         $scope.employees = edata;

                    RepositoryService.find("divsn",{},"advanced").then(function (data) {
                            $scope.divisions = data.data;
                        });

                            return $http({
                                method: 'GET',
                                url: restUrl + "/miscellaneous/surveyservice/survey/" + LocalStore.fetch("headerdata").id + '/' + 2 +'/SURVY00000000000000000000000000000002',
                                headers: {
                                    "content-type": "application/json",
                                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                                }
                            }).success(function (data) {

                                $scope.answers = data;


                                for(n in $scope.answers[2]){
                                    for(d in $scope.employees){
                                        var tList ={id:null,displayName:null,freqValue:null,freqName:null};

                                        if($scope.employees[d].emplId==$scope.answers[2][n].value1){
                                            tList.id=$scope.answers[2][n].value1;
                                            tList.displayName=$scope.employees[d].empName;

                                            if($scope.answers[2][n].value2 =='1' ){
                                                tList.freqValue = $scope.answers[2][n].value2;
                                                tList.freqName = 'Several times a week';
                                            }
                                            if($scope.answers[2][n].value2 =='2' ){
                                                tList.freqValue = $scope.answers[2][n].value2;
                                                tList.freqName = 'Once a week';
                                            }
                                            if($scope.answers[2][n].value2 =='3' ){
                                                tList.freqValue = $scope.answers[2][n].value2;
                                                tList.freqName = 'Once in 2 weeks or more';
                                            }

                                            if($scope.answers[2][n].value3 == '1'){
                                               tList.motivationValue = $scope.answers[2][n].value3;
                                               tList.motivationName = 'High';
                                            }

                                            if($scope.answers[2][n].value3 == '2'){
                                               tList.motivationValue = $scope.answers[2][n].value3;
                                               tList.motivationName = 'Medium';
                                            }
                                            if($scope.answers[2][n].value3 == '3'){
                                               tList.motivationValue = $scope.answers[2][n].value3;
                                               tList.motivationName = 'Low';
                                            }

                                            $scope.q8list.push(tList);
                                             break;
                                        }
                                    }
                                }

                                for(n in $scope.answers[4]){
                                    for(d in $scope.employees){
                                        var tList ={id:null,displayName:null,freqValue:null,freqName:null};

                                        if($scope.employees[d].emplId==$scope.answers[4][n].value1){
                                            tList.id=$scope.answers[4][n].value1;
                                            tList.displayName=$scope.employees[d].empName;

                                            if($scope.answers[4][n].value2 =='1' ){
                                                tList.freqValue = $scope.answers[4][n].value2;
                                                tList.freqName = 'Several times a week';
                                            }
                                            if($scope.answers[4][n].value2 =='2' ){
                                                tList.freqValue = $scope.answers[4][n].value2;
                                                tList.freqName = 'Once a week';
                                            }
                                            if($scope.answers[4][n].value2 =='3' ){
                                                tList.freqValue = $scope.answers[4][n].value2;
                                                tList.freqName = 'Once in 2 weeks or more';
                                            }

                                            if($scope.answers[4][n].value3 == '1'){
                                               tList.motivationValue = $scope.answers[4][n].value3;
                                               tList.motivationName = 'High';
                                            }

                                            if($scope.answers[4][n].value3 == '2'){
                                               tList.motivationValue = $scope.answers[4][n].value3;
                                               tList.motivationName = 'Medium';
                                            }
                                            if($scope.answers[4][n].value3 == '3'){
                                               tList.motivationValue = $scope.answers[4][n].value3;
                                               tList.motivationName = 'Low';
                                            }

                                            $scope.q11list.push(tList);
                                        break;
                                        }
                                    }
                                }
                            })
                    })
         }

        $scope.addEmployees3 = function(){

            $scope.fieldsMissing3 = false;
            if($scope.que8emply==null || $scope.que8emply=="" || $scope.motivation3==null || $scope.motivation3=="" || $scope.freq3==null || $scope.freq3==""){
                $scope.fieldsMissing3 = true;
                return;
            }

             var flag=0;
             var empId1={id:null,displayName:null,freqValue:null,freqName:null,motivationValue:null,motivationName:null};

             empId1.id = $scope.que8emply[0].emplId;
             empId1.displayName = $scope.que8emply[0].empName;


            if($scope.freq3==1){
                empId1.freqValue = 1;
                empId1.freqName = "Several times a week";
            }
            if($scope.freq3==2){
                empId1.freqValue = 2;
                empId1.freqName = "Once a week";
            }
            if($scope.freq3==3){
                empId1.freqValue = 3;
                empId1.freqName = "Once in 2 weeks or more";
            }

            if($scope.motivation3==1){
                empId1.motivationValue = $scope.motivation3;
                empId1.motivationName = "High";
            }
            if($scope.motivation3==2){
                empId1.motivationValue = $scope.motivation3;
                empId1.motivationName = "Medium";
            }
            if($scope.motivation3==3){
                empId1.motivationValue = $scope.motivation3;
                empId1.motivationName = "Low";
            }


            if($scope.q8list.length>4){
                alert("Maximum 5 Employees are expected.");
                return;
            }
            for(n in $scope.q8list){
                if($scope.q8list[n].id == $scope.que8emply[0].emplId)
                        flag=1;
            }

            if(flag==1){
                alert("You can not select same employee multiple times");
                 return false;
            }


            $scope.q8list.push(empId1);

            var temp ={value1:empId1.id,
                           value2:empId1.freqValue,
                           value3:empId1.motivationValue,
                           displaySequence:$scope.q8list.length,
                           owner:$scope.answers[2][0].owner,
                           question: $scope.answers[2][0].question}

            var tempArrary3 = [];
            tempArrary3.push(temp);
            $scope.answers.push(tempArrary3);

            $scope.divisionSelected3 = "";
            $scope.que8emply ="";
            $scope.freq3="";
            $scope.motivation3 ="";


        }

         $scope.deleteEmployee3 = function(index,id){
            for(n in $scope.answers[2]){
                 if($scope.answers[2][n].value1 == id)
                     $scope.answers[2].splice(n,1);
             }


            for(a in $scope.answers){
                if($scope.answers[a][0].value1==id){
                    $scope.answers.splice(a,1);

                }
            }
            $scope.q8list.splice(index,1);
         }


        $scope.addEmployees4 = function(){

         $scope.fieldsMissing4 = false;
        if($scope.que11emply==null || $scope.que11emply=="" || $scope.motivation4==null || $scope.motivation4=="" || $scope.freq4==null || $scope.freq4==""){
            $scope.fieldsMissing4 = true;
            return;
        }

             var flag=0;
             var empId1={id:null,displayName:null,freqValue:null,freqName:null,motivationValue:null,motivationName:null};

             empId1.id = $scope.que11emply[0].emplId;
             empId1.displayName = $scope.que11emply[0].empName;

            if($scope.freq4==1){
                empId1.freqValue = 1;
                empId1.freqName = "Several times a week";
            }
            if($scope.freq4==2){
                empId1.freqValue = 2;
                empId1.freqName = "Once a week";
            }
            if($scope.freq4==3){
                empId1.freqValue = 3;
                empId1.freqName = "Once in 2 weeks or more";
            }

            if($scope.motivation4==1){
                empId1.motivationValue = $scope.motivation4;
                empId1.motivationName = "High";
            }
            if($scope.motivation4==2){
                empId1.motivationValue = $scope.motivation4;
                empId1.motivationName = "Medium";
            }
            if($scope.motivation4==3){
                 empId1.motivationValue = $scope.motivation4;
                 empId1.motivationName = "Low";
             }


            if($scope.q11list.length>4){
                alert("Maximum 5 Employees are expected.");
                return;
            }
            for(n in $scope.q11list){
                if($scope.q11list[n].id == $scope.que11emply[0].emplId )
                        flag=1;
            }

            if(flag==1){
                alert("You can not select same employee multiple times");
                 return false;
            }


            $scope.q11list.push(empId1);

            var temp  ={value1:empId1.id,
                        value2:empId1.freqValue,
                        value3:empId1.motivationValue,
                        displaySequence:$scope.q11list.length,
                        owner:$scope.answers[4][0].owner,
                        question: $scope.answers[4][0].question}

            var tempArrary4 = [];
            tempArrary4.push(temp);
            $scope.answers.push(tempArrary4);

            $scope.divisionSelected4 = "";
            $scope.que11emply ="";
                $scope.freq4="";
                $scope.motivation4 = "";

        }

         $scope.deleteEmployee4 = function(index,id){
            for(n in $scope.answers[4]){
                if($scope.answers[4][n].value1 == id)
                    $scope.answers[4].splice(n,1);
            }


            for(a in $scope.answers){
                if($scope.answers[a][0].value1==id){
                    $scope.answers.splice(a,1);

                }
            }
            $scope.q11list.splice(index,1);
         }

         $scope.prvPage = function(){


                var data = {answerList: $scope.answers}
                           $http({
                               method: 'POST',
                               data: data,
                               url: restUrl + "/miscellaneous/surveyservice/saveanswers/" + LocalStore.fetch("headerdata").id +'/SURVY00000000000000000000000000000002' + '/'+2,
                               headers: {
                                   "content-type": "application/json",
                                   "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                               }
                           }).success(function (response) {
                               $state.go('home.employee_surveyPg1')
                 });
         }

        $scope.save = function(){
            $scope.mandatory = false;
            $scope.errors = [];
             if($scope.answers[0][0].value1 == ""){
                    $scope.mandatory = true;
                    $scope.errors.push("Question 7 is mandatory.");

            }
            if($scope.answers[1][0].value1 == ""){
                    $scope.mandatory = true;
                    $scope.errors.push(" Question 8 is mandatory.");

            }
            if($scope.answers[3][0].value1 == ""){
                    $scope.mandatory = true;
                    $scope.errors.push(" Question 10 is mandatory.");
            }

            if($scope.mandatory == true){
                   return false;
            }



                var data = {answerList: $scope.answers}
                 $http({
                     method: 'POST',
                     data: data,
                     url: restUrl + "/miscellaneous/surveyservice/saveanswers/" + LocalStore.fetch("headerdata").id +'/SURVY00000000000000000000000000000002' + '/'+2,
                     headers: {
                         "content-type": "application/json",
                         "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                     }
                 }).success(function (response) {
                         console.log(response)
                 });
        }

        $scope.submit = function(){
            $scope.m = "";
            if($scope.answers[0][0].value1 == ""){
                   $scope.m = true;
                   $scope.error ="Question 7 is mandatory.";
                   return false;
           }
           if($scope.answers[1][0].value1 == ""){
                   $scope.m = true;
                   $scope.error ="Question 8 is mandatory.";
                   return false;
           }
           if($scope.answers[3][0].value1 == ""){
                   $scope.m = true;
                   $scope.error ="Question 10 is mandatory.";
                   return false;
           }

            var data = {answerList: $scope.answers};
             $http({
                            method: 'POST',
                            data: data,
                            url: restUrl + "/miscellaneous/surveyservice/submit/" + LocalStore.fetch("headerdata").id + "/SURVY00000000000000000000000000000002/" + 2 ,
                            headers: {
                                "content-type": "application/json",
                                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                            }
                        }).success(function (response) {
                            alert('Survey submitted successfully.');
                        });
        }



 });

sfe.controller('hairClinicSurveyController', function ($scope, $http, $rootScope, $compile, $state, $stateParams,LocalStore, RepositoryService, UIService,SearchConfigService) {
$scope.answers =[];
$scope.specialityList =[];
$scope.que81 = {};
$scope.que82 = {};
$scope.que83 = {};
$scope.que84 = {};


    $scope.init = function(){
        var isEdit = 0;

        if($stateParams.id!= null ){
                    isEdit = $stateParams.id;

        }
             return $http({
                    method: 'GET',
                    url: restUrl + "/miscellaneous/surveyservice/survey/" + LocalStore.fetch("headerdata").id + '/' + 1 +'/SURVY00000000000000000000000000000004/'+ isEdit,
                    headers: {
                        "content-type": "application/json",
                        "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                    }
                }).success(function (data) {
                    $scope.answers = data;
                    $scope.que81 = $scope.answers[7][0].value1;
                    $scope.que82 = $scope.answers[7][0].value2;
                    $scope.que83 = $scope.answers[7][0].value3;
                    $scope.que84 = $scope.answers[7][0].value4;

                    RepositoryService.find("usrlv",{"type":'doctor_specialty'},"simple").then(function (data) {
                                $scope.specialityList = data.data;
                            });
                });

    }

    $scope.addNew = function(){
            var columnDefinitions = SearchConfigService.getColumns($state.current.data.labels);
            $state.go(columnDefinitions.newUrl);
        }



    $scope.clearBox = function (){
       $scope.answers[11][0].value2 ="";
    }



    $scope.save = function(){
              /*  $scope.mandatory = false;
                $scope.errors = [];
                 if($scope.answers[0][0].value1 == ""){
                        $scope.mandatory = true;
                        $scope.errors.push("Question 7 is mandatory.");

                }
                if($scope.answers[1][0].value1 == ""){
                        $scope.mandatory = true;
                        $scope.errors.push(" Question 8 is mandatory.");

                }
                if($scope.answers[3][0].value1 == ""){
                        $scope.mandatory = true;
                        $scope.errors.push(" Question 10 is mandatory.");
                }

                if($scope.mandatory == true){
                       return false;
                }
    */

                console.log($scope.answers)

                    var data = {answerList: $scope.answers}
                     $http({
                         method: 'POST',
                         data: data,
                         url: restUrl + "/miscellaneous/surveyservice/saveanswers/" + LocalStore.fetch("headerdata").id +'/SURVY00000000000000000000000000000004' + '/'+1,
                         headers: {
                             "content-type": "application/json",
                             "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                         }
                     }).success(function (response) {
                             alert("Survey saved successfully");
                     });
            }

});