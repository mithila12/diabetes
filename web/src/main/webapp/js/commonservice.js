sfe.service('CalendarService', function($http, LocalStore){
    this.getTourDetails = function(ownerId, month, year){
        var mth = month+ 1;
        var maxDate = new Date(year, month+1,0).getDate();
        var startDateFormatted = year + '-' + mth + '-' + 1;
        var endDateFormatted = year + '-' + mth + '-' + maxDate;
        return $http({
            method: 'GET',
            url: restUrl + "/sfe/tourplan/" + ownerId + "/" + startDateFormatted + "/" + endDateFormatted,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    }
});




sfe.factory('PrivilegeService',function(LocalStore){
   return {
       hasPrivilege : function(privilegeId){
           var privileges = LocalStore.fetch('userprivileges');
           for(var i=0;i<privileges.length;i++){
               if(privileges[i].id==privilegeId)
                   return true;
           }
           return false;
       }
   }
});

sfe.factory('TourPlanService',function($http, LocalStore, months, daysOfWeek) {
return {
    saveStandardPlan:function(plans){
        return $http({
            method: 'POST',
            url: restUrl + "/sfe/tourplan/standardTourPlan",
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data:plans
        });
    },
    getTourDetails: function (ownerId,month, year) {
        var mth = month + 1;
        var maxDate = new Date(year, month+1,0).getDate();
        var startDateFormatted = year + '-' + mth + '-' + 1;
        var endDateFormatted = year + '-' + mth + '-' + maxDate;

        return $http({
            method: 'GET',
            url: restUrl + "/sfe/tourplan/" + ownerId + "/" + startDateFormatted + "/" + endDateFormatted,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    },
    getTourDetailsForTerritory:function(locationId,year,month){
        return $http({
            method: 'GET',
            url: restUrl + "/sfe/tourplan/" + locationId + "/" + startDateFormatted + "/" + endDateFormatted,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    },
    getTourDataForManager: function (managerId, startDate, endDate) {
        return $http({
            method: 'GET',
            url: restUrl + "/sfe/tourplan/teamplan/" + managerId + "/" + startDate + "/" + endDate,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    },
    getJoineeDataForManager: function (managerId, startDate, endDate) {
        return $http({
            method: 'GET',
            url: restUrl + "/sfe/tourplan/joint/" + managerId + "/" + startDate + "/" + endDate,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    },
    defaultActivityList:  function (month, year) {
        var maxDate = new Date(year, month+1,0).getDate();
        var start = new Date(year, month, 1);
        var end = new Date(year, month, maxDate);
        var activity = new Array();
        var month = start.getMonth() + 1;
        while (start <= end) {
            activity.push({
                'activityDate': {
                    text: start.getDate() + '/' + month + '/' + start.getFullYear(),
                    date: start.getTime()
                },
                plannedActivityType: {id: ''},
                locationType: {id: null},
                numDoctors: 0,
                numChemists: 0
            });
            var newDate = start.setDate(start.getDate() + 1);
            start = new Date(newDate);
        }
        return activity;
    },
    editCreatePlan: function (plan) {
        return $http({
            method: 'PUT',
            url: restUrl + "/sfe/tourplan/",
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data: plan
        });
    },
    searchCustomers: function (context, planId, activityId, beatId, date) {
        var mth = date.getMonth() + 1;
        var formattedDate = date.getFullYear() + '-' +  mth + '-' + date.getDate();
        return $http({
            method: 'GET',
            url: restUrl + "/sfe/tourplan/" + context + "/" + planId + "/" + activityId + "/" +  beatId + '/' + formattedDate,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    },
    getDoctorProfilesForBeats: function(ids){
        return $http({
            method: 'POST',
            url: restUrl + "/sfe/tourplan/doctorprofiles",
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data:ids
        });
    },
    getDoctorProfiles: function(ids){
        return $http({
            method: 'PUT',
            url: restUrl + "/sfe/tourplan/doctorprofiles/" + LocalStore.fetch("headerdata").location.id,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data:ids
        });
    },
    getDoctorProfilesForActivity: function(id, planned){
        return $http({
            method: 'GET',
            url: restUrl + "/sfe/tourplan/customerprofiles/" + id + "?planned=" + planned,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    },
    getChemists: function(beatId,ids){
        return $http({
            method: 'PUT',
            url: restUrl + "/sfe/beats/chemists/" +beatId + '/' + LocalStore.fetch("headerdata").location.id,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data:ids
        });
    },
    getMyCustomers:function(employeeId){
        return $http({
            method: 'GET',
            url: restUrl + "/sfe/beats/customers/employee/"+ employeeId,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    },
    updateJoint: function(activityId, joint){
        return $http({
            method: 'PUT',
            url: restUrl + "/sfe/tourplan/join/" + activityId + '/'+ LocalStore.fetch('USER_INFO').id + '?joint=' + joint,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    },
    submitPlan:function(plan, status){
        return $http({
            method: 'PUT',
            url: restUrl + "/sfe/tourplan/approve/" + plan.id + '?status=' + status ,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    },
    rejectPlan:function(plan,comments){
        return $http({
            method:'POST',
            url: restUrl + "/sfe/tourplan/reject/" + plan.id,
            data:comments,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    },
    precallActivities:function(doctorId, numRows){
        return $http({
            method: 'GET',
            url: restUrl + "/sfe/tourplan/precall/doctor/" + doctorId + '?numRows=' + numRows ,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    },
    addAttendee:function(activityId, beatId, attendeeId){
        return $http({
            method: 'PUT',
            url: restUrl + "/sfe/tourplan/attendee/" + activityId  + '/' + beatId + '/' + attendeeId + "?locationId=" + LocalStore.fetch("headerdata").location.id,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    },
    removeAttendee:function(activityId, attendeeId){
        return $http({
            method: 'DELETE',
            url: restUrl + "/sfe/tourplan/attendee/" + activityId  + '/' + attendeeId + "?locationId=" + LocalStore.fetch("headerdata").location.id,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    },
    removeBeat :function(activityId, beatId){
        return $http({
            method: 'DELETE',
            url: restUrl + "/sfe/tourplan/beat/" + activityId  + '/' + beatId,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    },
    addBeat :function(activityId, beatId){
        return $http({
            method: 'POST',
            url: restUrl + "/sfe/tourplan/beat/" + activityId  + '/' + beatId,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    },
    copyPlan :function(activities){

        return $http({
            method: 'POST',
            url: restUrl +"/sfe/tourplan/copyTourPlan",
            headers:{
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data:activities
        });
    }


};
});

sfe.factory('LeaveService', function($http, LocalStore){
    return{
        submitLeave:function(leaveId, status){
            return $http({
                method: 'PUT',
                url: restUrl + "/sfe/leaveservice/approve/" + leaveId + '?status=' + status,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            })
        }
    }
});

sfe.factory('DoctorApprovalService', function($http, LocalStore){
    return{
        submitapproval:function(doctorId,employeeId,jobTitle,status){
            return $http({
                method: 'PUT',
                url: restUrl + "/sfe/doctorapproval/approve/" + doctorId +"/"+employeeId+"/"+jobTitle+'?status=' + status,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            })
        }
    }
});

sfe.factory('PatientApprovalService', function($http, LocalStore){
    return{
        submitapproval:function(patientId, status){
            return $http({
                method: 'PUT',
                url: restUrl + "/sfe/patientapproval/approve/" + patientId + '?status=' + status,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            })
        }
    }
});

sfe.factory('ActivityReportService',function($http, LocalStore, months, daysOfWeek) {
    return {
        getSecondarySales: function(stockist, date){
            return $http({
                method: 'GET',
                url: restUrl + "/sfe/activityreport/stockistSale/" + stockist + "/"  + date,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            });
        },
        getVisitActivity: function (owner, date) {
            return $http({
                method: 'PUT',
                url: restUrl + "/sfe/activityreport/" + owner + "/"  + date,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            });
        },
        updateStockistVisit: function(attendeeId, state){
            return $http({
                method:'PUT',
                url:restUrl + "/sfe/activityreport/stockistSales/" + attendeeId + "?visited=" + state ,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            });
        },
        updateDoctorVisit: function(attendeeref){
            return $http({
                method:'PUT',
                url: restUrl + "/sfe/activityreport/updateDoctorVisit/" + attendeeref ,
                headers:{
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            });
        },

        createVisitAttendee: function(attendee){
            return $http({
                method:'POST',
                url:restUrl + "/core/framework/ACTAT/",
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                },
                data: attendee
            });
        },
        saveVisitAttendee: function(attendee){
            return $http({
                method:'PUT',
                url:restUrl + "/core/framework/ACTAT/",
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                },
                data: attendee
            });
        },
        removeAttendee: function(attendee){
            return $http({
                method:'DELETE',
                url:restUrl + "/core/framework/ACTAT/" + attendee,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            });
        },
        saveDoctorChemistRCPA: function(doctorRx,chemistId){
            return $http({
                method:'POST',
                url: restUrl + "/sfe/activityreport/saveDocChemRCPA/" + chemistId,
                headers: {
                        "content-type": "application/json",
                        "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                },
                data: doctorRx
            });
        },

        saveDoctorDetailing: function(data){
            return $http({
                method:'PUT',
                url:restUrl + "/sfe/activityreport/details",
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                },
                data: data
            });
        },
        addPOBs: function(data){
            return $http({
                method:'POST',
                url:restUrl + "/sfe/activityreport/pobs",
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                },
                data: data
            });
        },
        addPOBDetail:function(object){
            return $http({
                method:'POST',
                url:restUrl + "/core/framework/POBDT/",
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                },
                data: object
            });
        },
        removePOBDetail: function(attendee){
            return $http({
                method:'DELETE',
                url:restUrl + "/core/framework/POBDT/" + attendee,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            });
        } ,
        reportChemistRCPA: function(detail){
            return $http({
                method:'POST',
                url:restUrl + "/sfe/activityreport/chemistRCPA",
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                },
                data: detail
            });
        },
        addJoinee: function(attendeeId,managers){
            return $http({
                method:'PUT',
                url:restUrl + "/sfe/activityreport/joint/" + attendeeId,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                },
                data: managers
            });
        },
        getJoinee: function(activityId){
            return $http({
                method:'GET',
                url:restUrl + "/sfe/activityreport/joint/" + activityId,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            });
        },
        addSecondarySales: function(detailId, stockistId, detail){
            return $http({
                method:'POST',
                url:restUrl + "/sfe/activityreport/stockistSale/" + detailId + "/" + stockistId,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                },
                data: detail
            });
        },
        getRCPA: function(attendeeId){
            return $http({
                method: 'GET',
                url: restUrl + "/sfe/activityreport/chemistRCPA/" + attendeeId,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            });
        },
        removeRCPA: function(rcpaId){
            return $http({
                method:'DELETE',
                url:restUrl + "/sfe/activityreport/chemistRCPA/" + rcpaId,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            });
        }
    };
});

sfe.service('CalendarCellPainter', function(){
    return {
        paint: function(month, year) {
            var maxDays = new Date(year,month + 1, 0).getDate();
            var weeks = [];
            var firstWeek = true;
            var dayNum = 0;
            var data = [];

            for (var i = 1; i <= maxDays; i++) {
                var today = new Date(year, month, i);
                var dayOfWeek = today.getDay();

                if (firstWeek) {
                    firstWeek = false;
                    for (var j = 0; j < dayOfWeek; j++)
                        data.push({date: '', time: -1});
                    dayNum = j;
                }
                var dFmted= moment(today).format('YYYY-MM-DD');
                data.push({date: today, time: today.getTime(), fmted: dFmted});
                j++;
                if (j > 6) {
                    j = 0;
                    weeks.push(data);
                    data = [];
                }
            }
            if (data.length > 0) {
                for (var j = data.length; j < 7; j++)
                    data.push({date: '', time: -1, fmted:''});
                weeks.push(data);
            }
            return weeks;
        }
    }
});

sfe.factory('HolidayService', function($http, LocalStore){
    return{

        configureForAllState:function(holidayList){
            return $http({
                method: 'POST',
                url: restUrl + "/sfe/holidayservice/configureHolidayForAllState",
                headers:{
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                },
                data: holidayList
            })
        },

        getHolidays:function(year){
            return $http({
                method: 'GET',
                url: restUrl + "/sfe/holidayservice/selectAll/" + year,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            });
        },
        deleteHoliday:function(sid,hid){
            return  $http({
                method: 'DELETE',
                url: restUrl + "/sfe/holidayservice/delete/" + sid+"/"+hid,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            });
        },
        deleteHolidays:function(hid){
            return  $http({
                method: 'DELETE',
                url: restUrl + "/sfe/holidayservice/delete/" +hid,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            });
        },
        updateHoliday:function(date,holiday){
            return  $http({
                method: 'PUT',
                url: restUrl + "/sfe/holidayservice/updatedate?date=" +date+"&holiday="+holiday,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            });
        },
        configureWeekend:function(configList){
            return $http({
                method: 'POST',
                url: restUrl + "/sfe/holidayservice/configureweekend",
                headers:{
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                },
                data: configList
            })
        },
        deleteweekendconfig:function(configList){
            return $http({
                method: 'DELETE',
                url: restUrl + "/sfe/holidayservice/deleteweekendconfig",
                headers:{
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                },
                data: configList
            })
        },
        fetchWeekendConfig:function(){
            return $http({
                method: 'POST',
                url: restUrl + "/sfe/holidayservice/fetchweekendconfig",
                headers:{
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            })
        },
        deleteAllWeekendConfigForDay:function(day){
            return $http({
                method: 'DELETE',
                url: restUrl + "/sfe/holidayservice/deleteAllWeekendConfigForDay/"+day,
                headers:{
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            })
        }
    }
});

