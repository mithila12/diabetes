(function () {
  'use strict';
  angular
    .module('app.patients')
    .controller('patientsctrl', patientsctrl);
  function patientsctrl(patientsservice, $window, $q, $state, sessionConstant, $scope, $timeout) {
    var vm = this;
    vm.getThreshold = getThreshold;
    vm.ShowSearch = false
    vm.showFilterBar = showFilterBar;
    vm.updateThreshold = updateThreshold;
    vm.loadThreshold = loadThreshold;
    vm.readOnly = true;
    vm.thresholdData = "";
    vm.currentPatient = sessionConstant.currentPatient;
    vm.goBack = goBackToList;
    vm.initPatientView = LoadPatientsList;
    vm.showFlag = ShouldShowFlag;
    vm.showDetails = goToPatientDetails;
    vm.loadPatientRecords = loadPatientRecords;
   // vm.listRecords = listRecords;
    vm.doRefreshEnterRecords = doRefreshEnterRecords;
    vm.response = [];
    vm.bld = false;
    vm.fp = false;
    vm.hb = false;
    vm.pp = false;
    vm.wg = false;
    vm.rb = false;
    vm.doRefresh = doRefresh;
    vm.ShouldShowSpinner = true;
    vm.spinnerStatus = 0;
    vm.openRecords = openRecords;
    vm.openThreadShold = openThreadShold;
    vm.getOverview = getOverview;
    vm.isGroupShown = isGroupShown;
    vm.toggleGroup = toggleGroup;
    vm.shownGroup = "";
    vm.getDiabetesPieData = getDiabetesPieData;
    vm.getCvdPieData = getCvdPieData;
    vm.getNephroPieData = getNephroPieData;
    vm.clear = clear;
    vm.riskLevel = sessionConstant.currentPatient.riskLevel
    vm.searchPatient = searchPatient;
    vm.back = back;
    vm.ShowSearch = false;
    vm.getMedicine = getMedicine;
    vm.getRegToday = getRegToday;

    function back() {
      $state.go('patients')
    }

    function getRegToday(){
    vm.userInfo = LocalStore.fetch('USER_INFO');
    console.log(vm.userInfo);
    }

    function toggleGroup(head) {
      if (vm.isGroupShown(head)) {
        vm.shownGroup = null;
      } else {
        vm.shownGroup = head;
      }
    };

    function isGroupShown(head) {
      return vm.shownGroup === head;
    };

    function onMedicineSuccess(medicineresponse){
          console.log(medicineresponse);
          vm.medicineData = medicineresponse;
        }

        function onMedicineFailure(error){
          console.log(error);
        }
        function getMedicine(){
          var patientId = sessionConstant.currentPatient.id;
          patientsservice.getMedicine(patientId)
            .then(onMedicineSuccess, onMedicineFailure);
        }


    function getOverview() {
      console.log('in overview');
      function onSuccess(response) {
        if (response) {
          console.log(response);
          vm.questions = {};
          for (var i in response) {
            if (!vm.questions[response[i].NAME]) {
              vm.questions[response[i].NAME] = [];
            }
          }
          patientsservice.getAnswers().then(onAnswerSuccess, onAnswerFailure);
        }
      }

      function onAnswerSuccess(result) {
        console.log(result);
        vm.answers = result;

        vm.overview = [{
          "head": "overview",
          "body": {
            "Type of diabetes": vm.currentPatient.diabetesType.displayName,
            "Diagnosed with diabetes since": vm.currentPatient.diabetesDiagnose,
            "Any History of hospitalization for conditions associated with diabetes: severely high blood sugars/diabetic ketoacidosis/hypoglycemia/severe urinary tract infection": vm.currentPatient.hospitalHistory,
            "Other Illness": vm.currentPatient.otherIllness,
            "Are you allergic to any medication": vm.currentPatient.allergy
          }
        }, {
          "head": "Family History",
          "body": {
            "Any Family member with Diabetes": vm.currentPatient.famDiabetes,
            "Any Family member with High Blood Pressure": vm.currentPatient.famBp,
            "Any Family member with High Cholesterol": vm.currentPatient.famChole
          }
        }, {
          "head": "Habits",
          "body": {
            "Tobacco Use": vm.currentPatient.tobaccoUse,
            "Duration": vm.currentPatient.tobaccoDuration,
            "Frequency/Day": vm.currentPatient.tobaccoFreq,
            "Alcohol Use": vm.currentPatient.alcoholUse,
            "Duration1": vm.currentPatient.alcoholDuration,
            "Frequency/Month": vm.currentPatient.alcoholFreq,
            "Quantity": vm.currentPatient.alcoholQty,
            "Occupation/Profession": vm.currentPatient.occupation
          }
        }, {
          "head": "Lifestyle",
          "body": vm.questions
        }];

      }

      function onAnswerFailure(error) {
        console.log(error);
      }

      function onFailure(error) {
        console.log(error);
      }

      var patientId = sessionConstant.currentPatient.id;
      vm.patientOverview = sessionConstant.currentPatient;
      console.log(vm.patientOverview);
      patientsservice.getOverview(patientId)
        .then(onSuccess, onFailure);
      console.log(vm.overview);
    }

    function onThresholdLoadSuccess(resData) {
      console.log(resData);
      vm.thresholdData = resData;
      vm.ShouldShowSpinner = false
    }

    function onThresholdLoadFailure(error) {
      vm.ShouldShowSpinner = false
      console.log(error);
    }

    function loadThreshold(value) {
      console.log(value);
      var dataToSend = sessionConstant.currentPatient.id;
      patientsservice.getThreshold(dataToSend, value)
        .then(onThresholdLoadSuccess, onThresholdLoadFailure);

    }

    function showFilterBar() {
      var filterBarInstance;
      filterBarInstance = $ionicFilterBar.show({
        items: vm.patients,
        update: function (filteredItems, filterText) {
          if (!filterText || filterText.length == 0) {
            $state.go($state.current, {}, {reload: true});
            console.log('no text');
            return;
          }
          else {
            vm.ShowSearch = true;
            vm.patients = filteredItems;
          }
        },
      });
      vm.ShowSearch = false;
    };

    function openRecords() {
      $state.go('records', {
        id: vm.currentPatient.id
      })
    }

    function openThreadShold() {
      $state.go('threshold');
    }

    function listRecords() {
      vm.ShouldShowSpinner = false
      vm.hba1c_List = []
      if (sessionConstant.reportType.hba1c.length > 0) {
        for (var i in sessionConstant.reportType.hba1c) {
          vm.hba1c_List.push({
            'date': sessionConstant.reportType.hba1c[i].DATE_RECORDED.text,
            'value': sessionConstant.reportType.hba1c[i].REPORT_VALUE,
          })
        }
      }


      if (sessionConstant.reportType.fastingsugar.length > 0) {
        vm.fastingsugar_List = []
        for (var i in sessionConstant.reportType.fastingsugar) {
          vm.fastingsugar_List.push({
            'date': sessionConstant.reportType.fastingsugar[i].DATE_RECORDED.text,
            'value': sessionConstant.reportType.fastingsugar[i].REPORT_VALUE,
          })
        }
      }
      if (sessionConstant.reportType.postmeal.length > 0) {
        vm.postmeal_List = []
        for (var i in sessionConstant.reportType.postmeal) {
          vm.postmeal_List.push({
            'date': sessionConstant.reportType.postmeal[i].DATE_RECORDED.text,
            'value': sessionConstant.reportType.postmeal[i].REPORT_VALUE,
          })
        }
      }
      if (sessionConstant.reportType.randomSugar.length > 0) {
        vm.randomSugar_List = []
        for (var i in sessionConstant.reportType.randomSugar) {
          vm.randomSugar_List.push({
            'date': sessionConstant.reportType.randomSugar[i].DATE_RECORDED.text,
            'value': sessionConstant.reportType.randomSugar[i].REPORT_VALUE,
          })
        }
      }

      if (sessionConstant.reportType.BloodPressure.length > 0) {
        vm.BloodPressure_List = []
        for (var i in sessionConstant.reportType.BloodPressure) {
          vm.BloodPressure_List.push({
            'date': sessionConstant.reportType.BloodPressure[i].DATE_RECORDED.text,
            'value': sessionConstant.reportType.BloodPressure[i].REPORT_VALUE,
          })
        }
      }

      if (sessionConstant.reportType.LDL.length > 0) {
        vm.LDL_List = []
        for (var i in sessionConstant.reportType.LDL) {
          vm.LDL_List.push({
            'date': sessionConstant.reportType.LDL[i].DATE_RECORDED.text,
            'value': sessionConstant.reportType.LDL[i].REPORT_VALUE,
          })
        }
      }

      if (sessionConstant.reportType.vldl.length > 0) {
        vm.vldl_List = []
        for (var i in sessionConstant.reportType.vldl) {
          vm.vldl_List.push({
            'date': sessionConstant.reportType.vldl[i].DATE_RECORDED.text,
            'value': sessionConstant.reportType.vldl[i].REPORT_VALUE,
          })
        }
      }

      if (sessionConstant.reportType.Triglycerides.length > 0) {
        vm.Triglycerides_List = []
        for (var i in sessionConstant.reportType.Triglycerides) {
          vm.Triglycerides_List.push({
            'date': sessionConstant.reportType.Triglycerides[i].DATE_RECORDED.text,
            'value': sessionConstant.reportType.Triglycerides[i].REPORT_VALUE,
          })
        }
      }

      if (sessionConstant.reportType.HDL.length > 0) {
        vm.HDL_List = []
        for (var i in sessionConstant.reportType.HDL) {
          vm.HDL_List.push({
            'date': sessionConstant.reportType.HDL[i].DATE_RECORDED.text,
            'value': sessionConstant.reportType.HDL[i].REPORT_VALUE,
          })
        }
      }

      if (sessionConstant.reportType.Cholesterol.length > 0) {
        vm.Cholesterol_List = []
        for (var i in sessionConstant.reportType.Cholesterol) {
          vm.Cholesterol_List.push({
            'date': sessionConstant.reportType.Cholesterol[i].DATE_RECORDED.text,
            'value': sessionConstant.reportType.Cholesterol[i].REPORT_VALUE,
          })
        }
      }

      if (sessionConstant.reportType.Serum.length > 0) {
        vm.Serum_List = []
        for (var i in sessionConstant.reportType.Serum) {
          vm.Serum_List.push({
            'date': sessionConstant.reportType.Serum[i].DATE_RECORDED.text,
            'value': sessionConstant.reportType.Serum[i].REPORT_VALUE,
          })
        }
      }
    }

    function onListSuccess(responseData) {
      console.log(responseData);
      vm.patients = responseData;
      console.log(vm.patients);
      console.log(responseData);
    }

    function onListFailure(errorMessage) {
      console.log(errorMessage);
    }

    function onGetDataSuccess(response) {
      console.log(response);
    }

    function onGetDataFailure(error) {
      console.log(error);
    }

    function getCvdPieData(value) {
      var doctorId = sessionConstant.login.id;
      vm.cCount = []
      patientsservice.getPieData(doctorId, value)
        .then(function (response) {
          vm.cvdCount = response.data;
          for (var i in vm.cvdCount) {
            vm.cCount.push(vm.cvdCount[i].count)
          }
          var chart = {
            type: 'pie',
            options3d: {
              enabled: true,
              alpha: 45,
              beta: 0
            }
          }
          var options = {
            exporting: {
              enabled: false
            }
          }
          var credits = {
            enabled: false
          }
          var title = {
            text: ''
          };
          var plotOptions = {
            pie: {
              allowPointSelect: false,
              cursor: 'pointer',
              dataLabels: {
                enabled: true
              },
              showInLegend: true
            }
          };
          var series = [{
            type: 'pie',
            data: [{
              name: 'High-Risk',
              y: vm.cCount[0]
            }, {
              name: 'Medium-Risk',
              y: vm.cCount[1],
            }, {
              name: 'Low-Risk',
              y: vm.cCount[2]
            }],
            colors: ['#FF5E5B', '#D7B83B', '#2A9DA4']
          }];
          vm.CvdPieData = {};
          vm.CvdPieData.chart = chart;
          vm.CvdPieData.title = title;
          vm.CvdPieData.series = series;
          vm.CvdPieData.plotOptions = plotOptions;
          vm.CvdPieData.options = options
          vm.CvdPieData.credits = credits
        });
    }

    vm.getObesityData = getObesityData;
    function onObesitySuccess(response) {
      console.log(response);
      vm.obesityCount = response.data;
    }

    function onObesityFailure(error) {
      console.log(error);
    }

    function getObesityData(value) {
      console.log('in');
      var doctorId = sessionConstant.login.id;
      patientsservice.getPieData(doctorId, value)
        .then(function (response) {
          vm.ovdCount = response.data;
          vm.oCount = []
          for (var i in vm.ovdCount) {
            vm.oCount.push(vm.ovdCount[i].count)
          }
          var chart = {
            type: 'pie',
            options3d: {
              enabled: true,
              alpha: 45,
              beta: 0
            }
          }
          var options = {
            exporting: {
              enabled: false
            }
          }
          var credits = {
            enabled: false
          }
          var title = {
            text: ''
          };
          var plotOptions = {
            pie: {
              allowPointSelect: false,
              cursor: 'pointer',
              dataLabels: {
                enabled: true
              },
              showInLegend: true
            }
          };
          var series = [{
            type: 'pie',
            data: [{
              name: 'High-Risk',
              y: vm.oCount[0]
            }, {
              name: 'Medium-Risk',
              y: vm.oCount[1],
            }, {
              name: 'Low-Risk',
              y: vm.oCount[2]
            }],
            colors: ['#FF5E5B', '#D7B83B', '#2A9DA4']
          }];
          vm.ObesityData = {};
          vm.ObesityData.chart = chart;
          vm.ObesityData.title = title;
          vm.ObesityData.series = series;
          vm.ObesityData.plotOptions = plotOptions;
          vm.ObesityData.options = options
          vm.ObesityData.credits = credits
        });
    }


    function getNephroPieData(value) {
      var doctorId = sessionConstant.login.id;
      patientsservice.getPieData(doctorId, value)
        .then(function (response) {
          vm.nCount = []
          vm.nephroCount = response.data;
          console.log(vm.nephroCount);

          for (var i in vm.nephroCount) {
            vm.nCount.push(vm.cvdCount[i].count)
          }

          var chart = {
            type: 'pie',
            options3d: {
              enabled: true,
              alpha: 45,
              beta: 0
            }
          }
          var options = {
            exporting: {
              enabled: false
            }
          }
          var credits = {
            enabled: false
          }
          var title = {
            text: ''
          };
          var plotOptions = {
            pie: {
              allowPointSelect: false,
              cursor: 'pointer',
              dataLabels: {
                enabled: true
              },
              showInLegend: true
            }
          };
          var series = [{
            type: 'pie',
            data: [{
              name: 'High-Risk',
              y: vm.nCount[0]
            }, {
              name: 'Medium-Risk',
              y: vm.nCount[1],
            }, {
              name: 'Low-Risk',
              y: vm.nCount[2]
            }],
            colors: ['#FF5E5B', '#D7B83B', '#2A9DA4']
          }];
          vm.nephropieChart = {};
          vm.nephropieChart.chart = chart;
          vm.nephropieChart.title = title;
          vm.nephropieChart.series = series;
          vm.nephropieChart.plotOptions = plotOptions;
          vm.nephropieChart.options = options
          vm.nephropieChart.credits = credits
        });
    }

    function getDiabetesPieData(value) {
      var doctorId = sessionConstant.login.id;
      patientsservice.getPieData(doctorId, value)
        .then(function (response) {
          vm.diabetesCount = response.data;
          vm.dcount = [];
          vm.risk = ['High-Risk', 'Medium-Risk', 'Low-Risk']
          for (var i  in vm.diabetesCount) {
            vm.dcount.push(vm.diabetesCount[i].count)
            // vm.dcount.push(vm.risk[i])
          }
          console.log(vm.dcount)
          var chart = {
            type: 'pie',
            options3d: {
              enabled: true,
              alpha: 45,
              beta: 0
            }
          }
          var options = {
            exporting: {
              enabled: false
            }
          }
          var credits = {
            enabled: false
          }
          var title = {
            text: ''
          };
          var plotOptions = {
            pie: {
              allowPointSelect: false,
              cursor: 'pointer',
              dataLabels: {
                enabled: true
              },
              showInLegend: true
            }
          };
          var series = [{
            type: 'pie',
            data: [{
              name: 'High-Risk',
              y: vm.dcount[0]
            }, {
              name: 'Medium-Risk',
              y: vm.dcount[1],
            }, {
              name: 'Low-Risk',
              y: vm.dcount[2]
            }],
            colors: ['#FF5E5B', '#D7B83B', '#2A9DA4']
          }];
          vm.pieChart = {};
          vm.pieChart.chart = chart;
          vm.pieChart.title = title;
          vm.pieChart.series = series;
          vm.pieChart.plotOptions = plotOptions;
          vm.pieChart.options = options
          vm.pieChart.credits = credits

        });
    }

    function LoadPatientsList() {
      if ($cordovaNetwork.isOnline()) {
        if (sessionConstant.list.length > 0) {
          console.log(sessionConstant.list.length)
          getPieData('diabetes');
          getPieData('cvd');
          getPieData('nephro');
        } else {
          vm.spinnerStatus++;
          patientsservice.getPatients().then(onListSuccess, onListFailure);
        }
      }
      else {
      }
    }

    function doRefresh() {
      if ($cordovaNetwork.isOnline()) {
        patientsservice.getPatients().then(onListSuccess, onListFailure);
      }
      else {
        vm.ShouldShowSpinner = false;
        commonService.ionicToaster("Please Check Your Internet Connection")
      }
    }

    function ShouldShowFlag(record) {
      if (record && record > 0) {
        return true;
      } else {
        return false;
      }
    }

    function goToPatientDetails(patient) {
      sessionConstant.currentPatient = patient;
      vm.currentPatient = sessionConstant.currentPatient;
      console.log(vm.currentPatient);
      $state.go('info');
    }

    function clear() {
      vm.patients = ""
    }

    function searchPatient() {
      if (vm.patient != undefined) {
        sessionConstant.currentPatient = vm.patient;
        vm.currentPatient = sessionConstant.currentPatient
        if (parseInt(sessionConstant.currentPatient.report_flag) >= 0 && parseInt(sessionConstant.currentPatient.report_flag) <= 3) {
          sessionConstant.currentPatient.riskLevel = 'Low Risk';
          vm.patient = ''
        } else if (parseInt(sessionConstant.currentPatient.report_flag) >= 7) {
          sessionConstant.currentPatient.riskLevel = 'High Risk';
          vm.patient = ''
        }
        else {
          sessionConstant.currentPatient.riskLevel = 'Medium Risk';
          vm.patient = ''
        }
        $state.go('info');
      }
    }

    function goBackToList() {
      $state.go('patients');
    }

    function onGetRecordSuccess(data) {
      var responseData = data;
      console.log(sessionConstant.PatientRecords)
      var responseData = data.data;
      console.log(responseData)
      var reportType = {
        LDL: [],
        fastingsugar: [],
        postmeal: [],
        randomSugar: [],
      }
      var Triglycerides = {
        dates: [],
        value: [],
        thread: [],
      }
      var Cholesterol = {
        dates: [],
        value: [],
        thread: [],
      }
      var HDL = {
        dates: [],
        value: [],
        thread: [],
      }
      var Serum = {
        dates: [],
        value: [],
        thread: [],
      }
      var hba1 = {
        dates: [],
        value: [],
        thread: [],
      }
      var fastingSugar = {
        dates: [],
        value: [],
        thread: [],
      }
      var vldl = {
        dates: [],
        value: [],
        thread: [],
      }
      var hba1c = {
        dates: [],
        value: [],
        thread: [],
      };
      var fastingsugar = {
        dates: [],
        value: [],
        thread: [],
      };
      var postmeal = {
        dates: [],
        value: [],
        thread: [],
      };
      var randomSugar = {
        dates: [],
        value: [],
        thread: [],
      };
      var GFR = {
        dates: [],
        value: [],
        thread: [],
      };
      var UrineAlbumin = {
        dates: [],
        value: [],
        thread: [],
      };
      var BloodUrea = {
        dates: [],
        value: [],
        thread: [],
      }
      var BloodPressure = {
        dates: [],
        value: [],
        thread: [],
      }
      var LDL = {
        dates: [],
        value: [],
        thread: [],
      }

      var reportType = {
        LDL: [],
        vldl: [],
        Triglycerides: [],
        HDL: [],
        Cholesterol: [],
        Serum: [],
        hba1c: [],
        fastingsugar: [],
        postmeal: [],
        randomSugar: [],
        GFR: [],
        UrineAlbumin: [],
        BloodUrea: [],
        BloodPressure: [],
      }


      for (var i in responseData) {
        console.log(responseData[i].REPORT_TYPE_ID)
        if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000001") {
          reportType.hba1c.push(responseData[i]);
        } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000002") {
          reportType.fastingsugar.push(responseData[i])
        } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000003") {
          reportType.postmeal.push(responseData[i])
        } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000004") {
          reportType.randomSugar.push(responseData[i])
        }
        else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000005") {
          console.log("Blood-Preaseure----USRLVRT000000000000000000000000000005")
          reportType.BloodPressure.push(responseData[i])
        } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000006") {
          console.log("USRLVRT000000000000000000000000000006----autocalc---nograh")
        } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000007") {
          reportType.LDL.push(responseData[i])
          console.log("USRLVRT000000000000000000000000000006")
        } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000008") {
          console.log("VLDL------------USRLVRT000000000000000000000000000008")
          reportType.vldl.push(responseData[i])
        } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000009") {
          reportType.Triglycerides.push(responseData[i])
        } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000010") {
          console.log("HDL---USRLVRT000000000000000000000000000010")
          reportType.HDL.push(responseData[i])
        } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000011") {
          console.log("Cholesterol Ratio---USRLVRT000000000000000000000000000011")
          reportType.Cholesterol.push(responseData[i])
        } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000012") {
          reportType.Serum.push(responseData[i])
          console.log("USRLVRT000000000000000000000000000006---nepro")
        } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000013") {
          reportType.BloodUrea.push(responseData[i])
          console.log("USRLVRT000000000000000000000000000006----------nepro")
        } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000014---nepro") {
          reportType.UrineAlbumin.push(responseData[i])
        } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000015----nerp---nograph-autocalc") {
          reportType.GFR.push(responseData[i])
        }
      }
      sessionConstant.reportType.hba1c = reportType.hba1c
      sessionConstant.reportType.fastingsugar = reportType.fastingsugar
      sessionConstant.reportType.postmeal = reportType.postmeal
      sessionConstant.reportType.GFR = reportType.GFR
      sessionConstant.reportType.UrineAlbumin = reportType.UrineAlbumin
      sessionConstant.reportType.BloodUrea = reportType.BloodUrea
      sessionConstant.reportType.Serum = reportType.Serum
      sessionConstant.reportType.Cholesterol = reportType.Cholesterol
      sessionConstant.reportType.HDL = reportType.HDL
      sessionConstant.reportType.Triglycerides = reportType.Triglycerides
      sessionConstant.reportType.vldl = reportType.vldl
      sessionConstant.reportType.LDL = reportType.LDL
      sessionConstant.reportType.BloodPressure = reportType.BloodPressure
      sessionConstant.reportType.randomSugar = reportType.randomSugar


      for (var j in reportType.hba1c) {
        hba1.dates.push(reportType.hba1c[j].DATE_RECORDED.text)
        hba1.value.push(parseInt(reportType.hba1c[j].REPORT_VALUE))
        hba1.thread.push(reportType.hba1c[j].THRESHOLD_VALUE)
      }

      for (var j in reportType.fastingsugar) {
        fastingSugar.dates.push(reportType.fastingsugar[j].DATE_RECORDED.text)
        fastingSugar.value.push(parseInt(reportType.fastingsugar[j].REPORT_VALUE))
        fastingSugar.thread.push(reportType.fastingsugar[j].THRESHOLD_VALUE)
      }

      for (var j in reportType.randomSugar) {
        randomSugar.dates.push(reportType.randomSugar[j].DATE_RECORDED.text)
        randomSugar.value.push(parseInt(reportType.randomSugar[j].REPORT_VALUE))
        randomSugar.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
      }

      for (var j in reportType.BloodPressure) {
        BloodPressure.dates.push(reportType.randomSugar[j].DATE_RECORDED.text)
        BloodPressure.value.push(parseInt(reportType.randomSugar[j].REPORT_VALUE))
        BloodPressure.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
      }

      for (var j in reportType.BloodUrea) {
        BloodUrea.dates.push(reportType.randomSugar[j].DATE_RECORDED.text)
        BloodUrea.value.push(parseInt(reportType.randomSugar[j].REPORT_VALUE))
        BloodUrea.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
      }

      for (var j in reportType.Cholesterol) {
        Cholesterol.dates.push(reportType.randomSugar[j].DATE_RECORDED.text)
        Cholesterol.value.push(parseInt(reportType.randomSugar[j].REPORT_VALUE))
        Cholesterol.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
      }

      for (var j in reportType.GFR) {
        GFR.dates.push(reportType.randomSugar[j].DATE_RECORDED.text)
        GFR.value.push(parseInt(reportType.randomSugar[j].REPORT_VALUE))
        GFR.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
      }

      for (var j in reportType.HDL) {
        HDL.dates.push(reportType.randomSugar[j].DATE_RECORDED.text)
        HDL.value.push(parseInt(reportType.randomSugar[j].REPORT_VALUE))
        HDL.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
      }

      for (var j in reportType.LDL) {
        LDL.dates.push(reportType.randomSugar[j].DATE_RECORDED.text)
        LDL.value.push(parseInt(reportType.randomSugar[j].REPORT_VALUE))
        LDL.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
      }

      for (var j in reportType.postmeal) {
        postmeal.dates.push(reportType.postmeal[j].DATE_RECORDED.text)
        postmeal.value.push(parseInt(reportType.postmeal[j].REPORT_VALUE))
        postmeal.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
      }
      for (var j in reportType.Serum) {
        Serum.dates.push(reportType.randomSugar[j].DATE_RECORDED.text)
        Serum.value.push(parseInt(reportType.randomSugar[j].REPORT_VALUE))
        Serum.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
      }
      for (var j in reportType.Triglycerides) {
        Triglycerides.dates.push(reportType.randomSugar[j].DATE_RECORDED.text)
        Triglycerides.value.push(parseInt(reportType.randomSugar[j].REPORT_VALUE))
        Triglycerides.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
      }
      for (var j in reportType.UrineAlbumin) {
        UrineAlbumin.dates.push(reportType.randomSugar[j].DATE_RECORDED.text)
        UrineAlbumin.value.push(parseInt(reportType.randomSugar[j].REPORT_VALUE))
        UrineAlbumin.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
      }
      for (var j in reportType.vldl) {
        vldl.dates.push(reportType.randomSugar[j].DATE_RECORDED.text)
        vldl.value.push(parseInt(reportType.randomSugar[j].REPORT_VALUE))
        vldl.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
      }
      for (var j in reportType.postmeal) {
        postmeal.dates.push(reportType.postmeal[j].DATE_RECORDED.text)
        postmeal.value.push(parseInt(reportType.postmeal[j].REPORT_VALUE))
        postmeal.thread.push(reportType.postmeal[j].THRESHOLD_VALUE)
      }
      vm.hba1c = {
        series: [{
          type: "line",
          name: "Thread-Should",
          data: hba1.thread
        }, {
          type: "area",
          name: "Values",
          data: hba1.value
        }],
        title: {
          text: 'HBA1'
        },
        xAxis: {
          categories: hba1.dates
        },
        options: {
          exporting: {
            enabled: false
          }
        },
        credits: {
          enabled: false
        },
      }

      vm.FastingSugar = {
        series: [{
          type: "line",
          name: "Thread-Should",
          data: fastingSugar.thread
        }, {
          type: "area",
          name: "Values",
          data: fastingSugar.value
        }],
        title: {
          text: 'Fasting Sugar'
        },
        xAxis: {
          categories: hba1.dates
        },
        options: {
          exporting: {
            enabled: false
          }
        },
        credits: {
          enabled: false
        },
      }

      vm.PostMeal = {
        series: [{
          type: "line",
          name: "Thread-Should",
          data: postmeal.thread
        }, {
          type: "area",
          name: "Values",
          data: postmeal.value
        }],
        title: {
          text: 'Post Meal'
        },
        xAxis: {
          categories: hba1.dates
        },
        options: {
          exporting: {
            enabled: false
          }
        },
        credits: {
          enabled: false
        },
      }

      vm.RandomSugar = {
        series: [{
          type: "line",
          name: "Thread-Should",
          data: randomSugar.thread
        }, {
          type: "area",
          name: "Values",
          data: randomSugar.value
        }],
        title: {
          text: 'Random Sugar'
        },
        xAxis: {
          categories: hba1.dates
        },
        options: {
          exporting: {
            enabled: false
          }
        },
        credits: {
          enabled: false
        },
      }

      vm.bloodPreasure = {
        series: [{
          type: "line",
          name: "Thread-Should",
          data: randomSugar.thread
        }, {
          type: "area",
          name: "Values",
          data: randomSugar.value
        }],
        title: {
          text: 'bloodPreasure'
        },
        xAxis: {
          categories: hba1.dates
        },
        options: {
          exporting: {
            enabled: false
          }
        },
        credits: {
          enabled: false
        },
      }

      vm.VLDL = {
        series: [{
          type: "line",
          name: "Thread-Should",
          data: randomSugar.thread
        }, {
          type: "area",
          name: "Values",
          data: randomSugar.value
        }],
        title: {
          text: 'VLDL'
        },
        xAxis: {
          categories: hba1.dates
        },
        options: {
          exporting: {
            enabled: false
          }
        },
        credits: {
          enabled: false
        },
      }

      vm.Triglycerides = {
        series: [{
          type: "line",
          name: "Thread-Should",
          data: randomSugar.thread
        }, {
          type: "area",
          name: "Values",
          data: randomSugar.value
        }],
        title: {
          text: 'Triglycerides'
        },
        xAxis: {
          categories: hba1.dates
        },
        options: {
          exporting: {
            enabled: false
          }
        },
        credits: {
          enabled: false
        },
      }

      vm.HDL = {
        series: [{
          type: "line",
          name: "Thread-Should",
          data: randomSugar.thread
        }, {
          type: "area",
          name: "Values",
          data: randomSugar.value
        }],
        title: {
          text: 'HDL'
        },
        xAxis: {
          categories: hba1.dates
        },
        options: {
          exporting: {
            enabled: false
          }
        },
        credits: {
          enabled: false
        },
      }

      vm.Cholesterol = {
        series: [{
          type: "line",
          name: "Thread-Should",
          data: randomSugar.thread
        }, {
          type: "area",
          name: "Values",
          data: randomSugar.value
        }],
        title: {
          text: 'Cholesterol'
        },
        xAxis: {
          categories: hba1.dates
        },
        options: {
          exporting: {
            enabled: false
          }
        },
        credits: {
          enabled: false
        },
      }

      vm.Serum = {
        series: [{
          type: "line",
          name: "Thread-Should",
          data: randomSugar.thread
        }, {
          type: "area",
          name: "Values",
          data: randomSugar.value
        }],
        title: {
          text: 'Serum'
        },
        xAxis: {
          categories: hba1.dates
        },
        options: {
          exporting: {
            enabled: false
          }
        },
        credits: {
          enabled: false
        },
      }
    }

    function onGetRecordFailure(error) {
      $scope.$broadcast('scroll.refreshComplete');
      vm.ShouldShowSpinner = false;
      console.log(error);
    }

    function doRefreshEnterRecords() {
      if ($cordovaNetwork.isOnline()) {
        var patientId = $state.params.id;
        patientsservice.getPatientRecords(patientId)
          .then(onGetRecordSuccess, onGetRecordFailure);
      } else {
        vm.ShouldShowSpinner = false
        commonService.ionicToaster("Please Check Your Internet Connection")
      }
    }

    vm.hideshowDiabetes = false
    vm.hideshownepro = false
    vm.hideshowcvd = false
    vm.hideshownepro = false
    function loadPatientRecords(value) {
      if (value == "diabetes") {
        vm.hideshowDiabetes = true
        vm.hideshownepro = false
        vm.hideshowcvd = false
        vm.hideshownepro = false
      } else if (value == "cvd") {
        vm.hideshowDiabetes = false
        vm.hideshownepro = false
        vm.hideshowcvd = true
        vm.hideshownepro = false
      } else if (value == "nephro") {
        vm.hideshowDiabetes = false
        vm.hideshownepro = false
        vm.hideshowcvd = true
        vm.hideshownepro = false
      } else if (value == "other") {
        vm.hideshowDiabetes = false
        vm.hideshownepro = false
        vm.hideshowcvd = false
        vm.hideshownepro = true
      }

      var patientId = $state.params.id;
      console.log(sessionConstant.PatientRecords);
      if ($cordovaNetwork.isOnline()) {
        console.log("First Time Request")
        patientsservice.getPatientRecords(patientId, value)
          .then(onGetRecordSuccess, onGetRecordFailure);
      } else {
        vm.ShouldShowSpinner = false
        commonService.ionicToaster("Please Check Your Internet Connection")
      }
    }

    $ionicModal.fromTemplateUrl('templates/patients/list.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.modal = modal;
    });
    $scope.closeModal = function () {
      $scope.modal.hide();
    };
    $scope.openModal = function (value) {
      console.log(value);
      $scope.modal.show();
      vm.listRecords()
      vm.ShouldShowSpinner = false
      if (value == 0) {
        sessionConstant.showHide = [true, false, false, false, false, false, false, false, false, false, false, false, false]
        vm.showHide = sessionConstant.showHide
      } else if (value == 1) {
        sessionConstant.showHide = [false, true, false, false, false, false, false, false, false, false, false, false, false]
        vm.showHide = sessionConstant.showHide
      } else if (value == 2) {
        sessionConstant.showHide = [false, false, true, false, false, false, false, false, false, false, false, false, false]
        vm.showHide = sessionConstant.showHide
      } else if (value == 3) {
        sessionConstant.showHide = [false, false, false, true, false, false, false, false, false, false, false, false, false]
        vm.showHide = sessionConstant.showHide
      } else if (value == 4) {
        sessionConstant.showHide = [false, false, false, false, true, false, false, false, false, false, false, false, false]
        vm.showHide = sessionConstant.showHide
      } else if (value == 5) {
        sessionConstant.showHide = [false, false, false, false, false, true, false, false, false, false, false, false, false]
        vm.showHide = sessionConstant.showHide
      } else if (value == 6) {
        sessionConstant.showHide = [false, false, false, false, false, false, true, false, false, false, false, false, false]
        vm.showHide = sessionConstant.showHide
      } else if (value == 7) {
        sessionConstant.showHide = [false, false, false, false, false, false, false, true, false, false, false, false, false]
        vm.showHide = sessionConstant.showHide
      } else if (value == 8) {
        sessionConstant.showHide = [false, false, false, false, false, false, false, false, true, false, false, false, false]
        vm.showHide = sessionConstant.showHide
      } else if (value == 9) {
        sessionConstant.showHide = [false, false, false, false, false, false, false, false, false, true, false, false, false]
        vm.showHide = sessionConstant.showHide
      } else if (value == 10) {
        sessionConstant.showHide = [false, false, false, false, false, false, false, false, false, false, true, false, false]
        vm.showHide = sessionConstant.showHide
      } else if (value == 11) {
        sessionConstant.showHide = [false, false, false, false, false, false, false, false, false, false, false, true, false]
        vm.showHide = sessionConstant.showHide
      } else if (value == 12) {
        sessionConstant.showHide = [false, false, false, false, false, false, false, false, false, false, false, false, true]
        vm.showHide = sessionConstant.showHide
      }
    }

    function getThreshold() {
      function onThresholdLoadSuccess(resData) {
        console.log(resData);

        vm.thresholdData = resData;
        vm.ShouldShowSpinner = false
      }

      function onThresholdLoadFailure(error) {
        vm.ShouldShowSpinner = false
        console.log(error);
      }

      var dataToSend = sessionConstant.currentPatient.id;
      patientsservice.getThresholData(dataToSend)
        .then(onThresholdLoadSuccess, onThresholdLoadFailure);

    }

    function updateThreshold(form, formData) {
      var onThresholdUpdateSuccess = function (updateresponse) {
        console.log(updateresponse);
        var patient = sessionConstant.currentPatient;
        vm.readOnly = true;
        $state.go('info');
      };
      var onThresholdUpdateFailure = function (errorMessage) {
        logger.feedback(errorMessage, errorMessage, 'Signin');
      };
      console.log(formData);
      var patientId = sessionConstant.currentPatient.id;
      patientsservice.updateThresholdData(formData, patientId).then(onThresholdUpdateSuccess, onThresholdUpdateFailure);
    }
  }
})
()
