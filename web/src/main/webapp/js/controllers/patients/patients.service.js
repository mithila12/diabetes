(function () {
  'use strict';
  angular
    .module('app.patients')
    .factory('patientsservice', patientsservice);

  function patientsservice($http, urlservice, appUrls, sessionConstant, $q) {
    var service = {
      getPatients: getPatients,
      getPatientRecords: getPatientRecords,
      // getThresholData: getThresholData,
      getThreshold: getThreshold,
      getAnswers: getAnswers,
      getOverview: getOverview,
      updateThresholdData: updateThresholdData,
      patientNotification:patientNotification,
      getPieData: getPieData
    };
    return service;

    function onGetPatientsSuccess(response) {
      console.log(response);
      console.log(response.data.result);
      /*console.log(response.data)
       sessionConstant.list = response.data.data;
       console.log(sessionConstant.list)*/
      return response.data.result;
    }

    function onGetPatientsFailure(error) {
      return error.data;
    }

    function getPieData(doctorId, value) {
      if (value == 'diabetes') {
        var prefix = "graph_report_count";
        var params = "DOCTOR_ID~" + doctorId + "^REPORT_TYPE~sugar_profile_report_type";
      }
      else if (value == 'cvd') {
        var prefix = "graph_report_count";
        var params = "DOCTOR_ID~" + doctorId + "^REPORT_TYPE~cvd_report_type";
      }
      else if (value == 'nephro') {
        var prefix = "graph_report_count";
        var params = "DOCTOR_ID~" + doctorId + "^REPORT_TYPE~nephro_report_type";
      }
      else if (value == 'obesity') {
        var prefix = "obesity_graph_count";
        var params = "DOC_ID~" + doctorId;
      }
      console.log(params);

      return $http.get(urlservice.selectUrl({searchParams: params, mode: 'simple', context: 'sysadmin'}, prefix),
        {})
        .then(function (response) {
          if (response) {
            console.log(response)
            return response;
          }
        }, function (error) {
          throw response.data.message;

        })
    }

    function getThreshold(dataToSend, value) {
      if (value == 'diabetes') {
        var params = "PATIENT_ID~" + dataToSend + "^REPORT_TYPE~sugar_profile_report_type";
      }
      else if (value == 'cvd') {
        var params = "PATIENT_ID~" + dataToSend + "^REPORT_TYPE~cvd_report_type";
      }
      else {
        var params = "PATIENT_ID~" + dataToSend + "^REPORT_TYPE~nephro_report_type";
      }
      console.log(params);
      var prefix = "patients_threshold";
      return $http.get(urlservice.selectUrl({searchParams: params, mode: 'simple', context: 'sysadmin'}, prefix),
        {})
        .then(onSuccess, onError);
    }

    function getPatients() {
      var params = "DOCTOR_ID~" + sessionConstant.login.id;
      var prefix = "patnt";
      console.log(params);
      return $http.get(urlservice.fetchUrl({searchParams: params, mode: 'simple', context: 'sysadmin'}, prefix),
        {})
        .then(onGetPatientsSuccess, onGetPatientsFailure);
    }

    function getPatientRecords(patientId, value) {
      if (value == 'diabetes') {
        var params = "PATIENT_ID~" + patientId + "^REPORT_TYPE~sugar_profile_report_type";
      }
      else if (value == 'cvd') {
        var params = "PATIENT_ID~" + patientId + "^REPORT_TYPE~cvd_report_type";
      }
      else if (value == 'nephro') {
        var params = "PATIENT_ID~" + patientId + "^REPORT_TYPE~nephro_report_type";
      }
      else {
        var params = "PATIENT_ID~" + patientId + "^REPORT_TYPE~nephro_report_type";
      }
      var prefix = "patients_reports";
      return $http.get(urlservice.selectUrl({searchParams: params, mode: 'simple', context: 'sysadmin'}, prefix),
        {})
        .then(function (response) {
          if (response) {
            console.log(response)
            /*sessionConstant.PatientRecords = response.data.data;
             sessionConstant.PatientRecords['PatientId'] = patientId
             console.log(sessionConstant.PatientRecords)
             console.log(response.data)*/
            //return sessionConstant.PatientRecords;
            return response
          }
        }, function (error) {
          throw response.data.message;

        })
    }

    function onSuccess(response) {
      if (response) {
        //sessionConstant.login.cert = loginresponse.data.reference_id;
        console.log(response);
        return response.data;
      } else {
        throw response.data.message;
      }
    };

    function onError(error) {
      throw error.data;
    }

    /*function getThresholData(dataToSend) {
     var params = "PATIENT_ID~" +dataToSend;
     var prefix = "thrsh";
     console.log(params);
     return $http.get(urlservice.fetchUrl({searchParams:params,mode:'simple',context:'sysadmin'},prefix),
     {})
     .then(onSuccess, onError);
     }*/

    function onUpdateSuccess(updateresponse) {
      if (updateresponse) {
        //sessionConstant.login.cert = loginresponse.data.reference_id;
        console.log(updateresponse);
        return updateresponse;
      } else {
        throw updateresponse.data.message;
      }
    };

    function onUpdateError(error) {
      throw error.data;
    }

    function updateThresholdData(thresholdData, patientId) {
      var def = $q.defer();
      var requests = [];
      for (var i in thresholdData) {
        console.log(thresholdData[i]);
        requests.push($http.put(
          urlservice.formUrl(appUrls.threshold.setThreshold),
          {
            id: thresholdData[i].ID,
            patient: {
              id: patientId
            },
            metric: {
              id: thresholdData[i].METRIC_ID
            },
            value: thresholdData[i].VALUE
          }));
      }
      return $q.all(requests).then(onUpdateSuccess, onUpdateError);


    }

    function getAnswers() {
      var params = "TYPE~_answers";
      var prefix = "answer"
      return $http.get(
        urlservice.selectUrl({searchParams: params, mode: 'simple', context: 'sysadmin'}, prefix),
        {})
        .then(function (result) {
          console.log(result);
          return result.data;
        })
    }

    function getOverview(patientId) {
      //vm.patientOverview = sessionConstant.currentPatient;
      // console.log(vm.patientOverview);
      var params = "PATIENT_ID~" + patientId;
      var prefix = "question"
      return $http.get(
        urlservice.selectUrl({searchParams: params, mode: 'simple', context: 'sysadmin'}, prefix),
        {})
        .then(function (response) {
            console.log(response);
            return response.data;
          }
        )
    }

    /*function patientNotification() {
      var params = "DOCTOR_ID~" +sessionConstant.login.id;
      var prefix = "patients_reg_count";
      console.log(params);
      return $http.get(urlservice.selectUrl({searchParams:params,context:'sysadmin'},prefix),
        {}).then(function (response) {
        if (response) {
          return response.data[0];
        }
      })
    }*/

    function patientNotification() {
      var params = "DOCTOR_ID~" +sessionConstant.login.id;
      var prefix = "patients_reg_count";
      console.log(params);
      return $http.get(urlservice.selectUrl({searchParams:params,context:'sysadmin'},prefix),
        {}).then(function (response) {
        if (response) {
          return response.data[0];
        }
      })
    }
  }
})();
