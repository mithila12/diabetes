sfe.controller('PatientController', function ($scope, $http,$state, $stateParams, $modal,
                                              RepositoryService, LabelService, LocalStore) {

    $scope.links = LabelService.getLabels('links');
    $scope.labels = LabelService.getLabels('patient');
    $scope.master = {};
    $scope.frequencyUnits=[];
    $scope.patient = {"status":{"id":"SYSLVDOCSTA00000000000000000000000003"}, address:{country:'India'}};
    $scope.controls = {};
    $scope.reason={};
    $scope.reasons=[];
    $scope.patreason={};
    $scope.showreason = false;
    $scope.clinic={};
    $scope.address={};
    $scope.clinicloc=null;
    $scope.patient.clinic={};
    $scope.patient.consultingDoctor={};
    $scope.enablePurchaseHistory=false;
    $scope.flag=false;
    $scope.currentPage = 0;
    $scope.doctorCode=null;
    $scope.currentDate = Date.parse(new Date());

    $scope.init = function() {
        console.log(LocalStore.fetch('headerdata'));
        RepositoryService.find("docdp",{locationId: LocalStore.fetch('headerdata').location.id, paged:'N'},'advanced').success(function(data){
            $scope.master.doctors = data;
            console.log(data);
        });

        if($stateParams.id!=null){
            $scope.enablePurchaseHistory=true;
            RepositoryService.restore($stateParams.id).success(function(patient){
                $scope.patient = patient;
                if($scope.patient.status.id=='SYSLVDOCSTA00000000000000000000000002' || $scope.patient.status.id =='SYSLVDOCSTA00000000000000000000000004')
                {
                    $scope.showreason=true;
                }
                $scope.getClinicInfo();
            });
            RepositoryService.find("patir",{"patientId":$stateParams.id},'simple').then(function(res){
                if(res.data[0]!=null)
                {
                    $scope.reason = res.data[0].reason;
                }
            });
        }

        RepositoryService.find("state",{},'simple').then(function(states){
            $scope.states = states.data;
        });

        RepositoryService.find("syslv",{"type":'PATIENT_INACTIVATION_REASON'},'simple').then(function(data){
            $scope.reasons = data.data;
        });

        $scope.usercontext = LocalStore.fetch('topcontext');
        if($scope.usercontext=='home')
        {
            var loggedInUserId = LocalStore.fetch('headerdata').id;
            RepositoryService.restore(loggedInUserId).then(function(data){
                RepositoryService.restore(data.data.companydetails[0].location.id).then(function(loc){
                    $scope.location = {"id":loc.data.id,"displayName":loc.data.name};
                    $scope.division = {"id":loc.data.division.id,"displayName":loc.data.division.displayName};
                    $scope.patient.belongsTo={"id":loc.data.id,"displayName":loc.data.name};
                    $scope.division = {"id":loc.data.division.id,"displayName":loc.data.division.displayName};
                });
            });
        }
    };

    $scope.chkvisibility = function(){
        if($scope.patient.status.id=='SYSLVDOCSTA00000000000000000000000002' || $scope.patient.status.id =='SYSLVDOCSTA00000000000000000000000004')
        {
            $scope.showreason=true;
        }
        else
        {
            $scope.showreason=false;
        }
    };

    $scope.getClinicInfo = function() {

        RepositoryService.restore($scope.patient.consultingDoctor.id).success(function(data){
            $scope.doctorCode = data.code;
        });

        RepositoryService.find("clinc",{"doctor_id":$scope.patient.consultingDoctor.id},'simple').then(function(data){
            $scope.clinic = data.data[0];
            RepositoryService.find("addrs",{"ownerId":$scope.clinic.id},'simple').then(function(data){
                $scope.address = data.data[0];
                console.log($scope.address);
                $scope.clinicloc=$scope.address.city.displayName;
            });
            $scope.patient.clinic["id"]=$scope.clinic.id;
            $scope.patient.clinic["displayName"] = $scope.clinic.name;
        });
    }

    $scope.save = function(){
            var flag = false;
        RepositoryService.save($scope.patient, 'PATNT').success(function(data){
            $scope.enablePurchaseHistory=true;
            $scope.controls.message = 'Added Successfully.';
            $scope.controls.showAlert = true;

            if($scope.reason.id!=null)
            {
                $scope.patreason["reason"]=$scope.reason;
                $scope.patreason["patient"] = {"id":$scope.patient.id};
                 $http({
                       method: 'POST',

                       url: restUrl + "/sfe/patientapproval/savePatientInactivationReason",
                       data: $scope.patreason,
                       headers: {
                           "content-type": "application/json",
                           "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                       }
                   }).success(function (data) {
                           console.log(data);
                   });/*
                RepositoryService.save($scope.patreason, 'PATIR').success(function(data){
                    console.log("Inactivated Successfully.");
                });*/
            }

            RepositoryService.restore(data.id).success(function(patient){
                $scope.patient = patient;
                if($stateParams.id==null){
                    $scope.newHistory();
                }
            });
        });
    };


    $scope.newHistory = function(id){
        if(id!=null)
        {
            $scope.flag=true;
        }
        else
        {
            $scope.flag=false;
        }
        var modalInstance = $modal.open({
            templateUrl: 'patientpurchase.html',
            controller: 'PatientPurchaseController',
            size: 'lg',
            backdrop: true,
            backdropClick: true,
            dialogFade: true,
            keyboard: false,
            resolve: {
                detail: function(){
                    return $scope.patient.id;
                },
                history: function(){
                    return id;
                },
                flag: function(){
                    return $scope.flag;
                }
            }
        });
    };


    $scope.showHistory = function(){

        if($scope.patient.id!=null)
        {
            $scope.showtable = true;
            RepositoryService.search('adhoc_skupd',{"ownerId":$scope.patient.id,paged:'Y', page:1}, 'simple', 'home',null).then(function(data){
                if(data.data.totalRecords==0) {
                    $scope.nodata = true;
                    $scope.showtable = false;
                }else{
                    $scope.nodata = false;
                }
                $scope.searchData = data.data.result;
                $scope.columns = data.data.columns;
                $scope.actions = data.data.actions;
                $scope.totalItems = data.data.totalRecords;
                $scope.currentPage = data.data.currentPage;
            });
        }
    }

    $scope.pageChanged = function(){
        RepositoryService.search('skupd',{"ownerId":$scope.patient.id,paged:'Y', page:$scope.currentPage}, 'simple', 'home',null).then(function(response){
            $scope.searchData = response.data.result;
            $scope.columns = response.data.columns;
            $scope.actions = response.data.actions;
            $scope.totalItems = response.data.totalRecords;
            $scope.currentPage = response.data.currentPage;
        });
    }

    $scope.changeStatus=function(id,statusId){
        var cnf = true;
        if(statusId=="SYSLVDOCSTA00000000000000000000000002")
        {
            cnf = confirm("Deactivation will not allow you to add further purchases for this SKU. Are you sure you want to deactivate?");
        }
        if(cnf){
            RepositoryService.restore(id).success(function(data){
                var ptbrand = data;
                ptbrand.status={"id":statusId};
                RepositoryService.save(ptbrand,'SKUPD').success(function(data){
                    $scope.showHistory();
                });
            });
        }
    }
});

sfe.controller('PatientApprovalController', function ($scope, $http,$state, $stateParams, $modal,SearchFormService,
                                                      RepositoryService, PatientApprovalService, LabelService, LocalStore) {

    $scope.links = LabelService.getLabels('links');
    $scope.labels = LabelService.getLabels('patient');
    $scope.stdFormData = {"approvarId":LocalStore.fetch('USER_INFO').id};
    $scope.flag = false;
    $scope.initSearch = function() {
        $scope.stdFormTemplate = SearchFormService.getForm($stateParams.context, 'patnt_approval_select', 'patient');
        $scope.search();
    };

    $scope.search=function(){

        RepositoryService.search("patnt_approval",$scope.stdFormData, $stateParams.context,'home').then(function(data){
            if(data.data.totalRecords==0) {
                $scope.nodata = true;
                $scope.showtable = false;
            }else{
                $scope.nodata = false;
                $scope.showtable = true;
            }
            $scope.searchData = data.data.result;
            $scope.columns = data.data.columns;
            $scope.actions = data.data.actions;
            $scope.totalItems = data.data.totalRecords;
            $scope.currentPage = data.data.currentPage;
        });
    };

    $scope.approve = function(id){
        var cnf = confirm("Are you sure you want to approve this Patient?");
        if(cnf) {
            PatientApprovalService.submitapproval(id, 'approved').then(function(data){
                $scope.search();
                alert("Successfully Approved.");
            });
        }
    };

    $scope.reject = function(id){
        var cnf = confirm("Are you sure you want to reject this Patient?");
        if(cnf) {
            PatientApprovalService.submitapproval(id, 'rejected').then(function(data){
                $scope.search();
                alert("Rejected!");
            });
        }
    };

});

sfe.controller('HoCounterReportController', function ($scope, $http,$state, $stateParams, $modal,SearchFormService,
                                                      RepositoryService,months,monthNameToIndexJS, LabelService, LocalStore) {

    $scope.links = LabelService.getLabels('links');
    $scope.labels = LabelService.getLabels('counter');
    $scope.divisions=[];
    $scope.locations=[];
    $scope.doctors=[];
    $scope.brands=[];
    $scope.products=[];
    $scope.statusList=[];
    $scope.months = months;
    var year = new Date().getFullYear();
    $scope.years = [year-1, year, year+1];
    $scope.filter={};

    $scope.init=function(){
        RepositoryService.find("divsn",{},'advanced').success(function(data){
            $scope.divisions=data;
        });

        RepositoryService.find("syslv",{type:"DOCTOR_STATUS1",paged:'N'},'advanced').success(function(data){
            $scope.statusList=data;
        });

    }

    $scope.getLocations=function(){
        RepositoryService.find("locat",{division_id:$scope.division,paged:'N'},'advanced').success(function(data){
            $scope.locations=data;
        });
        RepositoryService.find("brand",{ownerId:$scope.division, 'displayType':'true',paged:'N'},'advanced').success(function(data){
            $scope.brands=data;
        });
    }
    $scope.getDoctors=function(){
        RepositoryService.find("doctr",{location_id:$scope.location,paged:'N'},'advanced').success(function(data){
            $scope.doctors=data;
        });
    }
    $scope.getSKUs=function(){
        RepositoryService.find("prodt",{brand_id:$scope.brand,paged:'N'},'advanced').success(function(data){
            $scope.products=data;
        });
    }

    $scope.search=function(){
        var frommonth;
        var tomonth;

        if($scope.from_month){
            frommonth = monthNameToIndexJS[$scope.from_month] +1;
        }else{
            $scope.from_month =months[frommonth];
        }

        if($scope.to_month){
            tomonth = monthNameToIndexJS[$scope.to_month] +1;
        }else{
            $scope.to_month =months[tomonth];
        }


        $scope.filter = {"divisionId":$scope.division,"locationId":$scope.location,"doctorId":$scope.doctor,
            "brandId":$scope.brand,"productId":$scope.product,"from_month":frommonth,
            "from_year":$scope.from_year,"to_month":tomonth,"to_year":$scope.to_year,
            "statusId":$scope.status, paged:'Y', page:1};
        RepositoryService.search("pmt_counter_report",$scope.filter, $stateParams.context,'home').then(function(data){
            if(data.data.totalRecords==0) {
                $scope.nodata = true;
                $scope.showtable = false;
            }else{
                $scope.nodata = false;
                $scope.showtable = true;
            }
            $scope.searchData = data.data.result;
            $scope.columns = data.data.columns;
            $scope.actions = data.data.actions;
            $scope.totalItems = data.data.totalRecords;
            $scope.currentPage = data.data.currentPage;
        });
    };

    $scope.pageChanged = function(){
        $scope.filter["page"]=$scope.currentPage;
        RepositoryService.search('pmt_counter_report',$scope.filter, 'simple', 'home',null).then(function(response){
            $scope.searchData = response.data.result;
            $scope.columns = response.data.columns;
            $scope.actions = response.data.actions;
            $scope.totalItems = response.data.totalRecords;
            $scope.currentPage = response.data.currentPage;
        });
    }

    $scope.downloadReport=function(){

        var query="";
        if($scope.division!=null)
            if(query=="")
                query="divisionId~"+$scope.division;
            else
                query=query+"^divisionId~"+$scope.division;

        if($scope.location!=null)
            if(query=="")
                query="locationId~"+$scope.location;
            else
                query=query+"^locationId~"+$scope.location;


        if($scope.doctor!=null)
            if(query=="")
                query="doctorId~"+$scope.doctor;
            else
                query=query+"^doctorId~"+$scope.doctor;


        if($scope.brand!=null)
            if(query=="")
                query="brandId~"+$scope.brand;
            else
                query=query+"^brandId~"+$scope.brand;


        if($scope.product!=null)
            if(query=="")
                query="productId~"+$scope.product;
            else
                query=query+"^productId~"+$scope.product;

        if($scope.status!=null)
            if(query=="")
                query="statusId~"+$scope.status;
            else
                query=query+"^statusId~"+$scope.status;

        window.open("/webapp/download/report?cert=" +  encodeURIComponent(LocalStore.fetch('USER_INFO').certificate)  + "&queryString="+query+"&query=pmt_counter_report");
    }

});


sfe.controller('HoPatientReportController', function ($scope, $http,$state, $stateParams, $modal,SearchFormService,
                                                      RepositoryService,months,monthNameToIndexJS, LabelService, LocalStore) {

    $scope.links = LabelService.getLabels('links');
    $scope.labels = LabelService.getLabels('patient');
    $scope.divisions=[];
    $scope.locations=[];
    $scope.doctors=[];
    $scope.brands=[];
    $scope.products=[];
    $scope.statusList=[];
    $scope.months = months;
    var year = new Date().getFullYear();
    $scope.years = [year-1, year, year+1];
    $scope.filter={};

    $scope.init=function(){
        RepositoryService.find("divsn",{},'advanced').success(function(data){
            $scope.divisions=data;
        });

        RepositoryService.find("syslv",{type:"DOCTOR_STATUS1",paged:'N'},'advanced').success(function(data){
            $scope.statusList=data;
        });

    }

    $scope.getLocations=function(){
        RepositoryService.find("locat",{division_id:$scope.division,paged:'N'},'advanced').success(function(data){
            $scope.locations=data;
        });
        RepositoryService.find("brand",{ownerId:$scope.division, 'displayType':'true',paged:'N'},'advanced').success(function(data){
            $scope.brands=data;
        });
    }
    $scope.getDoctors=function(){
        RepositoryService.find("doctr",{location_id:$scope.location,paged:'N'},'advanced').success(function(data){
            $scope.doctors=data;
        });
    }
    $scope.getSKUs=function(){
        RepositoryService.find("prodt",{brand_id:$scope.brand,paged:'N'},'advanced').success(function(data){
            $scope.products=data;
        });
    }

    $scope.search=function(){
        $scope.filter={};
        var frommonth;
        var tomonth;

        if($scope.division==null || $scope.division==""){
            alert("You must select at least division")
        }
        else {
            $scope.filter.divisionId = $scope.division;

            if ($scope.location != null && $scope.location !="") {
                $scope.filter.locationId = $scope.location;
            }
            if ($scope.doctor != null && $scope.doctor != "") {
                $scope.filter.doctorId = $scope.doctor;
            }
            if ($scope.brand != null && $scope.brand != "") {
                $scope.filter.brandId = $scope.brand;
            }
            if ($scope.product != null && $scope.product != "") {
                $scope.filter.productId = $scope.product;
            }
            if ($scope.doctor != null && $scope.doctor != "") {
                $scope.filter.doctorId = $scope.doctor;
            }

            if ($scope.from_month != null && $scope.from_year != null) {

                if ($scope.from_month != null && $scope.from_month != "") {
                    frommonth = monthNameToIndexJS[$scope.from_month] + 1;
                    $scope.filter.from_month = frommonth;
                }

                if ($scope.from_year != null && $scope.from_year != "") {
                    $scope.filter.from_year = $scope.from_year;
                }

            }

            if ($scope.to_month != null && $scope.to_year != null) {

                if ($scope.to_month != null && $scope.to_month != "") {
                    tomonth = monthNameToIndexJS[$scope.to_month] + 1;
                    $scope.filter.to_month = tomonth;
                }

                if ($scope.to_year != null && $scope.to_year != "") {
                    $scope.filter.to_year = $scope.to_year;
                }

            }

            if ($scope.status != null && $scope.status != "") {
                $scope.filter.statusId = $scope.status;
            }

            $scope.filter.paged = 'Y';
            $scope.filter.page = '1';


            console.log($scope.filter);
            /*$scope.filter = {"divisionId":$scope.division,"locationId":$scope.location,"doctorId":$scope.doctor,
             "brandId":$scope.brand,"productId":$scope.product,"from_month":frommonth,
             "from_year":$scope.from_year,"to_month":tomonth,"to_year":$scope.to_year,
             "statusId":$scope.status, paged:'Y', page:1};*/

            RepositoryService.search("pmt_patient_report", $scope.filter, $stateParams.context, 'home').then(function (data) {
                if (data.data.totalRecords == 0) {
                    $scope.nodata = true;
                    $scope.showtable = false;
                } else {
                    $scope.nodata = false;
                    $scope.showtable = true;
                }
                $scope.searchData = data.data.result;
                $scope.columns = data.data.columns;
                $scope.actions = data.data.actions;
                $scope.totalItems = data.data.totalRecords;
                $scope.currentPage = data.data.currentPage;
            });
        }
    };

    $scope.pageChanged = function(){
        $scope.filter["page"]=$scope.currentPage;
        RepositoryService.search('pmt_patient_report',$scope.filter, 'simple', 'home',null).then(function(response){
            $scope.searchData = response.data.result;
            $scope.columns = response.data.columns;
            $scope.actions = response.data.actions;
            $scope.totalItems = response.data.totalRecords;
            $scope.currentPage = response.data.currentPage;
        });
    }

    $scope.downloadReport=function(){

        var query="";
        if($scope.division!=null)
            if(query=="")
                query="divisionId~"+$scope.division;
            else
                query=query+"^divisionId~"+$scope.division;

        if($scope.location!=null)
            if(query=="")
                query="locationId~"+$scope.location;
            else
                query=query+"^locationId~"+$scope.location;


        if($scope.doctor!=null)
            if(query=="")
                query="doctorId~"+$scope.doctor;
            else
                query=query+"^doctorId~"+$scope.doctor;


        if($scope.brand!=null)
            if(query=="")
                query="brandId~"+$scope.brand;
            else
                query=query+"^brandId~"+$scope.brand;


        if($scope.product!=null)
            if(query=="")
                query="productId~"+$scope.product;
            else
                query=query+"^productId~"+$scope.product;

        if($scope.status!=null)
            if(query=="")
                query="statusId~"+$scope.status;
            else
                query=query+"^statusId~"+$scope.status;

        console.log(LocalStore.fetch('USER_INFO').certificate);
        window.open("/webapp/download/report?cert=" +   encodeURIComponent(LocalStore.fetch('USER_INFO').certificate)  + "&queryString="+query+"&query=pmt_patient_report");
    }

});

sfe.controller('HOPatientApprovalController', function ($scope, $http,$state, $stateParams, $modal,SearchFormService,
                                                        RepositoryService, PatientApprovalService, LabelService, LocalStore) {

    $scope.links = LabelService.getLabels('links');
    $scope.labels = LabelService.getLabels('patient');
    $scope.stdFormData = {"approvarId":LocalStore.fetch('USER_INFO').id};
    $scope.flag = false;
    $scope.initSearch = function() {
        $scope.stdFormTemplate = SearchFormService.getForm($stateParams.context, 'ho_patnt_approval_select', 'patient');
        $scope.search();
    };

    $scope.search=function(){

        RepositoryService.search("ho_patnt_approval",$scope.stdFormData, $stateParams.context,'ho').then(function(data){
            if(data.data.totalRecords==0) {
                $scope.nodata = true;
                $scope.showtable = false;
            }else{
                $scope.nodata = false;
                $scope.showtable = true;
            }
            $scope.searchData = data.data.result;
            $scope.columns = data.data.columns;
            $scope.actions = data.data.actions;
            $scope.totalItems = data.data.totalRecords;
            $scope.currentPage = data.data.currentPage;
        });
    };

    $scope.approve = function(id){
        var cnf = confirm("Are you sure you want to approve this Patient?");
        if(cnf) {
            PatientApprovalService.submitapproval(id, 'approved').then(function(data){
                $scope.search();
                alert("Successfully Approved!");
            });
        }
    };

    $scope.reject = function(id){
        var cnf = confirm("Are you sure you want to reject this Patient?");
        if(cnf) {
            PatientApprovalService.submitapproval(id, 'rejected').then(function(data){
                $scope.search();
                alert("Rejected!");
            });
        }
    };

});


sfe.controller('CounterController', function ($scope, $http,$state, $stateParams, $modal,
                                              RepositoryService, LabelService, LocalStore) {

    $scope.links = LabelService.getLabels('links');
    $scope.labels = LabelService.getLabels('patient');
    $scope.master = {};
    $scope.controls = {};
    $scope.states = [];
    $scope.counter={"code":""};
    $scope.division={};

    $scope.init = function() {
        RepositoryService.find("docdp",{locationId: LocalStore.fetch('headerdata').location.id, paged:'N'},'advanced').success(function(data){
            $scope.master.doctors = data;
        });

        RepositoryService.find("state",{},'simple').then(function(states){
            $scope.states = states.data;
        });

        if($stateParams.id!=null) {
            RepositoryService.restore($stateParams.id).success(function (counter) {
                $scope.counter = counter;
            });
        }
        $scope.usercontext = LocalStore.fetch('topcontext');
        if($scope.usercontext=='home')
        {
            var loggedInUserId = LocalStore.fetch('headerdata').id;
            RepositoryService.restore(loggedInUserId).then(function(data){
                RepositoryService.restore(data.data.companydetails[0].location.id).then(function(loc){
                    $scope.counter.location={"id":loc.data.id,"displayName":loc.data.name};
                    $scope.division = {"id":loc.data.division.id,"displayName":loc.data.division.displayName};
                });
            });
        }

    };

    $scope.save = function(){
        RepositoryService.save($scope.counter, 'CONTR').success(function(data){
            $scope.controls.message = 'Added Successfully!';
            $scope.controls.showAlert = true;
            RepositoryService.restore(data.id).success(function(counter){
                $scope.counter = counter;
                $scope.newHistory();
                $scope.showHistory();
            });

        });
    };

    $scope.newHistory = function(id){
        if(id!=null)
        {
            $scope.flag=true;
        }
        else
        {
            $scope.flag=false;
        }
        var modalInstance = $modal.open({
            templateUrl: 'patientpurchase.html',
            controller: 'PatientPurchaseController',
            size: 'lg',
            backdrop: true,
            backdropClick: true,
            dialogFade: true,
            keyboard: false,
            resolve: {
                detail: function(){
                    console.log($scope.counter.id);
                    return $scope.counter.id;
                },
                history: function(){
                    return id;
                },
                flag: function(){
                    return $scope.flag;
                }
            }
        });
    };

    $scope.showHistory = function(){

        if($scope.counter.id!=null)
        {
            $scope.showtable = true;
            RepositoryService.search('adhoc_skupd',{"ownerId":$scope.counter.id,paged:'Y', page:1}, 'simple', 'home',null).then(function(data){
                if(data.data.totalRecords==0) {
                    $scope.nodata = true;
                    $scope.showtable = false;
                }else{
                    $scope.nodata = false;
                }
                $scope.searchData = data.data.result;
                $scope.columns = data.data.columns;
                $scope.actions = data.data.actions;
                $scope.totalItems = data.data.totalRecords;
                $scope.currentPage = data.data.currentPage;
            });
        }
    }

});


sfe.controller('PatientPurchaseController', function ($scope, $http,$state, $modal,$modalInstance, $stateParams, detail, history, flag,
                                                      RepositoryService, LabelService, LocalStore) {
    $scope.links = LabelService.getLabels('links');
    $scope.labels = LabelService.getLabels('patient');
    $scope.master = {};
    $scope.data=[];
    $scope.ppunits=[];
    $scope.fHide=false;
    $scope.pbrand = {};
    $scope.pbrand.frequency={};
    $scope.pbrand = {owner:{id:detail}};
    $scope.phistory={};
    $scope.phistory.unit={};
    $scope.phistory.patientBrand={};
    $scope.showRenewal = false;
    $scope.renewaldata=[];
    $scope.init =function(){
        if(history!=null)
        {
            RepositoryService.restore(history).success(function(data){
                $scope.pbrand = data;
                RepositoryService.find("skuph",{"ownerId":$scope.pbrand.id,"sortBy":"UPDATED_ON","sortOrder":"ASC"}).success(function(data){
                    $scope.data=data;
                });

                RepositoryService.find("prodt",{brand_id:$scope.pbrand.brand.id,paged:'N'},'advanced').success(function(data){
                    $scope.master.products=data;
                });

                if($scope.pbrand.frequency!=null){
                    $scope.phistory.unit.id=$scope.pbrand.frequency.frequencyUnit.id;
                }
            });
        }
        else
        {
            $scope.fHide=true;
        }

        if($scope.pbrand.frequency!=null){
            $scope.phistory.unit.id=$scope.pbrand.frequency.frequencyUnit.id;
        }
        RepositoryService.find("usrlv",{'type':'patientpurchaseunit'},'simple').success(function(data){
            $scope.ppunits = data;
        });
        RepositoryService.find("usrlv",{'type':'FREQUENCY_UNIT'},'simple').success(function(data){
            $scope.frequencyUnits = data;
        });
        RepositoryService.find("brand",{'displayType':'true','locationId':LocalStore.fetch('headerdata').companydetails[0].location.id,paged:'N'},'advanced').success(function(data){
            $scope.master.brands = data;
        });
    }

    $scope.showSkus = function(){
        RepositoryService.find("patient_sku",{"brandId":$scope.pbrand.brand.id, "ownerId":$stateParams.id, paged:'N'},'advanced').success(function(data){
            $scope.master.products = data;
        });
    }

    $scope.setpurchaseUnit = function(){
        if($scope.pbrand.frequency!=null){
            $scope.phistory.unit.id=$scope.pbrand.frequency.frequencyUnit.id;
        }
    }

    $scope.closeModal = function(){
        $modalInstance.dismiss('cancel');
    }


    $scope.save = function(){
        $scope.pbrand.status={"id":"SYSLVDOCSTA00000000000000000000000001"};
        var frequencyUnit;
        if($scope.pbrand.frequency!=null){
            frequencyUnit = $scope.pbrand.frequency.frequencyUnit.id;
        }
        RepositoryService.save($scope.pbrand, 'SKUPD').success(function(data){
            $scope.phistory.patientBrand={};
            $scope.phistory.patientBrand.id = data.id;
            RepositoryService.save($scope.phistory, 'SKUPH').success(function(data){
                if(flag)
                {
                    $scope.data.push($scope.phistory);
                    $scope.phistory={};
                    if($scope.pbrand.frequency!=null){
                        $scope.phistory.unit={"id":frequencyUnit};
                    }
                }else
                {
                    $scope.closeModal();
                }
            });
        });
    }
});

sfe.controller('DiabetisPatientController', function ($scope, $http,$state, $stateParams, $modal,
                                                      RepositoryService, LabelService, LocalStore) {
    $scope.dPatient={};
    $scope.dPatient = {"status":{"id":"SYSLVPATST000000000000000000000000001"},"counselor":{"id":LocalStore.fetch('headerdata').id}};
    $scope.master={};
    $scope.showreason=false;
    $scope.reason={};
    $scope.patreason={};
    $scope.counselors=[];
    $scope.patientReadings = [];
    $scope.master.brands = [];
    $scope.disableProfile = false;
    $scope.hour={};
    $scope.minute={};
    $scope.timestate={};
    $scope.hours = ["01","02","03","04","05","06","07","08","09","10","11","12"];
    $scope.minutes = ["00","15","30","45"];
    $scope.names=[];
    $scope.state={};
    $scope.city={};
    $scope.selectedDoctor = {};
    $scope.init = function() {

        var year = new Date().getFullYear();
        RepositoryService.find("state",{},'simple').then(function(states){
            $scope.states = states.data;
        });
        RepositoryService.find("usrlv",{'type':'INCOME_GROUP'},'simple').success(function(data){
            $scope.incomeGroup = data;
        });

        RepositoryService.find("syslv",{"type":'DIABETES_PATIENT_INACTIVATION_REASON'},'simple').then(function(data){
            $scope.reasons = data.data;
        });

        RepositoryService.find("usrlv",{"type":'INSULIN_THERAPY'},'simple').then(function(data){
            $scope.therapies = data.data;
        });

        RepositoryService.find("patient_counselor",{},'simple').then(function(data){
            $scope.counselors = data.data;
        });

        $scope.usercontext = LocalStore.fetch('topcontext');
        if($scope.usercontext=='home')
        {
            var loggedInUserId = LocalStore.fetch('headerdata').id;
            RepositoryService.restore(loggedInUserId).then(function(data){
                RepositoryService.restore(data.data.companydetails[0].location.id).then(function(loc){
                    $scope.dPatient.belongsTo={"id":loc.data.id,"displayName":loc.data.name};
                    $scope.division = {"id":loc.data.division.id,"displayName":loc.data.division.displayName};
                });
            });
        }
        if($stateParams.id!=null){
            RepositoryService.restore($stateParams.id).success(function(dPatient){
                $scope.dPatient = dPatient;
                if($scope.usercontext=='pcadmin'){
                    $scope.disableProfile = true;
                }
                else if($scope.dPatient.status.id=='SYSLVPATST000000000000000000000000002') {
                    $scope.showreason=true;
                    $scope.disableProfile = true;
                }
                else
                {
                    $scope.disableProfile = false;
                }
                RepositoryService.find("patdp",{"patientId":$stateParams.id},'simple').then(function(data) {
                    $scope.dPatient.diseaseProfile = data.data[0];
                });
                RepositoryService.find("patdi",{"patientId":$stateParams.id},'simple').then(function(data) {
                    $scope.dPatient.patientDiagnosicInfo = data.data[0];
                    if($scope.dPatient.patientDiagnosicInfo!=null){
                        if($scope.dPatient.patientDiagnosicInfo["preferredTimeToCall"]!=null){
                            var temp1 = $scope.dPatient.patientDiagnosicInfo["preferredTimeToCall"].split(":");
                            $scope.hour.number = temp1[0];
                            var temp2 = temp1[1].split(" ");
                            $scope.minute.number = temp2[0];
                            $scope.timestate.state=temp2[1];
                        }
                    }
                });
                RepositoryService.find("doctr_filter_by_city",{"doctorId": $scope.dPatient.consultingDoctor.id},'simple').success(function(data){
                    $scope.selectedDoctor = data[0];
                    $scope.city.displayname = $scope.selectedDoctor.city;
                    $scope.state.displayname = $scope.selectedDoctor.state;
                });
                RepositoryService.find("patir",{"patientId":$stateParams.id},'simple').then(function(res){
                    if(res.data[0]!=null)
                    {
                        $scope.reason = res.data[0].reason;
                    }
                });
            });
        }
    }

    $scope.showReasons = function(){
        if($scope.dPatient.status.id=='SYSLVPATST000000000000000000000000002') {
            $scope.showreason=true;
            $scope.disableProfile = true;

        } else {
            $scope.showreason=false;
            $scope.disableProfile = false;

        }
    };

    $scope.searchForDoctor=function(){
        var modalInstance = $modal.open({
            templateUrl: 'consultingDoctorSearch.html',
            controller: 'SearchDPDoctorController',
            size: 'lg',
            backdrop: true,
            backdropClick: true,
            dialogFade: true,
            keyboard: false,
            resolve: {
                state:function(){
                    return $scope.state.id;
                },
                city: function(){
                    return $scope.city.id;
                }
            }
        });
        modalInstance.result.then(function (val) {
            /*if(val.city!=null){
             $scope.selectDoctor(val.city);
             }*/
            $scope.dPatient.consultingDoctor = {};
            $scope.dPatient.consultingDoctor = val.doctor;
            $scope.city["displayname"] =val.city;
            $scope.state["displayname"] = val.state;
        }, function () {
        });
    };

    $scope.calculateBMI = function(){
        console.log($scope.dPatient.height);
        console.log($scope.dPatient.width);
        var heightInMeter = $scope.dPatient.height/100;
        var bmi = $scope.dPatient.weight/(heightInMeter * heightInMeter);
        $scope.dPatient.bmi = Math.round(bmi*100)/100;
    }

    $scope.save = function(flag){
        if($scope.dPatient.status.id=="SYSLVPATST000000000000000000000000001"){
            $scope.reason = {};
            $scope.dPatient["patientInactivationReason"]={};
        }
        else
        {
            $scope.dPatient["patientInactivationReason"]={"id":$scope.reason.id};
        }

        var heightInMeter = $scope.dPatient.height/100;
        var bmi = $scope.dPatient.weight/(heightInMeter * heightInMeter);
        $scope.dPatient.bmi = Math.round(bmi*100)/100;
        if($scope.dPatient.patientDiagnosicInfo==null){
            $scope.dPatient.patientDiagnosicInfo ={};
            if($scope.hour.number!=null&& $scope.minute.number!=null&&$scope.timestate.state!=null){
                $scope.dPatient.patientDiagnosicInfo["preferredTimeToCall"] = $scope.hour.number +":"+ $scope.minute.number+" "+ $scope.timestate.state;
            }
        }else
        {
            if($scope.hour.number!=null&& $scope.minute.number!=null&&$scope.timestate.state!=null){
                $scope.dPatient.patientDiagnosicInfo["preferredTimeToCall"] = $scope.hour.number +":"+ $scope.minute.number+" "+ $scope.timestate.state;
            }
        }

        RepositoryService.save($scope.dPatient, 'DPATN').success(function(data){

            if($stateParams.id!=null){
                alert("Saved Details successfully");
                $scope.init();
            }
            else
            {
                $scope.dPatient.id = data.id;
                $scope.patientReadingPopup($scope.dPatient.id,"add");
            }
        });
    }

    $scope.fetchcity=function(id,flag) {
        RepositoryService.find('towns', {"stateId": id}, 'simple').then(function (data) {
            $scope.cities = data.data;
        });
    }

    $scope.selectDoctor=function(city){
        RepositoryService.find("doctr_filter_by_city",{"city": city.id},'simple').success(function(data){
            $scope.master.doctors = data;
        });
    };

    $scope.showPatientCallDetails = function(){

        $scope.showtable = true;
        RepositoryService.search('pread',{"patientId":$scope.dPatient.id,paged:'Y', page:1}, 'simple', 'home',null).then(function(data){
            if(data.data.totalRecords==0) {
                $scope.nodata = true;
                $scope.showtable = false;
            }else{
                $scope.nodata = false;
            }
            $scope.searchData = data.data.result;
            $scope.columns = data.data.columns;
            $scope.actions = data.data.actions;
            $scope.totalItems = data.data.totalRecords;
            $scope.currentPage = data.data.currentPage;
        });
    };

    $scope.patientReadingPopup = function(id, action){
        $scope.disableData=true;
        $scope.disableNextDt=true;

        RepositoryService.find("pread",{"id": id},'simple').success(function(data){
            $scope.patientReadings = data[0];

            if($scope.patientReadings!=null){
                RepositoryService.getCurrentDate().success(function(cDate) {

                    var currentDate=new Date(cDate.date);
                    var callDate=new Date($scope.patientReadings.dateOfCall.date)
                    var nextCallDate=new Date($scope.patientReadings.nextCall.date)

                    if(currentDate.getFullYear()==callDate.getFullYear() && currentDate.getMonth()==callDate.getMonth()){
                        if(currentDate.getDate()<= callDate.getDate()+2){
                            $scope.disableData=false;
                        }
                    }

                    if(currentDate.getFullYear()==nextCallDate.getFullYear() && currentDate.getMonth()==nextCallDate.getMonth()){
                        if(currentDate.getDate()<= nextCallDate.getDate()+2){
                            $scope.disableNextDt=false;
                        }
                    }

                    var dateOfcall = $scope.patientReadings.dateOfCall.date;
                    var modalInstance = $modal.open({
                        templateUrl: 'patientReadings.html',
                        controller: 'PatientReadingController',
                        size: 'lg',
                        backdrop: true,
                        backdropClick: true,
                        dialogFade: true,
                        keyboard: false,
                        resolve: {
                            id:function(){
                                return id;
                            },
                            pid: function(){
                                return $scope.dPatient.id;
                            },
                            action: function(){
                                return action;
                            },
                            DisableData:function(){
                                return $scope.disableData;
                            },
                            DisableNextCallDate:function(){
                                return $scope.disableNextDt;
                            }
                        }
                    });
                    modalInstance.result.then(function (val) {
                    }, function () {
                        $scope.showPatientCallDetails();
                    });

                });
            }
            else
            {
                $scope.disableData=false;
                $scope.disableNextDt=false;
                var modalInstance = $modal.open({
                    templateUrl: 'patientReadings.html',
                    controller: 'PatientReadingController',
                    size: 'lg',
                    backdrop: true,
                    backdropClick: true,
                    dialogFade: true,
                    keyboard: false,
                    resolve: {
                        id:function(){
                            return id;
                        },
                        pid: function(){
                            return $scope.dPatient.id;
                        },
                        action: function(){
                            return action;
                        },
                        DisableData:function(){
                            return $scope.disableData;
                        },
                        DisableNextCallDate:function(){
                            return $scope.disableNextDt;
                        }
                    }
                });
                modalInstance.result.then(function (val) {
                }, function () {
                    $scope.showPatientCallDetails();
                });
            }

        });

    };
});

sfe.controller('PatientReadingController', function ($scope, $http,$state, $stateParams, $modal, id,pid, action,
                                                     RepositoryService, $modalInstance,LabelService, LocalStore,DisableData,DisableNextCallDate) {
    $scope.readings = {};
    $scope.readings.dpatient={"id":pid};
    $scope.readings.brandDetails=[];
    $scope.master={};
    $scope.master.brands = [];
    $scope.brandDetail={};
    $scope.existingCalls=[];
    $scope.flag=false;
    $scope.DisableData=DisableData;
    $scope.disableNextDt=DisableNextCallDate;

    $scope.init = function() {

        RepositoryService.find('exisiting_call_date',{"patientId":pid},'simple').then(function(data){
            $scope.existingCalls = data;
        });

        if(action == "edit"){
            RepositoryService.find('pread', {"id": id}, 'simple').then(function (data) {
                $scope.readings = data.data[0];
                RepositoryService.find("brdet",{"patientReadingId":id},'simple').success(function(data){

                    $scope.readings.brandDetails=data;
                });
                RepositoryService.find("pmt_brand",{"patientReadingId":id},'advanced').success(function(data){
                    $scope.master.brands=data;
                });
            });
        }
        else
        {
            RepositoryService.find("pmt_brand",{},'advanced').success(function(data){
                $scope.master.brands=data;
            });
        }
    }

    $scope.addBrand = function(){
        $scope.brandDetail = {};
        $scope.brandDetail.brand = {};
        var selectedBrand = {id:$scope.brand};
        $($scope.master.brands).each(function(data){
            var brand = $(this)[0];
            if(brand.id==$scope.brand){
                selectedBrand.name = brand.name;
                $scope.brandDetail.brand.displayName = selectedBrand.name;
                $scope.brandDetail.brand.id = selectedBrand.id;
                $scope.brandDetail.batch = $scope.batch;
                $scope.master.brands.splice(data,1);
            }
        });
        $scope.readings.brandDetails.push($scope.brandDetail);

    }

    $scope.removeBrand = function(id){
        var index = -1;
        $($scope.readings.brandDetails).each(function(i){
            var o = $(this)[0];
            if(o.brand.id==id)
            {
                index =i;
                var brand = {"name":o.brand.displayName, "id": o.brand.id};
                $scope.master.brands.push(brand);
            }
        });
        $scope.readings.brandDetails.splice(index,1);
    }

    $scope.save = function(){
        console.log($scope.readings);
        $($scope.existingCalls.data).each(function(i){
            var callDetail = $(this)[0];
            if(callDetail.date_of_call.text==$scope.readings.dateOfCall.text){
                $scope.flag = true;
                return false;
            }
            $scope.flag = false;
        });
        if($scope.readings.id!=null){
            RepositoryService.save($scope.readings, 'PREAD').success(function(data){
                alert("Patient details saved successfully.")
                $scope.closeModal();
            });
        }
        else
        {
            if(!$scope.flag){
                RepositoryService.save($scope.readings, 'PREAD').success(function(data){
                    alert("Patient details saved successfully.")
                    $scope.closeModal();
                });
            }
            else
            {
                alert("You are already having call detail for same date.");
            }
        }

    }

    $scope.closeModal = function(){
        $modalInstance.dismiss('cancel');
    }
});


sfe.controller('SearchDPDoctorController', function ($scope, $http,$state, $stateParams, $modal, state,city,
                                                     RepositoryService, $modalInstance, SearchFormService) {

    $scope.doctors = [];
    $scope.stdFormTemplate = {};
    $scope.stdFormData = {};
    $scope.selectedDoctor = {};
    $scope.init = function() {
        RepositoryService.find("state",{},'simple').then(function(states){
            $scope.states = states.data;
        });
    };

    $scope.selectcity=function(id,flag) {
        $scope.city = null;
        $scope.doctor =null;
        RepositoryService.find('towns', {"stateId": id}, 'simple').then(function (data) {
            $scope.cities = data.data;
        });
    }

    $scope.search=function(){
        var filter = {};
        if($scope.state!=null && $scope.state.id!=null){
            filter["stateId"] = $scope.state.id;
        }
        if($scope.city!=null && $scope.city.id!=null){
            filter["cityId"] = $scope.city.id;
        }
        if($scope.doctor!=null && $scope.doctor.name!=null){
            filter["doctorname"] = encodeURIComponent("%"+$scope.doctor.name+"%");
        }

        RepositoryService.find("doctr_filter_by_city",filter,'simple').success(function(data){
            $scope.doctors = data;
        });
    }

    $scope.doctorSelect=function(isSelected,doctr){
        if(isSelected) {
            $scope.selectedDoctor={};
            $scope.selectedDoctor["doctor"] = {"id":doctr.doctorId,"displayName":doctr.doctorname};
            $scope.selectedDoctor["city"] = doctr.city;
            $scope.selectedDoctor["state"]= doctr.state;
        }
        $modalInstance.close($scope.selectedDoctor);
    }

    $scope.closeModal = function(){
        $modalInstance.dismiss('cancel');

    }
});

sfe.controller('PatientCallSearchController',function ($scope, $http,$state, $stateParams, $modal,
                                                       RepositoryService, LocalStore) {

    $scope.results = [];
    $scope.init = function(){

    }

    $scope.search = function(){
        var arr = $scope.nextCallDate.split('/');
        var nextCallDate = arr[2]+"-"+arr[1]+"-"+arr[0];
        RepositoryService.find("patient_readings_by_nextcalldate",{'nextcalldate':nextCallDate, 'employeeId':LocalStore.fetch('headerdata').id},'simple').then(function(data){
            $scope.results = data.data;
        });
    }
});




