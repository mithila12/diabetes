/**
 * Created by mangesh on 05/08/15.
 */

sfe.controller('DoctorROIController', function ($scope, $http,$state, $stateParams, $modal,months,
                                              RepositoryService, LabelService, LocalStore,monthNameToIndexJS) {
    $scope.doctors = [];
    $scope.brands=[];
    $scope.years=[];
    $scope.brand = {};
    $scope.doctor = {};
    $scope.filter = {};
    $scope.chart = {};
    $scope.chart1 = {};
    $scope.months=[];
    $scope.master={};
    $scope.master.divisions = [];
    $scope.master.locations = [];
    $scope.monthList = [];
    $scope.displayMonthList = [];
    $scope.result = [];
    $scope.dateWiseSummaryTotal={};
    $scope.showTable=false;
    $scope.division={};
    $scope.location = {};
    $scope.dateWiseSummary = {};
    $scope.dateWiseSummaryTotal = {};

    $scope.init = function() {

        RepositoryService.find("divsn",{},'simple').success(function(data){
            $scope.master.divisions = data;
        });

        /*RepositoryService.find("doctor_roi_report",{},'simple').success(function(data){
            $scope.doctors = data;
        });*/

        $scope.months=months;
        var year = new Date().getFullYear();
        $scope.years = [year-1, year, year+1];
    };

    $scope.fetchLocations = function(){
        $scope.location={};
        $scope.doctors = [];
        $scope.doctor={};
        $scope.brands = [];
        $scope.brand={};
        RepositoryService.find("locat",{"division_id":$scope.division.id, "isActive":"true"},'advanced').success(function(data){
            $scope.master.locations = data;
        });
        RepositoryService.find("brand",{"ownerId":$scope.division.id},'advanced').success(function(data){
            $scope.brands = data;
        });
    }

    $scope.fetchDoctors = function(){
        RepositoryService.find("doctr",{"location_id":$scope.location.id, "KOL":"1"},'simple').success(function(data){
            $scope.doctors = data;
        });
    }

    $scope.generateReport = function() {

        $scope.filterList = {};
        $scope.dateWiseSummary = {};
        $scope.dateWiseSummaryTotal = {};
        $scope.monthList = [];
        $scope.displayMonthList=[];
        var fromMonth = monthNameToIndexJS[$scope.fromMonth] +1;
        var valueFromMonth = fromMonth;
        var toMonth = monthNameToIndexJS[$scope.toMonth]+1;
        $scope.monthList.push(fromMonth);
        while(fromMonth < toMonth){
            fromMonth = fromMonth +1;
            $scope.monthList.push(fromMonth);
        }
        for(var i=0;i<$scope.monthList.length;i++){
            $scope.displayMonthList.push(months[$scope.monthList[i] - 1]);

        }
        $scope.summaryColumns = [{"displayName":"Potential","columnName":"totalPotential","bold":false},
            {"displayName":"Actual Sales","columnName":"totalActualSales","bold":false},
            {"displayName":"COGS","columnName":"totalCogs","bold":false},
            {"displayName":"Brand Contribution", "columnName":"totalBrandContribution","bold":true},
            {"displayName":"Input Cost","columnName":"totalInputCost","bold":false},
            {"displayName":"Sample Cost","columnName":"totalSampleCost","bold":false},
            {"displayName":"Employee Cost","columnName":"totalEmployeeCost","bold":false},
            {"displayName":"Marketting Cost","columnName":"totalMarkettingCost","bold":true},
            {"displayName":"Marketting Contribution","columnName":"totalMarkettingContribution","bold":true},
            {"displayName":"CULBUTE(%)","columnName":"totalCulbute","bold":true}
            ];

        $scope.filterList = {"fromMonth":valueFromMonth,"toMonth":toMonth,"fromYear":$scope.fromYear,"toYear":$scope.toYear};
        if($scope.doctor.id!=null){
            $scope.filterList["doctorId"] = $scope.doctor.id;
        }
        if($scope.doctor.id!=null){
            $scope.filterList["doctorId"] = $scope.doctor.id;
        }
        if($scope.brand.id!=null){
            $scope.filterList["brandId"] = $scope.brand.id;
        }
        if($scope.location.id!=null){
            $scope.filterList["locationId"] = $scope.location.id;
        }
        if($scope.division.id!=null){
            $scope.filterList["divisionId"] = $scope.division.id;
        }


        RepositoryService.find("roi_summary",$scope.filterList,'simple').success(function(data){
            $scope.result = data;
            $scope.showTable=true;
            for(var i=0 ; i<$scope.result.length; i++){
                var tempDate = new Date($scope.result[i].date.date);
                var index = tempDate.getMonth() +1;
                $scope.dateWiseSummary[index] = $scope.result[i];
                $scope.createChart($scope.dateWiseSummary,$scope.displayMonthList);
            }
        });
        RepositoryService.find("roi_summary_total",$scope.filterList,'simple').success(function(data){
            $scope.dateWiseSummaryTotal = data[0];
        });
    }

    $scope.downloadReport= function(){
        var query="";
        if($scope.division.id!=null) {
            if(query=="")
                query="divisionId~"+$scope.division.id;
            else
                query=query+"^divisionId~"+$scope.division.id;
        }

        if($scope.location.id!=null){
            if(query=="")
                query="locationId~"+$scope.location.id;
            else
                query=query+"^locationId~"+$scope.location.id;
        }

        if($scope.doctor.id!=null){
            if(query=="")
                query="doctorId~"+$scope.doctor.id;
            else
                query=query+"^doctorId~"+$scope.doctor.id;
        }

        var valueFromMonth;
        if($scope.fromMonth!=null){
            valueFromMonth = monthNameToIndexJS[$scope.fromMonth] +1;
            if(query=="")
                query="frmoMonth~"+valueFromMonth;
            else
                query=query+"^fromMonth~"+valueFromMonth;
        }
        var toMonth;
        if($scope.toMonth!=null){
            var toMonth = monthNameToIndexJS[$scope.toMonth]+1;
            if(query=="")
                query="toMonth~"+toMonth;
            else
                query=query+"^toMonth~"+toMonth;
        }
        if($scope.fromYear!=null){
            if(query=="")
                query="fromYear~"+$scope.fromYear;
            else
                query=query+"^fromYear~"+$scope.fromYear;
        }
        if($scope.toYear!=null){
            if(query=="")
                query="toYear~"+$scope.toYear;
            else
                query=query+"^toYear~"+$scope.toYear;
        }

        window.open("/webapp/download/roiReport?certificate="+encodeURIComponent(LocalStore.fetch("USER_INFO").certificate)+"&queryString="+query+"&query=roi_summary&fromMonth="+valueFromMonth+"&toMonth="+toMonth);
    }

    $scope.createChart = function(data,displayMonthList){

        $scope.culbute=[];
        $scope.markettingContribution=[];
        $scope.markettingCost=[];
        $scope.brandContribution=[];

        for(var i=0;i<$scope.displayMonthList.length;i++){
            if(data[monthNameToIndexJS[$scope.displayMonthList[i]]]!=null){

                $scope.culbute[i-1]=data[monthNameToIndexJS[$scope.displayMonthList[i]]]["totalCulbute"];
                $scope.markettingContribution[i-1]=data[monthNameToIndexJS[$scope.displayMonthList[i]]]["totalMarkettingContribution"];
                $scope.brandContribution[i-1]=data[monthNameToIndexJS[$scope.displayMonthList[i]]]["totalBrandContribution"];
            }
            else {
                $scope.markettingContribution[i-1]=0;
                $scope.brandContribution[i-1]=0;
                $scope.culbute[i-1]=0;
            }
        }

        var seriesData1 = {"name":"Brand Contribution", "data":$scope.brandContribution};
        var seriesData2 = {"name":"Marketting Contribution","data":$scope.markettingContribution};
        var seriesData3 = {"name":"Culbute","data":$scope.culbute};
        var seriesDataList=[];
        seriesDataList.push(seriesData1);
        seriesDataList.push(seriesData2);
        seriesDataList.push(seriesData3);

        $scope.chart = {"type":"line"};
        $scope.chart.title = {"text" : "Culbute Summary "} ;
        $scope.chart.xAxis = { "categories" :displayMonthList};

        $scope.chart.yAxis =
            {
                title: {
                    text: 'Culbute (%)'
                }
            }
        ;
        $scope.chart.series = seriesDataList;
    }
});