sfe.controller('AnnouncementController', function($scope, $state, $stateParams, LocalStore,RepositoryService,$http,$modal,SearchFormService, SearchConfigService, LabelService){
    $scope.divisions={};
    $scope.locations={};
    $scope.jobTitles={};
    $scope.employeeList={};
    $scope.definition={};
    $scope.uploadResult={};
    $scope.uploadResult.successList=[];
    $scope.uploadResult.failureList=[];
    $scope.uploadResult.successCount=0;
    $scope.uploadResult.failCount=0;
    $scope.addedCriteria=[];
    $scope.htmlContent="";
    $scope.announcementId=null;
    $scope.init=function() {
        RepositoryService.find('divsn', {}, 'simple').then(function (data) {
            $scope.divisions = data.data;
        });
        RepositoryService.find('jobtt', {}, 'simple').then(function (data) {
            $scope.jobTitles = data.data;
        });

        if($stateParams.id!=null){
            RepositoryService.restore($stateParams.id).then(function(data){
                $scope.announcementId=data.data.id;
                var owners=data.data.owners;
                for(o in owners){
                    var owner=owners[o];
                    if(owner.type==='ADDED') {
                        var criteria={};
                        if(owner.division!=null) {
                            criteria.division = {id: owner.division.id, name: owner.division.displayName}
                        }else{
                            criteria.division={};
                        }
                        if(owner.location!=null) {
                            criteria.location = {id: owner.location.id, name: owner.location.displayName}
                        }else{
                            criteria.location={};
                        }
                        if(owner.jobTitle!=null) {
                            criteria.jobTitle = {id: owner.jobTitle.id, name: owner.jobTitle.displayName}
                        }else{
                            criteria.jobTitle={};
                        }
                        if(owner.employee!=null) {
                            criteria.employee = {id: owner.employee.id, name: owner.employee.displayName}
                        }else{
                            criteria.employee={};
                        }
                        $scope.addedCriteria.push(criteria);
                    }else{
                        RepositoryService.find("emply_simple",{id:owner.employee.id}).then(function(data){
                            $scope.uploadResult.successList.push(data.data[0]);
                            $scope.uploadResult.successCount=parseInt($scope.uploadResult.successCount)+1;
                        })
                    }
                }
                $scope.definition.title=data.data.title;
                $scope.htmlContent=data.data.message;
                $scope.definition.activeFrom=data.data.activeFrom.text;
                $scope.definition.activeTo=data.data.activeTo.text;
            })
        }
    };

    $scope.getLocation=function(){
        RepositoryService.find("locat",{division_id:$scope.definition.division.id,'sortBy':'name','sortOrder':'ASC'},'simple').then(function (data) {
            $scope.locations = data.data;

            RepositoryService.find("emply_creiteria",{division:$scope.definition.division.id},'simple').then(function(empData){
                $scope.employeeList=empData.data;
            })
        });
    };

    $scope.getEmployee=function(type){

        if(type==='location') {
            RepositoryService.find("emply_creiteria", {location: $scope.definition.location.id}, 'simple').then(function (empData) {
                $scope.employeeList = empData.data;
            })
        }else{
            if($scope.definition.division!=null && $scope.definition.location==null)
            RepositoryService.find("emply_creiteria", {division:$scope.definition.division.id,jobTitle:$scope.definition.jobTitle.id}, 'simple').then(function (empData) {
                $scope.employeeList = empData.data;
            })
        }
    }

    $scope.uploadEmployees=function(files){
        if(files==null) {
            alert("Select file for upload.");
        }else{
            var file = files[0];
            var re = /(?:\.([^.]+))?$/;
            var ext = re.exec(file.name)[1];

            if(ext==='csv'){
                var fd = new FormData();
                fd.append("file", file);
                return $http({
                    method: 'POST',
                    data: fd,
                    url: restUrl + "/miscellaneous/miscellaneousService/uploadEmployees/"+ext,
                    transformRequest: angular.identity,
                    headers: {
                        "content-type": "multipart/form-data",
                        "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                    }
                }).then(function (data) {
                    for(d in data.data.successList){
                        $scope.uploadResult.successList.push(data.data.successList[d])
                    }
                    for(d in data.data.failureList){
                        $scope.uploadResult.failureList.push(data.data.failureList[d])
                    }
                    $scope.uploadResult.successCount=parseInt($scope.uploadResult.successCount)+parseInt(data.data.successCount);
                    $scope.uploadResult.failCount=parseInt($scope.uploadResult.failCount)+parseInt(data.data.failCount);
                })
            }else{
                alert("Select csv format file for upload.");
            }
        }
    }

    $scope.addCriteria=function(){
        var criteria={};
        criteria.division={}
        criteria.location={}
        criteria.jobTitle={}
        criteria.employee={}
        if($scope.definition.division!=null) {
            for (d in $scope.divisions) {
                if ($scope.divisions[d].id == $scope.definition.division.id) {
                    criteria.division={id:$scope.definition.division.id,name:$scope.divisions[d].name}
                }
            }
        }
        if($scope.definition.location!=null) {
            for (d in $scope.locations) {
                if ($scope.locations[d].id == $scope.definition.location.id) {
                    criteria.location={id:$scope.definition.location.id,name:$scope.locations[d].name}
                }
            }
        }

        if($scope.definition.jobTitle!=null) {
            for (d in $scope.jobTitles) {
                if ($scope.jobTitles[d].id == $scope.definition.jobTitle.id) {
                    criteria.jobTitle={id:$scope.definition.jobTitle.id,name:$scope.jobTitles[d].name}
                }
            }
        }

        if($scope.definition.employee!=null) {
            for (d in $scope.employeeList) {
                if ($scope.employeeList[d].id == $scope.definition.employee.id) {
                    criteria.employee={id:$scope.definition.employee.id,name:$scope.employeeList[d].name}
                }
            }
        }
        $scope.addedCriteria.push(criteria);
    }

    $scope.save=function(){
        var announcement={};
        var ownersList=[];
        for(d in $scope.addedCriteria){
            var owners={};
            var criteria=$scope.addedCriteria[d];

            if(criteria.division!=null && criteria.division.id!=null){
                owners.division={id:criteria.division.id}
            }else{
                owners.division=null
            }

            if(criteria.location!=null && criteria.location.id!=null){
                owners.location={id:criteria.location.id}
            }else{
                owners.location=null
            }

            if(criteria.jobTitle!=null && criteria.jobTitle.id!=null){
                owners.jobTitle={id:criteria.jobTitle.id}
            }else{
                owners.jobTitle=null
            }

            if(criteria.employee!=null && criteria.employee.id!=null){
                owners.employee={id:criteria.employee.id}
            }else{
                owners.employee=null
            }
            owners.type="ADDED"
            ownersList.push(owners);
        }
        for(d in $scope.uploadResult.successList){
            var owner={};
            var criteria=$scope.uploadResult.successList[d];
            owner.division=null;
            owner.location=null;
            owner.jobTitle=null;
            owner.employee={id:criteria.id}
            owner.type="UPLOADED";
            ownersList.push(owner);
        }


        if($scope.announcementId!=null){
            announcement.id=$scope.announcementId;
        }
        announcement.title=$scope.definition.title;
        announcement.message=$scope.htmlContent;
        announcement.activeFrom={text:$scope.definition.activeFrom};
        announcement.activeTo={text:$scope.definition.activeTo};
        announcement.owners=ownersList;

        var cnt=0;
        if(announcement.title==null || !announcement.title.trim()){
            alert("Please add title to announcement");
            cnt++;
        }else if(announcement.message==null || !announcement.message.trim()){
            alert("Please add template to announcement");
            cnt++;
        }else if(announcement.activeFrom.text==null || announcement.activeTo.text==null || !announcement.activeFrom.text.trim() || !announcement.activeTo.text.trim() ){
            alert("Please add proper dates for announcement")
            cnt++;
        }else if(announcement.owners==null || announcement.owners.length<=0){
            alert("Please add criteria for announcement")
            cnt++;
        }
        console.log(announcement)
        if(cnt==0) {
            RepositoryService.save(announcement, 'ANSMT').success(function (data) {
            });
        }
    }


    $scope.showSuccessEmp=function(){

        var modalInstance = $modal.open({
            templateUrl: 'successEmp.html',
            controller: 'successEmpController',
            size: 'xs',
            backdrop: false,
            backdropClick: false,
            dialogFade: true,
            keyboard: false,
            resolve: {
                uploadResult: function () {
                    return $scope.uploadResult;
                }
            }
        })
    }



    $scope.showFailedEmp=function(){

        var modalInstance = $modal.open({
            templateUrl: 'failEmp.html',
            controller: 'failEmpEmpController',
            size: 'xs',
            backdrop: false,
            backdropClick: false,
            dialogFade: true,
            keyboard: false,
            resolve: {
                input: function () {
                    return $scope.uploadResult.failureList;
                }
            }
        })
    }

    $scope.removeCriteria=function(index){
        $scope.addedCriteria.splice(index,1);
    }

    $scope.browse=function(){
        var modalInstance = $modal.open({
            templateUrl: 'gallery.html',
            controller: 'galleryController',
            size: 'lg',
            backdrop: false,
            backdropClick: false,
            dialogFade: true,
            keyboard: false
        })
    }
})

sfe.controller('galleryController', function ($scope, $http,LabelService, RepositoryService, LocalStore, $modalInstance) {

    $scope.fileList={};
    $scope.init=function(){
            return $http({
                method: 'GET',
                url: restUrl + "/miscellaneous/miscellaneousService/getimages",
                transformRequest: angular.identity,
                headers: {
                    "content-type": "multipart/form-data",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            }).then(function (data) {
                $scope.fileList=data.data;
            });
        }

        $scope.closeModal = function(){
            $modalInstance.dismiss('cancel');
        }
    })


sfe.controller('successEmpController',
    function ($scope, $http,LabelService, RepositoryService, LocalStore, $modalInstance,daysOfWeek,months,uploadResult) {
        $scope.emps=uploadResult.successList;
        $scope.count=uploadResult.successCount;
        $scope.init=function(){}

        $scope.removeEmployee=function(index){
            $scope.emps.splice(index,1);
            $scope.count=parseInt($scope.count)-1;
        }

        $scope.closeModal = function(){
            $modalInstance.dismiss('cancel');
        }
 })

sfe.controller('failEmpEmpController',
    function ($scope, $http,LabelService, RepositoryService, LocalStore, $modalInstance,daysOfWeek,months,input) {
        $scope.emps=input;
        $scope.init=function(){}

        $scope.closeModal = function(){
            $modalInstance.dismiss('cancel');
        }
    })


sfe.controller('AnnouncementSearchController',function($scope, $state, $stateParams, LocalStore,
                                                       RepositoryService,$modal,SearchFormService, SearchConfigService, LabelService,
                                                       LovService,commonSearchService){
    $scope.labels = {};
    $scope.links = {};
    $scope.stdFormData = {};
    $scope.stdFormTemplate = {};
    $scope.formFields = {};
    $scope.divisionList={};
    $scope.locationList={};
    $scope.employeeList={};

    $scope.init=function(){
        var data = $state.current.data;
        $scope.definition = {};
        $scope.links = LabelService.getLabels("links");
        $scope.labels = LabelService.getLabels(data.labels);
        $scope.formFields = SearchFormService.getForm($stateParams.context, data.query, data.labels);

        RepositoryService.find("divsn",{},'simple').then(function (data) {
            $scope.divisionsList = data.data;
        });

        RepositoryService.find('jobtt', {}, 'simple').then(function (data) {
            $scope.jobTitles = data.data;
        });
    };

    $scope.getLocation=function(){
        $scope.locationList={};
        $scope.employeeList={};
        RepositoryService.find("locat",{division_id:$scope.stdFormData['o.DIVISION_ID'],'sortBy':'name','sortOrder':'ASC'},'simple').then(function (data) {
            $scope.locationList = data.data;

            RepositoryService.find("emply_creiteria",{division:$scope.stdFormData['o.DIVISION_ID']},'simple').then(function(empData){
                $scope.employeeList=empData.data;
            })
        });
    };

    $scope.getEmployee=function(type){

        if(type==='location') {
            RepositoryService.find("emply_creiteria", {location: $scope.stdFormData['o.LOCATION_ID']}, 'simple').then(function (empData) {
                $scope.employeeList = empData.data;
            })
        }else{
            if($scope.stdFormData['o.DIVISION_ID']!=null && $scope.stdFormData['o.LOCATION_ID']==null)
                RepositoryService.find("emply_creiteria", {division:$scope.stdFormData['o.DIVISION_ID'],jobTitle:$scope.stdFormData['o.JOBTITLE_ID']}, 'simple').then(function (empData) {
                    $scope.employeeList = empData.data;
                })
        }
    };

    $scope.columns = [];
    $scope.actions = [];
    $scope.totalItems = 0;
    $scope.currentPage = 0;
    $scope.maxSize = 5;
    $scope.numPages = 20;
    $scope.showtable = false;
    $scope.nodata = false;

    $scope.search=function(){
        var data = $state.current.data;
        var name = $state.current.name;
        $scope.stdFormData.paged = 'Y';
        $scope.stdFormData.page=1;
        $scope.showtable = true;
        console.log($scope.stdFormData);
        console.log(name.substr(0, name.indexOf('.')));
        RepositoryService.search(data.entity.toLowerCase(),$scope.stdFormData, $stateParams.context, name.substr(0, name.indexOf('.')), $state.current.data.fixedQuery).then(function(data){
            if(data.data.totalRecords==0) {
                $scope.nodata = true;
                $scope.showtable = false;
            }else{
                $scope.nodata = false;
            }
            $scope.searchData = data.data.result;
            $scope.columns = data.data.columns;
            $scope.actions = data.data.actions;
            $scope.totalItems = data.data.totalRecords;
            $scope.currentPage = data.data.currentPage;
        });
    };

    $scope.pageChanged = function(){
        var data = $state.current.data;
        var name = $state.current.name;

        $scope.stdFormData.page = $scope.currentPage;
        $scope.stdFormData.paged = 'Y';
        RepositoryService.search(data.entity.toLowerCase(),$scope.stdFormData, $stateParams.context, name.substr(0, name.indexOf('.'))).then(function(response){
            $scope.searchData = response.data.result;
            $scope.columns = response.data.columns;
            $scope.actions = response.data.actions;
            $scope.totalItems = response.data.totalRecords;
            $scope.currentPage = response.data.currentPage;
        });
    }

    $scope.addNew=function(){
        $state.go("sysadmin.announcements");
    }
})