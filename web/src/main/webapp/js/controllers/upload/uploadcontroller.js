sfe.controller('CommonUploadController', function ($scope, $http,$state, $stateParams,
                                                   RepositoryService,SearchFormService, LabelService,
                                                   SearchConfigService, LocalStore) {

    $scope.labels = {};
    $scope.links = {};
    $scope.stdFormData = {};

    $scope.columns = [];
    $scope.actions = [];
    $scope.totalItems = 0;
    $scope.currentPage = 0;
    $scope.maxSize = 5;
    $scope.numPages = 20;
    $scope.showtable = false;
    $scope.nodata = false;
    $scope.type = {};
    $scope.types = [{"id":"roi_brand_therapy_upload","name":"ROI Brand Therapy"},
                    {"id":"roi_doctor_investment_upload","name":"ROI Doctor Investment"},
        {"id" : "upload_division_external_code", "name" : "Division upload"},
        {"id" : "upload_location_external_code", "name" : "Location Ext Upload"},
        {"id" : "upload_product_external_code", "name" : "Product extr code upload"},
        {"id" : "upload_sap_location_mapping", name: "sap location mapping upload"},
        {"id" : "upload_sap_location_contribution", name : "Sap location contribution upload"},
        { "id" : "upload_stockist_sales_data_upload", name : "sales upload"},
        {"id":"brand_names_code_upload","name":"Brand Upload"},
        {"id":"doctor_transfer_upload", "name":"Doctor Transfer"},
        {"id" : "sales_target_upload", "name" : "Sales Target Upload"},
        {"id" : "upload_location_wise_sales_data_upload", "name" : "Location wise sales upload"},
        {"id" : "doctor_input_master_upload", "name" : "Doctor Input Master"},
        {"id" : "invoice_details_upload", "name" : "Invoice Details Upload"},
        {"id":"stockist_upload","name":"Stockist Upload"}];


    $scope.initSearch = function(){
        //console.log("1");
        var data = $state.current.data;
        //console.log(data);
        $scope.definition = {};
        $scope.type=data.type;
        $scope.links = LabelService.getLabels("links");
        $scope.labels =  LabelService.getLabels(data.labels);
        $scope.search();
    }


    $scope.downloadFile = function(context, id, type){
        return $http({
            method: 'GET',
            url: restUrl + "/core/framework/download/" + context + "/" + type + "/" + id ,
            transformRequest:angular.identity,
            headers: {
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }

        }).success(function(data, status, headers, config) {
            var blob = new Blob([data], {
                type: "application/msvend-xls"
            });
            //saveAs provided by FileSaver.js
            saveAs(blob, id + '.csv');
        });
    }

    $scope.search=function(){
        var data = $state.current.data;
        var name = $state.current.name;
        $scope.stdFormData.paged = 'Y';
        $scope.stdFormData.page=1;
        $scope.showtable = true;
        RepositoryService.search(data.entity.toLowerCase(),$scope.stdFormData, $stateParams.context, name.substr(0, name.indexOf('.')), $state.current.data.fixedQuery).then(function(data){
            if(data.data.totalRecords==0) {
                $scope.nodata = true;
                $scope.showtable = false;
            }else{
                $scope.nodata = false;
            }
            $scope.searchData = data.data.result;
            $scope.columns = data.data.columns;
            $scope.actions = data.data.actions;
            $scope.totalItems = data.data.totalRecords;
            $scope.currentPage = data.data.currentPage;
        });
    }

    $scope.pageChanged = function(){
        var data = $state.current.data;
        var name = $state.current.name;

        $scope.stdFormData.page = $scope.currentPage;
        $scope.stdFormData.paged = 'Y';
        RepositoryService.search(data.entity.toLowerCase(),$scope.stdFormData, $stateParams.context, name.substr(0, name.indexOf('.'))).then(function(response){
            $scope.searchData = response.data.result;
            $scope.columns = response.data.columns;
            $scope.actions = response.data.actions;
            $scope.totalItems = response.data.totalRecords;
            $scope.currentPage = response.data.currentPage;
        });
    }

    $scope.uploadFile = function(path){

        var file = $scope.files[0];
        var re = /(?:\.([^.]+))?$/;
        var ext = re.exec(file.name)[1];
        var fd = new FormData();

        fd.append("file", file);

        return $http({
            method: 'POST',
            data: fd,
            url: restUrl + "/core/framework/upload/upload/" + $scope.type.id + "/" + encodeURI(file.name) ,
            transformRequest: angular.identity,

            headers: {
                "content-type":"multipart/form-data",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }

        }).then(function(response) {
           fd.file = null;
           $scope.files[0] = null;
           $scope.search();
        });
    };

});