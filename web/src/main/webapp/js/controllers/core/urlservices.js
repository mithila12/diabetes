(function () {
  'use strict';

  angular
    .module('app.core')
    .service('urlservice', urlService);

  /* @ngInject */
  function urlService(urlConfig) {
    var service = {
      formUrl: formUrl,
      formSlugUrl: formSlugUrl,
      loginUrl: loginUrl,
      selectUrl: selectUrl,
      fetchUrl: fetchUrl
    };

    return service;

    function formUrl(uri, params) {
      var querystring = "";
      if (params) {
        querystring = "?";
        var numKeys = Object.keys(params);
        for (var i = 0; i < numKeys.length; i++) {
          var key = numKeys[i];
          var value = params[key];
          querystring = querystring + key + "=" + escape(value) + "&";
        }
      }
      console.log('http://' + urlConfig.app_url + '/' + uri + querystring);
      return 'http://' + urlConfig.app_url + '/' + uri + querystring;
      //http://dataqube.in/wdm-pm-api/user-info
     }

    function fetchUrl(params, prefix) {
          var querystring = "";
          if (params) {
            querystring = "?";
            var numKeys = Object.keys(params);
            for (var i = 0; i < numKeys.length; i++) {
              var key = numKeys[i];
              var value = params[key];
              querystring = querystring + key + "=" + escape(value) + "&";
            }
          }
          console.log('http://' + urlConfig.app_url + '/' + prefix + '/search' + querystring);
          var url= 'http://' + urlConfig.app_url + '/' + prefix + '/search' + querystring;
          url = url.slice(0,-1);
          console.log(url);
          return url;
          //http://dataqube.in/wdm-pm-api/user-info
          //http://localhost:8080/webapp/rest/v1.0/core/framework/patnt/search?searchParams=WD_PATIENT.DOCTOR_ID~DOCTR0000001eadcd380158b51eaf85008000&context=sysadmin
 //http://localhost:8080/webapp/rest/v1.0/core/framework/patients_threshold/find?searchParams=PATIENT_ID~PATNT0000004847b378015920efbfd6008000^REPORT_TYPE~nephro_report_type^context~sysadmin
        }

        function selectUrl(params, prefix) {
          var querystring = "";
          if (params) {
            querystring = "?";
            var numKeys = Object.keys(params);
            for (var i = 0; i < numKeys.length; i++) {
              var key = numKeys[i];
              var value = params[key];
              querystring = querystring + key + "=" + escape(value) + "&";
            }
          }
          console.log('http://' + urlConfig.app_url + '/' + prefix + '/find' + querystring);
          var url= 'http://' + urlConfig.app_url + '/' + prefix + '/find' + querystring;
          url = url.slice(0,-1);
          console.log(url);
          return url;
          //http://dataqube.in/wdm-pm-api/user-info
          //http://localhost:8080/webapp/rest/v1.0/core/framework/patnt/search?searchParams=WD_PATIENT.DOCTOR_ID~DOCTR0000001eadcd380158b51eaf85008000&context=sysadmin
 //http://localhost:8080/webapp/rest/v1.0/core/framework/patients_threshold/find?searchParams=PATIENT_ID~PATNT0000004847b378015920efbfd6008000^REPORT_TYPE~nephro_report_type^context~sysadmin
        }

    function loginUrl(uri, params) {
      var querystring = "";
      if (params) {
        querystring = "?";
        var numKeys = Object.keys(params);
        for (var i = 0; i < numKeys.length; i++) {
          var key = numKeys[i];
          var value = params[key];
          querystring = querystring + key + "=" + escape(value) + "&";
        }
      }
      return 'http://' + urlConfig.login_url;
      //http://dataqube.in/wdm-pm-api/user-info
    }

    function formSlugUrl(uri, params) {
      var querystring = "";
      if (params) {
        //querystring = "&";

        var numKeys = Object.keys(params);
        for (var i = 0; i < numKeys.length; i++) {
          var key = numKeys[i];
          var value = params[key];
          querystring = querystring + key + "=" + escape(value);
          console.log(querystring);
        }
      }
      console.log('http://' + urlConfig.admin_url + '/?' + uri + querystring)
      return 'http://' + urlConfig.admin_url + '/?' + uri + querystring;
    }
  }

})();
