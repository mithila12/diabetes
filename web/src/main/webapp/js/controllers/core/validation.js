(function () {

  angular
    .module('app.core')
    .factory('sameValidator', sameValidator)
    .config(validations, sameValidator)

  function sameValidator() {
    return {
      name: 'password',
      validate: function (value, args) {

        return value === document.querySelector('[name="' + args.as + '"]').value;
      }
    };
  };

  /* @ngInject */
  function validations(valdrProvider) {

    valdrProvider.addValidator('sameValidator');
    valdrProvider.addConstraints({
    'Login' : {
        'username':{
          'required' :{
            'message' : 'Username is required.'
          }
        },
        'password':{
          'required':{
            'message' : 'Password is required.'
          }
        },

      },
      'editProfile': {
        'firstName': {
          'size': {
            'min': 3,
            'max': 20,
            'message': 'First name is required to be between 3 and 20 characters.'
          },
          'required': {
            'message': 'First name is required.'
          }
        },
        'lastName': {
          'size': {
            'min': 3,
            'max': 20,
            'message': 'Last name is required to be between 3 and 20 characters.'
          },
          'required': {
            'message': 'Last name is required.'
          }
        },
        'gender': {
          'required': {
            'message': 'Please select Gender.'
          }
        },
        'email': {
          'email': {
            'message': 'Not a valid email address'
          },
          'required': {
            'message': 'Email is required.'
          }
        },
        'contact_no': {
          'min': {
            'value': 1000000000,
            'message': 'Contact no should be exactly 10 digits.'
          },
          'max': {
            'value': 9999999999,
            'message': 'Contact no should be exactly 10 digits.'
          },
          'required': {
            'message': 'Contact no is required.'
          }
        },
        'contact_emergency': {
          'min': {
            'value': 1000000000,
            'message': 'Emergency Contact no should be exactly 10 digits.'
          },
          'max': {
            'value': 9999999999,
            'message': 'Emergency Contact no should be exactly 10 digits.'
          },
          'required': {
            'message': 'Emergency Contact no is required.'
          }
        },
        'degrees': {
          'required': {
            'message': 'Degrees are required.'
          }
        }
      }
    });
  }
})();
