(function () {
  'use strict';
  angular
    .module('app.core')
    .constant('errorCodes', {
      "no_network": 200,
      "need_online": "You need to be connected to the network to access this information"
    })
    .constant('urlConfig', {
      //  "app_url": "dataqube.in/wdm-pm-api",
      //"app_url": "squer.mirealux.com/wdm-pm-api",
      "login_url":"/webapp/rest/v1.0/core/login",
      "app_url":"/webapp/rest/v1.0/core/framework",//"192.168.1.10:8080/webapp/rest/v1.0/core/framework",
      "app1_url1": "test1.medstudio.in/wdm-pm-api",
      "admin_url": "publisher.medstudio.in/api"//Knowledge
    })
    .constant('appUrls', {
      login: {
        login: 'login'
      },
      doctor:{
        saveDoctor: 'DOCTR'
      },
      patient:{
        savePatient:'PATNT'
      },
      message:{
        sendMessage:'MESSG'
      },
      threshold:{
        setThreshold:'THRSH'
      },
      /*profile: {
        getInfo: 'user-info',
        update: 'update-profile'
      },
      patients: {
        getPatients: 'get-patients',
        getReports: 'get-report',
        getAppointments: 'get-appointment',
        getThreshold: 'threshold-info',
        updateThreshold: 'update-threshold'
      },*/
      enrollPatients: {
        userName: 'pralok@medstudio.in',
        hash: "d7876caf2d1eacf8d4664b54e435076ed41e4781",
        sender: 'TXTLCL',
        getEnrollPatients: 'http://api.textlocal.in/send'
      },
      knowledgeResource: {
        knowledge: 'json=get_category_posts&'
      },
      /*Message: {
        sentMessage: 'add-notification',
        getMessage: 'notifications-for-patients',
//  getMessage: 'get-patients',
      },
      Notification: {
        getNotification: 'patient-registration-details'
      }*/
    })
    .value("sessionConstant", {
      login: {},
      currentPatient: {},
      list: {},
      myProfile: {},
      notification: {},
      knowledgeResource: {
        detailContent: ""
      },
      knowledge: 'json=get_category_posts&',
      reportType: {
          GFR: {},
          UrineAlbumin: {},
          BloodUrea: {},
          Serum: {},
          Cholesterol: {},
          HDL: {},
          Triglycerides: {},
          vldl: {},
          LDL: {},
          BloodPressure: {},
          randomSugar: {},
          postmeal: {},
          fastingsugar: {},
          hba1c: {}
        }
    })
    .constant('dbconfig', {
      name: 'dip.db',
      tables: [
        {
          name: 'APP_CONFIG',
          columns: [
            {name: 'id', type: 'integer primary key'},
            {name: 'reference_id', type: 'text'},
            {name: 'loginId', type:'text'}
          ]
        },
        {
          name: "ENROLLMENT_MESSAGES",
          columns: [
            {name: "id", type: "integer primary key"},
            {name: "template", type: "text"},
            {name: "date", type: "datetime"},
            {name: "number", type: "text"},
            {name: "status", type: "text"},
          ]
        },
        {
          name: "PATIENT_MESSAGES",
          columns: [
            {name: "id", type: "integer primary key"},
            {name: "content", type: "text"},
            {name: "date", type: "datetime"},
            {name: "status", type: "text"},
          ]
        }, {
          name: "NOTIFICATION",
          columns: [
            {name: "id", type: "integer primary key"},
            {name: "count", type: "text"},
            {name: "date", type: "text"},
          ]
        },
        {
          name: "WELCOME",
          columns: [
            {name: "id", type: "integer primary key"},
            {name: "show", type: "text"}
          ]
        }
      ]
    })
})
()
