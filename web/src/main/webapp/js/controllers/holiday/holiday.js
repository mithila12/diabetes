sfe.controller('HolidayController', function ($scope, $http, $rootScope, $compile, $state,$stateParams, RepositoryService,
                                              RepositoryService, SearchFormService, LabelService,LocalStore) {
    $scope.definition = {};
    $scope.links = LabelService.getLabels("links");
    $scope.labels = LabelService.getLabels("holiday");
    $scope.stateList={};
    $scope.holidayList={};
    $scope.addnew=0;
    $scope.prvEditId="";

    $scope.init=function(){
        if($stateParams.id!=null){
            var response = RepositoryService.restore($stateParams.id);
            response.then(function(data){
                $scope.definition=data.data;
            })
        }
    };

    $scope.edit=function(data)
    {
        if($scope.prvEditId!=null)
        {
            hidePrvEdit($scope.prvEditId);
        }


        $scope.prvEditId=data.id;
        //holiday
        $('#'+data.id+"_label").hide();
        $('#'+data.id+"_text").attr('type','text');
        $("#"+data.id+"_active").removeAttr("disabled");
        $("#"+data.id+"_edit").hide();
        $("#"+data.id+"_save").css({display:'block'});

        $("#lastTr").css({display:'none'});
    };

    $scope.update = function(id){

        var response = RepositoryService.restore(id);
        response.then(function(data){
            $scope.definition = data.data;
            $scope.definition.name=$("#"+id+"_text").val();

            if($("#"+id+"_active").is(':checked')){
                $scope.definition.active=true;
            }
            else{
                $scope.definition.active=false;
            }
            RepositoryService.save($scope.definition, 'HOLYD');
            $("#"+id+"_label").text($scope.definition.name);
            hidePrvEdit($scope.definition.id);
            $("#lastTr").css({display:'table-row'});
        });
    };

    $scope.save=function(){
        $scope.searchData.push(angular.copy($scope.definition));

        RepositoryService.save($scope.definition,'HOLYD');
        $scope.definition={};
    };

    function hidePrvEdit(prvEditId)
    {
        $('#'+prvEditId+"_label").show();
        $('#'+prvEditId+"_text").attr('type','hidden');
        $("#"+prvEditId+"_active").attr("disabled", true);
        $("#"+prvEditId+"_edit").show();
        $("#"+prvEditId+"_save").css({display:'none'})
    };
});

sfe.controller('HolidayPlanerController', function ($scope, $http, $rootScope, $compile, $state,$stateParams, RepositoryService,
                                                    SearchFormService, HolidayService, LabelService,LocalStore) {

    $scope.yearList=new Array();
    $scope.plannerDefinition={};
    $scope.plannerDefinition.holiday={};
    $scope.plannerDefinition.state={};
    $scope.plannerDefinition.date={};
    $scope.displayData={};
    $scope.holidayList=[];

    $scope.planer=function(){
        RepositoryService.find("state",{'sortBy':'NAME','sortOrder':'ASC'},"simple").then(function (data) {
            $scope.stateList = data.data;
        });
        RepositoryService.find("holyd",{},"simple").then(function (data) {
            $scope.holidayList = data.data;
        });

        var d = new Date();
        var yr = d.getFullYear();
        var i=0;

        $scope.yearList.push(yr-3);
        $scope.yearList.push(yr-2);
        $scope.yearList.push(yr-1);
        $scope.yearList.push(yr);
        $scope.yearList.push(yr+1);
        $scope.yearList.push(yr+2);
        $scope.yearList.push(yr+3);

        $scope.currentYr=yr;
        $scope.getHolidays(yr);


    };

    $scope.getHolidays=function(year)
    {
        HolidayService.getHolidays(year).then(function(response){
            $scope.displayData=response.data;
        });
    };

    $scope.updateHoliday=function(sid,sname,hid,hname,date,isAll){

        if($("#"+sid+"_"+hid+"_check").is(':checked'))
        {
            var arr=date.split("/");
            var d = new Date(arr[2],arr[1]-1,arr[0]);
            $scope.plannerDefinition.holiday.id=hid;
            $scope.plannerDefinition.holiday.displayName=hname;
            $scope.plannerDefinition.state.id=sid;
            $scope.plannerDefinition.state.displayName=sname;
            $scope.plannerDefinition.date.text= date;
            console.log($scope.plannerDefinition);
            RepositoryService.save($scope.plannerDefinition,'YRHLD');
        }
        else
        {
            if(isAll)
            {
                $("#"+hid+"_checkAll").prop('checked',false);
            }
            HolidayService.deleteHoliday(sid,hid);
        }
    };

    $scope.updateAllState=function(hid,name,date)
    {
        if($("#"+hid+"_checkAll").is(':checked'))
        {
            $scope.holidayList=[];
            changeCheckBocStatus(hid,"true");
            deleteHolidays(hid);
            for(s in $scope.stateList)
            {
                $scope.plannerDefinition = {};
                $scope.plannerDefinition.holiday={};
                $scope.plannerDefinition.state={};
                $scope.plannerDefinition.date={};
                var arr=date.split("/");
                var d = new Date(arr[2],arr[1]-1,arr[0]);
                $scope.plannerDefinition.holiday.id=hid;
                $scope.plannerDefinition.holiday.displayName=name;
                $scope.plannerDefinition.state.id= $scope.stateList[s].id;
                $scope.plannerDefinition.state.displayName= $scope.stateList[s].name;
                $scope.plannerDefinition.date.text= date;
                $scope.holidayList.push($scope.plannerDefinition);
            }
            HolidayService.configureForAllState($scope.holidayList);
        }
        else
        {
            changeCheckBocStatus(hid,"false");
            deleteHolidays(hid);
        }
    }

    function changeCheckBocStatus(hid,status){
        if(status=='true') {
            for (s in $scope.stateList) {
                $("#"+ $scope.stateList[s].id+"_"+hid+"_check").prop('checked',true);
            }
        }
        else
        {
            for (s in $scope.stateList) {
                $("#"+ $scope.stateList[s].id+"_"+hid+"_check").prop('checked',false);
            }
        }
    }

    function deleteHolidays(hid){
        HolidayService.deleteHolidays(hid);
    }

    $scope.changeDate = function(holiday,date) {
        HolidayService.updateHoliday(date,holiday);
    }
});

sfe.controller('WeekendConfigurationController', function ($scope, $http, $rootScope, $compile, $state,$stateParams, RepositoryService,
                                              RepositoryService, HolidayService, SearchFormService, LabelService,LocalStore, daysOfWeek,daysOfWeekToIndex) {

    $scope.stateList = [];
    $scope.weeklyConfigList=[];
    $scope.day=[];
    $scope.selectedDay=[];
    $scope.days=daysOfWeek;
    $scope.weeklyConfig={};
    $scope.config={};
    $scope.groupedWeekend=[];

    $scope.init = function(){
        $scope.config = {};
        RepositoryService.find("state",{'sortBy':'NAME','sortOrder':'ASC'},"simple").then(function (data) {
            $scope.stateList = data.data;
            RepositoryService.find("state_group_by_weekend",{},"simple").then(function(data){
                $scope.groupedWeekend = data;
                for(var i=0; i<data.data.length;i++){
                    if(data.data[i].count == $scope.stateList.length)
                    {
                        $scope.day[daysOfWeek[data.data[i].WEEKEND-1]] = true;
                    }
                }
            });
        });

        HolidayService.fetchWeekendConfig().success(function(data){
            for(var i=0 ; i<data.length;i++){
                var stateId = data[i].state.id;
                var dayName = daysOfWeek[data[i].weekend-1];
                $scope.config[stateId+dayName] = true;
            }
        });

    }

    $scope.makeWeekendConfigration=function(val){

        $scope.weeklyConfigList=[];
        var arr = val.split("-");
        $scope.weeklyConfig.weekend = daysOfWeekToIndex[arr[0]];

        if($("#"+arr[0]+arr[1]).is(':checked')){
            $scope.weeklyConfig.state = {"id":arr[1]};
            $scope.weeklyConfigList.push($scope.weeklyConfig);
            HolidayService.configureWeekend($scope.weeklyConfigList);
        } else {
            $scope.weeklyConfig.state = arr[1];
            $scope.weeklyConfigList.push($scope.weeklyConfig);
            HolidayService.deleteweekendconfig($scope.weeklyConfigList);
        }
    }

    $scope.makeWeekendConfigrationForAllState=function(d){
        $scope.weeklyConfigList=[];
        $("."+d).prop('checked',$scope.day[d]);
        HolidayService.deleteAllWeekendConfigForDay(daysOfWeekToIndex[d]);

        if($("#"+d).is(':checked')){
            for(var i=0;i<$scope.stateList.length;i++){
                $scope.weeklyConfig={};
                $scope.weeklyConfig.weekend = daysOfWeekToIndex[d];
                $scope.weeklyConfig.state = {"id":$scope.stateList[i].id};
                $scope.weeklyConfigList.push($scope.weeklyConfig);
            }
            HolidayService.configureWeekend($scope.weeklyConfigList);
        }
    }
});
