sfe.filter('sumOfValues',function(){
    return function(input){
        if(!input)
            return 0;
        var sum=0;
        for(i in input) {
            sum=sum+(getNestedPropertyByKey(input[i],arguments[1]))
        }
        return sum;
    }

    function getNestedPropertyByKey(data, key) {
        if(key=='plannedActivityType'){
            data = data['plannedActivityType'];
            if(data)
                data = data.id;
            if(data=='SYSLVFACT0000000000000000000000000001')
                return 1;
            else
                return 0;
        }else{
            data = data[key];
            return data;
        }

    }
});

sfe.controller('DisplayHolidayController',function ($scope, $http, $rootScope, $compile, $state, $stateParams,
                                                    RepositoryService, LocalStore, month,year) {

    $scope.holidayList = [];
    $scope.init = function() {
        RepositoryService.find("statewise_monthly_holiday",{"employeeId": LocalStore.fetch('headerdata').id,"month":month , "year":year,paged:'N'},'advanced').then(function(data){
            $scope.holidayList=data.data;
        });
    }
});

sfe.controller("NewPlanController", function ($scope, $http, $rootScope, $compile, $state, $stateParams,
                                              RepositoryService, SearchFormService, LocalStore, monthNameToIndexJS, CalendarService,
                                              $modal, $log, months, daysOfWeek, TourPlanService) {

    $scope.temp ={};
    $scope.referenceData = {};
    $scope.temp.showPlan = false;
    $scope.formdata = {};
    $scope.tourdata = {};
    $scope.plan = {};
    $scope.temp.showPrevious=false;
    $scope.temp.showMoreDetails = false;
    $scope.plan.showLocationType = new Array();
    $scope.temp.showFields = new Array();
    $scope.newplan = false;
    $scope.activityMasterListMap={};
    $scope.controls = {showAlert:false, message:''};
    $scope.holidayMaster = {};
    $scope.holidays=[];
    $scope.showRejectComment= false;


    $scope.init = function() {


        RepositoryService.getCurrentDate().success(function(cDate){
            $scope.referenceData.currentDate=cDate;

            console.log(moment().utcOffset());
            $scope.temp.showPlan = false;

            RepositoryService.find("activity",{locationId: LocalStore.fetch('headerdata').location.id, paged:'N'},'advanced').success(function(data){
                $scope.referenceData.activityMasterList = data;
                for(var i=0;i<$scope.referenceData.activityMasterList.length;i++ ){
                    $scope.activityMasterListMap[$scope.referenceData.activityMasterList[i].id] = $scope.referenceData.activityMasterList[i];
                }
            });
            RepositoryService.find("usrlv",{type: 'location_type', paged:'N'},'advanced').then(function(data){
                $scope.referenceData.locationTypeMasterList = data.data;
            });
            TourPlanService.getMyCustomers(LocalStore.fetch('headerdata').id).then(function(data){
                $scope.referenceData.beatCustomers = data.data;
            });

            var month = monthNameToIndexJS[$state.params.month];
            var year = parseInt($state.params.year);
            var ownerId = LocalStore.fetch("USER_INFO").id;

            /*RepositoryService.find("yrhld",{"state": 'STATE00000000000000000000000000000027',"month":month , paged:'N'},'advanced').then(function(data){
                $scope.holidays = data.data;
                for(var i=0; i<$scope.holidays.length; i++){
                    $scope.holidayMaster[$scope.holidays[i].date.text] =  $scope.holidays[i].holiday.displayName;
                }
            });*/

            /*var date = new Date(year, month, 1);
            console.log(date);*/
            $scope.referenceData.months = months;
            $scope.referenceData.years = [year-1, year, year+1];
            $scope.formdata.month= $state.params.month;
            $scope.formdata.year = year;

            TourPlanService.getDoctorProfiles('').success(function(beats){
                $scope.referenceData.beats = beats;
            })

            TourPlanService.getTourDetails(ownerId, month, year).success(function(plan){
                $scope.temp.editable = plan.status.id !='SYSLV40000000000000000000000000000006';

                if(!plan) {
                    $scope.temp.showSaveSubmit = true;
                    return;
                }

               RepositoryService.find("tourPlan_comment",{id:plan.id},"simple").success(function(data){
                   $scope.reject=data;
                   if($scope.reject.length>0){
                   $scope.showRejectComment = true;
                   }
                   console.log($scope.reject.length>0);
               })
                $scope.plan.tourPlan = plan;
                $scope.plan.showLocationType = {};
                $scope.temp.showSaveSubmit = (plan.status.id!='SYSLV40000000000000000000000000000007');

                $($scope.plan.tourPlan.activities).each(function(i){
                    var detail = $(this)[0];
                    if(detail.plannedActivityType){
                        if(detail.plannedActivityType.id !='SYSLVFACT0000000000000000000000000001' ){
                            $scope.plan.showLocationType[i] = false;
                        }else{
                            $scope.plan.showLocationType[i] = true;
                        }
                    }else{
                        $scope.plan.showLocationType[i] = false;
                    }

                });
            });
        });

    };
    $scope.init();

    $scope.getHolidayName = function(a){
        //console.log($scope.holidayMaster);
        //return $scope.holidayMaster;
    }

    $scope.getMonthlyHoliday = function(){
        var month = monthNameToIndexJS[$scope.formdata.month] +1;

        var modalInstance = $modal.open({
            templateUrl: 'holiday.html',
            controller: 'DisplayHolidayController',
            size: 'sm',
            resolve: {
                month:function(){
                    return month;
                },
                year:function(){
                    return $scope.formdata.year;
                }
            }
        });
    }

 $scope.rejectComment = function(){
        var rejectResult = $scope.reject;
        var modalInstance = $modal.open({
            templateUrl: 'rejectCommentDisplay.html',
            controller: 'DisplayRejectCommentController',
            size: 'lg',
            resolve: {
                rejectResult:function(){
                return rejectResult;
                }
            }
        });
    }

    $scope.isCurrentDate=function(detail){

        var today=moment($scope.referenceData.currentDate.text, "DD/MM/YYYY");
        var planDate=moment(detail.activityDate.text, "DD/MM/YYYY");

        var d1=new Date(today);
        var d2=new Date(planDate);
        var months;
        months = (d2.getFullYear() - d1.getFullYear()) * 12;
        months -= d1.getMonth() + 1;
        months += d2.getMonth();

        if(today.isAfter(planDate)){
            return true;
        } else if(months > 0) {
            return true;
        }

    }
    $scope.showBeatSelectionLink = function(detail){

        var today=moment($scope.referenceData.currentDate.text, "DD/MM/YYYY");
        var planDate=moment(detail.activityDate.text, "DD/MM/YYYY");

        if(today.isAfter(planDate))
            return false;

        if(!detail.plannedActivityType)
            return false;

        return (detail.plannedActivityType.id =='SYSLVFACT0000000000000000000000000001');
    }

    $scope.showBeatSelection = function(activityDate){
        var modalInstance = $modal.open({
            templateUrl: 'beatListing.html',
            controller: 'BeatsForTourController',
            size: 'lg',
            resolve: {
                detail: function(){
                    var index = -1;
                    $($scope.plan.tourPlan.activities).each(function(data){
                        var o = $(this)[0];
                        if(o.activityDate.date ==activityDate.date){
                            index = data;
                        }
                    });
                    return $scope.plan.tourPlan.activities[index];
                },
                plan:function(){
                    return $scope.plan.tourPlan
                },
                customers:function(){
                    return $scope.referenceData.beatCustomers;
                }
            }
        });

    }

    $scope.submitPlan = function(plan){
        TourPlanService.submitPlan($scope.plan.tourPlan,'submitted').then(function(d){
            $scope.init();
        });
    }

    $scope.toggleShowPrevious= function(){
        $scope.temp.showPrevious = !$scope.temp.showPrevious;
    }

    $scope.toggleShowMoreDetails= function(){
        $scope.temp.showMoreDetails = !$scope.temp.showMoreDetails;
    }

    $scope.copyActivity = function () {
        $scope.newplan = false;
        var ownerId = LocalStore.fetch("USER_INFO").id;
        var month = new Date().getMonth();
        var year = new Date().getFullYear();
        if($scope.formdata.month){
            month = monthNameToIndexJS[$scope.formdata.month] +1;
            RepositoryService.find("attendee_count",{"ownerId":ownerId, "month":month,"year":year},'simple').then(function(data){
                if(data.data[0]!=null)
                {
                    var doctors = data.data[0].num_doc;
                    var chemist = data.data[0].num_chemist;
                    if(doctors == 0 && chemist == 0) {
                        $scope.newplan = true;
                    }
                }

                if(new Date().getMonth()!=11) {
                    if (month < new Date().getMonth() + 1) {
                        alert("Can not plan for previous month");
                        return;
                    }
                }
                if(!$scope.newplan){
                    alert("Bulk copy option is only active when current month plan creation is not started");
                    return;
                }
                var modalInstance = $modal.open({
                    templateUrl: 'selectActivity.html',
                    controller: 'ActivityCopyController',
                    size: 'lg',
                    resolve: {
                        detail: function(){
                            return $scope.plan.tourPlan.activities;
                        },
                        masterActivities: function(){
                            return $scope.activityMasterListMap;
                        },
                        month: function(){
                            return month;
                        },
                        year: function(){
                            return $scope.formdata.year;
                        }
                    }
                })
                modalInstance.result.then(function (val) {
                    console.log(val);
                }, function () {
                    $scope.init();
                });
            });
        }else{
            $scope.formdata.month =months[month];
        }
    };

    $scope.viewSummaryPlan=function(){

        var month = new Date().getMonth();
        if($scope.formdata.month){
            month = monthNameToIndexJS[$scope.formdata.month] +1;
        }else{
            $scope.formdata.month =months[month];
        }

        var modalInstance = $modal.open({
            templateUrl: 'viewPlanSummary.html',
            controller: 'ViewPlanSummaryController',
            size: 'lg',
            resolve: {
                detail: function(){
                    return;
                },
                month: function(){
                    return month;
                },
                year:function(){
                    return $scope.formdata.year;
                }
            }
        });

    }

    $scope.openDoctor = function (activateDate,numDoctors) {
        var modalInstance = $modal.open({
            templateUrl: 'doctorListing.html',
            controller: 'DoctorForTourController',
            size: 'lg',
            resolve: {
                detail: function(){
                    var index = -1;
                    $($scope.plan.tourPlan.activities).each(function(data){
                        var o = $(this)[0];
                        if(o.activityDate.date ==activateDate.date){
                            index = data;
                        }
                    });
                    return $scope.plan.tourPlan.activities[index];
                },
                beats: function(){
                    return $scope.referenceData.beats;
                },
                editable:function(){
                    var today=moment($scope.referenceData.currentDate.text, "DD/MM/YYYY");
                    var planDate=moment.utc(activateDate.date);

                    if(today.isAfter(planDate))
                        return true;

                    return $scope.temp.editable;
                }
            }
        });
    };


    $scope.selectActivity = function(d){
        var details = $scope.plan.tourPlan.activities;
        var index = -1;
        $(details).each(function(data){
            var o = $(this)[0];
            if(o.activityDate.date == d){
                index = data;
            }
        });
        RepositoryService.update(details[index], "ACTDT").success(function(d){
        });
    }

    $scope.openChemist = function (d, numChemist) {
        if(numChemist==0)
            return;
        var modalInstance = $modal.open({
            templateUrl: 'chemistListing.html',
            controller: 'ChemistForTourController',
            size: 'lg',
            resolve: {
                detail: function(){
                    var index = -1;
                    $($scope.plan.tourPlan.activities).each(function(data){
                        var o = $(this)[0];
                        if(o.activityDate.date ==d){
                            index = data;
                        }
                    });
                    return $scope.plan.tourPlan.activities[index];
                },
                plan:function(){
                    return $scope.plan.tourPlan
                }
            }
        });
    };

    $scope.editCreatePlan = function(){
        var index = monthNameToIndexJS[$scope.formdata.month];

        var fromDate = new Date($scope.formdata.year, index, 1);
        var toDate = new Date($scope.formdata.year, index, new Date($scope.formdata.year, index +1, 0).getDate());
        var month = fromDate.getMonth() + 1;
        $state.go('home.new_tourplan',{month: $scope.formdata.month,
            year: $scope.formdata.year});
    }

});

    sfe.controller('DisplayRejectCommentController', function ($scope, $modalInstance, LabelService, TourPlanService, RepositoryService, LocalStore, $http,rejectResult) {
    $scope.reject= rejectResult;
console.log(rejectResult)
    $scope.close=function(){
            $modalInstance.dismiss('cancel');
        }
    });

sfe.controller('ActivityCopyController', function ($scope, $modalInstance, LabelService, TourPlanService, RepositoryService,months,
                                                      detail, month, year, masterActivities, monthNameToIndexJS, LocalStore, $http) {

    $scope.selectedMonth = month;
    $scope.activity={};
    $scope.masterActivities = masterActivities;
    $scope.months = months;
    var year = new Date().getFullYear();
    $scope.years = [year-1, year];
    $scope.showPlan = false;
    $scope.month="";
    $scope.year=year;

    $scope.validateMonth = function(){
        if($scope.activity.date!=null){
            alert("Please reset all current activities first");
            return;
        }
    }

    $scope.getPlan = function() {
        var ownerId = LocalStore.fetch("USER_INFO").id;
        var month;
        if($scope.month){
            month = monthNameToIndexJS[$scope.month];
        }else{
            $scope.month =months[month];
        }

        if($scope.year > new Date().getFullYear() || ($scope.year==new Date().getFullYear() && month > new Date().getMonth())){
            alert("You can copy plan of previous months only!");
            return;
        }

        TourPlanService.getTourDetails(ownerId, month, $scope.year).success(function(plan){
            $scope.details = plan.activities;
            $scope.showPlan = true;
        });
    }

    $scope.copyTourPlan = function (){
        var list = [];
        var copyActivityDetail ={};
        var tempList = [];
        for(var i=0 ; i<$scope.details.length; i++){
            if($scope.activity.date[$scope.details[i].id]!=null){
                copyActivityDetail ={};
                copyActivityDetail["activityId"] = $scope.details[i].id;
                var date = $scope.activity.date[$scope.details[i].id].split("/");
                var fromDate = new Date(date[2], date[1], date[0]);
                var month = fromDate.getMonth()+1;
                var forDate =  date[2]+ '-' + date[1] + '-' + date[0];
                if((tempList.indexOf(forDate) > -1)) {
                    alert("You have selected same date more than once. Please go back and select a date only once");
                    return;
                }
                tempList.push(forDate);
                copyActivityDetail["date"] = forDate;
                list.push(copyActivityDetail);
            }
        }
        var flag = confirm("Once you confirm by clicking ok, you will not able to come back and bulk copy for the same month again. Do you want to Proceed?");
        if(flag) {
            TourPlanService.copyPlan(list).success(function(plan){
                $modalInstance.dismiss('cancel');
            });
        }
    }

    $scope.resetActivities = function(){
        if($scope.activity.date!=null){
            var flag = confirm("This will clear all the selected dates till now");
            if(flag){
                $scope.activity.date = {};
            }else{
                $modalInstance.dismiss('cancel');
            }
        }
        else {
            $modalInstance.dismiss('cancel');
        }
    }

    $scope.closeModal = function(){
        if($scope.activity.date!=null){
            var flag = confirm("You haven't copied the plan. Press on cancel and then bulk copy to save the data. Clicking Ok will remove the selected dates");
            if(flag){
                $modalInstance.dismiss('cancel');
            }
        } else {
            $modalInstance.dismiss('cancel');
        }
    }
});


sfe.controller('ViewPlanSummaryController', function ($scope, $modalInstance, LabelService, TourPlanService, RepositoryService,
                                                    detail, month, year, LocalStore, $http) {


    $scope.detail = detail;

    $scope.init = function(){
        RepositoryService.find('rpt_dr_planned', {"from_month":month,"from_year":year,"location_id":LocalStore.fetch('headerdata').location.id,"employeeId":LocalStore.fetch('headerdata').id},'advanced').then(function(plans){
            $scope.plans = plans.data;
        });
    }

    $scope.closeModal = function(){
        $scope.controls.showAlert = false;
        $scope.controls.message = '';
        $modalInstance.dismiss('cancel');
    }
});


sfe.controller('BeatsForTourController', function ($scope, $modalInstance, LabelService, TourPlanService, RepositoryService,
                                                   detail, plan, customers, $http, LocalStore,blockUI, LocalStore) {
    $scope.data = {};
    $scope.data.beatsMasterList = [];
    $scope.detail = detail;
    $scope.detail.activityDate = detail.activityDate;
    $scope.data.beats=[];
    $scope.data.customers = customers;
    $scope.data.plan = plan;
    $scope.data.totalDoctors = 0;

    $scope.controls = {showAlert:false};
    $scope.data.maxDoctors = LocalStore.fetch("USER_INFO").preferences['doctors.day'];

    $scope.init = function(){
        $http({
            method: 'GET',
            url: restUrl + "/sfe/beats/mybeats/" + LocalStore.fetch("headerdata").location.id,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        }).success(function(data){
            $scope.data.beatsMasterList = data;
            if($scope.detail && $scope.detail.attendees){
                $scope.data.totalDoctors = $scope.detail.numDoctors;
            }else{
                $scope.data.totalDoctors = 0;
            }
            if($scope.detail.beats){
                var totalDoctors = 0;
                for(var i=0;i<$scope.detail.beats.length;i++){
                    $scope.data.selectedBeat[$scope.detail.beats[i].id]=true;

                }
            }
        });
    }

    $scope.data.selectedBeat = {};
    $scope.selectBeat = function(beat){
        var beatId = beat.id;
        if($scope.data.selectedBeat[beatId]){
            var totalDoctors = $scope.data.totalDoctors + beat.c;
            if(totalDoctors> $scope.data.maxDoctors){
                $scope.data.selectedBeat[beatId] = false;
                $scope.controls.message = 'You can not add more than ' + $scope.data.maxDoctors +  ' doctors in a day.' +
                    ' You can still plan individual doctors from the doctors link.';
                $scope.controls.showAlert = true;
                return;
            }

        }
        blockUI.start();

        var customers = $scope.data.customers;
        if($scope.data.selectedBeat[beatId]){
            var customers = customers[beatId];
            var numDoctors = 1 * $scope.detail.numDoctors;
            var numChemists = 1 * $scope.detail.numChemists;
            if(customers){
                for(var i=0;i< customers.length;i++){
                    var id = customers[i].id;
                    var role = '';
                    if(id.indexOf('DOCT')>=0) {
                        numDoctors = numDoctors + 1;
                        role = 'SYSLV40000000000000000000000000000003';
                    }else {
                        numChemists = numChemists + 1;
                        role = 'SYSLV40000000000000000000000000000005'
                    }
                    if($scope.detail.attendees){
                        $scope.detail.attendees.push({beat:{id:beatId},activity:{id:$scope.detail.id},attendee:{id: customers[i].id},
                            role:{id:role}, planned: 1, visited:0, visitJoint:0
                        });
                    }else{
                        var attendees = [];
                        attendees.push({beat:{id:beatId},activity:{id:$scope.detail.id},attendee:{id: customers[i].id},
                            role:{id:role},planned: 1, visited:0, visitJoint:0
                        });
                        $scope.detail.attendees = attendees;
                    }
                }
            }
            if($scope.detail.beats){
                $scope.detail.beats.push({id:beatId,displayName: beat.name});
            }else{
                var beats = [];
                beats.push({id:beatId,displayName: beat.name});
                $scope.detail.beats = beats;
            }
            $scope.detail.plan = {id: plan.id};
            TourPlanService.addBeat($scope.detail.id, beatId)
                .success(function(response){
                blockUI.stop();
                var totalDoctors = $scope.data.totalDoctors + response.numDoctors;
                $scope.data.totalDoctors = totalDoctors;
                $scope.detail.numDoctors = response.numDoctors ;
                $scope.detail.numChemists = response.numChemists;

            }).error(function(msg){
                $scope.controls.showAlert = true;
                if(msg.msg.indexOf('ACTIVITY_ID_ATTENDEE_ID_index')>=0){
                    $scope.controls.message = 'Looks like you are adding the same doctor twice for the date.';
                }else{
                    $scope.controls.message = msg.msg;
                }
                $scope.data.selectedBeat[beatId] = false;
                blockUI.stop();
            });

        }else{
            var i = -1;
            for(var i=0;i<$scope.detail.beats.length;i++)
                if(beatId==$scope.detail.beats[i].id)
                    break;
            $scope.controls.enable =false;
            TourPlanService.removeBeat($scope.detail.id, beatId).success(function(b){
                $scope.detail.beats.splice(i,1);

                var beatCustomers = customers[beatId];
                var numDoctors = 1 * $scope.detail.numDoctors;
                var numChemists = 1 * $scope.detail.numChemists;
                if(beatCustomers){
                    for(var i=0;i< beatCustomers.length;i++){
                        var id = beatCustomers[i].id;
                        var role = '';
                        if(id.indexOf('DOCT')>=0) {
                            numDoctors = numDoctors - 1;
                        }else {
                            numChemists = numChemists - 1;
                        }
                        $scope.detail.numDoctors = numDoctors ;
                        $scope.detail.numChemists = numChemists;
                    }
                }
                blockUI.stop();
                $scope.controls.enable = true;
                var totalDoctors = $scope.data.totalDoctors - beat.c;
                $scope.data.totalDoctors = totalDoctors;
            }).error(function(m){
                $scope.controls.showAlert = true;
                $scope.controls.message = m;
                $scope.controls.enable = true;
            });
        }
    }

    $scope.closeModal = function(){
        $scope.controls.showAlert = false;
        $scope.controls.message = '';
        $modalInstance.dismiss('cancel');
    }
});



sfe.controller('DoctorForTourController', function ($scope, $modalInstance, LabelService, TourPlanService, RepositoryService,
                                                    detail, beats, editable, LocalStore, $http) {

    $scope.masterdata = {doctors:[]};
    $scope.data= {totalDoctors: detail.numDoctors};

    $scope.controls = {showAlert:false, message:'', editable:editable};
    $scope.data.maxDoctors = LocalStore.fetch("USER_INFO").preferences['doctors.day'];
    $scope.detail = detail;


    $scope.init = function(){

        var doctorBeatMap = {};
        for(var i=0;i< beats.length;i++){
            doctorBeatMap[beats[i].doctorId] = beats[i];
        }
        RepositoryService.find("docdp",{locationId: LocalStore.fetch('headerdata').location.id, paged:'N', activeDr: 'true'},'advanced')
            .success(function(data){
                var selectedDoctorMap = {};
                TourPlanService.getDoctorProfilesForActivity(detail.id,"1").success(function(doctorProfiles) {
                    for (var i = 0; i < doctorProfiles.length; i++) {
                        selectedDoctorMap[doctorProfiles[i].doctor.id] = 'Y';
                    }
                    for (var i = 0; i < data.length; i++) {
                        var selected = false;
                        if (selectedDoctorMap[data[i].doctor.id])
                            selected = true;
                        $scope.masterdata.doctors.push({
                            doctor: data[i],
                            beat: doctorBeatMap[data[i].doctor.id],
                            selected: selected
                        });
                    }
                });
        });
    }


    $scope.selectDoctor = function(profile){
        if(profile.selected){
            if (($scope.data.totalDoctors + 1) > $scope.data.maxDoctors)
            {
                $scope.controls.message = 'You can not add more than ' + $scope.data.maxDoctors +  ' doctors in a day.' +
                    ' You can still plan individual doctors from the doctors link.';
                $scope.controls.showAlert = true;
                profile.selected = false;
                return;
            }
            TourPlanService.addAttendee(detail.id, profile.beat.beatId, profile.doctor.doctor.id)
                .success(function(d){
                $scope.data.totalDoctors = $scope.data.totalDoctors + 1;
                $scope.detail.numDoctors = $scope.data.totalDoctors ;
            });
        }else{
            TourPlanService.removeAttendee(detail.id, profile.doctor.doctor.id).success(function(d){
                $scope.data.totalDoctors = $scope.data.totalDoctors - 1;
                $scope.detail.numDoctors = $scope.data.totalDoctors ;
            });
        }
    }

    $scope.closeModal = function(){
        $scope.controls.showAlert = false;
        $scope.controls.message = '';
        $modalInstance.dismiss('cancel');
    }
});

