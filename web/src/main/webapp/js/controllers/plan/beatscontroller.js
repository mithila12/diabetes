sfe.controller('BeatSearchControllerController', function ($scope, $http,$state, $stateParams,RepositoryService,SearchFormService, LabelService, LocalStore,SearchConfigService) {
    $scope.totalItems = 0;
    $scope.currentPage = 0;
    $scope.maxSize = 5;
    $scope.numPages = 20;
    $scope.showtable = false;
    $scope.nodata = false;
    $scope.labels = {};
    $scope.links = {};
    $scope.stdFormData = {};
    $scope.stdFormTemplate = {};
    $scope.formFields = {};
    $scope.territorys={};
    $scope.statusList={};
    $scope.beat={};
    $scope.beat.status={};
    $scope.beat.id={};
    $scope.searchData={};

    $scope.isStatusInactive = false;

    $scope.initSearch = function(){
        var data = $state.current.data;
        $scope.definition = {};
        $scope.links = LabelService.getLabels("links");
        $scope.labels =  LabelService.getLabels(data.labels);
        $scope.formFields   = SearchFormService.getForm($stateParams.context,data.query,data.labels);

         RepositoryService.find("territory",{"employeeId":LocalStore.fetch("headerdata").id},'simple').then(function (data) {
                                    $scope.territorys = data.data;
         });

          RepositoryService.find("usrlv",{"type":"BEAT_STATUS"},'simple').then(function (data) {
                     $scope.statusList = data.data;

                 });
    }

    $scope.addNew = function(){
        var columnDefinitions = SearchConfigService.getColumns('beats');
        console.log(columnDefinitions);
        $state.go(columnDefinitions.newUrl);
    };
     $scope.search=function(){
            $scope.queryFilters={};
            $scope.queryFilters = $scope.stdFormData;
            if($scope.stdFormData["myTerritory"]==null || $scope.stdFormData["myTerritory"]=="") {
                $scope.queryFilters["sel_territory"] = LocalStore.fetch('headerdata').location.id;
            }else{
                 $scope.queryFilters["sel_territory"]=$scope.stdFormData["myTerritory"]
            }



            RepositoryService.search("beats_docCount",$scope.queryFilters, $stateParams.context,'home').then(function(data){

                if(data.data.totalRecords==0) {
                    $scope.nodata = true;
                    $scope.showtable = false;
                }else{
                    $scope.nodata = false;
                    $scope.showtable = true;
                }

                $scope.searchData = data.data.result;
                $scope.columns = data.data.columns;
                $scope.actions = data.data.actions;
                $scope.totalItems = data.data.totalRecords;
                $scope.currentPage = data.data.currentPage;
            });
     }



     $scope.pageChanged = function(){
            var data = $state.current.data;
            var name = $state.current.name;
            $scope.stdFormData.page = $scope.currentPage;
            $scope.stdFormData.paged = 'Y';
            var reportedBy = LocalStore.fetch('headerdata').id;
            var teammemberId = $scope.stdFormData.employee_id;
            RepositoryService.search("beats_docCount",$scope.queryFilters, $stateParams.context,'home').then(function(response){
                $scope.searchData = response.data.result;
                $scope.columns = response.data.columns;
                $scope.actions = response.data.actions;
                $scope.totalItems = response.data.totalRecords;
                $scope.currentPage = response.data.currentPage;
            });
     }

    $scope.changeStatus = function(docCount,statusId,id,btype){
        var flag=0;

        if(statusId=="USRLVBTST0000000000000000000000000001"){
            var cnf = confirm("Current status is Active, do you want to change that to Inactive ?");
            if(cnf){
                if(docCount>0){
                        alert("Status cannot be inactive if doctors are present in the beat.");
                }else  flag=1;
            }
        }
        else{
            var cnf = confirm("Current status is Inactive, do you want to change that to Active ?");
                if(cnf) flag=2;
        }



            RepositoryService.restore(id).success(function (data) {
                $scope.beat=data;
                if(flag==1){
                    $scope.beat.status="USRLVBTST0000000000000000000000000002";
                    RepositoryService.save($scope.beat, 'BEATS').success(function(msg){
                       alert("Status Changed Successfully.");
                        $scope.search();
                  })
                }else if(flag==2){
                    $scope.beat.status="USRLVBTST0000000000000000000000000001";
                    RepositoryService.save($scope.beat, 'BEATS').success(function(msg){
                        alert("Status Changed Successfully.");
                        $scope.search();
                    })
                }
            });

        }


});


sfe.controller('BeatsController', function ($scope, $http,$state, $stateParams, TourPlanService,SearchConfigService,
                                            RepositoryService,SearchFormService, LabelService, LocalStore) {

    $scope.links = LabelService.getLabels('links');
    $scope.labels = LabelService.getLabels('beats');

    $scope.definition = {};
    $scope.locations = {};
    $scope.definition.location ={id: LocalStore.fetch("headerdata").location.id};
    $scope.doctors = {};
    $scope.definition.doctors=[];
    $scope.chemists = [];
    $scope.controls = {showAlert:false};
    $scope.stdFormData = {};
    $scope.statusList={};
    $scope.isStatusInactive = false;


    $scope.addNew = function(){
        var columnDefinitions = SearchConfigService.getColumns('beats');
        console.log(columnDefinitions);
        $state.go(columnDefinitions.newUrl);
    };

    $scope.init = function() {
        $scope.labels =  LabelService.getLabels('beats');
        RepositoryService.find("locat",{"employeeId":LocalStore.fetch("headerdata").id},'simple').then(function (data) {
            $scope.locations = data.data;
        });

        RepositoryService.find("usrlv",{"type":"BEAT_STATUS"},'simple').then(function (data) {
            $scope.statusList = data.data;

        });

        if($stateParams.id!=null){
            var response = RepositoryService.restore($stateParams.id);
            response.then(function(data){
                $scope.definition = data.data;


            if($scope.definition.beatType=="SYSTEM_CREATED"){
                            $scope.isStatusInactive = true;
                                       console.log($scope.isStatusInactive);
                    }

               RepositoryService.find("default_beats_doctor",{"location_id": $scope.definition.location.id},'simple').then(function (data) {
                    $scope.doctors = data.data;
                });
                var doctorIds = [];
                $($scope.definition.doctors).each(function(index){
                    var o = $(this)[0];
                    doctorIds.push(o.id);
                });
                TourPlanService.getChemists($stateParams.id,doctorIds).then(function(response){
                    var chemists = response.data;
                    $(chemists).each(function(index){
                        var o = $(this)[0];
                        var chemistArray = $scope.chemists[o.doctorId];
                        if(chemistArray)
                            chemistArray.push(o);
                        else{
                            chemistArray = new Array();
                            chemistArray.push(o);
                        }
                        $scope.chemists[o.doctorId] = chemistArray;
                    });
                });
            });
        }
        else{
            RepositoryService.find("default_beats_doctor",{"location_id": $scope.definition.location.id},'simple').then(function (data) {
                $scope.doctors = data.data;
            });
        }



    };

    $scope.updateDoctors = function(id){
        RepositoryService.find("default_beats_doctor",{"location_id": $scope.definition.location.id},'simple').then(function (data) {
            $scope.doctors = data.data;
        });
    }

    $scope.removeDoctor = function(id){
        var index = -1;
        $($scope.definition.doctors).each(function(i){
            var o = $(this)[0];
            if(o.id==id)
            {
                index =i;
                $scope.doctors.push($scope.definition.doctors[index]);
            }
        });
        $scope.definition.doctors.splice(index,1);
    }
    $scope.save = function(){
        var locat= $scope.definition.location;
        RepositoryService.save(this.definition, 'BEATS').success(function(msg){
            $scope.controls.message = 'Added Successfully!';
            $scope.controls.showAlert = true;
            //$scope.definition = {};
            //$scope.definition.location=locat;
            //$scope.definition.doctors=[];
            alert("Saved successfully.");
            $state.go('home.search_beats',{context:'advanced'});
        });
    };

    $scope.checkStatus=function(){
        if($scope.definition.status.id=="USRLVBTST0000000000000000000000000002"){
            //check if doctorlist present
            if($scope.definition.doctors.length>0){
                alert("Status cannot be inactive if doctors are present in the beat.");
                $scope.definition.status.id="USRLVBTST0000000000000000000000000001";
            }
        }
    };


    $scope.addDoctor = function(){
        var selectedDoctor = {id:$scope.doctor};
        $($scope.doctors).each(function(data){
            var doctor = $(this)[0];
            if(doctor.id==$scope.doctor){
                //selectedDoctor.name = doctor.firstName + ' ' + doctor.lastName;
                selectedDoctor.name = doctor.name;
                $scope.doctors.splice(data,1);
            }
        });
        $scope.definition.doctors.push(selectedDoctor);
        TourPlanService.getChemists($stateParams.id,[$scope.doctor]).then(function(response){
            //var chemists = new Array();
            //chemists.push(response.data);
            $scope.chemists[$scope.doctor] = response.data;
        });

    }

    $scope.search = function(){
        $http({
            method: 'GET',
            url: restUrl + "/sfe/beats/" + LocalStore.fetch("headerdata").id  + "/my?query=ci_name~" + $scope.stdFormData.ci_name,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        }).then(function(data){
            $scope.searchData.data = data.data;
        });
    }
});