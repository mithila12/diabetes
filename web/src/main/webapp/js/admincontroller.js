sfe.controller('UserLovSearchController',
    function ($scope, $http, $rootScope, $compile, $location, $stateParams, $state, LabelService, RepositoryService) {
        $scope.links = LabelService.getLabels("links");

        $scope.labels = LabelService.getLabels($stateParams.type);

        $scope.type = $stateParams.type;

        $scope.stdFormData = {};

        $scope.icon = $scope.labels.icon;
        $scope.title = $scope.labels.title;
        $scope.stdFormData = {type: $scope.labels.type};

        $scope.stdFormTemplate = {
            "fieldset": {
                "type": "fieldset",
                "label": "Search",
                "attributes":{'class':"cols-xs-18 dynamic-fieldset"},
                "fields": {
                    "name": {
                        "type": "text",
                        "label": $scope.labels.search.name,
                        "placeholder": $scope.labels.search.name,
                        "model":"ci_name",
                        "attributes":{"class":"col-xs-9"}
                    },
                    "button": {
                        "type": "button",
                        "attributes":{"class":"btn btn-primary pull-left"},
                        "label": $scope.links.search,
                        "callback":"search()"
                    }
                }
            }
        };

        $scope.searchData = {
            enableSorting: true,
            columnDefs: [
                { name: $scope.labels.search.name, field: 'name' },
                {name: $scope.links.action, field:'id', cellTemplate:'<a ui-sref="sysadmin.' +  $stateParams.type + '({type:\'' + $stateParams.type +'\',id:' + '\'{{COL_FIELD}}\''+ '})">' + $scope.links.edit +'</a>' }
            ],data:[]
        };

       /** $scope.search=function(){
            RepositoryService.search("usrlv",$scope.stdFormData, $stateParams.context).then(function(data){
                $scope.searchData = data.data.result;
            });
        } */

        $scope.columns = [];
        $scope.actions = [];
        $scope.totalItems = 0;
        $scope.currentPage = 0;
        $scope.maxSize = 5;
        $scope.numPages = 20;
        $scope.showtable = false;
        $scope.nodata = false;
        $scope.search=function(){
            var data = $state.current.data;
            var name = $state.current.name;
            $scope.stdFormData.paged = 'Y';
            $scope.stdFormData.page=1;
            $scope.showtable = true;
            RepositoryService.search("usrlv",$scope.stdFormData, $stateParams.context).then(function(data){
                if(data.data.totalRecords==0) {
                    $scope.nodata = true;
                    $scope.showtable = false;
                }else{
                    $scope.nodata = false;
                }
                $scope.searchData = data.data.result;
                $scope.columns = data.data.columns;
                $scope.actions = data.data.actions;
                $scope.totalItems = data.data.totalRecords;
                $scope.currentPage = data.data.currentPage;
            });
        }

        $scope.pageChanged = function(){
            var data = $state.current.data;
            var name = $state.current.name;

            $scope.stdFormData.page = $scope.currentPage;
            $scope.stdFormData.paged = 'Y';
            RepositoryService.search(data.entity.toLowerCase(),$scope.stdFormData, $stateParams.context, name.substr(0, name.indexOf('.'))).then(function(response){
                $scope.searchData = response.data.result;
                $scope.columns = response.data.columns;
                $scope.actions = response.data.actions;
                $scope.totalItems = response.data.totalRecords;
                $scope.currentPage = response.data.currentPage;
            });
        }

    }
);

sfe.controller('UserLovController',
    function ($scope, $http, $rootScope, $compile, $state, $stateParams,
              RepositoryService,LabelService) {
    $scope.definition = {type: $stateParams.type};
    $scope.links = LabelService.getLabels("links");
    $scope.labels = LabelService.getLabels($stateParams.type);
    $scope.icon = $scope.labels.icon;
    $scope.title = $scope.labels.title;


    $scope.init = function(){
        if ($stateParams.id != null && $stateParams.id !='new') {
            RepositoryService.restore($stateParams.id).then(function (data) {
                $scope.definition = data.data;
            });
        } else {
            return {};
        }
    };

     $scope.save = function(){
            RepositoryService.save(this.definition, 'USRLV').success(function(data){
                $scope.definition.id = data.id;
            });
    };

});




sfe.controller('StateController', function ($scope, $state, $stateParams, RepositoryService, SearchFormService, LabelService) {

    $scope.links = {};
    $scope.labels = {};
    $scope.definition = {};

    $scope.init = function() {
        $scope.links = LabelService.getLabels("links");
        $scope.labels = LabelService.getLabels('state');

        if($stateParams.id!=null){
            var response = RepositoryService.restore($stateParams.id);
            response.then(function(data){
                $scope.definition = data.data;
            });
        }
    };

    $scope.save = function(){
        if($scope.definition.name==null){
        return false;
        }
        if($scope.definition.id!=null){
             RepositoryService.save(this.definition, 'STATE').success(function(){
                    alert('Saved Successfully');
                    });
        }else{
            RepositoryService.create($scope.definition,'STATE').success(function(){
                    $scope.definition ={};
                    alert ('Created successfully');
            });
        }
    };


});


sfe.controller('EmployeeStatusController', function ($scope, $http, $state, $stateParams,
                                                     RepositoryService,SearchFormService, LabelService) {
    $scope.links = LabelService.getLabels("links");
    $scope.labels = LabelService.getLabels('employeeStatus');

    $scope.employeeStatus = {};
    $scope.statusLovs = {};

    $scope.init = function() {
        if($stateParams.id!=null){
            var response = RepositoryService.restore($stateParams.id);
            response.then(function(data){
                $scope.employeeStatus = data.data;
            });
        }
        RepositoryService.findSysLov('USER_STATUS').then(function (data) {
            $scope.statusLovs = data.data;
        });


    };
    $scope.save = function(){
        RepositoryService.save(this.employeeStatus, 'EMPLS');
    };


});

sfe.controller('EmployeeSwappingController', function ($scope, $http, $state, $stateParams,RepositoryService,SearchFormService, LabelService,LocalStore) {
         $scope.showTable=false;
         $scope.emp1={};
         $scope.emp2={};
         $scope.activeFrom={};
         $scope.init = function(){
            RepositoryService.getCurrentDate().success(function(cDate) {
                $scope.activeFrom = cDate;
            });
         };

         $scope.search=function(){

            RepositoryService.find('emply_swap', {"fromEmp":$scope.employeecode1,"toEmp":$scope.employeecode2}, 'simple').success(function (data) {
            $scope.emp1=data[0];
            $scope.emp2=data[1];
                if($scope.emp1.ORGANIZATION_TYPE!=$scope.emp2.ORGANIZATION_TYPE){
                alert("JOB TITLE NOT MATCHING..")
                }else{
              $scope.showTable=true;
                }
            });
         }

        $scope.swap=function(){
            if(!confirm("Are you sure you want to swap ?")){
                return;
            }else{
                   var olddate = $scope.activeFrom.text;
                   var ldate = olddate.split("/");
                   var ndate = new Date(ldate[2],(ldate[1]-1),ldate[0],0,0,0,0);
                   var newdate = ndate.getTime();

            $http({
                  method: 'PUT',
                  url: restUrl + "/miscellaneous/miscellaneousService/employeeSwap/" + $scope.emp1.ID +"/" +$scope.emp2.ID +"/" + newdate,
                      headers: {
                            "content-type": "application/json",
                            "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                      }
                  }).success(function(response){
                      if(response){
                            var location=$scope.emp1.LOCATION;
                            $scope.emp1.LOCATION=$scope.emp2.LOCATION;
                            $scope.emp2.LOCATION=location;
                            $scope.emp1.ACTIVE_FROM={date:newdate,text:olddate};
                            $scope.emp2.ACTIVE_FROM={date:newdate,text:olddate};
                            var division=$scope.emp1.DIVISION;
                            $scope.emp1.DIVISION=$scope.emp2.DIVISION;
                            $scope.emp2.DIVISION=division;
                       }
                  });
            }
        };
});



sfe.controller('OrganizationHierarchyController', function ($scope, $http,$state, $stateParams,
                                                                   RepositoryService,SearchFormService, LabelService) {
    $scope.searchData = {};
    $scope.stdFormTemplate = {};
    $scope.links = LabelService.getLabels('links');
    $scope.labels = LabelService.getLabels('orgHierarchy');
    $scope.stdFormData = {};
    $scope.definition = {state:false, location:false};
    $scope.parents  ={};

    $scope.init = function() {
        if($stateParams.id!=null){
            var response = RepositoryService.restore($stateParams.id);
            response.then(function(data){
                $scope.definition = data.data;
            });
        }
        RepositoryService.search("orghy",{},'simple').then(function (data) {
            $scope.parents = data.data;
        });

    };
    $scope.save = function(){
        RepositoryService.save(this.definition, 'ORGHY');
    };
});

sfe.controller('LocationController', function ($scope, $http,$state, $stateParams,
                                                   RepositoryService,SearchFormService, LabelService) {
    $scope.links = LabelService.getLabels('links');
    $scope.labels = LabelService.getLabels('location');
    $scope.definition = {};
    $scope.parents  ={};
    $scope.divisions  ={};
    $scope.types  ={};
    $scope.states  ={};
    $scope.statusList = {};
    $scope.cities={};

    $scope.init = function() {
        if($stateParams.id!=null){
            var response = RepositoryService.restore($stateParams.id);
            response.then(function(data){
                $scope.definition = data.data;
                $scope.definition.parent.id = data.data.parent.id;
                $scope.updateList('locat',{'division_id':$scope.definition.division.id},0);
                if($scope.definition.state!=null) {
                    RepositoryService.find('towns', {"stateId": $scope.definition.state.id}, 'simple').then(function (data) {
                        $scope.cities = data.data;
                    });
                }

                /*if($scope.definition.division!=null){
                    RepositoryService.find('locat',{'division_id':$scope.definition.division.id,'hierarchy_id':$scope.definition.definition.id},'simple').then(function (data) {
                            $scope.parents = data.data;
                    });
                }*/
            });
        }
        RepositoryService.find("divsn",{},'simple').then(function (data) {
            $scope.divisions = data.data;
        });
        RepositoryService.find("orghy",{},'simple').then(function (data) {
            $scope.types = data.data;
        });
        RepositoryService.find("state",{},'simple').then(function (data) {
            $scope.states = data.data;
        });
        RepositoryService.find("syslv",{'type':'LOCATION_STATUS'},'simple').then(function (data) {
            $scope.statusList = data.data;
        });

    };

    $scope.updateList = function(prefix, filter, index){
        RepositoryService.find(prefix,filter,'simple').then(function (data) {
            if(index == 0)
                $scope.parents = data.data;

        });
    }

    $scope.save = function(){
        if($scope.definition.id!=null){
                RepositoryService.save(this.definition, 'LOCAT').success(function () {
                     alert("Saved Successfully");
                });
        }else{
                RepositoryService.create($scope.definition,'LOCAT').success(function (){
                     $scope.definition={};
                     alert ('Created successfully');
                });
        }
    };

    $scope.getTown = function(val) {
        return $http.get('http://maps.googleapis.com/maps/api/geocode/json', {
            params: {
                address: val,
                sensor: false
            }
        }).then(function(response){
            return response.data.results.map(function(item){
                return item.formatted_address;
            });
        });
    };

    $scope.getStateTowns=function(stateId){

        RepositoryService.find('towns', {"stateId":stateId},'simple').then(function(data){
            $scope.cities = data.data;
        });
    };
});

sfe.controller('ViewAllMYManager', function($scope, RepositoryService,$state, $stateParams,input) {
    $scope.data = input;
});


sfe.controller('EmployeeSearchController', function ($scope, $state, $stateParams, LocalStore,
                                               RepositoryService,$modal,SearchFormService, SearchConfigService, LabelService,
                                               LovService,commonSearchService) {

    $scope.labels = {};
    $scope.links = {};
    $scope.stdFormData = {};
    $scope.stdFormTemplate = {};
    $scope.formFields = {};
    $scope.divisionList={};
    $scope.locationList={};
    $scope.divisionFlag = false;


    $scope.initSearch = function() {
        var data = $state.current.data;
        $scope.definition = {};
        $scope.links = LabelService.getLabels("links");
        $scope.labels = LabelService.getLabels(data.labels);
        $scope.formFields = SearchFormService.getForm($stateParams.context, data.query, data.labels);
        RepositoryService.find("divsn",{},'simple').then(function (data) {
            $scope.divisionsList = data.data;
        });
    };

    $scope.getLocation=function(){
        $scope.divisionFlag = true;
        RepositoryService.find("locat",{division_id:$scope.empDivision,isActive:true,'sortBy':'name','sortOrder':'ASC'},'simple').then(function (data) {
            $scope.locationList = data.data;
        });
    };

    $scope.columns = [];
    $scope.actions = [];
    $scope.totalItems = 0;
    $scope.currentPage = 0;
    $scope.maxSize = 5;
    $scope.numPages = 20;
    $scope.showtable = false;
    $scope.nodata = false;

    $scope.search=function(){
        var data = $state.current.data;
        var name = $state.current.name;
        $scope.stdFormData.paged = 'Y';
        $scope.stdFormData.page=1;
        $scope.showtable = true;

        /*if(($scope.divisionFlag==true && $scope.stdFormData['CMT_COMPANY_DETAILS.BUSINESS_UNIT_ID']==null || $scope.stdFormData['CMT_COMPANY_DETAILS.BUSINESS_UNIT_ID']==null)
               && $scope.stdFormData['ci_name']==null ){
            alert('please select location for respective division.');
            return;
        }*/

        RepositoryService.search(data.entity.toLowerCase(),$scope.stdFormData, $stateParams.context, name.substr(0, name.indexOf('.')), $state.current.data.fixedQuery).then(function(data){
            if(data.data.totalRecords==0) {
                $scope.nodata = true;
                $scope.showtable = false;
            }else{
                $scope.nodata = false;
            }
            $scope.searchData = data.data.result;
            $scope.columns = data.data.columns;
            $scope.actions = data.data.actions;
            $scope.totalItems = data.data.totalRecords;
            $scope.currentPage = data.data.currentPage;
        });
    };

    $scope.addNew = function(){
        var columnDefinitions = SearchConfigService.getColumns($state.current.data.labels);
        $state.go(columnDefinitions.newUrl);
    }

    $scope.pageChanged = function(){
        var data = $state.current.data;
        var name = $state.current.name;

        $scope.stdFormData.page = $scope.currentPage;
        $scope.stdFormData.paged = 'Y';
        RepositoryService.search(data.entity.toLowerCase(),$scope.stdFormData, $stateParams.context, name.substr(0, name.indexOf('.'))).then(function(response){
            $scope.searchData = response.data.result;
            $scope.columns = response.data.columns;
            $scope.actions = response.data.actions;
            $scope.totalItems = response.data.totalRecords;
            $scope.currentPage = response.data.currentPage;
        });
    }

    $scope.confirmBox=function(owner,fnName){
        commonSearchService[fnName](owner);
    }
})

sfe.controller('EmployeeController', function ($scope, $state, $stateParams, LocalStore,
                                               RepositoryService,$modal,SearchFormService, LabelService,
                                               LovService) {


    $scope.links = LabelService.getLabels('links');
    $scope.labels = LabelService.getLabels('employee');
    $scope.employee = {};
    $scope.employee.companydetails = [];
    $scope.jobtitles = {};
    $scope.divisions = {};
    $scope.locations = {};
    $scope.statusLovs = {};
    $scope.genders = {};
    $scope.maritalStatuses = {};
    $scope.managers = {};
    $scope.divisionId=null;
    $scope.roles = [];
    $scope.usercontext={};

    $scope.init = function() {
        $scope.usercontext = LocalStore.fetch("topcontext");
        if($stateParams.id!=null){
            var response = RepositoryService.restore($stateParams.id);
            response.then(function(data){
                $scope.employee = data.data;
                $scope.oldActiveFrom = data.data.companydetails[0].activeFrom.text;
                $scope.employee.companydetails[0].activeFrom.text = $scope.convertDate();
                RepositoryService.restore($scope.employee.companydetails[0].location.id).then(
                    function(data){
                        var emp_location = data.data;
                        $scope.divisionId = emp_location.division.id;
                        $scope.updateList($scope.divisionId);
                        $scope.updateList();
                        $scope.fetchEmployees();
                    }
                );
            });
        }

        RepositoryService.find("srole",{},'simple').success(function (data) {
            $(data).each(function(index){
                var o = $(this)[0];
                $scope.roles.push({id: o.id, displayName: o.name});
            });
        });


        /*LovService.getUserLov('gender',function(response){
            $scope.genders = response;
            LovService.getUserLov('marital_status',function(response){
                $scope.maritalStatuses = response;
            });
            LovService.getUserLov('location_type',function(response){
                $scope.maritalStatuses = response;
            });
        });*/

        RepositoryService.find("divsn",{},'simple').then(function (data) {
            $scope.divisions = data.data;
        });

        /*RepositoryService.find("locat",{},"simple").then(function (data) {
            $scope.locations = data.data;
        });*/


        RepositoryService.find("usrlv",{type:"gender"},"simple").then(function (data) {
            $scope.genders = data.data;
        });

        RepositoryService.find("usrlv",{type:"marital_status"},"simple").then(function (data) {
            $scope.maritalStatuses = data.data;
        });

        RepositoryService.find("jobtt",{},"simple").then(function (data) {
            $scope.jobtitles = data.data;
        });


        RepositoryService.find('empls',{}).then(function (data) {
            $scope.statusLovs = data.data;
        });

    };

    $scope.resetTerminationDate = function(){
        if($scope.employee.companydetails[0].status.id != "EMPLS0000007160736a014b7e7ad4d0008000"){
            $scope.employee.dateOfLeaving= null;
        }
    }

    $scope.updateList = function(id){
          /*if(!$scope.divisionId){
            return;
        }*/
        RepositoryService.find("locat",{"division_id": id, "vacantLocations":"true" },"simple").then(function (data) {
            $scope.locations = data.data;
        });
    }

    $scope.convertDate = function() {
        var d = new Date(new Date() || Date.now()),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [day, month, year].join('/');
    }

    $scope.save = function(){
        RepositoryService.save(this.employee, 'EMPLY').then(function (data){
                alert("Saved Successfully");
                if($stateParams.id!=null){
                    if($scope.employee.dateOfLeaving==null){
                        RepositoryService.restore(data.data.id).then(function(data){
                            if(data!=null){
                                $scope.employee = data.data;
                                $scope.oldActiveFrom = data.data.companydetails[0].activeFrom.text;
                                console.log($scope.employee);
                                console.log($scope.oldActiveFrom);
                            }
                        });
                    }
                } else {
                    $scope.employee = {};
                }
            }
        );
    };

    $scope.fetchEmployees= function(){
        RepositoryService.find('emply_per_loc',{"location_id":$scope.employee.companydetails[0].location.id}).then(function (data) {
            $scope.managers = data.data;
        });
    };

    $scope.viewAllCompDetails = function(){
        var employeeId = $stateParams.id;
        var data;
        RepositoryService.find('cmpde',{"employee_id":employeeId}).then(function (data) {
            $scope.data = data.data;
        });
    };

    $scope.viewSecurityInfo = function(){
    }

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };



});

sfe.controller('ExpenseTypeController', function ($scope, $http,$state, $stateParams,
                                               RepositoryService,SearchFormService, LabelService) {
    $scope.links = LabelService.getLabels('links');
    $scope.labels = LabelService.getLabels('expenseType');

    $scope.definition = {};
    $scope.expensetype = {};
    $scope.expenseDesignationList = [];




    $scope.init = function() {
        if($stateParams.id!=null){
            var response = RepositoryService.restore($stateParams.id);
            response.then(function(data){
                $scope.definition = data.data;
                $scope.expenseDesignationList = data.data.expenseTypeDesignationDTOList;
            });
        }

         RepositoryService.find("divsn",{},'simple').then(function (data) {
            $scope.divisions = data.data;
        });

         RepositoryService.find("jobtt",{},"simple").then(function (data) {
            $scope.jobtitles = data.data;
        })
    };


    $scope.addExpenseDesignation = function (divId,jobId){
          var divi = {id:null,displayName:null};
          var jobt = {id:null,displayName:null};

            for(d in $scope.divisions){
                 if($scope.divisions[d].id==divId){
                     divi.id=divId;
                     divi.displayName=$scope.divisions[d].name;
                     break;
                 }
            }
             for(d in $scope.jobtitles){
                 if($scope.jobtitles[d].id==jobId){
                     jobt.id=jobId;
                     jobt.displayName=$scope.jobtitles[d].name;
                     break;
                 }
            }

             var designation={type:null,division:divi,jobtitle:jobt}
             console.log(designation);
            $scope.expenseDesignationList.push(designation);


    };

    $scope.deleteExpenseDesignation= function(index){
            $scope.expenseDesignationList.splice(index,1);
    };


    $scope.save = function(){
/*            console.log($scope.definition);

            $scope.definition.code = null;
            $scope.definition.jobTitles = null;
            $scope.definition.roles = null;
            console.log($scope.definition);
*/
            $scope.definition.expenseTypeDesignationDTOList = $scope.expenseDesignationList;
        if($scope.definition.id!=null){
                RepositoryService.save(this.definition, 'EXPTY').success(function(){
                        alert('Saved Successfully');
                });
        }else{
                RepositoryService.create(this.definition, 'EXPTY').success(function(){
                        $scope.definition={};
                        alert('Created successfully');
                });
        }

    };

   /* $scope.addAnother = function(){
        var i= 0;
        for(i=0;i<$scope.designations.length;i++){
            if($scope.designations[i].id == $scope.dummy.id) {
                $scope.definition.jobTitles.push({ "id": $scope.dummy.id, "displayName": $scope.designations[i].name});
            }
        };
    }

    $scope.removeDesignation = function(id){
        var i = -1;
        var j =0;
        for(j=0;j< $scope.definition.jobTitles.length;j++){
            if($scope.definition.jobTitles[j].id == id) {
                i = j;
            }
        };
        $scope.definition.jobTitles.splice(i,1);
    }*/
});


sfe.controller('SecurityRoleController', function ($scope, $http,$state, $stateParams,
                                                  RepositoryService,SearchFormService, LabelService) {
    $scope.links = LabelService.getLabels('links');
    $scope.labels = LabelService.getLabels('srole');

    $scope.definition = {};
    $scope.definition.privileges = [];
    $scope.privileges = [];
    $scope.privilege ={};

    $scope.init = function() {
        if($stateParams.id!=null){
            RepositoryService.restore($stateParams.id).then(function(data){
                $scope.definition = data.data;
            });
            var privileges= $scope.definition.privileges;

        }
        RepositoryService.find("spriv",{},'simple').then(function (data) {
            $scope.privileges = data.data;
        });

    };

    $scope.save = function(){
        if($scope.definition.name==null){
        return false ;
        }
        if($scope.definition.id!=null){
        RepositoryService.save(this.definition, 'SROLE').success(function(){
                    alert('Saved Successfully');
        });

        }else{
        RepositoryService.create($scope.definition,'SROLE').success(function(){
                             $scope.definition = {};
                             alert('Create Successfully');
        });
        }

    };

    $scope.addAnother = function(){
        var i= 0;
        for(i=0;i<$scope.privileges.length;i++){
            if($scope.privileges[i].id == $scope.privilege) {
                $scope.definition.privileges.push($scope.privileges[i]);
            }
        };
    }

    $scope.removePrivilege = function(id){
        var i = -1;
        var j =0;
        for(j=0;j< $scope.privileges.length;j++){
            if($scope.privileges[j].id == id) {
                i = j;
            }
        };
        $scope.definition.privileges.splice(i,1);
    }
});

sfe.controller('DesignationController', function ($scope, $http,$state, $stateParams,
                                                   RepositoryService,SearchFormService, LabelService) {
    $scope.links = LabelService.getLabels('links');
    $scope.labels = LabelService.getLabels('employee_designation');

    $scope.definition = {};

    $scope.init = function() {
        if($stateParams.id!=null){
            var response = RepositoryService.restore($stateParams.id);
            response.then(function(data){
                $scope.definition = data.data;
            });
        }

    };

    $scope.save = function(){
        if($scope.definition.name==null){
        return false;
        }
        if($scope.definition.id!=null){
            RepositoryService.save(this.definition, 'JOBTT').success(function(){
                        alert('Saved Successfully');
            });
        }else{
            RepositoryService.create($scope.definition,'JOBTT').success(function(){
                       $scope.definition = {};
                       alert('Created Successfully');
            });
        }

    };


});

sfe.controller('CompetitorController', function ($scope, $http,$state, $stateParams, $modal,
                                                  RepositoryService,SearchFormService, LabelService) {
    $scope.links = LabelService.getLabels('links');
    $scope.labels = LabelService.getLabels('competitor');
    $scope.brandStatus = {'SYSLV20000000000000000000000000000001':'Active','SYSLV20000000000000000000000000000002':'In-Active'};

    $scope.definition = {};
    $scope.definition.brands = [];
    $scope.brand={};

    $scope.init = function() {
        if($stateParams.id!=null){
            RepositoryService.restore($stateParams.id).then(function(data){
                $scope.definition = data.data;
            });
        }
    };

    $scope.save = function(){
        if($scope.definition.id!=null){
            RepositoryService.save(this.definition, 'COMPT').success(function(){
                    alert('Saved Successfully');
             });
        }else{
            RepositoryService.create($scope.definition, 'COMPT').success(function(){
                             $scope.definition = {};
                             alert('Created Successfully');
            });
        }
    };

    $scope.addAnother = function(){
        $scope.definition.brands.push(angular.copy($scope.brand));
    }

    $scope.removeBrand = function(brandName){
        var i = -1;
        $($scope.definition.brands).each(function(index){
            var o = $(this)[0];
            if(!o.id && o.name==brandName) {
                i = index;
            }
        });
        $scope.definition.brands.splice(i,1);
    }

    $scope.brandId = {};
    $scope.showProducts = function(brandId){
        var modalInstance = $modal.open({
            templateUrl: 'brandProductListing.html',
            controller: 'BrandProductController',
            size: 'lg',
            resolve: {
               brand: function () {
                   return brandId;
               }
            }
        });
    }

});



sfe.controller('BrandProductController', function ($scope, $modalInstance, LabelService, RepositoryService,
                                                brand) {
    $scope.links = LabelService.getLabels('links');
    $scope.brand = {};
    RepositoryService.restore(brand).then(function (data) {
        $scope.brand = data.data;
    });

    $scope.product = {};

    $scope.saveProductForBrand = function () {
        RepositoryService.save($scope.brand, 'COMPB');
    };

    $scope.productForBrandCancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.addAnother = function(){
        $scope.brand.products.push(angular.copy($scope.product));
    };


});


sfe.controller('ChemistController', function ($scope, LabelService, RepositoryService, $http, LocalStore, $state, $stateParams,$modal) {
    $scope.links = LabelService.getLabels('links');
    $scope.doctors = {};
    $scope.doctorSelected = [];
    $scope.definition ={};
    $scope.definition.doctors = [];
    $scope.master = {};
    $scope.temp = {};


    $scope.addDoctor = function(){
        var doctorList = $scope.master.doctors;
        var duplicateDoc=0;
        for(d in $scope.definition.doctors){
            if($scope.definition.doctors[d].id==$scope.temp.doctor){
                duplicateDoc=1;
            }
        }
        if(duplicateDoc==1){
            alert("Can not all one doctor multiple time");
        }
        else {
            for (var i = 0; i < doctorList.length; i++) {
                if (doctorList[i].id == $scope.temp.doctor) {
                    var doctorObj = doctorList[i];
                    $scope.definition.doctors.push({id: doctorObj.id, name: doctorObj.name});
                    $scope.temp.doctor="";
                    return;
                }
            }
        }
    }

    $scope.removeDoctor = function(doctorId, index){
        $scope.definition.doctors.splice(index,1);
    }

    $scope.save = function(){
        if($scope.temp.doctor!=null){
            $scope.addDoctor();
        }
        RepositoryService.save($scope.definition, 'CHEMT');
        var division = $scope.definition.division;
        var location = $scope.definition.location;
        alert("Saved Successfully");
        if($stateParams.id==null)
        {
            $scope.definition = {};
            $scope.definition.division=division;
            $scope.definition.location=location;
        }
    }

    $scope.init = function(){
        $scope.usercontext = LocalStore.fetch('topcontext');
        if($scope.usercontext=='home')
        {
            var loggedInUserId = LocalStore.fetch('headerdata').id;
            RepositoryService.restore(loggedInUserId).then(function(data){
                RepositoryService.restore(data.data.companydetails[0].location.id).then(function(loc){
                    $scope.location = {"id":loc.data.id,"displayName":loc.data.name};
                    $scope.division = {"id":loc.data.division.id,"displayName":loc.data.division.displayName};
                    if($stateParams.id == null) {
                        $scope.definition.location = {"id": loc.data.id, "displayName": loc.data.name};
                        $scope.definition.division = {"id": loc.data.division.id, "displayName": loc.data.division.displayName};
                    }
                });
            });
        }
        RepositoryService.find("state",{},'simple').then(function(states){
            $scope.states = states.data;
        });
        if ($stateParams.id != null) {
            RepositoryService.restore($stateParams.id).then(function (data) {
                $scope.definition = data.data;
                $scope.displayDoctors = $scope.definition.doctors;
                RepositoryService.find("divsn",{division:true},"advanced").then(function (data) {
                    $scope.master.divisions = data.data;
                });
                RepositoryService.find("locat",{"division_id":$scope.definition.division.id,"showLocations":"Y"},'advanced').then(function (data) {
                    $scope.master.locations = data.data;
                });

                RepositoryService.find("doctr",{'location_id': $scope.definition.location.id,'activeDr':'true'},'advanced')
                    .then(function(data){
                        $scope.master.doctors = data.data;
                    });
            });
        } else {
            RepositoryService.find("divsn",{division:true},"advanced").then(function (data) {
                $scope.master.divisions = data.data;
            });
            return {};
        }

    }

    $scope.selectDivision = function(){
        RepositoryService.find("locat",{"division_id":$scope.definition.division.id,"showLocations":"Y"},'advanced').then(function (data) {
            $scope.master.locations = data.data;
        });
    }

    $scope.changeLocation = function(){
        RepositoryService.find("docdp",{'location_id': $scope.definition.location.id},'advanced')
            .then(function(data){
                $scope.master.doctors = data.data;
            });
    }

    $scope.getDoctors=function(){
        var modalInstance = $modal.open({
            templateUrl: 'unTagDoc.html',
            controller: 'unTagDocController',
            size: 'xs',
            backdrop: false,
            backdropClick: false,
            dialogFade: true,
            keyboard: false
        });
    }

});

sfe.controller('unTagDocController',function($scope, $http,LabelService, RepositoryService, LocalStore, $modalInstance){
    $scope.unmappedDoc={};
    $scope.init=function(){
        RepositoryService.find('doctr_unmaped',{location: LocalStore.fetch('headerdata').location.id}, 'simple','home').then(function(data){
            $scope.unmappedDoc = data.data;
        });
    }

    $scope.closeModal = function(input){
        $modalInstance.dismiss('cancel');
    }
})

sfe.controller('adminLeaveController', function ($scope, LabelService, RepositoryService, $http, LocalStore, $state, $stateParams
) {
    $scope.links = LabelService.getLabels('links');
    $scope.leaves = {};
    $scope.leavesSelected = [];
    $scope.definition ={};
    $scope.masterdata = {};
    $scope.masterdata.leaveTypes ={};
    $scope.leaveDetails={};
    $scope.employeeCode ={};
    $scope.employId = {};
    $scope.emplname = {};
    $scope.showDetails = false ;

    $scope.init = function(){
            console.log(LocalStore.fetch("USER_INFO"));
            $scope.employeeCode = "";
    }
/*
    $scope.getLeaves = function(val){
        if(val.length<3)
            return {};
        return $http.get(restUrl + "/core/framework/leave_name/search", {
            params: {
                searchParams: 'first_name~' + val + '^format~fname-lname',
                mode:'advanced'

            },
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        }).then(function(response){
            return response.data.result;
        });
    };
*/

    $scope.addLeave = function(){
        var leaveObj = angular.copy($scope.leavesSelected);
        $scope.leaves.push({id: leaveObj.id, name:leaveObj.name});
        $scope.leavesSelected=null;
    }

    $scope.save = function(){

        var arr1 = $scope.definition.leaveStartDate.text.split("/");
        var sdate = arr1[2] + "-" + arr1[1] + "-" +arr1[0];
        var arr2 = $scope.definition.leaveEndDate.text.split("/");
        var edate = arr2[2] + "-" + arr2[1] + "-" +arr2[0];

        var startDate=moment($scope.definition.leaveStartDate.text, "DD/MM/YYYY");
        var endDate=moment($scope.definition.leaveEndDate.text, "DD/MM/YYYY");

        if(startDate.isAfter(endDate)){
            alert("Leave end date cannot be a previous date of start date");
        }
        else
        {
            RepositoryService.find("activity_detail_within_leave_date", {"startDate":sdate,
                "endDate":edate, "ownerId":$scope.employId},'simple').then(
                function(data){
                    if(data.data[0]>0){
                        alert("You can not apply for leave as you have already plan activity on same day");
                    }
                    else
                    {
                        $scope.definition.appliedBy = {id:$scope.employId};
                        var date = new Date();
                        var displayDate = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
                        $scope.definition.appliedOn = {text:  displayDate};
                        $scope.definition.status = {id:"SYSLV50000000000000000000000000000111"};
                        RepositoryService.save($scope.definition, 'LEAVE').success(function(data){
                            RepositoryService.find("leavb",{'employeeId':$scope.employId,'previousYear':'2016','leaveType':$scope.definition.leaveType.id},'simple')
                                .then(function(data){
                                    $scope.masterdata.leaveTypes = data.data;
                                    $scope.getLeaveDetails($scope.definition.leaveType.id);
                                    alert("Applied Leave Successfully");
                                });
                        });
                    }
                });
        }
    }

    $scope.cancelLeave = function(){
        $scope.definition.status = {id:"SYSLV50000000000000000000000000000114"};
        RepositoryService.save($scope.definition, 'LEAVE').success(function(data){
           alert("Leave Cancelled Successfully");
        });
    }

    $scope.getLeaveDetails=function(leaveId){
        $scope.leaveDetails={};
        for(l in $scope.masterdata.leaveTypes){
            var leaave=$scope.masterdata.leaveTypes[l];
            if(leaave.type.id==leaveId){
                $scope.leaveDetails=leaave;
            }
        }
    }

    $scope.search = function(){

        if($scope.employeeCode==null || $scope.employeeCode==""){
            alert('Please enter Employee Code.');
        }else{

              RepositoryService.find("emply",{'emplcode':$scope.employeeCode}, 'advanced').success(function (empldata) {
                    $scope.employId = empldata[0].id;
                    $scope.emplname = empldata[0].name;
                        if($scope.employId!=null){
                            $scope.showDetails = true;
                        }
                   RepositoryService.getCurrentDate().success(function(data) {
                      var currentDate=new Date(data.date);
                      RepositoryService.find("leavb", {'employeeId':$scope.employId,'previousYear': currentDate.getFullYear(),'checkValidity':true}, 'advanced')
                          .then(function (data) {
                              $scope.masterdata.leaveTypes = data.data;
                              console.log($scope.masterdata);
                          });

                      if ($stateParams.id != null) {
                          RepositoryService.restore($stateParams.id).then(function (data) {
                              $scope.definition = data.data;
                          });
                      }
                  });
              });

        }


    }


})

sfe.controller('StockistController', function ($scope, LabelService, RepositoryService, $http, LocalStore, $state, $stateParams
) {
    $scope.links = LabelService.getLabels('links');
    $scope.definition ={};
    $scope.divisionSel = {};
    $scope.definition.locations = [];
    $scope.definition.status = {};
    $scope.locationSelected = null;
    $scope.disableLocation = false;


    $scope.init = function(){
        console.log($scope.definition);
        if ($stateParams.id != null) {
            RepositoryService.restore($stateParams.id).then(function (data) {
                $scope.definition = data.data;
            });
        }else{
            $scope.definition.status={id:"SYSLVSTKTYPE0000000000000000000000001",displayName:"Active"};
        }

         RepositoryService.find("divsn",{}, 'simple').then(function(data){
                    $scope.divisionList = data.data;
         });

         RepositoryService.find("syslv",{type:'STOCKIST_STATUS'},'simple').then(function (data) {
            $scope.statusList = data.data;
         });

    }


    $scope.getLocations = function(val){
        if(val.length<3)
            return {};

        return $http.get(restUrl + "/core/framework/locat/find", {
            params: {
                searchParams: 'showLocations~Y^ci_name~' + val,
                mode:'advanced'
            },
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        }).then(function(response){
            return response.data;
        });
    };

    $scope.checkStatus=function(){

        if($scope.definition.status.id=="SYSLVSTKTYPE0000000000000000000000002"){
            if($scope.definition.locations.length>0){
                alert("Status cannot be inactive if locations are present in the list.");
                $scope.definition.status.id="SYSLVSTKTYPE0000000000000000000000001";
            }

            $scope.disableLocation = true;
        }
    };


    $scope.addLocation = function(divId,locId){
    console.log(divId,locId);
        if(!$scope.definition.locations)
            $scope.definition.locations = [];

            var flag=0;


            if(divId==null || divId=="" || locId==null || locId==""){
                alert("Can not add empty location to list.");
                return;
            }

            for(n in $scope.definition.locations){
                 if($scope.definition.locations[n].id == locId){
                       flag=1;
                 }
            }
               if(flag==1){
                   alert("Can not add same territory multiple times");
                    return false;
               }

             for(l in $scope.locationList){
                    if($scope.locationList[l].id==locId){
                                    $scope.definition.locations.push($scope.locationList[l]);
                                    return;
                    }
               }


        $scope.locationSelected = null;
        $scope.divisionSel = null ;
        console.log($scope.definition);
    };


    $scope.removeLocation = function(location){
        var locations = $scope.definition.locations;
        var index = -1;
        for(var i=0;i< locations.length;i++){
            if(locations[i].id == location.id){
                index = i;
            }
        }
        $scope.definition.locations.splice(index,1);
    };

    $scope.save = function(){

        if($scope.definition.status.id=="SYSLVSTKTYPE0000000000000000000000002"){
        if( $scope.definition.locations.length>0){
            alert("Can not add locations to an In-Active Stockist");
            return;
        }
        }

         if($scope.definition.id!=null){
            RepositoryService.save($scope.definition, 'STKST').success(function(){
                 alert('Saved Successfully');
            });
         }else{
            RepositoryService.create($scope.definition, 'STKST').success(function(){
                $scope.definition = {};
                alert('Created Successfully');
            });

         }
    }

    $scope.selectlocation = function(){
        RepositoryService.find("locat",{division_id :$scope.divisionSel,hierarchy_id:'ORGHY00000000000000000000000000000003'}, 'advanced').then(function(data){
                    $scope.locationList = data.data;
         });
    }

});


sfe.controller('CoachingFormController',function ($scope, LabelService, RepositoryService, $http, LocalStore, $state, $stateParams) {
    $scope.labels = LabelService.getLabels("coachingFormDefinition");
    $scope.links = LabelService.getLabels("links");

    $scope.controls = {};
    $scope.data = [];
    $scope.answerTypeList = [];
    $scope.sectionScoreTypeList = [];
    $scope.sectionRankLovList = [];
    $scope.definition = [];
    $scope.jobtitles = {};
    $scope.addSection = false;

    $scope.init = function(){

        RepositoryService.find("syslv",{type:'COACHING_FORM_SECTION_SCORE_TYPE'}, 'advanced').then(function(data){
            $scope.sectionScoreTypeList = data.data;
        });
        RepositoryService.find("syslv",{type:'COACHING_FORM_SECTION_RANK_LOV'}, 'advanced').then(function(data){
            $scope.sectionRankLovList = data.data;
        });

        RepositoryService.find("syslv",{type:'COACHING_FORM_ANS_TYPE'}, 'advanced').then(function(data){
            $scope.answerTypeList = data.data;
        });

        RepositoryService.find('jobtt',{},'advanced').then(function(data){
            $scope.jobtitles = data.data;
        });
    }

    $scope.search = function(jobtitleId){

      /* if(jobtitleId == null||jobtitleId==""){
            alert('please select designation.');
            return;
       }*/
        RepositoryService.find('coasm_all',{jobtitle:jobtitleId},'advanced').then(function(data){
            $scope.definition = data.data;
             $scope.addSection = true;
        });


    }

    $scope.removeQuestion = function(d){
        var index = -1;
        for(var i=0;i< $scope.data.length;i++){
            if(d.displayOrder==$scope.data[i].displayOrder)
                index = i;
        }
        $scope.data.splice(index, 1);
    }

    $scope.addQuestion = function(){
        $scope.data.push({
            displayOrder: $scope.controls.question.sequence,
            question: $scope.controls.question.question,
            answerType: {id:$scope.controls.question.answerType},
            hasScore: ($scope.controls.question.hasScore?true:false)
        });

        $scope.controls.question = {};
    }

    $scope.saveSection = function(){
        var section = {displayOrder: $scope.controls.section.displayOrder, name:$scope.controls.section.name,
                        scoreType:$scope.controls.section.scoreType, jobTitle:{id:$scope.jobtitle.id}};
        section.questions = $scope.data;
        var id = RepositoryService.create(section, "COASM");
     $scope.init();
    }

});


sfe.controller('DoctorInputController',function ($scope, LabelService, RepositoryService, $http, LocalStore, $state, $stateParams) {
    $scope.labels = LabelService.getLabels("doctor_inputs");
    $scope.links = LabelService.getLabels("links");

    $scope.brands = {};
    $scope.master = {};
    $scope.definition = {};
    $scope.definition.division = {};
    $scope.definition.division.id = {};

    $scope.init = function(){

        if($stateParams.id!=null){
            var response = RepositoryService.restore($stateParams.id);
            response.then(function(data){
                $scope.definition = data.data;
                $scope.selectBrand($scope.definition.division.id);
            });

        }
        RepositoryService.find("syslv",{type:'INPUT_STATUS'}, 'advanced').then(function(data){
            $scope.master.statuses = data.data;
        });
        RepositoryService.find("divsn",{}, 'advanced').then(function(data){
            $scope.master.divisions = data.data;

        });
         RepositoryService.find("usrlv",{"type":"doctor_input_type"}, 'advanced').then(function(data){
                    $scope.types = data.data;

                });

          $scope.definition.quantityPerTM=0;

    }


    $scope.selectBrand = function(id){
        RepositoryService.find("brand",{"division":id},'advanced').then(function(data){
              $scope.brands = data.data;
        });
    }


    $scope.save = function(){
    var qtyTm = $scope.definition.quantityPerTM;

                if( isNaN(qtyTm) ){
                    $scope.definition.quantityPerTM=0;
                    $scope.flag=false;
                    $scope.fieldmsg="Field accepts only numbers";
                    return false;
                }else{
                    $scope.flag=true;
                }

            if(qtyTm=="")
                $scope.definition.quantityPerTM=0;

                if($scope.definition.id!=null){
                    RepositoryService.save(this.definition, 'DOCTI').success(function(){
                        alert('Saved successfully');
                            $state.go('sysadmin.search_doctorInputs',{context:'simple'});
                    });
                }else{
                    RepositoryService.create($scope.definition, 'DOCTI').success(function(){
                        $scope.definition={};
                        alert('Created Successfully');
                    });
                }
    }

});


sfe.controller('ApprovalChainDefinitionController', function ($scope, $state, $stateParams, LocalStore,
                                               RepositoryService,SearchFormService, LabelService) {


    $scope.links = LabelService.getLabels('links');
    $scope.labels = LabelService.getLabels('chainDefinition');
    $scope.chain = {};
    $scope.divisions = {};
    $scope.types={};
    $scope.designations = {};
    $scope.detail={id:null,level:0,daysInQueue:0,required:false,accountability:{},chain:{},headerId:""};

    $scope.init = function() {
        if($stateParams.id!=null){
            var response = RepositoryService.restore($stateParams.id);
            response.then(function(data){
                $scope.chain = data.data
            });
        }else{
            $scope.detail.level = 1;
            $scope.chain.details=[];
            $scope.chain.details.push($scope.detail);
            $scope.detail = {id: null,level: 0,daysInQueue: 0, required: false, accountability: {}, chain: {},headerId: ""};
        }
        RepositoryService.find("syslv",{'type':'approval_doc'},"simple").then(function (data) {
            $scope.types = data.data;
        });
        RepositoryService.find("divsn",{},"simple").then(function (data) {
            $scope.divisions = data.data;
        });
        RepositoryService.find("jobtt",{},"simple").then(function (data) {
            $scope.designations = data.data;
        });
    };

    $scope.add=function(level,index){
        if($scope.chain.details[index].accountability.id==null || $scope.chain.details[index].accountability.id==""){
            alert("Please select job title");
            return false;
        }else {

            $scope.detail.level = level + 1;
            $scope.chain.details.push($scope.detail);
            $scope.detail = {id: null,level: 0,daysInQueue: 0, required: false, accountability: {}, chain: {},headerId: ""};
        }
    }

    $scope.remove=function(index){
        $scope.chain.details.splice(index,1);
        var cnt=1;
        for(d in $scope.chain.details){
            var dtl=$scope.chain.details[d];
            dtl.level=cnt;
            cnt=cnt+1;
        }
    }

    $scope.save = function(){
        RepositoryService.save(this.chain, 'ACDFN').then(function(data){
            $scope.chain.id=data.data.id;
        });
    };
    $scope.selectDesignation = function(i){
        $scope.chain.details[i].level = i+1;
    }


});


sfe.controller('TownController',function($scope,$state,$stateParams,LocalStore,RepositoryService,LabelService){
    $scope.links = LabelService.getLabels('links');
    $scope.labels = LabelService.getLabels('towns');

    $scope.definition ={};



    $scope.init=function() {
        RepositoryService.find("state", {}, 'simple').then(function (data) {
            $scope.states = data.data;

        });

        RepositoryService.find("usrlv", {"type":"town_category"}, 'simple').then(function (data) {
            $scope.categories = data.data;
        });


        if($stateParams.id!=null){
            var response = RepositoryService.restore($stateParams.id);
            response.then(function(data){
                $scope.definition = data.data;
            });
        }
    }

    $scope.save = function(){
        $scope.definition.secondaryZips=['NA'];
        if($scope.definition.id!=null){
                RepositoryService.save($scope.definition, 'TOWNS').success(function(response){
                    $scope.definition.id = response.id;
                    alert('Saved Successfully');
                });
        }else{
                RepositoryService.create($scope.definition, 'TOWNS').success(function(){
                    $scope.definition = {};
                    alert('Created Successfully');
                })
        }
    }
})

sfe.controller('StandardExpenseController', function ($scope, $state, $stateParams, LocalStore,
                                                      RepositoryService,SearchFormService, LabelService,SearchConfigService) {


    $scope.links = LabelService.getLabels('links');
    $scope.labels = LabelService.getLabels('standardExpense');
    $scope.standardExpense = {};
    $scope.type = {};
    $scope.location_type = {};
    $scope.jobTitles = {};
    $scope.switch = true;
    $scope.standardExpense = {};
    $scope.expenseValue = {};

    $scope.init = function () {
        if ($stateParams.id != null) {
            var response = RepositoryService.restore($stateParams.id);
            response.then(function (data) {
                $scope.standardExpense = data.data;
                $scope.expenseValue = $scope.standardExpense.expenseValue;
            });
        }

        RepositoryService.find("lovtype", {'userLovType': 'expense_type', 'sysLovType': 'expense_type'}, "simple")
            .then(function (data) {
                $scope.expenseTypes = data.data;
            });
        RepositoryService.find("usrlv", {'type': 'location_type'}, 'advanced')
            .then(function (data) {

                $scope.locations = data.data;
            });
        RepositoryService.find("jobtt", {}, "simple").then(function (data) {
            $scope.designations = data.data;
        });

        RepositoryService.find("divsn", {}, "simple").then(function (data) {
            $scope.divsnList = data.data;
        });
    };
    $scope.save = function () {

        var stdExp=this.standardExpense;
        var locationId;

        if(stdExp.locationType){
            locationId=stdExp.locationType.id
        }else{
            locationId=null;
        }

        /*RepositoryService.find('stdep',{expenseType:stdExp.expenseType.id,locationType:locationId,division:stdExp.division.id,jobTitles:stdExp.jobTitles.id,category:stdExp.category.id,validFrom:stdExp.validFrom.text,validTo:stdExp.validTo.text},'simple').then(function(data){
            //console.log(data.data);
        })*/
        if( $scope.standardExpense.id!=null){
        RepositoryService.save(this.standardExpense, 'STDEP').then(function (data) {
            $scope.standardExpense.id = data.id;
            alert('Saved Successfully');
        });
        }else{
        RepositoryService.create($scope.standardExpense, 'STDEP').success(function (){
            $scope.standardExpense = {};
            alert('Created Successfully');
        });
        }
    };

    $scope.isDisabled=function() {

        if (!$scope.standardExpense.expenseType)
            return "non";

        if ($scope.standardExpense.expenseType.id == 'SYSLVEXP00000000000000000000000000001')
            return 'loc';

        if ($scope.standardExpense.expenseType.id == 'SYSLVEXP00000000000000000000000000002')
            return 'dist';

        if ($scope.standardExpense.expenseType.id == 'SYSLVEXP00000000000000000000000000003')
            return 'sun';
    };

    $scope.setMaxVal = function () {
        var chk = document.getElementById("setMax");
        if (chk.checked) {
            $scope.expenseValue.maxValue = Number.MAX_SAFE_INTEGER
            $scope.standardExpense.expenseValue = $scope.expenseValue;
            document.getElementById("maxVal").disabled = true
        }
        else {
            $scope.expenseValue.maxValue = ""
            $scope.standardExpense.expenseValue = $scope.expenseValue;
            document.getElementById("maxVal").disabled = false;
        }
    }
});

sfe.controller('TourPlanApprovalController', function ($scope, $state, $http, $stateParams, LocalStore, RepositoryService, LabelService) {
    $scope.planId = '';
    $scope.tourplan = {};
    $scope.labels = {};
    $scope.status = '';
    $scope.statusList = [{'id':'approved', 'name':'Approve'}, {'id':'draft', 'name': 'Draft'}];

    $scope.init = function(){
        $scope.planId = $stateParams.id;
        $scope.labels = LabelService.getLabels('tourplanApproval');
        if($stateParams.id != null){
            RepositoryService.restore($stateParams.id).then(function(data){
                $scope.tourplan = data.data;
            })
        }
    }



    $scope.updateStatus = function() {
        $http({
            method: 'PUT',
            url: restUrl + "/sfe/tourplan/adminApprove/" + $scope.planId+"?status="+$scope.status,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        }).then(function(data){

            RepositoryService.restore($scope.planId).then(function(data){
                $scope.tourplan = data.data;
            })
        })
    }
});



sfe.controller('ExpenseApprovalEditController', function ($scope, $state, $http, $stateParams, LocalStore, RepositoryService, LabelService) {

    $scope.init = function () {
        $scope.expenseId = $stateParams.id;
        $scope.labels = LabelService.getLabels('expenseApprovalEdit');
        console.log($scope.labels);
        if ($stateParams.id != null) {
            RepositoryService.restore($stateParams.id).then(function (data) {
                $scope.expense = data.data;
            })
        }
    }

    $scope.updateStatus = function () {
        if ($scope.newStatus != null && $scope.newStatus != '') {
            $http({
                method: 'GET',
                url: restUrl + "/expense/expenseService/changeExpenseStatus/" + $scope.expenseId + "/" + $scope.newStatus,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            }).success(function (response) {
                $scope.expense.status={"id":"USRLVEXPSTS00000000000000000000000001","displayName":"DRAFT"};
            })
        }
    }
});

sfe.controller('DistanceController',function ($scope, $state, $http, $stateParams, LocalStore, RepositoryService, LabelService) {
    $scope.labels = LabelService.getLabels("distanceMaster");
    $scope.links = LabelService.getLabels("links");
    $scope.stateList=[];
    $scope.fromTowns=[];
    $scope.toTowns=[];
    $scope.disabled=false;
    $scope.showAlert=false;
    $scope.distance={};
    $scope.fromState;
    $scope.toState;
    $scope.init = function() {

        console.log($scope)
        RepositoryService.find("state",{},'simple').then(function (data) {
            $scope.stateList = data.data;
        });

        if($stateParams.id){
            RepositoryService.restore($stateParams.id).then(function (data) {
                $scope.distance = data.data;

                RepositoryService.restore($scope.distance.fromLocation.id).then(function (data) {
                    $scope.fromState=data.data.state.id;

                    RepositoryService.restore($scope.distance.fromLocation.id).then(function (data) {
                        $scope.toState=data.data.state.id;

                        if($scope.fromState==$scope.toState)
                        {
                            RepositoryService.find("towns",{'state':$scope.fromState},'simple').then(function (data) {
                                $scope.fromTowns = data.data;
                                $scope.toTowns = data.data;
                            });
                        }
                        else
                        {
                            RepositoryService.find("towns",{'state':$scope.fromState},'simple').then(function (data) {
                                $scope.fromTowns = data.data;
                                RepositoryService.find("towns",{'state':$scope.toState},'simple').then(function (data) {

                                    $scope.toTowns = data.data;
                                });
                            });
                        }
                    })
                })
            });
        }
    }

    $scope.getTowns=function(stateId,dir){
        RepositoryService.find("towns",{'state':stateId},'simple').then(function (data) {

            if(dir==='fromTowns') {
                $scope.fromTowns = data.data;
            }
            else{
                $scope.toTowns=data.data;
            }
        });
    }

    $scope.getDistance=function(fromTown,toTown){

        if(fromTown==toTown){
            alert("You have selected same city.");
            $scope.disabled=true;
        }
        else{
            $http({
                method: 'GET',
                url: restUrl + "/expense/expenseService/getDistance/"+fromTown+"/"+toTown,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            }).then(function(data) {
                var distance=data.data;
                if(distance.id){
                    $scope.disabled=false;
                    $scope.showAlert=true;
                    $scope.distance.kms=distance.kms;
                    $scope.distance.id=distance.id;
                    $scope.distance.fromToLoc=distance.fromToLoc;
                    $scope.distance.toFromLoc=distance.toFromLoc;
                }else{
                    $scope.distance.fromToLoc=fromTown+"~"+toTown;
                    $scope.distance.toFromLoc=toTown+"~"+fromTown;
                    $scope.disabled=false;
                }
            });
        }
    }

    $scope.save=function(){
        if($scope.distance.id!=null){

         RepositoryService.save($scope.distance, 'DISTN').success(function(data){
                    $scope.distance.id = data.id;
                    alert('Saved Successfully');
            });
        }else{

        RepositoryService.create($scope.distance,'DISTN').success(function (){
                    $scope.distance = {};
                    alert ('Created successfully');
        });

        }

    }
});

sfe.controller('BrandGroupController',function ($scope, $state, $http, $stateParams, LocalStore, RepositoryService, LabelService) {

    $scope.brands=[];
    $scope.init=function(){
        RepositoryService.find("brand",{'sortBy':'name','sortOrder':'ASC'},'simple').then(function (data) {
            $scope.brands=data.data;
            console.log($scope.brands);
        })
    }
});

sfe.controller('downloadRequest',function($scope, $state, $http, $stateParams, LocalStore, RepositoryService, LabelService) {

    $scope.towndisplayMsg = "";
    $scope.distdisplayMsg = "";
    $scope.divisions_field = {};
    $scope.divisions_dml = {};
    $scope.zones_dml = {};
    $scope.zones_field = {};
    $scope.classification = {};
    $scope.speciality = {};
    $scope.regionList = {};
    $scope.territoryList = {};
    $scope.jobTitles = {};

    $scope.init = function () {
        RepositoryService.find("divsn", {}, 'simple').then(function (data) {
            $scope.divisions_field = data.data;
            $scope.divisions_dml = data.data;
        });

        RepositoryService.find("usrlv", {type: 'DOCTOR_SPECIALTY', paged: 'N'}, 'advanced').then(function (data) {
            $scope.speciality = data.data;
        });

        RepositoryService.find("usrlv", {type: 'DOCTOR_CLASSIFICATION', paged: 'N'}, 'advanced').then(function (data) {
            $scope.classification = data.data;
        });

        RepositoryService.find("jobtt", {}, 'advanced').then(function (data) {
            $scope.jobTitles = data.data;
        });
    }

    $scope.getZone = function (id, dir) {
        RepositoryService.find('divsn_zone', {
            hid: 'ORGHY00000000000000000000000000000001',
            division: id
        }, 'advance').then(function (data) {
            if (dir === 'field')
                $scope.zones_field = data.data;
            else if (dir == 'cdml')
                $scope.zones_cdml = data.data;
            else
                $scope.zones_dml = data.data;
        })
    }

    $scope.getChild = function (id, level) {
        RepositoryService.find('locat', {'parent': id}, 'simple').then(function (data) {
            if (level == 2) {
                $scope.regionList = data.data;
            } else {
                $scope.territoryList = data.data;
            }
        })
    }

    $scope.downFieldStructure = function () {

        var query = "";
        if ($scope.field_div != null)
            if (query == "")
                query = "division~" + $scope.field_div;
            else
                query = query + "^division~" + $scope.field_div;

        if ($scope.field_zone != null)
            if (query == "")
                query = "zone~" + $scope.field_zone;
            else
                query = query + "^zone~" + $scope.field_zone;

        window.open("/webapp/download/report?cert=" + encodeURIComponent(LocalStore.fetch("USER_INFO").certificate) + "&queryString=" + query + "&query=fieldStructure&downloadType=excel");

    }

    $scope.downloadDML = function () {
        if ($scope.dml_speciality == null && ($scope.dml_div == null || $scope.dml_zone == null)) {
            alert("Please select minimum division and zone filters")
        }
        else {
            var query = "";
            if ($scope.dml_div != null)
                if (query == "")
                    query = "division~" + $scope.dml_div;
                else
                    query = query + "^division~" + $scope.dml_div;

            if ($scope.dml_zone != null)
                if (query == "")
                    query = "zone~" + $scope.dml_zone;
                else
                    query = query + "^zone~" + $scope.dml_zone;


            if ($scope.dml_region != null)
                if (query == "")
                    query = "region~" + $scope.dml_region;
                else
                    query = query + "^region~" + $scope.dml_region;


            if ($scope.dml_speciality != null)
                if (query == "")
                    query = "speciality~" + $scope.dml_speciality;
                else
                    query = query + "^speciality~" + $scope.dml_speciality;


            if ($scope.dml_territory != null)
                if (query == "")
                    query = "territory~" + $scope.dml_territory;
                else
                    query = query + "^territory~" + $scope.dml_territory;

            if ($scope.dml_classification != null)
                if (query == "")
                    query = "classification~" + $scope.dml_classification;
                else
                    query = query + "^classification~" + $scope.dml_classification;

            window.open("/webapp/download/report?cert=" + encodeURIComponent(LocalStore.fetch("USER_INFO").certificate) + "&queryString=" + query + "&query=dml&downloadType=excel");
        }
    }

    $scope.downloadDMLwithUID = function () {


        var query = "";
        window.open("/webapp/download/report?cert=" + encodeURIComponent(LocalStore.fetch("USER_INFO").certificate) + "&queryString=" + query + "&query=dml_uid&downloadType=excel");

    }



    $scope.downloadChemistDML=function() {
        if ($scope.cdml_div == null || $scope.cdml_zone == null) {
            alert("Please select minimum division and zone filters")
        }
        else {
            var query = "";
            if ($scope.cdml_div != null)
                if (query == "")
                    query = "division~" + $scope.cdml_div;
                else
                    query = query + "^division~" + $scope.cdml_div;

            if ($scope.cdml_zone != null)
                if (query == "")
                    query = "zone~" + $scope.cdml_zone;
                else
                    query = query + "^zone~" + $scope.cdml_zone;


            if ($scope.cdml_region != null)
                if (query == "")
                    query = "region~" + $scope.cdml_region;
                else
                    query = query + "^region~" + $scope.cdml_region;


            if ($scope.cdml_territory != null)
                if (query == "")
                    query = "territory~" + $scope.cdml_territory;
                else
                    query = query + "^territory~" + $scope.cdml_territory;

            window.open("/webapp/download/report?cert="+encodeURIComponent(LocalStore.fetch("USER_INFO").certificate)+"&queryString="+query+"&query=chemistDml&downloadType=excel");
        }
    }
            $scope.downloadReq=function(dir){

        if(dir=='town'){
            var fdate=moment($scope.townReq.fromDate,"DD/MM/YYYY");
            var fromDate=fdate.get('year')+"-"+(fdate.get('month')+1)+"-"+fdate.get('date');
            var tdate=moment($scope.townReq.toDate,"DD/MM/YYYY");
            var toDate=tdate.get('year')+"-"+(tdate.get('month')+1)+"-"+tdate.get('date');
            var query="fromDate~"+fromDate+"^toDate~"+toDate;
            window.open("/webapp/download/report?cert="+encodeURIComponent(LocalStore.fetch("USER_INFO").certificate)+"&queryString="+query+"&query=twnrq_rpt&downloadType=excel");
        }else{
            var fdate=moment($scope.distReq.fromDate,"DD/MM/YYYY");
            var fromDate=fdate.get('year')+"-"+(fdate.get('month')+1)+"-"+fdate.get('date');
            var tdate=moment($scope.distReq.toDate,"DD/MM/YYYY");
            var toDate=tdate.get('year')+"-"+(tdate.get('month')+1)+"-"+tdate.get('date');
            var query="fromDate~"+fromDate+"^toDate~"+toDate;
            window.open("/webapp/download/report?cert="+encodeURIComponent(LocalStore.fetch("USER_INFO").certificate)+"&queryString="+query+"&query=dstrq_rpt&downloadType=excel");
        }
    }
    $scope.productivityStatus=function(){
        window.open("/webapp/download/report?cert="+encodeURIComponent(LocalStore.fetch("USER_INFO").certificate)+"&query=productivityStatus&downloadType=excel");
    }

    $scope.nonPrescriberStatus=function(){
        window.open("/webapp/download/report?cert="+encodeURIComponent(LocalStore.fetch("USER_INFO").certificate)+"&query=nonPrescriberStatus&downloadType=excel");
    }
    $scope.productivityData=function(){
        window.open("/webapp/download/report?cert="+encodeURIComponent(LocalStore.fetch("USER_INFO").certificate)+"&query=productivityData&downloadType=excel");
    }

    $scope.brandReport=function(){
        window.open("/webapp/download/report?cert="+encodeURIComponent(LocalStore.fetch("USER_INFO").certificate)+"&query=productReport&downloadType=excel");
    }

    $scope.competitorBrandReport=function(){
        window.open("/webapp/download/report?cert="+encodeURIComponent(LocalStore.fetch("USER_INFO").certificate)+"&query=competitorBrandReport&downloadType=excel");
    }

    $scope.downloadReports=function(downloadType){
        var cnt=0;
        var errors=["Please select valid report type","Please select division","Please select From and To dates","From and To dates should be in same month"];
        var fdate=moment($scope.rptDownload_fromDate,"DD/MM/YYYY");
        var tdate=moment($scope.rptDownload_toDate,"DD/MM/YYYY");

        if($scope.rptDownload_type==null || $scope.rptDownload_type==""){
            cnt=1;
        }else if($scope.rptDownload_div==null || $scope.rptDownload_div==""){
            cnt=2;
        }else if($scope.rptDownload_toDate==null || $scope.rptDownload_fromDate==null){
            cnt=3;
        }else if(fdate.get('month')!=tdate.get('month') || fdate.get('year')!=tdate.get('year')){
            cnt=4;
        }

        if(cnt>0){
            alert(errors[cnt-1])
        }else{
            var query="";
            if ($scope.rptDownload_div != null)
                if (query == "")
                    query = "division~" + $scope.rptDownload_div;
                else
                    query = query + "^division~" + $scope.rptDownload_div;

            if ($scope.rptDownload_zone != null)
                if (query == "")
                    query = "zone~" + $scope.rptDownload_zone;
                else
                    query = query + "^zone~" + $scope.rptDownload_zone;

            if ($scope.rptDownload_region != null)
                if (query == "")
                    query = "region~" + $scope.rptDownload_region;
                else
                    query = query + "^region~" + $scope.rptDownload_region;

            if ($scope.rptDownload_territory != null)
                if (query == "")
                    query = "hq~" + $scope.rptDownload_territory;
                else
                    query = query + "^hq~" + $scope.rptDownload_territory;


            var fromDate=fdate.get('year')+"-"+(fdate.get('month')+1)+"-"+fdate.get('date');
            var toDate=tdate.get('year')+"-"+(tdate.get('month')+1)+"-"+tdate.get('date');

            if (query == "")
                 query="fromDate~"+fromDate+"^toDate~"+toDate;
            else
                query = query + "^fromDate~"+fromDate+"^toDate~"+toDate;

            window.open("/webapp/download/report?cert="+encodeURIComponent(LocalStore.fetch("USER_INFO").certificate)+"&queryString="+query+"&query="+$scope.rptDownload_type+"&downloadType="+downloadType);
        }
    }

    $scope.downloadVisitReports=function(downloadType){
        if($scope.rptdocVisit_div!=null && $scope.rptdocVisit_toDate!=null && $scope.rptdocVisit_fromDate!=null && $scope.rptdocVisit_toDate!="" && $scope.rptdocVisit_fromDate!=""){
            var fdate=moment($scope.rptdocVisit_fromDate,"DD/MM/YYYY");
            var tdate=moment($scope.rptdocVisit_toDate,"DD/MM/YYYY");
            var fromDate=fdate.get('year')+"-"+(fdate.get('month')+1)+"-"+fdate.get('date');
            var toDate=tdate.get('year')+"-"+(tdate.get('month')+1)+"-"+tdate.get('date');

            var query="division~" + $scope.rptdocVisit_div+"^fromDate~"+fromDate+"^toDate~"+toDate;

            if($scope.jobTitle!=null && $scope.jobTitle.trim()!="") {
               query= query+"^jobTitle~" + $scope.jobTitle;
            }

            if($scope.rptdocVisit_activity!=null && $scope.rptdocVisit_activity.trim()!="") {
                query= query+"^activity~" + $scope.rptdocVisit_activity;
            }

            if($scope.rptdocVisit_isKol!=null && $scope.rptdocVisit_isKol.trim()!="") {
                query= query+"^kol~" + $scope.rptdocVisit_isKol;
            }

            window.open("/webapp/download/report?cert="+encodeURIComponent(LocalStore.fetch("USER_INFO").certificate)+"&queryString="+query+"&query=doctor_visit"+"&downloadType="+downloadType);
        }else{
            alert("Please select all required fields");
        }
    }

    $scope.downloadSpecialityCoverageReport=function(downloadType) {
        if ($scope.rptDownload_toDate != null && $scope.rptDownload_fromDate != null && $scope.rptDownload_toDate != "" && $scope.rptDownload_fromDate != "") {
            var fdate = moment($scope.rptDownload_fromDate, "DD/MM/YYYY");
            var tdate = moment($scope.rptDownload_toDate, "DD/MM/YYYY");
            var fromDate = fdate.get('year') + "-" + (fdate.get('month') + 1) + "-" + fdate.get('date');
            var toDate = tdate.get('year') + "-" + (tdate.get('month') + 1) + "-" + tdate.get('date');

            var query = "^fromDate~" + fromDate + "^toDate~" + toDate;

            if ($scope.rptDownload_div != null && $scope.rptDownload_div.trim() != "") {
                query = query + "^division~" + $scope.rptDownload_div;
            }
            if ($scope.rptDownload_zone != null && $scope.rptDownload_zone.trim() != "") {
                query = query + "^zone~" + $scope.rptDownload_zone;
            }
            if ($scope.rptDownload_region != null && $scope.rptDownload_region.trim() != "") {
                query = query + "^region~" + $scope.rptDownload_region;
            }
            if ($scope.rptDownload_territory != null && $scope.rptDownload_territory.trim() != "") {
                query = query + "^hq~" + $scope.rptDownload_territory;
            }
            window.open("/webapp/download/report?cert=" + encodeURIComponent(LocalStore.fetch("USER_INFO").certificate) + "&queryString=" + query + "&query=specialityCoverage" + "&downloadType=" + downloadType);
        }
        else{
            alert("Please select from date and to date");
        }

    }

    $scope.getActivity=function(division){
        RepositoryService.find("actvy",{owner: division},'advanced').then(function(data){
            $scope.activities = data.data;
        });
    }

    $scope.downloadRcpa=function(downloadType){
        if($scope.rcpa_type!=null && $scope.rcpaOwn_div!=null && $scope.rcpaOwn_toDate!=null && $scope.rcpaOwn_fromDate!=null && $scope.rcpaOwn_toDate!="" && $scope.rcpaOwn_fromDate!=""){
            var fdate=moment($scope.rcpaOwn_fromDate,"DD/MM/YYYY");
            var tdate=moment($scope.rcpaOwn_toDate,"DD/MM/YYYY");
            var fromDate=fdate.get('year')+"-"+(fdate.get('month')+1)+"-"+fdate.get('date');
            var toDate=tdate.get('year')+"-"+(tdate.get('month')+1)+"-"+tdate.get('date');

            var query="division~" + $scope.rcpaOwn_div+"^fromDate~"+fromDate+"^toDate~"+toDate;
            window.open("/webapp/download/report?cert="+encodeURIComponent(LocalStore.fetch("USER_INFO").certificate)+"&queryString="+query+"&query="+$scope.rcpa_type+"&downloadType="+downloadType);
        }else{
            alert("Please select all required fields");
        }
    }

    $scope.downloadJointVisit=function(downloadType){
        if($scope.jointVisit_div==null || $scope.jointVisit_div.trim()==""){
            alert("Please Select Division");
        }else if($scope.jointVisit_fromDate==null || $scope.jointVisit_toDate==null){
            alert("Please Select From and To dates");
        }else{
            var fdate=moment($scope.jointVisit_fromDate,"DD/MM/YYYY");
            var tdate=moment($scope.jointVisit_toDate,"DD/MM/YYYY");
            var fromDate=fdate.get('year')+"-"+(fdate.get('month')+1)+"-"+fdate.get('date');
            var toDate=tdate.get('year')+"-"+(tdate.get('month')+1)+"-"+tdate.get('date');

            var query="division~" + $scope.jointVisit_div+"^fromDate~"+fromDate+"^toDate~"+toDate;

            if($scope.jointVisit_jobtitle!=null && $scope.jointVisit_jobtitle.trim()!=""){
                query=query+"^jobtitle~"+$scope.jointVisit_jobtitle;
            }
            window.open("/webapp/download/report?cert="+encodeURIComponent(LocalStore.fetch("USER_INFO").certificate)+"&queryString="+query+"&query=joint_visit&downloadType="+downloadType);
        }
    }

    $scope.downloadNonCallReport=function(downloadType){
        var query="";
        if($scope.nonCall_div!=null && $scope.nonCall_div.trim()!=""){
            var query="division~" + $scope.nonCall_div;
        }

        if($scope.nonCall_fromDate==null || $scope.nonCall_toDate==null){
            alert("Please Select From and To dates");
        }else{
            var fdate=moment($scope.nonCall_fromDate,"DD/MM/YYYY");
            var tdate=moment($scope.nonCall_toDate,"DD/MM/YYYY");
            var fromDate=fdate.get('year')+"-"+(fdate.get('month')+1)+"-"+fdate.get('date');
            var toDate=tdate.get('year')+"-"+(tdate.get('month')+1)+"-"+tdate.get('date');

            var query=query+"^fromDate~"+fromDate+"^toDate~"+toDate;


            window.open("/webapp/download/report?cert="+encodeURIComponent(LocalStore.fetch("USER_INFO").certificate)+"&queryString="+query+"&query=non_call_admin_report&downloadType="+downloadType);
        }
    }
})


sfe.controller('IssueRequestController',function($scope, $http,$state, $stateParams, TourPlanService,SearchConfigService,
                                                 RepositoryService,SearchFormService, LabelService, LocalStore){

    $scope.links = LabelService.getLabels('links');
    $scope.labels = LabelService.getLabels('issue');
    $scope.definition={};
    $scope.modules={};
    $scope.types={};
    $scope.issueStatus={};

    $scope.init=function(){
        if($stateParams.id!=null){
            var response = RepositoryService.restore($stateParams.id);
            response.then(function(data){
                $scope.definition = data.data;
            })

            RepositoryService.find("usrlv",{type: 'ISSUE_TYPE', paged:'N'},'advanced').then(function(data){
                $scope.types = data.data;
            });

            RepositoryService.find("syslv",{type: 'ISSUE_STATUS', paged:'N'},'advanced').then(function(data){
                $scope.issueStatus = data.data;
            });

        }
    }

    $scope.save=function(){
        RepositoryService.save(this.definition, 'ISSUE').success(function(msg){

        });
    }
})

sfe.controller('unlockRequestController',function($scope,$state, $http,LabelService, RepositoryService, LocalStore, $modal,daysOfWeek,months,UIService){
    $scope.years = [];
    $scope.currentYear;
    $scope.currentMonth;
    $scope.currentDate;
    $scope.monthList=[];
    $scope.selectedYear;
    $scope.prvLockData=[];
    $scope.lockData=[];
    $scope.activeLock={};
    $scope.showPrvLocks=false;
    $scope.showTable=false;
    $scope.configuration={};
    $scope.adhocConfiguration={};

    $scope.init=function() {
        var date=new Date;
        $scope.currentYear=date.getFullYear();
        $scope.years.push($scope.currentYear-1);
        $scope.years.push($scope.currentYear);
        $scope.monthList=months;
        $scope.currentMonth=date.getMonth()+1;
    }

    $scope.search=function(type){
        $scope.showPrvLocks=false;
        $scope.showTable=false;
        $scope.configuration={};
        $scope.adhocConfiguration={};
        $scope.prvLockData=[];
        $scope.lockData=[];

        var user=null;
        if(type==='MOBILE_REPORTING' && $scope.unlockUser_DL==null){
            alert("please select user");
        }else if(type==='ADHOC_UNLOCK' && $scope.unlockUser_PL==null){
            alert("please select user");
        }
        else{
            if(type==='MOBILE_REPORTING'){
                user=$scope.unlockUser_DL;
            }else{
                user=$scope.unlockUser_PL;
            }
            RepositoryService.getCurrentDate().success(function(data){
                $http({
                    method: 'GET',
                    url: restUrl + "/sfe/locakservice/getLockRecord/"+user+"/"+data.date+"/"+type,
                    headers: {
                        "content-type": "application/json",
                        "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                    }
                }).then(function(data) {
                    RepositoryService.getCurrentDate().success(function(cdata){
                        var currentDate=new Date(cdata.date).setHours(0,0,0,0);
                        if(type==='MOBILE_REPORTING') {
                            $scope.showTable=true;
                            $scope.currentDate = currentDate;
                            for (d in data.data) {
                                var ud = data.data[d];
                                if (ud.validFrom.date <= currentDate && ud.validUpto.date > currentDate && ud.status == 0) {
                                    $scope.configuration = ud;
                                } else {
                                    $scope.lockData.push(ud);
                                }
                            }
                            if ($scope.configuration.id == null) {
                                RepositoryService.find("emp_userId", {empCode: $scope.unlockUser_DL}, 'simple').success(function (data) {
                                    $scope.configuration.owner = {id: data[0].id};
                                    $scope.configuration.recurrenceFrequency = 0;
                                    $scope.configuration.status = true
                                })
                            }
                        }else{
                            $scope.showPrvLocks=true;
                            $scope.currentDate = currentDate;
                            for (d in data.data) {
                                var ud = data.data[d];

                                if (ud.validFrom.date <= currentDate && ud.validUpto.date > currentDate && ud.status == 0
                                    && ud.unlockFrom.text==$scope.adhoc_unlockFrom && ud.unlockUpto.text==$scope.adhoc_unlockUpto
                                ) {
                                    $scope.adhocConfiguration = ud;
                                } else {
                                    $scope.prvLockData.push(ud);
                                }
                            }
                            if ($scope.adhocConfiguration.id == null) {
                                RepositoryService.find("emp_userId", {empCode: $scope.unlockUser_PL}, 'simple').success(function (data) {
                                    $scope.adhocConfiguration.owner = {id: data[0].id};
                                    $scope.adhocConfiguration.recurrenceFrequency = 0;
                                    $scope.adhocConfiguration.status = true
                                })
                            }
                        }
                    })
                })
            });
        }
    }
    $scope.unlock=function(type){
        if(type=='mobile_reporting'){
            var cdate=new Date($scope.currentDate);
            RepositoryService.find("max_activity_date",{"employeeCode":$scope.unlockUser_DL,"year":cdate.getFullYear(),"month":cdate.getMonth()+1},'simple').success(function(data){
                var dateUpTo=new Date($scope.currentDate +  (2 * 24 * 60 * 60 * 1000));
                var unlockFrom;
                var activityDate=null;
                var ncActivityDate=null;

                if(data[0]!=null) {
                    activityDate = data[0].ACTIVITY_DATE;
                }
                if(data[1]!=null){
                    ncActivityDate=data[1].ACTIVITY_DATE;
                }

                $scope.configuration.id=null;
                $scope.configuration.name=type;
                $scope.configuration.type='MOBILE_REPORTING';
                $scope.configuration.status=false;
                $scope.configuration.validFrom={"text":cdate.getDate()+"/"+(cdate.getMonth()+1)+"/"+cdate.getFullYear()};
                $scope.configuration.validUpto={"text":dateUpTo.getDate()+"/"+(dateUpTo.getMonth()+1)+"/"+dateUpTo.getFullYear()};

                if(activityDate==null && ncActivityDate==null){
                    unlockFrom={"text":"01/"+(dateUpTo.getMonth()+1)+"/"+dateUpTo.getFullYear()};
                }
                else if(activityDate==null){
                    unlockFrom=ncActivityDate;
                }else if(ncActivityDate==null){
                    unlockFrom=activityDate;
                }else if(activityDate.date<ncActivityDate.data){
                    unlockFrom=activityDate;
                }else if(activityDate.date>ncActivityDate.data){
                    unlockFrom=ncActivityDate;
                }else{
                    unlockFrom=activityDate;
                }

                $scope.configuration.unlockFrom={"text":unlockFrom.text};
                $scope.configuration.unlockUpto={"text":cdate.getDate()+"/"+(cdate.getMonth()+1)+"/"+cdate.getFullYear()};

                RepositoryService.save($scope.configuration, 'CONFG').success(function(data){
                    $scope.configuration.id = data.id;
                    alert("Reporting is unlocked for this user");
                });
            })
        }else{
            var cdate=new Date($scope.currentDate);
            var dateUpTo=new Date($scope.currentDate +  (2 * 24 * 60 * 60 * 1000));
            $scope.adhocConfiguration.id=null;
            $scope.adhocConfiguration.name=type;
            $scope.adhocConfiguration.type='ADHOC_UNLOCK';
            $scope.adhocConfiguration.status=false;
            $scope.adhocConfiguration.validFrom={"text":cdate.getDate()+"/"+(cdate.getMonth()+1)+"/"+cdate.getFullYear()};
            $scope.adhocConfiguration.validUpto={"text":dateUpTo.getDate()+"/"+(dateUpTo.getMonth()+1)+"/"+dateUpTo.getFullYear()};
            $scope.adhocConfiguration.unlockFrom={"text":$scope.adhoc_unlockFrom};
            $scope.adhocConfiguration.unlockUpto={"text":$scope.adhoc_unlockUpto};

            RepositoryService.save($scope.adhocConfiguration, 'CONFG').success(function(data){
                $scope.adhocConfiguration.id = data.id;
                alert("Reporting is unlocked for this user");
            });
        }
    }

    $scope.lock=function(type){
        if(type=='mobile_reporting') {
            $scope.configuration.status = true;
            RepositoryService.save($scope.configuration, "CONFG").success(function (data) {
                    alert("Reporting is locked for this user");
                }
            );
        }else{
            $scope.adhocConfiguration.status = true;
            RepositoryService.save($scope.adhocConfiguration, "CONFG").success(function (data) {
                    alert("Reporting is locked for this user");
                }
            );
        }
    }
});


sfe.controller("SLController",function($scope,$state, $http,LabelService, RepositoryService, LocalStore, $modal,daysOfWeek,months,UIService){
    $scope.jobTitles={};
    $scope.results=[];

    $scope.init=function(){
        RepositoryService.find("jobtt",{},'advanced').then(function(data){
            $scope.jobTitles = data.data;
        });
    }

    $scope.search=function(){
        RepositoryService.find("slmst",{jobTitle:$scope.jobTitle,'sortBy':'SEQUENCE','sortOrder':'ASC',"paged":'N'},'advanced').then(function(data){
            $scope.results = data.data;
            $scope.results.push({id:null,designation:{id:$scope.jobTitle},sequence:$scope.results.length+1,title:null,benchmark:null,status:{id:"USRLVSLST0000000000000000000000000001"}})
        });
    };

    $scope.add=function(){
        var res=$scope.results[$scope.results.length-1];
        if(res.title==null || !res.title.trim() || res.benchmark==null || !res.benchmark.trim()){
            alert("Please fill up requested data before adding new row");
        }else {
            $scope.results.push({
                id: null,
                designation: {id: $scope.jobTitle},
                sequence: $scope.results.length + 1,
                title: null,
                benchmark: null,
                status: {id: "USRLVSLST0000000000000000000000000001"}
            })
        }
    }

    $scope.save=function(){
        $http({
            method: 'POST',
            data:$scope.results,
            url: restUrl + "/miscellaneous/miscellaneousService/saveSl",
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        }).success(function (response) {
            $scope.results=response
        })
    }

    $scope.updateStatus=function(data){
        var cnf=false;
        if(data.status.id=='USRLVSLST0000000000000000000000000001') {
            cnf = confirm("Deacivate this benchmark?");
        }else if(data.status.id=='USRLVSLST0000000000000000000000000002') {
            cnf = confirm("Activate this benchmark?");
        }
        if(cnf){
            if(data.status.id=='USRLVSLST0000000000000000000000000001') {
                data.status.id='USRLVSLST0000000000000000000000000002';
            }else{
                data.status.id='USRLVSLST0000000000000000000000000001';
            }

            $http({
                method: 'POST',
                data:data,
                url: restUrl + "/miscellaneous/miscellaneousService/updateSl",
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            }).success(function (response) {
                if(data.status.id=='USRLVSLST0000000000000000000000000001') {
                    alert("Benchmark is activate successfully");
                }else{
                    alert("Benchmark is deactivate successfully");
                }
            })
        }else{
            if(data.status.id=='USRLVSLST0000000000000000000000000001') {
                $('#'+data.id).prop('checked', true);
            }else{
                $('#'+data.id).prop('checked', false);
            }

        }
    }
})



sfe.controller('RoleConfigController',function($scope,$state, $http,LabelService,
                                               RepositoryService, LocalStore, $stateParams){

    $scope.definition={};
    $scope.divisions=[];
    $scope.jobTitles = [];
    $scope.roles = [];

    $scope.links = LabelService.getLabels('links');
    $scope.labels = LabelService.getLabels('roleConfiguration');

    $scope.init = function(){
        RepositoryService.find('divsn', {}, 'simple').then(function(data){
            $scope.divisions = data.data;
            $scope.divisions.push({'id' : 'DIVSN0000000000000000000000000000000d', 'name' : 'Default Configuration'});
        });

        RepositoryService.find('jobtt', {}, 'simple').then(function(data){
            $scope.jobTitles = data.data;
        });

        RepositoryService.find('srole', {}, 'simple').then(function(data){
            $scope.roles = data.data;
        });

        if($stateParams.id!=null){
            var response = RepositoryService.restore($stateParams.id);
            response.then(function(data){
                $scope.definition = data.data;
            });
        }
    }


    $scope.save = function(){
        if($scope.definition.name==null){
        return false;
        }
        if($scope.definition.id!=null){

            RepositoryService.save($scope.definition, 'RCONF').success(function(response){
                $scope.definition.id = response.id;
                alert('Saved successfully');
            });
        }else{
            RepositoryService.create($scope.definition, 'RCONF').success(function(){
                $scope.definition={};
                alert('Created successfully');
            });

        }
    }
})


sfe.controller('leaveDetailController',function($scope,$state, $http,LabelService,
                                               RepositoryService, LocalStore,LeaveService, $stateParams){
    $scope.empCode="";
    $scope.leaveData={};
    $scope.leaveBalance={};
    $scope.years=[];
    $scope.year;
    $scope.init=function(){
        RepositoryService.getCurrentDate().success(function(cdata){
            var currentDate=new Date(cdata.date);
            var year=currentDate.getFullYear();
            $scope.year=year;
            $scope.years.push(year-2);
            $scope.years.push(year-1);
            $scope.years.push(year);
            $scope.years.push(year+1);
        });
    };

    $scope.search=function(){
        if($scope.empCode==null || $scope.empCode.trim()==''){
            alert("Please Enter User Code");
        }else {
            RepositoryService.find('leave_details', {'empCode': $scope.empCode, 'leaveYear': $scope.year}, 'advanced').success(function (leaveData) {
                $scope.leaveData = leaveData;
                console.log(leaveData);
                RepositoryService.find('leavb_emply', {'empCode': $scope.empCode, 'previousYear': $scope.year}, 'advanced').success(function (leaveBalance) {
                    $scope.leaveBalance = leaveBalance;
                })
            })
        }
    };

    $scope.updateLeave=function(leave){
        RepositoryService.save(leave, 'LEAVB').success(function(data){alert("Leave Balance Updated Successfully")});
    }

     $scope.approve = function(id){
                LeaveService.submitLeave(id,'approved').then(function(d){
                    $scope.search();
                });
     };

    $scope.cancelLeave=function(leave){
        var cnf=confirm("You want to cancel this leave?");
        if(cnf) {
            leave.status = {id: "SYSLV50000000000000000000000000000114"};
            RepositoryService.save(leave, 'LEAVE').success(function (data) {
                alert("Leave Cancelled Successfully");
                $scope.search();
            });
        }
    }
})

sfe.controller('doctorSurveyUnlockController',function($scope,$state, $http,LabelService,RepositoryService, LocalStore, $stateParams){

    $scope.quarters=[];
    $scope.quarter=null;
    $scope.years=[];
    $scope.year;
    $scope.employee;
    $scope.surveyResult={};

     $scope.init=function(){
           RepositoryService.find("usrlv", {type: 'SL_QUARTERS', paged: 'N'}, 'advanced').then(function (data) {
               $scope.quarters = data.data;
               RepositoryService.getCurrentDate().success(function(data){
                 var date=new Date(data.date);
                 $scope.currentYear=date.getFullYear();
                   $scope.year=$scope.currentYear;
                   $scope.years.push($scope.currentYear-1);
                   $scope.years.push($scope.currentYear);
                   $scope.years.push($scope.currentYear+1);
               })
           })
       };

 $scope.changetodraft=function(masterID){
                 $http({
                 method: 'get',
                 url: restUrl + "/miscellaneous/miscellaneousService/changeToDraft/" + masterID ,
                 headers: {
                     "content-type": "application/json",
                     "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                 }
             }).success(function(data){
                     alert("Doctor Survey change to draft.");
                $scope.surveyResult.status.id="USRLVDOCSRVSTAT0000000000000000000001";

             });
 };

 $scope.search=function(){

        if($scope.quarter==null || $scope.quarter.trim()=="")
            alert("Please Select Quarter");
        if($scope.empCode==null || $scope.empCode.trim()==''){
            alert("Please Enter User Code");
        }else {
                RepositoryService.find('emply_simple', {'code': $scope.empCode}, 'simple').success(function (data) {
                $scope.employee=data;
                        $http({
                        method: 'get',
                        url: restUrl + "/miscellaneous/miscellaneousService/getfocusedbrandinput/" + $scope.employee[0].id + "/" + $scope.quarter+"/"+$scope.year+"/employee",
                        headers: {
                            "content-type": "application/json",
                            "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                        }
                         }).success(function (data) {
                            $scope.surveyResult=data[0];
                    })
                })
        }
    }
})


sfe.controller('LeaveReportController',function($scope,$state, $http,LabelService,RepositoryService, LocalStore, $stateParams){
    $scope.divisions=[];
    $scope.years=[];
    $scope.leaveData=[];
    $scope.init=function(){
        RepositoryService.find("divsn",{},"simple").success(function(data){
            $scope.divisions=data;
        });

        d = new Date();
        $scope.year = d.getFullYear();
        $scope.leaveYear = d.getFullYear();
        var i=0;
        $scope.years.push($scope.year-1);
        $scope.years.push($scope.year);
        $scope.years.push($scope.year+1);
    }

    $scope.search=function(){
        RepositoryService.find("leaveReport",{division:$scope.leaveDivision,year:$scope.leaveYear},"simple").success(function(data){
            $scope.leaveData=data;
        })
    }

})


sfe.controller('adminEmployeesController',function($scope,$state, $http,LabelService,RepositoryService, LocalStore, $stateParams){
    $scope.links = {};
    $scope.labels = {};
    $scope.definition = {};

    $scope.init = function() {
        $scope.links = LabelService.getLabels("links");
        $scope.labels = LabelService.getLabels('state');

        $scope.test=LocalStore.fetch("topcontext");
        RepositoryService.find("jobtt",{SPAN_DIVISION:"1"},"simple").success(function(data){
            $scope.jobTitleList=data;
        })

        if($stateParams.id!=null){
            var response = RepositoryService.restore($stateParams.id);
            response.then(function(data){
                $scope.definition = data.data;

            });
        }
    };

    $scope.save=function(){
        var adminDTO={"id":$scope.definition.id,"code":$scope.definition.code,"firstName":$scope.definition.firstName,"lastName":$scope.definition.lastName,
            "jobTitle":{"id":$scope.definition.jobTitle.id,"displayName":""}}

        $http({
            method: 'POST',
            url: restUrl + "/miscellaneous/miscellaneousService/saveAdminEmployee",
            data:adminDTO,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        }).success(function(response) {
            alert("Data saved successfully.");
        })
    }

})




sfe.controller('ebStatusController',function ($scope,$state, $http,LabelService, RepositoryService, LocalStore, $modal,daysOfWeek,months) {

        $scope.monthList = new Array();
        $scope.years = new Array();
        $scope.ebYear;
        $scope.ebMonth;
        $scope.user;
        $scope.displayData = {};
        $scope.ebManager;
        $scope.displayData={};

        $scope.init = function () {
            var d = new Date();
            var yr = d.getFullYear();
            var i = 0;
            $scope.years.push(yr - 1);
            $scope.years.push(yr);
            $scope.years.push(yr + 1);
            $scope.monthList = months;
            $scope.currentYr = yr;
            $scope.currentMonth = months[d.getMonth()];
            $scope.ebYear = d.getFullYear();
            $scope.ebMonth = d.getMonth();
            $scope.user = LocalStore.fetch('USER_INFO').id;
        };

        $scope.search=function(){
            if($scope.ebManager==null || $scope.ebManager==''){
                alert("Please enter manager code");
                return false;
            }

            var month =parseInt($scope.ebMonth)+1;
            RepositoryService.find('coafd',{"reported_month":month,"reported_year":$scope.ebYear,"mgrCode":$scope.ebManager,'sortBy':'REPORT_DATE','sortOrder':'DESC'},'advanced','home').success(function(data){
                $scope.displayData=data;
            })
        }

        $scope.delete=function(id,name){
            var cnf=confirm("Do you want to delete EB of "+ name.toUpperCase()+"?");
            if (cnf){
                $http({
                    method: 'get',
                    url: restUrl + "/miscellaneous/miscellaneousService/deleteeb/" + id,
                    headers: {
                        "content-type": "application/json",
                        "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                    }
                }).success(function (data) {
                    if(data){
                        alert("EB deleted successfully.");
                        $scope.search();
                    }else{
                        alert("There is problem while deleting EB. Please try later.");
                    }
                })
            }else{
                return false;
            }
        }
});
