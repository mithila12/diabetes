


sfe.controller('ReportsController',function ($scope, $http, $rootScope, $compile,
                                                LocalStore, RepositoryService, ReportSearchFormService, $state) {
    $scope.metadata = {};
    $scope.metadata.formFields = {};
    $scope.stdFormData = {};
    $scope.table = {};
    $scope.table.notes=[];
    $scope.metadata.labels = {};
    $scope.chart = {};
    $scope.metadata.tabs=[];
    $scope.table.showExport = false;
    $scope.table.showChart = false;
    $scope.metadata.reportMeta = {};
    $scope.bootstrapClass= 'col-xs-';
    $scope.metadata.showFilter = true;
    $scope.chart.chartConfig = {};
    $scope.employeeId;
    $scope.charts=[];
    $scope.colXs=[];


    $scope.init = function(){
        var categoryId = $state.params.id;
        $http({
                method: 'GET',
                url:restUrl+ "/reports/report/searchtabmeta/"+categoryId,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            }
        ).then(function(response){
                $scope.metadata.tabs=response.data;
                $scope.initReport($scope.metadata.tabs[0].reportId);
            });
    }

    $scope.export = function(reportId, filter){

        $http({
            method:'POST',
            url:restUrl + "/reports/report/export/csv/" + $scope.metadata.reportId,
            headers: {
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data: $scope.stdFormData
        }).then(function(response){
            var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL;
            var blob = new Blob([response.data], { type: "text/csv", endings: 'native' });
            var url = urlCreator.createObjectURL(blob);
            window.location = url;
        })

    }

    $scope.exportExcel = function(reportId, filter){


        $http({
            method:'POST',
            url:restUrl + "/reports/report/export/excel/" + reportId,
            headers: {
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data: filter
        }).then(function(response){
            var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL;
            var blob = new Blob([response.data], { type: "application/xls" });
            var url = urlCreator.createObjectURL(blob);
            window.location = url;
        })

    }

    $scope.initReport = function(reportId){
        $scope.metadata.formFields = {};
        $scope.stdFormData = {};
        $scope.table = {};
        $scope.metadata.labels = {};
        $scope.chart = {};
        $scope.metadata.reportId=reportId;
        //$scope.employeeId = LocalStore.fetch("headerdata").id;

        $http.get('js/report-labels.json')
            .then(function (data) {
                $scope.metadata.labels = data.data[$scope.metadata.reportId];
            });

        $scope.metadata.formFields = ReportSearchFormService.getForm($scope.metadata.reportId);

        /*if($scope.metadata.formFields == null){
            $scope.metadata.showFilter = false;
            $scope.execReport(reportId, {});
        }
*/
    }

    $scope.initAndExecuteReport = function(reportId){
        $scope.metadata.formFields = {};
        $scope.stdFormData = {};
        $scope.table = {};
        $scope.metadata.labels = {};
        $scope.chart = {};
        $scope.metadata.reportId=reportId;
        //$scope.employeeId = LocalStore.fetch("headerdata").id;

        $http.get('js/report-labels.json')
            .then(function (data) {
                $scope.metadata.labels = data.data[$scope.metadata.reportId];
            });

        $scope.metadata.formFields = ReportSearchFormService.getForm($scope.metadata.reportId);

        /*if($scope.metadata.formFields == null){
         $scope.metadata.showFilter = false;
         $scope.execReport(reportId, {});
         }
         */

        $scope.table.showExport = false;
        $scope.table.showChart = false;
        $scope.table = {};
        $http({
            method:'POST',
            url:restUrl + "/reports/report/" + reportId,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data: {}
        }).then(function(response) {

            $scope.table.columns = response.data.tableCols;
            $scope.table.data = response.data.data;
            $scope.table.notes = response.data.notes;
            if($scope.table.data.length > 0){
                $scope.table.showExport = true;
            }

            $scope.chart.chartConfig = JSON.parse(response.data.chartStr);

            if(response.data.chartStr != null){
                $scope.table.showChart = true;
            }else{
                $scope.table.showChart = false;
            }

        });
    }

    $scope.search = function(){
        var reportId =  $scope.metadata.reportId;

        $http({
            method:'GET',
            url:restUrl + "/reports/report/reportMeta/" + reportId,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data: $scope.stdFormData
        }).then(function(response) {
            $scope.metadata.reportMeta = response.data;
            var colspan = $scope.metadata.reportMeta.columnSpan;

            colspan = (24/colspan);
            $scope.bootstrapClass = 'col-xs-'+colspan;

            //if(JSON.stringify($scope.stdFormData) != "{}"){
                $scope.execReport(reportId, $scope.stdFormData);
            //}
        });
    }

    $scope.execReport = function(reportId, filters){

        $scope.table.showExport = false;
        $scope.table.showChart = false;
        $scope.table = {};
        $http({
            method:'POST',
            url:restUrl + "/reports/report/" + reportId,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data: filters
        }).then(function(response) {

            $scope.table.columns = response.data.tableCols;
            $scope.table.data = response.data.data;
            $scope.table.notes = response.data.notes;
            if($scope.table.data.length > 0){
                $scope.table.showExport = true;
            }

            $scope.charts = [];

            if(response.data.chartStrings != null){

                for(str in response.data.chartStrings){
                    $scope.charts.push(JSON.parse(response.data.chartStrings[str]));
                }
                $scope.table.showChart = true;
            }else{
                $scope.table.showChart = false;
            }


            var temp = 0;

            temp = $scope.charts.length%2;

            for(i = 0; i < $scope.charts.length; i++){

                $scope.colXs[i] = 12;

                if(i == $scope.charts.length-1){
                    if(temp == 1) {
                        $scope.colXs[i] = 24;
                    }
                }
            }

            console.log($scope.colXs);


            /*$scope.chart.chartConfig = JSON.parse(response.data.chartStr);

            if(response.data.chartStr != null){
                $scope.table.showChart = true;
            }else{
                $scope.table.showChart = false;
            }*/

        });
    }

    $scope.onChangeElement = function(model, query, filter){

        var filterStr = "";
        var queryFilter = query.split(':');

        if(filter != null) {
            var filterArr = filter.split("~^~");
            for(i = 0; i < filterArr.length; i++){

                var arr = filterArr[i].split('=');

                filterStr += arr[0] + "=" + $scope.stdFormData[arr[1]];

                if(i != filterArr.length-1){
                    filterStr += "~^~";
                }
            }
        }

        if(filterStr != "" && queryFilter[1] != null && queryFilter[1] != ""){
            query = query + "~^~"+filterStr;
        }else{
            query = query + filterStr;
        }

        $http({
            method:'GET',
            url:restUrl + "/reports/report/options/" + query,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        }).then(function(response) {
            var element = angular.element(document.getElementsByName(model));
            var options = response.data;
            element.empty();
            for(i = 0; i < options.length; i++){
                var temp = options[i];
                element.append("<option value = '"+ temp['value']+"'>"+ temp['label'] +"</option>")
            }
        });
    }


    $scope.onChange = function(arr){

        if(arr != null) {

            for(index = 0; index < arr.length; index++) {

                var model = arr[index]['model'];
                var query = arr[index]['query'];
                var filter = arr[index]['filter'];


                $scope.onChangeElement(model, query, filter);
                /*var filterStr = "";
                var queryFilter = query.split(':');

                if (filter != null) {
                    var filterArr = filter.split("~^~");
                    for (i = 0; i < filterArr.length; i++) {

                        var arr = filterArr[i].split('=');

                        filterStr += arr[0] + "=" + $scope.stdFormData[arr[1]];

                        if (i != filterArr.length - 1) {
                            filterStr += "~^~";
                        }
                    }
                }

                if (filterStr != "" && queryFilter[1] != null && queryFilter[1] != "") {
                    query = query + "~^~" + filterStr;
                } else {
                    query = query + filterStr;
                }

                $http({
                    method: 'GET',
                    url: restUrl + "/reports/report/options/" + query,
                    headers: {
                        "content-type": "application/json",
                        "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                    }
                }).then(function (response) {
                    var element = angular.element(document.getElementsByName(model));
                    var options = response.data;
                    element.empty();
                    for (i = 0; i < options.length; i++) {
                        var temp = options[i];
                        element.append("<option value = '" + temp['value'] + "'>" + temp['label'] + "</option>")
                    }
                });
                */
            }
        }
    }

});


sfe.controller('EffortReportController',function ($scope, $http, $rootScope, $compile,
                                             LocalStore, RepositoryService, ReportSearchFormService, $state) {

   $scope.metadata = {};
    $scope.metadata.tabs = [{'title':'Competency','reportId':'tm_competency_summary_select'},
                                          {'title':'Doctor Engagement','reportId':'tm_doctor_engagement_select'},
                                          //{'title':'Inputs','reportId':'tm_input_summary_select'},
                                          {'title':'Monthly Activity','reportId':'monthly_summary_select'},
                                          {'title':'Compliance','reportId':'compliance_summary_select'},
                                          {'title':'Daily Work','reportId':'dailywork_summary_select'},
                                          {'title':'Daywise Details','reportId':'daywise_summary_select'}] ;
    $scope.metadata.formFields = {};
    $scope.stdFormData = {};
    $scope.table = {};
    $scope.metadata.labels = {};
    $scope.chart = {};

    $scope.chart.showChart = false;

    $scope.search = function(){
        var reportId =  $scope.metadata.reportId;
        $http({
            method:'POST',
            url:restUrl + "/reports/report/" + reportId,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data: $scope.stdFormData
        }).then(function(response){
            $scope.table.columns = response.data.tableCols;
            $scope.table.data = response.data.data;

            var chartType = response.data.chartType;
            if(chartType) {
                var transposed = response.data.transposeChart;
                $scope.chart.showChart = true;
                var categories = [];
                var names = [];
                var columnName = response.data.tableCols[0].name;;
                var series = [];
                if(transposed) {


                    for (var i = 1; i < response.data.tableCols.length; i++) {
                        categories.push(response.data.tableCols[i].name);
                    }

                    for (var i = 0; i < response.data.data.length; i++) {
                        var data = {data: []};
                        data.name = response.data.data[i][columnName];
                        for (var j = 0; j < categories.length; j++) {

                            data.data.push(response.data.data[i][categories[j]]);
                        }
                        series.push(data);
                    }
                }else{
                    for(var i=0;i< response.data.data.length;i++){
                        categories.push(response.data.data[i][columnName]);
                    }

                    for(var j=1;j<response.data.tableCols.length;j++) {
                        var ser = response.data.tableCols[j].name;

                        var data = {data: []};
                        data.name = ser;
                        for (var i = 0; i < response.data.data.length; i++) {
                            data.data.push(response.data.data[i][ser]);
                        }
                        series.push(data);
                    }

                }


                $scope.chart.chartConfig = {
                    options: {
                        chart: {
                            type: chartType
                        }
                    },
                    series: series,
                    xAxis: {
                        categories: categories
                    },
                    title: {
                        text:''
                    },
                    loading:false
                }
            }
        });

    }


    $scope.initReport = function(reportId){
        $scope.metadata.reportId = reportId;
        $scope.metadata.formFields = null;
        $http.get('js/report-labels.json')
            .then(function (data) {;
                $scope.metadata.labels = data.data[reportId];
            });
        $scope.metadata.formFields = ReportSearchFormService.getForm(reportId);
        $scope.table = {};
        $scope.chart = {};
        $scope.chart.showChart= false;
    }
});


sfe.controller('OverviewReportController',function ($scope, $http, $rootScope, $compile,
                                                  LocalStore, RepositoryService, ReportSearchFormService, $state) {

    $scope.metadata = {};
    $scope.metadata.tabs = [{'title':'Efforts','reportId':'tm_performance_summary_select'},
        {'title':'Sales','reportId':'tm_sales_summary_select'}]
    $scope.metadata.formFields = {};
    $scope.stdFormData = {};
    $scope.stdFormData.parentId=null;
    $scope.table = {};
    $scope.table1 = {};
    $scope.metadata.labels = {};
    $scope.chart = {};
    $scope.chart1 = {};
    $scope.divisions= [];
    $scope.zones = [];
    $scope.regions = [];
    $scope.locations = [];
    $scope.chart.showChart = false;
    $scope.table.showExport = false;
    $scope.months = [];
    $scope.years= [];
    $scope.childLocations = [];
    $scope.reports = [];
    $scope.reportLocation;
    $scope.reportLocation1;
    $scope.type;
    $scope.type1;


    $scope.export = function(){
        var reportId =  $scope.metadata.reportId;

        $http({
            method:'POST',
            url:restUrl + "/reports/report/export/csv/" + reportId,
            headers: {
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data: $scope.stdFormData
        }).then(function(response){
            var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL;
            var blob = new Blob([response.data], { type: "text/csv" });
            var url = urlCreator.createObjectURL(blob);
            window.location = url;
        })

    }

    $scope.exportExcel = function(){
        var reportId =  $scope.metadata.reportId;
        $http({
            method:'POST',
            url:restUrl + "/reports/report/export/excel/" + reportId,
            headers: {
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data: $scope.stdFormData
        }).then(function(response){
            var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL;
            var blob = new Blob([response.data], { type: "application/xls" });
            var url = urlCreator.createObjectURL(blob);
            window.location = url;
        })

    }

    $scope.search = function(){
        $scope.execReport($scope.metadata.reportId, $scope.stdFormData);
        $scope.exec($scope.runExec);
    }

    $scope.execReport = function(reportId, filters){

        $scope.table.showExport = false;
        $scope.table = {};
        $http({
            method:'POST',
            url:restUrl + "/reports/report/" + reportId,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data: filters
        }).then(function(response) {
            $scope.table.columns = response.data.tableCols;
            $scope.table.data = response.data.data;
            if($scope.table.data.length > 0){
                $scope.table.showExport = true;
            }
        });
    }


    $scope.initReport = function(reportId){
        $scope.metadata.reportId = reportId;
        $scope.metadata.formFields = null;
        $http.get('js/report-labels.json')
            .then(function (data) {;
                $scope.metadata.labels = data.data[reportId];
            });
        $scope.metadata.formFields = ReportSearchFormService.getForm(reportId);
        $scope.table = {};
        $scope.chart = {};
    }

    $scope.init = function(){
        $scope.metadata.reportId = $state.params.id;
        RepositoryService.find("divsn",{},'simple').then(function (data) {
            $scope.divisions = data.data;
        });

        RepositoryService.find("month",{},'simple').then(function (data) {
            $scope.months = data.data;
        });

        RepositoryService.find("year",{},'simple').then(function (data) {
            $scope.years = data.data;
        });
        $scope.runExec = false;
    }

    $scope.getZones = function(){

        if($scope.stdFormData.division == null || $scope.stdFormData.division == ''){
            $scope.zones = [];
            $scope.childLocations = [];
            $scope.type1="";
            $scope.reportLocation1 = null;
            return;
        }

        RepositoryService.find("locat_overview",{'division': $scope.stdFormData.division,
            'hierarchyId' : 'ORGHY00000000000000000000000000000001'},'simple').then(function (data) {
            $scope.zones = data.data;
            $scope.regions = [];
            $scope.locations = [];
            $scope.childLocations = $scope.zones;
            $scope.type1= "Zone";
        });
        $scope.stdFormData.parentId=null;
        $scope.runExec = true;
        for(d in $scope.divisions){
            if($scope.divisions[d].id==$scope.stdFormData.division){
                $scope.reportLocation1 = $scope.divisions[d].name;
                break;
            }
        }

    }

    $scope.getRegion = function(){

        if($scope.stdFormData.zone == '' || $scope.stdFormData.zone == 'undefined' || $scope.stdFormData.zone == null){
            $scope.getZones();
            return;
        }

        RepositoryService.find("locat_overview",{'division': $scope.stdFormData.division,
            'hierarchyId' : 'ORGHY00000000000000000000000000000002',
            'parentId': $scope.stdFormData.zone },'simple').
            then(function (data) {
                $scope.regions = data.data;
                $scope.locations = [];
                $scope.childLocations = $scope.regions;
                $scope.type1= "Region";
            });
        $scope.stdFormData.parentId=$scope.stdFormData.zone;
        $scope.runExec = true;
        for(z in $scope.zones){
            if($scope.zones[z].id==$scope.stdFormData.zone){
                $scope.reportLocation1 = $scope.zones[z].name;
                break;
            }
        }
    }

    $scope.getLocation = function(){

        if($scope.stdFormData.region == '' || $scope.stdFormData.region == 'undefined' || $scope.stdFormData.region == null){
            $scope.getRegion();
            return;
        }

        RepositoryService.find("locat_overview",{'division': $scope.stdFormData.division,
            'hierarchyId' : 'ORGHY00000000000000000000000000000003',
            'parentId': $scope.stdFormData.region },'simple').
            then(function (data) {
                $scope.locations = data.data;
                $scope.childLocations = $scope.locations;
                $scope.type1= "Territory";
            });
        $scope.stdFormData.parentId=$scope.stdFormData.region;
        $scope.runExec = true;
        for(r in $scope.regions){
            if($scope.regions[r].id==$scope.stdFormData.region){
                $scope.reportLocation1 = $scope.regions[r].name;
                break;
            }
        }
    }

    $scope.setParent = function(){
        $scope.childLocations = [];
        $scope.runExec = false;

        for(l in $scope.locations){
            if($scope.locations[l].id==$scope.stdFormData.parentId){
                $scope.reportLocation1 = $scope.locations[l].name;
                break;
            }
        }
        $scope.type1= "";
    }

    $scope.exec = function(isExec){

        $scope.type=$scope.type1;
        $scope.reportLocation = $scope.reportLocation1;
        /*if(!isExec) {
            $scope.table1 = {};
            return;
        }*/

        var filters = JSON.parse(JSON.stringify($scope.stdFormData));
        filters.parentIds = [];
        filters.parentIds=$scope.childLocations;

        $http({
            method:'POST',
            url:restUrl + "/reports/report/company_overview1",
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data: filters
        }).then(function(response) {
            $scope.table1.columns = response.data.tableCols;
            $scope.table1.data = response.data.data;


            $scope.createChart($scope.table1);

        });


        $scope.createChart = function(data){

            if(JSON.stringify(data) == '{}'){
                $scope.chart = {};
                return;
            }

            $scope.chart1.showChart = true;

            $scope.chart.chart = {};
            $scope.chart.chart.zoomType = 'xy';
            $scope.chart.type="bar";
            $scope.chart.title = {"text" : "Overall Effort Report for Locations: " + $scope.type};


            var fieldDays = [];
            var dml = [];
            var coreCoverage = [];
            var locations = [];
            var row = {};
            for(i in data.data){
                row = data.data[i];
                console.log(row["LOCATION_NAME"]);
                //locations.push(row["LOCATION_NAME"]);

                locations[i] = row["LOCATION_NAME"];

                if(row["FIELD_DAYS"] == null || row["FIELD_DAYS"] == ''){
                    fieldDays.push(0);
                }else {
                    fieldDays.push(row["FIELD_DAYS"]);
                }

                if(row["DML_COVERAGE"] == null || row["DML_COVERAGE"] == ''){
                    dml.push(0);
                }else {
                    dml.push(row["DML_COVERAGE"]);
                }

                if(row["CORE_COVERAGE"] == null || row["CORE_COVERAGE"] == ''){
                    coreCoverage.push(0);
                }else {
                    coreCoverage.push(row["CORE_COVERAGE"]);
                }
            }
            $scope.chart.xAxis = { "categories" : locations , "crosshair": true, "labels" : {"rotation" : 60}};

            $scope.chart.yAxis = [
                {
                    gridLineWidth: 0,
                    "labels" : {
                        "format" : "{value} Days",
                        "style" : {
                            color: Highcharts.getOptions().colors[2]
                        }
                    },
                    "title" : {
                        "text" : "Field Days",
                        "style" : {
                            color: Highcharts.getOptions().colors[2]
                        }
                    },
                    "opposite" : true
                },
                {
                    "labels" : {
                        "format" : "{value} %",
                        "style" : {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    "title" : {
                        "text" : "Core Coverage",
                        "style" : {
                            color: Highcharts.getOptions().colors[0]
                        }
                    }
                },
                {
                    gridLineWidth: 0,
                    "labels" : {
                        "format" : "{value} %",
                        "style" : {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    "title" : {
                        "text" : "DML Coverage",
                        "style" : {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    "opposite" : true
                }
            ];

            $scope.chart.tooltip = {
                "shared": true
            };

            /*$scope.chart.legend = {
                layout: 'vertical',
                align: 'left',
                x: 80,
                verticalAlign: 'top',
                y: 55,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            }*/


            $scope.chart.series = [
                {
                    name  : "Field Days",
                    type: 'spline',
                    data : fieldDays,
                    tooltip: {
                        valueSuffix: ' days'
                    }
                },
                {
                    name  : "DML Coverage",
                    type: 'spline',
                    data : dml,
                    tooltip: {
                        valueSuffix: ' %'
                    }
                },
                {
                    name  : "Core Coverage",
                    type: 'spline',
                    data : coreCoverage,
                    tooltip: {
                        valueSuffix: ' %'
                    }
                }
            ];

            console.log($scope.chart);

        }
        /*for(i=0; i<$scope.childLocations.length; i++){
            var loc = $scope.childLocations[i];
            result = {};
            filters = JSON.parse(JSON.stringify($scope.stdFormData));
            filters.parentId=loc.id;
            result.table = {};

            result.location_name=loc.name;
            $http({
                method:'POST',
                url:restUrl + "/reports/report/" + $scope.metadata.reportId,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                },
                data: filters
            }).then(function(response) {
                $scope.reports.push({data : response.data});
            });
        }
        */

    }

});



sfe.controller('SalesReportController',function ($scope, $http, $rootScope, $compile,
                                                    LocalStore, RepositoryService, ReportSearchFormService, $state) {

    $scope.metadata = {};
    $scope.metadata.tabs = [{'title':'Brandwise Sales','reportId':'tm_brand_sales_select'},
        {'title':'Secondary Sales','reportId':'tm_secondary_sales_select'}, {'title':'Stockist Sale Achievement', 'reportId':'stockist_level_sale_achievement'}]
    $scope.metadata.formFields = {};
    $scope.stdFormData = {};
    $scope.table = {};
    $scope.metadata.labels = {};
    $scope.chart = {};

    $scope.chart.showChart = false;

    $scope.search = function(){
        var reportId =  $scope.metadata.reportId;
        $http({
            method:'POST',
            url:restUrl + "/reports/report/" + reportId,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data: $scope.stdFormData
        }).then(function(response){
            $scope.table.columns = response.data.tableCols;
            $scope.table.data = response.data.data;

            var chartType = response.data.chartType;
            if(chartType) {
                var transposed = response.data.transposeChart;
                $scope.chart.showChart = true;
                var categories = [];
                var names = [];
                var columnName = response.data.tableCols[0].name;;
                var series = [];
                if(transposed) {


                    for (var i = 1; i < response.data.tableCols.length; i++) {
                        categories.push(response.data.tableCols[i].name);
                    }

                    for (var i = 0; i < response.data.data.length; i++) {
                        var data = {data: []};
                        data.name = response.data.data[i][columnName];
                        for (var j = 0; j < categories.length; j++) {

                            data.data.push(response.data.data[i][categories[j]]);
                        }
                        series.push(data);
                    }
                }else{
                    for(var i=0;i< response.data.data.length;i++){
                        categories.push(response.data.data[i][columnName]);
                    }

                    for(var j=1;j<response.data.tableCols.length;j++) {
                        var ser = response.data.tableCols[j].name;

                        var data = {data: []};
                        data.name = ser;
                        for (var i = 0; i < response.data.data.length; i++) {
                            data.data.push(response.data.data[i][ser]);
                        }
                        series.push(data);
                    }

                }


                $scope.chart.chartConfig = {
                    options: {
                        chart: {
                            type: chartType
                        }
                    },
                    series: series,
                    xAxis: {
                        categories: categories
                    },
                    title: {
                        text:''
                    },
                    loading:false
                }
            }
        });

    }


    $scope.initReport = function(reportId){
        $scope.metadata.reportId = reportId;
        $scope.metadata.formFields = null;
        $http.get('js/report-labels.json')
            .then(function (data) {;
                $scope.metadata.labels = data.data[reportId];
            });
        $scope.metadata.formFields = ReportSearchFormService.getForm(reportId);

        $scope.table = {};
        $scope.chart = {};
        $scope.chart.showChart= false;
    }
});


sfe.controller('DoctorReportController',function ($scope, $http, $rootScope, $compile,
                                                 LocalStore, RepositoryService, ReportSearchFormService, $state) {

    $scope.metadata = {};
    $scope.metadata.tabs = [{'title':'Call Summary','reportId':'tm_call_summary'},
        {'title':'Visit Dates','reportId':'tm_secondary_sales_select'},
        {'title':'Doctor Rx Trend','reportId':'tm_secondary_sales_select'},
        {'title':'Missing Profile Details','reportId':'tm_secondary_sales_select'}]
    $scope.metadata.formFields = {};
    $scope.stdFormData = {};
    $scope.table = {};
    $scope.metadata.labels = {};
    $scope.chart = {};

    $scope.chart1 = {};
    $scope.divisions= [];
    $scope.chart.showChart = false;
    $scope.table.showExport = false;
    $scope.months = [];
    $scope.years= [];


    $scope.init = function(){
        $scope.metadata.reportId = $state.params.id;
        RepositoryService.find("divsn",{},'simple').then(function (data) {
            $scope.divisions = data.data;
        });

        RepositoryService.find("month",{},'simple').then(function (data) {
            $scope.months = data.data;
        });

        RepositoryService.find("year",{},'simple').then(function (data) {
            $scope.years = data.data;
        });
        $scope.runExec = false;
    }

    $scope.export = function(){
        var reportId =  $scope.metadata.reportId;

        $http({
            method:'POST',
            url:restUrl + "/reports/report/export/csv/" + reportId,
            headers: {
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data: $scope.stdFormData
        }).then(function(response){
            var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL;
            var blob = new Blob([response.data], { type: "text/csv" });
            var url = urlCreator.createObjectURL(blob);
            window.location = url;
        })

    }

    $scope.exportExcel = function(){
        var reportId =  $scope.metadata.reportId;
        $http({
            method:'POST',
            url:restUrl + "/reports/report/export/excel/" + reportId,
            headers: {
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data: $scope.stdFormData
        }).then(function(response){
            var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL;
            var blob = new Blob([response.data], { type: "application/xls" });
            var url = urlCreator.createObjectURL(blob);
            window.location = url;
        })

    }

    $scope.search = function(){
        $scope.execReport($scope.metadata.reportId, $scope.stdFormData);
        $scope.exec($scope.runExec);
    }

    $scope.execReport = function(reportId, filters){

        $scope.table.showExport = false;
        $scope.table = {};
        $http({
            method:'POST',
            url:restUrl + "/reports/report/" + reportId,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data: filters
        }).then(function(response) {
            $scope.table.columns = response.data.tableCols;
            $scope.table.data = response.data.data;
            if($scope.table.data.length > 0){
                $scope.table.showExport = true;
            }
            $scope.createChart($scope.table);
        });
    }



        $scope.createChart = function(data){

            if(JSON.stringify(data) == '{}'){
                $scope.chart = {};
                return;
            }


            $scope.chart1.showChart = true;

            $scope.chart.chart = {};
            $scope.chart.chart.zoomType = 'xy';

            $scope.chart.title = {"text" : "Doctor Visit Report"};


            var fieldDays = [];
            var dml = [];
            var coreCoverage = [];
            var locations = [];
            var row = {};

            for(i in data.data){

                row = data.data[i];
                //locations.push(row["LOCATION_NAME"]);

                locations[i] = row["SPECIALITY"];

                if(row["TOTAL_DOCTORS"] == null || row["TOTAL_DOCTORS"] == ''){
                    fieldDays.push(0);
                }else {
                    fieldDays.push(row["TOTAL_DOCTORS"]);
                }

                if(row["DR_VISITED"] == null || row["DR_VISITED"] == ''){
                    dml.push(0);
                }else {
                    dml.push(row["DR_VISITED"]);
                }

                if(row["COVERAGE"] == null || row["COVERAGE"] == ''){
                    coreCoverage.push(0);
                }else {
                    coreCoverage.push(row["COVERAGE"]);
                }
            }
            $scope.chart.xAxis = { "categories" : locations , "crosshair": true, "labels" : {"rotation" : 60}};

            $scope.chart.yAxis = [
                {
                    gridLineWidth: 0,
                    "labels" : {
                        "format" : "# {value}",
                        "style" : {
                            color: Highcharts.getOptions().colors[2]
                        }
                    },
                    "title" : {
                        "text" : "Total Doctors",
                        "style" : {
                            color: Highcharts.getOptions().colors[2]
                        }
                    },
                    "opposite" : true
                },
                {
                    "labels" : {
                        "format" : "# {value}",
                        "style" : {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    "title" : {
                        "text" : "Total Doctor Visited Atleast Once",
                        "style" : {
                            color: Highcharts.getOptions().colors[0]
                        }
                    }
                },
                {
                    gridLineWidth: 0,
                    "labels" : {
                        "format" : "{value} %",
                        "style" : {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    "title" : {
                        "text" : "Coverage",
                        "style" : {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    "opposite" : true
                }
            ];

            $scope.chart.tooltip = {
                "shared": true
            };

            /*$scope.chart.legend = {
             layout: 'vertical',
             align: 'left',
             x: 80,
             verticalAlign: 'top',
             y: 55,
             floating: true,
             backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
             }*/


            $scope.chart.series = [
                {
                    name  : "Total Doctors",
                    type: 'spline',
                    data : fieldDays,
                    tooltip: {
                        valuePrefix: '# '
                    }
                },
                {
                    name  : "Total Doctor Visited Atleast Once",
                    type: 'spline',
                    data : dml,
                    tooltip: {
                        valuePrefix: '# '
                    }
                },
                {
                    name  : "Coverage",
                    type: 'spline',
                    data : coreCoverage,
                    tooltip: {
                        valueSuffix: ' %'
                    }
                }
            ];

        }
});


sfe.controller("SalesReportsController", function($scope, $http, $rootScope, $compile,
                                                  LocalStore, RepositoryService, ReportSearchFormService, $state){
    $scope.metadata = {};
    $scope.metadata.formFields = {};
    $scope.stdFormData = {};
    $scope.table = {};
    $scope.metadata.labels = {};
    $scope.chart = {};
    $scope.metadata.tabs=[];
    $scope.table.showExport = false;
    $scope.table.showChart = false;
    $scope.metadata.reportMeta = {};
    $scope.bootstrapClass= 'col-xs-';
    $scope.metadata.showFilter = true;
    $scope.chart.chartConfig = {};
    $scope.employeeId;


    $scope.stdFormData.parentId=null;
    $scope.table = {};
    $scope.metadata.labels = {};
    $scope.chart = {};
    $scope.divisions= [];
    $scope.zones = [];
    $scope.regions = [];
    $scope.locations = [];
    $scope.brands = [];
    $scope.skus = [];
    $scope.table.showChart = false;
    $scope.table.showExport = false;
    $scope.months = [];
    $scope.years= [];
    $scope.childLocations = [];
    $scope.reports = [];
    $scope.reportLocation;
    $scope.reportLocation1;
    $scope.type;
    $scope.type1;


    $scope.export = function(reportId, filter){


        $http({
            method:'POST',
            url:restUrl + "/reports/report/export/csv/" + reportId,
            headers: {
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data: filter
        }).then(function(response){
            var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL;
            var blob = new Blob([response.data], { type: "text/csv" });
            var url = urlCreator.createObjectURL(blob);
            window.location = url;
        })

    }

    $scope.exportExcel = function(reportId, filter){
        var reportId =  $scope.metadata.reportId;
        $http({
            method:'POST',
            url:restUrl + "/reports/report/export/excel/" + reportId,
            headers: {
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data: filter
        }).then(function(response){
            var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL;
            var blob = new Blob([response.data], { type: "xls" });
            var url = urlCreator.createObjectURL(blob);
            window.location = url;
        })

    }

    $scope.initReport = function(reportId){
        $scope.metadata.formFields = {};
        $scope.stdFormData = {};
        $scope.table = {};
        $scope.metadata.labels = {};
        $scope.chart = {};
        $scope.metadata.reportId=reportId;
        //$scope.employeeId = LocalStore.fetch("headerdata").id;

        $http.get('js/report-labels.json')
            .then(function (data) {
                $scope.metadata.labels = data.data[$scope.metadata.reportId];
            });

        RepositoryService.find("divsn",{},'simple').then(function (data) {
            $scope.divisions = data.data;
        });

        RepositoryService.find("month",{},'simple').then(function (data) {
            $scope.months = data.data;
        });

        RepositoryService.find("year",{},'simple').then(function (data) {
            $scope.years = data.data;
        });
    }

    $scope.search = function(){
        var reportId =  $scope.metadata.reportId;

        $http({
            method:'GET',
            url:restUrl + "/reports/report/reportMeta/" + reportId,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data: $scope.stdFormData
        }).then(function(response) {
            $scope.metadata.reportMeta = response.data;
            var colspan = $scope.metadata.reportMeta.columnSpan;

            colspan = (24/colspan);
            $scope.bootstrapClass = 'col-xs-'+colspan;

            //if(JSON.stringify($scope.stdFormData) != "{}"){
            $scope.execReport(reportId, $scope.stdFormData);
            //}
        });
    }

    $scope.execReport = function(reportId, filters){

        $scope.table.showExport = false;
        $scope.table.showChart = false;
        $scope.table = {};
        $http({
            method:'POST',
            url:restUrl + "/reports/report/" + reportId,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data: filters
        }).then(function(response) {

            $scope.table.columns = response.data.tableCols;
            $scope.table.data = response.data.data;
            if($scope.table.data.length > 0){
                $scope.table.showExport = true;
            }

            $scope.chart.chartConfig = JSON.parse(response.data.chartStr);

            if(response.data.chartStr != null){
                $scope.table.showChart = true;
            }

        });
    }

    $scope.getZones = function(){

        if($scope.stdFormData.division == null || $scope.stdFormData.division == ''){
            $scope.zones = [];
            $scope.childLocations = [];
            $scope.type1="";
            $scope.reportLocation1 = null;
            return;
        }

        RepositoryService.find("locat_overview",{'division': $scope.stdFormData.division,
            'hierarchyId' : 'ORGHY00000000000000000000000000000001'},'simple').then(function (data) {
            $scope.zones = data.data;
            $scope.regions = [];
            $scope.locations = [];
            $scope.childLocations = $scope.zones;
            $scope.type1= "Zone";

            $scope.getBrands();
        });
        $scope.stdFormData.parentId=null;
        $scope.runExec = true;
        for(d in $scope.divisions){
            if($scope.divisions[d].id==$scope.stdFormData.division){
                $scope.reportLocation1 = $scope.divisions[d].name;
                break;
            }
        }

    }

    $scope.getBrands = function(){

        if($scope.stdFormData.division == null || $scope.stdFormData.division == ''){
            $scope.brands = [];
            $scope.childLocations = [];
            $scope.type1="";
            $scope.reportLocation1 = null;
            return;
        }

        RepositoryService.find("brand",{'division': $scope.stdFormData.division},'simple').then(function (data) {
            $scope.brands = data.data;
        });
    }

    $scope.getSKU = function(){

        if($scope.stdFormData.brand == null || $scope.stdFormData.brand == ''){
            $scope.skus = [];
            $scope.childLocations = [];
            $scope.type1="";
            $scope.reportLocation1 = null;
            return;
        }

        RepositoryService.find("prodt",{'division_id': $scope.stdFormData.division, 'brand' : $scope.stdFormData.brand },'simple').then(function (data) {
            $scope.skus = data.data;
        });

    }

    $scope.getRegion = function(){

        if($scope.stdFormData.zone == '' || $scope.stdFormData.zone == 'undefined' || $scope.stdFormData.zone == null){
            $scope.getZones();
            return;
        }

        RepositoryService.find("locat_overview",{'division': $scope.stdFormData.division,
            'hierarchyId' : 'ORGHY00000000000000000000000000000002',
            'parentId': $scope.stdFormData.zone },'simple').
            then(function (data) {
                $scope.regions = data.data;
                $scope.locations = [];
                $scope.childLocations = $scope.regions;
                $scope.type1= "Region";
            });
        $scope.stdFormData.parentId=$scope.stdFormData.zone;
        $scope.runExec = true;
        for(z in $scope.zones){
            if($scope.zones[z].id==$scope.stdFormData.zone){
                $scope.reportLocation1 = $scope.zones[z].name;
                break;
            }
        }
    }

    $scope.getLocation = function(){

        if($scope.stdFormData.region == '' || $scope.stdFormData.region == 'undefined' || $scope.stdFormData.region == null){
            $scope.getRegion();
            return;
        }

        RepositoryService.find("locat_overview",{'division': $scope.stdFormData.division,
            'hierarchyId' : 'ORGHY00000000000000000000000000000003',
            'parentId': $scope.stdFormData.region },'simple').
            then(function (data) {
                $scope.locations = data.data;
                $scope.childLocations = $scope.locations;
                $scope.type1= "Territory";
            });
        $scope.stdFormData.parentId=$scope.stdFormData.region;
        $scope.runExec = true;
        for(r in $scope.regions){
            if($scope.regions[r].id==$scope.stdFormData.region){
                $scope.reportLocation1 = $scope.regions[r].name;
                break;
            }
        }
    }

    $scope.setParent = function(){
        $scope.childLocations = [];
        $scope.runExec = false;

        for(l in $scope.locations){
            if($scope.locations[l].id==$scope.stdFormData.parentId){
                $scope.reportLocation1 = $scope.locations[l].name;
                break;
            }
        }
        $scope.type1= "";
    }

});



sfe.controller('DynamicReportsController',function ($scope, $http, $rootScope, $compile,
                                             LocalStore, RepositoryService, ReportSearchFormService, $state, $modal) {
    $scope.reportDefinition = {};
    $scope.reportDefinition.filtersApplied = [];
    $scope.reportId;
    $scope.newReportName;
    $scope.table = {};
    $scope.table.showExport = false;
    $scope.table.showChart = false;
    $scope.table.transposeTable = false;
    $scope.chart={};
    $scope.chart.chartType = 0;
    $scope.chart.xAxis = {};
    $scope.chart.xAxis = { title : "", attribute : { id : '' }, valueAttributes : [], params : {} };
    $scope.chart.yAxis = [];
    $scope.chart.params = {};

    $scope.axis = {  };
    $scope.axis.title;
    $scope.axis.attribute = {};
    $scope.axis.attribute.id;
    $scope.axis.valueAttributes = [];
    $scope.axis.params = {};
    $scope.appliedFunction;
    $scope.valueAttr;

    $scope.dto = {};
    $scope.searchFilters = [];
    $scope.selectAttributes = [];
    $scope.groupAttributes = [];
    $scope.aggregationAttributes = [];

    $scope.chartTabs = [];
    $scope.data = {};

    $scope.chartJsons = [];

    $scope.data.reportDefinition = {};
    $scope.data.searchFilters = [];
    $scope.data.selectAttributes = [];
    $scope.data.groupAttributes = [];
    $scope.data.aggregationAttributes = [];
    $scope.sortIndex=[];


    $scope.chartTypes = [];

    $scope.numPages = 20;
    $scope.totalItems = 0;
    $scope.currentPage = -1;
    $scope.maxSize = 5;

    $scope.nodata = false;
    $scope.showtable = true;
    $scope.reportDefinition.currentPage=$scope.currentPage;

    $scope.selectedFilter = {};
    $scope.selectedFilter.id;
    $scope.selectedFilter.name;
    $scope.selectedFilter.operator = 'equal';
    $scope.selectedFilter.value;
    $scope.selectedFilter.displayValue;
    $scope.filterControls={};
    $scope.functions = {};

    $scope.dateRange = [];
    $scope.dateRange[0] = null;
    $scope.dateRange[1] = null;
    $scope.multiSelectValue=[];
    $scope.selectValue = {};


    $scope.onAttributeSelect = function(){

        $scope.selectedFilter.value=null;

        var displayControl = $scope.filterControls[$scope.data.searchFilters[$scope.selectedFilter.id].filterType];

        if(displayControl != null){
            $scope.operators = displayControl.operators;
            $scope.type = displayControl.type;
        }

        if($scope.type == 1 || $scope.type == 4){
            $scope.options = $scope.data.searchFilters[$scope.selectedFilter.id].options;
        }

        $scope.selectedFilter.name=$scope.data.searchFilters[$scope.selectedFilter.id].attribute.displayName;

    }

    $scope.addFilter = function(){

        var filter = {  };
        filter.reportAttribute = {};
        filter.reportAttribute.id = $scope.selectedFilter.id;
        filter.reportAttribute.displayName = $scope.selectedFilter.name;
        filter.showFilter = true;
        filter.operator=$scope.selectedFilter.operator;

        if(($scope.type == 2 || $scope.type == 5 ) && filter.operator == 'between' && $scope.dateRange != {}){
            $scope.selectedFilter.value = $scope.dateRange[0]+'~^~'+$scope.dateRange[1];
            $scope.selectedFilter.displayValue = $scope.dateRange[0]+' to '+$scope.dateRange[1];
        }else if($scope.type ==4 && filter.operator == 'in' && $scope.multiSelectValue!=null){
            $scope.selectedFilter.value = "";
            $scope.selectedFilter.displayValue="";
            for(i = 0; i < $scope.multiSelectValue.length; i++){

                $scope.selectedFilter.value += $scope.multiSelectValue[i].id;


                if(i != $scope.multiSelectValue.length-1){
                    $scope.selectedFilter.value += ",";
                }
            }
            $scope.selectedFilter.displayValue = $scope.selectedFilter.value;
        }else if($scope.type == 1){
            $scope.selectedFilter.value = $scope.selectValue.id;
            $scope.selectedFilter.displayValue = $scope.selectValue.name
        } else{
            $scope.selectedFilter.displayValue = $scope.selectedFilter.value;
        }

        if($scope.selectedFilter.value != null) {
            filter.value = $scope.selectedFilter.value;
            filter.displayValue = $scope.selectedFilter.displayValue;
        }else{
            filter.isNull = true;
        }

        filter.report = { id : $scope.data.reportId }

        $scope.reportDefinition.filtersApplied.push(filter);
        $scope.type = 0;
        $scope.dateRange[0]=null;
        $scope.dateRange[1]=null;
        $scope.selectedFilter = {};
    }


    $scope.removeFilter = function(filter){

        var i = 0;
        var f = {};
        for(i = 0; i < $scope.reportDefinition.filtersApplied.length; i++){
            f = $scope.reportDefinition.filtersApplied[i];
            if(f.reportAttribute.id == filter.reportAttribute.id && f.operator==filter.operator && f.value==filter.value){
                $scope.reportDefinition.filtersApplied.splice(i, 1);
            }
        }
    }


    $scope.sort = function(column){

        var sort = 'ASC';
        var isPresent = false;
        if($scope.sortIndex[column] == null){
            $scope.sortIndex[column] = 1;
        }else{
            if($scope.sortIndex[column] == 2){
                $scope.sortIndex[column] = 0;
            }else{
                $scope.sortIndex[column]++;
                if($scope.sortIndex[column] == 2){
                    sort = 'DESC';
                }
            }
        }

        if($scope.reportDefinition.orderCols == null){
            $scope.reportDefinition.orderCols = [];


            $scope.reportDefinition.orderCols[0] =  {attribute : { id : column }, params: {orderBy : sort} }

        }else{
            for( i =0; i < $scope.reportDefinition.orderCols.length; i++ ){
                if($scope.reportDefinition.orderCols[i].attribute.id == column){
                    isPresent = true;
                    if($scope.sortIndex[column] == 0){
                        $scope.reportDefinition.orderCols.splice(i);
                    }else{
                        $scope.reportDefinition.orderCols[i].params.orderBy = sort;
                    }
                }
            }

            if(!isPresent){
                $scope.reportDefinition.orderCols[$scope.reportDefinition.orderCols.length] =  {attribute : { id : column }, params: {orderBy : sort} };
            }

        }

        $scope.search();

    }

    $scope.init = function(){
        $scope.reportId = $state.params.id;

        $http.get('js/reportFunctions.json')
            .success(function (data) {
                $scope.functions = data;
            });

        $http.get('js/filterControl.json').success(
            function(data){
                $scope.filterControls= data;
            }
        )

        $scope.chartTypes = [{ type : 0 , displayName : 'Line Chart'}];


        $http(
            {
                method: 'POST',
                url:restUrl+ "/reports/report/reportDef/"+$scope.reportId,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                },
                data : $scope.reportDefinition

            }
        ).then(function(response){
                $scope.reportDefinition = response.data;

                var selectedAttributes = $scope.reportDefinition.selectedCols;
                var entities = $scope.reportDefinition.usedEntity;

                var index = 0;
                if($scope.reportDefinition.searchFilter != null){
                    for(index = 0; index < $scope.reportDefinition.searchFilter.length; index++){
                        $scope.searchFilters[$scope.reportDefinition.searchFilter[index].attribute.id] = $scope.reportDefinition.searchFilter[index];
                    }
                }

                for(e in entities){

                    for(a in entities[e].attributes){
                        $scope.selectAttributes[entities[e].attributes[a].attribute.id] = { entity : entities[e].entity.displayName, attribute : entities[e].attributes[a].attribute, selected: false };
                        if(entities[e].attributes[a].groupingAllowed == true) {
                            $scope.groupAttributes.push({
                                entity: entities[e].entity.displayName,
                                attribute: entities[e].attributes[a].attribute
                            });
                        }
                        if(entities[e].attributes[a].numeric == true) {
                            $scope.aggregationAttributes.push({ entity : entities[e].entity.displayName, attribute : entities[e].attributes[a].attribute })
                        }
                    }
                }

                if(selectedAttributes.length > 0){
                    for(a in selectedAttributes){
                        $scope.selectAttributes[selectedAttributes[a].attribute.id].selected = true;
                    }
                }

                if($scope.reportDefinition.data != null){
                    $scope.table.columns = $scope.reportDefinition.data.tableCols;
                    $scope.table.data = $scope.reportDefinition.data.data;
                    $scope.table.transposeTable = $scope.reportDefinition.data.transposeHeaders;
                    if($scope.table.data.length > 0){
                        $scope.table.showExport = true;
                    }
                }
                if($scope.reportDefinition.data.totalRecords == 0){
                    $scope.nodata = true;
                    $scope.showtable = false;
                }else{
                    $scope.nodata = false;
                }
                $scope.totalItems = $scope.reportDefinition.data.totalRecords;
                $scope.currentPage = $scope.reportDefinition.data.currentPage;

            });


        $scope.data.reportDefinition = $scope.reportDefinition;
        $scope.data.searchFilters = $scope.searchFilters;
        $scope.data.selectAttributes = $scope.selectAttributes;
        $scope.data.groupAttributes = $scope.groupAttributes;
        $scope.data.aggregationAttributes = $scope.aggregationAttributes;
    }

    $scope.search = function(){

        //$scope.reportDefinition.currentPage = $scope.currentPage;
        $scope.reportDefinition.data = null;

        $http(
            {
                method: 'POST',
                url:restUrl+ "/reports/report/execute/"+$scope.reportId,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                },
                data : $scope.reportDefinition

            }
        ).then(function(response){

                if(response.data.totalRecords==0) {
                    $scope.nodata = true;
                    $scope.showtable = false;
                }else{
                    $scope.nodata = false;
                }
                $scope.table.columns = response.data.tableCols;
                $scope.table.data = response.data.data;
                $scope.table.transposeTable = response.data.transposeHeaders;
                if($scope.table.data.length > 0){
                    $scope.table.showExport = true;
                }
                $scope.totalItems = response.data.totalRecords;
                $scope.currentPage = response.data.currentPage;

            });
    }

    $scope.pageChanged = function(){
        console.log($scope.currentPage);
        $scope.reportDefinition.currentPage = $scope.currentPage;
        $scope.search();
    }

    $scope.save = function(){
        $http(
            {
                method: 'POST',
                url:restUrl+ "/reports/report/save/"+$scope.reportId+"/"+$scope.newReportName,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                },
                data : $scope.reportDefinition
            }
        ).then(function(response){
                alert("Report Created Successfully")
            });

    }

    $scope.openReportConfig = function(){

        var promise = $modal.open({
            templateUrl : "reportConfig.html",
            controller : "ReportConfigController",
            size : "lg",
            resolve : {
                reportDefinition : function(){
                    //var data = {};
                    //data.reportDefinition = $scope.reportDefinition;
                    //data.searchFilters = $scope.searchFilters;
                    //data.selectAttributes = $scope.selectAttributes;
                    //data.groupAttributes = $scope.groupAttributes;
                    //data.aggregationAttributes = $scope.aggregationAttributes;
                    return $scope.reportDefinition;
                },
                searchFilters : function(){
                    return $scope.searchFilters;
                },
                selectAttributes : function(){
                    return $scope.selectAttributes;
                },
                groupAttributes : function(){
                    return $scope.groupAttributes;
                },
                aggregationAttributes : function(){
                    return $scope.aggregationAttributes;
                },
                reportId : function(){
                    return $scope.reportId;
                }
            }
        });

        promise.result.then(
            function(response){
                $scope.search();
            }
        )
    }


    $scope.openSelectColumns = function(){

        var promise = $modal.open({
            templateUrl : "selectColumn.html",
            controller : "ReportConfigController",
            size : "lg",
            resolve : {
                reportDefinition : function(){
                    //var data = {};
                    //data.reportDefinition = $scope.reportDefinition;
                    //data.searchFilters = $scope.searchFilters;
                    //data.selectAttributes = $scope.selectAttributes;
                    //data.groupAttributes = $scope.groupAttributes;
                    //data.aggregationAttributes = $scope.aggregationAttributes;
                    return $scope.reportDefinition;
                },
                searchFilters : function(){
                    return $scope.searchFilters;
                },
                selectAttributes : function(){
                    return $scope.selectAttributes;
                },
                groupAttributes : function(){
                    return $scope.groupAttributes;
                },
                aggregationAttributes : function(){
                    return $scope.aggregationAttributes;
                },
                reportId : function(){
                    return $scope.reportId;
                }
            }
        });

        promise.result.then(
            function(response){
                $scope.search();
            }
        )
    }


    $scope.openFilter = function(){

        var promise = $modal.open({
            templateUrl : "reportFilter.html",
            controller : "ReportConfigController",
            size : "lg",
            resolve : {
                reportDefinition : function(){
                    //var data = {};
                    //data.reportDefinition = $scope.reportDefinition;
                    //data.searchFilters = $scope.searchFilters;
                    //data.selectAttributes = $scope.selectAttributes;
                    //data.groupAttributes = $scope.groupAttributes;
                    //data.aggregationAttributes = $scope.aggregationAttributes;
                    return $scope.reportDefinition;
                },
                searchFilters : function(){
                    return $scope.searchFilters;
                },
                selectAttributes : function(){
                    return $scope.selectAttributes;
                },
                groupAttributes : function(){
                    return $scope.groupAttributes;
                },
                aggregationAttributes : function(){
                    return $scope.aggregationAttributes;
                },
                reportId : function(){
                    return $scope.reportId;
                }
            }
        });

        promise.result.then(
            function(response){
                $scope.search();
            }
        )
    }

    $scope.openGroupBy = function(){

        var promise = $modal.open({
            templateUrl : "groupBy.html",
            controller : "ReportConfigController",
            size : "lg",
            resolve : {
                reportDefinition : function(){
                    //var data = {};
                    //data.reportDefinition = $scope.reportDefinition;
                    //data.searchFilters = $scope.searchFilters;
                    //data.selectAttributes = $scope.selectAttributes;
                    //data.groupAttributes = $scope.groupAttributes;
                    //data.aggregationAttributes = $scope.aggregationAttributes;
                    return $scope.reportDefinition;
                },
                searchFilters : function(){
                    return $scope.searchFilters;
                },
                selectAttributes : function(){
                    return $scope.selectAttributes;
                },
                groupAttributes : function(){
                    return $scope.groupAttributes;
                },
                aggregationAttributes : function(){
                    return $scope.aggregationAttributes;
                },
                reportId : function(){
                    return $scope.reportId;
                }
            }
        });

        promise.result.then(
            function(response){
                $scope.search();
            }
        )
    }

    $scope.openAggregation = function(){

        var promise = $modal.open({
            templateUrl : "aggregation.html",
            controller : "ReportConfigController",
            size : "lg",
            resolve : {
                reportDefinition : function(){
                    //var data = {};
                    //data.reportDefinition = $scope.reportDefinition;
                    //data.searchFilters = $scope.searchFilters;
                    //data.selectAttributes = $scope.selectAttributes;
                    //data.groupAttributes = $scope.groupAttributes;
                    //data.aggregationAttributes = $scope.aggregationAttributes;
                    return $scope.reportDefinition;
                },
                searchFilters : function(){
                    return $scope.searchFilters;
                },
                selectAttributes : function(){
                    return $scope.selectAttributes;
                },
                groupAttributes : function(){
                    return $scope.groupAttributes;
                },
                aggregationAttributes : function(){
                    return $scope.aggregationAttributes;
                },
                reportId : function(){
                    return $scope.reportId;
                }
            }
        });

        promise.result.then(
            function(response){
                $scope.search();
            }
        )
    }

    $scope.yColSelected = function(){

    }

    $scope.AddAggregationOnY = function(fun, v){


        /*if($scope.valueAttr == null || $scope.valueAttr == '' ){
            return;
        }
        var index = 0;
        var addNew = true;
        if($scope.axis.valueAttributes != null && $scope.axis.valueAttributes.length > 0){
            for(a in $scope.axis.valueAttributes){
                if($scope.axis.valueAttributes[a].attribute.id == $scope.valueAttr){
                    index = a;
                    addNew = false;
                    break;
                }
            }
        }else{
            $scope.axis.valueAttributes = [];
        }
        if(addNew){
            $scope.axis.valueAttributes.push({'attribute' : $scope.selectAttributes[$scope.valueAttr].attribute, 'aggregationCode' : '00000'});
            index = $scope.axis.valueAttributes.length-1;
        }
        var code = $scope.axis.valueAttributes[index].aggregationCode.split('');
        code[$scope.functions[$scope.appliedFunction].index] = 1;
        $scope.axis.valueAttributes[index].aggregationCode = code.join('');
        */

        if(v == null || v == '' ){
            return;
        }
        var index = 0;
        var addNew = true;
        if($scope.axis.valueAttributes != null && $scope.axis.valueAttributes.length > 0){
            for(a in $scope.axis.valueAttributes){
                if($scope.axis.valueAttributes[a].attribute.id == v){
                    index = a;
                    addNew = false;
                    break;
                }
            }
        }else{
            $scope.axis.valueAttributes = [];
        }
        if(addNew){
            $scope.axis.valueAttributes.push({'attribute' : $scope.selectAttributes[v].attribute, 'aggregationCode' : '00000'});
            index = $scope.axis.valueAttributes.length-1;
        }
        var code = $scope.axis.valueAttributes[index].aggregationCode.split('');
        code[$scope.functions[fun].index] = 1;
        $scope.axis.valueAttributes[index].aggregationCode = code.join('');

    }

    $scope.selectXAxis = function(index){

        if($scope.chartTabs[index].xAxis.attribute.id){
            $scope.chartTabs[index].xAxis.attribute = $scope.selectAttributes[$scope.chartTabs[index].xAxis.attribute.id].attribute;
        }

    }

    $scope.removeAggregationOnY = function(attributeId, fun, valueAttrId, temp){

        if(temp == 0){
            if($scope.axis.valueAttributes != null){

                for(a in $scope.axis.valueAttributes){
                    if($scope.axis.valueAttributes[a].attribute.id == valueAttrId){
                        var code = $scope.axis.valueAttributes[a].aggregationCode.split('');
                        code[func.index] = 0;
                        $scope.axis.valueAttributes[a].aggregationCode = code.join('');

                        if($scope.axis.valueAttributes[a].aggregationCode == '00000'){
                            $scope.axis.valueAttributes.splice(a, 1);
                        }
                    }
                }
            }
        }else{
            for( i = 0; i < $scope.chart.yAxis.length; i++) {
                if ($scope.chart.yAxis[i].attribute.id == attributeId &&  $scope.chart.yAxis[i].valueAttributes != null) {

                    for (a in $scope.chart.yAxis[i].valueAttributes) {
                        if ($scope.chart.yAxis[i].valueAttributes[a].attribute.id == valueAttrId) {
                            var code = $scope.chart.yAxis[i].valueAttributes[a].aggregationCode.split('');
                            code[fun.index] = 0;
                            $scope.chart.yAxis[i].valueAttributes[a].aggregationCode = code.join('');

                            if ($scope.chart.yAxis[i].valueAttributes[a].aggregationCode == '00000') {
                                $scope.chart.yAxis.splice(i, 1);
                            }
                        }
                    }
                }
            }
        }
    }

    $scope.AddYAxis = function(){

        if($scope.axis.attribute.id != null && $scope.axis.attribute.id != ''){
            $scope.axis.attribute = $scope.selectAttributes[$scope.axis.attribute.id].attribute;
            $scope.chart.yAxis.push($scope.axis);

            $scope.axis = {  };
            $scope.axis.title;
            $scope.axis.attribute = {};
            $scope.axis.attribute.id;
            $scope.axis.valueAttributes = [];
            $scope.axis.params = {};
        }

    }

    $scope.removeYAxis = function(index){
        $scope.chart.yAxis.slice(index, 1);
    }

    $scope.addChart = function(){

        $scope.chart={};
        $scope.chart.chartType = 0;
        $scope.chart.xAxis = {};
        $scope.chart.xAxis = { title : "", attribute : { id : '' }, valueAttributes : [], params : {} };
        $scope.chart.yAxis = [];
        $scope.chart.params = {};
        $scope.valueAttr = null;
        $scope.appliedFunction = 0;

        $scope.chart.title = "new Chart";
        $scope.chartType = 0;
        $scope.chartTabs.push($scope.chart);
        $scope.chartJsons.push({});
    }

    $scope.createChart = function(index, chartDTO){

        chartDTO.chartStr = '';
        chartDTO.filters = $scope.reportDefinition.filtersApplied;

        $http(
            {
                method: 'POST',
                url:restUrl+ "/reports/report/createChart/"+$scope.reportId,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                },
                data : chartDTO

            }
        ).then(function(response){
                $scope.chartTabs[index] = response.data;
                $scope.chartTabs[index].chartStr = JSON.parse(response.data.chartStr);
            });
    }



});




sfe.controller('ReportConfigController',function ($scope, $http, $rootScope, $compile,
                                                    LocalStore, RepositoryService, ReportSearchFormService, $state,
                                                    $modalInstance, reportDefinition, searchFilters, selectAttributes, groupAttributes, aggregationAttributes, reportId) {

        //$scope.selectAttributes = [];
        $scope.data = {reportDefinition : reportDefinition, searchFilters:  searchFilters, selectAttributes : selectAttributes, groupAttributes : groupAttributes, aggregationAttributes : aggregationAttributes, reportId : reportId };
        //$scope.groupAttributes = [];
        $scope.data.groupAttrSelected;
        $scope.data.groupAttrSelected1;
        //$scope.aggregationAttributes = [];
        $scope.data.aggregationAttrSelected;
        $scope.functions = [];
        $scope.data.appliedFunction;
        //$scope.searchFilters = [];
        $scope.selectedFilter = {};
        $scope.selectedFilter.id;
        $scope.selectedFilter.name;
        $scope.selectedFilter.operator = 'equal';
        $scope.selectedFilter.value;
        $scope.selectedFilter.displayValue;
        $scope.isSelect = false;
        $scope.type = 0; // 0: input box, 1 : select, 2: date, 3: number
        $scope.options = [];
        $scope.filterControls = {};
        $scope.operators=[ { id : 'equal', name : 'equal' } ];
        //$scope.data.reportDefinition.filtersApplied;
        $scope.dateRange = [];
        $scope.dateRange[0] = null;
        $scope.dateRange[1] = null;
        $scope.multiSelectValue=[];
        $scope.selectValue = {};

        $scope.groupingCols = [];
        $scope.groupingRows = [];


        $scope.onAttributeSelect = function(){

            $scope.selectedFilter.value=null;

            var displayControl = $scope.filterControls[$scope.data.searchFilters[$scope.selectedFilter.id].filterType];

            if(displayControl != null){
                $scope.operators = displayControl.operators;
                $scope.type = displayControl.type;
            }

            if($scope.type == 1 || $scope.type == 4){
                $scope.options = $scope.data.searchFilters[$scope.selectedFilter.id].options;
            }

            $scope.selectedFilter.name=$scope.data.searchFilters[$scope.selectedFilter.id].attribute.displayName;

        }


        $scope.addFilter = function(){

            var filter = {  };
            filter.reportAttribute = {};
            filter.reportAttribute.id = $scope.selectedFilter.id;
            filter.reportAttribute.displayName = $scope.selectedFilter.name;
            filter.showFilter = true;
            filter.operator=$scope.selectedFilter.operator;

            if(($scope.type == 2 || $scope.type == 5) && filter.operator == 'between' && $scope.dateRange != {}){
                $scope.selectedFilter.value = $scope.dateRange[0]+'~^~'+$scope.dateRange[1];
                $scope.selectedFilter.displayValue = $scope.dateRange[0]+' to '+$scope.dateRange[1];
            }else if($scope.type ==4 && filter.operator == 'in' && $scope.multiSelectValue!=null){
                $scope.selectedFilter.value = "";
                $scope.selectedFilter.displayValue="";
                for(i = 0; i < $scope.multiSelectValue.length; i++){

                    $scope.selectedFilter.value += $scope.multiSelectValue[i];

                    if(i != $scope.multiSelectValue.length-1){
                        $scope.selectedFilter.value += ",";
                    }
                }
                $scope.selectedFilter.displayValue = $scope.selectedFilter.value;
            }else if($scope.type == 1){
                $scope.selectedFilter.value = $scope.selectValue.id;
                $scope.selectedFilter.displayValue = $scope.selectValue.name
            } else{
                $scope.selectedFilter.displayValue = $scope.selectedFilter.value;
            }

            if($scope.selectedFilter.value != null) {
                filter.value = $scope.selectedFilter.value;
                filter.displayValue = $scope.selectedFilter.displayValue;
            }else{
                filter.isNull = true;
            }

            filter.report = { id : $scope.data.reportId }

            $scope.data.reportDefinition.filtersApplied.push(filter);
            $scope.type = 0;
            $scope.dateRange[0]=null;
            $scope.dateRange[1]=null;
            $scope.selectedFilter = {};
        }

        $scope.removeFilter = function(filter){

            var i = 0;
            var f = {};
            for(i = 0; i < $scope.data.reportDefinition.filtersApplied.length; i++){
                f = $scope.data.reportDefinition.filtersApplied[i];
                if(f.reportAttribute.id == filter.reportAttribute.id && f.operator==filter.operator && f.value==filter.value){
                    $scope.data.reportDefinition.filtersApplied.splice(i, 1);
                }
            }
        }

        $scope.init = function(){

            $http.get('js/reportFunctions.json')
                .success(function (data) {
                    $scope.functions = data;
                });

            $http.get('js/filterControl.json').success(
                function(data){
                    $scope.filterControls= data;
                }
            )

            if($scope.data.reportDefinition.gropingCols != null){

                for(i = 0; i < $scope.data.reportDefinition.gropingCols.length; i++){

                    if($scope.data.reportDefinition.gropingCols[i].params != {}){

                        if($scope.data.reportDefinition.gropingCols[i].params.groupingAttrPosition == 2){
                            $scope.groupingCols.push($scope.data.reportDefinition.gropingCols[i]);
                        }else if($scope.data.reportDefinition.gropingCols[i].params.groupingAttrPosition == 1){
                            $scope.groupingRows.push($scope.data.reportDefinition.gropingCols[i]);
                        }

                    }

                }

            }

            /*var selectedAttributes = $scope.data.reportDefinition.selectedCols;
            var entities = $scope.data.reportDefinition.usedEntity;



            var index = 0;
            if($scope.data.reportDefinition.searchFilter != null){
                for(index = 0; index < $scope.data.reportDefinition.searchFilter.length; index++){
                    $scope.searchFilters[$scope.data.reportDefinition.searchFilter[index].attribute.id] = $scope.data.reportDefinition.searchFilter[index];
                }
            }

            for(e in entities){

                for(a in entities[e].attributes){
                    $scope.selectAttributes[entities[e].attributes[a].attribute.id] = { entity : entities[e].entity.displayName, attribute : entities[e].attributes[a].attribute, selected: false };
                    $scope.groupAttributes.push({ entity : entities[e].entity.displayName, attribute : entities[e].attributes[a].attribute });
                    if(entities[e].attributes[a].numeric == true) {
                        $scope.aggregationAttributes.push({ entity : entities[e].entity.displayName, attribute : entities[e].attributes[a].attribute })
                    }
                }
            }

            if(selectedAttributes.length > 0){
                for(a in selectedAttributes){
                    $scope.selectAttributes[selectedAttributes[a].attribute.id].selected = true;
                }
            }
            */
        }

        $scope.selectAttribute = function(attribute){

            if($scope.data.selectAttributes[attribute.attribute.id].selected) {
                $scope.data.reportDefinition.selectedCols.push({attribute : attribute.attribute, params : {}});
            }else {
                for(i=0; i < $scope.data.reportDefinition.selectedCols.length; i++){
                    if($scope.data.reportDefinition.selectedCols[i].attribute.id == attribute.attribute.id){
                        $scope.data.reportDefinition.selectedCols.splice(i, 1);
                    }
                }
            }
        }

        $scope.addGroupAttribute = function(position){

            if(($scope.data.groupAttrSelected != null && $scope.data.groupAttrSelected != '') || ($scope.data.groupAttrSelected1 != null && $scope.data.groupAttrSelected1 != '')){

                if(position == 2){

                    if($scope.groupingRows.length >= 1){

                        return;
                    }

                    $scope.groupingCols.push({attribute : $scope.data.selectAttributes[$scope.data.groupAttrSelected].attribute, params : { groupingAttrPosition : position }});

                }else if(position == 1){

                    if($scope.groupingCols.length >= 1){

                        return;
                    }

                    $scope.groupingRows.push({attribute : $scope.data.selectAttributes[$scope.data.groupAttrSelected1].attribute, params : { groupingAttrPosition : position }});

                }

                $scope.data.reportDefinition.gropingCols = $scope.groupingCols.concat($scope.groupingRows);
            }
        }

        $scope.removeGroupAttribute = function(attr, position){

            if(attr != null && $scope.data.reportDefinition.gropingCols != null){

                if(position == 2){
                    for(i = 0; i < $scope.groupingCols.length; i++){
                        if($scope.groupingCols[i].attribute.id == attr.id){
                            $scope.groupingCols.splice(i, 1);
                        }
                    }
                }else if(position == 1){
                    for(i = 0; i < $scope.groupingRows.length; i++){
                        if($scope.groupingRows[i].attribute.id == attr.id){
                            $scope.groupingRows.splice(i, 1);
                        }
                    }
                }

                /*for(a in $scope.data.reportDefinition.gropingCols){

                    if($scope.data.reportDefinition.gropingCols[a].attribute.id == attr.id){
                        $scope.data.reportDefinition.gropingCols.splice(a, 1);
                    }

                }*/

                $scope.data.reportDefinition.gropingCols = $scope.groupingCols.concat($scope.groupingRows);

            }



        }

        $scope.addAggregationAttr = function(){

            if($scope.data.aggregationAttrSelected == null || $scope.data.aggregationAttrSelected == '' ){
                return;
            }
            var index = 0;
            var addNew = true;
            if($scope.data.reportDefinition.aggregationAttrs != null){
                for(a in $scope.data.reportDefinition.aggregationAttrs){
                    if($scope.data.reportDefinition.aggregationAttrs[a].attribute.id == $scope.data.aggregationAttrSelected){
                        index = a;
                        addNew = false;
                        break;
                    }

                }
            }else{
                $scope.data.reportDefinition.aggregationAttrs = [];
            }
            if(addNew){
                $scope.data.reportDefinition.aggregationAttrs.push({'attribute' : $scope.data.selectAttributes[$scope.data.aggregationAttrSelected].attribute, 'aggregationCode' : '00000'});
                index = $scope.data.reportDefinition.aggregationAttrs.length-1;
            }
            var code = $scope.data.reportDefinition.aggregationAttrs[index].aggregationCode.split('');
            code[$scope.functions[$scope.data.appliedFunction].index] = 1;
            $scope.data.reportDefinition.aggregationAttrs[index].aggregationCode = code.join('');
        }

        $scope.removeAggAttr = function(attribute, func){

            if($scope.data.reportDefinition.aggregationAttrs != null){

                for(a in $scope.data.reportDefinition.aggregationAttrs){
                    if($scope.data.reportDefinition.aggregationAttrs[a].attribute.id == attribute){
                        var code = $scope.data.reportDefinition.aggregationAttrs[a].aggregationCode.split('');
                        code[func.index] = 0;
                        $scope.data.reportDefinition.aggregationAttrs[a].aggregationCode = code.join('');

                        if($scope.data.reportDefinition.aggregationAttrs[a].aggregationCode == '00000'){
                            $scope.data.reportDefinition.aggregationAttrs.splice(a, 1);
                        }
                    }
                }
            }
        }

        $scope.run = function(){
            $modalInstance.close();
        }
    }
);


sfe.controller('ReportsHomeController',function ($scope, $http, $rootScope, $compile,
                                                  LocalStore, RepositoryService, ReportSearchFormService, $state) {

    $scope.categories = [];
    $scope.context;

    $scope.init = function(){

        $scope.context = $state.params.context;

        $http(
            {
                method: 'POST',
                url:restUrl+ "/reports/report/home",
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            }
        ).then(function(response){
                $scope.categories = response.data;
            });

    }

    $scope.goto = function(reportId){
        if($scope.context == 'home')
            $state.go('home.dynamicReports', {id:reportId});
        else if($scope.context  =='sysadmin'){
            $state.go('sysadmin.dynamicReports', {id:reportId});
        }
    }

});


