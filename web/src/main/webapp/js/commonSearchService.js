sfe.service('commonSearchService',function($http,LocalStore,RepositoryService){
    this.empPasswordReset=function(owner){
        RepositoryService.restore(owner).success(function(data){
            var dafultPass={"owner":data.user.id,"password":"welcome"}
            $http({
                method: 'PUT',
                url: restUrl + "/core/setdefaultpassword",
                headers: {
                    "CONSUMER_KEY": CONSUMER_KEY,
                    "content-type": "application/json"
                },
                data: dafultPass
            }).success(function (response) {
                if(response){
                    alert("Password changed for "+data.name);
                }
                else{
                    alert("SomeThing Bad Happen");
                }
            });
        })
    }

    this.addDoctorBrands=function(owner){
        RepositoryService.restore(owner).success(function(data){
            var visitedTowns = $modal.open({
                templateUrl: 'addBrand.html',
                controller: 'addBrandController',
                size: 'sm',
                backdrop: false,
                backdropClick: false,
                dialogFade: true,
                keyboard: false
            })
        })
    }

    this.deletePatientDetails=function(owner,resultset){
        console.log(resultset);
        $http({
            method: 'GET',
            url: restUrl + "/sfe/patientapproval/deletepatient/" + owner,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        }).success(function(response){
            if(response){
                //alert("Patient record deleted successfully.");

                for(r in resultset){
                    if(resultset[r].id==owner){
                        resultset.splice(r,1);
                    }
                }
                console.log(resultset);
                return resultset;
            }else{
                alert("Some thing bad happened.");
                return false;
            }
        })
    }
});