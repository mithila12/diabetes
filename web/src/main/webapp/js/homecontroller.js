sfe.controller('HomeController',function($http,LabelService, RepositoryService, LocalStore,months,$modal,$state,$scope){
    var vm =this;
    vm.getRegToday=getRegToday;
    //vm.getDiabetesPieData = getDiabetesPieData;
    //vm.getCvdPieData = getCvdPieData;
    vm.getNephroPieData = getNephroPieData;
    vm.getPatients = getPatients;
    vm.goToPatientDetails= goToPatientDetails;
    vm.getMyProfile= getMyProfile;
    vm.init = init;
    vm.initSearch = initsearch
    vm.getPieData=getPieData;
    vm.getStackGraph = getStackGraph;
    vm.app ={id:"sugrqb"};
    vm.filterList = [{id: "week", Name: "Weekly"}, {id: "quarter", Name: "Quarterly(3 Months)"}, {
        id: "half_year",
        Name: "Half-Yearly(6 Months)"
    }, {id: "year", Name: "Yearly"}];

    vm.typeList = [{id: "sugar_profile_report_type", Name: "Diabetes"}, {id: "cvd_report_type", Name: "CVD"}, {
        id: "nephro_report_type",
        Name: "Nephrology"
    }];
    vm.apps = [{id: "sugrqb", name: "SugrQb"}, {id: "sugrqb+", name: "SugrQb+"}];
    vm.filter = {id:"quarter"};
    vm.pietitle = [{id:"obesity",name:"Obesity"},{id:"nephro_report_type",name:"Creatinine Profile"},{id:"sugar_profile_report_type",name:"HBA1C"}];
    vm.pie= [{id:"obesity"}];
    vm.rType = [{id:"USRLVRT000000000000000000000000000001"}];
    vm.showFilter = "month";
    vm.patientaverage= [{id:"USRLVDIS00000000000000000000000000001",name:"First Time"},{id:"USRLVDIS00000000000000000000000000002",name:"After 3 Month"}];
    vm.patientinfo = [{id:"USRLVDIS00000000000000000000000000001",name:"First Time"}];
    vm.reportType = {id:"sugar_profile_report_type"};
    vm.reportTypes = [{id:"USRLVRT000000000000000000000000000001",name:"HBA1C"},{id:"USRLVRT000000000000000000000000000002",name:"FPG"},{id:"USRLVRT000000000000000000000000000003",name:"PPG"},
        {id:"USRLVRT000000000000000000000000000004",name:"Random Blood Sugar"},{id:"USRLVRT000000000000000000000000000005",name:"SBP"},{id:"USRLVRT0000000000000000000000000000018",name:"DBP"},
        {id:"USRLVRT000000000000000000000000000006",name:"Total Cholesterol"},{id:"USRLVRT000000000000000000000000000007",name:"LDL"},{id:"USRLVRT000000000000000000000000000009",name:"Triglyceride"},
        {id:"USRLVRT000000000000000000000000000010",name:"HDL"},{id:"USRLVRT000000000000000000000000000012",name:"Serum Creatinine"}];
    console.log(vm.filter);

    //vm.setDuration = setDuration;
    //http://localhost:8080/webapp/rest/v1.0/core/framework/patnt/search?searchParams=WD_PATIENT.DOCTOR_ID~DOCTR0000001eadcd380158b51eaf85008000&context=sysadmin
     //http://localhost:8080/webapp/rest/v1.0/core/framework/patients_threshold/find?searchParams=PATIENT_ID~PATNT0000004847b378015920efbfd6008000^REPORT_TYPE~nephro_report_type^context~sysadmin

    
    vm.downloadDetails = downloadDetails;
    vm.downloadQuestionnaireReport = downloadQuestionnaireReport;
    vm.downloadPatientReport = downloadPatientReport;

    function downloadPatientReport(type,duration) {
        console.log(duration);
        window.open("/webapp/report/patient-records?certificate=" +  encodeURIComponent(LocalStore.fetch('USER_INFO').certificate)  + "&type="+ type.id +"&duration="+duration.id);
    }

    function downloadQuestionnaireReport(type) {
        window.open("/webapp/report/questionnaire-report?certificate=" +  encodeURIComponent(LocalStore.fetch('USER_INFO').certificate)  + "&type="+ type.id);
    }

    function downloadDetails(type) {
        window.open("/webapp/report/riskwise-patient?certificate=" +  encodeURIComponent(LocalStore.fetch('USER_INFO').certificate)  + "&type="+ type.id);
    }

    vm.doctordetail = doctordetail;
    function doctordetail(){
        console.log(vm.selectedDoctorRisk);
          vm.selectedDoctorList = [];
          for(var i in vm.selectedDoctorRisk){
          vm.selectedDoctorList.push(vm.selectedDoctorRisk[i].id)
          }

          LocalStore.store("Doctor_List_Records",vm.selectedDoctorList);
          LocalStore.store("report",vm.reportType);
          LocalStore.store("duration",vm.toDate);
          LocalStore.store("date",vm.fromDate);
          LocalStore.store("filter",vm.showFilter);

          console.log("TO DATE>>>>>>>>",LocalStore.fetch("duration"));

        console.log(vm.selectedDoctorList);
    }

    vm.doctorriskdetail = doctorriskdetail;
    function doctorriskdetail(){
        console.log(vm.selectedDoctor);
         vm.doctorlist = [];
         for(var i in vm.selectedDoctor){
           vm.doctorlist.push(vm.selectedDoctor[i].id)
         }
         LocalStore.store('Doctor_List',vm.doctorlist);
        LocalStore.store('riskwisereport',vm.reportType);

         console.log(vm.doctorlist);

    }
   /* vm.detailuser = detailuser
    function detailuser(){
        console.log(vm.detaildoctorlist)
        vm.detailusersList = [];
        for (var i in vm.vm.detaildoctorlist){
            vm.detailusersList.push(vm.detaildoctorlist[i].id)

        }
        LocalStore.store('Datail_user',vm.detailusersList);
        LocalStore.store("duration",vm.toDate);
        LocalStore.store("date",vm.fromDate);

    }*/
    vm.updateChange = updateChange;

    function updateChange(displayType) {
        console.log("000000===============!!!!!!!!!!!!!!!!===================")
        console.log(displayType);
        LocalStore.store('report',displayType);
    }
    vm.avgRecordPatient = avgRecordPatient;
    function avgRecordPatient(){
      vm.avgrecord = [];
      for (var i in vm.avgPatientRecord){
      vm.avgrecord.push(vm.avgPatientRecord[i].id)
      }
        LocalStore.store('Doctor_List_Question',vm.avgrecord);

        LocalStore.store('report',vm.reportType);

    }

    /*function setDuration(duration,type) {
        console.log(duration);
        console.log(vm.filter);
        vm.filter = duration;
        vm.reportType = type;
    }*/

    vm.changeAllData = changeData;

    function changeData(toDate,fromDate,type) {
        console.log("in");
        console.log(vm.showFilter);
        console.log(type);
        initsearch();
        getPieData();
        getTrendData();
        pageViewSelect(vm.app,vm.fromDate,vm.toDate);
        getStackGraph(vm.avgrecord,vm.patientinfo);


    }

    vm.futuredatedisabletoDate = futuredatedisabletoDate;

    function futuredatedisabletoDate(date) {
        console.log(date);
        vm.toDate = date;
        vm.toDate = moment(vm.toDate).format('YYYY-MM-DD');
        console.log(vm.toDate);
        date = new Date(date);
        var today = new Date();
        vm.errorList = [];
        if (date > today) {
            vm.notValid = true;
            //window.plugins.toast.showShortCenter('Enter valid date');
            vm.errorList.push({
                "message": "Enter valid date"
            });
            return vm.notValid;
        }
        //else if(!date){
        //  vm.notValid = true;
        //console.log(vm.notValid);
        //vm.errorList.push({"message": "Please Enter Value"});
        //}
        else {
            vm.notValid = false;
            vm.errorList = [];
            return vm.notValid;
        }
    }

    vm.futuredatedisablefromDate = futuredatedisablefromDate;

    function futuredatedisablefromDate(date) {
        vm.fromDate = date;
        vm.fromDate = moment(vm.fromDate).format('YYYY-MM-DD');
        console.log(vm.fromDate);
        date = new Date(date);

        var today = new Date();
        vm.errorList = [];
        if (date > today) {
            vm.notValiddate = true;
            //window.plugins.toast.showShortCenter('Enter valid date');
            vm.errorList.push({
                "message": "Enter valid date"
            });
            return vm.notValiddate;
        }
        //else if(!date){
        //  vm.notValid = true;
        //console.log(vm.notValid);
        //vm.errorList.push({"message": "Please Enter Value"});
        //}
        else {
            vm.notValiddate = false;
            vm.errorList = [];
            return vm.notValiddate;
        }
    }



    /*function init() {
        vm.toDate = new Date();
        vm.fromDate = new Date();
        vm.fromDate.setMonth(vm.fromDate.getMonth()-3)

    vm.futuredatedisabletoDate = futuredatedisabletoDate;

    function futuredatedisabletoDate(date) {
        console.log(date);
        vm.toDate = date;
        console.log(vm.toDate);
        date = new Date(date);
        var today = new Date();
        vm.errorList = [];
        if (date > today) {
            vm.notValid = true;
            //window.plugins.toast.showShortCenter('Enter valid date');
            vm.errorList.push({
                "message": "Enter valid date"
            });
            return vm.notValid;
        }
        //else if(!date){
        //  vm.notValid = true;
        //console.log(vm.notValid);
        //vm.errorList.push({"message": "Please Enter Value"});
        //}
        else {
            vm.notValid = false;
            vm.errorList = [];
            return vm.notValid;
        }
    }

    vm.futuredatedisablefromDate = futuredatedisablefromDate;

    function futuredatedisablefromDate(date) {
        vm.fromDate = date;
        console.log(vm.fromDate);
        date = new Date(date);
        var today = new Date();
        vm.errorList = [];
        if (date > today) {
            vm.notValiddate = true;
            //window.plugins.toast.showShortCenter('Enter valid date');
            vm.errorList.push({
                "message": "Enter valid date"
            });
            return vm.notValiddate;
        }
        //else if(!date){
        //  vm.notValid = true;
        //console.log(vm.notValid);
        //vm.errorList.push({"message": "Please Enter Value"});
        //}
        else {
            vm.notValiddate = false;
            vm.errorList = [];
            return vm.notValiddate;
        }
    }*/


    function initsearch() {
        vm.getPageView = pageViewSelect;
        function pageViewSelect(app,fromDate,toDate) {
            $http({
                method: 'get',
                url: restUrl + "/diabetes/analytics/averagePageViewSelect/"+vm.app.id+'/'+vm.fromDate+'/'+vm.toDate+'/'+vm.showFilter,
                headers: {
                    "CONSUMER_KEY": CONSUMER_KEY,
                    "content-type": "application/json"
                }
            }).then(function (response) {
                console.log(response.data.xaxis);
                vm.pageName = [];
                vm.values = [];
                console.log(vm.fromDate);
                console.log(vm.toDate);
                console.log(vm.app);

                vm.horizontalChart = {
                    series: response.data.series,
                    title: {
                        text: ''
                    },

                    xAxis: {
                        categories: response.data.xaxis

                    },
                    yAxis: {

                        title: {
                            text: 'no of times feature used'

                        }

                    },
                    options: {
                        exporting: {
                            enabled: false
                        }
                    },

                    credits: {
                        enabled: false
                    },

                }
            })

        }
        $http({
            method:'get',
            url: restUrl + "/diabetes/analytics/activeDoctorsCount/",
            headers:{
                "CONSUMER_KEY": CONSUMER_KEY,
                "content-type": "application/json"
            }
            //http://localhost:8080/webapp/rest/v1.0/image/ImageService/select/PATNT000000797d92ab01594509d482008000/SYSLVIMGTP000000000000000000000000001
        }).then(function (response){
            console.log(response);
            vm.activeDoctorsCount = response.data;
            $http({
                method:'get',
                url: restUrl + "/diabetes/analytics/totalRegisteredDoctorsCount/",
                headers:{
                    "CONSUMER_KEY": CONSUMER_KEY,
                    "content-type": "application/json"
                }
                //http://localhost:8080/webapp/rest/v1.0/image/ImageService/select/PATNT000000797d92ab01594509d482008000/SYSLVIMGTP000000000000000000000000001
            }).then(function (response){
                console.log(response);
                vm.totalDoctors = response.data;
                vm.doctorsOptedOut = vm.totalDoctors - vm.activeDoctorsCount;
                $http({
                    method:'get',
                    url: restUrl + "/diabetes/PatientService/getAllDoctors/",
                    headers:{
                        "CONSUMER_KEY": CONSUMER_KEY,
                        "content-type": "application/json"
                    }
                    //http://localhost:8080/webapp/rest/v1.0/image/ImageService/select/PATNT000000797d92ab01594509d482008000/SYSLVIMGTP000000000000000000000000001
                }).then(function (response){
                    console.log(response);
                    vm.doctors = response.data;
                    vm.doctorlist =[];
                    vm.selectedDoctorList=[];
                    vm.selectedTrendDoctorList =[];
                    vm.avgrecord=[];
                    console.log(vm.doctors);
                    for(var i in vm.doctors){
                        vm.doctorlist.push(vm.doctors[i].id);
                        vm.selectedDoctorList.push(vm.doctors[i].id);
                        vm.avgrecord.push(vm.doctors[i].id);
                        vm.selectedTrendDoctorList.push(vm.doctors[i].id);
                    }
                    //vm.rType.id = "USRLVRT000000000000000000000000000001";
                    vm.pie.id = "sugar_profile_report_type";
                    getPieData();

                    getPatientData(vm.selectedDoctorList,vm.toDate,vm.fromDate);
                    console.log(vm.selectedDoctorList);

                    $http({
                        method: 'get',
                        url: restUrl + "/diabetes/analytics/totalRegisteredPatientCount/",
                        headers: {
                            "CONSUMER_KEY": CONSUMER_KEY,
                            "content-type": "application/json"
                        }
                    }).then(function (response) {
                        console.log(response);
                        vm.totalPatients = response.data;
                        $http({
                            method: 'get',
                            url: restUrl + "/diabetes/analytics/activePatientCount/",
                            headers: {
                                "CONSUMER_KEY": CONSUMER_KEY,
                                "content-type": "application/json"
                            }
                        }).then(function (response) {
                            console.log(response);
                            vm.activePatients = response.data;
                            vm.app ={id:"sugrqb"};
                            pageViewSelect(vm.app,vm.fromDate,vm.toDate);
                            vm.patientsOptedOut = vm.totalPatients - vm.activePatients;

                            $http({
                                method: 'get',
                                url: restUrl + "/diabetes/analytics/genderwisePatients/",
                                headers: {
                                    "CONSUMER_KEY": CONSUMER_KEY,
                                    "content-type": "application/json"
                                }
                            }).then(function (response) {
                                console.log(response);
                                vm.gender = [];
                                vm.count =[];
                                vm.series = [];
                                vm.genderwisePatients = response.data;
                                console.log(vm.genderwisePatients);


                                console.log(vm.series);
                                vm.genderGraph = {
                                    chart: {
                                        type: 'column'
                                    },
                                    xAxis: {
                                        categories: [
                                            'Male',
                                            'Female'
                                        ],
                                        crosshair: true
                                    },
                                    yAxis: {
                                        min: 0,
                                        title: {
                                            text: 'Patients'
                                        }
                                    },
                                    series: vm.genderwisePatients
                                }
                                //     series: [{
                                //     type:'column',
                                //     name: '18-24',
                                //     data: [49.9, 71.5]
                                //
                                // }, {
                                //         type:'column',
                                //     name: '25-30',
                                //     data: [83.6, 78.8]
                                //
                                // }, {
                                //         type:'column',
                                //     name: '31-36',
                                //     data: [48.9, 38.8]
                                //
                                // }, {
                                //         type:'column',
                                //     name: '37-death',
                                //     data: [42.4, 33.2]
                                //
                                // }]
                                // }
                                // var chart = {
                                //     type: 'pie',
                                //     options3d: {
                                //         enabled: true,
                                //         alpha: 45,
                                //         beta: 0
                                //     }
                                // }
                                // var options = {
                                //     exporting: {
                                //         enabled: false
                                //     }
                                // }
                                // var credits = {
                                //     enabled: false
                                // }
                                // var title = {
                                //     text: ''
                                // };
                                // var plotOptions = {
                                //     pie: {
                                //         allowPointSelect: false,
                                //         cursor: 'pointer',
                                //         dataLabels: {
                                //             enabled: true
                                //         },
                                //         showInLegend: true
                                //     }
                                // };
                                // var series = [{
                                //     type: 'pie',
                                //     data: [{
                                //         name: 'High Risk',
                                //         y: 20
                                //     }, {
                                //         name: 'Medium Risk',
                                //         y: 40
                                //     }, {
                                //         name: 'Low Risk',
                                //         y: 40
                                //     }],
                                //     colors: ['#FF5E5B', '#D7B83B', '#2A9DA4']
                                // }];
                                // vm.pieChart1 = {};
                                // vm.pieChart1.chart = chart;
                                // vm.pieChart1.title = title;
                                // vm.pieChart1.series = series;
                                // vm.pieChart1.plotOptions = plotOptions;
                                // vm.pieChart1.options = options;
                                // vm.pieChart1.credits = credits;

                                getStackGraph(vm.avgrecord,vm.patientinfo);

                                // vm.stackGraph = {
                                //     title: {
                                //         text: 'Stacked column chart'
                                //     },
                                //     xAxis: {
                                //         categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
                                //     },
                                //     yAxis: {
                                //         min: 0,
                                //         title: {
                                //             text: 'Total fruit consumption'
                                //         },
                                //         stackLabels: {
                                //             enabled: true,
                                //             style: {
                                //                 fontWeight: 'bold',
                                //                 color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                                //             }
                                //         }
                                //     },
                                //     options: {
                                //         chart: {
                                //             type: 'column'
                                //         },
                                //         legend: {
                                //             align: 'right',
                                //             x: -70,
                                //             verticalAlign: 'top',
                                //             y: 20,
                                //             floating: true,
                                //             backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                                //             borderColor: '#CCC',
                                //             borderWidth: 1,
                                //             shadow: false
                                //         },
                                //         tooltip: {
                                //             formatter: function() {
                                //                 return '<b>'+ this.x +'</b><br/>'+
                                //                     this.series.name +': '+ this.y +'<br/>'+
                                //                     'Total: '+ this.point.stackTotal;
                                //             }
                                //         },
                                //         plotOptions: {
                                //             column: {
                                //                 stacking: 'normal',
                                //                 dataLabels: {
                                //                     enabled: true,
                                //                     color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                                //                     style: {
                                //                         textShadow: '0 0 3px black, 0 0 3px black'
                                //                     }
                                //                 }
                                //             }
                                //         }},
                                //     series: [{
                                //         name: 'John',
                                //         data: [5, 3, 4, 7, 2]
                                //     }, {
                                //         name: 'Jane',
                                //         data: [2, 2, 3, 2, 1]
                                //     }, {
                                //         name: 'Joe',
                                //         data: [3, 4, 4, 2, 5]
                                //     }]
                                // }




                                /* $http({
                                 method: 'get',
                                 url: restUrl + "/diabetes/analytics/getQuestionnaireFilledCount/",
                                 headers: {
                                 "CONSUMER_KEY": CONSUMER_KEY,
                                 "content-type": "application/json"
                                 }
                                 }).then(function (response) {
                                 vm.questionsFilled = response.data;

                                 vm.notFilled = vm.activePatients - vm.questionsFilled;*/

                                $http({
                                    method: 'get',
                                    url: restUrl + "/diabetes/analytics/getAverageTimeSpentByApp/"+ vm.fromDate +'/' + vm.toDate,
                                    headers: {
                                        "CONSUMER_KEY": CONSUMER_KEY,
                                        "content-type": "application/json"
                                    }
                                }).then(function (response) {
                                    console.log(response.data);
                                    vm.timeList =[];
                                    vm.appTimeData = response.data;
                                    for(var i in vm.appTimeData){
                                        vm.timeList.push(parseFloat(vm.appTimeData[i].hours.toFixed(2)));
                                    }
                                    vm.appGraph = {
                                        chart: {
                                            type: 'column'
                                        },
                                        xAxis: {
                                            categories: [
                                                'SugrQb',
                                                'SugrQb+'
                                            ],
                                            crosshair: true
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'hours'
                                            }
                                        },
                                        series:
                                            [{
                                                type:"column",
                                                name:"Avg. Time Spent",
                                                data: vm.timeList
                                            }]
                                    }

                                    $http({
                                        method: 'get',

                                        url: restUrl + "/diabetes/analytics/totalActiveUsersSelect/"+ vm.fromDate +'/' + vm.toDate+'/'+vm.showFilter,
                                        headers: {
                                            "CONSUMER_KEY": CONSUMER_KEY,
                                            "content-type": "application/json"
                                        }
                                    }).then(function (response) {
                                        vm.usersData = {
                                            series: response.data.series,
                                            title: {
                                                text: 'Total Active Users'
                                            },
                                            xAxis: {
                                                categories: response.data.xaxis
                                            },
                                            yAxis:{
                                                title: {
                                                    text: 'Total Active Users'
                                                }
                                            },
                                            options: {
                                                exporting: {
                                                    enabled: false
                                                }
                                            },
                                            credits: {
                                                enabled: false
                                            }
                                        }

                                        $http({
                                            method:'get',
                                            url: restUrl + "/diabetes/analytics/totalInvitesSent/",
                                            headers:{
                                                "CONSUMER_KEY": CONSUMER_KEY,
                                                "content-type": "application/json"
                                            }
                                            //http://localhost:8080/webapp/rest/v1.0/image/ImageService/select/PATNT000000797d92ab01594509d482008000/SYSLVIMGTP000000000000000000000000001
                                        }).then(function (response){
                                            console.log(response);
                                            vm.totalInvitesSent = response.data;

                                            $http({
                                                method:'get',
                                                url: restUrl + "/diabetes/analytics/getPendingRegistrations/",
                                                headers:{
                                                    "CONSUMER_KEY": CONSUMER_KEY,
                                                    "content-type": "application/json"
                                                }
                                                //http://localhost:8080/webapp/rest/v1.0/image/ImageService/select/PATNT000000797d92ab01594509d482008000/SYSLVIMGTP000000000000000000000000001
                                            }).then(function (response){
                                                console.log(response);
                                                vm.getPendingRegistrations = response.data;


                                            })
                                        });

                                    })

                                })
                                //})


                            })


                        });
                    });
                });

            }, function (error){
                console.log(error);
            })
        }, function (error){
            console.log(error);
        })
    }

    function init() {
        LocalStore.remove('Doctor_List_Records');
        LocalStore.remove('Doctor_List_Question');
        LocalStore.remove('Doctor_List');

        console.log(vm.showFilter);
        vm.toDate = new Date();

        vm.toDate = moment(vm.toDate).format('YYYY-MM-DD');
        console.log(vm.toDate);

        vm.fromDate = new Date();
        vm.fromDate.setMonth(vm.fromDate.getMonth()-3);
        vm.fromDate = moment(vm.fromDate).format('YYYY-MM-DD');
        console.log(vm.fromDate);



        $http({
            method:'get',
            url: restUrl + "/diabetes/analytics/activeDoctorsCount/",
            headers:{
                "CONSUMER_KEY": CONSUMER_KEY,
                "content-type": "application/json"
            }
            //http://localhost:8080/webapp/rest/v1.0/image/ImageService/select/PATNT000000797d92ab01594509d482008000/SYSLVIMGTP000000000000000000000000001
        }).then(function (response){
            console.log(response);
            vm.activeDoctorsCount = response.data;
            $http({
                method:'get',
                url: restUrl + "/diabetes/analytics/totalRegisteredDoctorsCount/",
                headers:{
                    "CONSUMER_KEY": CONSUMER_KEY,
                    "content-type": "application/json"
                }
                //http://localhost:8080/webapp/rest/v1.0/image/ImageService/select/PATNT000000797d92ab01594509d482008000/SYSLVIMGTP000000000000000000000000001
            }).then(function (response){
                console.log(response);
                vm.totalDoctors = response.data;
                vm.doctorsOptedOut = vm.totalDoctors - vm.activeDoctorsCount;
                $http({
                    method:'get',
                    url: restUrl + "/diabetes/PatientService/getAllDoctors/",
                    headers:{
                        "CONSUMER_KEY": CONSUMER_KEY,
                        "content-type": "application/json"
                    }
                    //http://localhost:8080/webapp/rest/v1.0/image/ImageService/select/PATNT000000797d92ab01594509d482008000/SYSLVIMGTP000000000000000000000000001
                }).then(function (response){
                    console.log(response);
                    vm.doctors = response.data;
                    vm.doctorlist =[];
                    vm.selectedDoctorList=[];
                    vm.selectedTrendDoctorList=[];
                    vm.avgrecord=[];
                    console.log(vm.doctors);
                    for(var i in vm.doctors){
                        vm.doctorlist.push(vm.doctors[i].id);
                         vm.selectedDoctorList.push(vm.doctors[i].id);
                         vm.selectedTrendDoctorList.push(vm.doctors[i].id);
                         vm.avgrecord.push(vm.doctors[i].id);
                    }
                    vm.pie.id = "sugar_profile_report_type";
                    vm.rType.id ="USRLVRT000000000000000000000000000001";
                    getPieData();
                    getTrendData();
                    getPatientData(vm.selectedDoctorList,vm.toDate,vm.fromDate);
                    console.log(vm.selectedDoctorList);

                $http({
                    method: 'get',
                    url: restUrl + "/diabetes/analytics/totalRegisteredPatientCount/",
                    headers: {
                        "CONSUMER_KEY": CONSUMER_KEY,
                        "content-type": "application/json"
                    }
                }).then(function (response) {
                    console.log(response);
                    vm.totalPatients = response.data;
                    $http({
                        method: 'get',
                        url: restUrl + "/diabetes/analytics/activePatientCount/",
                        headers: {
                            "CONSUMER_KEY": CONSUMER_KEY,
                            "content-type": "application/json"
                        }
                    }).then(function (response) {
                        console.log(response);
                        vm.activePatients = response.data;
                        vm.app ={id:"sugrqb"};
                        pageViewSelect(vm.app,vm.fromDate,vm.toDate);
                        vm.patientsOptedOut = vm.totalPatients - vm.activePatients;

                        $http({
                            method: 'get',
                            url: restUrl + "/diabetes/analytics/genderwisePatients/",
                            headers: {
                                "CONSUMER_KEY": CONSUMER_KEY,
                                "content-type": "application/json"
                            }
                        }).then(function (response) {
                            console.log(response);
                            vm.gender = [];
                            vm.count =[];
                            vm.series = [];
                            vm.genderwisePatients = response.data;
                            console.log(vm.genderwisePatients);


                            console.log(vm.series);
                            vm.genderGraph = {
                                chart: {
                                    type: 'column'
                                },
                                xAxis: {
                                    categories: [
                                        'Male',
                                        'Female'
                                    ],
                                    crosshair: true
                                },
                                yAxis: {
                                    min: 0,
                                    title: {
                                        text: 'Age'
                                    }
                                },
                                series: vm.genderwisePatients
                            }
                            //     series: [{
                            //     type:'column',
                            //     name: '18-24',
                            //     data: [49.9, 71.5]
                            //
                            // }, {
                            //         type:'column',
                            //     name: '25-30',
                            //     data: [83.6, 78.8]
                            //
                            // }, {
                            //         type:'column',
                            //     name: '31-36',
                            //     data: [48.9, 38.8]
                            //
                            // }, {
                            //         type:'column',
                            //     name: '37-death',
                            //     data: [42.4, 33.2]
                            //
                            // }]
                            // }
                            // var chart = {
                            //     type: 'pie',
                            //     options3d: {
                            //         enabled: true,
                            //         alpha: 45,
                            //         beta: 0
                            //     }
                            // }
                            // var options = {
                            //     exporting: {
                            //         enabled: false
                            //     }
                            // }
                            // var credits = {
                            //     enabled: false
                            // }
                            // var title = {
                            //     text: ''
                            // };
                            // var plotOptions = {
                            //     pie: {
                            //         allowPointSelect: false,
                            //         cursor: 'pointer',
                            //         dataLabels: {
                            //             enabled: true
                            //         },
                            //         showInLegend: true
                            //     }
                            // };
                            // var series = [{
                            //     type: 'pie',
                            //     data: [{
                            //         name: 'High Risk',
                            //         y: 20
                            //     }, {
                            //         name: 'Medium Risk',
                            //         y: 40
                            //     }, {
                            //         name: 'Low Risk',
                            //         y: 40
                            //     }],
                            //     colors: ['#FF5E5B', '#D7B83B', '#2A9DA4']
                            // }];
                            // vm.pieChart1 = {};
                            // vm.pieChart1.chart = chart;
                            // vm.pieChart1.title = title;
                            // vm.pieChart1.series = series;
                            // vm.pieChart1.plotOptions = plotOptions;
                            // vm.pieChart1.options = options;
                            // vm.pieChart1.credits = credits;

                            getStackGraph(vm.avgrecord,vm.patientinfo);

                            // vm.stackGraph = {
                            //     title: {
                            //         text: 'Stacked column chart'
                            //     },
                            //     xAxis: {
                            //         categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
                            //     },
                            //     yAxis: {
                            //         min: 0,
                            //         title: {
                            //             text: 'Total fruit consumption'
                            //         },
                            //         stackLabels: {
                            //             enabled: true,
                            //             style: {
                            //                 fontWeight: 'bold',
                            //                 color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                            //             }
                            //         }
                            //     },
                            //     options: {
                            //         chart: {
                            //             type: 'column'
                            //         },
                            //         legend: {
                            //             align: 'right',
                            //             x: -70,
                            //             verticalAlign: 'top',
                            //             y: 20,
                            //             floating: true,
                            //             backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                            //             borderColor: '#CCC',
                            //             borderWidth: 1,
                            //             shadow: false
                            //         },
                            //         tooltip: {
                            //             formatter: function() {
                            //                 return '<b>'+ this.x +'</b><br/>'+
                            //                     this.series.name +': '+ this.y +'<br/>'+
                            //                     'Total: '+ this.point.stackTotal;
                            //             }
                            //         },
                            //         plotOptions: {
                            //             column: {
                            //                 stacking: 'normal',
                            //                 dataLabels: {
                            //                     enabled: true,
                            //                     color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                            //                     style: {
                            //                         textShadow: '0 0 3px black, 0 0 3px black'
                            //                     }
                            //                 }
                            //             }
                            //         }},
                            //     series: [{
                            //         name: 'John',
                            //         data: [5, 3, 4, 7, 2]
                            //     }, {
                            //         name: 'Jane',
                            //         data: [2, 2, 3, 2, 1]
                            //     }, {
                            //         name: 'Joe',
                            //         data: [3, 4, 4, 2, 5]
                            //     }]
                            // }




                           /* $http({
                                method: 'get',
                                url: restUrl + "/diabetes/analytics/getQuestionnaireFilledCount/",
                                headers: {
                                    "CONSUMER_KEY": CONSUMER_KEY,
                                    "content-type": "application/json"
                                }
                            }).then(function (response) {
                                vm.questionsFilled = response.data;

                                vm.notFilled = vm.activePatients - vm.questionsFilled;*/

                                $http({
                                    method: 'get',
                                    url: restUrl + "/diabetes/analytics/getAverageTimeSpentByApp/"+ vm.fromDate +'/' + vm.toDate,
                                    headers: {
                                        "CONSUMER_KEY": CONSUMER_KEY,
                                        "content-type": "application/json"
                                    }
                                }).then(function (response) {
                                    console.log(response.data);
                                    vm.timeList =[];
                                    vm.appTimeData = response.data;
                                    for(var i in vm.appTimeData){
                                        vm.timeList.push(parseFloat(vm.appTimeData[i].hours.toFixed(2)));
                                    }
                                    vm.appGraph = {
                                        chart: {
                                            type: 'column'
                                        },
                                        xAxis: {
                                            categories: [
                                                'SugrQb',
                                                'SugrQb+'
                                            ],
                                            crosshair: true
                                        },
                                        yAxis: {
                                            min: 0,
                                            title: {
                                                text: 'hours'
                                            }
                                        },
                                        series:
                                            [{
                                            type:"column",
                                          name:"Avg. Time Spent",
                                          data: vm.timeList
                                        }]
                                    }

                                    $http({
                                        method: 'get',

                                        url: restUrl + "/diabetes/analytics/totalActiveUsersSelect/"+ vm.fromDate +'/' + vm.toDate+'/'+vm.showFilter,
                                        headers: {
                                            "CONSUMER_KEY": CONSUMER_KEY,
                                            "content-type": "application/json"
                                        }
                                    }).then(function (response) {

                                        vm.usersData = {
                                            series: response.data.series,
                                            title: {
                                                text: 'Total Active Users'
                                            },
                                            xAxis: {
                                                categories: response.data.xaxis
                                            },
                                            yAxis:{
                                                title: {
                                                    text: 'Total Active Users'
                                                }
                                            },
                                            options: {
                                                exporting: {
                                                    enabled: false
                                                }
                                            },
                                            credits: {
                                                enabled: false
                                            }
                                        }

                                        $http({
                                                    method:'get',
                                                    url: restUrl + "/diabetes/analytics/totalInvitesSent/",
                                                    headers:{
                                                        "CONSUMER_KEY": CONSUMER_KEY,
                                                        "content-type": "application/json"
                                                    }
                                                    //http://localhost:8080/webapp/rest/v1.0/image/ImageService/select/PATNT000000797d92ab01594509d482008000/SYSLVIMGTP000000000000000000000000001
                                                }).then(function (response){
                                                    console.log(response);
                                                    vm.totalInvitesSent = response.data;

                                                    $http({
                                                                 method:'get',
                                                                 url: restUrl + "/diabetes/analytics/getPendingRegistrations/",
                                                                 headers:{
                                                                      "CONSUMER_KEY": CONSUMER_KEY,
                                                                      "content-type": "application/json"
                                                                 }
                                                                  //http://localhost:8080/webapp/rest/v1.0/image/ImageService/select/PATNT000000797d92ab01594509d482008000/SYSLVIMGTP000000000000000000000000001
                                                        }).then(function (response){
                                                                console.log(response);
                                                                vm.getPendingRegistrations = response.data;

                                                        })
                                                    });

                                    })

                                })
                            //})


                        })


                    });
                });
            });

            }, function (error){
                console.log(error);
            })
        }, function (error){
            console.log(error);
        })
    }

    vm.getTrendData = getTrendData;
    vm.trendDoctorsAdd = trendDoctorsAdd;

    function trendDoctorsAdd() {
        console.log(vm.selectedTrendDoctor);
        vm.selectedTrendDoctorList = [];
        for(var i in vm.selectedTrendDoctor){
            vm.selectedTrendDoctorList.push(vm.selectedTrendDoctor[i].id)
        }

        LocalStore.store("Doctor_List_Trend_Records",vm.selectedTrendDoctorList);
        LocalStore.store("report",vm.rType);
        LocalStore.store("duration",vm.toDate);
        LocalStore.store("date",vm.fromDate);

        console.log("TO DATE>>>>>>>>",LocalStore.fetch("duration"));

        console.log(vm.selectedTrendDoctorList);
    }
    function getTrendData() {
        $http({
            method: 'post',
            url: restUrl + "/diabetes/analytics/getPercentPatients/",
            headers: {
                "CONSUMER_KEY": CONSUMER_KEY,
                "content-type": "application/json"
            },
            data:{doctors:vm.selectedTrendDoctorList,reportId:vm.rType.id,fromDate:vm.fromDate,toDate:vm.toDate}
        }).then(function (response) {
            vm.trendGraph ={
                title: {
                    text: ''
                },
                xAxis: {
                    categories: response.data.xaxis
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                options: {
                    chart: {
                        type: 'column'
                    },
                    legend: {
                        align: 'center',
                        x: 0,
                        verticalAlign: 'top',
                        y: 0,
                        floating: true,
                        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                        borderColor: '#CCC',
                        borderWidth: 1,
                        shadow: false
                    },
                    tooltip: {
                        formatter: function() {
                            return '<b>'+ this.x +'</b><br/>'+
                                this.series.name +': '+ this.y +'<br/>'+
                                'Total: '+ this.point.stackTotal;
                        }
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: true,
                                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                                style: {
                                    textShadow: '0 0 3px black, 0 0 3px black'
                                }
                            }
                        }
                    }},
                series: response.data.series
            }
        }, function (err) {
            throw err;
        })

    }
    vm.getPageView = pageViewSelect;
    function pageViewSelect(app,fromDate,toDate) {
        $http({
            method: 'get',
            url: restUrl + "/diabetes/analytics/averagePageViewSelect/"+vm.app.id+'/'+vm.fromDate+'/'+vm.toDate+'/'+vm.showFilter,
            headers: {
                "CONSUMER_KEY": CONSUMER_KEY,
                "content-type": "application/json"
            }
        }).then(function (response) {
            console.log(response.data.xaxis);
            vm.pageName = [];
            vm.values = [];
        console.log(vm.fromDate);
        console.log(vm.toDate);
            console.log(vm.app);
            LocalStore.store("todate",vm.toDate);
            LocalStore.store("fromdate",vm.fromDate);
            LocalStore.store("appId",vm.app);

            vm.horizontalChart = {
                series: response.data.series,
                title: {
                    text: ''
                },
               
                xAxis: {
                    categories: response.data.xaxis
                    
                },
                yAxis: {
                   
                    title: {
                        text: 'no of times feature used'

                    }
                   
                },
                options: {
                    exporting: {
                        enabled: false
                    }
                },
                
                credits: {
                    enabled: false
                },
                
            }
        })

    }

    function getRegToday(){
        var id = LocalStore.fetch('USER_INFO').id;
        $http({
            method: 'get',
            url: restUrl + "/core/framework/patients_reg_today/find?searchParams=DOCTOR_ID~"+id+"&context=sysadmin",
            headers: {
                "CONSUMER_KEY": CONSUMER_KEY,
                "content-type": "application/json"
            }
        }).then(function (response){
            console.log(response);
            vm.regPatient = response.data;
        });
    }



    function getMyProfile(){
        var id = LocalStore.fetch('USER_INFO').id;
            $http({
                        method: 'get',
                        url: restUrl + "/core/framework/"+id,
                        headers:{
                            "CONSUMER_KEY": CONSUMER_KEY,
                            "content-type": "application/json"
                        }
                  }).then(function (response){
                    console.log(response);
                    $http({
                        method:'get',
                        url: restUrl + "/image/ImageService/select/" +id+"/SYSLVIMGTP000000000000000000000000001",
                        headers:{
                            "CONSUMER_KEY": CONSUMER_KEY,
                            "content-type": "application/json"
                        }
                        //http://localhost:8080/webapp/rest/v1.0/image/ImageService/select/PATNT000000797d92ab01594509d482008000/SYSLVIMGTP000000000000000000000000001
                    }).then(function (image){
                        console.log(image);
                        vm.image = image;
                    }, function (error){
                        console.log(error);
                    })
                    vm.myProfile = response.data;
                  })

    }


    function getPatients(){
        var id = LocalStore.fetch("USER_INFO").id;
        $http({
            method: 'get',
            url: restUrl + "/core/framework/patnt/search?searchParams=DOCTOR_ID~"+id+"&mode=simple&context=sysadmin",
            headers: {
                "CONSUMER_KEY": CONSUMER_KEY,
                "content-type": "application/json"
            }
        }).then(function (response){
            console.log(response);
            vm.patientList = response.data.result;
        });
    }

    function getPieData() {

        $http({
            method: 'post',
            url: restUrl + "/diabetes/analytics/doctorwiseRiskPatients/",
            headers: {
                "CONSUMER_KEY": CONSUMER_KEY,
                "content-type": "application/json"
            },
            data:{doctors:vm.doctorlist,type:vm.pie.id}
        }).then(function (response){
            console.log(response);
            vm.riskData = response.data;
            if(vm.pie.id == "obesity"){
                var chart = {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                }
                var options = {
                    exporting: {
                        enabled: false
                    }
                }
                var credits = {
                    enabled: false
                }
                var title = {
                    text: ''
                };
                var plotOptions = {
                    pie: {
                        allowPointSelect: false,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true
                        },
                        showInLegend: true
                    }
                };
                var series = [{
                    type: 'pie',
                    data: [{
                        name: 'Overweight',
                        y: vm.riskData.h
                    }, {
                        name: 'Normal',
                        y: vm.riskData.m
                    }, {
                        name: 'Underweight',
                        y: vm.riskData.l
                    }],
                    colors: ['#FF5E5B', '#D7B83B', '#2A9DA4']
                }];
            }else{
                var chart = {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45,
                        beta: 0
                    }
                }
                var options = {
                    exporting: {
                        enabled: false
                    }
                }
                var credits = {
                    enabled: false
                }
                var title = {
                    text: ''
                };
                var plotOptions = {
                    pie: {
                        allowPointSelect: false,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true
                        },
                        showInLegend: true
                    }
                };
                var series = [{
                    type: 'pie',
                    data: [{
                        name: 'Beyond Target',
                        y: vm.riskData.h
                    }, {
                        name: 'Within Target',
                        y: vm.riskData.m
                    }],
                    colors: ['#FF5E5B', '#D7B83B']
                }];
            }

            vm.pieChart1 = {};
            vm.pieChart1.chart = chart;
            vm.pieChart1.title = title;
            vm.pieChart1.series = series;
            vm.pieChart1.plotOptions = plotOptions;
            vm.pieChart1.options = options;
            vm.pieChart1.credits = credits;

        });

    }

    vm.saveRiskReportType = saveRiskReportType;
    function saveRiskReportType() {
        LocalStore.store("riskwisereport",vm.pie);

    }

    vm.saveReportType = saveReportType;
    function saveReportType() {
        LocalStore.store("report",vm.reportType);
    }

    vm.getPatientData = getPatientData;

    function getPatientData(doctorlist) {
        console.log(vm.toDate);
        console.log(vm.fromDate);
        console.log(vm.selectedDoctorList);
        console.log(vm.showFilter);
        $http({
            method: 'post',
            url: restUrl + "/diabetes/analytics/averagePatientRecords/",
            headers: {
                "CONSUMER_KEY": CONSUMER_KEY,
                "content-type": "application/json"
            },
            data:{
                type:vm.reportType.id,
                //duration:vm.filter.id,
                doctors: vm.selectedDoctorList,
                toDate:vm.toDate,
                fromDate:vm.fromDate,
                filter:vm.showFilter
            }
        }).then(function (response) {
            console.log(response.data);
            vm.hba1c = {
                series: response.data.series,
                title: {
                    text: ''
                },
                xAxis: {
                    categories: response.data.xaxis
                },
                yAxis:{
                     title:{
                        text: 'no of records'
                     }
                },
                options: {
                    exporting: {
                        enabled: false
                    }
                },
                credits: {
                    enabled: false
                },
            }
        });

        }

    function getStackGraph(duration,rep){
        $http({
            method: 'post',
            url: restUrl + "/diabetes/analytics/questionnaireGraph/",
            headers: {
                "CONSUMER_KEY": CONSUMER_KEY,
                "content-type": "application/json"
            },
            data:{
                type:vm.patientinfo.id,
                doctors:vm.avgrecord
            }
        }).then(function (response) {
            console.log(response.data);
            console.log(vm.patientinfo);
            // vm.hba1c = {
            //     series: response.data.series,
            //     title: {
            //         text: ''
            //     },
            //     xAxis: {
            //         categories: response.data.xAxis
            //     },
            //     options: {
            //         exporting: {
            //             enabled: false
            //         }
            //     },
            //     credits: {
            //         enabled: false
            //     },
            // }


            /*vm.stackGraph ={
                title: {
                    text: ''
                },
                xAxis: {
                    categories: response.data.xaxis
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                options: {
                    chart: {
                        type: 'column'
                    },
                    legend: {
                        align: 'right',
                        x: 0,
                        verticalAlign: 'top',
                        y: 20,
                        floating: true,
                        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                        borderColor: '#CCC',
                        borderWidth: 1,
                        shadow: false
                    },
                    tooltip: {
                        formatter: function() {
                            return '<b>'+ this.x +'</b><br/>'+
                                this.series.name +': '+ this.y +'<br/>'+
                                'Total: '+ this.point.stackTotal;
                        }
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: true,
                                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                                style: {
                                    textShadow: '0 0 3px black, 0 0 3px black'
                                }
                            }
                        }
                    }},
                series: response.data.series
            }*/
             vm.quesdata = response.data;
                        var chart = {
                            type: 'pie',
                            options3d: {
                                enabled: true,
                                alpha: 45,
                                beta: 0
                            }
                        }
                        var options = {
                            exporting: {
                                enabled: false
                            }
                        }
                        var credits = {
                            enabled: false
                        }
                        var title = {
                            text: ''
                        };
                        var plotOptions = {
                            pie: {
                                allowPointSelect: false,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true
                                },
                                showInLegend: true
                            }
                        };
                        var series = [{
                            type: 'pie',
                            data: [{
                                name: 'Questionnaire filled',
                                y: vm.quesdata.filledcount
                            }, {
                                name: 'Questionnaire not filled',
                                y: vm.quesdata.remaining
                            }],
                            colors: ['#FF5E5B', '#D7B83B']
                        }];
                        vm.stackGraph = {};
                        vm.stackGraph.chart = chart;
                        vm.stackGraph.title = title;
                        vm.stackGraph.series = series;
                        vm.stackGraph.plotOptions = plotOptions;
                        vm.stackGraph.options = options;
                        vm.stackGraph.credits = credits;

            $http({
                method: 'get',
                url: restUrl + "/diabetes/analytics/avgPatientsEnrolled/",
                headers: {
                    "CONSUMER_KEY": CONSUMER_KEY,
                    "content-type": "application/json"
                }
            }).then(function (response) {
                vm.avgPatients = response.data;
                vm.avgPatients = parseInt(vm.avgPatients);
            })
        });

    }

    function getNephroPieData(value) {
        var doctorId = LocalStore.fetch('USER_INFO').id;
        vm.pieChart={};
        var prefixParamMap = {
            'diabetes': {
                prefix: 'graph_report_count',
                params : "DOCTOR_ID~" + doctorId + "^REPORT_TYPE~sugar_profile_report_type"
            },
            'cvd': {
                prefix: 'graph_report_count',
                params: "DOCTOR_ID~" + doctorId + "^REPORT_TYPE~cvd_report_type"
            },
            'nephro': {
                prefix: 'graph_report_count',
                params: "DOCTOR_ID~" + doctorId + "^REPORT_TYPE~nephro_report_type"
            },
            'obesity':{
                prefix: 'obesity_graph_count',
                params: "DOC_ID~" + doctorId
            }
        };
        var prefix = prefixParamMap[value].prefix;
        var params = prefixParamMap[value].params;
        console.log(prefix, params);
        $http({
                method: 'get',
                url: restUrl + "/core/framework/"+prefix+"/find?searchParams="+params+"&context=sysadmin",
                headers: {
                    "CONSUMER_KEY": CONSUMER_KEY,
                    "content-type": "application/json"
                }
            })
            .then(function (response) {
              if (response) {
                vm.pieChart[value]=response.data;
                vm.count =[];
               for(var i in vm.pieChart[value]){
                    vm.count.push(vm.pieChart[value][i].count);
                    var chart = {
                            type: 'pie',
                            options3d: {
                              enabled: true,
                              alpha: 45,
                              beta: 0
                            }
                          }
                          var options = {
                            exporting: {
                              enabled: false
                            }
                          }
                          var credits = {
                            enabled: false
                          }
                          var title = {
                            text: ''
                          };
                          var plotOptions = {
                            pie: {
                              allowPointSelect: false,
                              cursor: 'pointer',
                              dataLabels: {
                                enabled: true
                              },
                              showInLegend: true
                            }
                          };
                          var series = [{
                            type: 'pie',
                            data: [{
                              name: 'Beyond-Target',
                              y: vm.count[0]
                            }, {
                              name: 'Within-Target',
                              y: vm.count[1]
                            }],
                            colors: ['#FF5E5B', '#D7B83B']
                          }];
                          vm.pieChart1 = {};
                          vm.pieChart1.chart = chart;
                          vm.pieChart1.title = title;
                          vm.pieChart1.series = series;
                          vm.pieChart1.plotOptions = plotOptions;
                          vm.pieChart1.options = options
                          vm.pieChart1.credits = credits
                }
                console.log(vm.count);
                console.log(vm.pieChart);


              }
            }, function (error) {
              throw response.data.message;

            });
    }
    function goToPatientDetails(patInfo){
        console.log(patInfo);
        LocalStore.store('CURRENT_PATIENT',patInfo);
         $state.go('sysadmin.patDetails');
    }


        vm.getDoctorInfo = getDoctorInfo;

            function getDoctorInfo(){

                var doctorList = LocalStore.fetch("Doctor_List_Records");
                console.log(doctorList);
                if(doctorList==null){
                    console.log(vm.doctorList);
                    alert("Please add atleast one doctor in list!");
                    return false;
                }
                else
                {
                    console.log("hello");
                    $state.go('sysadmin.modal')
                }
            }
    vm.detailtotalactiveuser = detailtotalactiveuser;

    function detailtotalactiveuser(){

        console.log("activeuser");

         $state.go('sysadmin.detailtotalactiveuser')
        //var url = '#' + $state.href('sysadmin.detailtotalactiveuser');
        //window.open(url,'_blank');

    }

        vm.docInfo = docInfo;
            function docInfo(){
                console.log("hi");

                var doctorList = LocalStore.fetch("Doctor_List");
                console.log(doctorList);
                if(doctorList==null){
                    alert("Please select atleast one doctor from list");
                    return false;
                }else{
                    $state.go('sysadmin.listmodal');
                }

            }

        vm.questionnaireInfo = questionnaireInfo;
        function questionnaireInfo(){
            var doctorList = LocalStore.fetch("Doctor_List_Question");
            if(doctorList==null){
                alert("Please select at least one doctor from list");
                return false;
            }else{
                $state.go('sysadmin.questionnaire');
            }
        }
    vm.avgTimeSpent = avgTimeSpent;
        function avgTimeSpent(){
            $state.go('sysadmin.appDetail');
        }
    vm.maximumUsage = maximumUsage;
        function maximumUsage(){
            $state.go('sysadmin.maxUsageFunc');
        }
    vm.avgEnrollPatient = avgEnrollPatient;
        function avgEnrollPatient(){
            $state.go('sysadmin.avgenrollpatient')
    }


    /*function doctorModal(){
        console.log("function called");
        console.log(vm.selectedDoctorList);
        $http({
            method:'post',
            url: restUrl + "/diabetes/analytics/averagePatientRecordsDetails/",
            headers:{
                "CONSUMER_KEY": CONSUMER_KEY,
                "content-type": "application/json"
            },
            data:{doctors:vm.selectedDoctorList,
                type:vm.reportType.id,
                duration:vm.filter.id}
            //http://localhost:8080/webapp/rest/v1.0/image/ImageService/select/PATNT000000797d92ab01594509d482008000/SYSLVIMGTP000000000000000000000000001
        }).then(function (response){
            console.log(response);
            vm.doctorsInfo = response.data;
            console.log(vm.doc.getDetailuserInfotorsInfo);

        })
    }*/
  $scope.getDetailuserInfo =function(){
        var modalInstance = $modal.open({
            templateUrl: 'TotalActiveUser.html',
            controller: 'DoctorwiseInfoController',
            size: 'xs',
            backdrop: false,
            backdropClick: false,
            dialogFade: true,
            keyboard: false
        });

    }

    $scope.getDoctorwiseInfo=function(){
        var modalInstance = $modal.open({
            templateUrl: 'DoctorwiseInfo.html',
            controller: 'DoctorwiseInfoController',
            size: 'xs',
            backdrop: false,
            backdropClick: false,
            dialogFade: true,
            keyboard: false,
            /*esolve: {
             input: function(){
             return vm.selectedDoctorList;
             },
             reportType: function(){
             return vm.reportType;
             },
             duration: function(){
             return vm.filter;
             }
             }*/

        });
        console.log('hi');
    }

    $scope.getDoctorwiseRisk=function(){
        var modalInstance = $modal.open({
            templateUrl: 'DoctorwiseRisk.html',
            controller: 'DoctorwiseRiskController',
            size: 'xs',
            backdrop: false,
            backdropClick: false,
            dialogFade: true,
            keyboard: false,
            /*esolve: {
             input: function(){
             return vm.selectedDoctorList;
             },
             reportType: function(){
             return vm.reportType;
             },
             duration: function(){
             return vm.filter;
             }
             }*/

        });
        console.log('hi');
    }
     $scope.getActiveDoctorsInfo=function(){
        var modalInstance = $modal.open({
            templateUrl: 'activeDoctors.html',
            controller: 'activeDoctorController',
            size: 'xs',
            backdrop: false,
            backdropClick: false,
            dialogFade: true,
            keyboard: false,
            /*esolve: {
                input: function(){
                    return vm.selectedDoctorList;
                },
                reportType: function(){
                    return vm.reportType;
                },
                duration: function(){
                    return vm.filter;
                }
            }*/

        });
       console.log('hi');
        }

        $scope.getActivePatientsInfo=function(){
                var modalInstance = $modal.open({
                    templateUrl: 'activePatients.html',
                    controller: 'activePatientsController',
                    size: 'xs',
                    backdrop: false,
                    backdropClick: false,
                    dialogFade: true,
                    keyboard: false,
                    /*resolve: {
                        input: function(){
                            return vm.doctorlist
                        },
                        reportType: function(){
                            return vm.pie;
                        }
                    }*/

                });
               console.log('hello');
                }


         $scope.getPendingRegistInfo=function(){
                        var modalInstance = $modal.open({
                            templateUrl: 'PendingRegist.html',
                            controller: 'PendingRegistController',
                            size: 'xs',
                            backdrop: false,
                            backdropClick: false,
                            dialogFade: true,
                            keyboard: false,
                            /*resolve: {
                                input: function(){
                                    return vm.avgrecord
                                },
                                reportType: function(){
                                    return vm.patientinfo;
                                }
                            }*/

                        });
                       console.log('hello');
                        }
        $scope.sqdtsqInfo=function(){
            var modalInstance = $modal.open({
                templateUrl: 'sqdtsqInfo.html',
                controller: 'sqdtsqInfoController',
                size: 'xs',
                backdrop: false,
                backdropClick: false,
                dialogFade: true,
                keyboard: false,
                /*resolve: {
                 input: function(){
                 return vm.avgrecord
                 },
                 reportType: function(){
                 return vm.patientinfo;
                 }
                 }*/

            });
            console.log('hello');
        }




});
sfe.controller('maxUsageController',function($scope,$http,LocalStore){
    var vm =this;
    vm.maxUsageFunc = maxUsageFunc;
    vm.dur = LocalStore.fetch("todate");
    console.log(vm.dur);
    vm.date = LocalStore.fetch("fromdate");
    console.log(vm.date);
    vm.app = LocalStore.fetch("appId");
    console.log(vm.app);
    function maxUsageFunc(){
        $http({
            method: 'get',
            url: restUrl + "/diabetes/analytics/maxUsageViewDetails/" +vm.app.id +'/' + vm.date + '/' + vm.dur,
            headers: {
                "CONSUMER_KEY": CONSUMER_KEY,
                "content-type": "application/json"
            }
        }).then(function (response) {
            console.log(response);
            vm.appDetail = response.data;
            console.log(vm.appDetail);
        })
    }
});
sfe.controller('appDetailModalController',function($scope,$http,LocalStore) {
    var vm = this;
    vm.maxFunctionality = maxFunctionality;
    vm.dur = LocalStore.fetch("todate");
    console.log(vm.dur);
    vm.date = LocalStore.fetch("fromdate");
    console.log(vm.date);
    vm.downloadavgTimeSpentApp = downloadavgTimeSpentApp;

    function downloadavgTimeSpentApp(fromDate,toDate) {

        window.open("/webapp/report/patient-records?certificate=" +  encodeURIComponent(LocalStore.fetch('USER_INFO').certificate) +"&fromDate="+vm.date+"&toDate="+ vm.dur);

    }
    function maxFunctionality() {
    $http({
        method: 'get',
        url: restUrl + "/diabetes/analytics/averagePageViewDetails/" + vm.date + '/' + vm.dur,
        headers: {
            "CONSUMER_KEY": CONSUMER_KEY,
            "content-type": "application/json"
        }
    }).then(function (response) {
        console.log(response);
        vm.maxValue = response.data;
        console.log(vm.maxValue);
    })
}
});
sfe.controller('avgenrollpatientController',function($scope,$http){
    var vm = this;
    $http({
        method:'get',
        url: restUrl + "/diabetes/PatientService/getAllDoctors/",
        headers:{
            "CONSUMER_KEY": CONSUMER_KEY,
            "content-type": "application/json"
        }
        //http://localhost:8080/webapp/rest/v1.0/image/ImageService/select/PATNT000000797d92ab01594509d482008000/SYSLVIMGTP000000000000000000000000001
    }).then(function (response) {
        console.log(response);
        vm.doctors = response.data;

        vm.selectedDoctorList = [];

        console.log(vm.doctors);
        for (var i in vm.doctors) {

            vm.selectedDoctorList.push(vm.doctors[i].id);

        }
        //vm.pie.id = "sugar_profile_report_type";
        //getPieData();

        //getPatientData(vm.selectedDoctorList, vm.toDate, vm.fromDate);
        console.log(vm.selectedDoctorList);
    })
    vm.doctordetail = doctordetail;
    function doctordetail(){
        console.log(vm.selectedDoctorRisk);
        vm.selectedDoctorList = [];
        for(var i in vm.selectedDoctorRisk){
            vm.selectedDoctorList.push(vm.selectedDoctorRisk[i].id)
        }

        //LocalStore.store("Doctor_List_Records",vm.selectedDoctorList);
        //LocalStore.store("report",vm.reportType);
        //LocalStore.store("duration",vm.toDate);
        //LocalStore.store("date",vm.fromDate);

        console.log(vm.selectedDoctorList);
    }
    vm.getAllPatient = getAllPatient;
    function getAllPatient(){
    $http({
        method:'get',
        url:restUrl + "/diabetes/PatientService/getAllPatientsList/" + vm.selectedDoctorList,
        headers:{
            "CONSUMER_KEY": CONSUMER_KEY,
            "content-type": "application/json"
        }
    }).then(function (response) {
        console.log(response);
        vm.patientList = response.data;
        console.log(vm.patientList);
    })
    }
});
sfe.controller('sqdtsqInfoController',function($scope,$modalInstance){
    console.log('hello');
    $scope.sqdtsqInfo = function(){
        console.log('init');
    }
    $scope.closeModal = function(){
        $modalInstance.dismiss('cancel');
    };
});
sfe.controller('DoctorwiseRiskController',function($scope,$modalInstance){
    console.log('hello');
    $scope.DoctorwiseRisk = function(){
        console.log('init');
    }
    $scope.closeModal = function(){
        $modalInstance.dismiss('cancel');
    };
});
sfe.controller('DoctorwiseInfoController',function($scope,$modalInstance){
    console.log('hello');
    $scope.DoctorwiseInfo = function(){
        console.log('init');
    }
    $scope.closeModal = function(){
        $modalInstance.dismiss('cancel');
    };
});
sfe.controller('activeDoctorController',function($scope,$modalInstance){
    console.log('hello');
    $scope.activeDoctors = function(){
console.log('init');
    }
    $scope.closeModal = function(){
     $modalInstance.dismiss('cancel');
     };
});
sfe.controller('activePatientsController',function($scope,$modalInstance){
    console.log('hello');
    $scope.activePatients = function(){
        console.log('init');
    }
    $scope.closeModal = function(){
        $modalInstance.dismiss('cancel');
    };
});
sfe.controller('PendingRegistController',function($scope,$modalInstance){
    console.log('hello');
    $scope.PendingRegist = function(){
        console.log('init');
    }
    $scope.closeModal = function(){
        $modalInstance.dismiss('cancel');
    };
});
sfe.controller('modalController',function($scope, $http,LocalStore){
    console.log('docmodal');
    var vm = this;
       vm.doctorModal = doctorModal;
       //vm.doctorlist = input;
       //vm.reportType = reportType;
       //vm.duration = duration;

        console.log("modal opened");
    vm.doc = LocalStore.fetch('Doctor_List_Records');
    console.log(vm.doc);
    vm.re =LocalStore.fetch('report');
    console.log(vm.re);
    vm.dur = LocalStore.fetch('duration');
    console.log(vm.dur);
    vm.date = LocalStore.fetch('date');
    console.log(vm.date);
    vm.filter = LocalStore.fetch('filter');
    console.log(vm.filter);
    vm.downloadPatientReport = downloadPatientReport;




    function downloadPatientReport(type,fromDate,toDate) {

        var emptylist = LocalStore.fetch('Doctor_List_Records');
        console.log(emptylist);
        if (emptylist.length == 0 && vm.doctorsInfo.length == 0)  {
            alert("There is no doctor selected to download");
            return false;
        }
        else{
            console.log('$$$$$$$$$$');
            window.open("/webapp/report/patient-records?certificate=" + encodeURIComponent(LocalStore.fetch('USER_INFO').certificate) + "&type=" + type.id + "&fromDate=" + vm.date + "&toDate=" + vm.dur);
        }
    }
        function doctorModal(){
        console.log("function called");
               $http({
                          method:'post',
                          url: restUrl + "/diabetes/analytics/averagePatientRecordsDetails/",
                          headers:{
                              "CONSUMER_KEY": CONSUMER_KEY,
                              "content-type": "application/json"
                          },
                           data:{doctors:vm.doc,
                           type:vm.re.id,
                           timeSpan:vm.filter,
                               toDate:vm.dur,
                               fromDate:vm.date
                               }
                          //http://localhost:8080/webapp/rest/v1.0/image/ImageService/select/PATNT000000797d92ab01594509d482008000/SYSLVIMGTP000000000000000000000000001
                      }).then(function (response){
                          console.log(response);
                          vm.doctorsInfo = response.data;

                    console.log(vm.doctorsInfo);

        })
}
 /*$scope.closeModal = function(){
   $modalInstance.dismiss('cancel');
   };*/
   })
sfe.controller('TotalActiveUserController',function($scope,$http,LocalStore){
    console.log('hello');
    var vm = this;
    vm.dur = LocalStore.fetch("todate");
    console.log(vm.dur);
    vm.date = LocalStore.fetch("fromdate");
    console.log(vm.date);
    $scope.TotalActiveUser = function(){
        console.log('init');
    }
    vm.fetchDetailUsers = fetchDetailUsers;
    vm.ActiveUser=[{id:"doctor",name:"Doctor"},{id:"patient",name:"Patient"}];
    vm.TotalActiveUser={id:"doctor"};
    console.log(vm.TotalActiveUser);
    console.log(vm.ActiveUser);
    vm.showUsers = showUsers;

    function  showUsers() {


    }


    function fetchDetailUsers() {
        $http({
            method:'get',
            url: restUrl + "/diabetes/analytics/activeUsersDetails/" + vm.date +'/'+vm.dur,
            headers:{
                "CONSUMER_KEY": CONSUMER_KEY,
                "content-type": "application/json"
            }
            //http://localhost:8080/webapp/rest/v1.0/image/ImageService/select/PATNT000000797d92ab01594509d482008000/SYSLVIMGTP000000000000000000000000001
        }).then(function (response){
            console.log(response);
            vm.detailUsers = response.data;
            console.log(vm.detailUsers);
        })
    }
    /*console.log('docmodal');
    var vm = this;
    vm.detaildoctorsInfo = detaildoctorsInfo
    //vm.doctorlist = input;
    //vm.reportType = reportType;
    //vm.duration = duration;


    function detaildoctorsInfo(){
        vm.toDate = new Date();

        vm.toDate = moment(vm.toDate).format('YYYY-MM-DD');
        console.log(vm.toDate);

        vm.fromDate = new Date();
        vm.fromDate.setMonth(vm.fromDate.getMonth()-3);
        vm.fromDate = moment(vm.fromDate).format('YYYY-MM-DD');
        console.log(vm.fromDate);
        console.log("function called");
        $http({
            method:'get',
            url: restUrl + "/diabetes/analytics/activeUsersDetails/"+ vm.fromDate+'/'+ vm.toDate,
            headers:{
                "CONSUMER_KEY": CONSUMER_KEY,
                "content-type": "application/json"
            },
            data:{

                //duration:vm.dur.id
                toDate:vm.toDate,
                fromDate:vm.fromDate
            }
            //http://localhost:8080/webapp/rest/v1.0/image/ImageService/select/PATNT000000797d92ab01594509d482008000/SYSLVIMGTP000000000000000000000000001
        }).then(function (response){
            console.log(response);
            vm.doctorsInfo = response.data;
            console.log(vm.doctorsInfo);

        })
    }
    /*$scope.closeModal = function(){
     $modalInstance.dismiss('cancel');
     };*/
})

sfe.controller('doctorListModalController',function($scope,$http,LocalStore){
        var vm = this;
         //vm.doctorlist = input;
               //vm.reportType = reportType;

               //console.log(vm.reportType);
               //console.log(vm.doctorlist);

                console.log("modal is here");
    vm.doc = LocalStore.fetch('Doctor_List');
    console.log(vm.doc);
    vm.test = LocalStore.fetch('report');
    console.log(vm.test);
    vm.re =LocalStore.fetch('riskwisereport');
    console.log(vm.re);
    vm.downloadDetails = downloadDetails;
    function downloadDetails(type) {
        window.open("/webapp/report/riskwise-patient?certificate=" +  encodeURIComponent(LocalStore.fetch('USER_INFO').certificate)  + "&type="+ type.id);
    }

                vm.doctorListModal = doctorListModal;
            function doctorListModal(){
                  $http({
                                          method:'post',
                                          url: restUrl + "/diabetes/analytics/doctorwisePatientDetails/",
                                          headers:{
                                              "CONSUMER_KEY": CONSUMER_KEY,
                                              "content-type": "application/json"
                                          },
                                           data:{doctors:vm.doc,
                                           type:vm.re.id,
                                          } ,

                                          //http://localhost:8080/webapp/rest/v1.0/image/ImageService/select/PATNT000000797d92ab01594509d482008000/SYSLVIMGTP000000000000000000000000001
                                      }).then(function (response){

                                          console.log(response);
                                          vm.doctorsList = response.data;
                                          console.log(vm.doctorsList);
                                          console.log('hi');

                        })
            }
          /*$scope.closeModal = function(){
             $modalInstance.dismiss('cancel');
             };*/


})


sfe.controller('questionnaireModalController',function($scope,$http,LocalStore){
        var vm = this;
         //vm.doctorlist = input;
               //vm.reportType = reportType;

               //console.log(vm.reportType);
               //console.log(vm.doctorlist);
    vm.doc = LocalStore.fetch('Doctor_List_Question');
    console.log(vm.doc);
    vm.re =LocalStore.fetch('report');
    console.log(vm.re);
    vm.user= LocalStore.fetch('USER_INFO');
    console.log(vm.user);
                console.log("modal is here");
    vm.downloadQuestionnaireReport = downloadQuestionnaireReport;


    function downloadQuestionnaireReport(type) {
        window.open("/webapp/report/questionnaire-report?certificate=" +  encodeURIComponent(vm.user.certificate)  + "&type="+ type.id);
        console.log("hi");
        console.log(type);
    }

    vm.questionnaireListModal = questionnaireListModal;
                function questionnaireListModal(){
                  $http({
                                          method:'post',
                                          url: restUrl + "/diabetes/analytics/questinnaireDetails/",
                                          headers:{
                                              "CONSUMER_KEY": CONSUMER_KEY,
                                              "content-type": "application/json"
                                          },
                                           data:{doctors:vm.doc,
                                           type:vm.re.id,
                                          }
                                          //http://localhost:8080/webapp/rest/v1.0/image/ImageService/select/PATNT000000797d92ab01594509d482008000/SYSLVIMGTP000000000000000000000000001
                                      }).then(function (response){
                                          console.log(response);
                                          vm.questionnaireList = response.data;
                                          console.log(vm.questionnaireList);

                        })
            }


})


sfe.controller('PatientsController', function($http,$state,LocalStore){
    var vm=this;
    vm.patientDetails = patientDetails;
    vm.openRecords = openRecords;
    vm.loadPatientRecords = loadPatientRecords;
    vm.hideshowDiabetes = false
    vm.hideshownepro = false
    vm.hideshowcvd = false
    vm.hideshownepro = false
    function patientDetails(){
        vm.currentPatient = LocalStore.fetch('CURRENT_PATIENT');
        console.log(vm.currentPatient);
    }
    function openRecords(){
    vm.currentPatient = LocalStore.fetch('CURRENT_PATIENT');
          $state.go('sysadmin.records', {
            id: vm.currentPatient.id
          })
    }
    function loadPatientRecords(value){
     if (value == "diabetes") {
        vm.hideshowDiabetes = true
        vm.hideshownepro = false
        vm.hideshowcvd = false
        vm.hideshownepro = false
      } else if (value == "cvd") {
        vm.hideshowDiabetes = false
        vm.hideshownepro = false
        vm.hideshowcvd = true
        vm.hideshownepro = false
      } else if (value == "nephro") {
        vm.hideshowDiabetes = false
        vm.hideshownepro = false
        vm.hideshowcvd = true
        vm.hideshownepro = false
      } else if (value == "other") {
        vm.hideshowDiabetes = false
        vm.hideshownepro = false
        vm.hideshowcvd = false
        vm.hideshownepro = true
      }
      var patientId = $state.params.id;
        if (value == 'diabetes') {
          var params = "PATIENT_ID~" + patientId + "^REPORT_TYPE~sugar_profile_report_type";
        }
        else if (value == 'cvd') {
          var params = "PATIENT_ID~" + patientId + "^REPORT_TYPE~cvd_report_type";
        }
        else if (value == 'nephro') {
          var params = "PATIENT_ID~" + patientId + "^REPORT_TYPE~nephro_report_type";
        }
        else {
          var params = "PATIENT_ID~" + patientId + "^REPORT_TYPE~nephro_report_type";
        }
        var prefix = "patients_reports";
        $http({
            method: 'get',
            url: restUrl + "/core/framework/"+prefix+"/find?searchParams="+params+"&context=sysadmin",
            headers: {
                "CONSUMER_KEY": CONSUMER_KEY,
                "content-type": "application/json"
            }
        })
        .then(function (response) {
            //var responseData = response;
                  //console.log(sessionConstant.PatientRecords)
                  var responseData = response.data;
                  console.log(responseData)
                  var reportType = {
                    LDL: [],
                    fastingsugar: [],
                    postmeal: [],
                    randomSugar: [],
                  }
                  var Triglycerides = {
                    dates: [],
                    value: [],
                    thread: [],
                  }
                  var Cholesterol = {
                    dates: [],
                    value: [],
                    thread: [],
                  }
                  var HDL = {
                    dates: [],
                    value: [],
                    thread: [],
                  }
                  var Serum = {
                    dates: [],
                    value: [],
                    thread: [],
                  }
                  var hba1 = {
                    dates: [],
                    value: [],
                    thread: [],
                  }
                  var fastingSugar = {
                    dates: [],
                    value: [],
                    thread: [],
                  }
                  var vldl = {
                    dates: [],
                    value: [],
                    thread: [],
                  }
                  var hba1c = {
                    dates: [],
                    value: [],
                    thread: [],
                  };
                  var fastingsugar = {
                    dates: [],
                    value: [],
                    thread: [],
                  };
                  var postmeal = {
                    dates: [],
                    value: [],
                    thread: [],
                  };
                  var randomSugar = {
                    dates: [],
                    value: [],
                    thread: [],
                  };
                  var GFR = {
                    dates: [],
                    value: [],
                    thread: [],
                  };
                  var UrineAlbumin = {
                    dates: [],
                    value: [],
                    thread: [],
                  };
                  var BloodUrea = {
                    dates: [],
                    value: [],
                    thread: [],
                  }
                  var BloodPressure = {
                    dates: [],
                    value: [],
                    thread: [],
                  }
                  var LDL = {
                    dates: [],
                    value: [],
                    thread: [],
                  }

                  var reportType = {
                    LDL: [],
                    vldl: [],
                    Triglycerides: [],
                    HDL: [],
                    Cholesterol: [],
                    Serum: [],
                    hba1c: [],
                    fastingsugar: [],
                    postmeal: [],
                    randomSugar: [],
                    GFR: [],
                    UrineAlbumin: [],
                    BloodUrea: [],
                    BloodPressure: [],
                  }


                  for (var i in responseData) {
                    console.log(responseData[i].REPORT_TYPE_ID)
                    if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000001") {
                      reportType.hba1c.push(responseData[i]);
                    } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000002") {
                      reportType.fastingsugar.push(responseData[i])
                    } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000003") {
                      reportType.postmeal.push(responseData[i])
                    } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000004") {
                      reportType.randomSugar.push(responseData[i])
                    }
                    else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000005") {
                      console.log("Blood-Preaseure----USRLVRT000000000000000000000000000005")
                      reportType.BloodPressure.push(responseData[i])
                    } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000006") {
                      console.log("USRLVRT000000000000000000000000000006----autocalc---nograh")
                    } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000007") {
                      reportType.LDL.push(responseData[i])
                      console.log("USRLVRT000000000000000000000000000006")
                    } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000008") {
                      console.log("VLDL------------USRLVRT000000000000000000000000000008")
                      reportType.vldl.push(responseData[i])
                    } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000009") {
                      reportType.Triglycerides.push(responseData[i])
                    } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000010") {
                      console.log("HDL---USRLVRT000000000000000000000000000010")
                      reportType.HDL.push(responseData[i])
                    } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000011") {
                      console.log("Cholesterol Ratio---USRLVRT000000000000000000000000000011")
                      reportType.Cholesterol.push(responseData[i])
                    } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000012") {
                      reportType.Serum.push(responseData[i])
                      console.log("USRLVRT000000000000000000000000000006---nepro")
                    } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000013") {
                      reportType.BloodUrea.push(responseData[i])
                      console.log("USRLVRT000000000000000000000000000006----------nepro")
                    } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000014---nepro") {
                      reportType.UrineAlbumin.push(responseData[i])
                    } else if (responseData[i].REPORT_TYPE_ID == "USRLVRT000000000000000000000000000015----nerp---nograph-autocalc") {
                      reportType.GFR.push(responseData[i])
                    }
                  }
                  /*sessionConstant.reportType.hba1c = reportType.hba1c
                  sessionConstant.reportType.fastingsugar = reportType.fastingsugar
                  sessionConstant.reportType.postmeal = reportType.postmeal
                  sessionConstant.reportType.GFR = reportType.GFR
                  sessionConstant.reportType.UrineAlbumin = reportType.UrineAlbumin
                  sessionConstant.reportType.BloodUrea = reportType.BloodUrea
                  sessionConstant.reportType.Serum = reportType.Serum
                  sessionConstant.reportType.Cholesterol = reportType.Cholesterol
                  sessionConstant.reportType.HDL = reportType.HDL
                  sessionConstant.reportType.Triglycerides = reportType.Triglycerides
                  sessionConstant.reportType.vldl = reportType.vldl
                  sessionConstant.reportType.LDL = reportType.LDL
                  sessionConstant.reportType.BloodPressure = reportType.BloodPressure
                  sessionConstant.reportType.randomSugar = reportType.randomSugar*/


                  for (var j in reportType.hba1c) {
                    hba1.dates.push(reportType.hba1c[j].DATE_RECORDED.text)
                    hba1.value.push(parseInt(reportType.hba1c[j].REPORT_VALUE))
                    hba1.thread.push(reportType.hba1c[j].THRESHOLD_VALUE)
                  }

                  for (var j in reportType.fastingsugar) {
                    fastingSugar.dates.push(reportType.fastingsugar[j].DATE_RECORDED.text)
                    fastingSugar.value.push(parseInt(reportType.fastingsugar[j].REPORT_VALUE))
                    fastingSugar.thread.push(reportType.fastingsugar[j].THRESHOLD_VALUE)
                  }

                  for (var j in reportType.randomSugar) {
                    randomSugar.dates.push(reportType.randomSugar[j].DATE_RECORDED.text)
                    randomSugar.value.push(parseInt(reportType.randomSugar[j].REPORT_VALUE))
                    randomSugar.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
                  }

                  for (var j in reportType.BloodPressure) {
                    BloodPressure.dates.push(reportType.randomSugar[j].DATE_RECORDED.text)
                    BloodPressure.value.push(parseInt(reportType.randomSugar[j].REPORT_VALUE))
                    BloodPressure.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
                  }

                  for (var j in reportType.BloodUrea) {
                    BloodUrea.dates.push(reportType.randomSugar[j].DATE_RECORDED.text)
                    BloodUrea.value.push(parseInt(reportType.randomSugar[j].REPORT_VALUE))
                    BloodUrea.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
                  }

                  for (var j in reportType.Cholesterol) {
                    Cholesterol.dates.push(reportType.randomSugar[j].DATE_RECORDED.text)
                    Cholesterol.value.push(parseInt(reportType.randomSugar[j].REPORT_VALUE))
                    Cholesterol.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
                  }

                  for (var j in reportType.GFR) {
                    GFR.dates.push(reportType.randomSugar[j].DATE_RECORDED.text)
                    GFR.value.push(parseInt(reportType.randomSugar[j].REPORT_VALUE))
                    GFR.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
                  }

                  for (var j in reportType.HDL) {
                    HDL.dates.push(reportType.randomSugar[j].DATE_RECORDED.text)
                    HDL.value.push(parseInt(reportType.randomSugar[j].REPORT_VALUE))
                    HDL.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
                  }

                  for (var j in reportType.LDL) {
                    LDL.dates.push(reportType.randomSugar[j].DATE_RECORDED.text)
                    LDL.value.push(parseInt(reportType.randomSugar[j].REPORT_VALUE))
                    LDL.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
                  }

                  for (var j in reportType.postmeal) {
                    postmeal.dates.push(reportType.postmeal[j].DATE_RECORDED.text)
                    postmeal.value.push(parseInt(reportType.postmeal[j].REPORT_VALUE))
                    postmeal.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
                  }
                  for (var j in reportType.Serum) {
                    Serum.dates.push(reportType.randomSugar[j].DATE_RECORDED.text)
                    Serum.value.push(parseInt(reportType.randomSugar[j].REPORT_VALUE))
                    Serum.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
                  }
                  for (var j in reportType.Triglycerides) {
                    Triglycerides.dates.push(reportType.randomSugar[j].DATE_RECORDED.text)
                    Triglycerides.value.push(parseInt(reportType.randomSugar[j].REPORT_VALUE))
                    Triglycerides.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
                  }
                  for (var j in reportType.UrineAlbumin) {
                    UrineAlbumin.dates.push(reportType.randomSugar[j].DATE_RECORDED.text)
                    UrineAlbumin.value.push(parseInt(reportType.randomSugar[j].REPORT_VALUE))
                    UrineAlbumin.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
                  }
                  for (var j in reportType.vldl) {
                    vldl.dates.push(reportType.randomSugar[j].DATE_RECORDED.text)
                    vldl.value.push(parseInt(reportType.randomSugar[j].REPORT_VALUE))
                    vldl.thread.push(reportType.randomSugar[j].THRESHOLD_VALUE)
                  }
                  for (var j in reportType.postmeal) {
                    postmeal.dates.push(reportType.postmeal[j].DATE_RECORDED.text)
                    postmeal.value.push(parseInt(reportType.postmeal[j].REPORT_VALUE))
                    postmeal.thread.push(reportType.postmeal[j].THRESHOLD_VALUE)
                  }
                  vm.hba1c = {
                    series: [{
                      type: "line",
                      name: "Thread-Should",
                      data: hba1.thread
                    }, {
                      type: "area",
                      name: "Values",
                      data: hba1.value
                    }],
                    title: {
                      text: 'HBA1'
                    },
                    xAxis: {
                      categories: hba1.dates
                    },
                    options: {
                      exporting: {
                        enabled: false
                      }
                    },
                    credits: {
                      enabled: false
                    },
                  }

                  vm.FastingSugar = {
                    series: [{
                      type: "line",
                      name: "Thread-Should",
                      data: fastingSugar.thread
                    }, {
                      type: "area",
                      name: "Values",
                      data: fastingSugar.value
                    }],
                    title: {
                      text: 'Fasting Sugar'
                    },
                    xAxis: {
                      categories: hba1.dates
                    },
                    options: {
                      exporting: {
                        enabled: false
                      }
                    },
                    credits: {
                      enabled: false
                    },
                  }

                  vm.PostMeal = {
                    series: [{
                      type: "line",
                      name: "Thread-Should",
                      data: postmeal.thread
                    }, {
                      type: "area",
                      name: "Values",
                      data: postmeal.value
                    }],
                    title: {
                      text: 'Post Meal'
                    },
                    xAxis: {
                      categories: hba1.dates
                    },
                    options: {
                      exporting: {
                        enabled: false
                      }
                    },
                    credits: {
                      enabled: false
                    },
                  }

                  vm.RandomSugar = {
                    series: [{
                      type: "line",
                      name: "Thread-Should",
                      data: randomSugar.thread
                    }, {
                      type: "area",
                      name: "Values",
                      data: randomSugar.value
                    }],
                    title: {
                      text: 'Random Sugar'
                    },
                    xAxis: {
                      categories: hba1.dates
                    },
                    options: {
                      exporting: {
                        enabled: false
                      }
                    },
                    credits: {
                      enabled: false
                    },
                  }

                  vm.bloodPreasure = {
                    series: [{
                      type: "line",
                      name: "Thread-Should",
                      data: randomSugar.thread
                    }, {
                      type: "area",
                      name: "Values",
                      data: randomSugar.value
                    }],
                    title: {
                      text: 'bloodPreasure'
                    },
                    xAxis: {
                      categories: hba1.dates
                    },
                    options: {
                      exporting: {
                        enabled: false
                      }
                    },
                    credits: {
                      enabled: false
                    },
                  }

                  vm.VLDL = {
                    series: [{
                      type: "line",
                      name: "Thread-Should",
                      data: randomSugar.thread
                    }, {
                      type: "area",
                      name: "Values",
                      data: randomSugar.value
                    }],
                    title: {
                      text: 'VLDL'
                    },
                    xAxis: {
                      categories: hba1.dates
                    },
                    options: {
                      exporting: {
                        enabled: false
                      }
                    },
                    credits: {
                      enabled: false
                    },
                  }

                  vm.Triglycerides = {
                    series: [{
                      type: "line",
                      name: "Thread-Should",
                      data: randomSugar.thread
                    }, {
                      type: "area",
                      name: "Values",
                      data: randomSugar.value
                    }],
                    title: {
                      text: 'Triglycerides'
                    },
                    xAxis: {
                      categories: hba1.dates
                    },
                    options: {
                      exporting: {
                        enabled: false
                      }
                    },
                    credits: {
                      enabled: false
                    },
                  }

                  vm.HDL = {
                    series: [{
                      type: "line",
                      name: "Thread-Should",
                      data: randomSugar.thread
                    }, {
                      type: "area",
                      name: "Values",
                      data: randomSugar.value
                    }],
                    title: {
                      text: 'HDL'
                    },
                    xAxis: {
                      categories: hba1.dates
                    },
                    options: {
                      exporting: {
                        enabled: false
                      }
                    },
                    credits: {
                      enabled: false
                    },
                  }

                  vm.Cholesterol = {
                    series: [{
                      type: "line",
                      name: "Thread-Should",
                      data: randomSugar.thread
                    }, {
                      type: "area",
                      name: "Values",
                      data: randomSugar.value
                    }],
                    title: {
                      text: 'Cholesterol'
                    },
                    xAxis: {
                      categories: hba1.dates
                    },
                    options: {
                      exporting: {
                        enabled: false
                      }
                    },
                    credits: {
                      enabled: false
                    },
                  }

                  vm.Serum = {
                    series: [{
                      type: "line",
                      name: "Thread-Should",
                      data: randomSugar.thread
                    }, {
                      type: "area",
                      name: "Values",
                      data: randomSugar.value
                    }],
                    title: {
                      text: 'Serum'
                    },
                    xAxis: {
                      categories: hba1.dates
                    },
                    options: {
                      exporting: {
                        enabled: false
                      }
                    },
                    credits: {
                      enabled: false
                    },
                  }

        })

    }



});




