sfe.service('LocalStore',function(localStorageService){
    return{
        store: function(key, value){
            //$cookieStore.put(key, value);
            localStorageService.set(key,value);
        },
        fetch: function(key){
            //return $cookieStore.get(key);
            return localStorageService.get(key);
        },
        remove: function(key){
            //$cookieStore.remove(key);
            localStorageService.remove(key);
        },
        removeAll : function(){
            /*angular.forEach($cookies, function (v, k) {
                //$cookieStore.remove(k);

            });*/
            localStorageService.clearAll();
        }
    };
});

sfe.service('LabelService',function($http, $rootScope) {
    $rootScope.allLabels = null;
    var promise = $http.get('js/labels.json?d=1')
        .success(function (data) {
            $rootScope.allLabels = data;
        });

    return {
        promise:promise,
        getLabels: function (pageId) {
            return $rootScope.allLabels[pageId];
        }
    };

});


sfe.factory('LovService',function($http,LocalStore, $rootScope){

   var userLovs = new Array();
   var getUserLov = function(typeIn, callback){
       var type = typeIn.toUpperCase() + '';
       if(userLovs.length>0){
           for(var i=0;i< userLovs.length;i++){
               console.log(userLovs[i]);
               var obj = userLovs[i];
               if(obj.key == type)
                return obj.value;
           }
           return new Array();
       }


       return $http({
           method: 'GET',
           url: restUrl + "/core/framework/usrlv/find?",
           headers: {
               "content-type": "application/json",
               "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
           }
       }).success(function(data){

           for(var k=0;k< data.length;k++){
               var o = data[k];
               var dtype = o.type.toUpperCase();
               var typeArray = new Array();
               var index = -1;
               for(var i=0;i< userLovs.length;i++){
                   var obj = userLovs[i];
                   if(obj.key == dtype) {
                       typeArray = obj.value;
                       index = i;
                       break;
                   }
               }
               if(index==-1) {
                   if(userLovs.length>0)
                    index = userLovs.length;
                   else
                    index=0;
               }
               typeArray.push(o);
               userLovs[index] = {key: dtype, value: typeArray};

           }

           var values = new Array();
           for(var i=0;i< userLovs.length;i++){
               var obj = userLovs[i];
               if(obj.key == type)
                   values= obj.value;
           }
           callback(values);
       });
   }

    return {getUserLov : getUserLov};
});


sfe.factory('PasswordUpdateService',function($http,LocalStore) {
         return{
                updatePassword: function (passwordObj) {
                    return $http({
                            method: 'PUT',
                             url: restUrl + "/core/changepassword/" ,
                             headers: {
                                 "content-type": "application/json",
                                 "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                             },
                             data: passwordObj
                    });
                }
     };
 });




sfe.service('UIService',function($http, LocalStore) {

    this.configEnabled = function (config, type, user) {
        return $http({
            method: 'GET',
            url: restUrl + "/core/uiservices/" + type + "/" + config + "/" + user,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    };

    this.checkConfigEnabledList = function (config, type, user) {
        return $http({
            method: 'GET',
            url: restUrl + "/core/uiservices/listcheck/" + type + "/" + config + "/" + user,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    };

    this.getConfiguration=function(owner,month,year){
       return $http({
            method: 'GET',
            url: restUrl + "/core/uiservices/configuration/" + owner + "/" + month + "/" + year,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    };

    this.monthlyLock=function(owner,month,year){
        return $http({
            method: 'GET',
            url: restUrl + "/core/uiservices/prvmonthlock/" + month + "/" + year+"/"+owner,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    }

    this.checkLock=function(year,month,list){

        return $http({
            method: 'POST',
            url: restUrl + "/core/uiservices/checklock/" + month + "/" + year,
            data:  list,
            datatype: "json",
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    }
});

sfe.service('RepositoryService',function($http, LocalStore){
    this.restore = function(id){
        return  $http({
            method: 'GET',
            url: restUrl + "/core/framework/" + id,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    };


    this.search = function(entity, params, mode, context , fixedQuery){
        var queryString = "";
        var paged = 'Y';
        var nameValue = new Array();
        if(fixedQuery){
            var fixedParams = fixedQuery.split('^');

            for(var i=0;i< fixedParams.length;i++){
                var nameValuePair = fixedParams[i].split('~');
                if(nameValuePair[1]=='location_id'){
                    params[nameValuePair[0]] = LocalStore.fetch('headerdata').location.id;
                }

		else if(nameValuePair[1]=='user_id'){
                    params[nameValuePair[0]] = LocalStore.fetch('headerdata').user.id;
                }
                else if(nameValuePair[1]=='id'){
                    params[nameValuePair[0]] = LocalStore.fetch('USER_INFO').id;
                }
		else{
                    var pair = fixedParams[i].split(':');
                    params[pair[0]] = pair[1];
                }
            }
        }
        $.each(params, function(name, value){
            if(name!='paged' && value!=null && value!="" && name!='page' && name!='sortBy' && name!='sortOrder') {
                if (queryString != "")
                    queryString = queryString + "^" + name + "~" + value;
                else
                    queryString = name + "~" + value;
            }else{
                nameValue.push(name + "=" + value);
            }
        });
        if(queryString!="")
            queryString = "searchParams=" + queryString;
        queryString = queryString + "&mode=" + mode + "&context=" + ((!context)?"default":context);
        for(var i=0;i< nameValue.length;i++)
            queryString = queryString + "&" + nameValue[i];
        return  $http({
            method: 'GET',
            url: restUrl + "/core/framework/" + entity + "/search?" + queryString,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    };

    this.delete = function(object, id){

        return  $http({
            method: 'DELETE',
            url: restUrl + "/core/framework/" + object + "/" + id,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });

    };

    this.find = function(context, params, mode ){
        var queryString = "";
        var paged = 'Y';
        var sortBy = '';
        var sortOrder = '';
        $.each(params, function(name, value){
            if(name=='paged')
                paged = value;
            else if(name=='sortBy')
                sortBy = value;
            else if(name=='sortOrder')
                sortOrder = value;
            else{
                if (queryString != "")
                    queryString = queryString + "^" + name + "~" + value;
                else
                    queryString = name + "~" + value;
            }
        });
        paged ='N';
        if(queryString!="")
            queryString = "searchParams=" + queryString;
        queryString = queryString + "&mode=" + mode + "&paged=" + paged;
        if(sortBy!='')
            queryString = queryString + "&sortBy=" + sortBy + "&sortOrder=" +  sortOrder;

        return  $http({
            method: 'GET',
            url: restUrl + "/core/framework/" + context + "/find?" + queryString,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    };


    this.save = function(object, context){
        if(!object.id)
            return this.create(object, context);
        else
            return this.update(object, context);
    };

    this.create = function(object, context){
        return $http({
            method:'POST',
            url:restUrl + "/core/framework/" + context + "/",
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data: object
        });
    };


    this.createWithPromise = function(object, context){
        return $http({
            method:'POST',
            url:restUrl + "/core/framework/" + context + "/",
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data: object
        });
    };


    this.update = function(object, context){
        return $http({
            method:'PUT',
            url:restUrl + "/core/framework/" + context + "/",
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            },
            data: object
        });
    };

    this.findSysLov = function(type){
        return  $http({
            method: 'GET',
            url: restUrl + "/core/framework/syslov/" + type,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    };

    this.searchForm = function(context, type){
        return  $http({
            method: 'GET',
            url: restUrl + "/core/framework/searchmeta/" + context + "/" + type,
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    };

    this.getCurrentDate=function(){
        return  $http({
            method: 'GET',
            url: restUrl + "/core/framework/currentDate",
            headers: {
                "content-type": "application/json",
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        });
    }


});

sfe.factory('MenuService', function ($http, $q, LocalStore) {
    return {
        getMenus : function(){
            return  $http({
                method: 'GET',
                url: restUrl + "/core/menus/" + LocalStore.fetch("topcontext"),
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            }).then(function(response){
                if(typeof  response.data == 'object'){
                    return response.data;
                }else{
                    return $q.reject(response.data);
                }
            }, function(response) {
                // something went wrong
                return $q.reject(response.data);
            });
        }
    };
});


sfe.service('fileUpload', function ($http, $q, LocalStore) {
    this.uploadFileToUrl = function(file, path){
        var fd = new FormData();
        fd.append('file', file);
        var url = restUrl+path;
        $http.post(url, fd, {
            transformRequest: angular.identity,
            headers: {
                "Content-Type": undefined,
                "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
            }
        })
            .success(function(){
            })
            .error(function(){
                alert("Failed to upload file.")
            });
    }
});

sfe.service('PatientsService', function ($http, $q, LocalStore) {
     var getPieData = function(doctorId, value) {
              if (value == 'diabetes') {
                var prefix = "graph_report_count";
                var params = "DOCTOR_ID~" + doctorId + "^REPORT_TYPE~sugar_profile_report_type";
              }
              else if (value == 'cvd') {
                var prefix = "graph_report_count";
                var params = "DOCTOR_ID~" + doctorId + "^REPORT_TYPE~cvd_report_type";
              }
              else if (value == 'nephro') {
                var prefix = "graph_report_count";
                var params = "DOCTOR_ID~" + doctorId + "^REPORT_TYPE~nephro_report_type";
              }
              else if (value == 'obesity') {
                var prefix = "obesity_graph_count";
                var params = "DOC_ID~" + doctorId;
              }
              console.log(params);
                $http({
                    method: 'get',
                    url: restUrl + "/core/framework/"+prefix+"/find?searchParams="+params+"&context=sysadmin",
                    headers: {
                        "CONSUMER_KEY": CONSUMER_KEY,
                        "content-type": "application/json"
                    }
                })
                .then(function (response) {
                  if (response) {
                    console.log(response)
                    return response;
                  }
                }, function (error) {
                  throw response.data.message;

                })  }

});


sfe.filter('getDateValue',function(){
    return function(input){
        return moment(input, "DD/MM/YYYY").format("MMM DD, ddd");
    }
});

sfe.filter('capitalize', function() {
    return function(input, all) {
        return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
});