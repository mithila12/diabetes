var restUrl = "rest/v1.0"
var appUrl = "rest/v1.0/core/framework"
var CONSUMER_KEY = "TENAT00000000000000000000000000000001";

var interceptor = function ($q, $location, $rootScope) {
    return {
        request: function (config) {
            return config;
        },

        response: function (result) {
            return result;
        },

        success:function(result){
            return result;
        },
        responseError: function (rejection) {
            $rootScope.showMessage = true;
            $rootScope.message = rejection.data.msg;
            $rootScope.type="danger";
            if (rejection.status == 403) {
                $location.url('/login');
            }

            return $q.reject(rejection);
        }
    }
};


var sfe = angular.module('sfe', ['ui.bootstrap','ui.router', 'ngRoute','ngCookies','ui.grid','dynform',
    'checklist-model','highcharts-ng','blockUI','valdr','isteven-multi-select','textAngular','LocalStorageModule',
    'pascalprecht.translate','mwFormBuilder', 'mwFormUtils',  'monospaced.elastic','mwFormViewer']);


sfe.config(function($httpProvider){
    $httpProvider.interceptors.push(interceptor);

});

sfe.config(function(valdrProvider) {
    valdrProvider.setConstraintUrl(restUrl + "/core/uiservices/constraints?CONSUMER_KEY=" + CONSUMER_KEY);
});


sfe.config(function(blockUIConfig){
    blockUIConfig.delay =1000;
    blockUIConfig.templateUrl = 'templates/component/block-ui-overlay.html';
    blockUIConfig.requestFilter = function(config) {

        var message;

        switch(config.method) {
            case 'GET':
                message = 'Fetching data ...';
                break;

            case 'POST':
            case 'DELETE':
            case 'PUT':
                message = 'Saving your changes ...';
                break;
        };

        return message;

    };

});




sfe.constant('months', ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']);
sfe.constant('monthNameToIndexJS', {'Jan' : 0,'Feb':1,'Mar':2,'Apr': 3,'May': 4,'Jun': 5,'Jul': 6,'Aug' :7,'Sep': 8,'Oct' : 9,'Nov' : 10,'Dec':11});
sfe.constant('monthNameToIndexJava', {'Jan' : 1,'Feb':2,'Mar':3,'Apr': 4,'May': 5,'Jun': 6,'Jul': 7,'Aug' :8,'Sep': 9,'Oct' : 10,'Nov' : 11,'Dec':12});
sfe.constant('daysOfWeek', ['Sun','Mon','Tue','Wed','Thu','Fri','Sat']);
sfe.constant('daysOfWeekToIndex', {'Sun' : 1,'Mon':2,'Tue':3,'Wed':4,'Thu':5,'Fri':6,'Sat':7});