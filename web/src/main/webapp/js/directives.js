sfe.directive('ngConfirmClick', [
    function(){
        return {
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmClick || "Are you sure?";
                var clickAction = attr.confirmedClick;
                element.bind('click',function (event) {
                    if ( window.confirm(msg) ) {
                        scope.$eval(clickAction)
                    }
                });
            }
        };
    }]);


sfe.directive('sfeAddress',
    function(RepositoryService){
        return {
            restrict:'E',
            replace:false,
            transclude:false,
            templateUrl:'templates/layout/address.html',
            controller: function($scope,$timeout){
                $scope.init=function(address) {
                    $scope.$watch('address', function(newValue) {
                        if (newValue !== undefined) {
                            if($scope.address!=null){
                                if($scope.address.state!=null){
                                    $scope.selectcity($scope.address.state.id,false);
                                }
                            }
                        }
                    });
                };
                $scope.cities = [];
                $scope.doctors=[];
                $scope.selectcity=function(id,flag){
                    RepositoryService.find('towns', {"stateId":id},'simple').then(function(data){
                        $scope.cities = data.data;
                    });
                    /*if(flag)
                     {
                     $scope.address.city = $scope.address.city+" - "+$scope.address.zip;
                     }*/
                };

                $scope.setcitynZip=function(city){
                    RepositoryService.restore(city.id).then(
                        function(town){
                            $scope.address.zip = town.data.primaryZip
                        }
                    );
                };
            },
            scope: {
                address:"=address",
                layout:"@",
                states:"=states"
            },
            link: function (scope, element, attr, $scope) {
                if(scope.layout!='row') {
                    scope.labelLayout = "col-xs-7";
                }
            }
        };
    });


sfe.directive('squerDatepicker', function ($parse) {
    return function (scope, element, attrs, controller) {
        var ngModel = $parse(attrs.ngModel);
        $(function(){
            $(element).datepicker({
                inline:true,
                showOn:"both",
                changeYear:true,
                changeMonth:true,
                dateFormat:'dd/mm/yy',
                maxDate: new Date(),
                yearRange: '1900:' + new Date().getFullYear(),
                onSelect:function (dateText, inst) {
                    scope.$apply(function(scope){
                        ngModel.assign(scope, dateText);
                    });
                }
            });
        });

    }
});


sfe.directive('salesDatepicker', function ($parse) {
    return function (scope, element, attrs, controller) {
        var ngModel = $parse(attrs.ngModel);

        var month;
        var year;
        var firstDay;
        var lastDay;
        month = attrs.month;
        year = attrs.year;
        if(attrs.dir=="PM") {
            if (month == 1) {
                month = 12;
                year = year - 1;
            } else {
                month = month - 1
            }
            firstDay = new Date(year, month, 01);
            lastDay = new Date(year, month, 01);
        }
        else{
            firstDay = new Date(year, month-1, 20);
            lastDay = new Date(year, month, 0);
        }

        $(function () {
            $(element).datepicker({
                inline: true,
                showOn: "both",
                changeYear: false,
                changeMonth: false,
                dateFormat: 'dd/mm/yy',
                minDate: firstDay,
                maxDate: lastDay,
                yearRange: year+':' + year,
                onSelect: function (dateText, inst) {
                    scope.$apply(function (scope) {
                        ngModel.assign(scope, dateText);
                    });
                }
            });
        });
    }
});

sfe.directive('squerDatepickerAll', function ($parse) {
    return function (scope, element, attrs, controller) {
        var ngModel = $parse(attrs.ngModel);
        $(function(){
            $(element).datepicker({
                inline:true,
                showOn:"both",
                changeYear:true,
                changeMonth:true,
                dateFormat:'dd/mm/yy',
                yearRange: (new Date().getFullYear()-2)+':' + (new Date().getFullYear()+2),
                onSelect:function (dateText, inst) {
                    scope.$apply(function(scope){
                        ngModel.assign(scope, dateText);
                    });
                }
            });
        });

    }
});

sfe.directive('squerDatepickerTourPlan', function ($parse) {
    return function (scope, element, attrs, controller) {
        var dt;
        if(scope.selectedMonth -1 > new Date().getMonth()){
            dt = new Date(new Date().getFullYear(), scope.selectedMonth-1, 1);
        } else {
            dt = new Date();
        }
        var ngModel = $parse(attrs.ngModel);
        $(function(){
            $(element).datepicker({
                minDate: dt,
                maxDate: new Date(dt.getFullYear(), dt.getMonth() + 1, 0),
                dateFormat:'dd/mm/yy',
                fromMonth: new Date().getMonth(),
                yearRange: '1900:' + new Date().getFullYear(),
                onSelect:function (dateText, inst) {
                    scope.$apply(function(scope){
                        ngModel.assign(scope, dateText);
                    });
                }
            });
        });

    }
});

sfe.directive('squerDatepickerFutureDate', function ($parse) {
    return function (scope, element, attrs, controller) {
        var ngModel = $parse(attrs.ngModel);
        $(function(){
            $(element).datepicker({
                inline:true,
                showOn:"both",
                changeYear:true,
                changeMonth:true,
                dateFormat:'dd/mm/yy',
                //minDate: new Date(),
                onSelect:function (dateText, inst) {
                    scope.$apply(function(scope){
                        ngModel.assign(scope, dateText);
                    });
                }
            });
        });

    }
});

sfe.directive('leftMenu',function(){
    return{
        restrict:'E',
        transclude:false,
        templateUrl:'templates/layout/left-menu.html'
    } ;
});

sfe.directive('topHeader',function(){
    return{
        restrict:'E',
        transclude:false,
        templateUrl:'templates/layout/header.html'
    }
});


sfe.directive('tableLink',function($compile,LabelService){
    return{
        link : function(scope, element){
            var template = null;
            var url =  scope.action.url;
            if(url.indexOf(":")>0){
                var urls = url.split(":");
                url = urls[0];
                var jsonString = "";
                for(var i=1;i<urls.length;i++){
                    var params = urls[i].split('-');
                    jsonString = jsonString + "," + params[0] + ":\"{{d." + params[1] + "}}\"";
                }
                if (scope.action.image == null)
                    template = "<a ui-sref='" + url + "({id: \"{{d.id}}\""  + jsonString + "})'>Edit</a>"
                else {
                    var image = 'images/' + scope.action.image;
                    template = "<a ui-sref='" + url +  "({id: \"{{d.id}}\""  + jsonString + "})'><img src='" + image + "' class='icon' /></a>"
                }
            }else if(url.indexOf("-")>0){
                var urls=url.split("-");
                var type=urls[1].replace("_"," ");
                var image = 'images/' + scope.action.image;
                var message=LabelService.getLabels("commonSearch")[type];
                console.log(type);
                template = "<a confirmed-click=\"confirmBox('{{d.id}}','"+type+"')\" ng-confirm-click=\"" + message + "{{d.name}}?\"><img src='" + image + "'class='icon'/></a>";
            }
            else {
                if (scope.action.image == null)
                    template = "<a ui-sref='{{action.url}}({id: \"{{d.id}}\"})'>Edit</a>"
                else {
                    var image = 'images/' + scope.action.image;
                    template = "<a ui-sref='{{action.url}}({id: \"{{d.id}}\"})'><img src='" + image + "' class='icon' /></a>"
                }
            }
            var linkFn = $compile(template);
            var content = linkFn(scope);
            element.append(content);
        }
    }
});


sfe.directive('activityPopover', function ($compile,$templateCache) {

    var getTemplate = function () {
        var template = $templateCache.get("activity.html");
        return template;
    }
    return {
        restrict: "A",
        link: function (scope, element, attrs) {
            var popOverContent;
            popOverContent = getTemplate();
            var options = {
                content: popOverContent,
                placement: "right",
                html: true,
                date: scope.date
            };
            $(element).popover(options);
        }
    };
});


sfe.directive('sfeClinc',
    function(RepositoryService){
        return {
            restrict:'E',
            replace:false,
            transclude:false,
            templateUrl:'templates/common/clinic.html',
            controller: function($scope){
                $scope.save = function(){
                    RepositoryService.save($scope.clinic,'CLINC');
                };
                $scope.collapsed = $scope.clinic?true: false;
                $scope.isCollapsed = function(clinicId){
                    return $scope.collapsed;
                };

                $scope.showMore = function(clinicId){
                    /*if(!clinicId)
                     return;*/
                    RepositoryService.restore(clinicId).then(function(clinic){
                        $scope.clinic = clinic.data;
                        $scope.collapsed = false;
                    }) ;
                }

                $scope.showLess = function(clinicId){
                    $scope.collapsed = true;
                }

                $scope.showTargets = function(){
                    return $scope.profile.id;
                }
            },
            scope: {
                clinic: "=",
                states:"="
            },
            link: function (scope, element, attr, $scope) {
                $scope.clinic = scope.clinic;
            }
        };
    });

sfe.directive('sfeDivisionProfile',
    function(RepositoryService,LocalStore){
        return {
            restrict:'E',
            replace:false,
            transclude:false,
            templateUrl:'templates/common/divisionprofile.html',
            controller: function($scope){

                $scope.master = {};
                $scope.definitions = [];
                $scope.kolDefLovs = {};
                $scope.usercontext=null;
                $scope.init = function(profile) {
                    $scope.profile = profile;
                    $scope.usercontext = LocalStore.fetch('topcontext');

                    if($scope.usercontext=='sysadmin')
                    {
                        RepositoryService.find("divsn", {}, 'simple').then(function (data) {
                            $scope.master.divisions = data.data;
                        });
                    }
                    RepositoryService.find('koldf',{"status":"active"},'advanced').then(function(data){
                        $scope.definitions = data.data;
                    });

                    if(profile != null) {
                        RepositoryService.find('dockp', {"divisionProfileId": profile.id}, 'advanced').then(function (data) {
                            $scope.profile["doctorKolProfile"] = data.data[0];
                        });
                    }
                }

                $scope.fetchKolLovs = function(type,profile){
                    $scope.profile = profile;
                    RepositoryService.find("usrlv",{type:type},"advanced").then(function(data){
                        $scope.kolDefLovs[type] = (data.data);
                    });
                }

                $scope.selectDivision = function(){
                    RepositoryService.find("locat",{"division_id":$scope.profile.division.id,"showLocations":"Y"},'simple').then(function (data) {
                        $scope.master.locations = data.data;
                    });
                }

                $scope.save = function(){
                    RepositoryService.save($scope.profile,'DOCDP');
                }
                $scope.targetCollapsed = $scope.profile?true: false;
                $scope.isTargetCollapsed = function(){
                    return $scope.targetCollapsed;
                }
                /*
                 $scope.showMore = function(clinicId){
                 if(!clinicId)
                 return;
                 RepositoryService.restore(clinicId).then(function(clinic){
                 $scope.clinic = clinic.data;
                 $scope.collapsed = false;
                 }) ;
                 }

                 $scope.showLess = function(clinicId){
                 $scope.collapsed = true;
                 }
                 */
            },
            scope: {
                profile: "="
            },
            link: function (scope, element, attr, $scope) {
                $scope.profile = scope.profile;
            }
        };
    });

sfe.directive('userLov',
    function(RepositoryService){
        return {
            restrict:'E',
            replace:false,
            transclude:false,
            templateUrl:'templates/widgets/userlov.html',
            scope: {
                type: "@",
                defaultText:"@",
                value:"="
            },
            link: function (scope, element, attr, $scope) {
                RepositoryService.find("usrlv",{"type":scope.type},'simple').then(function(lovs){
                    scope.lovs = lovs.data;
                });
            }
        };
    });

sfe.directive('reportTab',
    function(){
        return{
            restrict:'E',
            replace:false,
            transclude:false,
            templateUrl: 'templates/widgets/reportTab.html',
            scope:{
                reportId: '@'
            }
        }
    });

sfe.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

sfe.directive('uploadForm',
    function(){
        return{
            restrict:'E',
            replace:false,
            transclude:false,
            templateUrl: 'templates/widgets/uploadForm.html',
            scope:{
                prefix: '@',
                uploadPath: '@'
            }
        }
    });


sfe.directive('reportLocationFilter',
    function(){
        return {
            restrict: 'E',
            replace: false,
            transclude: false,
            templateUrl: 'templates/widgets/locationFilter.html',
            scope: {
                division:'=',
                zone : '=',
                region: '=',
                location: '=',
                employeeId : '@'
            }
        }
    }

);


sfe.directive('squerFromMonthYearDatepicker', function ($parse) {
    return function (scope, element, attrs, controller) {
        var ngModel = $parse(attrs.ngModel);
        $(function(){
            $(element).datepicker({
                inline:true,
                changeYear:true,
                changeMonth:true,
                showButtonPanel: true,
                dateFormat:'MM yy',
                yearRange: '1900:' + new Date().getFullYear(),
                onClose: function(dateText, inst) {
                    $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                    scope.$apply(function(scope){
                        dateText = '01/'+(inst.selectedMonth+1)+'/'+inst.selectedYear;
                        ngModel.assign(scope, dateText);
                    });
                }

            });

            $(element).focus(function(){
                $(".ui-datepicker-calendar").hide();
            });
        });

    }
});

sfe.directive('squerToMonthYearDatepicker', function ($parse) {
    return function (scope, element, attrs, controller) {
        var ngModel = $parse(attrs.ngModel);
        $(function(){
            $(element).datepicker({
                inline:true,
                changeYear:true,
                changeMonth:true,
                showButtonPanel: true,
                dateFormat:'MM yy',
                yearRange: '1900:' + new Date().getFullYear(),
                onClose: function(dateText, inst) {
                    $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                    scope.$apply(function(scope){
                        var lastDay = new Date(inst.selectedYear, inst.selectedMonth+1, 0);
                        dateText = lastDay.getDate()+'/'+(inst.selectedMonth+1)+'/'+inst.selectedYear;
                        ngModel.assign(scope, dateText);
                    });
                }

            });

            $(element).focus(function(){
                $(".ui-datepicker-calendar").hide();
            });
        });
    }
});
