
sfe.controller('LoginController', function ($scope, $route, $http, $rootScope,$modal,
                                            $state, LocalStore, LabelService, RepositoryService,$templateCache) {

    $scope.links= LabelService.getLabels('links');
    $scope.labels = LabelService.getLabels("login");
    var currentPassword=null;
    var userId=null;
    $scope.userdata={};
    $scope.login = function($scope) {
       var TZ = "IST";
        if(TZ==="IST" || TZ==="NPT") {
            userId = this.userdata.username;
            currentPassword = this.userdata.password;
            $http({
                method: 'post',
                url: restUrl + "/core/login",
                headers: {
                    "CONSUMER_KEY": CONSUMER_KEY,
                    "content-type": "application/json"
                },
                data: {
                    "username": this.userdata.username,
                    "password": this.userdata.password
                }
            }).then(function (response) {
                var data = response.data;
                LocalStore.store('USER_INFO', data);
                     LocalStore.store('headerdata', data);
                    console.log(data);
                            if(data.id.startsWith('DOCTR')){
                                LocalStore.store('topcontext', 'sysadmin');
                                $state.go('sysadmin.dashboard');
                            }
                            else if(data.id.startsWith('PATNT')){
                                LocalStore.store('topcontext', 'sysadmin');
                                $state.go('sysadmin.patnt');
                            }
                    $rootScope.mainViewStyle = "col-xs-19";
                    $rootScope.leftMenuStyle = "col-xs-3 sidebar";

                });
        }
        else{
            var timezone = $modal.open({
                templateUrl: 'timezone.html',
                controller: 'TimeZoneController',
                size: 'xs',
                backdrop: false,
                backdropClick: false,
                dialogFade: true,
                keyboard: false,
                resolve: {
                    timeZone: function () {
                        return TZ;
                    }
                }
            })
        }
    };
});

sfe.controller('TimeZoneController', function ($scope, $http,LabelService, RepositoryService, LocalStore, $modalInstance,timeZone) {
    $scope.timeZone=timeZone;
    $scope.init=function(){}
    $scope.close=function(){
        $modalInstance.dismiss('cancel');
    }
})

sfe.controller('passwordChangeController',
    function ($scope, $http,LabelService, RepositoryService, LocalStore, $modalInstance,currentPassword,userId) {
        var currentPassword=currentPassword;
        var userId=LocalStore.fetch('headerdata').user.id;
        $scope.errMsg;
        $scope.alertClass;
        $scope.showReturn=false;
        $scope.init = function(){
            $scope.showReturn=false;
        };
        $scope.updatePassword=function(){
            if(currentPassword==$scope.password.newPassword){
                $scope.errMsg="Can not set default password as new password";
                $scope.alertClass="alert alert-danger";
            }else if($scope.password.newPassword!=$scope.password.cnfPassword){
                $scope.errMsg="password must match";
                $scope.alertClass="alert alert-danger";
            }else{
                var newpass={"userId":userId,"currentPassword":currentPassword,"newPassword":$scope.password.newPassword}
                $http({
                    method: 'PUT',
                    url: restUrl + "/core/changepassword",
                    headers: {
                        "CONSUMER_KEY": CONSUMER_KEY,
                        "content-type": "application/json"
                    },
                    data: newpass
                }).success(function (response) {
                    if(response){
                        $scope.errMsg="Password updated successfully.";
                        $scope.alertClass="alert alert-success";
                        $scope.showReturn=true;
                    }
                });
            }
        }

        $scope.close=function(){
            $modalInstance.dismiss('cancel');
        }
    });



sfe.controller('HeaderController', function ($scope,$location, RepositoryService, LocalStore, $state,$modal,$templateCache) {
    $scope.headerData = {};
    $scope.items = [];
    $scope.init = function() {


        if (LocalStore.fetch('USER_INFO').id != null) {
            $scope.headerData = LocalStore.fetch('headerdata');
        } else {
            $scope.headerData = {firstName: 'Admin'};
            LocalStore.store('topcontext', 'sysadmin');
        }
    }

    $scope.logout = function(){
        /*LocalStore.remove('USER_INFO');
         LocalStore.remove('topcontext');*/
        LocalStore.removeAll();
        $templateCache.removeAll();
        window.location = '/webapp/index.jsp';
    }

    $scope.changePassword= function(id,context) {
        if (LocalStore.fetch('USER_INFO').id != null) {
            $state.go('home.password', {"id": id});
        }
        else {
            $state.go('sysadmin.password');
        }
    };

    $scope.viewProfile=function(){
        $state.go('home.employee',{"id":LocalStore.fetch('USER_INFO').id});
    }

    $scope.userLogin=function(){
        var documentInstance = $modal.open({
            templateUrl: 'userLogin.html',
            controller: 'userLoginController',
            size: 'sm',
            backdrop: false,
            backdropClick: false,
            dialogFade: true,
            keyboard: false,
            dialogClass:'myModel'
        })
    };
});


sfe.controller('userLoginController',function($scope,$modalInstance,LabelService,$state,LocalStore,$http,$rootScope,RepositoryService,$stateParams)
{
    $scope.init = function() {};

    $scope.loginAsGuest=function(){
        LocalStore.removeAll();
        $http({
            method: 'GET',
            url: restUrl + "/core/proxyLogin/"+$scope.guest.userId,
            headers: {
                "CONSUMER_KEY": CONSUMER_KEY,
                "content-type": "application/json"
            }
        }).then(function (response) {
            var data = response.data;
            $http({
                method: 'GET',
                url: restUrl + "/sfe/common/preferences/" + data.id,
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": data.certificate
                }
            }).success(function (preferences) {
                data.preferences = preferences;
                LocalStore.store('USER_INFO', data);
                RepositoryService.restore(data.id).success(function (data) {
                    if(data.companydetails!=null){
                        data.location = data.companydetails[0].location;
                    }
                    RepositoryService.find("user_privilege", {userId: data.user.id}, 'advanced')
                        .success(function (privs) {
                            LocalStore.store("userprivileges",privs);
                            LocalStore.store('headerdata', data);
                            if(data.companydetails!=null){
                                RepositoryService.restore(data.companydetails[0].jobTitle.id).then(function (jobTitleData) {
                                    console.log(jobTitleData.data.name);
                                    if (jobTitleData.data.acrossDivision) {
                                        if(jobTitleData.data.name == 'MIS'){
                                            LocalStore.store('topcontext', 'mis');
                                            $state.go('mis.dashboard');
                                            $modalInstance.dismiss('cancel');
                                        }
                                        else if(jobTitleData.data.name=='HO')
                                        {
                                            LocalStore.store('topcontext', 'ho');
                                            $state.go('ho.dashboard');
                                            $modalInstance.dismiss('cancel');
                                        }
                                    }
                                    else {
                                        LocalStore.store('topcontext', 'home');
                                        $state.go('home.dashboard');
                                        $modalInstance.dismiss('cancel');
                                    }
                                })
                            }

                        });
                });
            });
        });
        $rootScope.mainViewStyle = "col-xs-19";
        $rootScope.leftMenuStyle = "col-xs-3 sidebar";

    }

    $scope.close=function(){
        $modalInstance.dismiss('cancel');
    }
});


sfe.controller('PasswordController',function($scope,LabelService,$state,LocalStore,$stateParams,
                                             PasswordUpdateService)
{

    $scope.links = LabelService.getLabels('links');
    $scope.labels = LabelService.getLabels('password');
    $scope.init=function(){}
    $scope.passwordData = [];
    $scope.response={};
    $scope.changePassword=function() {

        var currentPassword = $scope.currentpassword;
        var password = $scope.password;
        var confirmPassword = $scope.confirmPassword;

        var passwordObj = {};
        passwordObj.userId = LocalStore.fetch("headerdata").user.id;
        passwordObj.currentPassword = currentPassword;
        passwordObj.newPassword = password;
        PasswordUpdateService.updatePassword(passwordObj).then(function (response) {
            $scope.response = {message: response.data, class: "alert alert-success"};
        });
    };

});


/*sfe.controller('LeftMenuController', function ($scope, $compile, MenuService, LocalStore, LabelService, $rootScope) {
    $scope.labels= {};
    $scope.oneAtATime = true;
    $scope.open=false;
    $scope.menus = {};
    $scope.showAll=true;
    $scope.isCollapsed = false;
    $scope.menuWidth="col-xs-4";
    $scope.dynamicPopover ="";
    $scope.showHome = false;

    $scope.init=function(){
        $scope.showHome = LocalStore.fetch('topcontext');
    }
    MenuService.getMenus().then(function(data){
        $scope.menus = data;
        $scope.labels= LabelService.getLabels("leftmenu");
    });

    $scope.collapse = function(){
        $scope.isCollapsed = !$scope.isCollapsed;
        if($scope.isCollapsed){
            $rootScope.leftMenuStyle = 'col-xs-1';
            $rootScope.mainViewStyle = 'col-xs-22';
        }else{
            $rootScope.leftMenuStyle = 'col-xs-4';
            $rootScope.mainViewStyle = 'col-xs-19';
        }
        if($scope.isCollapsed)
            $scope.state="open";
        else
            $scope.state="collapse";
    }

    $scope.showChildren = function(menu){
        $scope.dynamicPopover = $compile('Hello<b>Hell</b>')($scope);
    }

    $scope.status = {
        isopen: false
    };

    $scope.state="collapse";

    $scope.showHome = LocalStore.fetch('topContext')=='home' ;

});*/

sfe.controller('LandingPageController', function ($scope, $http, $location) {
    $location.path('')
});