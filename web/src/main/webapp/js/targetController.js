sfe.filter('sumOfValues',function(){
    return function(input){
        if(!input)
            return 0;
        var sum=0.0;
        for(i in input) {
            sum=sum+(getNestedPropertyByKey(input[i],arguments[1]))
        }
        return sum;
    }

    function getNestedPropertyByKey(data, key) {
        if(data!=null) {
            data = data[key];
            if (isNaN(data)) {
                return 0;
            } else {
                return parseFloat(data);
            }
        }
    }
});


sfe.filter('calculateGrowth',function(){

    return function(input){
        if(!input)
            return 0;
        var growth=0;
        var p=arguments[1].split("~");
        var lys=0;
        var ts=0;

        for(i in input) {
            lys=lys+(getNestedPropertyByKey(input[i],p[0]))
        }

        for(z in input) {
            ts=ts+(getNestedPropertyByKey(input[z],p[1]))
        }

        if(lys==0){
            growth=Math.round(parseFloat(ts)*100);
        }else{
            growth=Math.round(((parseFloat(ts)-parseFloat(lys))/parseFloat(lys))*100);
        }
        return growth;
    }

    function getNestedPropertyByKey(data, key) {
        data = data[key];
        return Math.round(data);
    }
});


sfe.filter('sumOfPCPM',function(){

    return function(input){
        if(!input)
            return 0;
        var pcpm=0;
        var p=arguments[1].split("~");
        var rs=0;
        var actLc=0;

        for(i in input) {
            rs=rs+(getNestedPropertyByKey(input[i],p[0]))
        }

        for(z in input) {
            actLc=actLc+(getNestedPropertyByKey(input[z],p[1]))
        }

        pcpm=parseInt(rs)/(parseInt(actLc)*12);
        return parseInt(pcpm);
    }

    function getNestedPropertyByKey(data, key) {
        data = data[key];
        return Math.round(data);
    }
});


sfe.controller('targetController', function ($scope, $http, $rootScope, $compile, $location, RepositoryService, PrivilegeService, LocalStore,$modal) {

    $scope.products=[];
    $scope.result=[];
    $scope.table1=false;
    $scope.table2=false;
    $scope.table3=false;
    $scope.table4=false;
    $scope.target={};
    $scope.title="Zones";
    $scope.quarterTotal={"q1":{ly:0,ts:0,rs:0,gr:0},"q2":{ly:0,ts:0,rs:0,gr:0},"q3":{ly:0,ts:0,rs:0,gr:0},"q4":{ly:0,ts:0,rs:0,gr:0}}
    var zoneData={zone:"",count:0,lys:0,ps:0,rvs:0,growth:0};

    $scope.init=function(){
        RepositoryService.find("sel_prodt", {"division_id": LocalStore.fetch("USER_INFO").preferences.Division}, 'simple').success(function (data) {
            $scope.products=data;
        })

        if(LocalStore.fetch("headerdata").jobTitle.id=='JOBTT00000000000000000000000000000001'){
            $scope.title="Territories";
        };
    };

    $scope.search=function(){

        if($scope.product==null){
            alert("Please Select Product");
            return false;
        }else {
            $http({
                method: 'GET',
                url: restUrl + "/sales/salesService/getTarget/" + $scope.product + "/" + LocalStore.fetch("USER_INFO").preferences.Division + "/" +
                LocalStore.fetch("headerdata").location.id + "/" + LocalStore.fetch("headerdata").jobTitle.id + "/",
                headers: {
                    "content-type": "application/json",
                    "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                }
            }).success(function (response) {
                RepositoryService.find("trget", {"year": '2016', "product": $scope.product},'simple').success(function (data) {
                    $scope.target = data[0];
                    $scope.result = response;
                    $scope.table1 = true;
                    $scope.getQuarterTotal();
                })
            });
        }
    }

    $scope.showTable=function(cnt){
        switch (cnt){
            case 1:
                $scope.table1=true;
                $scope.table2=false;
                $scope.table3=false;
                $scope.table4=false;
                break;
            case 2:
                $scope.table2=true;
                $scope.table1=false;
                $scope.table3=false;
                $scope.table4=false;
                break;
            case 3:
                $scope.table3=true;
                $scope.table2=false;
                $scope.table1=false;
                $scope.table4=false;
                break;
            case 4:
                $scope.table4=true;
                $scope.table2=false;
                $scope.table3=false;
                $scope.table1=false;
                break;
        }
    };

    $scope.reviseTarget=function(result){
        if(result.revisedSale % 1!=0 || isNaN(result.revisedSale)){
            alert("only integer values allowed");
            result.revisedSale=result.targetedSale;
        }else{
            if(result.lastYearSale==0){
                result.growth= Math.round(parseFloat(result.revisedSale)*100);
            }else{
                result.growth=Math.round(((parseFloat(result.revisedSale)-parseFloat(result.lastYearSale))/parseFloat(result.lastYearSale))*100);
            }

            //update pcpm
            result.pcpm=parseInt((result.revisedSale)/(result.activeLocations * 12));
        }
        //update quarters
        //q1
        result.quarter1.revisedSale=getRevisedSale(result.revisedSale,$scope.target.q1Target);
        result.quarter1.growth=getGrowth(result.quarter1.lastYearSale,result.quarter1.revisedSale);
        //q2
        result.quarter2.revisedSale=getRevisedSale(result.revisedSale,$scope.target.q2Target);
        result.quarter2.growth=getGrowth(result.quarter2.lastYearSale,result.quarter2.revisedSale);
        //q3
        result.quarter3.revisedSale=getRevisedSale(result.revisedSale,$scope.target.q3Target);
        result.quarter3.growth=getGrowth(result.quarter3.lastYearSale,result.quarter3.revisedSale);
        //q4
        result.quarter4.revisedSale=getRevisedSale(result.revisedSale,$scope.target.q4Target);
        result.quarter4.growth=getGrowth(result.quarter4.lastYearSale,result.quarter4.revisedSale);

        result.totalRevised=result.quarter1.revisedSale+result.quarter2.revisedSale+result.quarter3.revisedSale+result.quarter4.revisedSale;

        $scope.getQuarterTotal();
    }

    $scope.reviseQuarter=function(result,target){
        if(result.revisedSale % 1!=0 || isNaN(result.revisedSale)){
            alert("only integer values allowed");
            result.revisedSale=result.targetedSale;
        }else{
            var updateValue=true;

            if (result.type.id != 'USRLVQUARTER0000000000000000000000004') {
                var q4rs=parseInt(target.revisedSale)-(parseInt(target.quarter1.revisedSale)+parseInt(target.quarter2.revisedSale)+parseInt(target.quarter3.revisedSale))
                if(q4rs < 0){
                    result.revisedSale=result.targetedSale;
                    updateValue=false;
                    alert("Revised sale can not be grater than targeted sale");
                }else {
                    target.quarter4.revisedSale = q4rs;
                }
                $scope.getQuarterTotal();
            }else{
                var totalRevised = parseInt(target.quarter1.revisedSale) + parseInt(target.quarter2.revisedSale) + parseInt(target.quarter3.revisedSale) + parseInt(target.quarter4.revisedSale);
                if(totalRevised!=target.targetedSale){
                    result.revisedSale=result.targetedSale;
                    updateValue=false;
                    alert("Revised sale must be equal to targeted sale");
                }
                $scope.getQuarterTotal();
            }
            if(updateValue){
                if (result.lastYearSale == 0) {
                    result.growth = Math.round(parseFloat(result.revisedSale) * 100);
                } else {
                    result.growth = Math.round(((parseFloat(result.revisedSale) - parseFloat(result.lastYearSale)) / parseFloat(result.lastYearSale)) * 100);
                }
                $scope.getQuarterTotal();
                target.totalRevised = parseInt(target.quarter1.revisedSale) + parseInt(target.quarter2.revisedSale) + parseInt(target.quarter3.revisedSale) + parseInt(target.quarter4.revisedSale);
            }
        }
    }
    function getRevisedSale(rs,per){
        return Math.round((parseFloat(rs)*parseFloat(per))/100);
    }

    function getGrowth(ls,rs){
        if(ls==0){
            return Math.round(parseFloat(rs)*100);
        }else{
            return Math.round(((parseFloat(rs)-parseFloat(ls))/parseFloat(ls))*100)
        }
    }

    $scope.save=function(){

        var save=true;

        /*for(r in $scope.result){
            var res=$scope.result[r];
            if(res.totalRevised < res.totalTarget){
                alert("Your revised sale is less than targeted sale for "+ res.location.displayName.toLowerCase() +" zone");
                save=false;
                break;
            }else if(isNaN(res.revisedSale) || isNaN(res.quarter1.revisedSale) ||isNaN(res.quarter2.revisedSale)
                     || isNaN(res.quarter3.revisedSale) || isNaN(res.quarter4.revisedSale)){
                alert("Your have entered wrong revised sale  values for "+ res.location.displayName.toLowerCase() +" zone");
                save=false;
                break;
            }
        }*/

        if(save) {

            /*if($scope.quarterTotal.q1.ts!=$scope.quarterTotal.q1.rs){
                alert("Targeted sale and revised should same in quarter one");
                return false;
            }else if($scope.quarterTotal.q2.ts!=$scope.quarterTotal.q2.rs){
                alert("Targeted sale and revised should same in quarter two");
                return false;
            }else if($scope.quarterTotal.q3.ts!=$scope.quarterTotal.q3.rs){
                alert("Targeted sale and revised should same in quarter three");
                return false;
            }else if($scope.quarterTotal.q4.ts!=$scope.quarterTotal.q4.rs){
                alert("Targeted sale and revised should same in quarter four");
                return false;
            }else {*/
                var dto = {details: $scope.result};
                $http({
                    method: 'POST',
                    data: dto,
                    url: restUrl + "/sales/salesService/saveTargets",
                    headers: {
                        "content-type": "application/json",
                        "AUTH_CERTIFICATE": LocalStore.fetch('USER_INFO').certificate
                    }
                }).success(function (response) {
                    alert("Target saved successfully.");
                    $scope.search();
                })
            //}
        }
    }

    $scope.getQuarterTotal=function(){

        $scope.quarterTotal={"q1":{ly:0,ts:0,rs:0,gr:0},"q2":{ly:0,ts:0,rs:0,gr:0},"q3":{ly:0,ts:0,rs:0,gr:0},"q4":{ly:0,ts:0,rs:0,gr:0}};

        for(r in $scope.result){
            var target=$scope.result[r];
            $scope.quarterTotal.q1.ly=parseInt($scope.quarterTotal.q1.ly)+parseInt(target.quarter1.lastYearSale);
            $scope.quarterTotal.q1.ts=parseInt($scope.quarterTotal.q1.ts)+parseInt(target.quarter1.targetedSale);
            $scope.quarterTotal.q1.rs=parseInt($scope.quarterTotal.q1.rs)+parseInt(target.quarter1.revisedSale);

            $scope.quarterTotal.q2.ly=parseInt($scope.quarterTotal.q2.ly)+parseInt(target.quarter2.lastYearSale);
            $scope.quarterTotal.q2.ts=parseInt($scope.quarterTotal.q2.ts)+parseInt(target.quarter2.targetedSale);
            $scope.quarterTotal.q2.rs=parseInt($scope.quarterTotal.q2.rs)+parseInt(target.quarter2.revisedSale);

            $scope.quarterTotal.q3.ly=parseInt($scope.quarterTotal.q3.ly)+parseInt(target.quarter3.lastYearSale);
            $scope.quarterTotal.q3.ts=parseInt($scope.quarterTotal.q3.ts)+parseInt(target.quarter3.targetedSale);
            $scope.quarterTotal.q3.rs=parseInt($scope.quarterTotal.q3.rs)+parseInt(target.quarter3.revisedSale);

            $scope.quarterTotal.q4.ly=parseInt($scope.quarterTotal.q4.ly)+parseInt(target.quarter4.lastYearSale);
            $scope.quarterTotal.q4.ts=parseInt($scope.quarterTotal.q4.ts)+parseInt(target.quarter4.targetedSale);
            $scope.quarterTotal.q4.rs=parseInt($scope.quarterTotal.q4.rs)+parseInt(target.quarter4.revisedSale);
        }



        if($scope.quarterTotal.q1.ly==0){
            $scope.quarterTotal.q1.gr=parseInt($scope.quarterTotal.q1.rs)*100;
        }else{
            $scope.quarterTotal.q1.gr=((parseInt($scope.quarterTotal.q1.rs)-parseInt($scope.quarterTotal.q1.ly))/parseInt($scope.quarterTotal.q1.ly))*100;
        }

        if($scope.quarterTotal.q2.ly==0){
            $scope.quarterTotal.q2.gr=parseInt($scope.quarterTotal.q2.rs)*100;
        }else{
            $scope.quarterTotal.q2.gr=((parseInt($scope.quarterTotal.q2.rs)-parseInt($scope.quarterTotal.q2.ly))/parseInt($scope.quarterTotal.q2.ly))*100;
        }

        if($scope.quarterTotal.q3.ly==0){
            $scope.quarterTotal.q3.gr=parseInt($scope.quarterTotal.q3.rs)*100;
        }else{
            $scope.quarterTotal.q3.gr=((parseInt($scope.quarterTotal.q3.rs)-parseInt($scope.quarterTotal.q3.ly))/parseInt($scope.quarterTotal.q3.ly))*100;
        }

        if($scope.quarterTotal.q4.ly==0){
            $scope.quarterTotal.q4.gr=parseInt($scope.quarterTotal.q4.rs)*100;
        }else{
            $scope.quarterTotal.q4.gr=((parseInt($scope.quarterTotal.q4.rs)-parseInt($scope.quarterTotal.q4.ly))/parseInt($scope.quarterTotal.q4.ly))*100;
        }
    }

    $scope.getDetails=function(details){
        var visitedTowns = $modal.open({
            templateUrl: 'details.html',
            controller: 'detailsController',
            size: 'lg',
            backdrop: false,
            backdropClick: false,
            dialogFade: true,
            keyboard: false,
            resolve: {
                details: function () {
                    return details;
                }
            }
        })
    }
})


sfe.controller('detailsController',function ($scope, $http,LabelService, RepositoryService, LocalStore, $modalInstance,details){
    $scope.details=details;
    $scope.locationName="";
    $scope.init = function(){
        if($scope.details==0){
            $scope.locationName=null;
            RepositoryService.find("overall_target", {"location": LocalStore.fetch("headerdata").location.id,"jobtitle":LocalStore.fetch("headerdata").jobTitle.id}, 'simple').success(function (data) {
                $scope.data=data;
            })
        }else{
            $scope.locationName= $scope.details.location.displayName;
            RepositoryService.find("location_target", {"location": $scope.details.location.id,"jobtitle":LocalStore.fetch("headerdata").jobTitle.id}, 'simple').success(function (data) {
                $scope.data=data;
            })
        }
    };
    $scope.closeModal = function(){
        $modalInstance.dismiss('cancel');
    };
})
