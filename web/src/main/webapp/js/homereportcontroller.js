sfe.controller('MyFieldReportController',
    function ($scope, $http, $rootScope, $compile, $location, $stateParams, LabelService, RepositoryService,
            months,monthNameToIndexJS) {
        $scope.months = months;
        $scope.years = [];
        $scope.data = {table:{tabledata:[]}}
        $scope.charts= [];
        $scope.startMonth= {};
        $scope.endMonth = {};

        $scope.chartTitles = ['Field Days', 'Total Doctor Calls','Call Average','Overall Coverage'];
        $scope.selectedChartTypes = ["Bar","Bar","Line","Line"];
        $scope.selectedReports = ["fieldData","docCall","callAvg","overallCoverage"];
        $scope.titles = {};
        $scope.chartNames = [{"id":"fieldData","label":"Field Days"},
                             {"id":"docCall","label":"Total Doctor Calls"}, {"id":"callAvg","label":"Call Average"},
                             {"id":"overallCoverage","label":"Overall Coverage"}
                             ];
        $scope.headings = ['My Report','Field Coverage','Coaching'];
        $scope.initSearch= function(){

            var today = new Date();
            var year = today.getFullYear();
            $scope.years.push(year-1, year);
        }

        $scope.selectGraph = function(i, type){
            $scope.selectedChartTypes[i] = type;
        }

        $scope.showReport = function(){
            var startMonth = monthNameToIndexJS[$scope.startMonth];
            var endMonth = monthNameToIndexJS[$scope.endMonth];
            var fieldData = [24,20,28,23,21,28,22,20,27,21,20,18];
            var doctorCallData = [320,400,200,122,40,260,300,210,100,120,301,212];
            var callAverageData =  [11,13,20,7,5,1.6,14,11.5,8,3,4,10,11];
            var overallCoverageData = [80,90,85,55,95,100,110,93,35,100,101,85];


            $scope.data.table.tabledata = [];
            $scope.charts=[];

            var selectedReports = $scope.selectedReports;
            var tableheaders = ["Parameters","Target"];
            var fieldTableData = [{val:"Field Days"},{val:22}];
            var fieldChartData = [22];

            var totalDoctorCallsTableData = [{val:"Total Doctor Calls"},{val:250}];
            var totalDoctorChartData = [250];
            var callAverageChartData = [11];
            var callAverageTableData = [{val:"Call Average"},{val:12}];

            var overallCoverageTableData = [{val:'Overall Coverage'},{val:80}];
            var overallCoverageChartData = [80];

            for(var i=startMonth;i<=endMonth;i++){
                tableheaders.push(months[i]);
                if(selectedReports.indexOf("fieldData")>=0) {
                    fieldTableData.push({val: fieldData[i]});
                    fieldChartData.push(fieldData[i]);
                }
                if(selectedReports.indexOf("docCall")>=0) {
                    totalDoctorCallsTableData.push({val: doctorCallData[i]});
                    totalDoctorChartData.push(doctorCallData[i]);
                }
                if(selectedReports.indexOf("callAvg")>=0) {
                    callAverageTableData.push({val: callAverageData[i]});
                    callAverageChartData.push(callAverageData[i]);
                }
                if(selectedReports.indexOf('overallCoverage')>=0){
                    overallCoverageTableData.push({val:overallCoverageData[i]});
                    overallCoverageChartData.push(overallCoverageData[i]);
                }
            }
            $scope.data.table.tableheaders = tableheaders;

            if(selectedReports.indexOf("fieldData")>=0) {
                $scope.data.table.tabledata.push(fieldTableData);

                var data = {
                    labels :tableheaders.slice(1,endMonth+3),
                    datasets:[{
                        fillColor : "rgba(91,192,222,0.5)",
                        responsive: true,
                        strokeColor : "rgba(220,220,220,1)",
                        pointColor : "rgba(220,220,220,1)",
                        pointStrokeColor : "#fff",
                        data :fieldChartData
                    }]
                };

                $scope.charts.push({"data":data});
            }

            if(selectedReports.indexOf("docCall")>=0) {
                $scope.data.table.tabledata.push(totalDoctorCallsTableData);
                var data = {
                    labels :tableheaders.slice(1,endMonth+3),
                    datasets:[{
                        fillColor : "rgba(240,173,78,0.5)",
                        strokeColor : "rgba(220,220,220,1)",
                        pointColor : "rgba(220,220,220,1)",
                        pointStrokeColor : "#fff",
                        data :totalDoctorChartData
                    }]
                };

                $scope.charts.push({"data":data});

            }

            if(selectedReports.indexOf("callAvg")>=0) {
                $scope.data.table.tabledata.push(callAverageTableData);
                var data = {
                    labels :tableheaders.slice(1,endMonth+3),
                    datasets:[{
                        fillColor : "rgba(91,192,222,0.5)",
                        strokeColor : "rgba(220,220,220,1)",
                        pointColor : "rgba(220,220,220,1)",
                        pointStrokeColor : "#fff",
                        data :callAverageChartData
                    }]
                };
                $scope.charts.push({"data":data});
            }



            if(selectedReports.indexOf("overallCoverage")>=0) {
                $scope.data.table.tabledata.push(overallCoverageTableData);
                var data = {
                    labels :tableheaders.slice(1,endMonth+3),
                    datasets:[{
                        fillColor : "rgba(91,192,222,0.5)",
                        strokeColor : "rgba(220,220,220,1)",
                        pointColor : "rgba(220,220,220,1)",
                        pointStrokeColor : "#fff",
                        data :overallCoverageChartData
                    }]
                };
                $scope.charts.push({"data":data});
            }

        }
});