<%@ page import="java.util.Date" %>
<%--
  Created by IntelliJ IDEA.
  User: shriramgosavi
  Date: 07/08/15
  Time: 1:36 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.Date"%>
<!DOCTYPE HTML>
<html lang="en" ng-app="sfe">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE"/>
  <title>SugrQb</title>
  <link rel="stylesheet" href="css/angular-block-ui.min.css"/>

  <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>

  <link rel="stylesheet" href="css/bootstrap-theme.css" type="text/css"/>
  <link rel="stylesheet" href="css/form.css" type="text/css"/>
  <link rel="stylesheet" href="css/squerstyle.css" type="text/css"/>

  <link rel="stylesheet" href="css/jquery-ui.css"type="text/css"/>
  <link rel="stylesheet" href="css/bootstrap-dialog.min.css" type="text/css"/>
  <link rel="stylesheet" href="assets/rainbow.css"/>
  <link rel="stylesheet" href="css/ui-grid.css"/>
  <link rel="stylesheet" href="css/isteven-multi-select.css"/>
  <link rel="stylesheet" href="css/font-awesome.min.css"/>
  <link rel="stylesheet" href="js/lib/angular-surveys/form-builder-bootstrap.min.css"/>
  <link rel="stylesheet" href="js/lib/angular-surveys/form-builder.min.css"/>
  <link rel="stylesheet" href="js/lib/angular-surveys/form-viewer-bootstrap.min.css"/>
  <link rel="stylesheet" href="js/lib/angular-surveys/form-viewer.min.css"/>




  <!--link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"-->


  <style>
    .modal-dialog{
      /*position: absolute;
      left: 40%;
      /*now you must set a margin left under zero - value is a half width your window
      margin-left: -312px;
      // this same situation is with height - example
      height: 500px;
      top: 50%;
      margin-top: -250px;*/
    }
  </style>
  <!--[if lte IE 8]>
  <!--script src="js/lib/json3.js"></script>
  <script src="js/lib/es5-shim.min.js"></script-->
  <![endif]-->
</head>
<body>
<div class="container-fluid" ui-view></div>
<script src="js/lib/angular.min.js"></script>
<script src="js/lib/angular-route.min.js"></script>
<script src="js/lib/angular-ui-router.js"></script>
<script src="js/lib/angular-cookies.js"></script>
<script src="js/lib/angular-block-ui.min.js"></script>
<script src="js/lib/checklist.js"></script>
<script src="js/lib/dynaform.js"></script>
<script src="js/lib/jquery.js"></script>
<script src="js/lib/jquery-ui-1.10.3.custom.min.js"></script>
<script src="js/lib/moment.js"></script>
<script src="js/lib/lodash.js"></script>

<script src="js/lib/highcharts.js"></script>
<script src="js/lib/highcharts-all.js"></script>
<script src="js/lib/highcharts-ng.js"></script>
<script src="js/lib/highcharts-3d.js"></script>
<script src="js/lib/highcharts-ng.min.js"></script>
<script src="js/lib/highcharts-more.js"></script>

<script src="js/lib/ui-bootstrap-tpls-0.11.2.js"></script>
<script src="js/lib/FileSaver.js"></script>
<script src="js/lib/sortable/sortable.min.js"></script>
<script src="js/lib/sortable/ng-sortable.js"></script>
<script src="js/lib/angular-surveys/elastic.js"></script>
<script src="js/lib/angular-translate/angular-translate.js"></script>
<script src="js/lib/angular-translate/angular-translate-loader-static-files.min.js"></script>

<script src="js/lib/angular-surveys/form-utils.min.js"></script>
<script src="js/lib/angular-surveys/form-builder.min.js"></script>
<script src="js/lib/angular-surveys/form-builder-bootstrap-tpls.min.js"></script>
<script src="js/lib/angular-surveys/form-viewer.min.js"></script>
<script src="js/lib/angular-surveys/form-viewer-bootstrap-tpls.min.js"></script>
<script src="js/app.js?d=<%= new java.util.Date().getTime()%>"></script>
<script src="js/services.js?d=<%= new java.util.Date().getTime()%>"></script>
<script src="js/commonSearchService.js?d=<%= new java.util.Date().getTime()%>"></script>
<script src="js/directives.js?d=<%= new java.util.Date().getTime()%>"></script>
<script src="js/commonservice.js?d=<%= new java.util.Date().getTime()%>"></script>
<script src="js/lib/ui-grid.js"></script>
<script src="js/admincontroller.js?d=<%= new java.util.Date().getTime()%>"></script>
<script src="js/commoncontroller.js?d=<%= new java.util.Date().getTime()%>"></script>
<script src="js/homecontroller.js?d=<%= new java.util.Date().getTime()%>"></script>
<script src="js/expenseController.js?d=<%= new java.util.Date().getTime()%>"></script>
<script src="js/controllers/plan/plancontroller.js?d=<%= new java.util.Date().getTime()%>"></script>
<script src="js/controllers/plan/beatscontroller.js?d=<%= new java.util.Date().getTime()%>"></script>
<script src="js/controllers/roi/roicontroller.js?d=<%= new java.util.Date().getTime()%>"></script>
<script src="js/controllers/patient/patientcontroller.js?d=<%= new java.util.Date().getTime()%>"></script>
<script src="js/controllers/holiday/holiday.js?d=<%= new java.util.Date().getTime()%>"></script>
<script src="js/controllers/miscellaneous/miscellaneous.js?d=<%= new java.util.Date().getTime()%>"/>
<script src="js/homereportcontroller.js?d=<%= new java.util.Date().getTime()%>"></script>
<script src="js/targetController.js?d=<%= new java.util.Date().getTime()%>"></script>
<script src="js/miscontroller.js?d=<%= new java.util.Date().getTime()%>"></script>
<script src="js/reportcontroller.js?d=<%= new java.util.Date().getTime()%>"></script>
<script src="js/routemanager.js?d=<%= new java.util.Date().getTime()%>"></script>
<script src="js/lib/waypoints.js"></script>
<script src="js/lib/count-to.js"></script>
<script src="js/lib/valdr.js"></script>
<script src="js/lib/valdr-message.min.js"></script>
<script src="js/lib/isteven-multi-select.js"></script>
<script src='js/lib/textAngular-rangy.min.js'></script>
<script src='js/lib/textAngular-sanitize.min.js'></script>
<script src='js/lib/textAngular.min.js'></script>
<script src="js/angular-local-storage.min.js"></script>
<script src="js/controllers/upload/uploadcontroller.js" ></script>
<script src="js/surveyController.js?d=<%= new java.util.Date().getTime()%>"></script>
<script src="js/controllers/survey/surveyCtrl.js"></script>


<script>
  $(window).scroll(function(){
    $('#message_box').animate({top:$(window).scrollTop + "px"},{queue:false, duration: 350})
  });
  $('#close_message').click(function(){
    $('#message_box').animate({top:"+=15px",opacity:0},"slow");
  });
</script>
</body>

<div>
  <script type="text/ng-template" id="userLogin.html">
    <div class="modal-header" ng-init="init()">
      <div class="row">
        <div class="col-xs-20">
          <h4 class="modal-title">Login As User</h4>
        </div>
        <div>
          <a href ng-click="close()" class="pull-right"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
        </div>
      </div>
    </div>
    <div class="modal-body">
      <form class="form-horizontal">
        <div class="form-group">
          <label for="userId" class="col-xs-5 control-label">User Id</label>
          <div class="col-xs-12">
            <input type="text" class="form-control" id="userId" ng-model="guest.userId" required>
          </div>
          <button type="submit" class="btn btn-primary" ng-click="loginAsGuest()">Login</button>
        </div>
      </form>
    </div>
  </script>
</div>
</html>
