(function (define) {
    define(['common/directives/header', 'common/directives/footer'], function () {
        var loginDirective = function () {
            return {
                require: 'header,footer',
                restrict: 'E',
                scope: {
                },
                transclude: true,
                templateUrl: 'templates/login.html'
            };
        };
        return loginDirective;
    })
}(define));