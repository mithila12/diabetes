(function (define) {
    define([], function () {
        return function () {
            return {
                restrict: 'E',
                templateUrl: 'templates/menus.html'
            };
        }
    });
}(define));