(function (define) {
    define([], function () {
        return function ($timeout) {
            return {
                restrict: 'E',
                templateUrl: 'templates/slideshow.html',
                link: function (scope, elem, attrs) {
                    var images = scope && scope.data && scope.data.images ? scope.data.images : [];

                    scope.data.currentIndex=0;

                    scope.data.next=function(){
                        scope.data.currentIndex<images.length-1?scope.data.currentIndex++:scope.data.currentIndex=0;
                    };

                    scope.data.prev=function(){
                        scope.data.currentIndex > 0? scope.data.currentIndex-- : scope.data.currentIndex = images.length-1;
                    };

                    scope.$watch('data.currentIndex',function() {
                        if(images && images.length) {
                            images.forEach(function(image){
                                image.visible=false;
                            });
                            if(images && images[scope.data.currentIndex]) {
                                images[scope.data.currentIndex].visible=true;
                            }
                        }
                    });

                    /* Start: For Automatic slideshow
                    var timer;

                    var sliderFunc=function(){
                        timer=$timeout(function(){
                            scope.data.next();
                            timer=$timeout(sliderFunc,5000);
                        },5000);
                    };

                    sliderFunc();

                    scope.$on('$destroy',function(){
                        $timeout.cancel(timer);
                    });*/

                    /* End : For Automatic slideshow*/

                }
            };
        }
    });
}(define));