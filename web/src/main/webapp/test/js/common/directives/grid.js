(function (define) {
    define([], function () {
        return function () {
            return {
                restrict: 'E',
                scope: { obj: '=' },
                template: '<div class="gridStyle" ng-grid="data.gridOptions"></div>'
            };
        }
    });
}(define));