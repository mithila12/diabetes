(function (define) {
    define([], function () {
        return function () {
            return {
                restrict: 'E',
                template:
                '<fieldset class="data[data.modelName].buttons.class">' +
                    '<div class="btn-group btn-group-sm">' +
                        '<button ng-repeat="button in data[data.modelName].buttons.fields" ' +
                            'type="button" ' +
                            'class="{{button.class}}"' +
                            'click="{{button.callback}}">' +
                        '{{ button.label }}' +
                        '</button>' +
                    '</div>' +
                '</fieldset>'
            };
        }
    });
}(define));