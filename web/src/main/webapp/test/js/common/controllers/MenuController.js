(function (define) {
    define([], function () {
        var MenuController = function ($scope, $http, $compile, $location, MenuDataService) {
            $scope.data = $scope.data || {};
            $http.get('js/jsondata.json').then(function (response) {
                $scope.data.menus = response.data.homePageUIDetails.menus;
                $scope.data.formTemplate = response.data.homePageUIDetails.menus[0].formTemplate;
                $scope.menuClickHandler = function (menuUrl) {
                    $.each($scope.data.menus, function (index, menu) {
                        if (menu.url == menuUrl) {
                            $scope.data.formTemplate = menu.formTemplate;
                            $(".formContainer").html('<dynamicforms></dynamicforms>');
                            $compile($(".formContainer").contents())($scope);
                            $location.path(menu.url);
                        } else if (menu.split && menu.split.length) {
                            $.each(menu.split, function (subIndex, submenu) {
                                if (submenu.url == menuUrl) {
                                    $scope.data.formTemplate = submenu.formTemplate;
                                    $(".formContainer").html('<dynamicforms></dynamicforms>');
                                    $compile($(".formContainer").contents())($scope);
                                    $location.path(submenu.url);
                                }
                            });
                        }
                    });
                };
            });
        };
        return ["$scope", "$http", "$compile", "$location", "MenuDataService", MenuController];
    });
}(define));