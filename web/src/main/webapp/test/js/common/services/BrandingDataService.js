(function (define) {
    define([], function () {
        var BrandingDataService = function () {
            this.branding = {};
            return {
                getBrandingData : function() {
                    return this.branding;
                },
                setBrandingData: function(branding) {
                    this.branding = branding;
                }
            };
        };
        return BrandingDataService;
    });
}(define));