(function (define) {
    define([], function () {
        var MenuDataService = function () {
            this.menus = {};
            return {
                getMenus : function() {
                    return this.menus;
                },
                setMenus : function(menus) {
                    this.menus = menus;
                }
            };
        };
        return MenuDataService;
    });
}(define));