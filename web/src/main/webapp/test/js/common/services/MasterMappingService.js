(function (define) {
    define([], function () {
        var MasterMappingService = function () {
            var me = this;
            me.masterJsonMapping = {
                '/master/buyer/buyer-consignee-notify': 'js/mastersjson/buyer_consignee_notify.json',
                '/master/buyer/country': 'js/mastersjson/country_master.json',
                '/master/buyer/currency': 'js/mastersjson/currency_master.json',
                '/master/exporter-master/exporter': 'js/mastersjson/exporter_master.json',
                '/master/exporter-master/factory': 'js/mastersjson/factory_master.json',
                '/master/exporter-master/bank': 'js/mastersjson/bank_master.json',
                '/master/exporter-master/insurance': 'js/mastersjson/insurance_master.json',
                '/master/exporter-master/exporter-type': 'js/mastersjson/exporter_type_master.json',
                '/master/exporter-master/business-type': 'js/mastersjson/business_type_master.json',
                '/master/port/loading': 'js/mastersjson/loading_master.json',
                '/master/port/discharge': 'js/mastersjson/discharge_master.json',
                '/master/other/inco-terms': 'js/mastersjson/inco_terms_master.json',
                '/master/other/dispatch-mode': 'js/mastersjson/dispatch_mode_master.json',
                '/master/other/payment-mode': 'js/mastersjson/payment_mode_master.json',
                '/master/other/schema': 'js/mastersjson/schema_master.json',
                '/master/other/cha': 'js/mastersjson/cha_master.json',
                '/master/other/shipping-line': 'js/mastersjson/shipping_line_master.json',
                '/master/other/forwarder': 'js/mastersjson/forwarder_master.json',
                '/master/other/foreign-agent': 'js/mastersjson/foreign_agent_master.json',
                '/master/other/transporter': 'js/mastersjson/transporter_master.json',
                '/master/other/document': 'js/mastersjson/document_master.json',
                '/master/other/expenses': 'js/mastersjson/expenses_master.json',
                '/master/product/product-master': 'js/mastersjson/product_master.json',
                '/master/product/product-unit': 'js/mastersjson/product_unit_master.json',
                '/master/product/grade': 'js/mastersjson/grape_master.json',
                '/master/product/product-group-type': 'js/mastersjson/product_group_type_master.json',
                '/master/product/import-checklist': 'js/mastersjson/import_checklist_master.json',
                '/master/schema-application/advance-license': 'js/mastersjson/advance_license_master.json',
                '/master/schema-application/epcg-license': 'js/mastersjson/epcg_license_master.json',
                '/master/schema-application/system': 'js/mastersjson/system_master.json',
                '/master/user-management/employee': 'js/mastersjson/employee_master.json',
                '/master/user-management/designation': 'js/mastersjson/designation_master.json',
                '/master/user-management/department': 'js/mastersjson/department_master.json'
            };
            return {
                getJsonFileName: function(masterName) {
                    if(!me.masterJsonMapping[masterName]) {
                        throw "Master Name not registered: " + masterName;
                    }
                    return me.masterJsonMapping[masterName];
                },

                getFormData: function() {
                    var data = {};
                    if(jQuery(form)) {
                        jQuery.each(jQuery(form).serialize().split("&"),function(index,element) {
                            data[element.split("=")[0]] = element.split("=")[1]
                        });
                    }
                    return data;
                }
            };
        };
        return MasterMappingService;
    });
}(define));