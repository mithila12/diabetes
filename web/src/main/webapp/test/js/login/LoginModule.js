(function(define) {
    define(['login/LoginController', 'home/HomePageController', 'common/services/MenuDataService',
        'common/directives/login', 'common/directives/header', 'common/directives/footer'],
        function(LoginController, HomePageController, MenuDataService, loginDirective, headerDirective, footerDirective) {
        var moduleName = "loginModule";
        angular.module(moduleName, ['ngRoute'])
            .controller("LoginController", LoginController)
            .controller("HomePageController", HomePageController)
            .service("MenuDataService", MenuDataService)
            .directive('login', loginDirective)
            .directive('header', headerDirective)
            .directive('footer', footerDirective);
        return moduleName;
    });
}(define));