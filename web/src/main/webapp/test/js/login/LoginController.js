(function (define) {
    define([], function () {
        var LoginController = function ($scope, $http, $location, $timeout, $rootScope) {
            $scope.data = $scope.data || {};
            $rootScope.data = $rootScope.data || {};

            $http.get('js/jsondata.json').then(function (response) {
                $scope.data.branding = response.data.branding;
                $rootScope.data.branding = response.data.branding;
            });

            $scope.signIn = function () {
                var data = $scope.data;
                //TODO: replace with actual login
                $http.get('js/jsondata.json').then(function (response) {

                    if(response.data.login.username != data.username
                        || response.data.login.password != data.password) {
                        alert('Invalid username/passord');
                    } else {
                        $timeout(function() {
                            $location.path("home");
                        });
                    }
                });
            };
        };
        return ["$scope", "$http", '$location', '$timeout', '$rootScope', LoginController];
    });
}(define));