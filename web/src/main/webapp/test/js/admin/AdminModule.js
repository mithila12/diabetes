(function(define) {
    define(['admin/AdminController', 'common/services/MenuDataService', 'common/services/BrandingDataService'],
        function(AdminController, MenuDataService, BrandingDataService) {
            var moduleName = "adminModule";
            angular.module(moduleName, ['ngRoute'])
                .service("MenuDataService", MenuDataService)
                .service("BrandingDataService", BrandingDataService)
                .controller("AdminController", AdminController);
            return moduleName;
        });
}(define));