(function (define) {
    define([], function () {
        var AdminController = function ($scope, $http, $compile, $rootScope, $location, BrandingDataService) {
            $scope.data = $scope.data || {};
            $scope.settings = $scope.settings || {};

            $scope.data.branding = BrandingDataService.getBrandingData();

            var updateFormTemplate = function(response) {
                $scope.data.formTemplate = response.data.formTemplate;
                $compile($(".formContainer").contents())($scope);
            };

            if($location.path().indexOf('settings') > -1) {
                $http.get('js/settings.json').then(function (response) {
                    updateFormTemplate(response);
                });
            } else if($location.path().indexOf('employees') > -1) {
                $http.get('js/employee_profile.json').then(function (response) {
                    updateFormTemplate(response);
                });
            }

            $scope.settings.saveGlobalSettings = function() {
                var currency = $scope.data.currency,
                    dateformat = $scope.data.dateformat;

                if(!currency) {
                    alert('Currency is required');
                }

                if(!dateformat) {
                    alert('Date format is required');
                }
                alert("Saving Currency as " + currency);
                alert("Saving date format as " + dateformat);
            };

            $scope.settings.changeAdminPassword = function() {
                var oldPwd = $scope.data.oldPwd,
                    newPwd = $scope.data.newPwd;
              if(oldPwd === newPwd) {
                  alert('Old and New password cannot be same');
                  return false;
              }
              alert('Changing password from: ' + oldPwd + ' to ' + newPwd );
            };

            $scope.settings.saveEventSettings = function() {
                var events = $('[name*="settings.event"]');
                $.each(events, function(index, e) {
                    console.log($(e).attr('name') + ' ' + $scope.data[$(e).attr('name')]);
                });
            }
        };
        return ["$scope", "$http", "$compile", "$rootScope", "$location", "BrandingDataService", AdminController];
    });
}(define));