(function (define) {
    define([ 'login/LoginController', 'home/HomePageController', 'admin/AdminController', 'masters/MastersController'],
        function (LoginController, HomePageController, AdminController) {
            var RouteManager = function ($routeProvider) {
                $routeProvider.
                    when('/login', {
                        templateUrl: 'templates/login.html',
                        controller: 'LoginController'
                    })
                    .when('/home', {
                        templateUrl: 'templates/home.html',
                        controller: 'HomePageController'
                    })
                    .when('/productcreate', {
                        templateUrl: 'templates/productcreate.html',
                        controller: 'HomePageController'
                    })
                    .when('/settings', {
                        templateUrl: 'templates/settings.html',
                        controller: 'AdminController'
                    })
                    .when('/employees', {
                        templateUrl: 'templates/employee.html',
                        controller: 'AdminController'
                    })
                    .when('/master/:masterName*', {
                        templateUrl: 'templates/masters.html',
                        controller: 'MastersController'
                    })
                   ;
            };
            return ["$routeProvider", RouteManager ];
        });
}(define));