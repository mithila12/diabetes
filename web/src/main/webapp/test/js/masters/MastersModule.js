(function(define) {
    define(['masters/MastersController', 'home/HomePageController',
        'common/services/MenuDataService',
        'common/services/MasterMappingService',
        'common/directives/toolbar'
        ],
        function(MastersController, HomePageController, MenuDataService, MasterMappingService, toolbarDirective) {
            var moduleName = "mastersModule";
            angular.module(moduleName, ['ngRoute'])
                .controller("MastersController", MastersController)
                .controller("HomePageController", HomePageController)
                .service("MenuDataService", MenuDataService)
                .service("MasterMappingService", MasterMappingService)
                .directive('toolbar', toolbarDirective);
            return moduleName;
        });
}(define));