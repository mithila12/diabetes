(function (define) {
    define([], function () {
        var MastersController = function ($scope, $http, $location, $timeout, $rootScope, $compile, MasterMappingService) {
            $scope.data = $scope.data || {};
            $rootScope.data = $rootScope.data || {};

            var updateFormTemplate = function(response) {
                $scope.data.formTemplate = response.data.formTemplate;
                $compile($(".formContainer").contents())($scope);
            };

            var applyDataToResponse = function(response) {
                $scope.data = $scope.data || {};
                $scope.data.modelName = response.data.model;
                $scope.data[response.data.model] = {
                    "masterName": response.data.masterName,
                    "formTemplate": response.data.formTemplate,
                    "buttons": response.data.buttonBar
                };
            };
            var masterJSONFileName = MasterMappingService.getJsonFileName($location.path());
            $http.get(masterJSONFileName).then(function (response) {
                applyDataToResponse(response);
                $scope.newExporterMaster = function() {
                    $scope.fireEvent('formsubmit');
                };
                updateFormTemplate(response);
            });
        };
        return ["$scope", "$http", '$location', '$timeout', '$rootScope', '$compile', 'MasterMappingService', MastersController];
    });
}(define));