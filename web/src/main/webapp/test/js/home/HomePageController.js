
(function (define) {
    define(['common/controllers/MenuController'], function () {
        var HomePageController = function ($scope, $http, $rootScope, $compile, $location, MenuDataService, BrandingDataService) {
            $scope.data = $scope.data || {};
            $rootScope.data = $rootScope.data || {};
            BrandingDataService.setBrandingData($rootScope.data.branding);
            $scope.data.formTemplate = {};
            $scope.data.menus = MenuDataService.getMenus();


            if($location.path().indexOf('home') > -1) {
                $http.get('js/jsondata.json').then(function (response) {
                    $scope.data.images = response.data.images;
                    $compile($(".formContainer").contents())($scope);
                });
                $scope.submitForm = function() {
                    alert('submitting form');
                };
            } else if($location.path().indexOf('productcreate') > -1) {
                $http.get('js/product_create.json').then(function (response) {
                    $scope.data.formTemplate =  response.data.formTemplate;
                    $compile($(".formContainer").contents())($scope);
                });
            }


        };
        return ["$scope", "$http", '$rootScope', "$compile", "$location", 'MenuDataService', 'BrandingDataService', HomePageController];
    });
}(define));