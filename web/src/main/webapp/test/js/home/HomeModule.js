(function(define) {
    define(['home/HomePageController', 'common/controllers/MenuController',
            'common/directives/menus', 'common/directives/form', 'common/directives/slideshow', 'common/services/MenuDataService',
            'common/services/BrandingDataService'],
        function(HomePageController, MenuController, menusDirective, formDirective, slideshowDirective, MenuDataService, BrandingDataService) {
            var moduleName = "homeModule";
            angular.module(moduleName, ['ngRoute', 'dynform'])
                .controller("MenuController", MenuController)
                .controller("HomePageController", HomePageController)
                .service("MenuDataService", MenuDataService)
                .service("BrandingDataService", BrandingDataService)
                .directive('menus', menusDirective)
                .directive('dynamicforms', formDirective)
                .directive('slider', slideshowDirective);
            return moduleName;
        });
}(define));