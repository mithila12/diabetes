define(['login/LoginModule',
    'admin/AdminModule',
    'home/HomeModule',
    'masters/MastersModule',
    'common/services/MenuDataService', 'common/services/BrandingDataService', 'RouteManager'],
    function(LoginModule, AdminModule, HomeModule, MastersModule, MenuDataService, BrandingDataService, RouteManager) {
        var appName = 'exportDMS',
            depends = ["ngRoute", "ui.bootstrap", "ngGrid", "ngAnimate", LoginModule, AdminModule, HomeModule, MastersModule],
            app = angular.module(appName, depends)
                .config(RouteManager)
                .service(MenuDataService)
                .service(BrandingDataService);
        angular.bootstrap(document.getElementById("app-body"), [appName]);
        return app;
    }
);